@echo off
setlocal

SET DEST_DIR=..\deploy\hostinger

pause

:: build frontend
cd client
call gulp build:admin --production
call gulp build:frontend --production

php artisan optimize --force

rd %DEST_DIR% /S /Q
mkdir %DEST_DIR%

REM //--------------------
REM // Copy WEB
echo Copying app...
xcopy app %DEST_DIR%\app /E /C /I /R /Y /Q

echo Copying bootstrap...
xcopy bootstrap %DEST_DIR%\bootstrap /E /C /I /R /Y /Q

echo Copying config...
xcopy config %DEST_DIR%\config /E /C /I /R /Y /Q

echo Copying public...
xcopy public %DEST_DIR%\public /E /C /I /R /Y /Q

:: copy lang and views folder only
mkdir %DEST_DIR%\resources /a
echo Copying resources...
xcopy resources\lang %DEST_DIR%\resources\lang /E /C /I /R /Y /Q
xcopy resources\views %DEST_DIR%\resources\views /E /C /I /R /Y /Q

:: copy structure of storage
echo Creating storage...
mkdir %DEST_DIR%\storage\app
mkdir %DEST_DIR%\storage\framework\cache
mkdir %DEST_DIR%\storage\framework\sessions
mkdir %DEST_DIR%\storage\framework\views
mkdir %DEST_DIR%\storage\logs

:: copy other file
echo Copying vendor...
mkdir %DEST_DIR%\vendor
copy vendor\autoload.php %DEST_DIR%\vendor\autoload.php  /B /Y
copy vendor\compiled.php %DEST_DIR%\vendor\compiled.php  /B /Y
copy vendor\services.json %DEST_DIR%\vendor\services.json  /B /Y

:: Copy enviroment
copy .env.hostinger %DEST_DIR%\.env  /B /Y
copy env\.htaccess %DEST_DIR%\.htaccess  /B /Y
copy env\public\.htaccess %DEST_DIR%\public\.htaccess  /B /Y

REM //--------------------
REM // Copy ADMIN
xcopy %DEST_DIR%\public %DEST_DIR%\restaurant /E /C /I /R /Y /Q

REM //--------------------
REM // Copy API
xcopy %DEST_DIR%\public %DEST_DIR%\rest-api /E /C /I /R /Y /Q

REM //--------------------
REM // Delete unused
rd /S /Q %DEST_DIR%\public\admin

:: // TODO: share image with multi subdomain
REM rd /S /Q %DEST_DIR%\restaurant\frontend

rd /S /Q %DEST_DIR%\rest-api\admin
rd /S /Q %DEST_DIR%\rest-api\frontend
rd /S /Q %DEST_DIR%\rest-api\css
rd /S /Q %DEST_DIR%\rest-api\js

echo Finish.

endlocal