@echo off

setlocal

echo -----------------------------------------------------
echo IMPORTANT!
echo.
echo  DB will be reset. Please check connected DB
echo -----------------------------------------------------
type .\.env | findstr "DB_"
Set /P RUN=[IMPORTANT] Do you want to reset database?[y/n]

if "%RUN%" == "y" goto :MAIN

goto :eof

:MAIN
php artisan migrate:rollback
php artisan migrate
php artisan db:seed --class=DatabaseSeeder

endlocal