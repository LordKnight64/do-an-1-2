@echo off
setlocal

SET ANT_HOME=D:\DEV\Tools\Java\apache-ant-1.9.7
SET ANT_LIB=%ANT_HOME%\lib
SET ANT_DIR=%ANT_HOME%\bin
SET JAVACMD=D:\DEV\IDE\pleiades\java\8\bin\java.exe
SET GEN_DIR=.\temp
SET DEST_DIR=.\..\..
REM SET DEST_DIR=.\cuong

REM %ANT_DIR%\ant --help
call %ANT_DIR%\ant -lib "%ANT_LIB%;lib\**.*" -f gen-crud.xml

del %GEN_DIR%\App\Models\Permissions.php /F /S /Q
del %GEN_DIR%\App\Models\PermissionRole.php /F /S /Q
del %GEN_DIR%\App\Models\PermissionUser.php /F /S /Q
del %GEN_DIR%\App\Models\Roles.php /F /S /Q
del %GEN_DIR%\App\Models\RoleUser.php /F /S /Q
del %GEN_DIR%\App\Models\Users.php /F /S /Q
del %GEN_DIR%\App\Models\Migrations.php /F /S /Q

REM mkdir %DEST_DIR%
REM xcopy %GEN_DIR%\App %DEST_DIR%\app /E /C /I /R /Y /Q
REM xcopy %GEN_DIR%\resources %DEST_DIR%\resources /E /C /I /R /Y /Q

REM del %GEN_DIR%\App\Models\*.php /F /S /Q
REM rmdir %GEN_DIR%
REM rmdir null
REM rmdir /S /Q  temp

endlocal