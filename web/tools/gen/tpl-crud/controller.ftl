<?php
<#import "/lib.ftl" as lib>
<#if lib.copyright??>
${lib.copyright}
</#if>
<#assign tableClassName = entityDesc.simpleName>
<#assign tableVarName = lib.toVarName(entityDesc.simpleName)>
<#assign viewModuleForder = "">
<#if moduleName?has_content><#assign viewModuleForder = moduleName?lower_case + "."></#if>
namespace ${packageName};

use Log;
use Illuminate\Http\Request;
use App\Models\${tableClassName};
use App\Services\${tableClassName}Service;

/**
<#if entityDesc.showDbComment && entityDesc.comment??>
 * ${entityDesc.comment}
</#if>
<#if lib.author??>
 * @author ${lib.author}
</#if>
 *
 */ 
class ${simpleName} extends BaseController {
	protected $${tableVarName}Service;



	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(${tableClassName}Service $${tableVarName}Service)
    {
        $this->${tableVarName}Service = $${tableVarName}Service;
    }

    /**
     * Load list page
     * @return [Page] list page
     */
    public function index() {

        $result = [
            "setting" => [
                "url" => "/${tableVarName}"
            ]
        ];

        $data['model'] = json_encode($result);
    	return view("${viewModuleForder}${tableVarName}.index")->with($data);
    }

    /**
     * Search
     * @param  Request $request [description]
     * @return [JSON]           [description]
     */
    public function search(Request $request){
        $result = $this->${tableVarName}Service->search($request->all());
        return response()->json($result);
    }

    /**
     * Load create page
     * @return [type] [description]
     */
    public function create() {
        return view("admin.users.create");
    }

    /**
     * Create user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function insert(Request $request) {

        <#assign propList = lib.getEditProperty(entityDesc.ownEntityPropertyDescs)  />
        /*$this->validate($request,[
        	<#list propList as property>
        		<#assign temp = lib.getValidationStr(property, mapColumns[property.columnName])  />
        		<#if temp?has_content>${temp}<#sep>,</#sep> </#if>    		
        	</#list>
        ]);*/

        $param = $request->all();
        $this->${tableVarName}Service->create($param);
        return redirect("/${tableVarName}");
    }

    /**
     * Load update page
     * @param  [type] $account [description]
     * @param  [type] $id      [description]
     * @return [type]          [description]
     */
    public function update($account, $id) {

        $entity = $this->${tableVarName}Service->load($id);

        if( empty($entity) ) {
            abort(404);
        }

        $data = [
            'model'     => $entity
        ];

        return view("${viewModuleForder}${tableVarName}.update")->with($data);
    }

    /**
     * Update ${tableName}
     * @param  [type]  $account [description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function save($account, $id, Request $request) {

        <#assign propList = lib.getEditProperty(entityDesc.ownEntityPropertyDescs)  />
        /*$this->validate($request,[
        	<#list propList as property>
        		<#assign temp = lib.getValidationStr(property, mapColumns[property.columnName])  />
        		<#if temp?has_content>${temp}<#sep>,</#sep> </#if>    		
        	</#list>
        ]);*/

        $param = $request->all();
        $param['id'] = $id;
        $this->${tableVarName}Service->update($param);

        return redirect("/${tableVarName}");
    }

    /**
     * Delete ${tableName}
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete($account, $id, Request $request) {
        $result = $this->${tableVarName}Service->delete($request->input('id'));
        return response()->json($result);
    }

    /**
     * Load view page
     * @param  [type] $account [description]
     * @param  [type] $id      [description]
     * @return [type]          [description]
     */
    public function view($account, $id) {

        $entity = $this->${tableVarName}Service->load($id);

        if( empty($user) ) {
            abort(404);
        }

        $data = [
            'model'      => $entity,
        ];

        return view("${viewModuleForder}${tableVarName}.view")->with($data);
    }
}