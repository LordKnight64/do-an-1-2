/*
|--------------------------------------------------------------------------
| GenericDesc
|--------------------------------------------------------------------------
|
*/
	private String templateName;
	private String basePackage;
	private String moduleName;
	private String moduleNameCamel;
	private String entityName;
	private String entityNameCamel;
	private String entityClass;
	private String tableName;
	private EntityDesc entityDesc;
	private Map<String, EntityDesc> mapEntityDesc;
	private Map<String, TableMeta> mapTableMeta;
	private TableMeta tableMeta;
	private Vector<KeyValueConfig> packageSettings = new Vector<KeyValueConfig>();
	private Vector<KeyValueConfig> fileSettings = new Vector<KeyValueConfig>();
	private Map<String, ColumnMeta> mapColumns = new HashMap<String, ColumnMeta>();
/*
|--------------------------------------------------------------------------
| EntityDesc
|--------------------------------------------------------------------------
|
*/
	protected String catalogName;
	protected String schemaName;
	protected String tableName;
	protected String qualifiedTableName;
	protected String superclassSimpleName;
	protected String listenerClassSimpleName;
	protected NamingType namingType;
	protected String originalStatesPropertyName;
	protected boolean compositeId;
	protected boolean showCatalogName;
	protected boolean showSchemaName;
	protected boolean showTableName;
	protected boolean useAccessor;
	protected boolean useListener;
	protected boolean showDbComment;
	protected String templateName;
	protected final List<EntityPropertyDesc> entityPropertyDescs = new ArrayList();
	protected final List<EntityPropertyDesc> ownEntityPropertyDescs = new ArrayList();
	protected final List<EntityPropertyDesc> idEntityPropertyDescs = new ArrayList();
	protected EntityPropertyDesc versionEntityPropertyDesc;

/*
|--------------------------------------------------------------------------
| EntityPropertyDesc
|--------------------------------------------------------------------------
|
*/
	protected String name;
	protected String propertyClassName;
	protected boolean id;
	protected GenerationType generationType;
	protected Long initialValue;
	protected Long allocationSize;
	protected boolean version;
	protected String columnName;
	protected String comment;
	protected boolean showColumnName;
	protected boolean number;
	protected boolean time;
	protected boolean date;
	protected boolean timestamp;
	protected String entityClassName;

/*
|--------------------------------------------------------------------------
| TableMeta
|--------------------------------------------------------------------------
|
*/
	protected String catalogName;
	protected String schemaName;
	protected String name;
	protected String comment;
	protected final List<ColumnMeta> columnMetas = new ArrayList();
	protected final List<ColumnMeta> primaryKeyColumnMetas = new ArrayList();

/*
|--------------------------------------------------------------------------
| ColumnMeta
|--------------------------------------------------------------------------
|
*/
	protected String name;
	protected int sqlType;
	protected String typeName;
	protected int length;
	protected int scale;
	protected String defaultValue;
	protected boolean nullable;
	protected boolean primaryKey;
	protected boolean autoIncrement;
	protected boolean unique;
	protected String comment;
	protected TableMeta tableMeta;

/*
|--------------------------------------------------------------------------
| sqlType
|--------------------------------------------------------------------------
|
*/
		BIT             =  -7;
		TINYINT         =  -6;
		SMALLINT        =   5;
		INTEGER         =   4;
		BIGINT          =  -5;
		FLOAT           =   6;
		REAL            =   7;
		DOUBLE          =   8;
		NUMERIC         =   2;
		DECIMAL         =   3;
		CHAR            =   1;
		VARCHAR         =  12;
		LONGVARCHAR     =  -1;
		DATE            =  91;
		TIME            =  92;
		TIMESTAMP       =  93;
		BINARY          =  -2;
		VARBINARY       =  -3;
		LONGVARBINARY   =  -4;
		NULL            =   0;
		OTHER           = 1111;
		JAVA_OBJECT     = 2000;
		DISTINCT        = 2001;
		STRUCT          = 2002;
		ARRAY           = 2003;
		BLOB            = 2004;
		CLOB            = 2005;
		REF             = 2006;
		DATALINK        = 70;
		BOOLEAN         = 16;
		ROWID           = -8;
		NCHAR           = -15;
		NVARCHAR        = -9;
		LONGNVARCHAR    = -16;
		NCLOB           = 2011;
		SQLXML          = 2009;
		REF_CURSOR      = 2012;
		TIME_WITH_TIMEZONE = 2013;
		TIMESTAMP_WITH_TIMEZONE = 2014;