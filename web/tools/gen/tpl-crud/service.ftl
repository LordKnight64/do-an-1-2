<?php
<#import "/lib.ftl" as lib>
<#if lib.copyright??>
${lib.copyright}
</#if>
<#assign tableClassName = entityDesc.simpleName>
<#assign tableVarName = lib.toVarName(entityDesc.simpleName)>
namespace App\Services;

use DB;
use Log;
use Carbon\Carbon;

use App\Models\${tableClassName};

/**
<#if entityDesc.showDbComment && entityDesc.comment??>
 * ${entityDesc.comment}
</#if>
<#if lib.author??>
 * @author ${lib.author}
</#if>
 *
 */ 
class ${simpleName} extends BaseService {
	
	/**
	 * Search ${tableName}
	 * 
	 * @param  array
	 * 		[
	 * 			field => value,
	 * 			page  => 1,
	 * 			pageSize => 50,
	 * 			order => value
	 * 		]
	 * @return [type]
	 */
	public function search($param) {

		$sqlParam = array();
        $sql = "
			select
			<#list entityDesc.ownEntityPropertyDescs as property>
        	  ${property.columnName}<#sep>,</#sep>  		
        	</#list>
			from
			  ${tableName}
			where 
			  del_flg = '0'
			";

		<#assign propList = lib.getEditProperty(entityDesc.ownEntityPropertyDescs)  />
		<#list propList as property>
    	// ${lib.getSearchWhere(property, mapColumns[property.columnName])}
    	</#list>

  		//$sql .= "
  		//	order by id asc
        //  ";

        $result = $this->pagination($sql, $sqlParam, $param);
        return $result;
	}

	/**
	 * Create ${tableName}
	 * 
	 * @param  array
	 * 		[
	 * 			field => value
	 * 		]
	 * @return [type]
	 */
	public function create($param) {
  		$logonUser = $this->getCurrentUser();

        $entity = new ${tableClassName}();
        <#assign propList = lib.getEditProperty(entityDesc.ownEntityPropertyDescs)  />
		<#list propList as property>
		${lib.getSetColumnStr(property, mapColumns[property.columnName])}
    	</#list>
        $entity->created_by = $logonUser->id;
        $entity->updated_by = $logonUser->id;
        $entity->save();

        <#list entityDesc.idEntityPropertyDescs as property>
    	<#sep>//</#sep>return $entity->${property.columnName};
    	</#list>
        
	}

	/**
	 * Update user
	 * 
	 * @param  array
	 * 		[
	 * 			field => value
	 * 		]
	 * @return [type]
	 */
	public function update($param) {
  		$logonUser = $this->getCurrentUser();

        $entity = ${tableClassName}::find($param['id']);
        if( isset( $entity) ) {
        	<#assign propList = lib.getEditProperty(entityDesc.ownEntityPropertyDescs)  />
			<#list propList as property>
			${lib.getSetColumnStr(property, mapColumns[property.columnName])}
	    	</#list>
	        $entity->updated_by = $logonUser->id;
	        $entity->save();

	        return true;
        } 
        
        return false;
	}

	/**
	 * Load ${tableName}
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function load($id) {
		return ${tableClassName}::find($id);
	}

	/**
	 * Delete ${tableName}
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id) {
  		$logonUser = $this->getCurrentUser();

        $entity = ${tableClassName}::find($id);
        if( isset( $entity) ) {
        	$entity->del_flg = '1';
        	$entity->updated_by = $logonUser->id;
        	$entity->save();
        	return $this->ok();
        } 
        
        return $this->fail();
	}

}
