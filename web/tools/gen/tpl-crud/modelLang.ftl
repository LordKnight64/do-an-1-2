<#import "/lib.ftl" as lib>
<?php
return [

<#list mapTableMeta?keys as key>
    <#assign tabeMeta = mapTableMeta[key]  />
    /*
    |--------------------------------------------------------------------------
    | Table: ${tabeMeta.name}
    |--------------------------------------------------------------------------
    |
    */
    <#list tabeMeta.columnMetas as column>
    '${tabeMeta.name}_${column.name}' => '${tabeMeta.name}_${column.name}'<#sep>,</#sep>
    </#list>
</#list>


    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',


];
