<#assign copyright>
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */
</#assign>
<#assign author="SystemEXE VN">

<#function isRecordHeader columnName>
	<#if columnName == 'created_at' ||
         columnName == 'created_by' ||
         columnName == 'updated_at' ||
         columnName == 'updated_by' ||
         columnName == 'version_no' ||
         columnName == 'del_flg'>
         <#return true>
    </#if>
	<#return false>
</#function>

<#function getValueOf list key>
	<#list list as item>
	    <#if item.key == key>
	    	<#return item.value>
	    </#if>
	</#list>
	<#return "">
</#function>

<#function tableNamePascal tableName>
	<#assign tableNameDao><#list tableName?split("_") as x>${x?cap_first}</#list></#assign>
	<#return tableNameDao>
</#function>

<#function copyProperty var1 var2 property var2Type>
	<#switch var2Type>
		<#case 'String'>
	<#return var1 + ".set" + property.name?cap_first + "(String.valueOf(" + var2  + ".get" + property.name?cap_first + "()));">
		<#default>
	<#return var1 + ".set" + property.name?cap_first + "(" + var2  + ".get" + property.name?cap_first + "());">
	</#switch>

</#function>

<#function copyProperty2String var1 var2 property var2Type>
	<#switch var2Type>
		<#case 'String'>
	<#return var1 + ".set" + property.name?cap_first + "(String.valueOf(" + var2  + ".get" + property.name?cap_first + "()));">
		<#default>
	<#return var1 + ".set" + property.name?cap_first + "(" + toString(property, var2) + ");">
	</#switch>

</#function>

<#function toString property varName>
	<#local result = ""/>
	<#if property.propertyClassName == "java.lang.String">
		<#local result = varName + ".get" + property.name?cap_first + "()"/>
	<#else>
		<#local result = "String.valueOf(" + varName + ".get" + property.name?cap_first + "())"/>
	</#if>
	<#return result>
</#function>

<#function isFillableColumn column>
	<#if column.columnName == 'created_at' ||
         column.columnName == 'created_by' ||
         column.columnName == 'updated_at' ||
         column.columnName == 'updated_by' ||
         column.columnName == 'version_no' ||
         column.columnName == 'del_flg'>
         <#return false>
    </#if>
	<#return true>
</#function>

<#function toVarName tableName>
	<#local result = tableName?uncap_first />
	<#return result>
</#function>

<#function getEditProperty listProps>
	<#assign result = [  ] />
	<#list listProps as property>
		<#if isRecordHeader(property.columnName) == false && property.id == false >
			<#assign result = result + [ property ] />
		</#if>
	</#list>
	<#return result>
</#function>

<#function getEditColumns listProps>
	<#assign result = [ ] />
	<#list listProps as property>
		<#if isRecordHeader(property.name) == false >
			<#assign result = result + [ property ] />
		</#if>
	</#list>
	<#return result>
</#function>

<#function getValidationStr column columnMeta>
	<#assign result = "" />
	<#assign conditions = [] />

	<#if columnMeta.nullable == false>
		<#assign condition = "required" />
		<#assign conditions = conditions + [condition] />
	</#if>

	<#if columnMeta.unique == true>
		<#assign condition = "unique:" + columnMeta.tableMeta.name />
		<#assign conditions = conditions + [condition] />
	</#if>

	<#if column.columnName == "del_flg">
		<#assign condition = "boolean" />
		<#assign conditions = conditions + [condition] />
	<#elseif column.propertyClassName == "java.lang.String">
		<#assign condition = "max:" + columnMeta.length?string />
		<#assign conditions = conditions + [condition] />
	<#elseif column.number>
		<#assign condition = "numeric" />
		<#assign conditions = conditions + [condition] />
	<#elseif column.propertyClassName == "java.time.LocalDate">
		<#assign condition = "date|date_format:yyyy-MM-dd" />
		<#assign conditions = conditions + [condition] />
	<#elseif column.propertyClassName == "java.time.LocalDateTime">
		<#assign condition = "date|date_format:yyyy-MM-dd HH:mm" />
		<#assign conditions = conditions + [condition] />
	<#elseif column.propertyClassName == "java.time.LocalTime">
		<#assign condition = "date|date_format:HH:mm" />
		<#assign conditions = conditions + [condition] />
	</#if>

	<#if conditions?has_content>
		<#assign result = "\"" + column.columnName + "\"" + " => \"" + conditions?join("|") + "\"" />
	</#if>
	<#return result>
</#function>

<#function getSearchWhere column columnMeta>
	<#assign result = "$sql .= " />

	<#if column.propertyClassName == "java.lang.String">
		<#assign result = result + "$this->andWhereString(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.number>
		<#assign result = result + "$this->andWhereNumber(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.propertyClassName == "java.time.LocalDate">
		<#assign result = result + "$this->andWhereDate(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.propertyClassName == "java.time.LocalDateTime">
		<#assign result = result + "$this->andWhereDateTime(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.propertyClassName == "java.time.LocalTime">
		<#assign result = result + "$this->andWhereTime(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	</#if>

	<#return result>
</#function>

<#--
	Set column when create, update
	$entity->${property.columnName} = $param["${property.columnName}"];
-->
<#function getSetColumnStr column columnMeta>
	<#assign result = "" />
	<#assign result = result + "$entity->" + column.columnName />
	<#assign result = result + " = " />
		
	<#if column.propertyClassName == "java.time.LocalDate">
		<#assign result = result + "$this->dateFromString($param[\"" + column.columnName + "\"]);" />
	<#elseif column.propertyClassName == "java.time.LocalDateTime">
		<#assign result = result + "$this->dateTimeFromString($param[\"" + column.columnName + "\"]);" />
	<#elseif column.propertyClassName == "java.time.LocalTime">
		<#assign result = result + "$this->timeFromString($param[\"" + column.columnName + "\"]);" />
	<#else>
		<#assign result = result + "$param[\"" + column.columnName + "\"];" />
	</#if>
	
	<#return result>
</#function>

<#function getFactoryStr column columnMeta>
	<#assign result = column.columnName + " => $faker->" />
	
	<#if column.propertyClassName == "java.lang.String">
		<#assign result = result + "$this->andWhereString(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.number>
		<#assign result = result + "$this->andWhereNumber(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.propertyClassName == "java.time.LocalDate">
		<#assign result = result + "$this->andWhereDate(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.propertyClassName == "java.time.LocalDateTime">
		<#assign result = result + "$this->andWhereDateTime(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	<#elseif column.propertyClassName == "java.time.LocalTime">
		<#assign result = result + "$this->andWhereTime(" />
		<#assign result = result + "$param, '" + column.columnName + "', '"  + column.columnName + "', $sqlParam );" />
	</#if>

	<#return result>
</#function>