<#import "/lib.ftl" as lib>
@extends('layouts.admin')

@section('page-header')
<section class="content-header">
    <h1>
        Người dùng
        <small>Danh sách</small>
    </h1>

    <!-- <div class="breadcrumb text-right">
        <a class="btn btn-success btn-width" href="[[url('/users/create')]]"><i class="fa fa-plus fa-fw"></i>Thêm</a>
    </div> -->

    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
        <li class="active">Người dùng</li>
    </ol>

</section>
@endsection


@section('bodyAttr')
ng-app="appModule"
@endsection

@section('content')
    <div class="row" ng-controller="BaseSearchCtrl" ng-init="init(<%$model%>)" ng-cloak>
        <div class='col-md-12'>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách người dùng</h3>
                    <div class="box-tools pull-right">
                         <a class="btn btn-block btn-success btn-xs" href="[[url('/users/update')]]"><i class="fa fa-plus fa-fw"></i>Thêm</a>
                    </div>
                </div>
                <div class="box-header with-border">
                    <form role="form" ng-submit="search(1)">
                        <div class="col-md-3 col-sm-6 m-b-xs">
                            <div class="form-group">
                                <label>Tên</label>
                                <input type="text" ng-model="m.filter.name" class="form-control"
                                    placeholder='Tên'>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 m-b-xs">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" ng-model="m.filter.email" class="form-control"
                                    placeholder='email@example.com'>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 m-b-xs">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-width">
                                    <i class="fa fa-search fa-fw"></i>Tìm
                                </button>
                                <a class="btn btn-default btn-width" href="javascript:void(0)" ng-click="clear()"><i class="fa fa-eraser fa-fw"></i>Tìm lại</a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="box-body">
                    <div class="table-responsive">
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>Địa chỉ email</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat='item in m.list'>
                                    <td>{{$index + m.paginationInfo.from}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.email}}</td>
                                    <td>
                                        <a href="<% url('users/view') %>/{{item.id}}">
                                            <i class="fa fa-fw fa-eye"></i>
                                        </a>
                                        <a href="<% url('users/update') %>/{{item.id}}">
                                            <i class="fa fa-fw fa-edit"></i>
                                        </a>
                                        <a ng-click="confirmDelete($event, item, {userId: item.id }, item.id)" href="#">
                                            <i class="fa fa-fw fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                    
                    <div class="alert alert-danger" ng-if="m.paginationInfo.totalRecord == 0 && m.searched">
                        [[trans('labels.E000001')]]
                    </div>
                    <div class="alert alert-info" ng-if="m.paginationInfo.totalRecord == 0 && !m.searched">
                        [[trans('labels.I000001')]]
                    </div>
                </div>

                <footer class="panel-footer">
                    <div class="row" ng-if="m.paginationInfo.totalRecord > 0">
                        <div class="col-sm-4 hidden-xs">
                            <select ng-model="m.paginationInfo.pageSize"
                                class="input-sm form-control w-sm inline v-middle select-page-size"
                                ng-change="changePageSize()"
                                ng-options="item as item for item in m.listPageSize">
                            </select>
                        </div>

                        <div class="col-sm-4 text-center">
                            <small class="text-muted inline m-t-sm m-b-sm">{{m.paginationInfo.from}}-{{m.paginationInfo.to}}/{{m.paginationInfo.totalRecord}}</small>
                        </div>

                        <div class="col-sm-4 text-right text-center-xs">
                            <pagination ng-show="m.paginationInfo.page > 0"
                                total-items="m.paginationInfo.totalRecord"
                                ng-model="m.paginationInfo.page"
                                items-per-page="m.paginationInfo.pageSize"
                                ng-change="doSearch(m.paginationInfo.page)"
                                class= "pagination-sm m-t-none m-b-none">
                            </pagination>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
<script src="/js/libs/angular.min.js"></script>
<script src="/js/libs/angular-animate.min.js"></script>
<script src="/js/libs/toaster.js"></script>
<script src="/js/libs/angular-strap.min.js"></script>
<script src="/js/libs/angular-strap.tpl.min.js"></script>
<script src="/js/libs/ui-bootstrap-custom-pagination-tpls-0.13.3.js"></script>
<script src="/js/app.js"></script>
<script src="/js/utils/DateUtils.js"></script>
<script src="/js/services/ClientService.js"></script>
<script src="/js/services/ServerService.js"></script>
<script src="/js/BaseSearchCtrl.js"></script>
@endsection
