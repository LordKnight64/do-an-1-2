<#import "/lib.ftl" as lib>
<#assign tableClassName = entityDesc.simpleName>
<#assign tableVarName = lib.toVarName(entityDesc.simpleName)>
<?php

<#list mapEntityDesc?keys as key>
    <#assign itemEntity = mapEntityDesc[key]  />
/*
|--------------------------------------------------------------------------
| Table: ${itemEntity.tableName}
|--------------------------------------------------------------------------
|
*/
    
$factory->define(App\Models\${itemEntity.simpleName}::class, function (Faker\Generator $faker) {

    return [
    <#list itemEntity.ownEntityPropertyDescs as prop>
        <#assign temp = lib.getFactoryStr(prop, mapTableColumn[key][prop.columnName])  />
        <#if temp?has_content>${temp}<#sep>,</#sep> </#if>           
    </#list>
    ];
});
    
</#list>

