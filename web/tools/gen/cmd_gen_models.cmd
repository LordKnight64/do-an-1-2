@echo off
setlocal

SET ANT_HOME=D:\DEV\Tools\Java\apache-ant-1.9.7
SET ANT_LIB=%ANT_HOME%\lib
SET ANT_DIR=%ANT_HOME%\bin
SET JAVACMD=D:\DEV\IDE\pleiades\java\8\bin\java.exe
SET GEN_DIR=temp\tpl-models
SET DEST_DIR=..\..\app

REM %ANT_DIR%\ant --help
call %ANT_DIR%\ant -lib "%ANT_LIB%;lib\**.*" -f gen-model.xml

del %GEN_DIR%\App\Models\MstPermission.php /F /S /Q
del %GEN_DIR%\App\Models\MstPermissionRole.php /F /S /Q
del %GEN_DIR%\App\Models\MstPermissionUser.php /F /S /Q
del %GEN_DIR%\App\Models\MstRole.php /F /S /Q
del %GEN_DIR%\App\Models\MstRoleUser.php /F /S /Q
del %GEN_DIR%\App\Models\MstUser.php /F /S /Q
del %GEN_DIR%\App\Models\Migrations.php /F /S /Q

mkdir %DEST_DIR%\Models
xcopy %GEN_DIR%\App\Models %DEST_DIR%\Models /E /C /I /R /Y

del %GEN_DIR%\App\Models\*.php /F /S /Q
rmdir %GEN_DIR%
rmdir null
rmdir /S /Q  temp

endlocal