<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$DOMAIN_MAIN = env('DOMAIN_MAIN', 'mysite.local');
$DOMAIN_WWW = env('DOMAIN_WWW', 'mysite.local');
$DOMAIN_ADMIN = env('DOMAIN_ADMIN', 'admin.mysite.local');
$DOMAIN_API = env('DOMAIN_API', 'api.mysite.local');

Route::group(['domain' => $DOMAIN_WWW, 'namespace' => 'Frontend'], function () {
    Log::debug('$DOMAIN_WWW');
    Route::get('/', 'HomeController@index');
    Route::post('/changeLanguage', 'HomeController@changeLanguage');
    Route::get('/contact', 'ContactController@index');
    Route::get('/about', 'AboutController@index');
    Route::get('/features', 'FeaturesController@index');
    Route::get('/howitswork', 'HowItsWorkController@index');
    Route::get('/sitemap', 'SiteMapController@index');
    Route::get('/addRestaurant', 'AddRestaurantController@index');
    Route::get('/order', 'OrderController@index');
    Route::get('/orderBest', 'OrderController@indexBest');
    Route::get('/orderDiscount', 'OrderController@indexDiscount');
    Route::get('/404', function () {
        return View::make('frontend.404');
    });


    Route::get('/restaurantDetail', 'RestaurantDetailController@index');
    Route::get('/orderDetail', 'OrderDetailController@index');
    Route::get('/checkout', 'CheckoutController@index');
    Route::post('/payCheckout', 'CheckoutController@payCheckout');
    Route::post('/postCheckout', 'CheckoutController@postCheckout');
    Route::get('/reviewCheckout', 'CheckoutController@reviewCheckout');
    // Route::get('/restaurantList', 'RestaurantListController@index');
    Route::get('/profile', 'ProfileController@index');
    Route::get('/addRestaurantForm', 'AddRestaurantFormController@index');
    Route::post('/addRestaurantForm', 'AddRestaurantFormController@addRestaurant');
    Route::get('/restaurantActive', 'AddRestaurantFormController@restaurantActive');

    Route::post('/profile/detail', 'ProfileController@saveProfile');
    Route::post('/profile/password', 'ProfileController@changePassword');
    Route::get('/historyOrder', 'HistoryOrderController@index');
    Route::get('/historyOrderDetail', 'HistoryOrderDetailController@index');
    Route::get('/notificationList', 'NotificationListController@index');
    //Route::get('/userLogin', 'UserLoginController@index');
    //Route::post('/userLogin', 'UserLoginController@doLogin');
    Route::post('/registerUrlOther', 'UserRegisterController@registerUrlOther');
    Route::post('/sendMailUrlOther', 'UserRegisterController@sendMailUrlOther');
    Route::get('/userRegister', 'UserRegisterController@index');
    Route::post('/userRegister', 'UserRegisterController@doRegister');
    //Route::get('/addWishList','WishListController@index');
    Route::post('/addWishList','WishListController@index');
    Route::get('/myWishList','WishListController@loadMyWishlist');
    Route::get('/deleteWishlist','WishListController@deleteMyWishlist');
    Route::get('/newsDetail','NotificationDetailController@index');

    Route::get('/faq','FAQController@index');
    Route::get('/privatepolicy','PrivatePolicyController@index');
    Route::get('/termandcondition','TermAndConditionController@index');
   
    //Route::prefix('users')->group(function(){
    //Route::post('/password/email','UserForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
    //Route::get('/password/reset','UserForgotPasswordController@showLinkRequestForm')->name('user.password.request');
    //Route::post('/password/reset','UserResetPasswordController@reset');
    //Route::get('/password/reset/{token}','UserResetPasswordController@showResetForm')->name('user.password.reset');

    Route::post('/forgot','UserForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
    Route::get('/reset','UserForgotPasswordController@showLinkRequestForm')->name('user.password.request');
    Route::post('/reset','UserResetPasswordController@reset');
    Route::get('/reset{token}','UserResetPasswordController@showResetForm')->name('user.password.reset');
//});
    Route::get('/restaurantLogin', 'RestaurantLoginController@index');
    Route::get('/aboutSystem', 'AboutSystemController@index');
    Route::get('/search', 'SearchController@search');
    Route::post('/sendReview', 'OrderDetailController@sendReview');

    // Route::get('/search', 'SearchController@search');
    Route::get('/autocomplete', 'SearchController@autocomplete');
    // Log::debug('$LoginController');
    Route::post('/login', 'LoginController@postLogin');
    Route::post('/getRestaurantAfterLogin', 'LoginController@getRestaurantAfterLogin');
    //maps
    Route::post('/getAddress', 'GeoController@get_address_by_latlon');
    Route::post('/getLatlon', 'GeoController@get_latlon_by_address');
    Route::post('/get_distance_by_address', 'GeoController@get_distance_by_address');

    Route::get('/logout', ['as' => 'frontend.logout' , 'uses' => 'LogoutController@getLogout']);
    Route::post('/checkTab1', 'AddRestaurantFormController@tab1');
    Route::post('/checkTab2', 'AddRestaurantFormController@tab2');

    Route::post('/sent_contact', 'ContactController@sent_contact');
    Route::post('/sent_contact_restaurant', 'ContactController@sent_contact_restaurant');
    Route::post('/request_demo', 'DemoController@request_demo');
    Route::post('/subscribe', 'SubscribeController@doSubscribe');
  

    // Route::post('/checkout', 'CheckoutController@checkout');
    Route::post('/get_restaurants_by_locations', 'RestaurantListController@get_restaurants_by_locations');
    Route::post('/getRestaurantById', 'RestaurantDetailController@getRestaurantById');
    Route::get('/sendMail', 'SendMailController@index');
    Route::post('/sendMail', 'SendMailController@sendMail');
    //push
    Route::post('/store_endpoint', 'PushNotificationController@store');
    Route::get('/get_endpoint', 'PushNotificationController@get_all');
    Route::post('/push_notification', 'PushNotificationController@push_notification');
    Route::get('/404', 'PageNotFoundController@index');
    Route::get("{any}", function () {
        return redirect('404');
    });
});

Route::group(['domain' => $DOMAIN_WWW, 'namespace' => 'Frontend', 'middleware' => ['web', 'auth']], function () {
});

Route::group(['domain' => $DOMAIN_ADMIN], function () {
    Route::get('/', function () {
        return view('admin.index');
    });

    Route::get('/locale/{fileName}', function ($fileName) {
        $arr = explode(".", $fileName);
        $locale = $arr[0];

        //set locale
        App::setLocale($locale);

        //get file locale
        $fileLocale = Lang::get('admin/labels');

        return response()->json($fileLocale);
    });
});

// Auth::routes();
