<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

$DOMAIN_API = env('DOMAIN_API', 'api.mysite.local');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// $api = app('Dingo\Api\Routing\Router');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::post('/heroes', function (Request $request) {

// 	$param = $request->all();
// 	Log::debug('**************data**************', $param);

//     return response()->json([]);
// });

$api->group(['domain' => $DOMAIN_API, 'middleware' => ['api']], function ($api) {
    $api->get('/test1', 'TestController@test1');
    $api->post('/login', 'AuthController@login');
    //push
    $api->post('/store_endpoint', 'PushNotificationController@store');
    $api->get('/get_endpoint', 'PushNotificationController@get_all');
    $api->post('/push_notification', 'PushNotificationController@push_notification');
    //geo
    $api->post('/get_coordinates', 'GeoLocationController@get_coordinates');
    $api->post('/get_distance', 'GeoLocationController@get_distance');
    $api->post('/get_coordinates_by_addrress', 'GeoLocationController@get_coordinates_by_addrress');
    $api->post('/find_restaurants', 'GeoLocationController@find_restaurants');
    $api->post('/get_addrress_by_latlon', 'GeoLocationController@get_addrress_by_latlon');
    $api->post('/get_data_coordinates', 'GeoLocationController@get_data_coordinates');

    //api moblie app
    $api->post('/mobile_app/signup', 'MobileApp\SignupController@signup');
    $api->post('/mobile_app/login', 'MobileApp\LoginController@login');
    $api->post('/mobile_app/forgot_password', 'MobileApp\ForgotPasswordController@forgot_password');
    // $api->post('/mobile_app/get_reset', 'MobileApp\ResetPasswordController@get_reset');
    // $api->post('/mobile_app/post_reset', 'MobileApp\ResetPasswordController@post_reset');
    $api->post('/mobile_app/get_distance', 'MobileApp\GeoLocationController@get_distance');
    $api->post('/mobile_app/get_addrress_by_latlon', 'MobileApp\GeoLocationController@get_addrress_by_latlon');
    $api->post('/mobile_app/request_login', 'MobileApp\LoginController@request_login');
    $api->post('/mobile_app/get_news', 'MobileApp\NewsController@get_news');
    $api->post('/mobile_app/get_restaurant_detail', 'MobileApp\RestaurantController@get_restaurant_detail');
    $api->post('/mobile_app/get_restaurants_location', 'MobileApp\RestaurantController@get_restaurants_location');
    $api->post('/mobile_app/get_restaurants_by_province', 'MobileApp\RestaurantController@get_restaurants_by_province');
    $api->post('/mobile_app/find_restaurants', 'MobileApp\RestaurantController@find_restaurants');
    $api->post('/mobile_app/subscribe', 'MobileApp\TokenDevicesController@subscribe');
    $api->post('/mobile_app/get_ordered_restaurants_by_user', 'MobileApp\RestaurantController@get_ordered_restaurants_by_user');
    $api->post('/mobile_app/get_total_news', 'MobileApp\NewsController@get_total_news');
    $api->post('/mobile_app/get_provinces_by_city', 'MobileApp\ProvinceController@get_provinces_by_city');
    $api->post('/mobile_app/get_provinces_by_axis', 'MobileApp\ProvinceController@get_provinces_by_axis');
});

$api->group(['domain' => $DOMAIN_API, 'middleware' => ['api', 'jwt.auth']], function ($api) {

    //api moblie app
    $api->post('/mobile_app/logout', 'MobileApp\LogoutController@logout');
    $api->post('/mobile_app/change_password', 'MobileApp\ChangePasswordController@change_password');
    $api->post('/mobile_app/update_profile', 'MobileApp\UpdateProfileController@update_profile');
    $api->post('/mobile_app/get_notifications', 'MobileApp\NotificationsController@get_notifications');
    $api->post('/mobile_app/get_address_list', 'MobileApp\AddressController@get_address_list');
    $api->post('/mobile_app/update_address', 'MobileApp\AddressController@update_address');
    $api->post('/mobile_app/update_address', 'MobileApp\AddressController@update_address');
    $api->post('/mobile_app/get_orders_by_userid', 'MobileApp\OrderController@get_orders_by_userid');
    $api->post('/mobile_app/get_point', 'MobileApp\OrderController@get_point');
    $api->post('/mobile_app/set_favorite_by_id', 'MobileApp\OrderController@set_favorite_by_id');
    $api->post('/mobile_app/request_order_by_user_id', 'MobileApp\OrderController@request_order_by_user_id');
    $api->post('/mobile_app/get_orders_by_restid', 'MobileApp\OrderController@get_orders_by_restid');
    $api->post('/mobile_app/get_best_sale_by_restid', 'MobileApp\BestSaleController@get_best_sale_by_restid');
    $api->post('/mobile_app/get_item_detail_by_id', 'MobileApp\ItemController@get_item_detail_by_id');
    $api->post('/mobile_app/update_order_by_id', 'MobileApp\OrderController@update_order_by_id');
    $api->post('/mobile_app/send_contact', 'MobileApp\ContactController@send_contact');
    $api->post('/mobile_app/send_feedback', 'MobileApp\FeedbackController@send_feedback');

    $api->post('/test2', 'TestController@test2');
    $api->post('/update_profile', 'UserController@update_profile');

    $api->post('/get_orders_by_rest_id', 'OrderController@get_orders_by_rest_id');
    $api->post('/update_order_by_id', 'OrderController@update_order_by_id');

    $api->post('/get_categories_by_rest_id', 'CategoryController@get_categories_by_rest_id');
    $api->post('/update_categories_by_id', 'CategoryController@update_categories_by_id');

    $api->post('/get_area_by_rest_id', 'AreaController@get_area_by_rest_id');
    $api->post('/update_area_by_id', 'AreaController@update_area_by_id');

    $api->post('/get_table_by_rest_id', 'TableController@get_table_list');
    $api->post('/update_table_by_id', 'TableController@update_table_by_id');


    $api->post('/update_reversation_by_id', 'TableReversationController@update_reversation_by_id');



    $api->post('/get_items_by_rest_id', 'ItemController@get_items_by_rest_id');
    $api->post('/update_item_by_id', 'ItemController@update_item_by_id');

    $api->post('/get_options_by_rest_id', 'OptionController@get_options_by_rest_id');
    $api->post('/update_option_by_id', 'OptionController@update_option_by_id');

    $api->post('/get_options_value_by_rest_id', 'OptionValueController@get_options_value_by_rest_id');
    $api->post('/update_option_value_by_id', 'OptionValueController@update_option_value_by_id');

    $api->post('/get_contacts_by_rest_id', 'ContactController@get_contacts_by_rest_id');
    $api->post('/get_contact_by_id', 'ContactController@get_contact_by_id');
    $api->post('/update_contact_by_id', 'ContactController@update_contact_by_id');

    $api->post('/get_news_by_rest_id', 'NewsController@get_news_by_rest_id');
    $api->post('/get_news_by_id', 'NewsController@get_news_by_id');
    $api->post('/update_news_by_id', 'NewsController@update_news_by_id');

    $api->post('/get_email_marketing_list', 'EmailMarketingController@get_email_marketing_list');
    $api->post('/send_email_marketing_list', 'EmailMarketingController@send_email_marketing_list');
    $api->post('/delete_email_marketing_list', 'EmailMarketingController@delete_email_marketing_list');
    $api->post('/get_email_marketing_detail', 'EmailMarketingController@get_email_marketing_detail');

    $api->post('/send_email_accept_order','EmailOrderController@send_email_accept_order');

    $api->post('/get_restaurant_reviews', 'RestaurantReviewsController@get_restaurant_reviews');
    $api->post('/update_restaurant_review', 'RestaurantReviewsController@update_restaurant_review');
    $api->post('/delete_restaurant_review', 'RestaurantReviewsController@delete_restaurant_review');
    $api->post('/get_items_reviews', 'RestaurantReviewsController@get_items_reviews');

    $api->post('/get_turnover_planing', 'TurnOverPlaningController@get_turnover_planing');
    $api->post('/get_turnover', 'TurnOverPlaningController@get_turnover');
    $api->post('/update_turnover_planing', 'TurnOverPlaningController@update_turnover_planing');

    $api->post('/get_subscribe', 'SubscribeController@get_subscribe');
    $api->post('/delete_subscribe', 'SubscribeController@delete_subscribe');

    $api->post('/get_search_items', 'SearchController@get_search_items');
    $api->post('/delete_search_items', 'SearchController@delete_search_items');


    $api->post('/get_order_setting_by_rest_id', 'RestaurantController@get_order_setting_by_rest_id');
    $api->post('/update_order_setting_by_rest_id', 'RestaurantController@update_order_setting_by_rest_id');
    $api->post('/get_contents_by_rest_id', 'RestaurantController@get_contents_by_rest_id');
    $api->post('/update_contents_by_rest_id', 'RestaurantController@update_contents_by_rest_id');
    $api->post('/get_restaurant_detail_by_id', 'RestaurantController@get_restaurant_detail_by_id');
    $api->post('/send_feedback', 'RestaurantController@send_feedback');

    $api->post('/authenticate/user', 'AuthController@getAuthenticatedUser');

    $api->post('/token', 'AuthController@getToken')->middleware('jwt.refresh');
    // $api->post('/authenticate/user', 'AuthController@getAuthenticatedUser');
    $api->post('/upload_file', 'UploadController@uploadFileImage');
    $api->post('/delete_file', 'UploadController@delete_file');

    $api->post('/get_code_by_cd_group', 'ComCodeController@get_code_by_cd_group');
});
