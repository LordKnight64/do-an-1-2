appModule.controller('ADM0300Ctrl', ['$scope', '$log', 'commonService', '$window', '$rootScope', '$alert', function($scope, $log, commonService, $window, $rootScope, $alert) {
    $scope.m = {};
    // start serve
    /**
     * [initDispData description]
     * @param  [type] params, fnCallback [description]
     * @return [type]  [description]
     */
    $scope.initDispData = function(params, fnCallback) {
        commonService.doPost('/PhanQuyen/initDispData', params, {
            success: function(result) {
                $scope.m = angular.copy(result);
                $scope.m.roles = $scope.m.roles.map(function(item) {
                    return {
                        id: item.id.toString(),
                        display_name: item.display_name
                    };
                });
                console.log('-----object');
                (fnCallback || angular.noop)(result);
            }
        });
    };
    /**
     * [saveRole description]
     * @param  [type] params [description]
     * @return [type]  [description]
     */
    $scope.saveRole = function(params, fnCallback) {
        commonService.doPost('/PhanQuyen/saveRole', params, {
            success: function(result) {
                (fnCallback || angular.noop)(result);
            }
        });
    };
    /**
     * [getDataByRoleId description]
     * @param  [type] params, fnCallback [description]
     * @return [type] body [description]
     */
    $scope.getDataByRoleId = function(params, fnCallback) {
        commonService.doPost('/PhanQuyen/getDataByRoleId', params, {
            success: function(result) {
                $scope.m.screens = angular.copy(result.screens);
                $scope.m.sports = angular.copy(result.sports);
                (fnCallback || angular.noop)(result);
            }
        });
    };
    // end serve
    // client start
    /**
     * [init description]
     * @return {[type]} [description]
     */
    $scope.init = function() {
        $scope.initDispData({}, function(result) {
            // selected role default
            $scope.m.roleSelected = angular.copy($scope.m.roles[0].id);
            $scope.m.roleSelectedPrevious = angular.copy($scope.m.roles[0].id);
            // process screen list
            $scope.processScreen();
        });
    };
    /**
     * [processScreen description]
     * @param  [type]  [description]
     * @return [type] [description]
     */
    $scope.processScreen = function() {
        var newList = [];
        var n = {};
        $scope.m.screens.forEach(function(item) {
            if (!n[item.grp_screen_cd]) {
                n[item.grp_screen_cd] = true;
                item.childList = [];
                newList.push(item);
            }
        });
        for (var i = $scope.m.screens.length - 1; i >= 0; i--) {
            var itemFor = $scope.m.screens[i];
            for (var j = newList.length - 1; j >= 0; j--) {
                var itemJ = newList[j];
                if (itemFor.grp_screen_cd == itemJ.grp_screen_cd) {
                    newList[j].childList.push(itemFor);
                    $scope.m.screens.splice(i, 1);
                    break;
                }
            }
        }
        $scope.m.newScreen = newList;
    };
    /**
     * [changeViewAll description]
     * @param  [type] item [description]
     * @return [type] [description]
     */
    $scope.changeViewAll = function(item) {
        for (var i = item.childList.length - 1; i >= 0; i--) {
            var itemFor = item.childList[i];
            itemFor.view_flg = item.viewFlgAll;
            if (item.viewFlgAll == '0') {
                itemFor.edit_flg = item.viewFlgAll;
                itemFor.del_flg = item.viewFlgAll;
            }
        }
        if (item.viewFlgAll == '0') {
            item.editFlgAll = '0';
            item.delFlgAll = '0';
        }
        $scope.m.isChange = true;
    };
    /**
     * [changeEditAll description]
     * @param  [type] item [description]
     * @return [type]  [description]
     */
    $scope.changeEditAll = function(item) {
        var allView = true;
        for (var i = item.childList.length - 1; i >= 0; i--) {
            var itemFor = item.childList[i];
            if(itemFor.can_edit_flg == '1'){
	            itemFor.edit_flg = item.editFlgAll;

	            if( item.editFlgAll == '1') {
	                itemFor.view_flg = '1';
	            }
            }

            if(itemFor.view_flg != '1') {
                allView = false; 
            }
        }
        if( allView == true) {
            item.viewFlgAll = '1';
        }
        // if (item.editFlgAll == '1') {
        //     item.viewFlgAll = '1';
        // }
        $scope.m.isChange = true;
    };
    /**
     * [changeDelAll description]
     * @param  [type] item [description]
     * @return [type] [description]
     */
    $scope.changeDelAll = function(item) {
        var allView = true;
        for (var i = item.childList.length - 1; i >= 0; i--) {
            var itemFor = item.childList[i];
            if(itemFor.can_del_flg == '1'){
            	itemFor.del_flg = item.delFlgAll;

                if( item.delFlgAll == '1') {
                    itemFor.view_flg = '1';
                }
            }
            if(itemFor.view_flg != '1') {
                allView = false; 
            }
        }
        if( allView == true) {
            item.viewFlgAll = '1';
        }
        $scope.m.isChange = true;
    };
    /**
     * [changeView description]
     * @param  [type] child, parent [description]
     * @return [type] [description]
     */
    $scope.changeView = function(child, parent) {
        if (child.view_flg == 0) {
            child.edit_flg = '0';
            parent.editFlgAll = '0';
        }
        var isValid = parent.childList.some(function(item) {
            return (angular.isUndefined(item.view_flg) || item.view_flg === '0');
        }); // isValid is true
        if (!isValid) {
            parent.viewFlgAll = '1';
        } else {
            parent.viewFlgAll = '0';
        }
        $scope.m.isChange = true;
    };
    /**
     * [changeEdit description]
     * @param  [type] child, parent [description]
     * @return [type] [description]
     */
    $scope.changeEdit = function(child, parent) {
        if (child.edit_flg == 1) {
            child.view_flg = '1';
        }
        var isValid = parent.childList.some(function(item) {
            return (angular.isUndefined(item.edit_flg) || item.edit_flg === '0');
        }); // isValid is true
        if (!isValid) {
            parent.editFlgAll = '1';
            parent.viewFlgAll = '1';
        } else {
            parent.editFlgAll = '0';
        }
        $scope.m.isChange = true;
    };
    /**
     * [changeDel description]
     * @param  [type] child, parent [description]
     * @return [type] [description]
     */
    $scope.changeDel = function(child, parent) {
        if (child.del_flg == 1) {
            child.view_flg = '1';
        }
        var isValid = parent.childList.some(function(item) {
            return (angular.isUndefined(item.del_flg) || item.del_flg === '0');
        }); // isValid is true
        if (!isValid) {
            parent.delFlgAll = '1';
            parent.viewFlgAll = '1';
        } else {
            parent.delFlgAll = '0';
        }
        $scope.m.isChange = true;
    };
    /**
     * [saveRole description]
     * @param  [type]  [description]
     * @return [type]  [description]
     */
    $scope.clickOnSaveRole = function(roleSelectedPrevious, fnCallback) {
        var roleSelected = angular.copy($scope.m.roleSelected);
        if (angular.isDefined(roleSelectedPrevious)) {
            roleSelected = roleSelectedPrevious;
        }
        var listRole = [];
        for (var i = $scope.m.newScreen.length - 1; i >= 0; i--) {
            var itemFor = $scope.m.newScreen[i];
            for (var j = itemFor.childList.length - 1; j >= 0; j--) {
                var itemJ = itemFor.childList[j];
                listRole.push({
                    role_id: roleSelected,
                    permission_id: itemJ.id,
                    view_flg: angular.isUndefined(itemJ.view_flg) || itemJ.view_flg == null ? '0' : itemJ.view_flg,
                    edit_flg: angular.isUndefined(itemJ.edit_flg) || itemJ.edit_flg == null ? '0' : itemJ.edit_flg,
                    del_flg: angular.isUndefined(itemJ.del_flg) || itemJ.del_flg == null ? '0' : itemJ.del_flg,
                })
            }
        }
        var sports = [];
        $scope.m.sports.forEach(function(item) {
            if (item.checked == 1) {
                sports.push({
                    sport_id: item.sport_id
                });
            }
        });
        var paramsSave = {
            role_id: roleSelected,
            sports: sports,
            listRole: listRole
        }
        $scope.saveRole(paramsSave, function(result) {
            commonService.showAlert(result.msg, 'success');
            $scope.m.isChange = false;
            (fnCallback || angular.noop)();
        });
    };
    /**
     * [changeRole description]
     * @param  [type]  [description]
     * @return [type]  [description]
     */
    $scope.changeRole = function(item) {
        // $scope.m.roleSelected = item.id;
        if ($scope.m.isChange == true) {
            $('.btn-modal-change-role').trigger('click');
        } else {
            $scope.afterChangeRole();
        }
    };
    /**
     * [afterChangeRole description]
     * @param  [type]  [description]
     * @return [type]  [description]
     */
    $scope.afterChangeRole = function() {
        var paramsSearch = {
            roleId: $scope.m.roleSelected
        };
        $scope.getDataByRoleId(paramsSearch, function(result) {
            $scope.processScreen();
            $scope.m.isChange = false;
            $scope.m.roleSelectedPrevious = angular.copy($scope.m.roleSelected);
        })
    };
    /**
     * [afterChangeRole description]
     * @param  [type]  [description]
     * @return [type]  [description]
     */
    $scope.acceptSave = function() {
        var modal = this;
        $scope.clickOnSaveRole($scope.m.roleSelectedPrevious, function() {
            $scope.afterChangeRole();
            modal.$hide();
        });
    };
    /**
     * [checkToCheckingSport description]
     * @param  [type] item [description]
     * @return [type]  [description]
     */
    $scope.checkToCheckingSport = function(item) {
        if (item.cont_text != null) {
            item.checked = '1';
        }
    };
    /**
     * [checkToCheckingAll description]
     * @param  [type] item [description]
     * @return [type]  [description]
     */
    $scope.checkToCheckingAll = function(item) {
        // checking viewFlgAll
        item.viewFlgAll = '1';
        var isValid = item.childList.some(function(itemChild) {
            return (itemChild.view_flg == 0 || itemChild.view_flg == null);
        }); // isValid is true
        if (isValid) {
            item.viewFlgAll = '0';
        }
        // checking editFlgAll
        item.editFlgAll = '1';
        var isValid = item.childList.some(function(itemChild) {
            return (itemChild.edit_flg == 0 || itemChild.edit_flg == null);
        }); // isValid is true
        if (isValid) {
            item.editFlgAll = '0';
        }

     // checking editFlgAll
        item.delFlgAll = '1';
        var isValid = item.childList.some(function(itemChild) {
            return (itemChild.del_flg == 0 || itemChild.del_flg == null);
        }); // isValid is true
        if (isValid) {
            item.delFlgAll = '0';
        }
    };
    /**
     * [changeSport description]
     * @param  [type]  [description]
     * @return [type]  [description]
     */
    $scope.changeSport = function() {
        $scope.m.isChange = true;
    };
    // client end
}]);