var map = null;
var markers = [];
var markersStore = [];
var locations = [];
var infoWindows = [];
var radius = 6.0;

function openMap(loc) {
  if(loc!=null)
  {
      setTimeout(function() {
          initAutocomplete(loc);
      }, 800);
  }
  else{
    if (map == null) {
        setTimeout(function() {
            initAutocomplete();
        }, 800);
    }
  }
}

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
function initAutocomplete(loc) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: -33.8688,
            lng: 151.2195
        },
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    if(loc!=null)
    {
      console.log(navigator.geolocation);
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var userPos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            map.setCenter(userPos);
            drawMarkerLocation(userPos);
            drawMarker(markers[0].getPosition());

  });
}
}




    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Draw control input radius
    drawInputControl();

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        // drawMarkerRestaurants(locations);
    });

    // [START region_getplaces]
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        drawMarkers(searchBox);
    });
}

function drawInputControl() {
    // Create a div to hold everything else
    var controlDiv = document.createElement('DIV');
    controlDiv.id = "radiusDiv";
    controlDiv.style = "width:180px"

    // Create an input field
    var controlInput = document.createElement('input');
    controlInput.id = "radiusInput";
    controlInput.name = "radiusInput";
    controlInput.value = radius;
    controlInput.type = "number";
    controlInput.style = "width:45px"


    // Create a label
    var controlLabel = document.createElement('label');
    controlLabel.innerHTML = 'Radius (km)';
    controlLabel.id = 'radiusLabel';
    controlLabel.setAttribute("for", "radiusInput");

    // Create a button to send the information
    var controlButton = document.createElement('input');
    controlButton.id = 'radiusButton';
    controlButton.type = "button";
    controlButton.value = "save";

    // Append everything to the wrapper div
    controlDiv.appendChild(controlLabel);
    controlDiv.appendChild(controlInput);
    controlDiv.appendChild(controlButton);

    var onClick = function() {
        radius = $("#radiusInput").val();
        drawMarkerRestaurants(locations);
    };
    google.maps.event.addDomListener(controlButton, 'click', onClick);
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);
}

/**
 * [drawMarkers description]
 * @param  {[type]} searchBox [description]
 * @return {[type]}           [description]
 */
function drawMarkers(searchBox) {
    //1: check length places
    var places = searchBox.getPlaces();
    if (places.length == 0) {
        return;
    }

    //2: Clean marker location
    cleanMarkersLocation();

    //3: draw marker location and get locaions for post get_restaurants_by_locations
    locations = [];
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
        //3.1: draw marker location
        var pos = place.geometry.location;
        drawMarkerLocation(pos);
        if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }

        //3.2: Get locaions for post get_restaurants_by_locations
        var location = getLocation(pos.lat(), pos.lng());
        locations.push(location);
    });

    //4: Sets the viewport to contain the given bounds.
    map.fitBounds(bounds);

    //5: draw marker restaurants
    drawMarkerRestaurants();
}

/**
 * [geocodePosition description]
 * @param  {[type]} pos [description]
 * @return {[type]}     [description]
 */
function drawMarker(pos) {
    //1: Clean marker location
    cleanMarkersLocation();

    //2: Get locaions for post get_restaurants_by_locations
    locations = [];
    var location = getLocation(pos.lat(), pos.lng());
    locations.push(location);

    //3: draw marker
    drawMarkerLocation(pos);
    drawMarkerRestaurants();
}

function drawMarkerLocation(pos) {
    var icon = {
        url: '',
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
    };

    // Create a marker for each place.
    var marker = new google.maps.Marker({
        map: map,
        position: pos,
        draggable: true
    });

    markers.push(marker);
    google.maps.event.addListener(marker, 'dragend', function() {
        drawMarker(marker.getPosition());
        console.log('đây nè');
    });
}

function drawMarkerRestaurants() {
    cleanMarkersRestaurants();
    cleanInfoWindows();

    for (var i = 0; i < locations.length; i++) {
        locations[i] = getLocation(locations[i].lat_val, locations[i].long_val);
    }

    var param = { 'locations': locations };
    console.log(location);
    serverServices.doPost('/get_restaurants_by_locations', param).then(
        function(okResult) {
            //1: User return request ok
            if (okResult.data.return_cd === 0) {

                //1.1: return if restaurantList not data
                if (!okResult.data.restaurantList || okResult.data.restaurantList.length === 0) {
                    return;
                }

                //1.2 Create data and loop
                var restaurantList = okResult.data.restaurantList;
                var index = 0;
                restaurantList.forEach(function(restaurants) {
                    var iconStore = {
                        url: "frontend/img/gplace-all-places-in-thailand-549000.png",
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(40, 40)
                    };

                    for (var i = 0; i < restaurants.length; i++) {
                        var lat_val = restaurants[i].lat_val;
                        var long_val = restaurants[i].long_val;
                        var positionStore = new google.maps.LatLng(lat_val, long_val);
                        markerStore = new google.maps.Marker({
                            map: map,
                            icon: iconStore,
                            position: positionStore,
                            anchorPoint: new google.maps.Point(0, -30)
                        });
                        markersStore.push(markerStore);

                        addContentInfoWindow(markerStore, restaurants[i]);
                    };
                });
            } else { //2: User return request fail

            }
        },
        function(errResult) {
            console.log('errResult', errResult);
        }
    );
}

/**
 * [cleanMarkersLocation description]
 * @return {[type]} [description]
 */
function cleanMarkersLocation() {
    // Clear out the old markers.
    markers.forEach(function(marker) {
        marker.setMap(null);
    });
    markers = [];
}

/**
 * [cleanMarkersRestaurants description]
 * @return {[type]} [description]
 */
function cleanMarkersRestaurants() {
    // Clear out the old markers store.
    markersStore.forEach(function(markerStore) {
        markerStore.setMap(null);
    });
    markersStore = [];
}

function cleanInfoWindows() {
    infoWindows.forEach(function(infoWindow) {
        if (infoWindow) {
            infoWindow.close();
        }
    });
    infoWindows = [];
}

/**
 * [getLocation get location for action get_restaurants_by_locations]
 * @param  {[type]} lat_val  [description]
 * @param  {[type]} long_val [description]
 * @return {[type]}          [description]
 */
function getLocation(lat_val, long_val) {
    var location = {};
    location.radius = radius;
    location.lat_val = lat_val;
    location.long_val = long_val;
    return location;
}

function addContentInfoWindow(markerStore, restaurant) {

    var infowindow = new google.maps.InfoWindow();
    var maxStar = 5;
    var star = restaurant.rating;
    var content = '<div id="infoWindow">' +
        '<div id="divLeft">' +
        '	<a href="/restaurantDetail?id=' + restaurant.id + '" target="_blank">' +
        '	<img id="infoImage" src="' + restaurant.logo + '"></a>' +
        '</div id="divImgae">' +
        '<div id="divRight">' +
        '	<a id="infoTitle" href="/restaurantDetail?id=' + restaurant.id + '" target="_blank">' + restaurant.name + '</a>' +
        '		<div id="divRate">' + restaurant.rating +
        '			<ol id="rate">';
    for (var i = 1; i <= maxStar; i++) {
        if (i <= star) {
            content += '<li class="cards-rating-star"></li>';
        } else if (i - star <= 0.5) {
            content += '<li class="cards-rating-star-half"></li>';
        } else {
            content += '<li class="cards-rating-star-empty"></li>';
        }
    }
    content += '			</ol>' +
        '		</div>' +
        '	<div>' + restaurant.mobile + '</div>' +
        '	<div>' + restaurant.address + '</div>' +
        '</div>' +
        '</div>';

    google.maps.event.addListener(markerStore, 'click', function() {
        cleanInfoWindows();
        infowindow.setContent(content);
        infoWindows.push(infowindow);
        infowindow.open(map, this);
    });
}
