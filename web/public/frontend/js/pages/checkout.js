var itemList = [];
var checkAcceptFlg = null;
var shippDetail = null;
var restaurantCheckout = null;
var valueSelect = null;
$(document).ready(function() {
    itemList = cookieService.getCookie("ShoppingCart");
    // shippDetail = cookieService.getCookie("ShippDetail");
    var localStorage = window.localStorage;
    if (localStorage.getItem('ShippingDetail') != undefined || localStorage.getItem('ShippingDetail') != null) {
        shippDetail = JSON.parse(localStorage.getItem('ShippingDetail'));
    }
    if (shippDetail != null) {
        document.getElementById("inputName").value = shippDetail.name;
        document.getElementById("inputEmail").value = shippDetail.email;
        document.getElementById("inputPhone").value = shippDetail.phone;
        document.getElementById("inputAddress").innerHTML = shippDetail.address;
        document.getElementById("orderComment").innerHTML = shippDetail.content;
        document.getElementById("delivery_type").value = shippDetail.delivery_type;
    }
    restaurantCheckout = cookieService.getCookie("restaurantCheckout");
    if (restaurantCheckout != null) {
        document.getElementById("promoCode").value = restaurantCheckout.promote_code;
    }
    addRow();
    $.validate({
        form: '#checkout',
        modules: '',
        onError: function($form) {

        },
        onSuccess: function($form) {
            setTimeout(function() {
                var items = [];
                if (itemList != null && itemList.length > 0) {
                    itemList.forEach(function(element) {
                        var item = {
                            'item_id': element.item_id.toString(),
                            'number': element.number.toString(),
                            "quantity": element.quantity.toString()
                        }
                        items.push(item);
                        // document.getElementById("items").value = items;
                    }, this);
                }
                checkvalidateServer(items, $form);
            }, 800);
            return false;
        },
        onModulesLoaded: function() {

        }
    });

    $('#modal-user-login').on('hidden.bs.modal', function(e) {
        $('#msg-error-login').css('display', 'none');
    });
    if (document.getElementById('checkAccept').checked == true) {
        checkAcceptFlg = 1;
    }
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#datetimepicker1').datetimepicker({
        daysOfWeekDisabled: [0, 6],
        minDate: moment(),
    });
});

function checkvalidateServer(items, $form) {

    showAndClear("#err_name", null);
    showAndClear("#err_email", null);
    showAndClear("#err_address", null);
    showAndClear("#err_phone", null);
    showAndClear("#err_checkAccept", null);
    showAndClear("#err_promoCode", null);
    showAndClear("#err_deliveryType", null);
    showAndClear("#err_deliveryTs", null);
    var promoCode = null;
    if ($form.find('input[name="inputPromoCode"]').val() != "" && $form.find('input[name="inputPromoCode"]').val() != null) {
        promoCode = $form.find('input[name="inputPromoCode"]').val();
    }
    // var deliveryTime = moment($('#datetimepicker1').data('date'), 'D/M/YYYY hh:mm:ss')._d;
    var dateTime = null;
    if ($('#datetimepicker1').data('date') != null && $('#datetimepicker1').data('date') != '') {
        dateTime = new Date($('#datetimepicker1').data('date'));
        dateTime = moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
    }
    var param = {
        'name': $form.find('input[name="name"]').val(),
        'email': $form.find('input[name="email"]').val(),
        'phone': $form.find('input[name="phone"]').val(),
        'content': $form.find('textarea[name="orderComment"]').val(),
        'address': $form.find('textarea[name="address"]').val(),
        'checkAccept': checkAcceptFlg,
        'promo_code': promoCode,
        'delivery_ts': dateTime,
        'delivery_type': document.getElementById("delivery_type").value,
        'promo_codeRes': $form.find('input[name="promoCode"]').val(),
        'items': items
    };
    var skillsSelect = document.getElementById("delivery_type");
    var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
    serverServices.doPost('/postCheckout', param).then(
        function(okResult) {
            if (okResult.data.return_cd === 0) {
                // var keyName = "ShippDetail";
                // var expireDay = 1;
                // cookieService.updateCookie(keyName, param, expireDay);
                var param1 = {
                    'name': $form.find('input[name="name"]').val(),
                    'email': $form.find('input[name="email"]').val(),
                    'phone': $form.find('input[name="phone"]').val(),
                    'content': $form.find('textarea[name="orderComment"]').val(),
                    'promo_code': $form.find('input[name="inputPromoCode"]').val(),
                    'delivery_ts': dateTime,
                    'delivery_type': document.getElementById("delivery_type").value,
                    'delivery_type_name': selectedText,
                    'address': $form.find('textarea[name="address"]').val()
                };
                var localStorage = window.localStorage;
                localStorage.setItem('ShippingDetail', JSON.stringify(param1));
                // ShoppingCart.setShippingDetail(param);
                swal("Sent contact success!", "Thank you for contacting.<br> We will respond as soon as possible.", "success");
                var url = ENVIRONMENT.DOMAIN_WWW + "/reviewCheckout";
                window.location.href = url;
            } else {
                //validate
                // 
                var error = okResult.data.errors;
                if (okResult.data.code == 422) {
                    if (error.name) {
                        showAndClear("#err_name", error.name);
                    }
                    if (error.email) {
                        showAndClear("#err_email", error.email);
                    }
                    if (error.address) {
                        showAndClear("#err_address", error.address);
                    }
                    if (error.phone) {
                        showAndClear("#err_phone", error.phone);
                    }
                    if (error.checkAccept) {
                        showAndClear("#err_checkAccept", error.checkAccept);
                    }
                    if (error.promo_code) {
                        showAndClear("#err_promoCode", error.promo_code);
                    }
                    if (error.delivery_type) {
                        showAndClear("#err_deliveryType", error.delivery_type);
                    }
                    if (error.delivery_ts) {
                        showAndClear("#err_deliveryTs", error.delivery_ts);
                    }
                    if (error.quantity) {
                        swal(
                            'Error input quantity!',
                            'error'
                        );
                    }
                } else {
                    swal(
                        'Send data Fail!',
                        error[0],
                        'error'
                    );
                }

            }
        },
        function(errResult) {
            swal("Sent contact error!", "Request failed.<br> Please contact us again later.", "error");
            console.log('errResult', errResult);
        }
    );
}

function showAndClear(id, data) {
    if (data != null) {
        $(id).text(data);
    } else
        $(id).text('');
}

function addRow() {
    ShoppingCartUI.loadCookieForcheckout();
}

function changeValue(id, number) {
    var valueQuanliti = $('#quantity' + id + number).val().trim();
    itemList.forEach(function(element) {
        if (element.item_id == id && element.number == number) {
            element.quantity = valueQuanliti;
            ShoppingCart.setShoppingCartItem(element, restaurantCheckout);
        }
    }, this);
    addRow();
}

function CheckAccept(event) {

    var valuecheck = event.target.checked;
    if (valuecheck == true) {
        checkAcceptFlg = '1';
        showAndClear("#err_checkAccept", null);
    } else {
        checkAcceptFlg = null;
    }
}

function deleteItem(id, number) {
    // ShoppingCart.doSetShoppingCartItem();

    swal({
        title: 'Are you sure?',
        text: "Are you delete this item in Shopping Cart?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete!'
    }).then(function() {
        var i = 0;
        itemList.forEach(function(element) {
            if (element.item_id == id && element.number == number) {
                itemList.splice(i, 1);
                ShoppingCart.deleteShoppingCartItem(element.item_id, element.number);
                return;
            }
            i++;
        }, this);
        addRow();
    });

}

function deleteOption(id, number, optionId, optionValueId) {

    itemList.forEach(function(element) {
        if (element.item_id == id && element.number == number) {
            var option_items = element.option_items;
            var i = 0;
            option_items.forEach(function(elementItem) {
                if (elementItem.option_id == optionId && elementItem.option_value_id == optionValueId) {
                    element.price = parseInt(element.price) - parseInt(elementItem.add_price);
                    option_items.splice(i, 1);
                    ShoppingCart.setShoppingCartItem(element, restaurantCheckout);
                    return;
                }
                i++;
            }, this);
        }
    }, this);

    // var keyName = "ShoppingCart";
    // var expireDay = 1;
    // cookieService.updateCookie(keyName, itemList, expireDay);
    addRow();
}