/**
 * [order Description]
 * Function to use:
 * - setShoppingCartItemInOrder(item, restaurant, urlRedirect): use to add order to shopping cart with restaurant
 *
 * @author Dang Nhat Anh
 */

function clickLoading() {

}

testLoading();

function testLoading() {
    $('.overlay_loading').addClass('on_overlay_loading');
    setTimeout(function() {
        $('.overlay_loading').removeClass('on_overlay_loading');
    }, 1000);
}
/**
 * [setShoppingCartItemInOrder Add order to shopping cart with restaurant]
 * @param {[type]} item        [description]
 * @param {[type]} restaurant  [description]
 * @param {[type]} urlRedirect [url if redirect]
 */
function setShoppingCartItemInOrder(item, restaurant, urlRedirect) {

    var orderDetail = {};
    orderDetail['item_id'] = item.id;
    orderDetail['name'] = item.name;
    orderDetail['price'] = item.price;
    orderDetail['thumb'] = item.thumb;
    orderDetail['quantity'] = 1;
    orderDetail['notes'] = "";
    orderDetail['number'] = 0;

    var option_items = [];
    orderDetail['option_items'] = option_items;
    var restaurantCokie = null;
    if (restaurant != null) {
        restaurantCokie = {
            "id": restaurant.id,
            "name": restaurant.name,
            "address": restaurant.address,
            "promo_discount": restaurant.promo_discount,
            "promote_code": restaurant.promote_code,
            "delivery_charge": restaurant.delivery_charge,
            "default_currency": restaurant.default_currency,
        }
    }

    ShoppingCart.setShoppingCartItem(orderDetail, restaurantCokie, urlRedirect);
}