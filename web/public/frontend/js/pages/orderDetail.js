/**
 * [orderDetail Description]
 * Function to use:
 * - checkOption(id): event checked
 * - loadCookie(): use to load detail order from shopping cart
 * - setShoppingCartItem(item, restaurant, urlRedirect): use to add order to shopping cart with restaurant
 * - doContinue(): use to click button continue
 * - doCheckout(): use to click button checkout
 *
 * @author Dang Nhat Anh
 */

var restarantCheckoutKey = "restaurantCheckout";
var defaultCurrency = "vnđ"
$(document).ready(function() {
  // Load detail order from shopping cart when loaded
  loadCookie();
  checkReNew();
  // Event onChange quantity
  $('#input-quantity').on("change", function() {
    var quantity = parseInt($('#input-quantity').val());
    var price = parseInt($('#priceAfter').val());
    var total_price = quantity * price;
    $('#total-price').text(new Intl.NumberFormat('vi-VN').format(total_price) + " " + defaultCurrency);
  });

});

//################################## xoa trong tuong lai ###############
function showCookie() {
  var decodedCookie = decodeURIComponent(document.cookie);
  alert(decodedCookie);

  var aa = cookieService.getCookie("test");
  console.log(aa);
}

/**
 * [deleteShoppingCart description]
 * @return {[type]} [description]
 */
function deleteShoppingCart() {
  ShoppingCart.deleteShoppingCart();
}
//====================================================

/**
 * [checkOption change checked radio ]
 * @param  {[type]} id [id radio]
 * @return {[type]}    [description]
 */
function checkOption(id) {
  if ($('.' + id).find('input[type=radio]').prop('checked') == true) {
    $('.' + id).find('input[type=radio]').prop('checked', false);
  } else {
    $('.' + id).find('input[type=radio]').prop('checked', true);
  }
  var quantity = parseInt($('#input-quantity').val());

  var price = parseInt($('#price').val());
  // var DecimalSeparator = Number("1.2").toLocaleString().substr(1, 1);

  // var AmountWithCommas = price.toLocaleString();
  // var arParts = String(AmountWithCommas).split(DecimalSeparator);
  // var intPart = arParts[0];
  // var decPart = (arParts.length > 1 ? arParts[1] : '');
  // decPart = (decPart + '00').substr(0, 2);

  // $('#price').text(price);
  $('input[type=radio]:checked').each(function(element) {
    var addPrice = $(this).parent().find('.option-value-add_price').val();
    price = price + parseInt(addPrice);
  });
  $('#priceDisplay').text(new Intl.NumberFormat('vi-VN').format(price) + " " + defaultCurrency);
  $('#priceAfter').val(price);
  var total_price = quantity * price;
  $('#total-price').text(new Intl.NumberFormat('vi-VN').format(total_price) + " " + defaultCurrency);
}

/**
 * [Load Checker option value id for Renew order]
 */
function checkReNew()
{
    var checker = document.getElementsByClassName("checker");
    
    for (var i = 0; i < checker.length; i++) 
    {
        checkOption(checker[i].value);
    }
    
}

/**
 * [loadCookie load detail order from shopping cart]
 * @return {[type]} [description]
 */
function loadCookie() {
  var restaurant_id = parseInt($('#restaurant_id').val());
  var restaurant = cookieService.getCookie(restarantCheckoutKey);
  if (restaurant != null) {
    defaultCurrency = restaurant.default_currency;
  }

  if (restaurant != null && restaurant.id === restaurant_id) {
    var item_id = parseInt($('#item_id').val());
    var number = parseInt($('#number').val());
    var item = ShoppingCart.getShoppingCartItem(item_id, number);

    if (item != null) {
      var total_price = item.price * item.quantity;
      $('#priceDisplay').text(new Intl.NumberFormat('vi-VN').format(item.price) + " " + defaultCurrency);
      $('#priceAfter').val(item.price);
      $('#input-quantity').val(item.quantity);
      $('#notes').val(item.notes);
      $('#total-price').text(new Intl.NumberFormat('vi-VN').format(total_price) + " " + defaultCurrency);

      var option_items = item['option_items'];
      $('.menu-details ul li .menu-list-item').each(function(element) {
        var option_id = parseInt($(this).find('.option_id').val());
        var option_value_id = parseInt($(this).find('.option_value_id').val());

        if (option_items != null) {
          for (var i = 0; i < option_items.length; i++) {
            if (option_id === option_items[i]['option_id'] 
                && option_value_id === option_items[i]['option_value_id']) {
              $(this).find('input[type=radio]').prop('checked', true)
            }
          }
        }
      });
    }
  }
}

/**
 * [setShoppingCartItem Add order to shopping cart with restaurant]
 * @param {[type]} item        [description]
 * @param {[type]} restaurant  [description]
 * @param {[type]} urlRedirect [url if redirect]
 */
function setShoppingCartItem(item, restaurant, urlRedirect) {

  var orderDetail = {};
  orderDetail['item_id'] = item.id;
  orderDetail['name'] = item.name;
  orderDetail['price'] = $('#priceAfter').val();
  orderDetail['thumb'] = item.thumb;
  orderDetail['quantity'] = parseInt($('#input-quantity').val());
  orderDetail['notes'] = $('#notes').val();
  orderDetail['number'] = parseInt($('#number').val());

  var option_items = [];
  $('input[type=radio]:checked').each(function(element) {
    var option_item = {};
    option_item['option_id'] = parseInt($(this).parent().find('.option_id').val());
    option_item['option_name'] = $(this).parent().find('.option_name').val();
    option_item['option_value_id'] = parseInt($(this).parent().find('.option_value_id').val());
    option_item['option_value_name'] = $(this).parent().find('.option_value_name').val();
    option_item['add_price'] = $(this).parent().find('.option-value-add_price').val();
    option_items.push(option_item);
  });
  orderDetail['option_items'] = option_items;
  var restaurantCokie = null;
  if (restaurant != null) {
    restaurantCokie = {
      "id": restaurant.id,
      "name": restaurant.name,
      "address": restaurant.address,
      "promo_discount": restaurant.promo_discount,
      "promote_code": restaurant.promote_code,
      "delivery_charge": restaurant.delivery_charge,
      "default_currency": restaurant.default_currency,
    }
  }
  ShoppingCart.setShoppingCartItem(orderDetail, restaurantCokie, urlRedirect);
}

/**
 * [doContinue event button continue]
 * @param  {[type]} item       [description]
 * @param  {[type]} restaurant [description]
 * @return {[type]}            [description]
 */
function doContinue(item, restaurant) {
  var urlRedirect = ENVIRONMENT.DOMAIN_WWW + "/order?restaurant_id=" + restaurant.id;
  setShoppingCartItem(item, restaurant, urlRedirect);
}

/**
 * [doCheckout event button checkout]
 * @param  {[type]} item       [description]
 * @param  {[type]} restaurant [description]
 * @return {[type]}            [description]
 */
function doCheckout(item, restaurant) {
  var urlRedirect = ENVIRONMENT.DOMAIN_WWW + "/checkout";
  setShoppingCartItem(item, restaurant, urlRedirect);
}
//====================================================

/**
 * REVIEW
 **/

var num = 0;

function addReview() {
  var userCheck = $('#userIdLogin').val();
  if (userCheck == "" || userCheck == null)
  swal(
    'Bạn vẫn chưa đăng nhập?',
    '',
    'question'
  ).then(function(){
       $("#modal-user-login").modal();
  });


  $('#myTab a[href="#review"]').tab('show');
  $('html, body').animate({
    scrollTop: $("#addReviewForm").offset().top
  }, 500);
}

function star(num) {
  var userCheck = $('#userIdLogin').val();
  if (userCheck == "" || userCheck == null){
  swal(
    'Bạn vẫn chưa đăng nhập',
    '',
    'question'
  ).then(function(){
       $("#modal-user-login").modal();
  });
  }
  else{
    $('#guilde-star').text("Đánh giá: "+num + " Sao");
  for (var i = 1; i <= 5; i++) {
    $('#' + i).addClass('fa-star-o');
    $('#' + i).removeClass('fa-star');
  }
  for (var i = num; i >= 1; i--) {
    $('#' + i).removeClass('fa-star-o');
    $('#' + i).addClass('fa-star');
  }
  this.num = num;
   $('#sendReview').attr("disabled", false);
   }
}

function sendReview() {
  var userCheck = $('#userIdLogin').val();
  if (userCheck != "" && userCheck != null) {
    var title = $('#title_review').val();
    var content = $('#content_review').val();
    console.log('rate:' + num);
    console.log('title:' + title);
    console.log('content:' + content);
    var param = {
      "title": title,
      "rating": num,
      "content": content,
      "restaurant_id" : $('#restaurant_id').val(),
      "item_id" : $('#item_id').val()
    };
    serverServices.doPost('/sendReview', param).then(function(okSuccess) {
      console.log(okSuccess);
      if (okSuccess.data == "201") {
        swal('Review success!', '', 'success').then(function(){
            location.reload();
        });
      } else {
        swal(
          'Bạn vẫn chưa đăng nhập?',
          '',
          'question'
        ).then(function(){
             $("#modal-user-login").modal();
        });

      }
    });
  } else $("#modal-user-login").modal();
}
