/**
 * [ShoppingCart all function support for shopping cart]
 *
 * @public
 * getShoppingCartItem(itemId, number): Get item in shopping cart by itemId and number
 * setShoppingCartItem(item, restaurant, urlRedirect): Add item to shopping cart with param(item, restaurant, urlRedirect)
 * deleteShoppingCartItem(itemId, number): Delete item in shopping cart by itemId and number
 * getShoppingCart(): Get all item in shopping cart
 * deleteShoppingCart(): Delete all item in shopping cart and restaurant saved in cookie
 * @private
 * doSetShoppingCartItem(item, restaurant): Support for function setShoppingCartItem
 *
 * @author Dang Nhat Anh
 */

var restarantCheckoutKey = "restaurantCheckout";
var shoppingKey = "ShoppingCart";
var expireDay = 1;

var ShoppingCart = {
    getShoppingCartItem: function(itemId, number) {
        var listItem = this.getShoppingCart();

        if (!listItem) {
            return null;
        }

        for (var i = 0; i < listItem.length; i++) {
            if (listItem[i].item_id === itemId &&
                listItem[i].number === number) {
                return listItem[i];
            }
        }

        return null;
    },

    setShoppingCartItem: function(item, restaurant, urlRedirect) {

        var restaurantCookie = cookieService.getCookie(restarantCheckoutKey);

        if (restaurantCookie != null && restaurantCookie.id !== restaurant.id) {

            swal({
                title: 'Are you sure?',
                text: "You have Shopping Cart of other restaurant. Are you delete the Shopping Cart to continue?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete old ShoppingCart!'
            }).then(function() {
                ShoppingCart.deleteShoppingCart();
                ShoppingCart.doSetShoppingCartItem(item, restaurant);

                swal("Add ShoppingCart success", "You add ShoppingCart success!!", "success").then(function() {
                    // Redirect if url exist
                    if (urlRedirect != null && urlRedirect.length !== 0) {
                        window.location.href = urlRedirect;
                    }
                });
            });

        } else {
            this.doSetShoppingCartItem(item, restaurant);

            swal("Add ShoppingCart success", "You add ShoppingCart success!!", "success").then(function() {
                // Redirect if url exist
                if (urlRedirect != null && urlRedirect.length !== 0) {
                    window.location.href = urlRedirect;
                }
            });
        }
    },

    doSetShoppingCartItem: function(item, restaurant) {
        var listItem = this.getShoppingCart();
        var listItemTmp = [];
        var indexItem = 0;
        var flagUpdate = false;

        if (listItem === null) {
            listItem = [];
        }

        //Get list itemTmp with id == item.id
        //and update item detail from itemTmp == item.option_items
        for (var i = listItem.length - 1; i >= 0; i--) {
            var itemTmp = listItem[i];
            if (item.item_id === itemTmp.item_id) {
                indexItem = i;
                listItem.splice(i, 1);

                var flag_merge_detail = false;
                if (item.number === itemTmp.number) {
                    itemTmp = item;
                    flagUpdate = true;
                } else {
                    var option_items1 = item.option_items;
                    var option_items2 = itemTmp.option_items;

                    if (option_items1 == null && option_items2 == null) {
                        flag_merge_detail = true;
                    }

                    if (option_items1 != null &&
                        option_items2 != null &&
                        option_items1.length === option_items2.length) {
                        flag_merge_detail = true;
                        for (var oi = 0; oi < option_items1.length; oi++) {
                            if (option_items1[oi].option_id != option_items2[oi].option_id ||
                                option_items1[oi].option_value_id != option_items2[oi].option_value_id) {
                                flag_merge_detail = false;
                                break;
                            }
                        }
                    }
                }

                if (flag_merge_detail) {
                    // Merge itemTmp detail to item
                    item.quantity += itemTmp.quantity;
                    if (itemTmp.notes != null && itemTmp.notes.length !== 0) {
                        item.notes += "\n" + itemTmp.notes;
                    }
                } else {
                    listItemTmp.push(itemTmp);
                }
            }
        }

        // Push listItemTmp with id == item.id to listItem
        var index = 0;
        for (i = listItemTmp.length - 1; i >= 0; i--) {
            var tmp = listItemTmp[i];
            tmp.number = index + 1;
            listItem.splice(indexItem, 0, tmp);
            indexItem++;
            index++;
        }

        // Push item to listItem
        if (!flagUpdate) {
            item.number = index + 1;
            listItem.splice(indexItem, 0, item);
        }

        cookieService.setCookie(restarantCheckoutKey, restaurant, expireDay);
        cookieService.setCookie(shoppingKey, listItem, expireDay);
        ShoppingCartUI.loadCookieForShoppingCart();
    },

    deleteShoppingCartItem: function(itemId, number) {
        var listItem = this.getShoppingCart();
        if (listItem) {
            for (var i = 0; i < listItem.length; i++) {
                if (listItem[i].item_id === itemId &&
                    listItem[i].number === number) {
                    listItem.splice(i, 1);
                    cookieService.updateCookie(shoppingKey, listItem, expireDay);
                    ShoppingCartUI.loadCookieForShoppingCart();
                    return true;
                }
            }
        }

        return false;
    },

    getShoppingCart: function() {
        var result = cookieService.getCookie(shoppingKey);
        return result;
    },

    deleteShoppingCart: function() {
        cookieService.deleteCookie(restarantCheckoutKey);
        cookieService.deleteCookie(shoppingKey);
        ShoppingCartUI.loadCookieForShoppingCart();
    },
};