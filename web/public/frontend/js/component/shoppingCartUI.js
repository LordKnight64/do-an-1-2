/**
 * [ShoppingCartUI Using for ShoppingCart control in head]
 *
 * @public
 * loadCookieForShoppingCart(): load items for ShoppingCart control in head

 * @author Dang Nhat Anh
 */
var restarantCheckoutKey = "restaurantCheckout";
var ShoppingCartUI = {
    loadCookieForShoppingCart: function() {
        var listItem = ShoppingCart.getShoppingCart();
        var restaurant = cookieService.getCookie(restarantCheckoutKey);

        // Set total_item for shopping cart
        var total_item = 0;
        var html = "";

        if (listItem != null && restaurant != null) {
            total_item = listItem.length;

            for (var i = 0; i < listItem.length; i++) {

                var item = listItem[i];
                html += "<li role='menuitem'>";
                html += "<div class='cart-item'>";
                html += "<a href='#' onclick='deleteShoppingCartItem(" + item.item_id + "," + item.number + ")'><i class='fa fa-times'></i></a>";
                html += "<a href='/orderDetail?restaurant_id=" + restaurant.id + "&item_id=" + item.item_id + "&number=" + item.number + "'>";
                html += "<img class='img-responsive img-rounded' src='" + item.thumb + "' alt='' />";
                html += "<span class='cart-title'>" + item.name + "</span></a>";
                html += "<div class='clearfix'></div>";
                html += "</div>";
                html += "</li>";
            }
        }

        html += "<li id='button-checkout-in-shoppingcart'>";
        html += "<div class='cart-item'>";
        html += "<a class='btn btn-danger' data-toggle='modal' href='/checkout'>Checkout</a>";
        html += "</div>";
        html += "</li>";

        $('#total_item').text(total_item);
        document.getElementById("list-item-in-shoppingcart").innerHTML = html;
        if (listItem == null || listItem.length == 0) {
            document.getElementById("btn-cart-md-order").disabled = true;
            document.getElementById("list-item-in-shoppingcart").disabled = true;
            document.getElementById("list-item-in-shoppingcart").style.display = "none";
            document.getElementById("btn-cart-md-order").style.display = "none";
            $('#total_item').text(total_item);
            return;
        } else {
            document.getElementById("btn-cart-md-order").disabled = false;
            document.getElementById("list-item-in-shoppingcart").disabled = false;
            document.getElementById("btn-cart-md-order").style.display = "block";
            $('#total_item').text(total_item);
            return;
        }
    },
    loadCookieForcheckout: function() {
        var itemList = ShoppingCart.getShoppingCart();
        var restaurant = cookieService.getCookie(restarantCheckoutKey);
        var div = document.createElement('div');
        var defaultCurrency = "vnđ"
        if (restaurant != null) {
            defaultCurrency = restaurant.default_currency;
        }
        div.className = 'row';
        var draw = '<table class="table"><thead><tr><th class="col-xs-4 col-md-4 col-sm-4">Name</th><th class="col-xs-2 col-md-2 col-sm-2">Quantity</th><th class="col-xs-2 col-md-2 col-sm-2">Price</th><th class="col-xs-2 col-md-2 col-sm-2">Action</th></tr></thead><tbody>';
        if (itemList != null && itemList.length > 0) {
            itemList.forEach(function(element) {
                var price = new Intl.NumberFormat('vi-VN').format(element.price) + " " + defaultCurrency;
                draw = draw + '<tr>';
                draw = draw + '<td>' + element.name + '</td>';
                draw = draw + '<td>' + '<input id="quantity' + element.item_id + element.number + '"name="quantity' + element.item_id + element.number + '" type="text" class="form-control" id="quantity" placeholder="quantity" value="' + element.quantity + '" onchange="changeValue(' + element.item_id + ',' + element.number + ')" data-validation="number | length" data-validation-length="max11">' + '</td>';
                draw = draw + '<td>' + price + '</td>';
                draw = draw + '<td >' + '<a id="edit" data-toggle="modal" class="btn btn-success fa-pencil fa" type="button" style="white-space: nowrap;float: center;"  data-toggle="tooltip" data-placement="top" title="Edit" href = "orderDetail?item_id=' + element.item_id + '&number=' + element.number + '&restaurant_id=' + restaurant.id + '"></a>&nbsp;&nbsp;' + '<a id="delete" data-toggle="modal" class="btn btn-danger fa fa-trash" type="button" style="white-space: nowrap; margin-top: 0px;" data-toggle="tooltip" data-placement="top" title="Delete" onclick = "deleteItem(' + element.item_id + ',' + element.number + ')"></a>' + '</td>';

                var option_items = element.option_items;
                if (option_items != null && option_items.length > 0) {
                    option_items.forEach(function(elementItem) {
                        var add_price = new Intl.NumberFormat('vi-VN').format(elementItem.add_price) + " " + defaultCurrency;
                        draw = draw + '<tr class="option">';
                        draw = draw + '<td>' + "&nbsp;&nbsp;&nbsp;&nbsp; + " + elementItem.option_name + " ( " + elementItem.option_value_name + " ) " + '</td>';
                        draw = draw + '<td>' + '</td>';
                        draw = draw + '<td>' + add_price + '</td>';
                        draw = draw + '<td>' + '<a id="delete" data-toggle="modal" class="btn btn-danger fa fa-trash" type="button" style="white-space: nowrap; margin-top: 0px;" data-toggle="tooltip" data-placement="top" title="Delete" onclick = "deleteOption(' + element.item_id + ',' + element.number + ',' + elementItem.option_id + ',' + elementItem.option_value_id + ')"></a>' + '</td>';
                        draw = draw + '</tr>';
                    }, this);
                }
                draw = draw + '</tr>';
            }, this);
        }
        draw = draw + '</tbody></table>';
        document.getElementById("content").innerHTML = draw;
    },
    loadCookieForreviewCheckout: function() {
        var itemList = ShoppingCart.getShoppingCart();
        var restaurant = cookieService.getCookie(restarantCheckoutKey);
        var div = document.createElement('div');
        var defaultCurrency = "vnđ"
        if (restaurant != null) {
            defaultCurrency = restaurant.default_currency;
        }
        div.className = 'row';
        var draw = '<table class="table"><thead><tr><th class="col-xs-6 col-md-6 col-sm-6">Name</th><th class="col-xs-3 col-md-3 col-sm-3">Quantity</th><th class="col-xs-3 col-md-3 col-sm-3">Price</th></tr></thead><tbody>';
        if (itemList != null && itemList.length > 0) {
            itemList.forEach(function(element) {
                var price = new Intl.NumberFormat('vi-VN').format(element.price) + " " + defaultCurrency;
                draw = draw + '<tr>';
                draw = draw + '<td>' + element.name + '</td>';
                draw = draw + '<td>' + element.quantity + '</td>';
                draw = draw + '<td>' + price + '</td>';
                var option_items = element.option_items;
                if (option_items != null && option_items.length > 0) {
                    option_items.forEach(function(elementItem) {
                        var add_price = new Intl.NumberFormat('vi-VN').format(elementItem.add_price) + " " + defaultCurrency;
                        draw = draw + '<tr class="option">';
                        draw = draw + '<td>' + "&nbsp;&nbsp;&nbsp;&nbsp; + " + elementItem.option_name + " ( " + elementItem.option_value_name + " ) " + '</td>';
                        draw = draw + '<td>' + '</td>';
                        draw = draw + '<td>' + add_price + '</td>';
                        draw = draw + '</tr>';
                    }, this);
                }
                draw = draw + '</tr>';
            }, this);
        }
        draw = draw + '</tbody></table>';
        document.getElementById("content").innerHTML = draw;
    }
};