"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
function routerTransition() {
    return slideToLeft();
}
exports.routerTransition = routerTransition;
function slideToLeft() {
    return core_1.trigger('routerTransition', [
        core_1.state('void', core_1.style({ position: 'fixed', width: '100%', 'padding-right': '60px' })),
        core_1.state('*', core_1.style({ position: 'static', width: '100%' })),
        core_1.transition(':enter', [
            core_1.style({ transform: 'translateX(100%)' }),
            core_1.animate('0.5s ease-in-out', core_1.style({ transform: 'translateX(0%)' }))
        ]),
        core_1.transition(':leave', [])
    ]);
}
function routerFade() {
    return fadeIn();
}
exports.routerFade = routerFade;
function fadeIn() {
    return core_1.trigger('routerFade', [
        core_1.state('void', core_1.style({ position: 'fixed', width: '100%', 'padding-right': '60px' })),
        core_1.state('*', core_1.style({ position: 'static', width: '100%' })),
        core_1.transition(':enter', [
            core_1.style({ opacity: 0 }),
            core_1.animate('0.5s ease-in-out', core_1.style({ opacity: 1 }))
        ]),
        core_1.transition(':leave', [])
    ]);
}
function routerGrow() {
    return GrowUp();
}
exports.routerGrow = routerGrow;
function GrowUp() {
    return core_1.trigger('routerGrow', [
        core_1.state('void', core_1.style({ position: 'fixed', width: '100%' })),
        core_1.state('*', core_1.style({ position: 'fixed', width: '100%' })),
        core_1.transition(':enter', [
            core_1.style({ transform: 'scale(0)' }),
            core_1.animate('0.5s ease-in-out', core_1.style({ transform: 'scale(1)' }))
        ]),
        core_1.transition(':leave', [])
    ]);
}

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9yb3V0ZXIuYW5pbWF0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUV6RTtJQUNFLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUN2QixDQUFDO0FBRkQsNENBRUM7QUFFRDtJQUVFLE1BQU0sQ0FBQyxjQUFPLENBQUMsa0JBQWtCLEVBQUU7UUFDakMsWUFBSyxDQUFDLE1BQU0sRUFBRSxZQUFLLENBQUMsRUFBQyxRQUFRLEVBQUMsT0FBTyxFQUFFLEtBQUssRUFBQyxNQUFNLEVBQUUsZUFBZSxFQUFHLE1BQU0sRUFBRSxDQUFDLENBQUU7UUFDbEYsWUFBSyxDQUFDLEdBQUcsRUFBRSxZQUFLLENBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxNQUFNLEVBQUUsQ0FBQyxDQUFFO1FBQ3RELGlCQUFVLENBQUMsUUFBUSxFQUFFO1lBQ25CLFlBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSxrQkFBa0IsRUFBQyxDQUFDO1lBQ3RDLGNBQU8sQ0FBQyxrQkFBa0IsRUFBRSxZQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUMsQ0FBQyxDQUFDO1NBQ2xFLENBQUM7UUFDRixpQkFBVSxDQUFDLFFBQVEsRUFBRSxFQUdwQixDQUFDO0tBQ0gsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUNEO0lBQ0UsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO0FBQ2xCLENBQUM7QUFGRCxnQ0FFQztBQUVEO0lBQ0UsTUFBTSxDQUFDLGNBQU8sQ0FBQyxZQUFZLEVBQUU7UUFDM0IsWUFBSyxDQUFDLE1BQU0sRUFBRSxZQUFLLENBQUMsRUFBQyxRQUFRLEVBQUMsT0FBTyxFQUFFLEtBQUssRUFBQyxNQUFNLEVBQUUsZUFBZSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUU7UUFDakYsWUFBSyxDQUFDLEdBQUcsRUFBRSxZQUFLLENBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxNQUFNLEVBQUMsQ0FBQyxDQUFFO1FBQ3JELGlCQUFVLENBQUMsUUFBUSxFQUFFO1lBQ25CLFlBQUssQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQztZQUNuQixjQUFPLENBQUMsa0JBQWtCLEVBQUUsWUFBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7U0FDakQsQ0FBQztRQUNGLGlCQUFVLENBQUMsUUFBUSxFQUFFLEVBR3BCLENBQUM7S0FDSCxDQUFDLENBQUM7QUFDTCxDQUFDO0FBQ0Q7SUFDRSxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7QUFDbEIsQ0FBQztBQUZELGdDQUVDO0FBQ0Q7SUFDRSxNQUFNLENBQUMsY0FBTyxDQUFDLFlBQVksRUFBRTtRQUMzQixZQUFLLENBQUMsTUFBTSxFQUFFLFlBQUssQ0FBQyxFQUFDLFFBQVEsRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFDLE1BQU0sRUFBRSxDQUFDLENBQUU7UUFDeEQsWUFBSyxDQUFDLEdBQUcsRUFBRSxZQUFLLENBQUMsRUFBQyxRQUFRLEVBQUMsT0FBTyxFQUFFLEtBQUssRUFBQyxNQUFNLEVBQUMsQ0FBQyxDQUFFO1FBQ3BELGlCQUFVLENBQUMsUUFBUSxFQUFFO1lBQ25CLFlBQUssQ0FBQyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUMsQ0FBQztZQUMvQixjQUFPLENBQUMsa0JBQWtCLEVBQUUsWUFBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQUM7U0FDNUQsQ0FBQztRQUNGLGlCQUFVLENBQUMsUUFBUSxFQUFFLEVBR3BCLENBQUM7S0FDSCxDQUFDLENBQUM7QUFDTCxDQUFDIiwiZmlsZSI6ImFwcC9yb3V0ZXIuYW5pbWF0aW9ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7dHJpZ2dlciwgc3RhdGUsIGFuaW1hdGUsIHN0eWxlLCB0cmFuc2l0aW9ufSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGZ1bmN0aW9uIHJvdXRlclRyYW5zaXRpb24oKSB7IFxuICByZXR1cm4gc2xpZGVUb0xlZnQoKTsgXG59XG5cbmZ1bmN0aW9uIHNsaWRlVG9MZWZ0KCkge1xuIFxuICByZXR1cm4gdHJpZ2dlcigncm91dGVyVHJhbnNpdGlvbicsIFtcbiAgICBzdGF0ZSgndm9pZCcsIHN0eWxlKHtwb3NpdGlvbjonZml4ZWQnLCB3aWR0aDonMTAwJScsICdwYWRkaW5nLXJpZ2h0JzogICc2MHB4JyB9KSApLFxuICAgIHN0YXRlKCcqJywgc3R5bGUoe3Bvc2l0aW9uOidzdGF0aWMnLCB3aWR0aDonMTAwJScgfSkgKSxcbiAgICB0cmFuc2l0aW9uKCc6ZW50ZXInLCBbICAvLyBiZWZvcmUgMi4xOiB0cmFuc2l0aW9uKCd2b2lkID0+IConLCBbXG4gICAgICBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgxMDAlKSd9KSxcbiAgICAgIGFuaW1hdGUoJzAuNXMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgwJSknfSkpXG4gICAgXSksXG4gICAgdHJhbnNpdGlvbignOmxlYXZlJywgWyAgLy8gYmVmb3JlIDIuMTogdHJhbnNpdGlvbignKiA9PiB2b2lkJywgW1xuICAgICAvLyBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgwJSknfSksXG4gICAgICAvL2FuaW1hdGUoJzAuNXMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgtMTAwJSknfSkpXG4gICAgXSlcbiAgXSk7XG59XG5leHBvcnQgZnVuY3Rpb24gcm91dGVyRmFkZSgpIHtcbiAgcmV0dXJuIGZhZGVJbigpO1xufVxuXG5mdW5jdGlvbiBmYWRlSW4oKSB7XG4gIHJldHVybiB0cmlnZ2VyKCdyb3V0ZXJGYWRlJywgW1xuICAgIHN0YXRlKCd2b2lkJywgc3R5bGUoe3Bvc2l0aW9uOidmaXhlZCcsIHdpZHRoOicxMDAlJywgJ3BhZGRpbmctcmlnaHQnOiAnNjBweCcgfSkgKSxcbiAgICBzdGF0ZSgnKicsIHN0eWxlKHtwb3NpdGlvbjonc3RhdGljJywgd2lkdGg6JzEwMCUnfSkgKSxcbiAgICB0cmFuc2l0aW9uKCc6ZW50ZXInLCBbICAvLyBiZWZvcmUgMi4xOiB0cmFuc2l0aW9uKCd2b2lkID0+IConLCBbXG4gICAgICBzdHlsZSh7b3BhY2l0eTogMH0pLFxuICAgICAgYW5pbWF0ZSgnMC41cyBlYXNlLWluLW91dCcsIHN0eWxlKHtvcGFjaXR5OiAxfSkpXG4gICAgXSksXG4gICAgdHJhbnNpdGlvbignOmxlYXZlJywgWyAgLy8gYmVmb3JlIDIuMTogdHJhbnNpdGlvbignKiA9PiB2b2lkJywgW1xuICAgICAvLyBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgwJSknfSksXG4gICAgICAvL2FuaW1hdGUoJzAuNXMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgtMTAwJSknfSkpXG4gICAgXSlcbiAgXSk7XG59XG5leHBvcnQgZnVuY3Rpb24gcm91dGVyR3JvdygpIHtcbiAgcmV0dXJuIEdyb3dVcCgpO1xufVxuZnVuY3Rpb24gR3Jvd1VwKCkge1xuICByZXR1cm4gdHJpZ2dlcigncm91dGVyR3JvdycsIFtcbiAgICBzdGF0ZSgndm9pZCcsIHN0eWxlKHtwb3NpdGlvbjonZml4ZWQnLCB3aWR0aDonMTAwJScgfSkgKSxcbiAgICBzdGF0ZSgnKicsIHN0eWxlKHtwb3NpdGlvbjonZml4ZWQnLCB3aWR0aDonMTAwJSd9KSApLFxuICAgIHRyYW5zaXRpb24oJzplbnRlcicsIFsgIC8vIGJlZm9yZSAyLjE6IHRyYW5zaXRpb24oJ3ZvaWQgPT4gKicsIFtcbiAgICAgIHN0eWxlKHsgdHJhbnNmb3JtOiAnc2NhbGUoMCknfSksXG4gICAgICBhbmltYXRlKCcwLjVzIGVhc2UtaW4tb3V0Jywgc3R5bGUoe3RyYW5zZm9ybTogJ3NjYWxlKDEpJ30pKVxuICAgIF0pLFxuICAgIHRyYW5zaXRpb24oJzpsZWF2ZScsIFsgIC8vIGJlZm9yZSAyLjE6IHRyYW5zaXRpb24oJyogPT4gdm9pZCcsIFtcbiAgICAgLy8gc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMCUpJ30pLFxuICAgICAgLy9hbmltYXRlKCcwLjVzIGVhc2UtaW4tb3V0Jywgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLTEwMCUpJ30pKVxuICAgIF0pXG4gIF0pO1xufSJdfQ==
