"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var restaurant_service_1 = require("app/services/restaurant.service");
var notification_service_1 = require("app/services/notification.service");
var user_service_1 = require("app/services/user.service");
var ContentsComponent = (function () {
    function ContentsComponent(restServ, fb, userServ, notifyServ) {
        this.restServ = restServ;
        this.fb = fb;
        this.userServ = userServ;
        this.notifyServ = notifyServ;
        this.aboutUs = null;
        this.privatePolicy = null;
        this.termOfUse = null;
        this.formErrors = {
            'aboutUs': [],
            'privatePolicy': [],
            'termOfUse': []
        };
        this.validationMessages = {
            'aboutUs': {
                'maxlength': 'The about us may not be greater than 1024 characters.'
            },
            'privatePolicy': {
                'maxlength': 'The private policy may not be greater than 1024 characters.'
            },
            'termOfUse': {
                'maxlength': 'The term of use may not be greater than 1024 characters.'
            }
        };
    }
    ContentsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        // this.userServ.currentUser.subscribe((user) => {
        //   console.log(user);
        //   if (!user.email) {
        //     console.log('user 1', user);
        //   }
        // });
        this.buildForm();
        this.mymodel = {
            aboutUs: null,
            privatePolicy: null,
            termOfUse: null
        };
        var params = {
            'rest_id': this.restId
        };
        this.restServ.getContents(params).then(function (response) {
            _this.mymodel.aboutUs = response.about_us;
            _this.mymodel.privatePolicy = response.private_policy;
            _this.mymodel.termOfUse = response.term_of_use;
            _this.setVar();
        }, function (error) {
            console.log(error);
        });
        console.log('init contents component');
    };
    ContentsComponent.prototype.setVar = function () {
        if (this.mymodel != null) {
            this.mymodel = {
                aboutUs: this.mymodel.aboutUs,
                privatePolicy: this.mymodel.privatePolicy,
                termOfUse: this.mymodel.termOfUse
            };
            this.dtForm.controls['aboutUs'].setValue(this.mymodel.aboutUs);
            this.dtForm.controls['privatePolicy'].setValue(this.mymodel.privatePolicy);
            this.dtForm.controls['termOfUse'].setValue(this.mymodel.termOfUse);
            this.aboutUs = this.dtForm.controls['aboutUs'];
            this.privatePolicy = this.dtForm.controls['privatePolicy'];
            this.termOfUse = this.dtForm.controls['termOfUse'];
        }
    };
    ContentsComponent.prototype.buildForm = function () {
        var _this = this;
        this.dtForm = this.fb.group({
            'aboutUs': ['', [forms_1.Validators.maxLength(1024)]],
            'privatePolicy': ['', [forms_1.Validators.maxLength(1024)]],
            'termOfUse': ['', [forms_1.Validators.maxLength(1024)]]
        });
        this.aboutUs = this.dtForm.controls['aboutUs'];
        this.privatePolicy = this.dtForm.controls['privatePolicy'];
        this.termOfUse = this.dtForm.controls['termOfUse'];
        this.dtForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
        //this.onValueChanged(); // (re)set validation messages now
    };
    ContentsComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.dtForm) {
            return;
        }
        var form1 = this.dtForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form1.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    ContentsComponent.prototype.addAboutUs = function () {
        this.saveContent('About us');
    };
    ContentsComponent.prototype.addPrivatePolicy = function () {
        this.saveContent('Private policy');
    };
    ContentsComponent.prototype.addTermOfUse = function () {
        this.saveContent('Term of use');
    };
    ContentsComponent.prototype.saveContent = function (msg) {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'about_us': this.aboutUs.value,
            'private_policy': this.privatePolicy.value,
            'term_of_use': this.termOfUse.value
        };
        this.restServ.updateContents(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            _this.notifyServ.success(msg + ' has been updated');
        }, function (error) {
            console.log(error);
        });
    };
    ContentsComponent = __decorate([
        core_1.Component({
            selector: 'contents',
            templateUrl: utils_1.default.getView('app/components/contents/contents.component.html'),
            styleUrls: [utils_1.default.getView('app/components/contents/contents.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
            forms_1.FormBuilder,
            user_service_1.UserService,
            notification_service_1.NotificationService])
    ], ContentsComponent);
    return ContentsComponent;
}());
exports.ContentsComponent = ContentsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NvbnRlbnRzL2NvbnRlbnRzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx3Q0FBNkY7QUFDN0YseUNBQW9DO0FBRXBDLDZEQUF1RTtBQUN2RSxzRUFBb0U7QUFDcEUsMEVBQXdFO0FBQ3hFLDBEQUF3RDtBQVN4RDtJQVVFLDJCQUNVLFFBQTJCLEVBQzNCLEVBQWUsRUFDZixRQUFxQixFQUNyQixVQUErQjtRQUgvQixhQUFRLEdBQVIsUUFBUSxDQUFtQjtRQUMzQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixlQUFVLEdBQVYsVUFBVSxDQUFxQjtRQVJqQyxZQUFPLEdBQW9CLElBQUksQ0FBQztRQUNoQyxrQkFBYSxHQUFvQixJQUFJLENBQUM7UUFDdEMsY0FBUyxHQUFvQixJQUFJLENBQUM7UUFnRzFDLGVBQVUsR0FBRztZQUNYLFNBQVMsRUFBRSxFQUFFO1lBQ2IsZUFBZSxFQUFFLEVBQUU7WUFDbkIsV0FBVyxFQUFFLEVBQUU7U0FDaEIsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLFNBQVMsRUFBRTtnQkFDVCxXQUFXLEVBQUUsdURBQXVEO2FBQ3JFO1lBQ0QsZUFBZSxFQUFFO2dCQUNmLFdBQVcsRUFBRSw2REFBNkQ7YUFDM0U7WUFDRCxXQUFXLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLDBEQUEwRDthQUN4RTtTQUNGLENBQUM7SUF6R0UsQ0FBQztJQUVMLG9DQUFRLEdBQVI7UUFBQSxpQkFvQ0M7UUFsQ0MsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUMsQ0FBQztRQUVELGtEQUFrRDtRQUNsRCx1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLG1DQUFtQztRQUNuQyxNQUFNO1FBQ04sTUFBTTtRQUVOLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsT0FBTyxFQUFFLElBQUk7WUFDYixhQUFhLEVBQUUsSUFBSTtZQUNuQixTQUFTLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBRUYsSUFBSSxNQUFNLEdBQUc7WUFDWCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07U0FDdkIsQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDN0MsS0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN6QyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDO1lBQ3JELEtBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDOUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2hCLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFTyxrQ0FBTSxHQUFkO1FBRUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUc7Z0JBQ2IsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTztnQkFDN0IsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYTtnQkFDekMsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUzthQUNsQyxDQUFDO1lBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDM0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDckQsQ0FBQztJQUNILENBQUM7SUFFTyxxQ0FBUyxHQUFqQjtRQUFBLGlCQWFDO1FBWEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUMxQixTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzdDLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkQsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNoRCxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7YUFDckIsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztRQUN2RCwyREFBMkQ7SUFDN0QsQ0FBQztJQUVPLDBDQUFjLEdBQXRCLFVBQXVCLE9BQU8sRUFBRSxJQUFVO1FBRXhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQzdCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDN0QsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFvQk8sc0NBQVUsR0FBbEI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFTyw0Q0FBZ0IsR0FBeEI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVPLHdDQUFZLEdBQXBCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRU8sdUNBQVcsR0FBbkIsVUFBb0IsR0FBRztRQUF2QixpQkE2QkM7UUE1QkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0gsQ0FBQztRQUVELElBQUksTUFBTSxHQUFHO1lBQ1gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQzlCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSztZQUMxQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO1NBQ3BDLENBQUM7UUFFRixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ2hELElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNyQixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDcEMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDaEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xELENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNiLE1BQU0sQ0FBQztZQUNULENBQUM7WUFDRCxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsbUJBQW1CLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFuS1UsaUJBQWlCO1FBUDdCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsVUFBVTtZQUNwQixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxpREFBaUQsQ0FBQztZQUM3RSxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7WUFDNUUsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBRSxxQkFBcUIsRUFBRSxFQUFFLEVBQUU7U0FDcEMsQ0FBQzt5Q0FZb0Isc0NBQWlCO1lBQ3ZCLG1CQUFXO1lBQ0wsMEJBQVc7WUFDVCwwQ0FBbUI7T0FkOUIsaUJBQWlCLENBb0s3QjtJQUFELHdCQUFDO0NBcEtELEFBb0tDLElBQUE7QUFwS1ksOENBQWlCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL2NvbnRlbnRzL2NvbnRlbnRzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0sIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBSZXN0YXVyYW50U2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9yZXN0YXVyYW50LnNlcnZpY2UnO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NvbnRlbnRzJyxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2NvbnRlbnRzL2NvbnRlbnRzLmNvbXBvbmVudC5odG1sJyksXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2NvbnRlbnRzL2NvbnRlbnRzLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7ICdbQHJvdXRlclRyYW5zaXRpb25dJzogJycgfVxufSlcbmV4cG9ydCBjbGFzcyBDb250ZW50c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgZHRGb3JtOiBGb3JtR3JvdXA7XG4gIHByaXZhdGUgYWJvdXRVczogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBwcml2YXRlUG9saWN5OiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIHRlcm1PZlVzZTogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJlc3RTZXJ2OiBSZXN0YXVyYW50U2VydmljZSxcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIG5vdGlmeVNlcnY6IE5vdGlmaWNhdGlvblNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcblxuICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVzZXJJZCA9IHVzZXJLZXkudXNlcl9pbmZvLmlkO1xuICAgICAgdGhpcy5yZXN0SWQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIH1cblxuICAgIC8vIHRoaXMudXNlclNlcnYuY3VycmVudFVzZXIuc3Vic2NyaWJlKCh1c2VyKSA9PiB7XG4gICAgLy8gICBjb25zb2xlLmxvZyh1c2VyKTtcbiAgICAvLyAgIGlmICghdXNlci5lbWFpbCkge1xuICAgIC8vICAgICBjb25zb2xlLmxvZygndXNlciAxJywgdXNlcik7XG4gICAgLy8gICB9XG4gICAgLy8gfSk7XG5cbiAgICB0aGlzLmJ1aWxkRm9ybSgpO1xuICAgIHRoaXMubXltb2RlbCA9IHtcbiAgICAgIGFib3V0VXM6IG51bGwsXG4gICAgICBwcml2YXRlUG9saWN5OiBudWxsLFxuICAgICAgdGVybU9mVXNlOiBudWxsXG4gICAgfTtcblxuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkXG4gICAgfTtcbiAgICB0aGlzLnJlc3RTZXJ2LmdldENvbnRlbnRzKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICB0aGlzLm15bW9kZWwuYWJvdXRVcyA9IHJlc3BvbnNlLmFib3V0X3VzO1xuICAgICAgdGhpcy5teW1vZGVsLnByaXZhdGVQb2xpY3kgPSByZXNwb25zZS5wcml2YXRlX3BvbGljeTtcbiAgICAgIHRoaXMubXltb2RlbC50ZXJtT2ZVc2UgPSByZXNwb25zZS50ZXJtX29mX3VzZTtcbiAgICAgIHRoaXMuc2V0VmFyKCk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICAgIGNvbnNvbGUubG9nKCdpbml0IGNvbnRlbnRzIGNvbXBvbmVudCcpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRWYXIoKTogdm9pZCB7XG5cbiAgICBpZiAodGhpcy5teW1vZGVsICE9IG51bGwpIHtcbiAgICAgIHRoaXMubXltb2RlbCA9IHtcbiAgICAgICAgYWJvdXRVczogdGhpcy5teW1vZGVsLmFib3V0VXMsXG4gICAgICAgIHByaXZhdGVQb2xpY3k6IHRoaXMubXltb2RlbC5wcml2YXRlUG9saWN5LFxuICAgICAgICB0ZXJtT2ZVc2U6IHRoaXMubXltb2RlbC50ZXJtT2ZVc2VcbiAgICAgIH07XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snYWJvdXRVcyddLnNldFZhbHVlKHRoaXMubXltb2RlbC5hYm91dFVzKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydwcml2YXRlUG9saWN5J10uc2V0VmFsdWUodGhpcy5teW1vZGVsLnByaXZhdGVQb2xpY3kpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ3Rlcm1PZlVzZSddLnNldFZhbHVlKHRoaXMubXltb2RlbC50ZXJtT2ZVc2UpO1xuICAgICAgdGhpcy5hYm91dFVzID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2Fib3V0VXMnXTtcbiAgICAgIHRoaXMucHJpdmF0ZVBvbGljeSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydwcml2YXRlUG9saWN5J107XG4gICAgICB0aGlzLnRlcm1PZlVzZSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWyd0ZXJtT2ZVc2UnXTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGJ1aWxkRm9ybSgpOiB2b2lkIHtcblxuICAgIHRoaXMuZHRGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAnYWJvdXRVcyc6IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMjQpXV0sXG4gICAgICAncHJpdmF0ZVBvbGljeSc6IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMjQpXV0sXG4gICAgICAndGVybU9mVXNlJzogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAyNCldXVxuICAgIH0pO1xuICAgIHRoaXMuYWJvdXRVcyA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydhYm91dFVzJ107XG4gICAgdGhpcy5wcml2YXRlUG9saWN5ID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ3ByaXZhdGVQb2xpY3knXTtcbiAgICB0aGlzLnRlcm1PZlVzZSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWyd0ZXJtT2ZVc2UnXTtcbiAgICB0aGlzLmR0Rm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGZhbHNlLCBkYXRhKSk7XG4gICAgLy90aGlzLm9uVmFsdWVDaGFuZ2VkKCk7IC8vIChyZSlzZXQgdmFsaWRhdGlvbiBtZXNzYWdlcyBub3dcbiAgfVxuXG4gIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQodW5EaXJ0eSwgZGF0YT86IGFueSkge1xuXG4gICAgaWYgKCF0aGlzLmR0Rm9ybSkgeyByZXR1cm47IH1cbiAgICBjb25zdCBmb3JtMSA9IHRoaXMuZHRGb3JtO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybTEuZ2V0KGZpZWxkKTtcbiAgICAgIGlmIChjb250cm9sICYmIChjb250cm9sLmRpcnR5IHx8IHVuRGlydHkpICYmIGNvbnRyb2wuaW52YWxpZCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlcyA9IHRoaXMudmFsaWRhdGlvbk1lc3NhZ2VzW2ZpZWxkXTtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJvbC5lcnJvcnMpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdLnB1c2gobWVzc2FnZXNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmb3JtRXJyb3JzID0ge1xuICAgICdhYm91dFVzJzogW10sXG4gICAgJ3ByaXZhdGVQb2xpY3knOiBbXSxcbiAgICAndGVybU9mVXNlJzogW11cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ2Fib3V0VXMnOiB7XG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBhYm91dCB1cyBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxMDI0IGNoYXJhY3RlcnMuJ1xuICAgIH0sXG4gICAgJ3ByaXZhdGVQb2xpY3knOiB7XG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBwcml2YXRlIHBvbGljeSBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxMDI0IGNoYXJhY3RlcnMuJ1xuICAgIH0sXG4gICAgJ3Rlcm1PZlVzZSc6IHtcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHRlcm0gb2YgdXNlIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDEwMjQgY2hhcmFjdGVycy4nXG4gICAgfVxuICB9O1xuXG4gIHByaXZhdGUgYWRkQWJvdXRVcygpIHtcbiAgICB0aGlzLnNhdmVDb250ZW50KCdBYm91dCB1cycpO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRQcml2YXRlUG9saWN5KCkge1xuICAgIHRoaXMuc2F2ZUNvbnRlbnQoJ1ByaXZhdGUgcG9saWN5Jyk7XG4gIH1cblxuICBwcml2YXRlIGFkZFRlcm1PZlVzZSgpIHtcbiAgICB0aGlzLnNhdmVDb250ZW50KCdUZXJtIG9mIHVzZScpO1xuICB9XG5cbiAgcHJpdmF0ZSBzYXZlQ29udGVudChtc2cpIHtcbiAgICB0aGlzLm9uVmFsdWVDaGFuZ2VkKHRydWUpO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICBpZiAodGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5sZW5ndGggPiAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgJ3VzZXJfaWQnOiB0aGlzLnVzZXJJZCxcbiAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAnYWJvdXRfdXMnOiB0aGlzLmFib3V0VXMudmFsdWUsXG4gICAgICAncHJpdmF0ZV9wb2xpY3knOiB0aGlzLnByaXZhdGVQb2xpY3kudmFsdWUsXG4gICAgICAndGVybV9vZl91c2UnOiB0aGlzLnRlcm1PZlVzZS52YWx1ZVxuICAgIH07XG5cbiAgICB0aGlzLnJlc3RTZXJ2LnVwZGF0ZUNvbnRlbnRzKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBsZXQgaGFyRXJyb3IgPSBmYWxzZTtcbiAgICAgIGZvciAoY29uc3QgZmllbGQgaW4gcmVzcG9uc2UuZXJyb3JzKSB7XG4gICAgICAgIGhhckVycm9yID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IHJlc3BvbnNlLmVycm9yc1tmaWVsZF07XG4gICAgICB9XG4gICAgICBpZiAoaGFyRXJyb3IpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5ub3RpZnlTZXJ2LnN1Y2Nlc3MobXNnICsgJyBoYXMgYmVlbiB1cGRhdGVkJyk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG59XG4iXX0=
