"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var area_service_1 = require("app/services/area.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var CreateAreaComponent = (function () {
    function CreateAreaComponent(fb, areaService, StorageService, authServ, notif, router, route) {
        this.fb = fb;
        this.areaService = areaService;
        this.StorageService = StorageService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.formErrors = {
            'name': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            }
        };
        /*upload file*/
        this.sactiveColor = 'green';
        this.baseColor = '#ccc';
        this.overlayColor = 'rgba(255,255,255,0.5)';
        this.dragging = false;
        this.loaded = false;
        this.imageLoaded = false;
        this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
        this.flgImageChoose = false;
        this.iconColor = '';
        this.borderColor = '';
        this.activeColor = '';
        this.hiddenImage = false;
    }
    CreateAreaComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            _this.isCheckRadio = '1';
            _this.createAreaForm.controls['nameRadio'].setValue(1);
            if (_this.id != null) {
                _this.areaItem = _this.StorageService.getScope();
                if (_this.areaItem == false) {
                    _this.fetchAllAreas(_this.restId, _this.id);
                }
                else {
                    _this.createAreaForm.controls['name'].setValue(_this.areaItem.name);
                    _this.createAreaForm.controls['nameRadio'].setValue(_this.areaItem.is_active);
                    _this.name = _this.createAreaForm.controls['name'];
                    _this.nameRadio = _this.createAreaForm.controls['nameRadio'];
                    _this.isCheckRadio = _this.areaItem.is_active;
                    _this.imageSrc = _this.areaItem.thumb;
                    if (_this.imageSrc == null) {
                        _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                    }
                }
            }
        });
    };
    CreateAreaComponent.prototype.checkFlg = function (value) {
        if (this.isCheckRadio == undefined || this.isCheckRadio == null) {
            this.isCheckRadio = '1';
        }
        if (value == this.isCheckRadio) {
            return true;
        }
    };
    CreateAreaComponent.prototype.changenameRadio = function (value) {
        this.isCheckRadio = value;
    };
    CreateAreaComponent.prototype.fetchAllAreas = function (restId, id) {
        var _this = this;
        this.areaService.getAreas(restId, id).then(function (response) {
            if (response.areas != null && response.areas.length > 0) {
                _this.areaItem = response.areas[0];
                _this.createAreaForm.controls['name'].setValue(_this.areaItem.name);
                _this.createAreaForm.controls['nameRadio'].setValue(_this.areaItem.is_active);
                _this.name = _this.createAreaForm.controls['name'];
                _this.nameRadio = _this.createAreaForm.controls['nameRadio'];
                _this.isCheckRadio = _this.areaItem.is_active;
                _this.imageSrc = _this.areaItem.thumb;
                if (_this.imageSrc == null) {
                    _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                }
            }
            else {
                _this.notif.error('area had delete');
                _this.router.navigateByUrl('area-list');
            }
        }, function (error) {
            console.log(error);
        });
    };
    CreateAreaComponent.prototype.buildForm = function () {
        var _this = this;
        this.createAreaForm = this.fb.group({
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ], 'nameRadio': []
        });
        this.name = this.createAreaForm.controls['name'];
        this.nameRadio = this.createAreaForm.controls['nameRadio'];
        this.createAreaForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    CreateAreaComponent.prototype.onValueChanged = function (data) {
        if (!this.createAreaForm) {
            return;
        }
        var form = this.createAreaForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    CreateAreaComponent.prototype.handleDragEnter = function () {
        this.dragging = true;
    };
    CreateAreaComponent.prototype.handleDragLeave = function () {
        this.dragging = false;
    };
    CreateAreaComponent.prototype.handleDrop = function (e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    };
    CreateAreaComponent.prototype.handleImageLoad = function () {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if (this.imageLoaded = true) {
            this.hiddenImage = true;
        }
    };
    CreateAreaComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        this.loaded = false;
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose = true;
    };
    CreateAreaComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    };
    CreateAreaComponent.prototype._setActive = function () {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    };
    CreateAreaComponent.prototype._setInactive = function () {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    };
    CreateAreaComponent.prototype.createArea = function () {
        var _this = this;
        var id = '-1';
        if (this.id != null) {
            id = this.id;
        }
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.areaObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'area': {
                'id': id,
                'name': this.createAreaForm.value.name,
                'thumb': '',
                'is_active': this.isCheckRadio,
                'is_delete': '0'
            }
        };
        this.areaService.createArea(this.areaObject).then(
        //          response  => {
        //          	this.areas = response;
        //          	console.log(response, this.areas);
        //          },
        //  error => {console.log(error)
        function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
        // });
    };
    CreateAreaComponent.prototype.processResult = function (response) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            var imagePost = '';
            if (this.flgImageChoose == true) {
                imagePost = this.imageSrc;
            }
            this.areas = response;
            var areaUpload = {
                'id': this.areas.area_id,
                'thumb': imagePost
            };
            this.areaService.areaUploadFile(areaUpload).then(function (response) {
                _this.notif.success('New area has been added');
                _this.router.navigateByUrl('area-list');
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.name) {
                    this.formErrors['name'] = this.error.name;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    CreateAreaComponent.prototype.failedCreate = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    CreateAreaComponent = __decorate([
        core_1.Component({
            selector: 'create-area',
            templateUrl: utils_1.default.getView('app/components/create-area/create-area.component.html'),
            styleUrls: [utils_1.default.getView('app/components/create-area/create-area.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': ''
            }
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            area_service_1.AreaService,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router, router_2.ActivatedRoute])
    ], CreateAreaComponent);
    return CreateAreaComponent;
}());
exports.CreateAreaComponent = CreateAreaComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NyZWF0ZS1hcmVhL2NyZWF0ZS1hcmVhLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx3Q0FBNEY7QUFDNUYseUNBQW9DO0FBR3BDLDZEQUEyRDtBQUMzRCwwREFBd0Q7QUFDeEQsMERBQXlEO0FBQ3pELDJDQUE4QztBQUM5QywwRUFBd0U7QUFDeEUsMENBQXlDO0FBQ3pDLDBDQUFpRDtBQUNqRCxnRUFBOEQ7QUFTOUQ7SUFXRSw2QkFBb0IsRUFBZSxFQUMzQixXQUF3QixFQUN4QixjQUE4QixFQUM5QixRQUFxQixFQUNyQixLQUEwQixFQUN6QixNQUFjLEVBQVMsS0FBcUI7UUFMakMsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUMzQixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFnR3ZELGVBQVUsR0FBRztZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1gsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQU8sdUJBQXVCO2dCQUN4QyxXQUFXLEVBQU0sK0NBQStDO2FBQ2pFO1NBQ0YsQ0FBQztRQUVGLGVBQWU7UUFDUixpQkFBWSxHQUFXLE9BQU8sQ0FBQztRQUM1QixjQUFTLEdBQVcsTUFBTSxDQUFDO1FBQzNCLGlCQUFZLEdBQVcsdUJBQXVCLENBQUM7UUFFL0MsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUMxQixXQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGFBQVEsR0FBVyxpRUFBaUUsQ0FBQztRQUNyRixtQkFBYyxHQUFXLEtBQUssQ0FBQztRQUMvQixjQUFTLEdBQVcsRUFBRSxDQUFDO1FBQ3RCLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBQ3pCLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBQzFCLGdCQUFXLEdBQVMsS0FBSyxDQUFDO0lBdkhwQyxDQUFDO0lBRUQsc0NBQVEsR0FBUjtRQUFBLGlCQTBCQztRQXpCRSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUM1QyxLQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QixLQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQztZQUN4QixLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEQsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLEVBQUUsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUNoQixLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQy9DLEVBQUUsQ0FBQSxDQUFDLEtBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLENBQUEsQ0FBQztvQkFDekIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDMUMsQ0FBQztnQkFBQSxJQUFJLENBQUEsQ0FBQztvQkFDRixLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQzVFLEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2pELEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzNELEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7b0JBQ3BDLEVBQUUsQ0FBQSxDQUFFLEtBQUksQ0FBQyxRQUFRLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQzt3QkFDdEIsS0FBSSxDQUFDLFFBQVEsR0FBRyxpRUFBaUUsQ0FBQztvQkFDckYsQ0FBQztnQkFDTCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELHNDQUFRLEdBQVIsVUFBUyxLQUFLO1FBQ1YsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBRSxTQUFTLElBQUksSUFBSSxDQUFDLFlBQVksSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQzFELElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDO1FBQzFCLENBQUM7UUFDRCxFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBLENBQUM7WUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO0lBQ0wsQ0FBQztJQUNELDZDQUFlLEdBQWYsVUFBZ0IsS0FBSztRQUNuQixJQUFJLENBQUMsWUFBWSxHQUFFLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBQ08sMkNBQWEsR0FBckIsVUFBc0IsTUFBTSxFQUFDLEVBQUU7UUFBL0IsaUJBcUJFO1FBcEJBLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQ3hCLFVBQUEsUUFBUTtZQUNOLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUUsSUFBSSxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pFLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM3RSxLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNqRCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzRCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUM1QyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2dCQUNwQyxFQUFFLENBQUEsQ0FBRSxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7b0JBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsaUVBQWlFLENBQUM7Z0JBQ3JGLENBQUM7WUFDRixDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0osS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDcEMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDekMsQ0FBQztRQUNILENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNJLHVDQUFTLEdBQWpCO1FBQUEsaUJBZUc7UUFkQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ2xDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDVCxrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUI7YUFFRixFQUFDLFdBQVcsRUFBQyxFQUFFO1NBQ2pCLENBQUMsQ0FBQztRQUNBLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVk7YUFDN0IsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDO1FBRWhELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLGtDQUFrQztJQUMzRCxDQUFDO0lBQ1EsNENBQWMsR0FBdEIsVUFBdUIsSUFBVTtRQUNoQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUNyQyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBRWpDLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRWhDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEQsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBK0JDLDZDQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQsNkNBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRCx3Q0FBVSxHQUFWLFVBQVcsQ0FBQztRQUNSLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELDZDQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLENBQUM7SUFDQyxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCLFVBQWtCLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVwQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFFLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQsaURBQW1CLEdBQW5CLFVBQW9CLENBQUM7UUFDakIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELHdDQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFRCwwQ0FBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3BDLENBQUM7SUFDTCxDQUFDO0lBQ0wsd0NBQVUsR0FBVjtRQUFBLGlCQWdDQztRQS9CQyxJQUFJLEVBQUUsR0FBSSxJQUFJLENBQUM7UUFDZixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDaEIsRUFBRSxHQUFJLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDaEIsQ0FBQztRQUNELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNsQyx3Q0FBd0M7WUFDdkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDbkMsTUFBTSxDQUFDO1lBQ1QsQ0FBQztRQUNKLENBQUM7UUFDSCxJQUFJLENBQUMsVUFBVSxHQUFHO1lBQ1IsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBQyxJQUFJLENBQUMsTUFBTTtZQUNyQixNQUFNLEVBQUM7Z0JBQ0wsSUFBSSxFQUFDLEVBQUU7Z0JBQ1AsTUFBTSxFQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLElBQUk7Z0JBQ3JDLE9BQU8sRUFBQyxFQUFFO2dCQUNWLFdBQVcsRUFBQyxJQUFJLENBQUMsWUFBWTtnQkFDN0IsV0FBVyxFQUFDLEdBQUc7YUFDaEI7U0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUk7UUFDeEMsMEJBQTBCO1FBQzFCLG1DQUFtQztRQUNuQywrQ0FBK0M7UUFDL0MsY0FBYztRQUNuQixnQ0FBZ0M7UUFDdkIsVUFBQSxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixFQUNwQyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7UUFDdkQsTUFBTTtJQUViLENBQUM7SUFFTywyQ0FBYSxHQUFyQixVQUFzQixRQUFRO1FBQTlCLGlCQWtDRztRQWhDQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLFNBQVMsSUFBSSxRQUFRLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLEVBQUUsQ0FBQSxDQUFFLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDL0IsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDNUIsQ0FBQztZQUNBLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO1lBQ3RCLElBQUksVUFBVSxHQUFHO2dCQUNkLElBQUksRUFBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87Z0JBQ3ZCLE9BQU8sRUFBQyxTQUFTO2FBQ25CLENBQUE7WUFDRixJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQ2hDLFVBQUEsUUFBUTtnQkFDUCxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO2dCQUM5QyxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN4QyxDQUFDLEVBQ2QsVUFBQSxLQUFLO2dCQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7WUFFN0IsQ0FBQyxDQUFDLENBQUM7UUFDTixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDN0IsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQzVDLENBQUM7WUFDSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04scUJBQUksQ0FDRixjQUFjLEVBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFDTywwQ0FBWSxHQUFwQixVQUFzQixHQUFHO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0QiwwQkFBMEI7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQztnQkFDckQseUJBQXlCO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTiw0Q0FBNEM7Z0JBQzVDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsd0RBQXdEO2dCQUMxRCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBM1JVLG1CQUFtQjtRQVIvQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGFBQWE7WUFDdkIsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsdURBQXVELENBQUM7WUFDbkYsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxzREFBc0QsQ0FBQyxDQUFDO1lBQ2xGLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUMscUJBQXFCLEVBQUUsRUFBRTthQUNqQztTQUNBLENBQUM7eUNBWXdCLG1CQUFXO1lBQ2QsMEJBQVc7WUFDUixnQ0FBYztZQUNwQiwwQkFBVztZQUNkLDBDQUFtQjtZQUNqQixlQUFNLEVBQWdCLHVCQUFjO09BaEIxQyxtQkFBbUIsQ0E0Ui9CO0lBQUQsMEJBQUM7Q0E1UkQsQUE0UkMsSUFBQTtBQTVSWSxrREFBbUIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvY3JlYXRlLWFyZWEvY3JlYXRlLWFyZWEuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMsIE5nRm9ybSAsQWJzdHJhY3RDb250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IEFyZWFTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2FyZWEuc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjcmVhdGUtYXJlYScsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtYXJlYS9jcmVhdGUtYXJlYS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtYXJlYS9jcmVhdGUtYXJlYS5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJydcbn1cbn0pXG5leHBvcnQgY2xhc3MgQ3JlYXRlQXJlYUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0cHJpdmF0ZSBpZDogc3RyaW5nO1xuICBwcml2YXRlIHN1YjogYW55O1xuXHRwcml2YXRlIG15bW9kZWw6IGFueTtcblx0cHJpdmF0ZSBjcmVhdGVBcmVhRm9ybTogRm9ybUdyb3VwO1xuXHRwcml2YXRlIG5hbWU6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBuYW1lUmFkaW86IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgaXNDaGVja1JhZGlvIDogc3RyaW5nO1xuICBwcml2YXRlIGFyZWFJdGVtIDogYW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgcHJpdmF0ZSBhcmVhU2VydmljZTogQXJlYVNlcnZpY2UsXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLFxuICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcbiAgcHJpdmF0ZSBub3RpZjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIscHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgICBsZXQgdXNlckluZm8gPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICAgdGhpcy5yZXN0SWQgPSB1c2VySW5mby5yZXN0YXVyYW50c1swXS5pZDtcbiAgICAgdGhpcy51c2VySWQgPSB1c2VySW5mby51c2VyX2luZm8uaWQ7XG4gIFx0IHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgXHR0aGlzLnN1YiA9IHRoaXMucm91dGUucGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgICAgdGhpcy5pZCA9IHBhcmFtc1snaWQnXTtcbiAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gJzEnO1xuICAgICAgdGhpcy5jcmVhdGVBcmVhRm9ybS5jb250cm9sc1snbmFtZVJhZGlvJ10uc2V0VmFsdWUoMSk7XG4gICAgICBpZih0aGlzLmlkIT1udWxsKXtcbiAgICAgICAgdGhpcy5hcmVhSXRlbSA9IHRoaXMuU3RvcmFnZVNlcnZpY2UuZ2V0U2NvcGUoKTtcbiAgICAgICAgaWYodGhpcy5hcmVhSXRlbSA9PSBmYWxzZSl7XG4gICAgICAgICAgdGhpcy5mZXRjaEFsbEFyZWFzKHRoaXMucmVzdElkLHRoaXMuaWQpO1xuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlQXJlYUZvcm0uY29udHJvbHNbJ25hbWUnXS5zZXRWYWx1ZSh0aGlzLmFyZWFJdGVtLm5hbWUpO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVBcmVhRm9ybS5jb250cm9sc1snbmFtZVJhZGlvJ10uc2V0VmFsdWUodGhpcy5hcmVhSXRlbS5pc19hY3RpdmUpO1xuICAgICAgICAgICAgdGhpcy5uYW1lID0gdGhpcy5jcmVhdGVBcmVhRm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgICAgICAgICAgdGhpcy5uYW1lUmFkaW8gPSB0aGlzLmNyZWF0ZUFyZWFGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXTtcbiAgICAgICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gdGhpcy5hcmVhSXRlbS5pc19hY3RpdmU7XG4gICAgICAgICAgICB0aGlzLmltYWdlU3JjID0gdGhpcy5hcmVhSXRlbS50aHVtYjtcbiAgICAgICAgICAgIGlmKCB0aGlzLmltYWdlU3JjPT1udWxsKXtcbiAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VTcmMgPSAnaHR0cDovL3d3dy5wbGFjZWhvbGQuaXQvMjAweDE1MC9FRkVGRUYvQUFBQUFBJmFtcDt0ZXh0PW5vK2ltYWdlJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIGNoZWNrRmxnKHZhbHVlKXtcbiAgICAgIGlmKHRoaXMuaXNDaGVja1JhZGlvPT11bmRlZmluZWQgfHwgdGhpcy5pc0NoZWNrUmFkaW89PW51bGwpe1xuICAgICAgICB0aGlzLmlzQ2hlY2tSYWRpbyA9ICcxJztcbiAgICAgIH1cbiAgICAgIGlmKHZhbHVlPT10aGlzLmlzQ2hlY2tSYWRpbyl7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gIH1cbiAgY2hhbmdlbmFtZVJhZGlvKHZhbHVlKXtcbiAgICB0aGlzLmlzQ2hlY2tSYWRpbyA9dmFsdWU7XG4gIH1cbiAgcHJpdmF0ZSBmZXRjaEFsbEFyZWFzKHJlc3RJZCxpZCkge1xuICBcdFx0dGhpcy5hcmVhU2VydmljZS5nZXRBcmVhcyhyZXN0SWQsaWQpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5hcmVhcyE9bnVsbCAmJiByZXNwb25zZS5hcmVhcy5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5hcmVhSXRlbSA9IHJlc3BvbnNlLmFyZWFzWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICBcdHRoaXMuY3JlYXRlQXJlYUZvcm0uY29udHJvbHNbJ25hbWUnXS5zZXRWYWx1ZSh0aGlzLmFyZWFJdGVtLm5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlQXJlYUZvcm0uY29udHJvbHNbJ25hbWVSYWRpbyddLnNldFZhbHVlKHRoaXMuYXJlYUl0ZW0uaXNfYWN0aXZlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlQXJlYUZvcm0uY29udHJvbHNbJ25hbWUnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubmFtZVJhZGlvID0gdGhpcy5jcmVhdGVBcmVhRm9ybS5jb250cm9sc1snbmFtZVJhZGlvJ107XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQ2hlY2tSYWRpbyA9IHRoaXMuYXJlYUl0ZW0uaXNfYWN0aXZlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9IHRoaXMuYXJlYUl0ZW0udGh1bWI7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiggdGhpcy5pbWFnZVNyYz09bnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlU3JjID0gJ2h0dHA6Ly93d3cucGxhY2Vob2xkLml0LzIwMHgxNTAvRUZFRkVGL0FBQUFBQSZhbXA7dGV4dD1ubytpbWFnZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWYuZXJyb3IoJ2FyZWEgaGFkIGRlbGV0ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ2FyZWEtbGlzdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbnByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xuICAgIHRoaXMuY3JlYXRlQXJlYUZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICAgICduYW1lJzogWycnLCBbXG4gICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZCxcbiAgICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTYpLFxuICAgICAgICBdXG5cbiAgICAgIF0sJ25hbWVSYWRpbyc6W11cbiAgICB9KTtcbiAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZUFyZWFGb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgICAgdGhpcy5uYW1lUmFkaW8gPSB0aGlzLmNyZWF0ZUFyZWFGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXTtcbiAgICB0aGlzLmNyZWF0ZUFyZWFGb3JtLnZhbHVlQ2hhbmdlc1xuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMub25WYWx1ZUNoYW5nZWQoZGF0YSkpO1xuXG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCgpOyAvLyAocmUpc2V0IHZhbGlkYXRpb24gbWVzc2FnZXMgbm93XG4gIH1cbiAgIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQoZGF0YT86IGFueSkge1xuICAgIGlmICghdGhpcy5jcmVhdGVBcmVhRm9ybSkgeyByZXR1cm47IH1cbiAgICBjb25zdCBmb3JtID0gdGhpcy5jcmVhdGVBcmVhRm9ybTtcblxuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuXG4gICAgICBpZiAoY29udHJvbCAmJiBjb250cm9sLmRpcnR5ICYmICFjb250cm9sLnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuZm9ybUVycm9ycyA9IHtcbiAgICAnbmFtZSc6IFtdXG4gIH07XG5cbiAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICduYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIG5hbWUgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBuYW1lIG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9XG4gIH07XG5cbiAgLyp1cGxvYWQgZmlsZSovXG5cdHByaXZhdGUgc2FjdGl2ZUNvbG9yOiBzdHJpbmcgPSAnZ3JlZW4nO1xuICAgIHByaXZhdGUgYmFzZUNvbG9yOiBzdHJpbmcgPSAnI2NjYyc7XG4gICAgcHJpdmF0ZSBvdmVybGF5Q29sb3I6IHN0cmluZyA9ICdyZ2JhKDI1NSwyNTUsMjU1LDAuNSknO1xuXG4gICAgcHJpdmF0ZSBkcmFnZ2luZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgbG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpbWFnZUxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaW1hZ2VTcmM6IHN0cmluZyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgIHByaXZhdGUgZmxnSW1hZ2VDaG9vc2U6Ym9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaWNvbkNvbG9yOiBzdHJpbmcgPSAnJztcbiAgICBwcml2YXRlICBib3JkZXJDb2xvcjogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSAgYWN0aXZlQ29sb3I6IHN0cmluZyA9ICcnO1xuICAgIHByaXZhdGUgaGlkZGVuSW1hZ2U6Ym9vbGVhbj1mYWxzZTtcbiAgICBwcml2YXRlIGFyZWFzIDogYW55O1xuICAgIHByaXZhdGUgYXJlYU9iamVjdCA6IGFueTtcbiAgICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgICBwcml2YXRlIGZpbGVDaG9vc2UgOiBGaWxlO1xuICAgIHByaXZhdGUgZXJyb3I6IGFueTtcbiAgICBoYW5kbGVEcmFnRW50ZXIoKSB7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSB0cnVlO1xuICAgIH1cblxuICAgIGhhbmRsZURyYWdMZWF2ZSgpIHtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgIH1cblxuICAgIGhhbmRsZURyb3AoZSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5oYW5kbGVJbnB1dENoYW5nZShlKTtcbiAgICB9XG5cbiAgICBoYW5kbGVJbWFnZUxvYWQoKSB7XG4gICAgICAgIHRoaXMuaW1hZ2VMb2FkZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLmljb25Db2xvciA9IHRoaXMub3ZlcmxheUNvbG9yO1xuICAgICAgICBpZih0aGlzLmltYWdlTG9hZGVkID0gdHJ1ZSl7XG5cdFx0XHR0aGlzLmhpZGRlbkltYWdlID0gdHJ1ZTtcblx0XHR9XG4gICAgfVxuXG4gICAgaGFuZGxlSW5wdXRDaGFuZ2UoZSkge1xuICAgICAgICB2YXIgZmlsZSA9IGUuZGF0YVRyYW5zZmVyID8gZS5kYXRhVHJhbnNmZXIuZmlsZXNbMF0gOiBlLnRhcmdldC5maWxlc1swXTtcbiAgICAgICAgdGhpcy5maWxlQ2hvb3NlID0gZmlsZTtcbiAgICAgICAgdmFyIHBhdHRlcm4gPSAvaW1hZ2UtKi87XG4gICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXG4gICAgICAgIGlmICghZmlsZS50eXBlLm1hdGNoKHBhdHRlcm4pKSB7XG4gICAgICAgICAgICBhbGVydCgnaW52YWxpZCBmb3JtYXQnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubG9hZGVkID0gZmFsc2U7XG5cbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9IHRoaXMuX2hhbmRsZVJlYWRlckxvYWRlZC5iaW5kKHRoaXMpO1xuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcbiAgICAgICAgdGhpcy5mbGdJbWFnZUNob29zZT0gdHJ1ZTtcbiAgICB9XG5cbiAgICBfaGFuZGxlUmVhZGVyTG9hZGVkKGUpIHtcbiAgICAgICAgdmFyIHJlYWRlciA9IGUudGFyZ2V0O1xuICAgICAgICB0aGlzLmltYWdlU3JjID0gcmVhZGVyLnJlc3VsdDtcbiAgICAgICAgdGhpcy5sb2FkZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIF9zZXRBY3RpdmUoKSB7XG4gICAgICAgIHRoaXMuYm9yZGVyQ29sb3IgPSB0aGlzLmFjdGl2ZUNvbG9yO1xuICAgICAgICBpZiAodGhpcy5pbWFnZVNyYy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5hY3RpdmVDb2xvcjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIF9zZXRJbmFjdGl2ZSgpIHtcbiAgICAgICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYmFzZUNvbG9yO1xuICAgICAgICBpZiAodGhpcy5pbWFnZVNyYy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5iYXNlQ29sb3I7XG4gICAgICAgIH1cbiAgICB9XG5jcmVhdGVBcmVhKCl7XG4gIGxldCBpZCAgPSAnLTEnO1xuICBpZih0aGlzLmlkIT1udWxsKXtcbiAgICBpZCAgPSB0aGlzLmlkO1xuICB9XG4gIHRoaXMub25WYWx1ZUNoYW5nZWQoKTtcbiAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIC8vIGNsZWFyIHByZXZpb3VzIGVycm9yIG1lc3NhZ2UgKGlmIGFueSlcbiAgICAgICBpZih0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+MCl7XG4gICAgICAgICByZXR1cm47XG4gICAgICAgfVxuICAgIH1cbiAgdGhpcy5hcmVhT2JqZWN0ID0ge1xuICAgICAgICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICdhcmVhJzp7XG4gICAgICAgICAgICAgICdpZCc6aWQsXG4gICAgICAgICAgICAgICduYW1lJzp0aGlzLmNyZWF0ZUFyZWFGb3JtLnZhbHVlLm5hbWUsXG4gICAgICAgICAgICAgICd0aHVtYic6JycsXG4gICAgICAgICAgICAgICdpc19hY3RpdmUnOnRoaXMuaXNDaGVja1JhZGlvLFxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzonMCdcbiAgICAgICAgICAgIH19O1xuICBcdHRoaXMuYXJlYVNlcnZpY2UuY3JlYXRlQXJlYSh0aGlzLmFyZWFPYmplY3QpLnRoZW4oXG4gICAgICAgICAgICAvLyAgICAgICAgICByZXNwb25zZSAgPT4ge1xuICAgICAgICAgICAgLy8gICAgICAgICAgXHR0aGlzLmFyZWFzID0gcmVzcG9uc2U7XG4gICAgICAgICAgICAvLyAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlLCB0aGlzLmFyZWFzKTtcbiAgICAgICAgICAgIC8vICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQvLyAgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcbiAgXHRcdFx0XHRcdC8vIH0pO1xuXG59XG5cbnByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXNwb25zZSkge1xuXG4gICAgaWYgKHJlc3BvbnNlLm1lc3NhZ2UgPT0gdW5kZWZpbmVkIHx8IHJlc3BvbnNlLm1lc3NhZ2UgPT0gJ09LJykge1xuICAgICBsZXQgaW1hZ2VQb3N0ID0gJyc7XG4gICAgIGlmKCB0aGlzLmZsZ0ltYWdlQ2hvb3NlID09IHRydWUpe1xuICAgICAgIGltYWdlUG9zdCA9IHRoaXMuaW1hZ2VTcmM7XG4gICAgIH1cbiAgICAgIHRoaXMuYXJlYXMgPSByZXNwb25zZTtcbiAgICAgIGxldCBhcmVhVXBsb2FkID0ge1xuICAgICAgICAgJ2lkJzp0aGlzLmFyZWFzLmFyZWFfaWQsXG4gICAgICAgICAndGh1bWInOmltYWdlUG9zdFxuICAgICAgfVxuXHQgICAgdGhpcy5hcmVhU2VydmljZS5hcmVhVXBsb2FkRmlsZShhcmVhVXBsb2FkKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHtcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5ub3RpZi5zdWNjZXNzKCdOZXcgYXJlYSBoYXMgYmVlbiBhZGRlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ2FyZWEtbGlzdCcpO1xuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG5cbiAgXHRcdFx0XHRcdH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmVycm9yID0gcmVzcG9uc2UuZXJyb3JzO1xuICAgICAgaWYgKHJlc3BvbnNlLmNvZGUgPT0gNDIyKSB7XG4gICAgICAgIGlmICh0aGlzLmVycm9yLm5hbWUpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ25hbWUnXSA9IHRoaXMuZXJyb3IubmFtZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3dhbChcbiAgICAgICAgICAnQ3JlYXRlIEZhaWwhJyxcbiAgICAgICAgICB0aGlzLmVycm9yWzBdLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICBwcml2YXRlIGZhaWxlZENyZWF0ZSAocmVzKSB7XG4gICAgaWYgKHJlcy5zdGF0dXMgPT0gNDAxKSB7XG4gICAgICAvLyB0aGlzLmxvZ2luZmFpbGVkID0gdHJ1ZVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAocmVzLmRhdGEuZXJyb3JzLm1lc3NhZ2VbMF0gPT0gJ0VtYWlsIFVudmVyaWZpZWQnKSB7XG4gICAgICAgIC8vIHRoaXMudW52ZXJpZmllZCA9IHRydWVcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIG90aGVyIGtpbmRzIG9mIGVycm9yIHJldHVybmVkIGZyb20gc2VydmVyXG4gICAgICAgIGZvciAodmFyIGVycm9yIGluIHJlcy5kYXRhLmVycm9ycykge1xuICAgICAgICAgIC8vIHRoaXMubG9naW5mYWlsZWRlcnJvciArPSByZXMuZGF0YS5lcnJvcnNbZXJyb3JdICsgJyAnXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==
