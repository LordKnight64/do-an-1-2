"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var email_service_1 = require("app/services/email.service");
var router_1 = require("@angular/router");
var EmailMarketingListComponent = (function () {
    function EmailMarketingListComponent(emailServ, userServ, router, storageServ, notifyServ) {
        this.emailServ = emailServ;
        this.userServ = userServ;
        this.router = router;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.currentPage = 1;
        this.totalItems = 0;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    EmailMarketingListComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.doSearch();
    };
    EmailMarketingListComponent.prototype.doSearch = function () {
        var _this = this;
        console.log(' this.restId ', this.restId);
        var params = {
            'rest_id': this.restId
        };
        this.emailServ.getEmailMarketingList(params).then(function (response) {
            _this.newsList = response.emails;
            _this.totalItems = _this.newsList.length;
            if (_this.totalItems > 0)
                _this.pageChanged({ page: _this.currentPage, itemsPerPage: _this.itemsPerPage });
        }, function (error) {
            console.log(error);
        });
    };
    EmailMarketingListComponent.prototype.pageChanged = function (event) {
        var start = (event.page - 1) * event.itemsPerPage;
        var end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
        this.data = this.newsList.slice(start, end);
    };
    EmailMarketingListComponent.prototype.doDeleteSearch = function (obj) {
        var id = obj.id;
        var params = {
            'id': id
        };
        var me = this;
        this.emailServ.deleteEmailMarketingList(params).then(function (response) {
            if (response.return_cd == 0) {
                me.notifyServ.success('Search Items has been deleted');
                me.doSearch();
            }
        }, function (error) {
            console.log(error);
        });
    };
    EmailMarketingListComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    EmailMarketingListComponent.prototype.viewEmailDetail = function (item) {
        this.router.navigateByUrl('/view-email-marketing-detail/' + item.id);
    };
    EmailMarketingListComponent = __decorate([
        core_1.Component({
            selector: 'table-list',
            templateUrl: utils_1.default.getView('app/components/email-marketing-list/email-marketing-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/email-marketing-list/email-marketing-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [email_service_1.EmailService,
            user_service_1.UserService,
            router_1.Router,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], EmailMarketingListComponent);
    return EmailMarketingListComponent;
}());
exports.EmailMarketingListComponent = EmailMarketingListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2VtYWlsLW1hcmtldGluZy1saXN0L2VtYWlsLW1hcmtldGluZy1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFFcEMsNkRBQXVFO0FBQ3ZFLDBEQUF3RDtBQUN4RCxnRUFBOEQ7QUFDOUQsMEVBQXdFO0FBQ3hFLDREQUEwRDtBQUMxRCwwQ0FBeUM7QUFTekM7SUFVRSxxQ0FDVSxTQUF1QixFQUN2QixRQUFxQixFQUNwQixNQUFjLEVBQ2YsV0FBMkIsRUFDM0IsVUFBK0I7UUFKL0IsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3BCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDM0IsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFWakMsZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFDeEIsZUFBVSxHQUFXLENBQUMsQ0FBQztRQUN2QixpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUMxQixZQUFPLEdBQVcsRUFBRSxDQUFDO0lBUXpCLENBQUM7SUFFTCw4Q0FBUSxHQUFSO1FBRUcsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDNUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUMsQ0FBQztRQUNELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRU8sOENBQVEsR0FBaEI7UUFBQSxpQkFjQztRQWJDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUzQyxJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRyxJQUFJLENBQUMsTUFBTTtTQUN4QixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ3hELEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUNoQyxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1lBQ3ZDLEVBQUUsQ0FBQSxDQUFDLEtBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQixLQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQ25GLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLGlEQUFXLEdBQWxCLFVBQW1CLEtBQVU7UUFDM0IsSUFBSSxLQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7UUFDbEQsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDeEYsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVPLG9EQUFjLEdBQXRCLFVBQXVCLEdBQUc7UUFFeEIsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLEVBQUUsQ0FBQztRQUNoQixJQUFJLE1BQU0sR0FBRztZQUNYLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQztRQUNGLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQztRQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsd0JBQXdCLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUMzRCxFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLEVBQUUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLCtCQUErQixDQUFDLENBQUM7Z0JBQ3ZELEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQixDQUFDO1FBQ0gsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sZ0RBQVUsR0FBbEIsVUFBbUIsR0FBRztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRU8scURBQWUsR0FBdkIsVUFBd0IsSUFBSTtRQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQywrQkFBK0IsR0FBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQTNFVSwyQkFBMkI7UUFQdEMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLHlFQUF5RSxDQUFDO1lBQ3JHLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsd0VBQXdFLENBQUMsQ0FBQztZQUNwRyxVQUFVLEVBQUUsQ0FBQyxvQ0FBZ0IsRUFBRSxDQUFDO1lBQ2hDLElBQUksRUFBRSxFQUFDLHFCQUFxQixFQUFFLEVBQUUsRUFBQztTQUNsQyxDQUFDO3lDQVlxQiw0QkFBWTtZQUNiLDBCQUFXO1lBQ1osZUFBTTtZQUNGLGdDQUFjO1lBQ2YsMENBQW1CO09BZjlCLDJCQUEyQixDQTZFdkM7SUFBRCxrQ0FBQztDQTdFRCxBQTZFQyxJQUFBO0FBN0VZLGtFQUEyQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9lbWFpbC1tYXJrZXRpbmctbGlzdC9lbWFpbC1tYXJrZXRpbmctbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSwgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgRW1haWxTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2VtYWlsLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuIEBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3RhYmxlLWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvZW1haWwtbWFya2V0aW5nLWxpc3QvZW1haWwtbWFya2V0aW5nLWxpc3QuY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvZW1haWwtbWFya2V0aW5nLWxpc3QvZW1haWwtbWFya2V0aW5nLWxpc3QuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJUcmFuc2l0aW9uXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBFbWFpbE1hcmtldGluZ0xpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgcHJpdmF0ZSByZXN0SWQ7XG4gIHByaXZhdGUgdXNlcklkO1xuICBwcml2YXRlIG5ld3NMaXN0OiBhbnk7XG4gIHByaXZhdGUgZGF0YTogYW55O1xuICBwcml2YXRlIGN1cnJlbnRQYWdlOiBudW1iZXIgPSAxO1xuICBwcml2YXRlIHRvdGFsSXRlbXM6IG51bWJlciA9IDA7XG4gIHByaXZhdGUgaXRlbXNQZXJQYWdlOiBudW1iZXIgPSAxMDtcbiAgcHJpdmF0ZSBtYXhTaXplOiBudW1iZXIgPSAxMDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGVtYWlsU2VydjogRW1haWxTZXJ2aWNlLFxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxuICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVzZXJJZCA9IHVzZXJLZXkudXNlcl9pbmZvLmlkO1xuICAgICAgdGhpcy5yZXN0SWQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIH1cbiAgICB0aGlzLmRvU2VhcmNoKCk7XG4gIH1cblxuICBwcml2YXRlIGRvU2VhcmNoKCkge1xuICAgIGNvbnNvbGUubG9nKCcgdGhpcy5yZXN0SWQgJywgIHRoaXMucmVzdElkKTtcbiAgIFxuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAncmVzdF9pZCcgOiB0aGlzLnJlc3RJZFxuICAgIH07XG4gICAgdGhpcy5lbWFpbFNlcnYuZ2V0RW1haWxNYXJrZXRpbmdMaXN0KHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICB0aGlzLm5ld3NMaXN0ID0gcmVzcG9uc2UuZW1haWxzO1xuICAgICAgdGhpcy50b3RhbEl0ZW1zID0gdGhpcy5uZXdzTGlzdC5sZW5ndGg7XG4gICAgICBpZih0aGlzLnRvdGFsSXRlbXMgPiAwKVxuICAgICAgICAgdGhpcy5wYWdlQ2hhbmdlZCh7IHBhZ2U6IHRoaXMuY3VycmVudFBhZ2UsIGl0ZW1zUGVyUGFnZTogdGhpcy5pdGVtc1BlclBhZ2UgfSk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHBhZ2VDaGFuZ2VkKGV2ZW50OiBhbnkpOiB2b2lkIHtcbiAgICBsZXQgc3RhcnQgPSAoZXZlbnQucGFnZSAtIDEpICogZXZlbnQuaXRlbXNQZXJQYWdlO1xuICAgIGxldCBlbmQgPSBldmVudC5pdGVtc1BlclBhZ2UgPiAtMSA/IChzdGFydCArIGV2ZW50Lml0ZW1zUGVyUGFnZSkgOiB0aGlzLm5ld3NMaXN0Lmxlbmd0aDtcbiAgICB0aGlzLmRhdGEgPSB0aGlzLm5ld3NMaXN0LnNsaWNlKHN0YXJ0LCBlbmQpO1xuICB9XG5cbiAgcHJpdmF0ZSBkb0RlbGV0ZVNlYXJjaChvYmopIHtcbiAgIFxuICAgIGxldCBpZCA9IG9iai5pZDtcbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgJ2lkJzogaWRcbiAgICB9O1xuICAgIHZhciBtZSA9IHRoaXM7XG4gICAgdGhpcy5lbWFpbFNlcnYuZGVsZXRlRW1haWxNYXJrZXRpbmdMaXN0KHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBpZihyZXNwb25zZS5yZXR1cm5fY2QgPT0gMCkge1xuICAgICAgICBtZS5ub3RpZnlTZXJ2LnN1Y2Nlc3MoJ1NlYXJjaCBJdGVtcyBoYXMgYmVlbiBkZWxldGVkJyk7XG4gICAgICAgIG1lLmRvU2VhcmNoKCk7XG4gICAgICB9XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb1NldFNjb3BlKG9iaikge1xuICAgIHRoaXMuc3RvcmFnZVNlcnYuc2V0U2NvcGUob2JqKTtcbiAgfVxuXG4gIHByaXZhdGUgdmlld0VtYWlsRGV0YWlsKGl0ZW0pIHtcbiAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvdmlldy1lbWFpbC1tYXJrZXRpbmctZGV0YWlsLycrIGl0ZW0uaWQpO1xuICB9XG5cbn1cbiJdfQ==
