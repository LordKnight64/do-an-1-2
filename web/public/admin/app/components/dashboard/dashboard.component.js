"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var breadcrumb_service_1 = require("app/services/breadcrumb.service");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var order_service_1 = require("app/services/order.service");
var contact_service_1 = require("app/services/contact.service");
var foodItem_service_1 = require("app/services/foodItem.service");
var category_service_1 = require("app/services/category.service");
var table_service_1 = require("app/services/table.service");
var turnover_service_1 = require("app/services/turnover.service");
var Observable_1 = require("rxjs/Observable");
var notification_service_1 = require("app/services/notification.service");
var comCode_service_1 = require("app/services/comCode.service");
var DashboardComponent = (function () {
    function DashboardComponent(contactServ, foodItemServ, categoryServ, breadServ, userServ, orderServ, tableService, comCodeServ, notifyServ, turnOverService, storageServ) {
        this.contactServ = contactServ;
        this.foodItemServ = foodItemServ;
        this.categoryServ = categoryServ;
        this.breadServ = breadServ;
        this.userServ = userServ;
        this.orderServ = orderServ;
        this.tableService = tableService;
        this.comCodeServ = comCodeServ;
        this.notifyServ = notifyServ;
        this.turnOverService = turnOverService;
        this.storageServ = storageServ;
        this.order = {};
        this.totalTurnOverByMonth = 0;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.doSearch();
        this.getOrderRequest(true);
        this.processResult();
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId
        };
        this.fetchTurnOver(this.restId, 2017);
        // long polling
        this.subscription = Observable_1.Observable.interval(30000)
            .map(function (x) { return x + 1; })
            .subscribe(function (x) {
            _this.getOrderRequest(false);
        });
        this.order = {};
    };
    DashboardComponent.prototype.viewDetail = function (row) {
        console.log(row);
        this.order = row;
    };
    DashboardComponent.prototype.fetchTurnOver = function (restId, year) {
        var _this = this;
        var param = {
            'rest_id': restId,
            'year_no': year
        };
        this.turnOverService.getTurnover(param).then(function (response) {
            console.log('getTurnover abc', response);
            _this.turnOvers = response.turnover;
            var currentMonth = (new Date()).getMonth();
            console.log('currentMonth ', _this.turnOvers[currentMonth]);
            _this.calcTurnOverByMonth(_this.turnOvers[currentMonth]);
        }, function (error) {
            console.log(error);
        });
    };
    DashboardComponent.prototype.calcTurnOverByMonth = function (turnOversByMonth) {
        console.log('this.turnOvers ', this.turnOvers);
        if (turnOversByMonth.length == 0) {
            this.totalTurnOverByMonth = 0;
        }
        else {
            for (var j = 0; j < turnOversByMonth.length; j++) {
                this.totalTurnOverByMonth += Number(turnOversByMonth[j].price);
            }
        }
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    DashboardComponent.prototype.processResult = function () {
        this.breadServ.set({
            header: "Dashboard",
            description: 'This is our Home page',
            display: true,
            levels: [
                {
                    icon: 'dashboard',
                    link: ['/'],
                    title: 'Dashboard',
                    state: 'app'
                }
            ]
        });
    };
    DashboardComponent.prototype.getDeliveryMethod = function () {
        var _this = this;
        this.comCodeServ.getCode({ 'cd_group': '0' }).then(function (response) {
            console.log('response ', response);
            var codes = response.code;
            for (var i = 0; i < codes.length; i++) {
                if (_this.order.delivery_type == +codes[i].cd) {
                    _this.deliveryMethod = codes[i].cd_name;
                    console.log(_this.deliveryMethod);
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    DashboardComponent.prototype.doUpdateOrder = function (orderStatus) {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'order_id': this.order.id,
            'status': orderStatus
        };
        this.orderServ.updateOrder(params).then(function (response) {
            _this.notifyServ.success('Order has been updated');
            _this.getOrderRequest(false);
            //this.goBack();
        }, function (error) {
            console.log(error);
        });
    };
    DashboardComponent.prototype.getOrderRequest = function (isSelect) {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId
        };
        this.orderServ.getOrderList(params).then(function (response) {
            _this.orderList = response.orders;
            _this.requestOrderList = _this.orderList.filter(function (x) { return x.order_sts == 0; });
            _this.requestOrderDelivery = _this.requestOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.requestOrderPickup = _this.requestOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.verifiedOrderList = _this.orderList.filter(function (x) { return x.order_sts == 1; });
            _this.verifiedOrderDelivery = _this.verifiedOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.verifiedOrderPickup = _this.verifiedOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.pendingOrderList = _this.orderList.filter(function (x) { return x.order_sts == 99; });
            _this.pendingOrderDelivery = _this.pendingOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.pendingOrderPickup = _this.pendingOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.deliveryOrderList = _this.orderList.filter(function (x) { return x.order_sts == 2; });
            _this.deliveryOrderDelivery = _this.deliveryOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.deliveryOrderPickup = _this.deliveryOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.requestOrderTempList = _this.requestOrderList;
            _this.verifiedOrderTempList = _this.verifiedOrderList;
            _this.pendingOrderTempList = _this.pendingOrderList;
            _this.deliveryOrderTempList = _this.deliveryOrderList;
            // don't update order detail when auto loop
            if (isSelect == true)
                _this.order = _this.requestOrderList[0];
            _this.getDeliveryMethod();
        }, function (error) {
            console.log(error);
        });
    };
    DashboardComponent.prototype.doSearch = function () {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId
        };
        this.contactServ.getContactList({ 'rest_id': this.restId }).then(function (response) {
            _this.contactCount = response.contacts.length;
        }, function (error) {
            console.log(error);
        });
        this.foodItemServ.getFoodItems(this.restId, null, null).then(function (response) {
            _this.optionFoodItemCount = response.food_items.length;
        }, function (error) {
            console.log(error);
        });
        this.categoryServ.getCategories(this.restId, null).then(function (response) {
            _this.categoryCount = response.categories.length;
        }, function (error) {
            console.log(error);
        });
        this.tableService.getTables(this.restId, null, null).then(function (response) {
            _this.tableCount = response.tables.length;
        }, function (error) {
            console.log(error);
        });
    };
    DashboardComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    DashboardComponent.prototype.doModelDisplay = function (orderType, deliveryType) {
        if (orderType == 0) {
            if (deliveryType == 0) {
                this.requestOrderList = this.requestOrderTempList;
            }
            else if (deliveryType == 1) {
                this.requestOrderList = this.requestOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.requestOrderList = this.requestOrderPickup;
            }
        }
        else if (orderType == 1) {
            if (deliveryType == 0) {
                this.verifiedOrderList = this.verifiedOrderTempList;
            }
            else if (deliveryType == 1) {
                this.verifiedOrderList = this.verifiedOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.verifiedOrderList = this.verifiedOrderPickup;
            }
        }
        else if (orderType == 2) {
            if (deliveryType == 0) {
                this.deliveryOrderList = this.deliveryOrderTempList;
            }
            else if (deliveryType == 1) {
                this.deliveryOrderList = this.deliveryOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.deliveryOrderList = this.deliveryOrderPickup;
            }
        }
        else if (orderType == 99) {
            if (deliveryType == 0) {
                this.pendingOrderList = this.pendingOrderTempList;
            }
            else if (deliveryType == 1) {
                this.pendingOrderList = this.pendingOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.pendingOrderList = this.pendingOrderPickup;
            }
        }
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'dashboard',
            templateUrl: utils_1.default.getView('app/components/dashboard/dashboard.component.html'),
            styleUrls: [utils_1.default.getView('app/components/dashboard/dashboard.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [contact_service_1.ContactService,
            foodItem_service_1.FoodItemService,
            category_service_1.CategoryService,
            breadcrumb_service_1.BreadcrumbService,
            user_service_1.UserService,
            order_service_1.OrderService,
            table_service_1.TableService,
            comCode_service_1.ComCodeService,
            notification_service_1.NotificationService,
            turnover_service_1.TurnOverService,
            storage_service_1.StorageService])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHlDQUFvQztBQUNwQyxzRUFBb0U7QUFDcEUsNkRBQXVFO0FBQ3ZFLDBEQUF3RDtBQUN4RCxnRUFBOEQ7QUFDOUQsNERBQTBEO0FBQzFELGdFQUE4RDtBQUM5RCxrRUFBZ0U7QUFDaEUsa0VBQWdFO0FBQ2hFLDREQUEwRDtBQUMxRCxrRUFBOEQ7QUFDOUQsOENBQTJDO0FBQzNDLDBFQUF3RTtBQUN4RSxnRUFBOEQ7QUFROUQ7SUE4QkUsNEJBQ1UsV0FBMkIsRUFDM0IsWUFBNkIsRUFDN0IsWUFBNkIsRUFDN0IsU0FBNEIsRUFDNUIsUUFBcUIsRUFDckIsU0FBdUIsRUFDdkIsWUFBMEIsRUFDekIsV0FBMkIsRUFDNUIsVUFBK0IsRUFDL0IsZUFBZ0MsRUFDaEMsV0FBMkI7UUFWM0IsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzNCLGlCQUFZLEdBQVosWUFBWSxDQUFpQjtRQUM3QixpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFDN0IsY0FBUyxHQUFULFNBQVMsQ0FBbUI7UUFDNUIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixjQUFTLEdBQVQsU0FBUyxDQUFjO1FBQ3ZCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQ3pCLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUM1QixlQUFVLEdBQVYsVUFBVSxDQUFxQjtRQUMvQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBZjdCLFVBQUssR0FBTyxFQUFFLENBQUM7UUFHZix5QkFBb0IsR0FBUSxDQUFDLENBQUM7SUFnQnJDLENBQUM7SUFFRixxQ0FBUSxHQUFSO1FBQUEsaUJBOEJDO1FBNUJDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFDLENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDcEIsSUFBSSxNQUFNLEdBQUc7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3ZCLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFdkMsZUFBZTtRQUNmLElBQUksQ0FBQyxZQUFZLEdBQUcsdUJBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2FBQzFDLEdBQUcsQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsR0FBQyxDQUFDLEVBQUgsQ0FBRyxDQUFDO2FBQ2YsU0FBUyxDQUFDLFVBQUMsQ0FBQztZQUNYLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxHQUFHLEVBRVosQ0FBQztJQUNKLENBQUM7SUFFTyx1Q0FBVSxHQUFsQixVQUFtQixHQUFHO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7SUFDbkIsQ0FBQztJQUVRLDBDQUFhLEdBQXJCLFVBQXNCLE1BQU0sRUFBRSxJQUFJO1FBQWxDLGlCQWVDO1FBZEUsSUFBSSxLQUFLLEdBQUc7WUFDVixTQUFTLEVBQUUsTUFBTTtZQUNqQixTQUFTLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBQ0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUNwQyxVQUFBLFFBQVE7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZDLEtBQUksQ0FBQyxTQUFTLEdBQUksUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUNuQyxJQUFJLFlBQVksR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBQyxLQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDM0QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUN2RCxDQUFDLEVBQ0gsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFUyxnREFBbUIsR0FBM0IsVUFBNEIsZ0JBQWdCO1FBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzdDLEVBQUUsQ0FBQSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxDQUFDLENBQUM7UUFDbEMsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQy9DLENBQUM7Z0JBQ0csSUFBSSxDQUFDLG9CQUFvQixJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuRSxDQUFDO1FBQ0gsQ0FBQztJQUNMLENBQUM7SUFFSCx3Q0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRU8sMENBQWEsR0FBckI7UUFFRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztZQUNqQixNQUFNLEVBQUUsV0FBVztZQUNuQixXQUFXLEVBQUUsdUJBQXVCO1lBQ3BDLE9BQU8sRUFBRSxJQUFJO1lBQ2IsTUFBTSxFQUFFO2dCQUNOO29CQUNFLElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUM7b0JBQ1gsS0FBSyxFQUFFLFdBQVc7b0JBQ2xCLEtBQUssRUFBRSxLQUFLO2lCQUNiO2FBQ0Y7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUEsOENBQWlCLEdBQWpCO1FBQUEsaUJBY0E7UUFiQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDM0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbkMsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUMxQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDdEMsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRyxDQUFDLENBQUMsQ0FBQztvQkFDOUMsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO29CQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUUsQ0FBQztnQkFDbkMsQ0FBQztZQUNGLENBQUM7UUFFRixDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFTywwQ0FBYSxHQUFyQixVQUFzQixXQUFXO1FBQWpDLGlCQWNBO1FBWkEsSUFBSSxNQUFNLEdBQUc7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QixRQUFRLEVBQUUsV0FBVztTQUNyQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUMvQyxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ2xELEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsZ0JBQWdCO1FBQ2pCLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVRLDRDQUFlLEdBQXZCLFVBQXdCLFFBQVE7UUFBaEMsaUJBK0JDO1FBOUJDLElBQUksTUFBTSxHQUFHO1lBQ1gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN2QixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUUvQyxLQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDakMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQWhCLENBQWdCLENBQUMsQ0FBQztZQUNyRSxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUM7WUFDcEYsS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsYUFBYSxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1lBQ2xGLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFoQixDQUFnQixDQUFDLENBQUM7WUFDdEUsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsYUFBYSxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1lBQ3RGLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGFBQWEsSUFBSSxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztZQUNwRixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBakIsQ0FBaUIsQ0FBQyxDQUFDO1lBQ3RFLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGFBQWEsSUFBSSxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztZQUNwRixLQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUM7WUFDbEYsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQWhCLENBQWdCLENBQUMsQ0FBQztZQUN0RSxLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUM7WUFDdEYsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsYUFBYSxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1lBQ3BGLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDbEQsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQztZQUNwRCxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDO1lBQ2xELEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUE7WUFFbkQsMkNBQTJDO1lBQzNDLEVBQUUsQ0FBQSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUM7Z0JBQUMsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUQsS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDNUIsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8scUNBQVEsR0FBaEI7UUFBQSxpQkFpQ0M7UUEvQkUsSUFBSSxNQUFNLEdBQUc7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3ZCLENBQUM7UUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ3ZFLEtBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDL0MsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBRW5FLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztRQUN4RCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNwQixDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUM5RCxLQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO1FBQ2xELENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUM1QyxVQUFBLFFBQVE7WUFDUixLQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBRXpDLENBQUMsRUFDVCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLHVDQUFVLEdBQWxCLFVBQW1CLEdBQUc7UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVPLDJDQUFjLEdBQXRCLFVBQXVCLFNBQVMsRUFBRSxZQUFZO1FBQzVDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25CLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1lBQ3BELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDcEQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUNsRCxDQUFDO1FBQ0gsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQixFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztZQUN0RCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDO1lBQ3RELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDcEQsQ0FBQztRQUNILENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUM7WUFDdEQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztZQUN0RCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3BELENBQUM7UUFDSCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNCLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1lBQ3BELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDcEQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUNsRCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFqUlUsa0JBQWtCO1FBUDlCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxtREFBbUQsQ0FBQztZQUMvRSxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7WUFDOUUsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBRSxxQkFBcUIsRUFBRSxFQUFFLEVBQUU7U0FDcEMsQ0FBQzt5Q0FnQ3VCLGdDQUFjO1lBQ2Isa0NBQWU7WUFDZixrQ0FBZTtZQUNsQixzQ0FBaUI7WUFDbEIsMEJBQVc7WUFDViw0QkFBWTtZQUNULDRCQUFZO1lBQ1osZ0NBQWM7WUFDaEIsMENBQW1CO1lBQ2Qsa0NBQWU7WUFDbkIsZ0NBQWM7T0F6QzFCLGtCQUFrQixDQWtSOUI7SUFBRCx5QkFBQztDQWxSRCxBQWtSQyxJQUFBO0FBbFJZLGdEQUFrQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlLCByb3V0ZXJUcmFuc2l0aW9uIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZVwiO1xuaW1wb3J0IHsgT3JkZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29udGFjdFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY29udGFjdC5zZXJ2aWNlJztcbmltcG9ydCB7IEZvb2RJdGVtU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9mb29kSXRlbS5zZXJ2aWNlJztcbmltcG9ydCB7IENhdGVnb3J5U2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9jYXRlZ29yeS5zZXJ2aWNlJztcbmltcG9ydCB7IFRhYmxlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy90YWJsZS5zZXJ2aWNlJztcbmltcG9ydCB7VHVybk92ZXJTZXJ2aWNlfSBmcm9tICdhcHAvc2VydmljZXMvdHVybm92ZXIuc2VydmljZSc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzL09ic2VydmFibGVcIjtcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tQ29kZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY29tQ29kZS5zZXJ2aWNlJztcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Rhc2hib2FyZCcsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5odG1sJyksXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsgJ1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJyB9XG59KVxuZXhwb3J0IGNsYXNzIERhc2hib2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHJpdmF0ZSByZXN0SWQ7XG4gIHByaXZhdGUgdXNlcklkO1xuICBwcml2YXRlIGNvbnRhY3RDb3VudDtcbiAgcHJpdmF0ZSBvcHRpb25Gb29kSXRlbUNvdW50O1xuICBwcml2YXRlIGNhdGVnb3J5Q291bnQ7XG4gIHByaXZhdGUgdGFibGVDb3VudDtcbiAgcHJpdmF0ZSBvcmRlckxpc3Q6IGFueTtcbiAgcHJpdmF0ZSByZXF1ZXN0T3JkZXJMaXN0OiBhbnk7XG4gIHByaXZhdGUgcmVxdWVzdE9yZGVyVGVtcExpc3Q6IGFueTtcbiAgcHJpdmF0ZSByZXF1ZXN0T3JkZXJEZWxpdmVyeTogYW55O1xuICBwcml2YXRlIHJlcXVlc3RPcmRlclBpY2t1cDogYW55O1xuICBwcml2YXRlIHZlcmlmaWVkT3JkZXJMaXN0OiBhbnk7XG4gIHByaXZhdGUgdmVyaWZpZWRPcmRlclRlbXBMaXN0OiBhbnk7XG4gIHByaXZhdGUgdmVyaWZpZWRPcmRlckRlbGl2ZXJ5OiBhbnk7XG4gIHByaXZhdGUgdmVyaWZpZWRPcmRlclBpY2t1cDogYW55O1xuICBwcml2YXRlIHBlbmRpbmdPcmRlckxpc3Q6IGFueTtcbiAgcHJpdmF0ZSBwZW5kaW5nT3JkZXJUZW1wTGlzdDogYW55O1xuICBwcml2YXRlIHBlbmRpbmdPcmRlckRlbGl2ZXJ5OiBhbnk7XG4gIHByaXZhdGUgcGVuZGluZ09yZGVyUGlja3VwOiBhbnk7XG4gIHByaXZhdGUgZGVsaXZlcnlPcmRlckxpc3Q6IGFueTtcbiAgcHJpdmF0ZSBkZWxpdmVyeU9yZGVyVGVtcExpc3Q6IGFueTtcbiAgcHJpdmF0ZSBkZWxpdmVyeU9yZGVyRGVsaXZlcnk6IGFueTtcbiAgcHJpdmF0ZSBkZWxpdmVyeU9yZGVyUGlja3VwOiBhbnk7XG4gIHByaXZhdGUgc3Vic2NyaXB0aW9uOmFueTtcbiAgcHJpdmF0ZSBvcmRlcjphbnkgPSB7fTtcbiAgcHJpdmF0ZSB0dXJuT3ZlcnM6YW55W107XG4gIHByaXZhdGUgZGVsaXZlcnlNZXRob2Q6IGFueTtcbiAgcHJpdmF0ZSB0b3RhbFR1cm5PdmVyQnlNb250aDogYW55ID0gMDtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb250YWN0U2VydjogQ29udGFjdFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmb29kSXRlbVNlcnY6IEZvb2RJdGVtU2VydmljZSxcbiAgICBwcml2YXRlIGNhdGVnb3J5U2VydjogQ2F0ZWdvcnlTZXJ2aWNlLFxuICAgIHByaXZhdGUgYnJlYWRTZXJ2OiBCcmVhZGNydW1iU2VydmljZSxcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIG9yZGVyU2VydjogT3JkZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgdGFibGVTZXJ2aWNlOiBUYWJsZVNlcnZpY2UsXG4gICAgIHByaXZhdGUgY29tQ29kZVNlcnY6IENvbUNvZGVTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgICBwcml2YXRlIHR1cm5PdmVyU2VydmljZTogVHVybk92ZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlXG5cbiAgKSB7XG4gICBcbiAgIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgIFxuICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVzZXJJZCA9IHVzZXJLZXkudXNlcl9pbmZvLmlkO1xuICAgICAgdGhpcy5yZXN0SWQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIH1cblxuICAgIHRoaXMuZG9TZWFyY2goKTtcbiAgICB0aGlzLmdldE9yZGVyUmVxdWVzdCh0cnVlKTtcbiAgICB0aGlzLnByb2Nlc3NSZXN1bHQoKTtcbiAgICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICd1c2VyX2lkJzogdGhpcy51c2VySWQsXG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkXG4gICAgfTtcblxuICAgIHRoaXMuZmV0Y2hUdXJuT3Zlcih0aGlzLnJlc3RJZCwgMjAxNyk7XG4gIFxuICAgLy8gbG9uZyBwb2xsaW5nXG4gICB0aGlzLnN1YnNjcmlwdGlvbiA9IE9ic2VydmFibGUuaW50ZXJ2YWwoMzAwMDApXG4gICAgICAubWFwKCh4KSA9PiB4KzEpXG4gICAgICAuc3Vic2NyaWJlKCh4KSA9PiB7XG4gICAgICAgIHRoaXMuZ2V0T3JkZXJSZXF1ZXN0KGZhbHNlKTtcbiAgICB9KTtcblxuICAgIHRoaXMub3JkZXIgPSB7XG5cbiAgICB9O1xuICB9XG5cbiAgcHJpdmF0ZSB2aWV3RGV0YWlsKHJvdykge1xuICAgIGNvbnNvbGUubG9nKHJvdyk7XG4gICAgdGhpcy5vcmRlciA9IHJvdztcbiAgfVxuXG4gICBwcml2YXRlIGZldGNoVHVybk92ZXIocmVzdElkLCB5ZWFyKSB7XG4gICAgICBsZXQgcGFyYW0gPSB7XG4gICAgICAgICdyZXN0X2lkJzogcmVzdElkLFxuICAgICAgICAneWVhcl9ubyc6IHllYXJcbiAgICAgIH07XG4gIFx0XHR0aGlzLnR1cm5PdmVyU2VydmljZS5nZXRUdXJub3ZlcihwYXJhbSkudGhlbihcbiAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7XG4gICAgICAgICAgXHRjb25zb2xlLmxvZygnZ2V0VHVybm92ZXIgYWJjJyxyZXNwb25zZSk7XG4gICAgICAgICAgICB0aGlzLnR1cm5PdmVycyAgPSByZXNwb25zZS50dXJub3ZlcjtcbiAgICAgICAgICAgICBsZXQgY3VycmVudE1vbnRoID0gKG5ldyBEYXRlKCkpLmdldE1vbnRoKCk7XG4gICAgICAgICAgICAgY29uc29sZS5sb2coJ2N1cnJlbnRNb250aCAnLHRoaXMudHVybk92ZXJzW2N1cnJlbnRNb250aF0pO1xuICAgICAgICAgICAgdGhpcy5jYWxjVHVybk92ZXJCeU1vbnRoKHRoaXMudHVybk92ZXJzW2N1cnJlbnRNb250aF0pO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gICAgICAgIH0pO1xuICBcdH1cblxuICAgICBwcml2YXRlIGNhbGNUdXJuT3ZlckJ5TW9udGgodHVybk92ZXJzQnlNb250aCkge1xuICAgICAgY29uc29sZS5sb2coJ3RoaXMudHVybk92ZXJzICcsIHRoaXMudHVybk92ZXJzKTtcbiAgICAgICAgaWYodHVybk92ZXJzQnlNb250aC5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgdGhpcy50b3RhbFR1cm5PdmVyQnlNb250aCA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgZm9yKHZhciBqID0gMDsgaiA8IHR1cm5PdmVyc0J5TW9udGgubGVuZ3RoOyBqKyspXG4gICAgICAgICAge1xuICAgICAgICAgICAgICB0aGlzLnRvdGFsVHVybk92ZXJCeU1vbnRoICs9IE51bWJlcih0dXJuT3ZlcnNCeU1vbnRoW2pdLnByaWNlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgbmdPbkRlc3Ryb3koKXtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzUmVzdWx0KCkge1xuXG4gICAgdGhpcy5icmVhZFNlcnYuc2V0KHtcbiAgICAgIGhlYWRlcjogXCJEYXNoYm9hcmRcIixcbiAgICAgIGRlc2NyaXB0aW9uOiAnVGhpcyBpcyBvdXIgSG9tZSBwYWdlJyxcbiAgICAgIGRpc3BsYXk6IHRydWUsXG4gICAgICBsZXZlbHM6IFtcbiAgICAgICAge1xuICAgICAgICAgIGljb246ICdkYXNoYm9hcmQnLFxuICAgICAgICAgIGxpbms6IFsnLyddLFxuICAgICAgICAgIHRpdGxlOiAnRGFzaGJvYXJkJyxcbiAgICAgICAgICBzdGF0ZTogJ2FwcCdcbiAgICAgICAgfVxuICAgICAgXVxuICAgIH0pO1xuICB9XG5cbiAgIGdldERlbGl2ZXJ5TWV0aG9kKCkge1xuICAgIHRoaXMuY29tQ29kZVNlcnYuZ2V0Q29kZSh7ICdjZF9ncm91cCc6ICcwJyB9KS50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0Y29uc29sZS5sb2coJ3Jlc3BvbnNlICcsIHJlc3BvbnNlKTsgXG5cdFx0XHRcdGxldCBjb2RlcyA9IHJlc3BvbnNlLmNvZGU7XG5cdFx0XHRcdGZvcihsZXQgaSA9IDA7IGkgPCBjb2Rlcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0XHRcdGlmKHRoaXMub3JkZXIuZGVsaXZlcnlfdHlwZSA9PSArY29kZXNbaV0uY2QgKSB7XG5cdFx0XHRcdFx0XHR0aGlzLmRlbGl2ZXJ5TWV0aG9kID0gY29kZXNbaV0uY2RfbmFtZTtcblx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKHRoaXMuZGVsaXZlcnlNZXRob2QgKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XG5cdFx0XHR9LCBlcnJvciA9PiB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKGVycm9yKTtcblx0XHRcdH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb1VwZGF0ZU9yZGVyKG9yZGVyU3RhdHVzKSB7XG5cblx0XHR2YXIgcGFyYW1zID0ge1xuXHRcdFx0J3VzZXJfaWQnOiB0aGlzLnVzZXJJZCxcblx0XHRcdCdvcmRlcl9pZCc6IHRoaXMub3JkZXIuaWQsXG5cdFx0XHQnc3RhdHVzJzogb3JkZXJTdGF0dXNcblx0XHR9O1xuXHRcdHRoaXMub3JkZXJTZXJ2LnVwZGF0ZU9yZGVyKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHR0aGlzLm5vdGlmeVNlcnYuc3VjY2VzcygnT3JkZXIgaGFzIGJlZW4gdXBkYXRlZCcpO1xuXHRcdCB0aGlzLmdldE9yZGVyUmVxdWVzdChmYWxzZSk7XG5cdFx0XHQvL3RoaXMuZ29CYWNrKCk7XG5cdFx0fSwgZXJyb3IgPT4ge1xuXHRcdFx0Y29uc29sZS5sb2coZXJyb3IpO1xuXHRcdH0pO1xuXHR9XG5cbiAgcHJpdmF0ZSBnZXRPcmRlclJlcXVlc3QoaXNTZWxlY3QpIHtcbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgJ3VzZXJfaWQnOiB0aGlzLnVzZXJJZCxcbiAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWRcbiAgICB9O1xuICAgIHRoaXMub3JkZXJTZXJ2LmdldE9yZGVyTGlzdChwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgXG4gICAgICB0aGlzLm9yZGVyTGlzdCA9IHJlc3BvbnNlLm9yZGVycztcbiAgICAgIHRoaXMucmVxdWVzdE9yZGVyTGlzdCA9IHRoaXMub3JkZXJMaXN0LmZpbHRlcih4ID0+IHgub3JkZXJfc3RzID09IDApO1xuICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJEZWxpdmVyeSA9IHRoaXMucmVxdWVzdE9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMCk7XG4gICAgICB0aGlzLnJlcXVlc3RPcmRlclBpY2t1cCA9IHRoaXMucmVxdWVzdE9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMSk7XG4gICAgICB0aGlzLnZlcmlmaWVkT3JkZXJMaXN0ID0gdGhpcy5vcmRlckxpc3QuZmlsdGVyKHggPT4geC5vcmRlcl9zdHMgPT0gMSk7XG4gICAgICB0aGlzLnZlcmlmaWVkT3JkZXJEZWxpdmVyeSA9IHRoaXMudmVyaWZpZWRPcmRlckxpc3QuZmlsdGVyKHggPT4geC5kZWxpdmVyeV90eXBlID09IDApO1xuICAgICAgdGhpcy52ZXJpZmllZE9yZGVyUGlja3VwID0gdGhpcy52ZXJpZmllZE9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMSk7XG4gICAgICB0aGlzLnBlbmRpbmdPcmRlckxpc3QgPSB0aGlzLm9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4Lm9yZGVyX3N0cyA9PSA5OSk7XG4gICAgICB0aGlzLnBlbmRpbmdPcmRlckRlbGl2ZXJ5ID0gdGhpcy5wZW5kaW5nT3JkZXJMaXN0LmZpbHRlcih4ID0+IHguZGVsaXZlcnlfdHlwZSA9PSAwKTtcbiAgICAgIHRoaXMucGVuZGluZ09yZGVyUGlja3VwID0gdGhpcy5wZW5kaW5nT3JkZXJMaXN0LmZpbHRlcih4ID0+IHguZGVsaXZlcnlfdHlwZSA9PSAxKTtcbiAgICAgIHRoaXMuZGVsaXZlcnlPcmRlckxpc3QgPSB0aGlzLm9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4Lm9yZGVyX3N0cyA9PSAyKTtcbiAgICAgIHRoaXMuZGVsaXZlcnlPcmRlckRlbGl2ZXJ5ID0gdGhpcy5kZWxpdmVyeU9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMCk7XG4gICAgICB0aGlzLmRlbGl2ZXJ5T3JkZXJQaWNrdXAgPSB0aGlzLmRlbGl2ZXJ5T3JkZXJMaXN0LmZpbHRlcih4ID0+IHguZGVsaXZlcnlfdHlwZSA9PSAxKTtcbiAgICAgIHRoaXMucmVxdWVzdE9yZGVyVGVtcExpc3QgPSB0aGlzLnJlcXVlc3RPcmRlckxpc3Q7XG4gICAgICB0aGlzLnZlcmlmaWVkT3JkZXJUZW1wTGlzdCA9IHRoaXMudmVyaWZpZWRPcmRlckxpc3Q7XG4gICAgICB0aGlzLnBlbmRpbmdPcmRlclRlbXBMaXN0ID0gdGhpcy5wZW5kaW5nT3JkZXJMaXN0O1xuICAgICAgdGhpcy5kZWxpdmVyeU9yZGVyVGVtcExpc3QgPSB0aGlzLmRlbGl2ZXJ5T3JkZXJMaXN0XG4gICAgICBcbiAgICAgIC8vIGRvbid0IHVwZGF0ZSBvcmRlciBkZXRhaWwgd2hlbiBhdXRvIGxvb3BcbiAgICAgIGlmKGlzU2VsZWN0ID09IHRydWUpIHRoaXMub3JkZXIgPSB0aGlzLnJlcXVlc3RPcmRlckxpc3RbMF07XG4gICAgICAgdGhpcy5nZXREZWxpdmVyeU1ldGhvZCgpO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZWFyY2goKSB7XG5cbiAgICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICd1c2VyX2lkJzogdGhpcy51c2VySWQsXG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkXG4gICAgfTtcblxuICAgIHRoaXMuY29udGFjdFNlcnYuZ2V0Q29udGFjdExpc3QoeyAncmVzdF9pZCc6IHRoaXMucmVzdElkIH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgdGhpcy5jb250YWN0Q291bnQgPSByZXNwb25zZS5jb250YWN0cy5sZW5ndGg7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5mb29kSXRlbVNlcnYuZ2V0Rm9vZEl0ZW1zKHRoaXMucmVzdElkLCBudWxsLCBudWxsKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIFxuICAgICAgdGhpcy5vcHRpb25Gb29kSXRlbUNvdW50ID0gcmVzcG9uc2UuZm9vZF9pdGVtcy5sZW5ndGg7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpXG4gICAgfSk7XG5cbiAgICB0aGlzLmNhdGVnb3J5U2Vydi5nZXRDYXRlZ29yaWVzKHRoaXMucmVzdElkLCBudWxsKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMuY2F0ZWdvcnlDb3VudCA9IHJlc3BvbnNlLmNhdGVnb3JpZXMubGVuZ3RoO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKVxuICAgIH0pO1xuXG4gICAgXHR0aGlzLnRhYmxlU2VydmljZS5nZXRUYWJsZXModGhpcy5yZXN0SWQsbnVsbCxudWxsKS50aGVuKFxuICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy50YWJsZUNvdW50ID0gcmVzcG9uc2UudGFibGVzLmxlbmd0aDtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gICAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZXRTY29wZShvYmopIHtcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2LnNldFNjb3BlKG9iaik7XG4gIH1cblxuICBwcml2YXRlIGRvTW9kZWxEaXNwbGF5KG9yZGVyVHlwZSwgZGVsaXZlcnlUeXBlKSB7XG4gICAgaWYgKG9yZGVyVHlwZSA9PSAwKSB7XG4gICAgICBpZiAoZGVsaXZlcnlUeXBlID09IDApIHtcbiAgICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJUZW1wTGlzdDtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDEpIHtcbiAgICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJEZWxpdmVyeTtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDIpIHtcbiAgICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJQaWNrdXA7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChvcmRlclR5cGUgPT0gMSkge1xuICAgICAgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAwKSB7XG4gICAgICAgIHRoaXMudmVyaWZpZWRPcmRlckxpc3QgPSB0aGlzLnZlcmlmaWVkT3JkZXJUZW1wTGlzdDtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDEpIHtcbiAgICAgICAgdGhpcy52ZXJpZmllZE9yZGVyTGlzdCA9IHRoaXMudmVyaWZpZWRPcmRlckRlbGl2ZXJ5O1xuICAgICAgfSBlbHNlIGlmIChkZWxpdmVyeVR5cGUgPT0gMikge1xuICAgICAgICB0aGlzLnZlcmlmaWVkT3JkZXJMaXN0ID0gdGhpcy52ZXJpZmllZE9yZGVyUGlja3VwO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAob3JkZXJUeXBlID09IDIpIHtcbiAgICAgIGlmIChkZWxpdmVyeVR5cGUgPT0gMCkge1xuICAgICAgICB0aGlzLmRlbGl2ZXJ5T3JkZXJMaXN0ID0gdGhpcy5kZWxpdmVyeU9yZGVyVGVtcExpc3Q7XG4gICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAxKSB7XG4gICAgICAgIHRoaXMuZGVsaXZlcnlPcmRlckxpc3QgPSB0aGlzLmRlbGl2ZXJ5T3JkZXJEZWxpdmVyeTtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDIpIHtcbiAgICAgICAgdGhpcy5kZWxpdmVyeU9yZGVyTGlzdCA9IHRoaXMuZGVsaXZlcnlPcmRlclBpY2t1cDtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKG9yZGVyVHlwZSA9PSA5OSkge1xuICAgICAgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAwKSB7XG4gICAgICAgIHRoaXMucGVuZGluZ09yZGVyTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyVGVtcExpc3Q7XG4gICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAxKSB7XG4gICAgICAgIHRoaXMucGVuZGluZ09yZGVyTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyRGVsaXZlcnk7XG4gICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAyKSB7XG4gICAgICAgIHRoaXMucGVuZGluZ09yZGVyTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyUGlja3VwO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19
