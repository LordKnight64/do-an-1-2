"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var searchs_service_1 = require("app/services/searchs.service");
var SearchItemsComponent = (function () {
    function SearchItemsComponent(searchServ, userServ, storageServ, notifyServ) {
        this.searchServ = searchServ;
        this.userServ = userServ;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    SearchItemsComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        this.doSearch();
    };
    SearchItemsComponent.prototype.doSearch = function () {
        var _this = this;
        var params = {};
        this.searchServ.getSearchList(params).then(function (response) {
            var searchWithoutLogin = response.searchWithoutLogin;
            _this.newsList = response.search;
            for (var i = 0; i < searchWithoutLogin.length; i++) {
                var searchItem = searchWithoutLogin[i];
                searchItem.first_name = "anonymous";
                searchItem.last_name = "anonymous";
                _this.newsList.push(searchItem);
            }
            _this.totalItems = _this.newsList.length;
            _this.pageChanged({ page: _this.currentPage, itemsPerPage: _this.itemsPerPage });
        }, function (error) {
            console.log(error);
        });
    };
    SearchItemsComponent.prototype.pageChanged = function (event) {
        var start = (event.page - 1) * event.itemsPerPage;
        var end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
        this.data = this.newsList.slice(start, end);
    };
    SearchItemsComponent.prototype.doDeleteSearch = function (obj) {
        var id = obj.id;
        var params = {
            'id': id
        };
        var me = this;
        this.searchServ.deleteSearch(params).then(function (response) {
            if (response.return_cd == 0) {
                me.notifyServ.success('Search Items has been deleted');
                me.doSearch();
            }
        }, function (error) {
            console.log(error);
        });
    };
    SearchItemsComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    SearchItemsComponent = __decorate([
        core_1.Component({
            selector: 'table-list',
            templateUrl: utils_1.default.getView('app/components/search-items/search-items.component.html'),
            styleUrls: [utils_1.default.getView('app/components/search-items/search-items.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [searchs_service_1.SearchsService,
            user_service_1.UserService,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], SearchItemsComponent);
    return SearchItemsComponent;
}());
exports.SearchItemsComponent = SearchItemsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3NlYXJjaC1pdGVtcy9zZWFyY2gtaXRlbXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHlDQUFvQztBQUVwQyw2REFBdUU7QUFDdkUsMERBQXdEO0FBQ3hELGdFQUE4RDtBQUM5RCwwRUFBd0U7QUFDeEUsZ0VBQThEO0FBUzlEO0lBVUUsOEJBQ1UsVUFBMEIsRUFDMUIsUUFBcUIsRUFDckIsV0FBMkIsRUFDM0IsVUFBK0I7UUFIL0IsZUFBVSxHQUFWLFVBQVUsQ0FBZ0I7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDM0IsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFUakMsZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFFeEIsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFXLEVBQUUsQ0FBQztJQU96QixDQUFDO0lBRUwsdUNBQVEsR0FBUjtRQUVFLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUVELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRU8sdUNBQVEsR0FBaEI7UUFBQSxpQkF1QkM7UUF0QkMsSUFBSSxNQUFNLEdBQUcsRUFDWixDQUFDO1FBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUVqRCxJQUFJLGtCQUFrQixHQUFVLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQztZQUU1RCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDaEMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDbEQsSUFBSSxVQUFVLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRXZDLFVBQVUsQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDO2dCQUNwQyxVQUFVLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztnQkFFbkMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakMsQ0FBQztZQUdELEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDdEMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFJLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztRQUNqRixDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSwwQ0FBVyxHQUFsQixVQUFtQixLQUFVO1FBQzNCLElBQUksS0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDO1FBQ2xELElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1FBQ3hGLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFTyw2Q0FBYyxHQUF0QixVQUF1QixHQUFHO1FBRXhCLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLENBQUM7UUFDaEIsSUFBSSxNQUFNLEdBQUc7WUFDWCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7UUFDRixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ2hELEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsK0JBQStCLENBQUMsQ0FBQztnQkFDdkQsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLENBQUM7UUFDSCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyx5Q0FBVSxHQUFsQixVQUFtQixHQUFHO1FBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUE3RVUsb0JBQW9CO1FBUC9CLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx5REFBeUQsQ0FBQztZQUNyRixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHdEQUF3RCxDQUFDLENBQUM7WUFDcEYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0FZc0IsZ0NBQWM7WUFDaEIsMEJBQVc7WUFDUixnQ0FBYztZQUNmLDBDQUFtQjtPQWQ5QixvQkFBb0IsQ0ErRWhDO0lBQUQsMkJBQUM7Q0EvRUQsQUErRUMsSUFBQTtBQS9FWSxvREFBb0IiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvc2VhcmNoLWl0ZW1zL3NlYXJjaC1pdGVtcy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSwgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgU2VhcmNoc1NlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc2VhcmNocy5zZXJ2aWNlJztcblxuIEBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3RhYmxlLWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvc2VhcmNoLWl0ZW1zL3NlYXJjaC1pdGVtcy5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9zZWFyY2gtaXRlbXMvc2VhcmNoLWl0ZW1zLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ31cbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoSXRlbXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgbmV3c0xpc3Q6IGFueTtcbiAgcHJpdmF0ZSBkYXRhOiBhbnk7XG4gIHByaXZhdGUgY3VycmVudFBhZ2U6IG51bWJlciA9IDE7XG4gIHByaXZhdGUgdG90YWxJdGVtczogbnVtYmVyO1xuICBwcml2YXRlIGl0ZW1zUGVyUGFnZTogbnVtYmVyID0gMTA7XG4gIHByaXZhdGUgbWF4U2l6ZTogbnVtYmVyID0gMTA7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBzZWFyY2hTZXJ2OiBTZWFyY2hzU2VydmljZSxcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2OiBTdG9yYWdlU2VydmljZSxcbiAgICBwcml2YXRlIG5vdGlmeVNlcnY6IE5vdGlmaWNhdGlvblNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcblxuICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gXG5cbiAgICB0aGlzLmRvU2VhcmNoKCk7XG4gIH1cblxuICBwcml2YXRlIGRvU2VhcmNoKCkge1xuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgfTtcbiAgICB0aGlzLnNlYXJjaFNlcnYuZ2V0U2VhcmNoTGlzdChwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgXG4gICAgICBsZXQgc2VhcmNoV2l0aG91dExvZ2luOiBhbnlbXSA9IHJlc3BvbnNlLnNlYXJjaFdpdGhvdXRMb2dpbjtcbiAgICBcbiAgICAgIHRoaXMubmV3c0xpc3QgPSByZXNwb25zZS5zZWFyY2g7XG4gICAgICBmb3IodmFyIGkgPSAwOyBpIDwgc2VhcmNoV2l0aG91dExvZ2luLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBzZWFyY2hJdGVtID0gc2VhcmNoV2l0aG91dExvZ2luW2ldO1xuICAgICAgICBcbiAgICAgICAgc2VhcmNoSXRlbS5maXJzdF9uYW1lID0gXCJhbm9ueW1vdXNcIjtcbiAgICAgICAgc2VhcmNoSXRlbS5sYXN0X25hbWUgPSBcImFub255bW91c1wiO1xuICAgICAgXG4gICAgICAgIHRoaXMubmV3c0xpc3QucHVzaChzZWFyY2hJdGVtKTtcbiAgICAgIH1cbiAgICAgIFxuXG4gICAgICB0aGlzLnRvdGFsSXRlbXMgPSB0aGlzLm5ld3NMaXN0Lmxlbmd0aDtcbiAgICAgICB0aGlzLnBhZ2VDaGFuZ2VkKHsgcGFnZTogdGhpcy5jdXJyZW50UGFnZSwgaXRlbXNQZXJQYWdlOiB0aGlzLml0ZW1zUGVyUGFnZSB9KTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgcGFnZUNoYW5nZWQoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCBzdGFydCA9IChldmVudC5wYWdlIC0gMSkgKiBldmVudC5pdGVtc1BlclBhZ2U7XG4gICAgbGV0IGVuZCA9IGV2ZW50Lml0ZW1zUGVyUGFnZSA+IC0xID8gKHN0YXJ0ICsgZXZlbnQuaXRlbXNQZXJQYWdlKSA6IHRoaXMubmV3c0xpc3QubGVuZ3RoO1xuICAgIHRoaXMuZGF0YSA9IHRoaXMubmV3c0xpc3Quc2xpY2Uoc3RhcnQsIGVuZCk7XG4gIH1cblxuICBwcml2YXRlIGRvRGVsZXRlU2VhcmNoKG9iaikge1xuICAgXG4gICAgbGV0IGlkID0gb2JqLmlkO1xuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAnaWQnOiBpZFxuICAgIH07XG4gICAgdmFyIG1lID0gdGhpcztcbiAgICB0aGlzLnNlYXJjaFNlcnYuZGVsZXRlU2VhcmNoKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBpZihyZXNwb25zZS5yZXR1cm5fY2QgPT0gMCkge1xuICAgICAgICBtZS5ub3RpZnlTZXJ2LnN1Y2Nlc3MoJ1NlYXJjaCBJdGVtcyBoYXMgYmVlbiBkZWxldGVkJyk7XG4gICAgICAgIG1lLmRvU2VhcmNoKCk7XG4gICAgICB9XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb1NldFNjb3BlKG9iaikge1xuICAgIHRoaXMuc3RvcmFnZVNlcnYuc2V0U2NvcGUob2JqKTtcbiAgfVxuXG59XG4iXX0=
