"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("app/router.animations");
var table_service_1 = require("app/services/table.service");
var auth_service_1 = require("app/services/auth.service");
var notification_service_1 = require("app/services/notification.service");
var sweetalert2_1 = require("sweetalert2");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var ng2_select_1 = require("ng2-select");
var area_service_1 = require("app/services/area.service");
var Observable_1 = require("rxjs/Observable");
var TableListComponent = (function () {
    function TableListComponent(tableService, areaService, authServ, notif, router, StorageService) {
        this.tableService = tableService;
        this.areaService = areaService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.StorageService = StorageService;
        this.items = [];
        this.value = {};
    }
    TableListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllAreas(this.restId);
        this.areaId = null;
        if (this.value.id != undefined && this.value.id != null && this.value.id != 0) {
            this.areaId = this.value.id;
        }
        this.fetchAllTables(this.restId, this.areaId);
        // long polling
        this.subscription = Observable_1.Observable.interval(30000)
            .map(function (x) { return x + 1; })
            .subscribe(function (x) {
            _this.fetchAllTables(_this.restId, _this.areaId);
        });
    };
    TableListComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    TableListComponent.prototype.fetchAllTables = function (restId, area_id) {
        var _this = this;
        this.tableService.getTables(restId, null, area_id).then(function (response) {
            _this.tables = response.tables;
            console.log(response.tables);
        }, function (error) {
            console.log(error);
        });
    };
    TableListComponent.prototype.fetchAllAreas = function (restId) {
        var _this = this;
        this.areaService.getAreas(restId, null).then(function (response) {
            _this.areas = response.areas;
            _this.items.push({
                id: 0,
                text: "ALL"
            });
            for (var i = 0; i < _this.areas.length; i++) {
                _this.items.push({
                    id: _this.areas[i].id,
                    text: "" + _this.areas[i].name
                });
            }
            _this.select.items = _this.items;
            _this.value = _this.items[0];
            console.log(response.areas);
        }, function (error) {
            console.log(error);
        });
    };
    TableListComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
        if (this.value.id != undefined && this.value.id != null && this.value.id != 0) {
            this.areaId = this.value.id;
        }
    };
    TableListComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    TableListComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    TableListComponent.prototype.refreshValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.value = value;
            if (this.value.id != undefined && this.value.id != null && this.value.id != 0) {
                this.areaId = this.value.id;
            }
            this.fetchAllTables(this.restId, this.areaId);
        }
        else {
            this.fetchAllTables(this.restId, null);
        }
    };
    TableListComponent.prototype.updateTable = function (item, isActive) {
        var _this = this;
        var action = 'update_tables_by_id';
        this.tableObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'table': {
                'id': item.id,
                'name': item.name,
                'content': item.content,
                'thumb': item.thumb,
                'description': item.description,
                'is_active': isActive,
                'is_delete': '0'
            }
        };
        this.tableService.createTable(this.tableObject).then(function (response) { return _this.processResult(response, null); }, function (error) { return _this.failedCreate.bind(error); });
    };
    TableListComponent.prototype.deleteTable = function (item, isActive) {
        var that = this;
        sweetalert2_1.default({
            title: 'Bạn có chắc không?',
            text: "Khi đã xóa không thể khôi phục lại",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Có'
        }).then(function () {
            that.tableObject = {
                'rest_id': that.restId,
                'user_id': that.userId,
                'table': {
                    'id': item.id,
                    'name': item.name,
                    'thumb': item.thumb,
                    'description': item.description,
                    'is_active': isActive,
                    'is_delete': '1'
                }
            };
            that.tableService.createTable(that.tableObject).then(function (response) { return that.processResult(response, item.thumb); }, function (error) { return that.failedCreate.bind(error); });
        });
    };
    TableListComponent.prototype.reserveTable = function (id) {
        this.router.navigateByUrl('/table-booking-schedule/view/' + id);
    };
    TableListComponent.prototype.processResult = function (response, thumb) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            if (thumb != null) {
                this.notif.success('Table is deleting...');
                this.tableService.deleteFile({ 'thumb': thumb }).then(function (response) {
                    _this.notif.success('Done');
                }, function (error) {
                    console.log(error);
                });
                this.fetchAllTables(this.restId, this.value.id);
            }
            else {
                this.notif.success('Done');
                this.fetchAllTables(this.restId, this.value.id);
            }
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    TableListComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    TableListComponent.prototype.createTable = function () {
        //   this.userService.currentUser.subscribe((user) => {
        //    console.log('user', user);
        // });
        this.StorageService.setScope(null);
        this.router.navigateByUrl('/table/create');
    };
    TableListComponent.prototype.editTable = function (item) {
        this.StorageService.setScope(item);
        this.router.navigateByUrl('/table/create/' + item.id);
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], TableListComponent.prototype, "select", void 0);
    TableListComponent = __decorate([
        core_1.Component({
            selector: 'table-list',
            templateUrl: utils_1.default.getView('app/components/table-list/table-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/table-list/table-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [table_service_1.TableService,
            area_service_1.AreaService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            storage_service_1.StorageService])
    ], TableListComponent);
    return TableListComponent;
}());
exports.TableListComponent = TableListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3RhYmxlLWxpc3QvdGFibGUtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBK0U7QUFDL0UseUNBQW9DO0FBR3BDLDJEQUF5RDtBQUN6RCw0REFBMEQ7QUFDMUQsMERBQXdEO0FBQ3hELDBFQUF3RTtBQUN4RSwyQ0FBOEM7QUFDOUMsMENBQXlDO0FBQ3pDLGdFQUE4RDtBQUM5RCx5Q0FBd0Q7QUFDeEQsMERBQXdEO0FBQ3hELDhDQUEyQztBQVEzQztJQVVFLDRCQUFvQixZQUEwQixFQUN0QyxXQUF3QixFQUN4QixRQUFxQixFQUNyQixLQUEwQixFQUMxQixNQUFjLEVBQ2QsY0FBOEI7UUFMbEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQUMxQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBUjlCLFVBQUssR0FBZSxFQUFFLENBQUM7UUFTdkIsVUFBSyxHQUFPLEVBQUUsQ0FBQztJQURvQixDQUFDO0lBRzVDLHFDQUFRLEdBQVI7UUFBQSxpQkFtQkM7UUFqQkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFFNUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFFLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUN0RSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1FBQzlCLENBQUM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTlDLGVBQWU7UUFDZixJQUFJLENBQUMsWUFBWSxHQUFHLHVCQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzthQUMxQyxHQUFHLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLEdBQUMsQ0FBQyxFQUFILENBQUcsQ0FBQzthQUNmLFNBQVMsQ0FBQyxVQUFDLENBQUM7WUFDYixLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHdDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUFFUSwyQ0FBYyxHQUF0QixVQUF1QixNQUFNLEVBQUMsT0FBTztRQUFyQyxpQkFRQztRQVBBLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNwQyxVQUFBLFFBQVE7WUFDUCxLQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUNkLFVBQUEsS0FBSztZQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRVEsMENBQWEsR0FBckIsVUFBc0IsTUFBTTtRQUE1QixpQkFvQkk7UUFuQkEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDMUIsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNQLEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxLQUFLO2FBQ1osQ0FBQyxDQUFDO1lBQ1YsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUN2QyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztvQkFDZCxFQUFFLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNwQixJQUFJLEVBQUUsS0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQU07aUJBQzlCLENBQUMsQ0FBQztZQUNSLENBQUM7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDO1lBQzlCLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHTyxxQ0FBUSxHQUFmLFVBQWdCLEtBQVM7UUFFdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUUxQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBRSxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDdEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUM5QixDQUFDO0lBQ0gsQ0FBQztJQUVNLG9DQUFPLEdBQWQsVUFBZSxLQUFTO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLGtDQUFLLEdBQVosVUFBYSxLQUFTO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLHlDQUFZLEdBQW5CLFVBQW9CLEtBQVM7UUFDM0IsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLFNBQVMsSUFBSSxLQUFLLElBQUUsSUFBSSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUVuQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBRSxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3RFLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDOUIsQ0FBQztZQUNELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUMsQ0FBQztRQUFBLElBQUksQ0FBQSxDQUFDO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pDLENBQUM7SUFDSixDQUFDO0lBRVQsd0NBQVcsR0FBWCxVQUFZLElBQUksRUFBQyxRQUFRO1FBQXpCLGlCQWlCQztRQWhCQyxJQUFJLE1BQU0sR0FBRyxxQkFBcUIsQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHO1lBQ1QsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBQyxJQUFJLENBQUMsTUFBTTtZQUNyQixPQUFPLEVBQUM7Z0JBQ04sSUFBSSxFQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNaLE1BQU0sRUFBQyxJQUFJLENBQUMsSUFBSTtnQkFDaEIsU0FBUyxFQUFDLElBQUksQ0FBQyxPQUFPO2dCQUN0QixPQUFPLEVBQUMsSUFBSSxDQUFDLEtBQUs7Z0JBQ2xCLGFBQWEsRUFBQyxJQUFJLENBQUMsV0FBVztnQkFDOUIsV0FBVyxFQUFDLFFBQVE7Z0JBQ3BCLFdBQVcsRUFBQyxHQUFHO2FBQ2hCO1NBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQ3ZDLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLEVBQWpDLENBQWlDLEVBQ3pDLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsd0NBQVcsR0FBWCxVQUFZLElBQUksRUFBQyxRQUFRO1FBQ3ZCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixxQkFBSSxDQUFDO1lBQ0wsS0FBSyxFQUFFLG9CQUFvQjtZQUMzQixJQUFJLEVBQUUsb0NBQW9DO1lBQzFDLElBQUksRUFBRSxTQUFTO1lBQ2YsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixrQkFBa0IsRUFBRSxTQUFTO1lBQzdCLGlCQUFpQixFQUFFLE1BQU07WUFDekIsaUJBQWlCLEVBQUUsSUFBSTtTQUN4QixDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ04sSUFBSSxDQUFDLFdBQVcsR0FBRztnQkFDVCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07Z0JBQ3RCLFNBQVMsRUFBQyxJQUFJLENBQUMsTUFBTTtnQkFDckIsT0FBTyxFQUFDO29CQUNOLElBQUksRUFBQyxJQUFJLENBQUMsRUFBRTtvQkFDWixNQUFNLEVBQUMsSUFBSSxDQUFDLElBQUk7b0JBQ2hCLE9BQU8sRUFBQyxJQUFJLENBQUMsS0FBSztvQkFDbEIsYUFBYSxFQUFDLElBQUksQ0FBQyxXQUFXO29CQUM5QixXQUFXLEVBQUMsUUFBUTtvQkFDcEIsV0FBVyxFQUFDLEdBQUc7aUJBQ2hCO2FBQUMsQ0FBQztZQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBRXZDLFVBQUEsUUFBUSxJQUFLLE9BQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUF2QyxDQUF1QyxFQUMvQyxVQUFBLEtBQUssSUFBSyxPQUFBLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUMxQyxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO0lBQ0gsQ0FBQztJQUVPLHlDQUFZLEdBQXBCLFVBQXFCLEVBQUU7UUFFckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsK0JBQStCLEdBQUUsRUFBRSxDQUFDLENBQUM7SUFDakUsQ0FBQztJQUVPLDBDQUFhLEdBQXJCLFVBQXNCLFFBQVEsRUFBQyxLQUFLO1FBQXBDLGlCQTBCRztRQXhCQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLFNBQVMsSUFBSSxRQUFRLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDOUQsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO29CQUM1RCxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDN0IsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVqRCxDQUFDO1lBQ0QsSUFBSSxDQUNKLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBRWpELENBQUM7UUFFSCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixxQkFBSSxDQUNBLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixDQUFDO0lBQ0gsQ0FBQztJQUNPLHlDQUFZLEdBQXBCLFVBQXNCLEdBQUc7UUFDdkIscUJBQUksQ0FDRSxjQUFjLEVBQ2QsT0FBTyxDQUNSLENBQUMsS0FBSyxDQUFDLHFCQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUNELHdDQUFXLEdBQVg7UUFDRSx1REFBdUQ7UUFDdkQsZ0NBQWdDO1FBRWhDLE1BQU07UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBQ0Qsc0NBQVMsR0FBVCxVQUFVLElBQUk7UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsR0FBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDekQsQ0FBQztJQTNNc0I7UUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7a0NBQWdCLDRCQUFlO3NEQUFDO0lBRDNDLGtCQUFrQjtRQVA3QixnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLFlBQVk7WUFDdEIsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMscURBQXFELENBQUM7WUFDakYsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1lBQ2hGLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUMscUJBQXFCLEVBQUUsRUFBRSxFQUFDO1NBQ2xDLENBQUM7eUNBV2tDLDRCQUFZO1lBQ3pCLDBCQUFXO1lBQ2QsMEJBQVc7WUFDZCwwQ0FBbUI7WUFDbEIsZUFBTTtZQUNFLGdDQUFjO09BZjNCLGtCQUFrQixDQTZNN0I7SUFBRCx5QkFBQztDQTdNRixBQTZNRSxJQUFBO0FBN01XLGdEQUFrQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy90YWJsZS1saXN0L3RhYmxlLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsVmlld0NoaWxkLFZpZXdFbmNhcHN1bGF0aW9uICB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSB9IGZyb20gJ2FwcC9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyByb3V0ZXJUcmFuc2l0aW9uIH0gZnJvbSAnYXBwL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IFRhYmxlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy90YWJsZS5zZXJ2aWNlJztcbmltcG9ydCAgeyBBdXRoU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHtTZWxlY3RNb2R1bGUsU2VsZWN0Q29tcG9uZW50fSBmcm9tICduZzItc2VsZWN0JztcbmltcG9ydCB7IEFyZWFTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2FyZWEuc2VydmljZSc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzL09ic2VydmFibGVcIjtcbiBAQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd0YWJsZS1saXN0JyxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL3RhYmxlLWxpc3QvdGFibGUtbGlzdC5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy90YWJsZS1saXN0L3RhYmxlLWxpc3QuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJUcmFuc2l0aW9uXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBUYWJsZUxpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKCdTZWxlY3RJZCcpIHB1YmxpYyBzZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcbiAgcHJpdmF0ZSB0YWJsZXMgOiBhbnk7XG4gIHByaXZhdGUgdG9rZW4gOiBhbnk7XG4gIHByaXZhdGUgdXNlcklkIDogU3RyaW5nO1xuICBwcml2YXRlIHJlc3RJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSB0YWJsZU9iamVjdCA6IGFueTtcbiAgcHJpdmF0ZSBpdGVtczogQXJyYXk8YW55PiA9IFtdO1xuICBwcml2YXRlIGFyZWFzIDogYW55O1xuICBwcml2YXRlIHN1YnNjcmlwdGlvbjogYW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRhYmxlU2VydmljZTogVGFibGVTZXJ2aWNlLFxuICBwcml2YXRlIGFyZWFTZXJ2aWNlOiBBcmVhU2VydmljZSxcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG4gIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLCkgeyB9XG4gIHByaXZhdGUgdmFsdWU6YW55ID0ge307XG4gIHByaXZhdGUgYXJlYUlkOiBhbnk7XG4gIG5nT25Jbml0KCkge1xuXG4gICAgbGV0IHVzZXJJbmZvID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG5cbiAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICAgIHRoaXMuZmV0Y2hBbGxBcmVhcyh0aGlzLnJlc3RJZCk7XG4gICAgdGhpcy5hcmVhSWQgPSBudWxsO1xuICAgIGlmKHRoaXMudmFsdWUuaWQhPXVuZGVmaW5lZCAmJiB0aGlzLnZhbHVlLmlkIT1udWxsICYmIHRoaXMudmFsdWUuaWQhPTApe1xuICAgICAgdGhpcy5hcmVhSWQgPSB0aGlzLnZhbHVlLmlkO1xuICAgIH1cbiAgICB0aGlzLmZldGNoQWxsVGFibGVzKHRoaXMucmVzdElkLHRoaXMuYXJlYUlkKTtcbiAgIFxuICAgLy8gbG9uZyBwb2xsaW5nXG4gICB0aGlzLnN1YnNjcmlwdGlvbiA9IE9ic2VydmFibGUuaW50ZXJ2YWwoMzAwMDApXG4gICAgICAubWFwKCh4KSA9PiB4KzEpXG4gICAgICAuc3Vic2NyaWJlKCh4KSA9PiB7XG4gICAgICB0aGlzLmZldGNoQWxsVGFibGVzKHRoaXMucmVzdElkLHRoaXMuYXJlYUlkKTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCl7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIFx0cHJpdmF0ZSBmZXRjaEFsbFRhYmxlcyhyZXN0SWQsYXJlYV9pZCkge1xuICBcdFx0dGhpcy50YWJsZVNlcnZpY2UuZ2V0VGFibGVzKHJlc3RJZCxudWxsLGFyZWFfaWQpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLnRhYmxlcyA9IHJlc3BvbnNlLnRhYmxlcztcbiAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2UudGFibGVzKTtcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICBcdFx0XHRcdFx0fSk7XG4gIFx0fVxuXG4gICAgcHJpdmF0ZSBmZXRjaEFsbEFyZWFzKHJlc3RJZCkge1xuICAgICAgXHRcdHRoaXMuYXJlYVNlcnZpY2UuZ2V0QXJlYXMocmVzdElkLG51bGwpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBcdHRoaXMuYXJlYXMgPSByZXNwb25zZS5hcmVhcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXRlbXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IGBBTExgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8ICB0aGlzLmFyZWFzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiB0aGlzLmFyZWFzW2ldLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogYCR7dGhpcy5hcmVhc1tpXS5uYW1lfWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3QuaXRlbXMgPSB0aGlzLml0ZW1zO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLml0ZW1zWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2UuYXJlYXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgICAgIFx0XHRcdFx0XHR9KTtcbiAgICAgIFx0fVxuXG5cbiAgICAgICAgcHVibGljIHNlbGVjdGVkKHZhbHVlOmFueSk6dm9pZCB7XG5cbiAgICAgICAgICBjb25zb2xlLmxvZygnU2VsZWN0ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgICAgICAgICBcbiAgICAgICAgICBpZih0aGlzLnZhbHVlLmlkIT11bmRlZmluZWQgJiYgdGhpcy52YWx1ZS5pZCE9bnVsbCAmJiB0aGlzLnZhbHVlLmlkIT0wKXtcbiAgICAgICAgICAgIHRoaXMuYXJlYUlkID0gdGhpcy52YWx1ZS5pZDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdSZW1vdmVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBwdWJsaWMgdHlwZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgcHVibGljIHJlZnJlc2hWYWx1ZSh2YWx1ZTphbnkpOnZvaWQge1xuICAgICAgICAgIGlmKHZhbHVlIT11bmRlZmluZWQgJiYgdmFsdWUhPW51bGwgJiYgdmFsdWUubGVuZ3RoIT0wKXtcbiAgICAgICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgICAgXG4gICAgICAgICAgICBpZih0aGlzLnZhbHVlLmlkIT11bmRlZmluZWQgJiYgdGhpcy52YWx1ZS5pZCE9bnVsbCAmJiB0aGlzLnZhbHVlLmlkIT0wKXtcbiAgICAgICAgICAgICAgdGhpcy5hcmVhSWQgPSB0aGlzLnZhbHVlLmlkO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5mZXRjaEFsbFRhYmxlcyh0aGlzLnJlc3RJZCx0aGlzLmFyZWFJZCk7XG4gICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICB0aGlzLmZldGNoQWxsVGFibGVzKHRoaXMucmVzdElkLG51bGwpO1xuICAgICAgICAgICB9XG4gICAgICAgIH1cblxudXBkYXRlVGFibGUoaXRlbSxpc0FjdGl2ZSl7XG4gIHZhciBhY3Rpb24gPSAndXBkYXRlX3RhYmxlc19ieV9pZCc7XG4gIHRoaXMudGFibGVPYmplY3QgPSB7XG4gICAgICAgICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgICAgICAgJ3VzZXJfaWQnOnRoaXMudXNlcklkLFxuICAgICAgICAgICAgJ3RhYmxlJzp7XG4gICAgICAgICAgICAgICdpZCc6aXRlbS5pZCxcbiAgICAgICAgICAgICAgJ25hbWUnOml0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgJ2NvbnRlbnQnOml0ZW0uY29udGVudCxcbiAgICAgICAgICAgICAgJ3RodW1iJzppdGVtLnRodW1iLFxuICAgICAgICAgICAgICAnZGVzY3JpcHRpb24nOml0ZW0uZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICdpc19hY3RpdmUnOmlzQWN0aXZlLFxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzonMCdcbiAgICAgICAgICAgIH19O1xuICBcdHRoaXMudGFibGVTZXJ2aWNlLmNyZWF0ZVRhYmxlKHRoaXMudGFibGVPYmplY3QpLnRoZW4oXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHRoaXMucHJvY2Vzc1Jlc3VsdChyZXNwb25zZSxudWxsKSxcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG59XG5cbmRlbGV0ZVRhYmxlKGl0ZW0saXNBY3RpdmUpe1xuICBsZXQgdGhhdCA9IHRoaXM7XG4gIHN3YWwoe1xuICB0aXRsZTogJ0LhuqFuIGPDsyBjaOG6r2Mga2jDtG5nPycsXG4gIHRleHQ6IFwiS2hpIMSRw6MgeMOzYSBraMO0bmcgdGjhu4Mga2jDtGkgcGjhu6VjIGzhuqFpXCIsXG4gIHR5cGU6ICd3YXJuaW5nJyxcbiAgc2hvd0NhbmNlbEJ1dHRvbjogdHJ1ZSxcbiAgY29uZmlybUJ1dHRvbkNvbG9yOiAnIzMwODVkNicsXG4gIGNhbmNlbEJ1dHRvbkNvbG9yOiAnI2QzMycsXG4gIGNvbmZpcm1CdXR0b25UZXh0OiAnQ8OzJ1xufSkudGhlbihmdW5jdGlvbiAoKSB7XG4gIHRoYXQudGFibGVPYmplY3QgPSB7XG4gICAgICAgICAgICAncmVzdF9pZCc6IHRoYXQucmVzdElkLFxuICAgICAgICAgICAgJ3VzZXJfaWQnOnRoYXQudXNlcklkLFxuICAgICAgICAgICAgJ3RhYmxlJzp7XG4gICAgICAgICAgICAgICdpZCc6aXRlbS5pZCxcbiAgICAgICAgICAgICAgJ25hbWUnOml0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgJ3RodW1iJzppdGVtLnRodW1iLFxuICAgICAgICAgICAgICAnZGVzY3JpcHRpb24nOml0ZW0uZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICdpc19hY3RpdmUnOmlzQWN0aXZlLFxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzonMSdcbiAgICAgICAgICAgIH19O1xuICBcdHRoYXQudGFibGVTZXJ2aWNlLmNyZWF0ZVRhYmxlKHRoYXQudGFibGVPYmplY3QpLnRoZW4oXG5cbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhhdC5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlLGl0ZW0udGh1bWIpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoYXQuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpLFxuICAgICAgICAgICAgICAgICApO1xufSk7XG59XG5cbnByaXZhdGUgcmVzZXJ2ZVRhYmxlKGlkKVxue1xuICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvdGFibGUtYm9va2luZy1zY2hlZHVsZS92aWV3LycrIGlkKTtcbn1cblxucHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlLHRodW1iKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgICBpZih0aHVtYiE9bnVsbCl7XG4gICAgICAgIHRoaXMubm90aWYuc3VjY2VzcygnVGFibGUgaXMgZGVsZXRpbmcuLi4nKTtcbiAgICAgICAgdGhpcy50YWJsZVNlcnZpY2UuZGVsZXRlRmlsZSh7ICd0aHVtYic6IHRodW1iIH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgIHRoaXMubm90aWYuc3VjY2VzcygnRG9uZScpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5mZXRjaEFsbFRhYmxlcyh0aGlzLnJlc3RJZCx0aGlzLnZhbHVlLmlkKTtcblxuICAgICAgfVxuICAgICAgZWxzZVxuICAgICAge1xuICAgICAgICB0aGlzLm5vdGlmLnN1Y2Nlc3MoJ0RvbmUnKTtcbiAgICAgICAgdGhpcy5mZXRjaEFsbFRhYmxlcyh0aGlzLnJlc3RJZCx0aGlzLnZhbHVlLmlkKTtcblxuICAgICAgfVxuXG4gICAgfSBlbHNlIHtcbiAgICAgIHN3YWwoXG4gICAgICAgICAgJ1VwZGF0ZSBGYWlsIScsXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICB9XG4gIGNyZWF0ZVRhYmxlKCl7XG4gICAgLy8gICB0aGlzLnVzZXJTZXJ2aWNlLmN1cnJlbnRVc2VyLnN1YnNjcmliZSgodXNlcikgPT4ge1xuICAgIC8vICAgIGNvbnNvbGUubG9nKCd1c2VyJywgdXNlcik7XG5cbiAgICAvLyB9KTtcbiAgICAgIHRoaXMuU3RvcmFnZVNlcnZpY2Uuc2V0U2NvcGUobnVsbCk7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvdGFibGUvY3JlYXRlJyk7XG4gIH1cbiAgZWRpdFRhYmxlKGl0ZW0pe1xuICAgICAgdGhpcy5TdG9yYWdlU2VydmljZS5zZXRTY29wZShpdGVtKTtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy90YWJsZS9jcmVhdGUvJysgaXRlbS5pZCk7XG4gIH1cbiB9XG4iXX0=
