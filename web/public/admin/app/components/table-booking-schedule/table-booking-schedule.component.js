"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var restaurant_service_1 = require("app/services/restaurant.service");
var notification_service_1 = require("app/services/notification.service");
var user_service_1 = require("app/services/user.service");
var router_animations_1 = require("../../router.animations");
var router_1 = require("@angular/router");
var TableBookingScheduleComponent = (function () {
    function TableBookingScheduleComponent(restServ, fb, userServ, router, notifyServ) {
        this.restServ = restServ;
        this.fb = fb;
        this.userServ = userServ;
        this.router = router;
        this.notifyServ = notifyServ;
        this.content = null;
        this.fromDate = null;
        this.Settings = {
            statusDateEdit: {
                t2: false,
                t3: false,
                t4: false,
                t5: false,
                t6: false,
                t7: false,
                cn: false,
                hn: false
            },
            list: null,
            currentPage: 0,
            filter: {
                title: "",
                category_id: "",
                created_at_from: "",
                created_at_to: ""
            },
            showFilter: {
                'tinh_trang': '1',
                'loaiKhach': '-1'
            },
            deletePopover: {
                content: 'Bạn có thực sự muốn xóa không?',
                Yes: 'Có',
                No: 'Không',
                flag: -1
            },
            timeMstList: ['07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'],
            dayInWeek: {
                t2: 'Thứ Hai',
                t3: 'Thứ Ba',
                t4: 'Thứ Tư',
                t5: 'Thứ Năm',
                t6: 'Thứ Sáu',
                t7: 'Thứ Bảy',
                cn: 'Chủ nhật'
            }
        };
        this.timeList = [];
        for (var i = 0; i < this.Settings.timeMstList.length; i++) {
            var timeMst = this.Settings.timeMstList[i];
            if (timeMst != '24') {
                var time1 = {
                    stTime: timeMst + ":00",
                    endTime: timeMst + ":30",
                    cusName: "",
                    checkFlg: 0,
                    endFlg: 0,
                    loaiKhach: '0'
                };
                this.timeList.push(time1);
                var end = '' + timeMst + 1;
                if ((end + '').length == 1) {
                    end = '0' + end;
                }
                var time1 = {
                    stTime: timeMst + ":30",
                    endTime: end + ":00",
                    cusName: "",
                    checkFlg: 0,
                    endFlg: 0,
                    loaiKhach: '0'
                };
                this.timeList.push(time1);
            }
        }
        this.scheduleList = [
            { 'time': '2017/06/21 ' },
            { 'time': '2017/06/22' },
            { 'time': '2017/06/23 ' },
            { 'time': '2017/06/24' },
            { 'time': '2017/06/25 ' },
            { 'time': '2017/06/26 ' }, { 'time': '2017/06/27 ' },
            { 'time': '2017/06/28 ' },
            { 'time': '2017/06/29 ' },
            { 'time': '2017/06/30 ' }
        ];
    }
    TableBookingScheduleComponent.prototype.ngOnInit = function () {
    };
    TableBookingScheduleComponent.prototype.onSelectFromDate = function (value) {
    };
    TableBookingScheduleComponent.prototype.reserveTable = function () {
        this.router.navigateByUrl('/reserve-table/create/');
    };
    TableBookingScheduleComponent = __decorate([
        core_1.Component({
            selector: 'table-booking-schedule',
            templateUrl: utils_1.default.getView('app/components/table-booking-schedule/table-booking-schedule.component.html'),
            styleUrls: [utils_1.default.getView('app/components/table-booking-schedule/table-booking-schedule.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
            forms_1.FormBuilder,
            user_service_1.UserService,
            router_1.Router,
            notification_service_1.NotificationService])
    ], TableBookingScheduleComponent);
    return TableBookingScheduleComponent;
}());
exports.TableBookingScheduleComponent = TableBookingScheduleComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3RhYmxlLWJvb2tpbmctc2NoZWR1bGUvdGFibGUtYm9va2luZy1zY2hlZHVsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQsd0NBQTZGO0FBQzdGLHlDQUFvQztBQUNwQyxzRUFBb0U7QUFDcEUsMEVBQXdFO0FBQ3hFLDBEQUF3RDtBQUN4RCw2REFBMkQ7QUFDM0QsMENBQXlDO0FBUXpDO0lBVUUsdUNBQ1UsUUFBMkIsRUFDM0IsRUFBZSxFQUNmLFFBQXFCLEVBQ3BCLE1BQWMsRUFDZixVQUErQjtRQUovQixhQUFRLEdBQVIsUUFBUSxDQUFtQjtRQUMzQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNwQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2YsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFWakMsWUFBTyxHQUFvQixJQUFJLENBQUM7UUFJL0IsYUFBUSxHQUFRLElBQUksQ0FBQztRQVM5QixJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ2IsY0FBYyxFQUFDO2dCQUNkLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2FBQ1I7WUFDRSxJQUFJLEVBQUUsSUFBSTtZQUNWLFdBQVcsRUFBRSxDQUFDO1lBQ2QsTUFBTSxFQUFFO2dCQUNKLEtBQUssRUFBRSxFQUFFO2dCQUNULFdBQVcsRUFBRSxFQUFFO2dCQUNmLGVBQWUsRUFBRSxFQUFFO2dCQUNuQixhQUFhLEVBQUUsRUFBRTthQUNwQjtZQUNELFVBQVUsRUFBQztnQkFDUCxZQUFZLEVBQUcsR0FBRztnQkFDbEIsV0FBVyxFQUFDLElBQUk7YUFDbkI7WUFFRCxhQUFhLEVBQUU7Z0JBQ1gsT0FBTyxFQUFFLGdDQUFnQztnQkFDekMsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsRUFBRSxFQUFFLE9BQU87Z0JBQ1gsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUNYO1lBQ0QsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFDLElBQUksRUFBRSxJQUFJLENBQUM7WUFDdkgsU0FBUyxFQUFFO2dCQUNQLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxRQUFRO2dCQUNaLEVBQUUsRUFBRSxRQUFRO2dCQUNaLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxVQUFVO2FBQ2pCO1NBQ0osQ0FBQztRQUVILElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekQsSUFBSSxPQUFPLEdBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFNUMsRUFBRSxDQUFBLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLElBQUksS0FBSyxHQUFHO29CQUNELE1BQU0sRUFBRSxPQUFPLEdBQUcsS0FBSztvQkFDdkIsT0FBTyxFQUFFLE9BQU8sR0FBRyxLQUFLO29CQUN4QixPQUFPLEVBQUUsRUFBRTtvQkFDWCxRQUFRLEVBQUUsQ0FBQztvQkFDWCxNQUFNLEVBQUUsQ0FBQztvQkFDVCxTQUFTLEVBQUUsR0FBRztpQkFDakIsQ0FBQztnQkFFRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxHQUFHLEdBQUcsRUFBRSxHQUFFLE9BQU8sR0FBRyxDQUFDLENBQUM7Z0JBQzFCLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN6QixHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztnQkFDcEIsQ0FBQztnQkFDRCxJQUFJLEtBQUssR0FBRztvQkFDUixNQUFNLEVBQUUsT0FBTyxHQUFHLEtBQUs7b0JBQ3ZCLE9BQU8sRUFBRSxHQUFHLEdBQUcsS0FBSztvQkFDcEIsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLENBQUM7b0JBQ1gsTUFBTSxFQUFFLENBQUM7b0JBQ1QsU0FBUyxFQUFFLEdBQUc7aUJBQ2pCLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEMsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHO1lBQ2QsRUFBQyxNQUFNLEVBQUUsYUFBYSxFQUFDO1lBQ3ZCLEVBQUMsTUFBTSxFQUFFLFlBQVksRUFBQztZQUN0QixFQUFDLE1BQU0sRUFBRSxhQUFhLEVBQUM7WUFDdkIsRUFBQyxNQUFNLEVBQUUsWUFBWSxFQUFDO1lBQ3RCLEVBQUMsTUFBTSxFQUFFLGFBQWEsRUFBQztZQUN2QixFQUFDLE1BQU0sRUFBRSxhQUFhLEVBQUMsRUFBQyxFQUFDLE1BQU0sRUFBRSxhQUFhLEVBQUM7WUFDOUMsRUFBQyxNQUFNLEVBQUUsYUFBYSxFQUFDO1lBQ3hCLEVBQUMsTUFBTSxFQUFFLGFBQWEsRUFBQztZQUN2QixFQUFDLE1BQU0sRUFBRSxhQUFhLEVBQUM7U0FDMUIsQ0FBQztJQUlOLENBQUM7SUFFRCxnREFBUSxHQUFSO0lBRUEsQ0FBQztJQUVPLHdEQUFnQixHQUF2QixVQUF3QixLQUFVO0lBRW5DLENBQUM7SUFHTyxvREFBWSxHQUFuQjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDbkQsQ0FBQztJQXRIUyw2QkFBNkI7UUFQeEMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSx3QkFBd0I7WUFDbEMsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsNkVBQTZFLENBQUM7WUFDekcsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyw0RUFBNEUsQ0FBQyxDQUFDO1lBQ3RHLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDbEMsSUFBSSxFQUFFLEVBQUMscUJBQXFCLEVBQUUsRUFBRSxFQUFDO1NBQ2xDLENBQUM7eUNBWW9CLHNDQUFpQjtZQUN2QixtQkFBVztZQUNMLDBCQUFXO1lBQ1osZUFBTTtZQUNILDBDQUFtQjtPQWY5Qiw2QkFBNkIsQ0F5SHpDO0lBQUQsb0NBQUM7Q0F6SEQsQUF5SEMsSUFBQTtBQXpIWSxzRUFBNkIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvdGFibGUtYm9va2luZy1zY2hlZHVsZS90YWJsZS1ib29raW5nLXNjaGVkdWxlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0sIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgUmVzdGF1cmFudFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvcmVzdGF1cmFudC5zZXJ2aWNlJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG4gQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndGFibGUtYm9va2luZy1zY2hlZHVsZScsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy90YWJsZS1ib29raW5nLXNjaGVkdWxlL3RhYmxlLWJvb2tpbmctc2NoZWR1bGUuY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvdGFibGUtYm9va2luZy1zY2hlZHVsZS90YWJsZS1ib29raW5nLXNjaGVkdWxlLmNvbXBvbmVudC5jc3MnKV0sXG4gICAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJUcmFuc2l0aW9uXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBUYWJsZUJvb2tpbmdTY2hlZHVsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHByaXZhdGUgbXltb2RlbDtcbiAgcHJpdmF0ZSByZXN0SWQ7XG4gIHByaXZhdGUgdXNlcklkO1xuICBwcml2YXRlIGR0Rm9ybTogRm9ybUdyb3VwO1xuICBwcml2YXRlIGNvbnRlbnQ6IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgU2V0dGluZ3M6YW55O1xuICBwcml2YXRlIHNjaGVkdWxlTGlzdDogYW55W107XG4gIHByaXZhdGUgdGltZUxpc3Q6YW55W107XG4gICBwcml2YXRlIGZyb21EYXRlOiBhbnkgPSBudWxsO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJlc3RTZXJ2OiBSZXN0YXVyYW50U2VydmljZSxcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIG5vdGlmeVNlcnY6IE5vdGlmaWNhdGlvblNlcnZpY2VcbiAgKSB7IFxuICAgXG4gIHRoaXMuU2V0dGluZ3MgPSB7XG4gICAgXHRzdGF0dXNEYXRlRWRpdDp7XG4gICAgXHRcdHQyOmZhbHNlLFxuICAgIFx0XHR0MzpmYWxzZSxcbiAgICBcdFx0dDQ6ZmFsc2UsXG4gICAgXHRcdHQ1OmZhbHNlLFxuICAgIFx0XHR0NjpmYWxzZSxcbiAgICBcdFx0dDc6ZmFsc2UsXG4gICAgXHRcdGNuOmZhbHNlLFxuICAgIFx0XHRobjpmYWxzZVxuICAgIFx0fSxcbiAgICAgICAgbGlzdDogbnVsbCxcbiAgICAgICAgY3VycmVudFBhZ2U6IDAsXG4gICAgICAgIGZpbHRlcjoge1xuICAgICAgICAgICAgdGl0bGU6IFwiXCIsXG4gICAgICAgICAgICBjYXRlZ29yeV9pZDogXCJcIixcbiAgICAgICAgICAgIGNyZWF0ZWRfYXRfZnJvbTogXCJcIixcbiAgICAgICAgICAgIGNyZWF0ZWRfYXRfdG86IFwiXCIgXG4gICAgICAgIH0sXG4gICAgICAgIHNob3dGaWx0ZXI6e1xuICAgICAgICAgICAgJ3RpbmhfdHJhbmcnIDogJzEnLFxuICAgICAgICAgICAgJ2xvYWlLaGFjaCc6Jy0xJ1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlbGV0ZVBvcG92ZXI6IHtcbiAgICAgICAgICAgIGNvbnRlbnQ6ICdC4bqhbiBjw7MgdGjhu7FjIHPhu7EgbXXhu5FuIHjDs2Ega2jDtG5nPycsXG4gICAgICAgICAgICBZZXM6ICdDw7MnLFxuICAgICAgICAgICAgTm86ICdLaMO0bmcnLFxuICAgICAgICAgICAgZmxhZzogLTFcbiAgICAgICAgfSxcbiAgICAgICAgdGltZU1zdExpc3Q6IFsnMDcnLCcwOCcsICcwOScsICcxMCcsICcxMScsICcxMicsICcxMycsICcxNCcsICcxNScsICcxNicsICcxNycsICcxOCcsICcxOScsICcyMCcsICcyMScsICcyMicsJzIzJywgJzI0J10sXG4gICAgICAgIGRheUluV2Vlazoge1xuICAgICAgICAgICAgdDI6ICdUaOG7qSBIYWknLFxuICAgICAgICAgICAgdDM6ICdUaOG7qSBCYScsXG4gICAgICAgICAgICB0NDogJ1Ro4bupIFTGsCcsXG4gICAgICAgICAgICB0NTogJ1Ro4bupIE7Eg20nLCAgICBcbiAgICAgICAgICAgIHQ2OiAnVGjhu6kgU8OhdScsXG4gICAgICAgICAgICB0NzogJ1Ro4bupIELhuqN5JyxcbiAgICAgICAgICAgIGNuOiAnQ2jhu6cgbmjhuq10J1xuICAgICAgICB9XG4gICAgfTtcblxuICAgdGhpcy50aW1lTGlzdCA9IFtdO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMuU2V0dGluZ3MudGltZU1zdExpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIHRpbWVNc3QgPSAgdGhpcy5TZXR0aW5ncy50aW1lTXN0TGlzdFtpXTtcblxuICAgICAgICBpZih0aW1lTXN0ICE9ICcyNCcpe1xuICAgICAgICBcdHZhciB0aW1lMSA9IHtcbiAgICAgICAgICAgICAgICAgICAgc3RUaW1lOiB0aW1lTXN0ICsgXCI6MDBcIixcbiAgICAgICAgICAgICAgICAgICAgZW5kVGltZTogdGltZU1zdCArIFwiOjMwXCIsXG4gICAgICAgICAgICAgICAgICAgIGN1c05hbWU6IFwiXCIsXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrRmxnOiAwLFxuICAgICAgICAgICAgICAgICAgICBlbmRGbGc6IDAsXG4gICAgICAgICAgICAgICAgICAgIGxvYWlLaGFjaDogJzAnXG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIHRoaXMudGltZUxpc3QucHVzaCh0aW1lMSk7XG4gICAgICAgICAgICAgICAgdmFyIGVuZCA9ICcnICt0aW1lTXN0ICsgMTtcbiAgICAgICAgICAgICAgICBpZiAoKGVuZCArICcnKS5sZW5ndGggPT0gMSkge1xuICAgICAgICAgICAgICAgICAgICBlbmQgPSAnMCcgKyBlbmQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciB0aW1lMSA9IHtcbiAgICAgICAgICAgICAgICAgICAgc3RUaW1lOiB0aW1lTXN0ICsgXCI6MzBcIixcbiAgICAgICAgICAgICAgICAgICAgZW5kVGltZTogZW5kICsgXCI6MDBcIixcbiAgICAgICAgICAgICAgICAgICAgY3VzTmFtZTogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgY2hlY2tGbGc6IDAsXG4gICAgICAgICAgICAgICAgICAgIGVuZEZsZzogMCxcbiAgICAgICAgICAgICAgICAgICAgbG9haUtoYWNoOiAnMCdcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIHRoaXMudGltZUxpc3QucHVzaCh0aW1lMSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLnNjaGVkdWxlTGlzdCA9IFtcbiAgICAgICAgICB7J3RpbWUnOiAnMjAxNy8wNi8yMSAnfSwgXG4gICAgICAgICAgeyd0aW1lJzogJzIwMTcvMDYvMjInfSxcbiAgICAgICAgICB7J3RpbWUnOiAnMjAxNy8wNi8yMyAnfSxcbiAgICAgICAgICB7J3RpbWUnOiAnMjAxNy8wNi8yNCd9LFxuICAgICAgICAgIHsndGltZSc6ICcyMDE3LzA2LzI1ICd9LFxuICAgICAgICAgIHsndGltZSc6ICcyMDE3LzA2LzI2ICd9LHsndGltZSc6ICcyMDE3LzA2LzI3ICd9XG4gICAgICAgICAgLHsndGltZSc6ICcyMDE3LzA2LzI4ICd9LFxuICAgICAgICAgIHsndGltZSc6ICcyMDE3LzA2LzI5ICd9LFxuICAgICAgICAgIHsndGltZSc6ICcyMDE3LzA2LzMwICd9XG4gICAgICBdO1xuXG4gICAgICBcblxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICBcbiAgfSBcblxuICAgcHVibGljIG9uU2VsZWN0RnJvbURhdGUodmFsdWU6IGFueSk6IHZvaWQge1xuICAgIFxuICB9XG4gIFxuICAgXG4gICBwdWJsaWMgcmVzZXJ2ZVRhYmxlKCkge1xuICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvcmVzZXJ2ZS10YWJsZS9jcmVhdGUvJyk7XG4gICB9XG5cblxufVxuIl19
