"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var SystemErrorComponent = (function () {
    function SystemErrorComponent() {
    }
    SystemErrorComponent.prototype.ngOnInit = function () {
    };
    SystemErrorComponent = __decorate([
        core_1.Component({
            selector: 'system-error',
            // templateUrl: './system-error.component.html',
            // styleUrls: ['./system-error.component.css']
            styleUrls: [utils_1.default.getView('app/components/system-error/system-error.component.css')],
            templateUrl: utils_1.default.getView('app/components/system-error/system-error.component.html')
        }),
        __metadata("design:paramtypes", [])
    ], SystemErrorComponent);
    return SystemErrorComponent;
}());
exports.SystemErrorComponent = SystemErrorComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3N5c3RlbS1lcnJvci9zeXN0ZW0tZXJyb3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHlDQUFvQztBQVNwQztJQUVFO0lBQWdCLENBQUM7SUFFakIsdUNBQVEsR0FBUjtJQUNBLENBQUM7SUFMVSxvQkFBb0I7UUFQaEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1lBQ3hCLGdEQUFnRDtZQUNoRCw4Q0FBOEM7WUFDOUMsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyx3REFBd0QsQ0FBQyxDQUFDO1lBQ3BGLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLHlEQUF5RCxDQUFDO1NBQ3RGLENBQUM7O09BQ1csb0JBQW9CLENBT2hDO0lBQUQsMkJBQUM7Q0FQRCxBQU9DLElBQUE7QUFQWSxvREFBb0IiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvc3lzdGVtLWVycm9yL3N5c3RlbS1lcnJvci5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3N5c3RlbS1lcnJvcicsXG4gIC8vIHRlbXBsYXRlVXJsOiAnLi9zeXN0ZW0tZXJyb3IuY29tcG9uZW50Lmh0bWwnLFxuICAvLyBzdHlsZVVybHM6IFsnLi9zeXN0ZW0tZXJyb3IuY29tcG9uZW50LmNzcyddXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL3N5c3RlbS1lcnJvci9zeXN0ZW0tZXJyb3IuY29tcG9uZW50LmNzcycpXSxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL3N5c3RlbS1lcnJvci9zeXN0ZW0tZXJyb3IuY29tcG9uZW50Lmh0bWwnKVxufSlcbmV4cG9ydCBjbGFzcyBTeXN0ZW1FcnJvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=
