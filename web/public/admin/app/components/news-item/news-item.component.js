"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
// import { Location } from '@angular/common';
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var news_service_1 = require("app/services/news.service");
var NewsItemComponent = (function () {
    function NewsItemComponent(newsServ, userServ, fb, router, activatedRoute, storageServ, notifyServ) {
        this.newsServ = newsServ;
        this.userServ = userServ;
        this.fb = fb;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.description = null;
        this.is_active = null;
        this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
        this.formErrors = {
            'title': []
        };
        this.validationMessages = {
            'title': {
                'required': 'The title field is required.',
                'maxlength': 'The title may not be greater than 1536 characters.'
            }
        };
        /*upload file*/
        this.sactiveColor = 'green';
        this.baseColor = '#ccc';
        this.overlayColor = 'rgba(255,255,255,0.5)';
        this.dragging = false;
        this.loaded = false;
        this.imageLoaded = false;
        this.iconColor = '';
        this.borderColor = '';
        this.activeColor = '';
        this.hiddenImage = false;
    }
    NewsItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.imageSrcInit = this.imageSrc;
        this.buildForm();
        this.mymodel = {
            id: null,
            title: null,
            description: null,
            is_active: null,
            thumb: null,
            is_delete: 0,
            slug: null
        };
        if (this.router.url.indexOf('/news/edit') == 0) {
            this.action = ['Edit', 'UPDATE', 'updated'];
            if (this.storageServ.scope) {
                this.mymodel = this.storageServ.scope;
                this.setVar();
            }
            else {
                var params = {
                    'id': this.activatedRoute.snapshot.params['id']
                };
                this.newsServ.getNews(params).then(function (response) {
                    _this.mymodel = response.news;
                    _this.setVar();
                }, function (error) {
                    console.log(error);
                });
            }
        }
        if (this.router.url.indexOf('/news/create') == 0) {
            this.action = ['Add', 'ADD', 'added'];
            this.storageServ.setScope(null);
            this.dtForm.controls['is_active'].setValue('1');
        }
        console.log('init news item component');
    };
    // doToBack() {
    //   this.location.back();
    // }
    NewsItemComponent.prototype.setVar = function () {
        // subscribe to router event
        // this.activatedRoute.params.subscribe((params: Params) => {
        //   if (params['id'] != null) {
        //     //this.mymodel = params;
        //     this.mymodel = {
        //       id : params['id'],
        //       title : params['title'],
        //       description : params['description'],
        //       is_active : params['is_active'],
        //       thumb : params['thumb'],
        //       is_delete : 0,
        //       slug : params['title']
        //     };
        //     this.title = params['title'];
        //     this.description = params['description'];
        //     this.is_active = params['is_active'];
        //     this.imageSrc = params['thumb'];
        //   }
        // });
        if (this.mymodel != null) {
            this.mymodel = {
                id: this.mymodel.id,
                title: this.mymodel.title,
                description: this.mymodel.description,
                is_active: this.mymodel.is_active.toString(),
                thumb: this.mymodel.thumb,
                is_delete: 0,
                slug: this.mymodel.title
            };
            this.dtForm.controls['title'].setValue(this.mymodel.title);
            this.dtForm.controls['description'].setValue(this.mymodel.description);
            this.dtForm.controls['is_active'].setValue(this.mymodel.is_active);
            this.title = this.dtForm.controls['title'];
            this.description = this.dtForm.controls['description'];
            this.is_active = this.dtForm.controls['is_active'];
            if (this.mymodel.thumb) {
                this.imageSrc = this.mymodel.thumb;
            }
        }
    };
    NewsItemComponent.prototype.buildForm = function () {
        var _this = this;
        this.dtForm = this.fb.group({
            'title': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(1536)]],
            'description': [],
            'is_active': []
        });
        this.title = this.dtForm.controls['title'];
        this.description = this.dtForm.controls['description'];
        this.is_active = this.dtForm.controls['is_active'];
        this.dtForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
        //this.onValueChanged(); // (re)set validation messages now
    };
    NewsItemComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.dtForm) {
            return;
        }
        var form = this.dtForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    NewsItemComponent.prototype.handleDragEnter = function () {
        this.dragging = true;
    };
    NewsItemComponent.prototype.handleDragLeave = function () {
        this.dragging = false;
    };
    NewsItemComponent.prototype.handleDrop = function (e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    };
    NewsItemComponent.prototype.handleImageLoad = function () {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if (this.imageLoaded = true) {
            this.hiddenImage = true;
        }
    };
    NewsItemComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        this.loaded = false;
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
    };
    NewsItemComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    };
    NewsItemComponent.prototype._setActive = function () {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    };
    NewsItemComponent.prototype._setInactive = function () {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    };
    NewsItemComponent.prototype.doSaveNews = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        //dto
        if (this.action[0] === 'Add') {
            this.mymodel.id = -1;
        }
        this.mymodel.title = this.title.value;
        this.mymodel.slug = this.title.value;
        this.mymodel.description = this.description.value;
        this.mymodel.is_active = this.is_active.value;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'news': this.mymodel
        };
        this.newsServ.updateNews(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            else if (_this.imageSrcInit !== _this.imageSrc && _this.mymodel.thumb !== _this.imageSrc) {
                _this.mymodel.id = response.news.id;
                _this.mymodel.thumb = _this.imageSrc;
                _this.newsServ.uploadFile(_this.mymodel).then(function (response) {
                    _this.notifyServ.success('News has been ' + _this.action[2]);
                    _this.router.navigateByUrl('news');
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.notifyServ.success('News has been ' + _this.action[2]);
                _this.router.navigateByUrl('news');
            }
        }, function (error) {
            console.log(error);
        });
    };
    ;
    NewsItemComponent = __decorate([
        core_1.Component({
            selector: 'news-item',
            templateUrl: utils_1.default.getView('app/components/news-item/news-item.component.html'),
            styleUrls: [utils_1.default.getView('app/components/news-item/news-item.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [news_service_1.NewsService,
            user_service_1.UserService,
            forms_1.FormBuilder,
            router_1.Router,
            router_1.ActivatedRoute,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], NewsItemComponent);
    return NewsItemComponent;
}());
exports.NewsItemComponent = NewsItemComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL25ld3MtaXRlbS9uZXdzLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELDBDQUFpRTtBQUNqRSx3Q0FBNkY7QUFDN0YsOENBQThDO0FBQzlDLHlDQUFvQztBQUVwQyw2REFBdUU7QUFDdkUsMERBQXdEO0FBQ3hELGdFQUE4RDtBQUM5RCwwRUFBd0U7QUFDeEUsMERBQXdEO0FBU3hEO0lBYUUsMkJBQ1UsUUFBcUIsRUFDckIsUUFBcUIsRUFDckIsRUFBZSxFQUNmLE1BQWMsRUFDZCxjQUE4QixFQUM5QixXQUEyQixFQUMzQixVQUErQjtRQU4vQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzNCLGVBQVUsR0FBVixVQUFVLENBQXFCO1FBWmpDLGdCQUFXLEdBQW9CLElBQUksQ0FBQztRQUNwQyxjQUFTLEdBQW9CLElBQUksQ0FBQztRQUVsQyxhQUFRLEdBQVcsaUVBQWlFLENBQUM7UUEwSTdGLGVBQVUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1NBQ1osQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLE9BQU8sRUFBRTtnQkFDUCxVQUFVLEVBQUUsOEJBQThCO2dCQUMxQyxXQUFXLEVBQUUsb0RBQW9EO2FBQ2xFO1NBQ0YsQ0FBQztRQUVGLGVBQWU7UUFDUCxpQkFBWSxHQUFXLE9BQU8sQ0FBQztRQUMvQixjQUFTLEdBQVcsTUFBTSxDQUFDO1FBQzNCLGlCQUFZLEdBQVcsdUJBQXVCLENBQUM7UUFDL0MsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUMxQixXQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFDdkIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsZ0JBQVcsR0FBWSxLQUFLLENBQUM7SUFySmpDLENBQUM7SUFFTCxvQ0FBUSxHQUFSO1FBQUEsaUJBNENDO1FBMUNDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFDLENBQUM7UUFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDYixFQUFFLEVBQUUsSUFBSTtZQUNSLEtBQUssRUFBRSxJQUFJO1lBQ1gsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLElBQUk7WUFDZixLQUFLLEVBQUUsSUFBSTtZQUNYLFNBQVMsRUFBRSxDQUFDO1lBQ1osSUFBSSxFQUFFLElBQUk7U0FDWCxDQUFDO1FBRUYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDNUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDaEIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLElBQUksTUFBTSxHQUFHO29CQUNYLElBQUksRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2lCQUNoRCxDQUFDO2dCQUNGLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7b0JBQ3pDLEtBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFDN0IsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNoQixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztRQUNILENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEQsQ0FBQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsZUFBZTtJQUNmLDBCQUEwQjtJQUMxQixJQUFJO0lBRUksa0NBQU0sR0FBZDtRQUVFLDRCQUE0QjtRQUM1Qiw2REFBNkQ7UUFDN0QsZ0NBQWdDO1FBQ2hDLCtCQUErQjtRQUMvQix1QkFBdUI7UUFDdkIsMkJBQTJCO1FBQzNCLGlDQUFpQztRQUNqQyw2Q0FBNkM7UUFDN0MseUNBQXlDO1FBQ3pDLGlDQUFpQztRQUNqQyx1QkFBdUI7UUFDdkIsK0JBQStCO1FBQy9CLFNBQVM7UUFDVCxvQ0FBb0M7UUFDcEMsZ0RBQWdEO1FBQ2hELDRDQUE0QztRQUM1Qyx1Q0FBdUM7UUFDdkMsTUFBTTtRQUNOLE1BQU07UUFFTixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRztnQkFDYixFQUFFLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNuQixLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUN6QixXQUFXLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFO2dCQUM1QyxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUN6QixTQUFTLEVBQUUsQ0FBQztnQkFDWixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2FBQ3pCLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN2RSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDckMsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRU8scUNBQVMsR0FBakI7UUFBQSxpQkFhQztRQVhDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDMUIsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNoRSxhQUFhLEVBQUUsRUFBRTtZQUNqQixXQUFXLEVBQUUsRUFBRTtTQUNoQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7YUFDckIsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztRQUN2RCwyREFBMkQ7SUFDN0QsQ0FBQztJQUVPLDBDQUFjLEdBQXRCLFVBQXVCLE9BQU8sRUFBRSxJQUFVO1FBRXhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQzdCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDekIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDN0QsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUF5QkQsMkNBQWUsR0FBZjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCwyQ0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVELHNDQUFVLEdBQVYsVUFBVyxDQUFDO1FBQ1YsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsMkNBQWUsR0FBZjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNuQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDMUIsQ0FBQztJQUNILENBQUM7SUFFRCw2Q0FBaUIsR0FBakIsVUFBa0IsQ0FBQztRQUNqQixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXhFLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVwQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsK0NBQW1CLEdBQW5CLFVBQW9CLENBQUM7UUFDbkIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVELHNDQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsQ0FBQztJQUNILENBQUM7SUFFRCx3Q0FBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2xDLENBQUM7SUFDSCxDQUFDO0lBRU8sc0NBQVUsR0FBbEI7UUFBQSxpQkE2Q0M7UUE1Q0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0gsQ0FBQztRQUVELEtBQUs7UUFDTCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDdkIsQ0FBQztRQUNELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQ2xELElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO1FBQzlDLElBQUksTUFBTSxHQUFHO1lBQ1gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU87U0FDckIsQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDNUMsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEQsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsTUFBTSxDQUFDO1lBQ1QsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxLQUFLLEtBQUksQ0FBQyxRQUFRLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEtBQUssS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZGLEtBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNuQyxLQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNuQyxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtvQkFDbEQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDcEMsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixDQUFDLENBQUMsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNELEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BDLENBQUM7UUFDSCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFBQSxDQUFDO0lBcFJTLGlCQUFpQjtRQVA3QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFdBQVc7WUFDckIsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsbURBQW1ELENBQUM7WUFDL0UsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO1lBQzlFLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUUscUJBQXFCLEVBQUUsRUFBRSxFQUFFO1NBQ3BDLENBQUM7eUNBZW9CLDBCQUFXO1lBQ1gsMEJBQVc7WUFDakIsbUJBQVc7WUFDUCxlQUFNO1lBQ0UsdUJBQWM7WUFDakIsZ0NBQWM7WUFDZiwwQ0FBbUI7T0FwQjlCLGlCQUFpQixDQXFSN0I7SUFBRCx3QkFBQztDQXJSRCxBQXFSQyxJQUFBO0FBclJZLDhDQUFpQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9uZXdzLWl0ZW0vbmV3cy1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG4vLyBpbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlLCByb3V0ZXJUcmFuc2l0aW9uIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZVwiO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBOZXdzU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9uZXdzLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZXdzLWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvbmV3cy1pdGVtL25ld3MtaXRlbS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9uZXdzLWl0ZW0vbmV3cy1pdGVtLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7ICdbQHJvdXRlclRyYW5zaXRpb25dJzogJycgfVxufSlcbmV4cG9ydCBjbGFzcyBOZXdzSXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgYWN0aW9uO1xuICBwcml2YXRlIGR0Rm9ybTogRm9ybUdyb3VwO1xuICBwcml2YXRlIHRpdGxlOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgZGVzY3JpcHRpb246IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgaXNfYWN0aXZlOiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIGltYWdlU3JjSW5pdDogYW55O1xuICBwcml2YXRlIGltYWdlU3JjOiBzdHJpbmcgPSAnaHR0cDovL3d3dy5wbGFjZWhvbGQuaXQvMjAweDE1MC9FRkVGRUYvQUFBQUFBJmFtcDt0ZXh0PW5vK2ltYWdlJztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIG5ld3NTZXJ2OiBOZXdzU2VydmljZSxcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gICAgbGV0IHVzZXJLZXkgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICBpZiAoIXVzZXJLZXkpIHtcbiAgICAgIHRoaXMudXNlclNlcnYubG9nb3V0KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMudXNlcklkID0gdXNlcktleS51c2VyX2luZm8uaWQ7XG4gICAgICB0aGlzLnJlc3RJZCA9IHVzZXJLZXkucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgfVxuICAgIHRoaXMuaW1hZ2VTcmNJbml0ID0gdGhpcy5pbWFnZVNyYztcbiAgICB0aGlzLmJ1aWxkRm9ybSgpO1xuICAgIHRoaXMubXltb2RlbCA9IHtcbiAgICAgIGlkOiBudWxsLFxuICAgICAgdGl0bGU6IG51bGwsXG4gICAgICBkZXNjcmlwdGlvbjogbnVsbCxcbiAgICAgIGlzX2FjdGl2ZTogbnVsbCxcbiAgICAgIHRodW1iOiBudWxsLFxuICAgICAgaXNfZGVsZXRlOiAwLFxuICAgICAgc2x1ZzogbnVsbFxuICAgIH07XG5cbiAgICBpZiAodGhpcy5yb3V0ZXIudXJsLmluZGV4T2YoJy9uZXdzL2VkaXQnKSA9PSAwKSB7XG4gICAgICB0aGlzLmFjdGlvbiA9IFsnRWRpdCcsICdVUERBVEUnLCAndXBkYXRlZCddO1xuICAgICAgaWYgKHRoaXMuc3RvcmFnZVNlcnYuc2NvcGUpIHtcbiAgICAgICAgdGhpcy5teW1vZGVsID0gdGhpcy5zdG9yYWdlU2Vydi5zY29wZTtcbiAgICAgICAgdGhpcy5zZXRWYXIoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAgICAgJ2lkJzogdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5wYXJhbXNbJ2lkJ11cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5uZXdzU2Vydi5nZXROZXdzKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgdGhpcy5teW1vZGVsID0gcmVzcG9uc2UubmV3cztcbiAgICAgICAgICB0aGlzLnNldFZhcigpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKHRoaXMucm91dGVyLnVybC5pbmRleE9mKCcvbmV3cy9jcmVhdGUnKSA9PSAwKSB7XG4gICAgICB0aGlzLmFjdGlvbiA9IFsnQWRkJywgJ0FERCcsICdhZGRlZCddO1xuICAgICAgdGhpcy5zdG9yYWdlU2Vydi5zZXRTY29wZShudWxsKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydpc19hY3RpdmUnXS5zZXRWYWx1ZSgnMScpO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZygnaW5pdCBuZXdzIGl0ZW0gY29tcG9uZW50Jyk7XG4gIH1cblxuICAvLyBkb1RvQmFjaygpIHtcbiAgLy8gICB0aGlzLmxvY2F0aW9uLmJhY2soKTtcbiAgLy8gfVxuXG4gIHByaXZhdGUgc2V0VmFyKCk6IHZvaWQge1xuXG4gICAgLy8gc3Vic2NyaWJlIHRvIHJvdXRlciBldmVudFxuICAgIC8vIHRoaXMuYWN0aXZhdGVkUm91dGUucGFyYW1zLnN1YnNjcmliZSgocGFyYW1zOiBQYXJhbXMpID0+IHtcbiAgICAvLyAgIGlmIChwYXJhbXNbJ2lkJ10gIT0gbnVsbCkge1xuICAgIC8vICAgICAvL3RoaXMubXltb2RlbCA9IHBhcmFtcztcbiAgICAvLyAgICAgdGhpcy5teW1vZGVsID0ge1xuICAgIC8vICAgICAgIGlkIDogcGFyYW1zWydpZCddLFxuICAgIC8vICAgICAgIHRpdGxlIDogcGFyYW1zWyd0aXRsZSddLFxuICAgIC8vICAgICAgIGRlc2NyaXB0aW9uIDogcGFyYW1zWydkZXNjcmlwdGlvbiddLFxuICAgIC8vICAgICAgIGlzX2FjdGl2ZSA6IHBhcmFtc1snaXNfYWN0aXZlJ10sXG4gICAgLy8gICAgICAgdGh1bWIgOiBwYXJhbXNbJ3RodW1iJ10sXG4gICAgLy8gICAgICAgaXNfZGVsZXRlIDogMCxcbiAgICAvLyAgICAgICBzbHVnIDogcGFyYW1zWyd0aXRsZSddXG4gICAgLy8gICAgIH07XG4gICAgLy8gICAgIHRoaXMudGl0bGUgPSBwYXJhbXNbJ3RpdGxlJ107XG4gICAgLy8gICAgIHRoaXMuZGVzY3JpcHRpb24gPSBwYXJhbXNbJ2Rlc2NyaXB0aW9uJ107XG4gICAgLy8gICAgIHRoaXMuaXNfYWN0aXZlID0gcGFyYW1zWydpc19hY3RpdmUnXTtcbiAgICAvLyAgICAgdGhpcy5pbWFnZVNyYyA9IHBhcmFtc1sndGh1bWInXTtcbiAgICAvLyAgIH1cbiAgICAvLyB9KTtcblxuICAgIGlmICh0aGlzLm15bW9kZWwgIT0gbnVsbCkge1xuICAgICAgdGhpcy5teW1vZGVsID0ge1xuICAgICAgICBpZDogdGhpcy5teW1vZGVsLmlkLFxuICAgICAgICB0aXRsZTogdGhpcy5teW1vZGVsLnRpdGxlLFxuICAgICAgICBkZXNjcmlwdGlvbjogdGhpcy5teW1vZGVsLmRlc2NyaXB0aW9uLFxuICAgICAgICBpc19hY3RpdmU6IHRoaXMubXltb2RlbC5pc19hY3RpdmUudG9TdHJpbmcoKSxcbiAgICAgICAgdGh1bWI6IHRoaXMubXltb2RlbC50aHVtYixcbiAgICAgICAgaXNfZGVsZXRlOiAwLFxuICAgICAgICBzbHVnOiB0aGlzLm15bW9kZWwudGl0bGVcbiAgICAgIH07XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1sndGl0bGUnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwudGl0bGUpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2Rlc2NyaXB0aW9uJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLmRlc2NyaXB0aW9uKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydpc19hY3RpdmUnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuaXNfYWN0aXZlKTtcbiAgICAgIHRoaXMudGl0bGUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1sndGl0bGUnXTtcbiAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXTtcbiAgICAgIHRoaXMuaXNfYWN0aXZlID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2lzX2FjdGl2ZSddO1xuICAgICAgaWYgKHRoaXMubXltb2RlbC50aHVtYikge1xuICAgICAgICB0aGlzLmltYWdlU3JjID0gdGhpcy5teW1vZGVsLnRodW1iO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xuXG4gICAgdGhpcy5kdEZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICAgICd0aXRsZSc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDE1MzYpXV0sXG4gICAgICAnZGVzY3JpcHRpb24nOiBbXSxcbiAgICAgICdpc19hY3RpdmUnOiBbXVxuICAgIH0pO1xuICAgIHRoaXMudGl0bGUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1sndGl0bGUnXTtcbiAgICB0aGlzLmRlc2NyaXB0aW9uID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2Rlc2NyaXB0aW9uJ107XG4gICAgdGhpcy5pc19hY3RpdmUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snaXNfYWN0aXZlJ107XG4gICAgdGhpcy5kdEZvcm0udmFsdWVDaGFuZ2VzXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5vblZhbHVlQ2hhbmdlZChmYWxzZSwgZGF0YSkpO1xuICAgIC8vdGhpcy5vblZhbHVlQ2hhbmdlZCgpOyAvLyAocmUpc2V0IHZhbGlkYXRpb24gbWVzc2FnZXMgbm93XG4gIH1cblxuICBwcml2YXRlIG9uVmFsdWVDaGFuZ2VkKHVuRGlydHksIGRhdGE/OiBhbnkpIHtcblxuICAgIGlmICghdGhpcy5kdEZvcm0pIHsgcmV0dXJuOyB9XG4gICAgY29uc3QgZm9ybSA9IHRoaXMuZHRGb3JtO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuICAgICAgaWYgKGNvbnRyb2wgJiYgKGNvbnRyb2wuZGlydHkgfHwgdW5EaXJ0eSkgJiYgY29udHJvbC5pbnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZvcm1FcnJvcnMgPSB7XG4gICAgJ3RpdGxlJzogW11cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ3RpdGxlJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSB0aXRsZSBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgdGl0bGUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTUzNiBjaGFyYWN0ZXJzLidcbiAgICB9XG4gIH07XG5cbiAgLyp1cGxvYWQgZmlsZSovXG4gIHByaXZhdGUgc2FjdGl2ZUNvbG9yOiBzdHJpbmcgPSAnZ3JlZW4nO1xuICBwcml2YXRlIGJhc2VDb2xvcjogc3RyaW5nID0gJyNjY2MnO1xuICBwcml2YXRlIG92ZXJsYXlDb2xvcjogc3RyaW5nID0gJ3JnYmEoMjU1LDI1NSwyNTUsMC41KSc7XG4gIHByaXZhdGUgZHJhZ2dpbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgcHJpdmF0ZSBsb2FkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgcHJpdmF0ZSBpbWFnZUxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBwcml2YXRlIGljb25Db2xvcjogc3RyaW5nID0gJyc7XG4gIHByaXZhdGUgYm9yZGVyQ29sb3I6IHN0cmluZyA9ICcnO1xuICBwcml2YXRlIGFjdGl2ZUNvbG9yOiBzdHJpbmcgPSAnJztcbiAgcHJpdmF0ZSBoaWRkZW5JbWFnZTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGhhbmRsZURyYWdFbnRlcigpIHtcbiAgICB0aGlzLmRyYWdnaW5nID0gdHJ1ZTtcbiAgfVxuXG4gIGhhbmRsZURyYWdMZWF2ZSgpIHtcbiAgICB0aGlzLmRyYWdnaW5nID0gZmFsc2U7XG4gIH1cblxuICBoYW5kbGVEcm9wKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgIHRoaXMuaGFuZGxlSW5wdXRDaGFuZ2UoZSk7XG4gIH1cblxuICBoYW5kbGVJbWFnZUxvYWQoKSB7XG4gICAgdGhpcy5pbWFnZUxvYWRlZCA9IHRydWU7XG4gICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLm92ZXJsYXlDb2xvcjtcbiAgICBpZiAodGhpcy5pbWFnZUxvYWRlZCA9IHRydWUpIHtcbiAgICAgIHRoaXMuaGlkZGVuSW1hZ2UgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGhhbmRsZUlucHV0Q2hhbmdlKGUpIHtcbiAgICB2YXIgZmlsZSA9IGUuZGF0YVRyYW5zZmVyID8gZS5kYXRhVHJhbnNmZXIuZmlsZXNbMF0gOiBlLnRhcmdldC5maWxlc1swXTtcblxuICAgIHZhciBwYXR0ZXJuID0gL2ltYWdlLSovO1xuICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXG4gICAgaWYgKCFmaWxlLnR5cGUubWF0Y2gocGF0dGVybikpIHtcbiAgICAgIGFsZXJ0KCdpbnZhbGlkIGZvcm1hdCcpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMubG9hZGVkID0gZmFsc2U7XG5cbiAgICByZWFkZXIub25sb2FkID0gdGhpcy5faGFuZGxlUmVhZGVyTG9hZGVkLmJpbmQodGhpcyk7XG4gICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XG4gIH1cblxuICBfaGFuZGxlUmVhZGVyTG9hZGVkKGUpIHtcbiAgICB2YXIgcmVhZGVyID0gZS50YXJnZXQ7XG4gICAgdGhpcy5pbWFnZVNyYyA9IHJlYWRlci5yZXN1bHQ7XG4gICAgdGhpcy5sb2FkZWQgPSB0cnVlO1xuICB9XG5cbiAgX3NldEFjdGl2ZSgpIHtcbiAgICB0aGlzLmJvcmRlckNvbG9yID0gdGhpcy5hY3RpdmVDb2xvcjtcbiAgICBpZiAodGhpcy5pbWFnZVNyYy5sZW5ndGggPT09IDApIHtcbiAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5hY3RpdmVDb2xvcjtcbiAgICB9XG4gIH1cblxuICBfc2V0SW5hY3RpdmUoKSB7XG4gICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYmFzZUNvbG9yO1xuICAgIGlmICh0aGlzLmltYWdlU3JjLmxlbmd0aCA9PT0gMCkge1xuICAgICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLmJhc2VDb2xvcjtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGRvU2F2ZU5ld3MoKSB7XG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCh0cnVlKTtcbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgaWYgKHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ubGVuZ3RoID4gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy9kdG9cbiAgICBpZiAodGhpcy5hY3Rpb25bMF0gPT09ICdBZGQnKSB7XG4gICAgICB0aGlzLm15bW9kZWwuaWQgPSAtMTtcbiAgICB9XG4gICAgdGhpcy5teW1vZGVsLnRpdGxlID0gdGhpcy50aXRsZS52YWx1ZTtcbiAgICB0aGlzLm15bW9kZWwuc2x1ZyA9IHRoaXMudGl0bGUudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmRlc2NyaXB0aW9uID0gdGhpcy5kZXNjcmlwdGlvbi52YWx1ZTtcbiAgICB0aGlzLm15bW9kZWwuaXNfYWN0aXZlID0gdGhpcy5pc19hY3RpdmUudmFsdWU7XG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICd1c2VyX2lkJzogdGhpcy51c2VySWQsXG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgJ25ld3MnOiB0aGlzLm15bW9kZWxcbiAgICB9O1xuICAgIHRoaXMubmV3c1NlcnYudXBkYXRlTmV3cyhwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgbGV0IGhhckVycm9yID0gZmFsc2U7XG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHJlc3BvbnNlLmVycm9ycykge1xuICAgICAgICBoYXJFcnJvciA9IHRydWU7XG4gICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSByZXNwb25zZS5lcnJvcnNbZmllbGRdO1xuICAgICAgfVxuICAgICAgaWYgKGhhckVycm9yKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5pbWFnZVNyY0luaXQgIT09IHRoaXMuaW1hZ2VTcmMgJiYgdGhpcy5teW1vZGVsLnRodW1iICE9PSB0aGlzLmltYWdlU3JjKSB7XG4gICAgICAgIHRoaXMubXltb2RlbC5pZCA9IHJlc3BvbnNlLm5ld3MuaWQ7XG4gICAgICAgIHRoaXMubXltb2RlbC50aHVtYiA9IHRoaXMuaW1hZ2VTcmM7XG4gICAgICAgIHRoaXMubmV3c1NlcnYudXBsb2FkRmlsZSh0aGlzLm15bW9kZWwpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCdOZXdzIGhhcyBiZWVuICcgKyB0aGlzLmFjdGlvblsyXSk7XG4gICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnbmV3cycpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCdOZXdzIGhhcyBiZWVuICcgKyB0aGlzLmFjdGlvblsyXSk7XG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ25ld3MnKTtcbiAgICAgIH1cbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH07XG59XG4iXX0=
