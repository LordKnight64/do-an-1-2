"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var email_service_1 = require("app/services/email.service");
var router_1 = require("@angular/router");
var ViewEmailMarketingComponent = (function () {
    function ViewEmailMarketingComponent(emailServ, userServ, router, route, storageServ, notifyServ) {
        this.emailServ = emailServ;
        this.userServ = userServ;
        this.router = router;
        this.route = route;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.id = 1;
        this.email = {};
        this.currentPage = 1;
        this.totalItems = 0;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    ViewEmailMarketingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            var userKey = JSON.parse(localStorage.getItem('user_key'));
            if (!userKey) {
                _this.userServ.logout();
            }
            _this.doSearch();
        });
    };
    ViewEmailMarketingComponent.prototype.doSearch = function () {
        var _this = this;
        var params = {
            'id': this.id
        };
        this.emailServ.getEmailMarketingDetail(params).then(function (response) {
            _this.email = response.email[0];
            console.log(_this.email);
        }, function (error) {
            console.log(error);
        });
    };
    ViewEmailMarketingComponent.prototype.doDeleteSearch = function () {
        var id = 1;
        var params = {
            'id': id
        };
        var me = this;
        this.emailServ.deleteEmailMarketingList(params).then(function (response) {
            if (response.return_cd == 0) {
                me.notifyServ.success('Search Items has been deleted');
                me.doSearch();
            }
        }, function (error) {
            console.log(error);
        });
    };
    ViewEmailMarketingComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    ViewEmailMarketingComponent.prototype.doBack = function () {
        this.router.navigateByUrl('/email-marketing-list');
    };
    ViewEmailMarketingComponent = __decorate([
        core_1.Component({
            selector: 'table-list',
            templateUrl: utils_1.default.getView('app/components/view-email-marketing/view-email-marketing.component.html'),
            styleUrls: [utils_1.default.getView('app/components/view-email-marketing/view-email-marketing.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [email_service_1.EmailService,
            user_service_1.UserService,
            router_1.Router,
            router_1.ActivatedRoute,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], ViewEmailMarketingComponent);
    return ViewEmailMarketingComponent;
}());
exports.ViewEmailMarketingComponent = ViewEmailMarketingComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3ZpZXctZW1haWwtbWFya2V0aW5nL3ZpZXctZW1haWwtbWFya2V0aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFFcEMsNkRBQXVFO0FBQ3ZFLDBEQUF3RDtBQUN4RCxnRUFBOEQ7QUFDOUQsMEVBQXdFO0FBQ3hFLDREQUEwRDtBQUMxRCwwQ0FBeUQ7QUFTekQ7SUFVRSxxQ0FDVSxTQUF1QixFQUN2QixRQUFxQixFQUNyQixNQUFjLEVBQ2QsS0FBcUIsRUFDckIsV0FBMkIsRUFDM0IsVUFBK0I7UUFML0IsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDM0IsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFmaEMsT0FBRSxHQUFXLENBQUMsQ0FBQztRQUVoQixVQUFLLEdBQVEsRUFBRSxDQUFDO1FBRWhCLGdCQUFXLEdBQVcsQ0FBQyxDQUFDO1FBQ3hCLGVBQVUsR0FBVyxDQUFDLENBQUM7UUFDdkIsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFXLEVBQUUsQ0FBQztJQVN6QixDQUFDO0lBRUwsOENBQVEsR0FBUjtRQUFBLGlCQWFDO1FBVkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUNoQyxLQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV0QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUMxRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUN6QixDQUFDO1lBQ0QsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVPLDhDQUFRLEdBQWhCO1FBQUEsaUJBWUM7UUFUQyxJQUFJLE1BQU0sR0FBRztZQUNYLElBQUksRUFBRyxJQUFJLENBQUMsRUFBRTtTQUNmLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDeEQsS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUlPLG9EQUFjLEdBQXRCO1FBRUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsSUFBSSxNQUFNLEdBQUc7WUFDWCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7UUFDRixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDM0QsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO2dCQUN2RCxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsQ0FBQztRQUNILENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGdEQUFVLEdBQWxCLFVBQW1CLEdBQUc7UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVPLDRDQUFNLEdBQWQ7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUF6RVUsMkJBQTJCO1FBUHRDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx5RUFBeUUsQ0FBQztZQUNyRyxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHdFQUF3RSxDQUFDLENBQUM7WUFDcEcsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0FZcUIsNEJBQVk7WUFDYiwwQkFBVztZQUNiLGVBQU07WUFDUCx1QkFBYztZQUNSLGdDQUFjO1lBQ2YsMENBQW1CO09BaEI5QiwyQkFBMkIsQ0EyRXZDO0lBQUQsa0NBQUM7Q0EzRUQsQUEyRUMsSUFBQTtBQTNFWSxrRUFBMkIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvdmlldy1lbWFpbC1tYXJrZXRpbmcvdmlldy1lbWFpbC1tYXJrZXRpbmcuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IEVtYWlsU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9lbWFpbC5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG4gQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndGFibGUtbGlzdCcsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy92aWV3LWVtYWlsLW1hcmtldGluZy92aWV3LWVtYWlsLW1hcmtldGluZy5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy92aWV3LWVtYWlsLW1hcmtldGluZy92aWV3LWVtYWlsLW1hcmtldGluZy5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJyd9XG59KVxuZXhwb3J0IGNsYXNzIFZpZXdFbWFpbE1hcmtldGluZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICBwcml2YXRlIGlkOiBudW1iZXIgPSAxO1xuICBwcml2YXRlIHVzZXJJZDtcbiAgcHJpdmF0ZSBlbWFpbDogYW55ID0ge307XG4gIHByaXZhdGUgZGF0YTogYW55O1xuICBwcml2YXRlIGN1cnJlbnRQYWdlOiBudW1iZXIgPSAxO1xuICBwcml2YXRlIHRvdGFsSXRlbXM6IG51bWJlciA9IDA7XG4gIHByaXZhdGUgaXRlbXNQZXJQYWdlOiBudW1iZXIgPSAxMDtcbiAgcHJpdmF0ZSBtYXhTaXplOiBudW1iZXIgPSAxMDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGVtYWlsU2VydjogRW1haWxTZXJ2aWNlLFxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSBzdG9yYWdlU2VydjogU3RvcmFnZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBub3RpZnlTZXJ2OiBOb3RpZmljYXRpb25TZXJ2aWNlXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgIFxuICAgIHRoaXMucm91dGUucGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgICAgdGhpcy5pZCA9IHBhcmFtc1snaWQnXTsgXG4gICAgICBcbiAgICAgICBsZXQgdXNlcktleSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgICAgICBpZiAoIXVzZXJLZXkpIHtcbiAgICAgICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgICAgICB9IFxuICAgICAgICB0aGlzLmRvU2VhcmNoKCk7XG4gICAgfSk7XG5cbiAgfVxuXG4gIHByaXZhdGUgZG9TZWFyY2goKSB7XG4gICBcbiAgIFxuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAnaWQnIDogdGhpcy5pZFxuICAgIH07XG4gICAgdGhpcy5lbWFpbFNlcnYuZ2V0RW1haWxNYXJrZXRpbmdEZXRhaWwocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5lbWFpbCA9IHJlc3BvbnNlLmVtYWlsWzBdO1xuICAgICAgICBjb25zb2xlLmxvZyggdGhpcy5lbWFpbCk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cblxuXG4gIHByaXZhdGUgZG9EZWxldGVTZWFyY2goKSB7XG4gICBcbiAgICBsZXQgaWQgPSAxO1xuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAnaWQnOiBpZFxuICAgIH07XG4gICAgdmFyIG1lID0gdGhpcztcbiAgICB0aGlzLmVtYWlsU2Vydi5kZWxldGVFbWFpbE1hcmtldGluZ0xpc3QocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIGlmKHJlc3BvbnNlLnJldHVybl9jZCA9PSAwKSB7XG4gICAgICAgIG1lLm5vdGlmeVNlcnYuc3VjY2VzcygnU2VhcmNoIEl0ZW1zIGhhcyBiZWVuIGRlbGV0ZWQnKTtcbiAgICAgICAgbWUuZG9TZWFyY2goKTtcbiAgICAgIH1cbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRvU2V0U2NvcGUob2JqKSB7XG4gICAgdGhpcy5zdG9yYWdlU2Vydi5zZXRTY29wZShvYmopO1xuICB9XG5cbiAgcHJpdmF0ZSBkb0JhY2soKSB7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvZW1haWwtbWFya2V0aW5nLWxpc3QnKTtcbiAgfVxuXG59XG4iXX0=
