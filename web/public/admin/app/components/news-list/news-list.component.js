"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var news_service_1 = require("app/services/news.service");
var NewsListComponent = (function () {
    function NewsListComponent(newsServ, userServ, storageServ, notifyServ) {
        this.newsServ = newsServ;
        this.userServ = userServ;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    NewsListComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.doSearch();
    };
    NewsListComponent.prototype.doSearch = function () {
        var _this = this;
        var params = {
            'rest_id': this.restId
        };
        this.newsServ.getNewsList(params).then(function (response) {
            _this.newsList = response.news;
            _this.totalItems = _this.newsList.length;
            for (var i = 0; i < _this.totalItems; i++) {
                _this.newsList[i].description = _this.ConvertBR(_this.newsList[i].description);
            }
            _this.pageChanged({ page: _this.currentPage, itemsPerPage: _this.itemsPerPage });
        }, function (error) {
            console.log(error);
        });
    };
    NewsListComponent.prototype.pageChanged = function (event) {
        var start = (event.page - 1) * event.itemsPerPage;
        var end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
        this.data = this.newsList.slice(start, end);
    };
    NewsListComponent.prototype.doDeleteNews = function (obj) {
        var _this = this;
        obj.is_delete = 1;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'news': obj
        };
        this.newsServ.updateNews(params).then(function (response) {
            _this.newsServ.deleteFile({ 'thumb': response.news.thumb }).then(function (response) {
                _this.notifyServ.success('News has been deleted');
                _this.doSearch();
            }, function (error) {
                console.log(error);
            });
        }, function (error) {
            console.log(error);
        });
    };
    NewsListComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    NewsListComponent.prototype.ConvertBR = function (input) {
        console.log("convert");
        var output = "";
        for (var i = 0; i < input.length; i++) {
            if (i + 1 < input.length && input.charCodeAt(i) == 10) {
                output += "<br>";
            }
            else {
                output += input.charAt(i);
            }
        }
        return output;
    };
    NewsListComponent = __decorate([
        core_1.Component({
            selector: 'news-list',
            templateUrl: utils_1.default.getView('app/components/news-list/news-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/news-list/news-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [news_service_1.NewsService,
            user_service_1.UserService,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], NewsListComponent);
    return NewsListComponent;
}());
exports.NewsListComponent = NewsListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL25ld3MtbGlzdC9uZXdzLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHlDQUFvQztBQUVwQyw2REFBdUU7QUFDdkUsMERBQXdEO0FBQ3hELGdFQUE4RDtBQUM5RCwwRUFBd0U7QUFDeEUsMERBQXdEO0FBU3hEO0lBV0UsMkJBQ1UsUUFBcUIsRUFDckIsUUFBcUIsRUFDckIsV0FBMkIsRUFDM0IsVUFBK0I7UUFIL0IsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUMzQixlQUFVLEdBQVYsVUFBVSxDQUFxQjtRQVRqQyxnQkFBVyxHQUFXLENBQUMsQ0FBQztRQUV4QixpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUMxQixZQUFPLEdBQVcsRUFBRSxDQUFDO0lBT3pCLENBQUM7SUFFTCxvQ0FBUSxHQUFSO1FBQ0UsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUMsQ0FBQztRQUVELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRU8sb0NBQVEsR0FBaEI7UUFBQSxpQkFlQztRQWRDLElBQUksTUFBTSxHQUFHO1lBQ1gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3ZCLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQzdDLEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUM5QixLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1lBQ3ZDLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLEdBQUMsS0FBSSxDQUFDLFVBQVUsRUFBQyxDQUFDLEVBQUUsRUFDbkMsQ0FBQztnQkFDQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUUsQ0FBQztZQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSSxDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7UUFDaEYsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sdUNBQVcsR0FBbEIsVUFBbUIsS0FBVTtRQUMzQixJQUFJLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQztRQUNsRCxJQUFJLEdBQUcsR0FBRyxLQUFLLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUN4RixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUU5QyxDQUFDO0lBRU8sd0NBQVksR0FBcEIsVUFBcUIsR0FBRztRQUF4QixpQkFpQkM7UUFoQkMsR0FBRyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDbEIsSUFBSSxNQUFNLEdBQUc7WUFDWCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLE1BQU0sRUFBRSxHQUFHO1NBQ1osQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDNUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7Z0JBQ3RFLEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQ2pELEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNsQixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxzQ0FBVSxHQUFsQixVQUFtQixHQUFHO1FBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFJTyxxQ0FBUyxHQUFqQixVQUFrQixLQUFLO1FBRXJCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdkIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBRTlCLEVBQUUsQ0FBQSxDQUFDLENBQUMsR0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFFLEVBQUUsQ0FBQyxDQUNqRCxDQUFDO2dCQUNHLE1BQU0sSUFBSSxNQUFNLENBQUM7WUFDckIsQ0FBQztZQUNELElBQUksQ0FDSixDQUFDO2dCQUNHLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLENBQUM7UUFDWCxDQUFDO1FBRUQsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNkLENBQUM7SUFoR1UsaUJBQWlCO1FBUDdCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxtREFBbUQsQ0FBQztZQUMvRSxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7WUFDOUUsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBRSxxQkFBcUIsRUFBRSxFQUFFLEVBQUU7U0FDcEMsQ0FBQzt5Q0Fhb0IsMEJBQVc7WUFDWCwwQkFBVztZQUNSLGdDQUFjO1lBQ2YsMENBQW1CO09BZjlCLGlCQUFpQixDQWlHN0I7SUFBRCx3QkFBQztDQWpHRCxBQWlHQyxJQUFBO0FBakdZLDhDQUFpQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9uZXdzLWxpc3QvbmV3cy1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlLCByb3V0ZXJUcmFuc2l0aW9uIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZVwiO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBOZXdzU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9uZXdzLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZXdzLWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvbmV3cy1saXN0L25ld3MtbGlzdC5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9uZXdzLWxpc3QvbmV3cy1saXN0LmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7ICdbQHJvdXRlclRyYW5zaXRpb25dJzogJycgfVxufSlcbmV4cG9ydCBjbGFzcyBOZXdzTGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHJpdmF0ZSByZXN0SWQ7XG4gIHByaXZhdGUgdXNlcklkO1xuICBwcml2YXRlIG5ld3NMaXN0OiBhbnk7XG4gIHByaXZhdGUgZGF0YTogYW55O1xuICBwcml2YXRlIGN1cnJlbnRQYWdlOiBudW1iZXIgPSAxO1xuICBwcml2YXRlIHRvdGFsSXRlbXM6IG51bWJlcjtcbiAgcHJpdmF0ZSBpdGVtc1BlclBhZ2U6IG51bWJlciA9IDEwO1xuICBwcml2YXRlIG1heFNpemU6IG51bWJlciA9IDEwO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgbmV3c1NlcnY6IE5ld3NTZXJ2aWNlLFxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVzZXJJZCA9IHVzZXJLZXkudXNlcl9pbmZvLmlkO1xuICAgICAgdGhpcy5yZXN0SWQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIH1cblxuICAgIHRoaXMuZG9TZWFyY2goKTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZWFyY2goKSB7XG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWRcbiAgICB9O1xuICAgIHRoaXMubmV3c1NlcnYuZ2V0TmV3c0xpc3QocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMubmV3c0xpc3QgPSByZXNwb25zZS5uZXdzO1xuICAgICAgdGhpcy50b3RhbEl0ZW1zID0gdGhpcy5uZXdzTGlzdC5sZW5ndGg7XG4gICAgICBmb3IodmFyIGkgPSAwO2k8dGhpcy50b3RhbEl0ZW1zO2krKylcbiAgICAgIHtcbiAgICAgICAgdGhpcy5uZXdzTGlzdFtpXS5kZXNjcmlwdGlvbiA9IHRoaXMuQ29udmVydEJSKHRoaXMubmV3c0xpc3RbaV0uZGVzY3JpcHRpb24pO1xuICAgICAgfVxuICAgICAgdGhpcy5wYWdlQ2hhbmdlZCh7IHBhZ2U6IHRoaXMuY3VycmVudFBhZ2UsIGl0ZW1zUGVyUGFnZTogdGhpcy5pdGVtc1BlclBhZ2UgfSk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHBhZ2VDaGFuZ2VkKGV2ZW50OiBhbnkpOiB2b2lkIHtcbiAgICBsZXQgc3RhcnQgPSAoZXZlbnQucGFnZSAtIDEpICogZXZlbnQuaXRlbXNQZXJQYWdlO1xuICAgIGxldCBlbmQgPSBldmVudC5pdGVtc1BlclBhZ2UgPiAtMSA/IChzdGFydCArIGV2ZW50Lml0ZW1zUGVyUGFnZSkgOiB0aGlzLm5ld3NMaXN0Lmxlbmd0aDtcbiAgICB0aGlzLmRhdGEgPSB0aGlzLm5ld3NMaXN0LnNsaWNlKHN0YXJ0LCBlbmQpO1xuICAgIFxuICB9XG5cbiAgcHJpdmF0ZSBkb0RlbGV0ZU5ld3Mob2JqKSB7XG4gICAgb2JqLmlzX2RlbGV0ZSA9IDE7XG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICd1c2VyX2lkJzogdGhpcy51c2VySWQsXG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgJ25ld3MnOiBvYmpcbiAgICB9O1xuICAgIHRoaXMubmV3c1NlcnYudXBkYXRlTmV3cyhwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgdGhpcy5uZXdzU2Vydi5kZWxldGVGaWxlKHsgJ3RodW1iJzogcmVzcG9uc2UubmV3cy50aHVtYiB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5ub3RpZnlTZXJ2LnN1Y2Nlc3MoJ05ld3MgaGFzIGJlZW4gZGVsZXRlZCcpO1xuICAgICAgICB0aGlzLmRvU2VhcmNoKCk7XG4gICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIH0pO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZXRTY29wZShvYmopIHtcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2LnNldFNjb3BlKG9iaik7XG4gIH1cblxuICBcblxuICBwcml2YXRlIENvbnZlcnRCUihpbnB1dClcbiAge1xuICAgIGNvbnNvbGUubG9nKFwiY29udmVydFwiKTtcbiAgICB2YXIgb3V0cHV0ID0gXCJcIjtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGlucHV0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmKGkrMSA8IGlucHV0Lmxlbmd0aCAmJiBpbnB1dC5jaGFyQ29kZUF0KGkpPT0xMClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBvdXRwdXQgKz0gXCI8YnI+XCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgb3V0cHV0ICs9IGlucHV0LmNoYXJBdChpKTtcbiAgICAgICAgICAgIH1cblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIG91dHB1dDtcbiAgfVxufVxuIl19
