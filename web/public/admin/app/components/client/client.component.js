"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var client_1 = require("app/models/client");
// import { ClientDAL } from '../../dal/client.dal';
var breadcrumb_service_1 = require("app/services/breadcrumb.service");
var utils_1 = require("app/utils/utils");
var notification_service_1 = require("app/services/notification.service");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var router_animations_1 = require("../../router.animations");
// import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';
// import { ModalDirective } from 'ng2-bootstrap/ng2-bootstrap';
var objectAssign = require("object-assign");
var ngx_bootstrap_2 = require("ngx-bootstrap");
var auth_service_1 = require("app/services/auth.service");
var ClientComponent = (function () {
    function ClientComponent(breadServ, notif, authServ
        // private childModal:ModalDirective
    ) {
        var _this = this;
        this.breadServ = breadServ;
        this.notif = notif;
        this.authServ = authServ;
        // private clients: FirebaseListObservable<Array<Client>>;
        // constructor(private dal: ClientDAL, private breadServ: BreadcrumbService) {
        //   // TODO
        // }
        this.singleModel = '1';
        this.checkModel = { left: false, middle: true, right: false };
        this.radioModel = 'Middle';
        this.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };
        // public items: string[] = ['Item 1', 'Item 2', 'Item 3'];
        // public groups: any[] = [
        //   {
        //     title: 'Dynamic Group Header - 1',
        //     content: 'Dynamic Group Body - 1'
        //   },
        //   {
        //     title: 'Dynamic Group Header - 2',
        //     content: 'Dynamic Group Body - 2'
        //   }
        // ];
        this.isFirstOpen = true;
        this.customClass = 'customClass';
        this.alerts = [
            {
                type: 'success',
                msg: "You successfully read this important alert message."
            },
            {
                type: 'info',
                msg: "This alert needs your attention, but it's not super important."
            },
            {
                type: 'danger',
                msg: "Better check yourself, you're not looking too good."
            }
        ];
        this.isCollapsed = false;
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
        this.items = ['Amsterdam', 'Antwerp', 'Athens', 'Barcelona',
            'Berlin', 'Birmingham', 'Bradford', 'Bremen', 'Brussels', 'Bucharest',
            'Budapest', 'Cologne', 'Copenhagen', 'Dortmund', 'Dresden', 'Dublin',
            'Düsseldorf', 'Essen', 'Frankfurt', 'Genoa', 'Glasgow', 'Gothenburg',
            'Hamburg', 'Hannover', 'Helsinki', 'Kraków', 'Leeds', 'Leipzig', 'Lisbon',
            'London', 'Madrid', 'Manchester', 'Marseille', 'Milan', 'Munich', 'Málaga',
            'Naples', 'Palermo', 'Paris', 'Poznań', 'Prague', 'Riga', 'Rome',
            'Rotterdam', 'Seville', 'Sheffield', 'Sofia', 'Stockholm', 'Stuttgart',
            'The Hague', 'Turin', 'Valencia', 'Vienna', 'Vilnius', 'Warsaw', 'Wrocław',
            'Zagreb', 'Zaragoza', 'Łódź'];
        this.save = function (client) {
            // this.dal.update(client.clientId, new Client(client.name, client.clientId, client.address));
        };
        this.delete = function (client) {
            // this.dal.delete(client);
        };
        this.add = function () {
            _this.notif.success('New client has been added');
            // this.dal.create(new Client());
        };
    }
    ClientComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.clients = this.dal.readAll();
        this.authServ.getToken().then(function (response) { return _this.processResult(response); }, function (error) { return _this.handleError(error); });
    };
    ClientComponent.prototype.ngOnDestroy = function () {
        // this.breadServ.clear();
        this.clients = null;
    };
    ClientComponent.prototype.showChildModal = function () {
        this.childModal.show();
    };
    // public addItem(): void {
    //   this.items.push(`Items ${this.items.length + 1}`);
    // }
    ClientComponent.prototype.reset = function () {
        // console.log('-----object-----', Object.);
        // console.log('-----objectAssign-----', objectAssign());
        this.alerts = this.alerts.map(function (alert) { return objectAssign({}, alert); });
    };
    ClientComponent.prototype.collapsed = function (event) {
        console.log('collapsed', event);
    };
    ClientComponent.prototype.expanded = function (event) {
        console.log('expanded', event);
    };
    ClientComponent.prototype.processResult = function (res) {
        this.clients = [new client_1.Client()];
        this.breadServ.set({
            header: "Client",
            description: 'This is our Client page',
            display: true,
            levels: [
                {
                    icon: 'dashboard',
                    link: ['/'],
                    title: 'Dashboard',
                    state: 'app'
                },
                {
                    icon: 'clock-o',
                    link: ['/client'],
                    title: 'Client',
                    state: 'app.client'
                }
            ]
        });
    };
    ClientComponent.prototype.handleError = function (error) {
        console.log(error);
    };
    Object.defineProperty(ClientComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    ClientComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
    };
    ClientComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    ClientComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    ClientComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    __decorate([
        core_1.ViewChild('childModal'),
        __metadata("design:type", ngx_bootstrap_1.ModalDirective)
    ], ClientComponent.prototype, "childModal", void 0);
    ClientComponent = __decorate([
        core_1.Component({
            // providers: [ClientDAL],
            selector: 'app-client',
            styleUrls: [utils_1.default.getView('app/components/client/client.component.css')],
            animations: [router_animations_1.routerFade()],
            host: { '[@routerFade]': '' },
            //  styles: [`
            // :host >>> .alert-md-color {
            //   background-color: #009688;
            //   border-color: #00695C;
            //   color: #fff;
            // }
            // `],
            templateUrl: utils_1.default.getView('app/components/client/client.component.html'),
            // encapsulation: ViewEncapsulation.None
            providers: [{ provide: ngx_bootstrap_2.CarouselConfig, useValue: { interval: 1500, noPause: true } }]
        }),
        __metadata("design:paramtypes", [breadcrumb_service_1.BreadcrumbService,
            notification_service_1.NotificationService,
            auth_service_1.AuthService
            // private childModal:ModalDirective
        ])
    ], ClientComponent);
    return ClientComponent;
}());
exports.ClientComponent = ClientComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NsaWVudC9jbGllbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJGO0FBQzNGLDRDQUEyQztBQUMzQyxvREFBb0Q7QUFDcEQsc0VBQW9FO0FBQ3BFLHlDQUFvQztBQUNwQywwRUFBd0U7QUFDeEUsK0NBQStDO0FBQy9DLDZEQUFxRDtBQUNyRCx3RUFBd0U7QUFDeEUsZ0VBQWdFO0FBQ2hFLDRDQUErQztBQUMvQywrQ0FBK0M7QUFDL0MsMERBQXdEO0FBa0J4RDtJQWdFRSx5QkFDVSxTQUE0QixFQUM1QixLQUEwQixFQUN6QixRQUFxQjtRQUM5QixvQ0FBb0M7O1FBSnRDLGlCQU9DO1FBTlMsY0FBUyxHQUFULFNBQVMsQ0FBbUI7UUFDNUIsVUFBSyxHQUFMLEtBQUssQ0FBcUI7UUFDekIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQWpFaEMsMERBQTBEO1FBRTFELDhFQUE4RTtRQUM5RSxZQUFZO1FBQ1osSUFBSTtRQUVJLGdCQUFXLEdBQVcsR0FBRyxDQUFDO1FBQzNCLGVBQVUsR0FBUSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUM7UUFDOUQsZUFBVSxHQUFXLFFBQVEsQ0FBQztRQUU5QixXQUFNLEdBQVE7WUFDbkIsV0FBVyxFQUFFLElBQUk7WUFDakIsZUFBZSxFQUFFLEtBQUs7U0FDdkIsQ0FBQztRQUNGLDJEQUEyRDtRQUMzRCwyQkFBMkI7UUFDM0IsTUFBTTtRQUNOLHlDQUF5QztRQUN6Qyx3Q0FBd0M7UUFDeEMsT0FBTztRQUNQLE1BQU07UUFDTix5Q0FBeUM7UUFDekMsd0NBQXdDO1FBQ3hDLE1BQU07UUFDTixLQUFLO1FBRUUsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFDNUIsZ0JBQVcsR0FBVyxhQUFhLENBQUM7UUFFcEMsV0FBTSxHQUFRO1lBQ25CO2dCQUNFLElBQUksRUFBRSxTQUFTO2dCQUNmLEdBQUcsRUFBRSxxREFBcUQ7YUFDM0Q7WUFDRDtnQkFDRSxJQUFJLEVBQUUsTUFBTTtnQkFDWixHQUFHLEVBQUUsZ0VBQWdFO2FBQ3RFO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsR0FBRyxFQUFFLHFEQUFxRDthQUMzRDtTQUNGLENBQUM7UUFFSyxnQkFBVyxHQUFXLEtBQUssQ0FBQztRQUUzQixVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2YsZUFBVSxHQUFVLEdBQUcsQ0FBQztRQUN4QixhQUFRLEdBQVcsS0FBSyxDQUFDO1FBRTFCLFVBQUssR0FBaUIsQ0FBQyxXQUFXLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxXQUFXO1lBQ3pFLFFBQVEsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsV0FBVztZQUNyRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFFBQVE7WUFDcEUsWUFBWSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZO1lBQ3BFLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVE7WUFDekUsUUFBUSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUTtZQUMxRSxRQUFRLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxNQUFNO1lBQ2hFLFdBQVcsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsV0FBVztZQUN0RSxXQUFXLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxTQUFTO1lBQzFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7UUF5QnhCLFNBQUksR0FBRyxVQUFDLE1BQWM7WUFDNUIsOEZBQThGO1FBQ2hHLENBQUMsQ0FBQTtRQUVPLFdBQU0sR0FBRyxVQUFDLE1BQWM7WUFDOUIsMkJBQTJCO1FBQzdCLENBQUMsQ0FBQTtRQUVPLFFBQUcsR0FBRztZQUNaLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLDJCQUEyQixDQUFDLENBQUE7WUFDL0MsaUNBQWlDO1FBQ25DLENBQUMsQ0FBQTtJQTFCRCxDQUFDO0lBRU0sa0NBQVEsR0FBZjtRQUFBLGlCQU1DO1FBTEMscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUMxQixVQUFBLFFBQVEsSUFBSSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEVBQTVCLENBQTRCLEVBQ3hDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsQ0FDbEMsQ0FBQztJQUNKLENBQUM7SUFFTSxxQ0FBVyxHQUFsQjtRQUNFLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDO0lBZU0sd0NBQWMsR0FBckI7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCwyQkFBMkI7SUFDM0IsdURBQXVEO0lBQ3ZELElBQUk7SUFFRywrQkFBSyxHQUFaO1FBQ0UsNENBQTRDO1FBQzVDLHlEQUF5RDtRQUN6RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQUMsS0FBVSxJQUFLLE9BQUEsWUFBWSxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFTSxtQ0FBUyxHQUFoQixVQUFpQixLQUFTO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFTSxrQ0FBUSxHQUFmLFVBQWdCLEtBQVM7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVPLHVDQUFhLEdBQXJCLFVBQXNCLEdBQUc7UUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksZUFBTSxFQUFFLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztZQUNqQixNQUFNLEVBQUMsUUFBUTtZQUNmLFdBQVcsRUFBRSx5QkFBeUI7WUFDdEMsT0FBTyxFQUFFLElBQUk7WUFDYixNQUFNLEVBQUU7Z0JBQ047b0JBQ0UsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQztvQkFDWCxLQUFLLEVBQUUsV0FBVztvQkFDbEIsS0FBSyxFQUFFLEtBQUs7aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDO29CQUNqQixLQUFLLEVBQUUsUUFBUTtvQkFDZixLQUFLLEVBQUUsWUFBWTtpQkFDcEI7YUFDRjtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxxQ0FBVyxHQUFuQixVQUFvQixLQUFLO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUdELHNCQUFZLHNDQUFTO2FBQXJCO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQzthQUVELFVBQXNCLEtBQVk7WUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxLQUFLLEdBQUcsQ0FBQztRQUMxQyxDQUFDOzs7T0FMQTtJQU9NLGtDQUFRLEdBQWYsVUFBZ0IsS0FBUztRQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFTSxpQ0FBTyxHQUFkLFVBQWUsS0FBUztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSwrQkFBSyxHQUFaLFVBQWEsS0FBUztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSxzQ0FBWSxHQUFuQixVQUFvQixLQUFTO1FBQzNCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUE3R3dCO1FBQXhCLGdCQUFTLENBQUMsWUFBWSxDQUFDO2tDQUFvQiw4QkFBYzt1REFBQztJQS9EaEQsZUFBZTtRQWpCM0IsZ0JBQVMsQ0FBQztZQUNULDBCQUEwQjtZQUMxQixRQUFRLEVBQUUsWUFBWTtZQUN0QixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7WUFDeEUsVUFBVSxFQUFFLENBQUMsOEJBQVUsRUFBRSxDQUFDO1lBQzFCLElBQUksRUFBRSxFQUFDLGVBQWUsRUFBRSxFQUFFLEVBQUM7WUFDM0IsY0FBYztZQUNkLDhCQUE4QjtZQUM5QiwrQkFBK0I7WUFDL0IsMkJBQTJCO1lBQzNCLGlCQUFpQjtZQUNqQixJQUFJO1lBQ0osTUFBTTtZQUNOLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLDZDQUE2QyxDQUFDO1lBQ3pFLHdDQUF3QztZQUN4QyxTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSw4QkFBYyxFQUFFLFFBQVEsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBQyxFQUFDLENBQUM7U0FDbEYsQ0FBQzt5Q0FrRXFCLHNDQUFpQjtZQUNyQiwwQ0FBbUI7WUFDZiwwQkFBVztZQUM5QixvQ0FBb0M7O09BcEUzQixlQUFlLENBOEszQjtJQUFELHNCQUFDO0NBOUtELEFBOEtDLElBQUE7QUE5S1ksMENBQWUiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvY2xpZW50L2NsaWVudC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENsaWVudCB9IGZyb20gJ2FwcC9tb2RlbHMvY2xpZW50JztcclxuLy8gaW1wb3J0IHsgQ2xpZW50REFMIH0gZnJvbSAnLi4vLi4vZGFsL2NsaWVudC5kYWwnO1xyXG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xyXG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1vZGFsRGlyZWN0aXZlIH0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7IHJvdXRlckZhZGUgfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XHJcbi8vIGltcG9ydCB7IE1vZGFsRGlyZWN0aXZlIH0gZnJvbSAnbmcyLWJvb3RzdHJhcC9tb2RhbC9tb2RhbC5jb21wb25lbnQnO1xyXG4vLyBpbXBvcnQgeyBNb2RhbERpcmVjdGl2ZSB9IGZyb20gJ25nMi1ib290c3RyYXAvbmcyLWJvb3RzdHJhcCc7XHJcbmltcG9ydCBvYmplY3RBc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XHJcbmltcG9ydCB7IENhcm91c2VsQ29uZmlnIH0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XHJcbkBDb21wb25lbnQoe1xyXG4gIC8vIHByb3ZpZGVyczogW0NsaWVudERBTF0sXHJcbiAgc2VsZWN0b3I6ICdhcHAtY2xpZW50JyxcclxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jbGllbnQvY2xpZW50LmNvbXBvbmVudC5jc3MnKV0sXHJcbiAgYW5pbWF0aW9uczogW3JvdXRlckZhZGUoKV0sXHJcbiAgaG9zdDogeydbQHJvdXRlckZhZGVdJzogJyd9LFxyXG4gIC8vICBzdHlsZXM6IFtgXHJcbiAgLy8gOmhvc3QgPj4+IC5hbGVydC1tZC1jb2xvciB7XHJcbiAgLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4gIC8vICAgYm9yZGVyLWNvbG9yOiAjMDA2OTVDO1xyXG4gIC8vICAgY29sb3I6ICNmZmY7XHJcbiAgLy8gfVxyXG4gIC8vIGBdLFxyXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jbGllbnQvY2xpZW50LmNvbXBvbmVudC5odG1sJyksXHJcbiAgLy8gZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG4gIHByb3ZpZGVyczogW3twcm92aWRlOiBDYXJvdXNlbENvbmZpZywgdXNlVmFsdWU6IHtpbnRlcnZhbDogMTUwMCwgbm9QYXVzZTogdHJ1ZX19XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2xpZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAvLyBwcml2YXRlIGNsaWVudHM6IEZpcmViYXNlTGlzdE9ic2VydmFibGU8QXJyYXk8Q2xpZW50Pj47XHJcblxyXG4gIC8vIGNvbnN0cnVjdG9yKHByaXZhdGUgZGFsOiBDbGllbnREQUwsIHByaXZhdGUgYnJlYWRTZXJ2OiBCcmVhZGNydW1iU2VydmljZSkge1xyXG4gIC8vICAgLy8gVE9ET1xyXG4gIC8vIH1cclxuXHJcbiAgcHJpdmF0ZSBzaW5nbGVNb2RlbDogc3RyaW5nID0gJzEnO1xyXG4gIHB1YmxpYyBjaGVja01vZGVsOiBhbnkgPSB7IGxlZnQ6IGZhbHNlLCBtaWRkbGU6IHRydWUsIHJpZ2h0OiBmYWxzZSB9O1xyXG4gIHB1YmxpYyByYWRpb01vZGVsOiBzdHJpbmcgPSAnTWlkZGxlJztcclxuICBwcml2YXRlIGNsaWVudHM6IEFycmF5PENsaWVudD47XHJcbiAgcHVibGljIHN0YXR1czogYW55ID0ge1xyXG4gICAgaXNGaXJzdE9wZW46IHRydWUsXHJcbiAgICBpc0ZpcnN0RGlzYWJsZWQ6IGZhbHNlXHJcbiAgfTtcclxuICAvLyBwdWJsaWMgaXRlbXM6IHN0cmluZ1tdID0gWydJdGVtIDEnLCAnSXRlbSAyJywgJ0l0ZW0gMyddO1xyXG4gIC8vIHB1YmxpYyBncm91cHM6IGFueVtdID0gW1xyXG4gIC8vICAge1xyXG4gIC8vICAgICB0aXRsZTogJ0R5bmFtaWMgR3JvdXAgSGVhZGVyIC0gMScsXHJcbiAgLy8gICAgIGNvbnRlbnQ6ICdEeW5hbWljIEdyb3VwIEJvZHkgLSAxJ1xyXG4gIC8vICAgfSxcclxuICAvLyAgIHtcclxuICAvLyAgICAgdGl0bGU6ICdEeW5hbWljIEdyb3VwIEhlYWRlciAtIDInLFxyXG4gIC8vICAgICBjb250ZW50OiAnRHluYW1pYyBHcm91cCBCb2R5IC0gMidcclxuICAvLyAgIH1cclxuICAvLyBdO1xyXG5cclxuICBwdWJsaWMgaXNGaXJzdE9wZW46IGJvb2xlYW4gPSB0cnVlO1xyXG4gIHB1YmxpYyBjdXN0b21DbGFzczogc3RyaW5nID0gJ2N1c3RvbUNsYXNzJztcclxuXHJcbiAgcHVibGljIGFsZXJ0czogYW55ID0gW1xyXG4gICAge1xyXG4gICAgICB0eXBlOiAnc3VjY2VzcycsXHJcbiAgICAgIG1zZzogYFlvdSBzdWNjZXNzZnVsbHkgcmVhZCB0aGlzIGltcG9ydGFudCBhbGVydCBtZXNzYWdlLmBcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHR5cGU6ICdpbmZvJyxcclxuICAgICAgbXNnOiBgVGhpcyBhbGVydCBuZWVkcyB5b3VyIGF0dGVudGlvbiwgYnV0IGl0J3Mgbm90IHN1cGVyIGltcG9ydGFudC5gXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICB0eXBlOiAnZGFuZ2VyJyxcclxuICAgICAgbXNnOiBgQmV0dGVyIGNoZWNrIHlvdXJzZWxmLCB5b3UncmUgbm90IGxvb2tpbmcgdG9vIGdvb2QuYFxyXG4gICAgfVxyXG4gIF07XHJcbiAgXHJcbiAgcHVibGljIGlzQ29sbGFwc2VkOmJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgcHJpdmF0ZSB2YWx1ZTphbnkgPSB7fTtcclxuICBwcml2YXRlIF9kaXNhYmxlZFY6c3RyaW5nID0gJzAnO1xyXG4gIHByaXZhdGUgZGlzYWJsZWQ6Ym9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICBwdWJsaWMgaXRlbXM6QXJyYXk8c3RyaW5nPiA9IFsnQW1zdGVyZGFtJywgJ0FudHdlcnAnLCAnQXRoZW5zJywgJ0JhcmNlbG9uYScsXHJcbiAgICAnQmVybGluJywgJ0Jpcm1pbmdoYW0nLCAnQnJhZGZvcmQnLCAnQnJlbWVuJywgJ0JydXNzZWxzJywgJ0J1Y2hhcmVzdCcsXHJcbiAgICAnQnVkYXBlc3QnLCAnQ29sb2duZScsICdDb3BlbmhhZ2VuJywgJ0RvcnRtdW5kJywgJ0RyZXNkZW4nLCAnRHVibGluJyxcclxuICAgICdEw7xzc2VsZG9yZicsICdFc3NlbicsICdGcmFua2Z1cnQnLCAnR2Vub2EnLCAnR2xhc2dvdycsICdHb3RoZW5idXJnJyxcclxuICAgICdIYW1idXJnJywgJ0hhbm5vdmVyJywgJ0hlbHNpbmtpJywgJ0tyYWvDs3cnLCAnTGVlZHMnLCAnTGVpcHppZycsICdMaXNib24nLFxyXG4gICAgJ0xvbmRvbicsICdNYWRyaWQnLCAnTWFuY2hlc3RlcicsICdNYXJzZWlsbGUnLCAnTWlsYW4nLCAnTXVuaWNoJywgJ03DoWxhZ2EnLFxyXG4gICAgJ05hcGxlcycsICdQYWxlcm1vJywgJ1BhcmlzJywgJ1Bvem5hxYQnLCAnUHJhZ3VlJywgJ1JpZ2EnLCAnUm9tZScsXHJcbiAgICAnUm90dGVyZGFtJywgJ1NldmlsbGUnLCAnU2hlZmZpZWxkJywgJ1NvZmlhJywgJ1N0b2NraG9sbScsICdTdHV0dGdhcnQnLFxyXG4gICAgJ1RoZSBIYWd1ZScsICdUdXJpbicsICdWYWxlbmNpYScsICdWaWVubmEnLCAnVmlsbml1cycsICdXYXJzYXcnLCAnV3JvY8WCYXcnLFxyXG4gICAgJ1phZ3JlYicsICdaYXJhZ296YScsICfFgcOzZMW6J107XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ2NoaWxkTW9kYWwnKSBwdWJsaWMgY2hpbGRNb2RhbDogTW9kYWxEaXJlY3RpdmU7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGJyZWFkU2VydjogQnJlYWRjcnVtYlNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG5vdGlmOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgIHByaXZhdGUgYXV0aFNlcnY6IEF1dGhTZXJ2aWNlXHJcbiAgICAvLyBwcml2YXRlIGNoaWxkTW9kYWw6TW9kYWxEaXJlY3RpdmVcclxuICApIHtcclxuICAgXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgbmdPbkluaXQoKSB7XHJcbiAgICAvLyB0aGlzLmNsaWVudHMgPSB0aGlzLmRhbC5yZWFkQWxsKCk7XHJcbiAgICB0aGlzLmF1dGhTZXJ2LmdldFRva2VuKCkudGhlbihcclxuICAgICAgIHJlc3BvbnNlID0+IHRoaXMucHJvY2Vzc1Jlc3VsdChyZXNwb25zZSksXHJcbiAgICAgICBlcnJvciA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBuZ09uRGVzdHJveSgpIHtcclxuICAgIC8vIHRoaXMuYnJlYWRTZXJ2LmNsZWFyKCk7XHJcbiAgICB0aGlzLmNsaWVudHMgPSBudWxsO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzYXZlID0gKGNsaWVudDogQ2xpZW50KTogdm9pZCA9PiB7XHJcbiAgICAvLyB0aGlzLmRhbC51cGRhdGUoY2xpZW50LmNsaWVudElkLCBuZXcgQ2xpZW50KGNsaWVudC5uYW1lLCBjbGllbnQuY2xpZW50SWQsIGNsaWVudC5hZGRyZXNzKSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGRlbGV0ZSA9IChjbGllbnQ6IENsaWVudCk6IHZvaWQgPT4ge1xyXG4gICAgLy8gdGhpcy5kYWwuZGVsZXRlKGNsaWVudCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFkZCA9ICgpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubm90aWYuc3VjY2VzcygnTmV3IGNsaWVudCBoYXMgYmVlbiBhZGRlZCcpXHJcbiAgICAvLyB0aGlzLmRhbC5jcmVhdGUobmV3IENsaWVudCgpKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzaG93Q2hpbGRNb2RhbCgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2hpbGRNb2RhbC5zaG93KCk7XHJcbiAgfVxyXG5cclxuICAvLyBwdWJsaWMgYWRkSXRlbSgpOiB2b2lkIHtcclxuICAvLyAgIHRoaXMuaXRlbXMucHVzaChgSXRlbXMgJHt0aGlzLml0ZW1zLmxlbmd0aCArIDF9YCk7XHJcbiAgLy8gfVxyXG5cclxuICBwdWJsaWMgcmVzZXQoKTogdm9pZCB7XHJcbiAgICAvLyBjb25zb2xlLmxvZygnLS0tLS1vYmplY3QtLS0tLScsIE9iamVjdC4pO1xyXG4gICAgLy8gY29uc29sZS5sb2coJy0tLS0tb2JqZWN0QXNzaWduLS0tLS0nLCBvYmplY3RBc3NpZ24oKSk7XHJcbiAgICB0aGlzLmFsZXJ0cyA9IHRoaXMuYWxlcnRzLm1hcCgoYWxlcnQ6IGFueSkgPT4gb2JqZWN0QXNzaWduKHt9LCBhbGVydCkpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNvbGxhcHNlZChldmVudDphbnkpOnZvaWQge1xyXG4gICAgY29uc29sZS5sb2coJ2NvbGxhcHNlZCcsIGV2ZW50KTtcclxuICB9XHJcbiBcclxuICBwdWJsaWMgZXhwYW5kZWQoZXZlbnQ6YW55KTp2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdleHBhbmRlZCcsIGV2ZW50KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXMpe1xyXG4gICAgdGhpcy5jbGllbnRzID0gW25ldyBDbGllbnQoKV07XHJcbiAgICB0aGlzLmJyZWFkU2Vydi5zZXQoe1xyXG4gICAgICBoZWFkZXI6XCJDbGllbnRcIixcclxuICAgICAgZGVzY3JpcHRpb246ICdUaGlzIGlzIG91ciBDbGllbnQgcGFnZScsXHJcbiAgICAgIGRpc3BsYXk6IHRydWUsXHJcbiAgICAgIGxldmVsczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIGljb246ICdkYXNoYm9hcmQnLFxyXG4gICAgICAgICAgbGluazogWycvJ10sXHJcbiAgICAgICAgICB0aXRsZTogJ0Rhc2hib2FyZCcsXHJcbiAgICAgICAgICBzdGF0ZTogJ2FwcCdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGljb246ICdjbG9jay1vJyxcclxuICAgICAgICAgIGxpbms6IFsnL2NsaWVudCddLFxyXG4gICAgICAgICAgdGl0bGU6ICdDbGllbnQnLFxyXG4gICAgICAgICAgc3RhdGU6ICdhcHAuY2xpZW50J1xyXG4gICAgICAgIH1cclxuICAgICAgXVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yKXtcclxuICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICB9XHJcblxyXG4gIFxyXG4gIHByaXZhdGUgZ2V0IGRpc2FibGVkVigpOnN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fZGlzYWJsZWRWO1xyXG4gIH1cclxuIFxyXG4gIHByaXZhdGUgc2V0IGRpc2FibGVkVih2YWx1ZTpzdHJpbmcpIHtcclxuICAgIHRoaXMuX2Rpc2FibGVkViA9IHZhbHVlO1xyXG4gICAgdGhpcy5kaXNhYmxlZCA9IHRoaXMuX2Rpc2FibGVkViA9PT0gJzEnO1xyXG4gIH1cclxuIFxyXG4gIHB1YmxpYyBzZWxlY3RlZCh2YWx1ZTphbnkpOnZvaWQge1xyXG4gICAgY29uc29sZS5sb2coJ1NlbGVjdGVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gXHJcbiAgcHVibGljIHJlbW92ZWQodmFsdWU6YW55KTp2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdSZW1vdmVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gXHJcbiAgcHVibGljIHR5cGVkKHZhbHVlOmFueSk6dm9pZCB7XHJcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xyXG4gIH1cclxuIFxyXG4gIHB1YmxpYyByZWZyZXNoVmFsdWUodmFsdWU6YW55KTp2b2lkIHtcclxuICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==
