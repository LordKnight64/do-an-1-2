"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var subscribe_service_1 = require("app/services/subscribe.service");
var SubscribeListComponent = (function () {
    function SubscribeListComponent(subscribeServ, userServ, storageServ, notifyServ) {
        this.subscribeServ = subscribeServ;
        this.userServ = userServ;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    SubscribeListComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        this.doSearch();
    };
    SubscribeListComponent.prototype.doSearch = function () {
        var _this = this;
        var params = {};
        this.subscribeServ.getSubscribeList(params).then(function (response) {
            console.log('response ', response.subscribe);
            _this.newsList = response.subscribe;
            _this.totalItems = _this.newsList.length;
            _this.pageChanged({ page: _this.currentPage, itemsPerPage: _this.itemsPerPage });
        }, function (error) {
            console.log(error);
        });
    };
    SubscribeListComponent.prototype.pageChanged = function (event) {
        var start = (event.page - 1) * event.itemsPerPage;
        var end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
        this.data = this.newsList.slice(start, end);
    };
    SubscribeListComponent.prototype.doDeleteSubscribe = function (obj) {
        console.log(obj);
        var email = obj.email;
        var params = {
            'email': email
        };
        var me = this;
        this.subscribeServ.deleteSubscribe(params).then(function (response) {
            if (response.return_cd == 0) {
                me.notifyServ.success('Subscribe Email has been deleted');
                me.doSearch();
            }
        }, function (error) {
            console.log(error);
        });
    };
    SubscribeListComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    SubscribeListComponent = __decorate([
        core_1.Component({
            selector: 'table-list',
            templateUrl: utils_1.default.getView('app/components/subscribe-list/subscribe-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/subscribe-list/subscribe-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [subscribe_service_1.SubscribeService,
            user_service_1.UserService,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], SubscribeListComponent);
    return SubscribeListComponent;
}());
exports.SubscribeListComponent = SubscribeListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3N1YnNjcmliZS1saXN0L3N1YnNjcmliZS1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFFcEMsNkRBQXVFO0FBQ3ZFLDBEQUF3RDtBQUN4RCxnRUFBOEQ7QUFDOUQsMEVBQXdFO0FBQ3hFLG9FQUFrRTtBQVNsRTtJQVdFLGdDQUNVLGFBQStCLEVBQy9CLFFBQXFCLEVBQ3JCLFdBQTJCLEVBQzNCLFVBQStCO1FBSC9CLGtCQUFhLEdBQWIsYUFBYSxDQUFrQjtRQUMvQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUMzQixlQUFVLEdBQVYsVUFBVSxDQUFxQjtRQVRqQyxnQkFBVyxHQUFXLENBQUMsQ0FBQztRQUV4QixpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUMxQixZQUFPLEdBQVcsRUFBRSxDQUFDO0lBT3pCLENBQUM7SUFFTCx5Q0FBUSxHQUFSO1FBRUUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBRUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFTyx5Q0FBUSxHQUFoQjtRQUFBLGlCQVdDO1FBVkMsSUFBSSxNQUFNLEdBQUcsRUFDWixDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3QyxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7WUFDbkMsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUN0QyxLQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQ2pGLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLDRDQUFXLEdBQWxCLFVBQW1CLEtBQVU7UUFDM0IsSUFBSSxLQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7UUFDbEQsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDeEYsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVPLGtEQUFpQixHQUF6QixVQUEwQixHQUFHO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztRQUN0QixJQUFJLE1BQU0sR0FBRztZQUNYLE9BQU8sRUFBRSxLQUFLO1NBQ2YsQ0FBQztRQUNGLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQztRQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDdEQsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO2dCQUMxRCxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsQ0FBQztRQUNILENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLDJDQUFVLEdBQWxCLFVBQW1CLEdBQUc7UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQWxFVSxzQkFBc0I7UUFQakMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLDZEQUE2RCxDQUFDO1lBQ3pGLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsNERBQTRELENBQUMsQ0FBQztZQUN4RixVQUFVLEVBQUUsQ0FBQyxvQ0FBZ0IsRUFBRSxDQUFDO1lBQ2hDLElBQUksRUFBRSxFQUFDLHFCQUFxQixFQUFFLEVBQUUsRUFBQztTQUNsQyxDQUFDO3lDQWF5QixvQ0FBZ0I7WUFDckIsMEJBQVc7WUFDUixnQ0FBYztZQUNmLDBDQUFtQjtPQWY5QixzQkFBc0IsQ0FvRWxDO0lBQUQsNkJBQUM7Q0FwRUQsQUFvRUMsSUFBQTtBQXBFWSx3REFBc0IiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3Qvc3Vic2NyaWJlLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFN1YnNjcmliZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc3Vic2NyaWJlLnNlcnZpY2UnO1xuXG4gQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndGFibGUtbGlzdCcsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9zdWJzY3JpYmUtbGlzdC9zdWJzY3JpYmUtbGlzdC5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9zdWJzY3JpYmUtbGlzdC9zdWJzY3JpYmUtbGlzdC5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJyd9XG59KVxuZXhwb3J0IGNsYXNzIFN1YnNjcmliZUxpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuIFxuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgbmV3c0xpc3Q6IGFueTtcbiAgcHJpdmF0ZSBkYXRhOiBhbnk7XG4gIHByaXZhdGUgY3VycmVudFBhZ2U6IG51bWJlciA9IDE7XG4gIHByaXZhdGUgdG90YWxJdGVtczogbnVtYmVyO1xuICBwcml2YXRlIGl0ZW1zUGVyUGFnZTogbnVtYmVyID0gMTA7XG4gIHByaXZhdGUgbWF4U2l6ZTogbnVtYmVyID0gMTA7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBzdWJzY3JpYmVTZXJ2OiBTdWJzY3JpYmVTZXJ2aWNlLFxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gICAgbGV0IHVzZXJLZXkgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICBpZiAoIXVzZXJLZXkpIHtcbiAgICAgIHRoaXMudXNlclNlcnYubG9nb3V0KCk7XG4gICAgfSBcblxuICAgIHRoaXMuZG9TZWFyY2goKTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZWFyY2goKSB7XG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICB9O1xuICAgIHRoaXMuc3Vic2NyaWJlU2Vydi5nZXRTdWJzY3JpYmVMaXN0KHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBjb25zb2xlLmxvZygncmVzcG9uc2UgJywgcmVzcG9uc2Uuc3Vic2NyaWJlKTtcbiAgICAgIHRoaXMubmV3c0xpc3QgPSByZXNwb25zZS5zdWJzY3JpYmU7XG4gICAgICB0aGlzLnRvdGFsSXRlbXMgPSB0aGlzLm5ld3NMaXN0Lmxlbmd0aDtcbiAgICAgICB0aGlzLnBhZ2VDaGFuZ2VkKHsgcGFnZTogdGhpcy5jdXJyZW50UGFnZSwgaXRlbXNQZXJQYWdlOiB0aGlzLml0ZW1zUGVyUGFnZSB9KTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgcGFnZUNoYW5nZWQoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCBzdGFydCA9IChldmVudC5wYWdlIC0gMSkgKiBldmVudC5pdGVtc1BlclBhZ2U7XG4gICAgbGV0IGVuZCA9IGV2ZW50Lml0ZW1zUGVyUGFnZSA+IC0xID8gKHN0YXJ0ICsgZXZlbnQuaXRlbXNQZXJQYWdlKSA6IHRoaXMubmV3c0xpc3QubGVuZ3RoO1xuICAgIHRoaXMuZGF0YSA9IHRoaXMubmV3c0xpc3Quc2xpY2Uoc3RhcnQsIGVuZCk7XG4gIH1cblxuICBwcml2YXRlIGRvRGVsZXRlU3Vic2NyaWJlKG9iaikge1xuICAgIGNvbnNvbGUubG9nKG9iaik7XG4gICAgbGV0IGVtYWlsID0gb2JqLmVtYWlsO1xuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAnZW1haWwnOiBlbWFpbFxuICAgIH07XG4gICAgdmFyIG1lID0gdGhpcztcbiAgICB0aGlzLnN1YnNjcmliZVNlcnYuZGVsZXRlU3Vic2NyaWJlKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBpZihyZXNwb25zZS5yZXR1cm5fY2QgPT0gMCkge1xuICAgICAgICBtZS5ub3RpZnlTZXJ2LnN1Y2Nlc3MoJ1N1YnNjcmliZSBFbWFpbCBoYXMgYmVlbiBkZWxldGVkJyk7XG4gICAgICAgIG1lLmRvU2VhcmNoKCk7XG4gICAgICB9XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb1NldFNjb3BlKG9iaikge1xuICAgIHRoaXMuc3RvcmFnZVNlcnYuc2V0U2NvcGUob2JqKTtcbiAgfVxuXG59XG4iXX0=
