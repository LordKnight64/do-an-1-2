"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var foodItem_service_1 = require("app/services/foodItem.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var review_service_1 = require("app/services/review.service");
var ReviewItemComponent = (function () {
    function ReviewItemComponent(foodItemService, authServ, notif, reviewService, router, StorageService) {
        this.foodItemService = foodItemService;
        this.authServ = authServ;
        this.notif = notif;
        this.reviewService = reviewService;
        this.router = router;
        this.StorageService = StorageService;
    }
    ReviewItemComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllFoodItems(this.restId);
        this.fetchAllReviewFoodItems(this.restId);
    };
    ReviewItemComponent.prototype.fetchAllFoodItems = function (restId) {
        var _this = this;
        var param = {
            'rest_id': restId
        };
        this.reviewService.getItemsReviews(param).then(function (response) {
            console.log('fetchAllFoodItems ', response);
            _this.foodItems = response.items;
        }, function (error) {
            console.log(error);
        });
    };
    ReviewItemComponent.prototype.fetchAllReviewFoodItems = function (restId) {
        var _this = this;
        var param = {
            'rest_id': restId
        };
        this.reviewService.getRestaurantReviews(param).then(function (response) {
            console.log('getRestaurantReviews ', response);
            _this.reviewFoodItems = response.reviews;
        }, function (error) {
            console.log(error);
        });
    };
    ReviewItemComponent.prototype.getRating = function (item) {
        var rating = [];
        var itemRating = Number(item.rating);
        for (var i = 0; i < itemRating; i++) {
            rating.push(i);
        }
        return rating;
    };
    ReviewItemComponent.prototype.deleteReviewFoodItem = function (item) {
        var _this = this;
        var param = {
            'id': item.id
        };
        alert(item.id);
        this.reviewService.deleteRestaurantReview(param).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    ReviewItemComponent.prototype.updateReviewFoodItems = function (item) {
        var _this = this;
        console.log('updateReviewFoodItems ', item);
        var param = {
            'id': item.id,
            'active_flg': !item.active_flg
        };
        console.log('param ', param);
        this.reviewService.updateRestaurantReview(param).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    ReviewItemComponent.prototype.processResult = function (response) {
        console.log(response);
        if (response.return_cd == 0) {
            this.notif.success(response.message);
            this.fetchAllFoodItems(this.restId);
            this.fetchAllReviewFoodItems(this.restId);
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    ReviewItemComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    ReviewItemComponent = __decorate([
        core_1.Component({
            selector: 'review-item',
            templateUrl: utils_1.default.getView('app/components/review-item/review-item.component.html'),
            styleUrls: [utils_1.default.getView('app/components/review-item/review-item.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [foodItem_service_1.FoodItemService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            review_service_1.ReviewService,
            router_1.Router,
            storage_service_1.StorageService])
    ], ReviewItemComponent);
    return ReviewItemComponent;
}());
exports.ReviewItemComponent = ReviewItemComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3Jldmlldy1pdGVtL3Jldmlldy1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFHcEMsNkRBQTJEO0FBRTNELGtFQUFnRTtBQUNoRSwwREFBeUQ7QUFDekQsMkNBQThDO0FBQzlDLDBFQUF3RTtBQUN4RSwwQ0FBeUM7QUFFekMsZ0VBQThEO0FBQzlELDhEQUE2RDtBQVE3RDtJQU9FLDZCQUNRLGVBQWdDLEVBRWhDLFFBQXFCLEVBQ3JCLEtBQTBCLEVBQzFCLGFBQTRCLEVBQzVCLE1BQWMsRUFDZCxjQUE4QjtRQU45QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFFaEMsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO0lBQUksQ0FBQztJQUUzQyxzQ0FBUSxHQUFSO1FBQ0ssSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1FBRXJDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUUvQyxDQUFDO0lBQ08sK0NBQWlCLEdBQXpCLFVBQTBCLE1BQU07UUFBaEMsaUJBWUU7UUFYQyxJQUFJLEtBQUssR0FBRztZQUNULFNBQVMsRUFBRyxNQUFNO1NBQ25CLENBQUE7UUFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQzdCLFVBQUEsUUFBUTtZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUMsUUFBUSxDQUFFLENBQUM7WUFDN0MsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1FBRWpDLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLHFEQUF1QixHQUEvQixVQUFnQyxNQUFNO1FBQXRDLGlCQVlFO1FBWEUsSUFBSSxLQUFLLEdBQUc7WUFDVixTQUFTLEVBQUcsTUFBTTtTQUNuQixDQUFBO1FBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQ2xDLFVBQUEsUUFBUTtZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEVBQUMsUUFBUSxDQUFFLENBQUM7WUFDL0MsS0FBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO1FBRXpDLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVRLHVDQUFTLEdBQWpCLFVBQWtCLElBQUk7UUFDcEIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25CLENBQUM7UUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFSCxrREFBb0IsR0FBcEIsVUFBcUIsSUFBSTtRQUF6QixpQkFTQztRQVJLLElBQUksS0FBSyxHQUFJO1lBQ1gsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFO1NBRWQsQ0FBQztRQUNGLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQ3hDLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBNUIsQ0FBNEIsRUFDcEMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFJSCxtREFBcUIsR0FBckIsVUFBc0IsSUFBSTtRQUExQixpQkFZQztRQVhDLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBSSxLQUFLLEdBQUk7WUFDTCxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDYixZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVTtTQUMvQixDQUFDO1FBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQ25DLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBNUIsQ0FBNEIsRUFDcEMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO0lBRWxFLENBQUM7SUFFUywyQ0FBYSxHQUFyQixVQUFzQixRQUFRO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFN0MsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04scUJBQUksQ0FDQSxjQUFjLEVBQ2QsT0FBTyxDQUNSLENBQUMsS0FBSyxDQUFDLHFCQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkIsQ0FBQztJQUNILENBQUM7SUFDTywwQ0FBWSxHQUFwQixVQUFzQixHQUFHO1FBQ3ZCLHFCQUFJLENBQ0UsY0FBYyxFQUNkLE9BQU8sQ0FDUixDQUFDLEtBQUssQ0FBQyxxQkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUEzR1UsbUJBQW1CO1FBUC9CLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsYUFBYTtZQUN2QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx1REFBdUQsQ0FBQztZQUNuRixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHNEQUFzRCxDQUFDLENBQUM7WUFDbEYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0FTeUIsa0NBQWU7WUFFdEIsMEJBQVc7WUFDZCwwQ0FBbUI7WUFDWCw4QkFBYTtZQUNwQixlQUFNO1lBQ0UsZ0NBQWM7T0FkM0IsbUJBQW1CLENBNEcvQjtJQUFELDBCQUFDO0NBNUdELEFBNEdDLElBQUE7QUE1R1ksa0RBQW1CIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL3Jldmlldy1pdGVtL3Jldmlldy1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcblxuaW1wb3J0IHsgRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2Zvb2RJdGVtLnNlcnZpY2UnO1xuaW1wb3J0ICB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBkZWZhdWx0IGFzIHN3YWwgfSBmcm9tICdzd2VldGFsZXJ0Mic7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UnO1xuaW1wb3J0ICB7IFJldmlld1NlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvcmV2aWV3LnNlcnZpY2UnO1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncmV2aWV3LWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvcmV2aWV3LWl0ZW0vcmV2aWV3LWl0ZW0uY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvcmV2aWV3LWl0ZW0vcmV2aWV3LWl0ZW0uY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJUcmFuc2l0aW9uXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBSZXZpZXdJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgcmV2aWV3Rm9vZEl0ZW1zIDogYW55W107XG4gIHByaXZhdGUgZm9vZEl0ZW1zIDogYW55W107XG5cbiAgY29uc3RydWN0b3IoXG4gIHByaXZhdGUgZm9vZEl0ZW1TZXJ2aWNlOiBGb29kSXRlbVNlcnZpY2UsXG4gXG4gIHByaXZhdGUgYXV0aFNlcnY6IEF1dGhTZXJ2aWNlLFxuICBwcml2YXRlIG5vdGlmOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxuICBwcml2YXRlIHJldmlld1NlcnZpY2U6IFJldmlld1NlcnZpY2UsXG4gIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICAgICBsZXQgdXNlckluZm8gPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICAgICAgXG4gICAgXHQgdGhpcy5mZXRjaEFsbEZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgICAgdGhpcy5mZXRjaEFsbFJldmlld0Zvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG5cbiAgfVxuICBwcml2YXRlIGZldGNoQWxsRm9vZEl0ZW1zKHJlc3RJZCkge1xuICAgICBsZXQgcGFyYW0gPSB7XG4gICAgICAgICdyZXN0X2lkJyA6IHJlc3RJZFxuICAgICAgfVxuICBcdFx0dGhpcy5yZXZpZXdTZXJ2aWNlLmdldEl0ZW1zUmV2aWV3cyhwYXJhbSkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZmV0Y2hBbGxGb29kSXRlbXMgJyxyZXNwb25zZSApO1xuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLmZvb2RJdGVtcyA9IHJlc3BvbnNlLml0ZW1zO1xuICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbiAgcHJpdmF0ZSBmZXRjaEFsbFJldmlld0Zvb2RJdGVtcyhyZXN0SWQpIHtcbiAgICAgIGxldCBwYXJhbSA9IHtcbiAgICAgICAgJ3Jlc3RfaWQnIDogcmVzdElkXG4gICAgICB9XG4gIFx0XHR0aGlzLnJldmlld1NlcnZpY2UuZ2V0UmVzdGF1cmFudFJldmlld3MocGFyYW0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZ2V0UmVzdGF1cmFudFJldmlld3MgJyxyZXNwb25zZSApO1xuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLnJldmlld0Zvb2RJdGVtcyA9IHJlc3BvbnNlLnJldmlld3M7XG4gICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cblxuICAgIHByaXZhdGUgZ2V0UmF0aW5nKGl0ZW0pIHtcbiAgICAgIGxldCByYXRpbmcgPSBbXTtcbiAgICAgIGxldCBpdGVtUmF0aW5nID0gTnVtYmVyKGl0ZW0ucmF0aW5nKTtcbiAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBpdGVtUmF0aW5nOyBpKyspIHtcbiAgICAgICAgICByYXRpbmcucHVzaChpKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiByYXRpbmc7XG4gICAgfVxuICBcbiAgZGVsZXRlUmV2aWV3Rm9vZEl0ZW0oaXRlbSl7XG4gICAgICAgIGxldCBwYXJhbSAgPSB7XG4gICAgICAgICAgJ2lkJzogaXRlbS5pZFxuICAgICAgICAgIFxuICAgICAgICB9O1xuICAgICAgICBhbGVydChpdGVtLmlkKTtcbiAgICAgIFx0dGhpcy5yZXZpZXdTZXJ2aWNlLmRlbGV0ZVJlc3RhdXJhbnRSZXZpZXcocGFyYW0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UpLFxuICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG4gIH1cblxuIFxuXG51cGRhdGVSZXZpZXdGb29kSXRlbXMoaXRlbSkge1xuICBjb25zb2xlLmxvZygndXBkYXRlUmV2aWV3Rm9vZEl0ZW1zICcsIGl0ZW0pO1xuICBsZXQgcGFyYW0gID0ge1xuICAgICAgICAgICdpZCc6IGl0ZW0uaWQsXG4gICAgICAgICAgJ2FjdGl2ZV9mbGcnOiAhaXRlbS5hY3RpdmVfZmxnXG4gICAgICAgIH07XG4gIGNvbnNvbGUubG9nKCdwYXJhbSAnLCBwYXJhbSk7XG4gXG4gIHRoaXMucmV2aWV3U2VydmljZS51cGRhdGVSZXN0YXVyYW50UmV2aWV3KHBhcmFtKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiAgdGhpcy5mYWlsZWRDcmVhdGUuYmluZChlcnJvcikpO1xuXG59XG5cbiAgcHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSB7XG4gICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgIGlmIChyZXNwb25zZS5yZXR1cm5fY2QgPT0gMCkge1xuICAgICAgdGhpcy5ub3RpZi5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xuICAgICAgIHRoaXMuZmV0Y2hBbGxGb29kSXRlbXModGhpcy5yZXN0SWQpO1xuICAgICAgIHRoaXMuZmV0Y2hBbGxSZXZpZXdGb29kSXRlbXModGhpcy5yZXN0SWQpO1xuXG4gICAgfSBlbHNlIHtcbiAgICAgIHN3YWwoXG4gICAgICAgICAgJ1VwZGF0ZSBGYWlsIScsXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICB9XG59XG4iXX0=
