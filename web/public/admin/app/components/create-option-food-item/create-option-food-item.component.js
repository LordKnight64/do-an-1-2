"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var forms_1 = require("@angular/forms");
var router_animations_1 = require("../../router.animations");
var optionFoodItem_service_1 = require("app/services/optionFoodItem.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var CreateOptionFoodItemComponent = (function () {
    function CreateOptionFoodItemComponent(fb, OptionFoodItemService, StorageService, authServ, notif, router, route) {
        this.fb = fb;
        this.OptionFoodItemService = OptionFoodItemService;
        this.StorageService = StorageService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.formErrors = {
            'name': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            }
        };
    }
    CreateOptionFoodItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
        if (this.id != null) {
            this.optionFoodItem = this.StorageService.getScope();
            if (this.optionFoodItem == false) {
                this.fetchAllOptionFoodItems(this.restId, this.id);
            }
            else {
                this.createOptionItemForm.controls['name'].setValue(this.optionFoodItem.name);
                this.name = this.createOptionItemForm.controls['name'];
            }
        }
    };
    CreateOptionFoodItemComponent.prototype.fetchAllOptionFoodItems = function (restId, id) {
        var _this = this;
        this.OptionFoodItemService.getOptionFoodItems(restId, id).then(function (response) {
            console.log(response.options);
            if (response.options != null && response.options.length > 0) {
                _this.optionFoodItem = response.options[0];
                _this.createOptionItemForm.controls['name'].setValue(_this.optionFoodItem.name);
                _this.name = _this.createOptionItemForm.controls['name'];
            }
            else {
                _this.notif.error('Food item had delete');
                _this.router.navigateByUrl('option-food-items/list');
            }
        }, function (error) {
            console.log(error);
        });
    };
    CreateOptionFoodItemComponent.prototype.buildForm = function () {
        var _this = this;
        this.createOptionItemForm = this.fb.group({
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ]
        });
        this.name = this.createOptionItemForm.controls['name'];
        this.createOptionItemForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    CreateOptionFoodItemComponent.prototype.onValueChanged = function (data) {
        if (!this.createOptionItemForm) {
            return;
        }
        var form = this.createOptionItemForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    CreateOptionFoodItemComponent.prototype.optionItemCreate = function () {
        var _this = this;
        var id = '-1';
        if (this.id != null) {
            id = this.id;
        }
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.optionFoodItem = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'option': {
                'id': id,
                'name': this.createOptionItemForm.value.name,
                'is_delete': '0'
            }
        };
        this.OptionFoodItemService.createOptionFoodItems(this.optionFoodItem).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    ;
    CreateOptionFoodItemComponent.prototype.processResult = function (response) {
        if (response.message == undefined || response.message == 'OK') {
            this.notif.success('New foot item has been added');
            this.router.navigateByUrl('option-food-items/list');
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.name) {
                    this.formErrors['name'] = this.error.name;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    CreateOptionFoodItemComponent.prototype.failedCreate = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    CreateOptionFoodItemComponent = __decorate([
        core_1.Component({
            selector: 'create-option-food-item',
            templateUrl: utils_1.default.getView('app/components/create-option-food-item/create-option-food-item.component.html'),
            styleUrls: [utils_1.default.getView('app/components/create-option-food-item/create-option-food-item.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            optionFoodItem_service_1.OptionFoodItemService,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            router_2.ActivatedRoute])
    ], CreateOptionFoodItemComponent);
    return CreateOptionFoodItemComponent;
}());
exports.CreateOptionFoodItemComponent = CreateOptionFoodItemComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFDcEMsd0NBQTRGO0FBRzVGLDZEQUEyRDtBQUMzRCw4RUFBNEU7QUFDNUUsMERBQXlEO0FBQ3pELDJDQUE4QztBQUM5QywwRUFBd0U7QUFDeEUsMENBQXlDO0FBQ3pDLDBDQUFpRDtBQUNqRCxnRUFBOEQ7QUFROUQ7SUFZRSx1Q0FBb0IsRUFBZSxFQUMzQixxQkFBNEMsRUFDNUMsY0FBOEIsRUFDOUIsUUFBcUIsRUFDckIsS0FBMEIsRUFDMUIsTUFBYyxFQUNkLEtBQXFCO1FBTlQsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUMzQiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFVBQUssR0FBTCxLQUFLLENBQXFCO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQW1FN0IsZUFBVSxHQUFHO1lBQ1QsTUFBTSxFQUFFLEVBQUU7U0FDWCxDQUFDO1FBRUosdUJBQWtCLEdBQUc7WUFDbkIsTUFBTSxFQUFFO2dCQUNOLFVBQVUsRUFBTyx1QkFBdUI7Z0JBQ3hDLFdBQVcsRUFBTSwrQ0FBK0M7YUFDakU7U0FDRixDQUFDO0lBM0VGLENBQUM7SUFFRCxnREFBUSxHQUFSO1FBQUEsaUJBZ0JDO1FBZkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDN0MsS0FBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFBQyxDQUFDLENBQUMsQ0FBQztRQUMzQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDZCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDckQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsQ0FBQSxDQUFDO2dCQUMvQixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEQsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMzRCxDQUFDO1FBQ0gsQ0FBQztJQUNMLENBQUM7SUFDTywrREFBdUIsR0FBL0IsVUFBZ0MsTUFBTSxFQUFDLEVBQUU7UUFBekMsaUJBZUU7UUFkQSxJQUFJLENBQUMscUJBQXFCLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FDNUMsVUFBQSxRQUFRO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDNUIsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBRSxJQUFJLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDdEQsS0FBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxLQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNMLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3pDLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDdEQsQ0FBQztRQUNILENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLGlEQUFTLEdBQWpCO1FBQUEsaUJBYUc7UUFaQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDeEMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNULGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUMxQjthQUNGO1NBQ0YsQ0FBQyxDQUFDO1FBQ0EsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZO2FBQ25DLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQXpCLENBQXlCLENBQUMsQ0FBQztRQUVoRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxrQ0FBa0M7SUFDM0QsQ0FBQztJQUNNLHNEQUFjLEdBQXRCLFVBQXVCLElBQVU7UUFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUMzQyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7UUFFdkMsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDL0MsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFZRCx3REFBZ0IsR0FBaEI7UUFBQSxpQkF3QkM7UUF0QkcsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDO1FBQ2QsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2hCLEVBQUUsR0FBSSxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdEMsd0NBQXdDO1lBQ3ZDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25DLE1BQU0sQ0FBQztZQUNULENBQUM7UUFDSixDQUFDO1FBQ0MsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNaLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUMsSUFBSSxDQUFDLE1BQU07WUFDckIsUUFBUSxFQUFDO2dCQUNQLElBQUksRUFBQyxFQUFFO2dCQUNQLE1BQU0sRUFBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLElBQUk7Z0JBQzNDLFdBQVcsRUFBQyxHQUFHO2FBQ2hCO1NBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUM3RCxVQUFBLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEVBQTVCLENBQTRCLEVBQ3BDLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBQUEsQ0FBQztJQUVNLHFEQUFhLEdBQXJCLFVBQXNCLFFBQVE7UUFFMUIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7WUFDbkQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUN4RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDN0IsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQzVDLENBQUM7WUFDSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04scUJBQUksQ0FDRixjQUFjLEVBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFDTyxvREFBWSxHQUFwQixVQUFzQixHQUFHO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0QiwwQkFBMEI7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQztnQkFDckQseUJBQXlCO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTiw0Q0FBNEM7Z0JBQzVDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsd0RBQXdEO2dCQUMxRCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBM0pRLDZCQUE2QjtRQVB6QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQywrRUFBK0UsQ0FBQztZQUMzRyxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDhFQUE4RSxDQUFDLENBQUM7WUFDMUcsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0Fhd0IsbUJBQVc7WUFDSiw4Q0FBcUI7WUFDNUIsZ0NBQWM7WUFDcEIsMEJBQVc7WUFDZCwwQ0FBbUI7WUFDbEIsZUFBTTtZQUNQLHVCQUFjO09BbEJsQiw2QkFBNkIsQ0E0SnpDO0lBQUQsb0NBQUM7Q0E1SkQsQUE0SkMsSUFBQTtBQTVKWSxzRUFBNkIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvY3JlYXRlLW9wdGlvbi1mb29kLWl0ZW0vY3JlYXRlLW9wdGlvbi1mb29kLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtICxBYnN0cmFjdENvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IE9wdGlvbkZvb2RJdGVtU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9vcHRpb25Gb29kSXRlbS5zZXJ2aWNlJztcbmltcG9ydCAgeyBBdXRoU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgZGVmYXVsdCBhcyBzd2FsIH0gZnJvbSAnc3dlZXRhbGVydDInO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlJztcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtJyxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtLmNvbXBvbmVudC5odG1sJyksXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ31cbn0pXG5leHBvcnQgY2xhc3MgQ3JlYXRlT3B0aW9uRm9vZEl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHByaXZhdGUgbXltb2RlbDogYW55O1xuXHRwcml2YXRlIGNyZWF0ZU9wdGlvbkl0ZW1Gb3JtOiBGb3JtR3JvdXA7XG5cdHByaXZhdGUgbmFtZTogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgaWQgOiBzdHJpbmc7XG4gIHByaXZhdGUgb3B0aW9uRm9vZEl0ZW06IGFueTtcbiAgcHJpdmF0ZSBzdWI6IGFueTtcbiAgcHJpdmF0ZSBlcnJvcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLFxuICBwcml2YXRlIE9wdGlvbkZvb2RJdGVtU2VydmljZTogT3B0aW9uRm9vZEl0ZW1TZXJ2aWNlLCBcbiAgcHJpdmF0ZSBTdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UsIFxuICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcbiAgcHJpdmF0ZSBub3RpZjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHsgXG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBsZXQgdXNlckluZm8gPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICBcdHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgdGhpcy5zdWIgPSB0aGlzLnJvdXRlLnBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcbiAgICB0aGlzLmlkID0gcGFyYW1zWydpZCddOyB9KTtcbiAgICBpZih0aGlzLmlkIT1udWxsKXtcbiAgICAgICAgdGhpcy5vcHRpb25Gb29kSXRlbSA9IHRoaXMuU3RvcmFnZVNlcnZpY2UuZ2V0U2NvcGUoKTtcbiAgICAgICAgaWYodGhpcy5vcHRpb25Gb29kSXRlbSA9PSBmYWxzZSl7XG4gICAgICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCx0aGlzLmlkKTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICB0aGlzLmNyZWF0ZU9wdGlvbkl0ZW1Gb3JtLmNvbnRyb2xzWyduYW1lJ10uc2V0VmFsdWUodGhpcy5vcHRpb25Gb29kSXRlbS5uYW1lKTtcbiAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlT3B0aW9uSXRlbUZvcm0uY29udHJvbHNbJ25hbWUnXTtcbiAgICAgICAgfVxuICAgICAgfVxuICB9XG4gIHByaXZhdGUgZmV0Y2hBbGxPcHRpb25Gb29kSXRlbXMocmVzdElkLGlkKSB7XG4gIFx0XHR0aGlzLk9wdGlvbkZvb2RJdGVtU2VydmljZS5nZXRPcHRpb25Gb29kSXRlbXMocmVzdElkLGlkKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlLm9wdGlvbnMpOyBcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlLm9wdGlvbnMhPW51bGwgJiYgcmVzcG9uc2Uub3B0aW9ucy5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICAgICAgIFx0ICB0aGlzLm9wdGlvbkZvb2RJdGVtID0gcmVzcG9uc2Uub3B0aW9uc1swXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVPcHRpb25JdGVtRm9ybS5jb250cm9sc1snbmFtZSddLnNldFZhbHVlKHRoaXMub3B0aW9uRm9vZEl0ZW0ubmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlT3B0aW9uSXRlbUZvcm0uY29udHJvbHNbJ25hbWUnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWYuZXJyb3IoJ0Zvb2QgaXRlbSBoYWQgZGVsZXRlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnb3B0aW9uLWZvb2QtaXRlbXMvbGlzdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbiAgcHJpdmF0ZSBidWlsZEZvcm0oKTogdm9pZCB7XG4gICAgICB0aGlzLmNyZWF0ZU9wdGlvbkl0ZW1Gb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAgICduYW1lJzogWycnLCBbXG4gICAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgICAgICBdXG4gICAgICAgIF1cbiAgICAgIH0pO1xuICAgICAgICAgdGhpcy5uYW1lID0gdGhpcy5jcmVhdGVPcHRpb25JdGVtRm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgICAgdGhpcy5jcmVhdGVPcHRpb25JdGVtRm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMub25WYWx1ZUNoYW5nZWQoZGF0YSkpO1xuICBcbiAgICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTsgLy8gKHJlKXNldCB2YWxpZGF0aW9uIG1lc3NhZ2VzIG5vd1xuICAgIH1cbiAgIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQoZGF0YT86IGFueSkge1xuICAgIGlmICghdGhpcy5jcmVhdGVPcHRpb25JdGVtRm9ybSkgeyByZXR1cm47IH1cbiAgICBjb25zdCBmb3JtID0gdGhpcy5jcmVhdGVPcHRpb25JdGVtRm9ybTtcblxuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuXG4gICAgICBpZiAoY29udHJvbCAmJiBjb250cm9sLmRpcnR5ICYmICFjb250cm9sLnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuICBmb3JtRXJyb3JzID0ge1xuICAgICAgJ25hbWUnOiBbXVxuICAgIH07XG5cbiAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICduYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIG5hbWUgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBuYW1lIG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9XG4gIH07XG5cbiAgb3B0aW9uSXRlbUNyZWF0ZSgpe1xuXG4gICAgICBsZXQgaWQgPSAnLTEnO1xuICAgICAgaWYodGhpcy5pZCE9bnVsbCl7XG4gICAgICAgIGlkICA9IHRoaXMuaWQ7XG4gICAgICB9XG4gICAgICB0aGlzLm9uVmFsdWVDaGFuZ2VkKCk7XG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxuICAgICAgIGlmKHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ubGVuZ3RoID4wKXtcbiAgICAgICAgIHJldHVybjtcbiAgICAgICB9XG4gICAgfVxuICAgICAgdGhpcy5vcHRpb25Gb29kSXRlbSA9IHtcbiAgICAgICAgICAgICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICAgICAnb3B0aW9uJzp7XG4gICAgICAgICAgICAgICAgICAnaWQnOmlkLFxuICAgICAgICAgICAgICAgICAgJ25hbWUnOnRoaXMuY3JlYXRlT3B0aW9uSXRlbUZvcm0udmFsdWUubmFtZSxcbiAgICAgICAgICAgICAgICAgICdpc19kZWxldGUnOicwJ1xuICAgICAgICAgICAgICAgIH19O1xuICAgICAgXHR0aGlzLk9wdGlvbkZvb2RJdGVtU2VydmljZS5jcmVhdGVPcHRpb25Gb29kSXRlbXModGhpcy5vcHRpb25Gb29kSXRlbSkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHRoaXMucHJvY2Vzc1Jlc3VsdChyZXNwb25zZSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcbiAgfTtcblxuICBwcml2YXRlIHByb2Nlc3NSZXN1bHQocmVzcG9uc2UpIHtcbiAgXG4gICAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgICAgICAgdGhpcy5ub3RpZi5zdWNjZXNzKCdOZXcgZm9vdCBpdGVtIGhhcyBiZWVuIGFkZGVkJyk7XG4gICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnb3B0aW9uLWZvb2QtaXRlbXMvbGlzdCcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5lcnJvciA9IHJlc3BvbnNlLmVycm9ycztcbiAgICAgICAgaWYgKHJlc3BvbnNlLmNvZGUgPT0gNDIyKSB7XG4gICAgICAgICAgaWYgKHRoaXMuZXJyb3IubmFtZSkge1xuICAgICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWyduYW1lJ10gPSB0aGlzLmVycm9yLm5hbWU7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHN3YWwoXG4gICAgICAgICAgICAnQ3JlYXRlIEZhaWwhJyxcbiAgICAgICAgICAgIHRoaXMuZXJyb3JbMF0sXG4gICAgICAgICAgICAnZXJyb3InXG4gICAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICAgIGlmIChyZXMuc3RhdHVzID09IDQwMSkge1xuICAgICAgICAvLyB0aGlzLmxvZ2luZmFpbGVkID0gdHJ1ZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHJlcy5kYXRhLmVycm9ycy5tZXNzYWdlWzBdID09ICdFbWFpbCBVbnZlcmlmaWVkJykge1xuICAgICAgICAgIC8vIHRoaXMudW52ZXJpZmllZCA9IHRydWVcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBvdGhlciBraW5kcyBvZiBlcnJvciByZXR1cm5lZCBmcm9tIHNlcnZlclxuICAgICAgICAgIGZvciAodmFyIGVycm9yIGluIHJlcy5kYXRhLmVycm9ycykge1xuICAgICAgICAgICAgLy8gdGhpcy5sb2dpbmZhaWxlZGVycm9yICs9IHJlcy5kYXRhLmVycm9yc1tlcnJvcl0gKyAnICdcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG59XG4iXX0=
