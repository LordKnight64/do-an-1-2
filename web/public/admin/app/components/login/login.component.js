"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var user_1 = require("app/models/user");
var user_service_1 = require("app/services/user.service");
var server_service_1 = require("app/services/server.service");
var router_animations_1 = require("../../router.animations");
var router_1 = require("@angular/router");
var utils_1 = require("app/utils/utils");
var auth_service_1 = require("app/services/auth.service");
var email_validator_directive_1 = require("app/utils/email-validator.directive");
var sweetalert2_1 = require("sweetalert2");
var ng2_select_1 = require("ng2-select");
var ng2_translate_1 = require("ng2-translate");
var environment_1 = require("environments/environment");
var login_modal_component_1 = require("app/components/login-modal/login-modal.component");
var LoginComponent = (function () {
    function LoginComponent(serverServ, userServ, viewContainerRef, authServ, fb, router, translateService) {
        this.serverServ = serverServ;
        this.userServ = userServ;
        this.viewContainerRef = viewContainerRef;
        this.authServ = authServ;
        this.fb = fb;
        this.router = router;
        this.translateService = translateService;
        this.isHidden = true;
        this.items = [];
        this.url = environment_1.ENVIRONMENT.DOMAIN_API + ':' + environment_1.ENVIRONMENT.SERVER_PORT + '/';
        this.formErrors = {
            'email': [],
            'password': []
        };
        this.validationMessages = {
            'email': {
                'required': 'The email is required.',
                'minlength': 'The email must be at least 6 characters long.',
                'invalidEmail': 'The email must be a valid email address.',
            },
            'password': {
                'required': 'The password is required.',
                'minlength': 'The password must be at least 6 characters long.',
            }
        };
        /*select start*/
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.buildForm();
        window.dispatchEvent(new Event('resize'));
        this.items.push({ id: 1, text: "English" });
        this.items.push({ id: 2, text: "Vi\u1EC7t Nam" });
        this.items.push({ id: 3, text: "Japan" });
        // this.SelectId.items = this.items;
        // this.SelectId.active = this.items[1];
        if (localStorage.getItem('langChoosen') != undefined && localStorage.getItem('langChoosen') != null) {
            this.translateService.setDefaultLang(localStorage.getItem('langChoosen'));
            this.translateService.use(localStorage.getItem('langChoosen'));
            for (var i = 1; i <= this.items.length; i++) {
                var langDisplay = null;
                if (this.items[i - 1].id == 1) {
                    langDisplay = 'en';
                }
                if (this.items[i - 1].id == 2) {
                    langDisplay = 'vi';
                }
                if (this.items[i - 1].id == 3) {
                    langDisplay = 'ja';
                }
                if (langDisplay == localStorage.getItem('langChoosen')) {
                    this.value = this.items[i - 1];
                }
            }
        }
        else {
            this.value = this.items[0];
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.isHidden = false;
        // test les champs en js
        // envoyer les champs a php
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        // si retour positif, log le user
        if (1 === 1) {
            var action = 'login';
            var params = {
                'password': this.loginForm.value.password,
                'email': this.loginForm.value.email,
                'rest_id': 1
            };
            this.serverServ.doPost(action, params).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedLogin.bind(error); });
            // this.router.navigate( ['home'] );
        }
        else {
            // je recupere l'erreur du php
            // et on le place dans un label, ou un toaster
        }
    };
    LoginComponent.prototype.processResult = function (response) {
        this.isHidden = true;
        if (response.message == undefined) {
            if (response.restaurants.length > 1) {
                this.lgModal.restaurants = response.restaurants;
                this.lgModal.response = response;
                this.lgModal.user = response.user_info;
                this.lgModal.show();
            }
            else {
                var user = response.user_info;
                var restaurant = response.restaurants;
                console.log('user ', restaurant.lenght);
                // var token = response.token;
                var loginedUser = new user_1.User({
                    email: user.email,
                    firstname: user.first_name,
                    lastname: user.last_name,
                    roles: user.roles,
                    phone: user.phone,
                    tel: user.tel
                }, restaurant);
                // set token
                this.authServ.setToken(response.token);
                loginedUser.isInit = true;
                this.userServ.setCurrentUser(loginedUser);
                this.userServ.setUserInfo(response);
            }
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.email) {
                    this.formErrors['email'] = this.error.email;
                }
                if (this.error.password) {
                    this.formErrors['password'] = this.error.password;
                }
            }
            else if (response.code == 401) {
                sweetalert2_1.default('Login Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    LoginComponent.prototype.failedLogin = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    LoginComponent.prototype.buildForm = function () {
        var _this = this;
        this.loginForm = this.fb.group({
            'email': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(6),
                    email_validator_directive_1.invalidEmailValidator()
                ]
            ],
            'password': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(6),
                ]
            ]
        });
        this.email = this.loginForm.controls['email'];
        this.password = this.loginForm.controls['password'];
        this.loginForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    LoginComponent.prototype.onValueChanged = function (data) {
        if (!this.loginForm) {
            return;
        }
        var form = this.loginForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    Object.defineProperty(LoginComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.selected = function (value) {
        var langchoosen = 'en';
        if (value.id == 2) {
            langchoosen = 'vi';
        }
        if (value.id == 3) {
            langchoosen = 'ja';
        }
        if (localStorage.getItem('langChoosen') != undefined && localStorage.getItem('langChoosen') != null && langchoosen != localStorage.getItem('langChoosen')) {
            localStorage.setItem('langChoosen', langchoosen);
            this.translateService.setDefaultLang(localStorage.getItem('langChoosen'));
            this.translateService.use(localStorage.getItem('langChoosen'));
        }
        console.log('Selected value is: ', value);
    };
    LoginComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    LoginComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    LoginComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], LoginComponent.prototype, "SelectId", void 0);
    __decorate([
        core_1.ViewChild('childModal'),
        __metadata("design:type", login_modal_component_1.LoginModalComponent)
    ], LoginComponent.prototype, "lgModal", void 0);
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            styleUrls: [utils_1.default.getView('app/components/login/login.component.css')],
            templateUrl: utils_1.default.getView('app/components/login/login.component.html'),
            animations: [router_animations_1.routerGrow()],
            host: { '[@routerGrow]': '' }
        }),
        __metadata("design:paramtypes", [server_service_1.ServerService,
            user_service_1.UserService,
            core_1.ViewContainerRef,
            auth_service_1.AuthService,
            forms_1.FormBuilder,
            router_1.Router,
            ng2_translate_1.TranslateService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUEwRjtBQUMxRix3Q0FBNEY7QUFDNUYsd0NBQXVDO0FBQ3ZDLDBEQUF3RDtBQUN4RCw4REFBNEQ7QUFDNUQsNkRBQXFEO0FBQ3JELDBDQUF5QztBQUN6Qyx5Q0FBb0M7QUFFcEMsMERBQXdEO0FBQ3hELGlGQUE0RTtBQUM1RSwyQ0FBOEM7QUFDOUMseUNBQTZDO0FBQzdDLCtDQUFpRDtBQUNqRCx3REFBdUQ7QUFHdkQsMEZBQXNGO0FBY3RGO0lBYUUsd0JBQ1UsVUFBeUIsRUFDekIsUUFBcUIsRUFFckIsZ0JBQWtDLEVBRWxDLFFBQXFCLEVBQ3JCLEVBQWUsRUFDZixNQUFjLEVBQ2QsZ0JBQWtDO1FBUmxDLGVBQVUsR0FBVixVQUFVLENBQWU7UUFDekIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUVyQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBRWxDLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBakJwQyxhQUFRLEdBQVEsSUFBSSxDQUFDO1FBRXJCLFVBQUssR0FBZSxFQUFFLENBQUM7UUFDdkIsUUFBRyxHQUFHLHlCQUFXLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyx5QkFBVyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7UUFrTTNFLGVBQVUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsVUFBVSxFQUFFLEVBQUU7U0FFZixDQUFDO1FBRUYsdUJBQWtCLEdBQUc7WUFDbkIsT0FBTyxFQUFFO2dCQUNQLFVBQVUsRUFBTyx3QkFBd0I7Z0JBQ3pDLFdBQVcsRUFBTSwrQ0FBK0M7Z0JBQ2hFLGNBQWMsRUFBRywwQ0FBMEM7YUFDNUQ7WUFDRCxVQUFVLEVBQUU7Z0JBQ1YsVUFBVSxFQUFPLDJCQUEyQjtnQkFDNUMsV0FBVyxFQUFNLGtEQUFrRDthQUNwRTtTQUNGLENBQUM7UUFFRixnQkFBZ0I7UUFFVixVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2IsZUFBVSxHQUFVLEdBQUcsQ0FBQztRQUN4QixhQUFRLEdBQVcsS0FBSyxDQUFDO0lBdk1qQyxDQUFDO0lBRU0saUNBQVEsR0FBZjtRQUVFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixNQUFNLENBQUMsYUFBYSxDQUFFLElBQUksS0FBSyxDQUFFLFFBQVEsQ0FBRSxDQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsZUFBVSxFQUFFLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDMUMsb0NBQW9DO1FBQ3BDLHdDQUF3QztRQUV2QyxFQUFFLENBQUEsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFFLFNBQVMsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDL0YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDL0QsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUMxQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUN0QixXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixDQUFDO2dCQUNBLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUN2QixXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixDQUFDO2dCQUNBLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUN2QixXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixDQUFDO2dCQUNGLEVBQUUsQ0FBQSxDQUFDLFdBQVcsSUFBRSxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsQ0FBQztvQkFDcEQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsQ0FBQztZQUNGLENBQUM7UUFDSCxDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDSixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0IsQ0FBQztJQUNILENBQUM7SUFDTyw4QkFBSyxHQUFiO1FBQUEsaUJBa0NDO1FBaENDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBRXRCLHdCQUF3QjtRQUV4QiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLHdDQUF3QztZQUN2QyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNuQyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0osQ0FBQztRQUNELGlDQUFpQztRQUNqQyxFQUFFLENBQUMsQ0FBRSxDQUFDLEtBQUssQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNyQixJQUFJLE1BQU0sR0FBRztnQkFDVCxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUTtnQkFDekMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUs7Z0JBQ25DLFNBQVMsRUFBRSxDQUFDO2FBRWYsQ0FBQztZQUVGLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQzVCLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBNUIsQ0FBNEIsRUFDekMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFDO1lBRXZELG9DQUFvQztRQUN0QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTiw4QkFBOEI7WUFDOUIsOENBQThDO1FBQ2hELENBQUM7SUFFSCxDQUFDO0lBRU8sc0NBQWEsR0FBckIsVUFBc0IsUUFBUTtRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztnQkFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO2dCQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZCLENBQUM7WUFDRCxJQUFJLENBQ0osQ0FBQztnQkFDRyxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUM5QixJQUFJLFVBQVUsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO2dCQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRXhDLDhCQUE4QjtnQkFDOUIsSUFBSSxXQUFXLEdBQUcsSUFBSSxXQUFJLENBQUU7b0JBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztvQkFDakIsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVO29CQUMxQixRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0JBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztvQkFDakIsS0FBSyxFQUFDLElBQUksQ0FBQyxLQUFLO29CQUNoQixHQUFHLEVBQUMsSUFBSSxDQUFDLEdBQUc7aUJBQ2YsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFFakIsWUFBWTtnQkFDWixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3ZDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBRSxXQUFXLENBQUUsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUUsUUFBUSxDQUFFLENBQUM7WUFDMUMsQ0FBQztRQUdILENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUM3QixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztnQkFDOUMsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7Z0JBQ3BELENBQUM7WUFFSCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEMscUJBQUksQ0FDRixhQUFhLEVBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixDQUFDO1FBQ0wsQ0FBQztJQUNELENBQUM7SUFFTyxvQ0FBVyxHQUFuQixVQUFxQixHQUFHO1FBQ3RCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0QiwwQkFBMEI7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQztnQkFDckQseUJBQXlCO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTiw0Q0FBNEM7Z0JBQzVDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsd0RBQXdEO2dCQUMxRCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRU8sa0NBQVMsR0FBakI7UUFBQSxpQkFvQkM7UUFuQkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUM3QixPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ1Ysa0JBQVUsQ0FBQyxRQUFRO29CQUNuQixrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLGlEQUFxQixFQUFFO2lCQUN4QjthQUNGO1lBQ0YsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNaLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2lCQUN4QjthQUNGO1NBQ0YsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXBELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUN4QixTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUF6QixDQUF5QixDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsa0NBQWtDO0lBQzNELENBQUM7SUFDTyx1Q0FBYyxHQUF0QixVQUF1QixJQUFVO1FBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQ2hDLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFFNUIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDL0MsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUEwQkQsc0JBQVkscUNBQVM7YUFBckI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDO2FBRUQsVUFBc0IsS0FBWTtZQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssR0FBRyxDQUFDO1FBQzFDLENBQUM7OztPQUxBO0lBT00saUNBQVEsR0FBZixVQUFnQixLQUFTO1FBQ3ZCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQztRQUN2QixFQUFFLENBQUEsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDaEIsV0FBVyxHQUFHLElBQUksQ0FBQztRQUNyQixDQUFDO1FBQ0QsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ2hCLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDckIsQ0FBQztRQUNELEVBQUUsQ0FBQSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUUsU0FBUyxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUUsSUFBSSxJQUFJLFdBQVcsSUFBRSxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNsSixZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUMxRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUNqRSxDQUFDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU0sZ0NBQU8sR0FBZCxVQUFlLEtBQVM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sOEJBQUssR0FBWixVQUFhLEtBQVM7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0scUNBQVksR0FBbkIsVUFBb0IsS0FBUztRQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBNVBzQjtRQUF0QixnQkFBUyxDQUFDLFVBQVUsQ0FBQztrQ0FBbUIsNEJBQWU7b0RBQUM7SUFDaEM7UUFBeEIsZ0JBQVMsQ0FBQyxZQUFZLENBQUM7a0NBQWtCLDJDQUFtQjttREFBQztJQVZuRCxjQUFjO1FBUjFCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7WUFDdEUsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsMkNBQTJDLENBQUM7WUFDdkUsVUFBVSxFQUFFLENBQUMsOEJBQVUsRUFBRSxDQUFDO1lBQzFCLElBQUksRUFBRSxFQUFDLGVBQWUsRUFBRSxFQUFFLEVBQUM7U0FFNUIsQ0FBQzt5Q0Flc0IsOEJBQWE7WUFDZiwwQkFBVztZQUVILHVCQUFnQjtZQUV4QiwwQkFBVztZQUNqQixtQkFBVztZQUNQLGVBQU07WUFDSSxnQ0FBZ0I7T0F0QmpDLGNBQWMsQ0F1UTFCO0lBQUQscUJBQUM7Q0F2UUQsQUF1UUMsSUFBQTtBQXZRWSx3Q0FBYyIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtICxBYnN0cmFjdENvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgVXNlciB9IGZyb20gJ2FwcC9tb2RlbHMvdXNlcic7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc2VydmVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyByb3V0ZXJHcm93IH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcclxuaW1wb3J0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpXHJcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgaW52YWxpZEVtYWlsVmFsaWRhdG9yIH0gZnJvbSBcImFwcC91dGlscy9lbWFpbC12YWxpZGF0b3IuZGlyZWN0aXZlXCI7XHJcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcclxuaW1wb3J0IHsgU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnbmcyLXNlbGVjdCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICduZzItdHJhbnNsYXRlJztcclxuaW1wb3J0IHsgRU5WSVJPTk1FTlQgfSBmcm9tICdlbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xyXG5pbXBvcnQgeyBNb2RhbE1vZHVsZSAsIE1vZGFsRGlyZWN0aXZlfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuXHJcbmltcG9ydCB7IExvZ2luTW9kYWxDb21wb25lbnR9IGZyb20gJ2FwcC9jb21wb25lbnRzL2xvZ2luLW1vZGFsL2xvZ2luLW1vZGFsLmNvbXBvbmVudCc7XHJcblxyXG5cclxuXHJcblxyXG5kZWNsYXJlIHZhciBteUV4dE9iamVjdDogYW55O1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1sb2dpbicsXHJcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcycpXSxcclxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50Lmh0bWwnKSxcclxuICBhbmltYXRpb25zOiBbcm91dGVyR3JvdygpXSxcclxuICBob3N0OiB7J1tAcm91dGVyR3Jvd10nOiAnJ31cclxuXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcHJpdmF0ZSBwYXNzd29yZDogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgZW1haWw6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGVycm9yOiBhbnk7XHJcbiAgcHJpdmF0ZSBsb2dpbkZvcm06IEZvcm1Hcm91cDtcclxuICBwcml2YXRlIGlzSGlkZGVuOiBhbnkgPSB0cnVlO1xyXG4gICBcclxuICBwcml2YXRlIGl0ZW1zOiBBcnJheTxhbnk+ID0gW107XHJcbiAgcHJpdmF0ZSB1cmwgPSBFTlZJUk9OTUVOVC5ET01BSU5fQVBJICsgJzonICsgRU5WSVJPTk1FTlQuU0VSVkVSX1BPUlQgKyAnLyc7ICBcclxuICBAVmlld0NoaWxkKCdTZWxlY3RJZCcpIHByaXZhdGUgU2VsZWN0SWQ6IFNlbGVjdENvbXBvbmVudDtcclxuICBAVmlld0NoaWxkKCdjaGlsZE1vZGFsJykgcHJpdmF0ZSBsZ01vZGFsOiBMb2dpbk1vZGFsQ29tcG9uZW50O1xyXG5cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcclxuICBcclxuICAgIHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcclxuICAgXHJcbiAgICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcclxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgdHJhbnNsYXRlU2VydmljZTogVHJhbnNsYXRlU2VydmljZVxyXG4gICkge1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgbmdPbkluaXQoKSB7XHJcbiAgIFxyXG4gICAgdGhpcy5idWlsZEZvcm0oKTtcclxuICAgIHdpbmRvdy5kaXNwYXRjaEV2ZW50KCBuZXcgRXZlbnQoICdyZXNpemUnICkgKTtcclxuICAgIHRoaXMuaXRlbXMucHVzaCh7IGlkOiAxLCB0ZXh0OiBgRW5nbGlzaGAgfSk7XHJcbiAgICB0aGlzLml0ZW1zLnB1c2goeyBpZDogMiwgdGV4dDogYFZp4buHdCBOYW1gIH0pO1xyXG4gICAgdGhpcy5pdGVtcy5wdXNoKHsgaWQ6IDMsIHRleHQ6IGBKYXBhbmAgfSk7XHJcbiAgICAvLyB0aGlzLlNlbGVjdElkLml0ZW1zID0gdGhpcy5pdGVtcztcclxuICAgIC8vIHRoaXMuU2VsZWN0SWQuYWN0aXZlID0gdGhpcy5pdGVtc1sxXTtcclxuICAgIFxyXG4gICAgIGlmKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsYW5nQ2hvb3NlbicpIT11bmRlZmluZWQgJiYgbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xhbmdDaG9vc2VuJykhPW51bGwpe1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVNlcnZpY2Uuc2V0RGVmYXVsdExhbmcobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xhbmdDaG9vc2VuJykpO1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVNlcnZpY2UudXNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsYW5nQ2hvb3NlbicpKTtcclxuICAgICAgZm9yKHZhciBpID0gMTsgaSA8PSB0aGlzLml0ZW1zLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICB2YXIgbGFuZ0Rpc3BsYXkgPSBudWxsO1xyXG4gICAgICAgIGlmKHRoaXMuaXRlbXNbaS0xXS5pZD09MSl7XHJcbiAgICAgICAgICAgIGxhbmdEaXNwbGF5ID0gJ2VuJztcclxuICAgICAgICB9XHJcbiAgICAgICAgIGlmKHRoaXMuaXRlbXNbaS0xXS5pZD09Mil7XHJcbiAgICAgICAgICAgIGxhbmdEaXNwbGF5ID0gJ3ZpJztcclxuICAgICAgICB9XHJcbiAgICAgICAgIGlmKHRoaXMuaXRlbXNbaS0xXS5pZD09Myl7XHJcbiAgICAgICAgICAgIGxhbmdEaXNwbGF5ID0gJ2phJztcclxuICAgICAgICB9XHJcbiAgICAgICBpZihsYW5nRGlzcGxheT09bG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xhbmdDaG9vc2VuJykpe1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLml0ZW1zW2ktMV07XHJcbiAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1lbHNle1xyXG4gICAgICB0aGlzLnZhbHVlID0gdGhpcy5pdGVtc1swXTtcclxuICAgIH1cclxuICB9XHJcbiAgcHJpdmF0ZSBsb2dpbigpIHtcclxuICBcclxuICAgIHRoaXMuaXNIaWRkZW4gPSBmYWxzZTtcclxuICAgXHJcbiAgICAvLyB0ZXN0IGxlcyBjaGFtcHMgZW4ganNcclxuXHJcbiAgICAvLyBlbnZveWVyIGxlcyBjaGFtcHMgYSBwaHBcclxuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTtcclxuICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xyXG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXHJcbiAgICAgICBpZih0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+MCl7XHJcbiAgICAgICAgIHJldHVybjtcclxuICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIHNpIHJldG91ciBwb3NpdGlmLCBsb2cgbGUgdXNlclxyXG4gICAgaWYgKCAxID09PSAxICkgeyAgIFxyXG4gICAgICB2YXIgYWN0aW9uID0gJ2xvZ2luJztcclxuICAgICAgdmFyIHBhcmFtcyA9IHtcclxuICAgICAgICAgICdwYXNzd29yZCc6IHRoaXMubG9naW5Gb3JtLnZhbHVlLnBhc3N3b3JkLFxyXG4gICAgICAgICAgJ2VtYWlsJzogdGhpcy5sb2dpbkZvcm0udmFsdWUuZW1haWwsXHJcbiAgICAgICAgICAncmVzdF9pZCc6IDFcclxuICAgICAgIFxyXG4gICAgICB9O1xyXG4gICAgICBcclxuICAgICAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcykudGhlbihcclxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHRoaXMucHJvY2Vzc1Jlc3VsdChyZXNwb25zZSksXHJcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZExvZ2luLmJpbmQoZXJyb3IpKTtcclxuXHJcbiAgICAgIC8vIHRoaXMucm91dGVyLm5hdmlnYXRlKCBbJ2hvbWUnXSApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gamUgcmVjdXBlcmUgbCdlcnJldXIgZHUgcGhwXHJcbiAgICAgIC8vIGV0IG9uIGxlIHBsYWNlIGRhbnMgdW4gbGFiZWwsIG91IHVuIHRvYXN0ZXJcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByb2Nlc3NSZXN1bHQocmVzcG9uc2UpIHtcclxuICAgIHRoaXMuaXNIaWRkZW4gPSB0cnVlO1xyXG4gICAgaWYgKHJlc3BvbnNlLm1lc3NhZ2UgPT0gdW5kZWZpbmVkKSB7ICBcclxuICAgICAgaWYocmVzcG9uc2UucmVzdGF1cmFudHMubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICB0aGlzLmxnTW9kYWwucmVzdGF1cmFudHMgPSByZXNwb25zZS5yZXN0YXVyYW50cztcclxuICAgICAgICAgdGhpcy5sZ01vZGFsLnJlc3BvbnNlID0gcmVzcG9uc2U7XHJcbiAgICAgICAgIHRoaXMubGdNb2RhbC51c2VyID0gcmVzcG9uc2UudXNlcl9pbmZvO1xyXG4gICAgICAgICB0aGlzLmxnTW9kYWwuc2hvdygpOyAgXHJcbiAgICAgIH1cclxuICAgICAgZWxzZVxyXG4gICAgICB7XHJcbiAgICAgICAgICB2YXIgdXNlciA9IHJlc3BvbnNlLnVzZXJfaW5mbztcclxuICAgICAgICAgIHZhciByZXN0YXVyYW50ID0gcmVzcG9uc2UucmVzdGF1cmFudHM7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZygndXNlciAnLCByZXN0YXVyYW50LmxlbmdodCk7XHJcbiAgICBcclxuICAgICAgICAgIC8vIHZhciB0b2tlbiA9IHJlc3BvbnNlLnRva2VuO1xyXG4gICAgICAgICAgbGV0IGxvZ2luZWRVc2VyID0gbmV3IFVzZXIoIHsgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgZW1haWw6IHVzZXIuZW1haWwsXHJcbiAgICAgICAgICAgICAgICBmaXJzdG5hbWU6IHVzZXIuZmlyc3RfbmFtZSxcclxuICAgICAgICAgICAgICAgIGxhc3RuYW1lOiB1c2VyLmxhc3RfbmFtZSxcclxuICAgICAgICAgICAgICAgIHJvbGVzOiB1c2VyLnJvbGVzLFxyXG4gICAgICAgICAgICAgICAgcGhvbmU6dXNlci5waG9uZSxcclxuICAgICAgICAgICAgICAgIHRlbDp1c2VyLnRlbFxyXG4gICAgICAgICAgICB9LCByZXN0YXVyYW50KTtcclxuICAgIFxyXG4gICAgICAgICAgLy8gc2V0IHRva2VuXHJcbiAgICAgICAgICB0aGlzLmF1dGhTZXJ2LnNldFRva2VuKHJlc3BvbnNlLnRva2VuKTsgXHJcbiAgICAgICAgICBsb2dpbmVkVXNlci5pc0luaXQgPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy51c2VyU2Vydi5zZXRDdXJyZW50VXNlciggbG9naW5lZFVzZXIgKTtcclxuICAgICAgICAgIHRoaXMudXNlclNlcnYuc2V0VXNlckluZm8oIHJlc3BvbnNlICk7XHJcbiAgICAgIH1cclxuXHJcbiAgICBcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZXJyb3IgPSByZXNwb25zZS5lcnJvcnM7ICBcclxuICAgICAgaWYgKHJlc3BvbnNlLmNvZGUgPT0gNDIyKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZXJyb3IuZW1haWwpIHtcclxuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1snZW1haWwnXSA9IHRoaXMuZXJyb3IuZW1haWw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmVycm9yLnBhc3N3b3JkKSB7XHJcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ3Bhc3N3b3JkJ10gPSB0aGlzLmVycm9yLnBhc3N3b3JkO1xyXG4gICAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UuY29kZSA9PSA0MDEpIHtcclxuICAgICAgICBzd2FsKFxyXG4gICAgICAgICAgJ0xvZ2luIEZhaWwhJyxcclxuICAgICAgICAgIHRoaXMuZXJyb3JbMF0sXHJcbiAgICAgICAgICAnZXJyb3InXHJcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBmYWlsZWRMb2dpbiAocmVzKSB7XHJcbiAgICBpZiAocmVzLnN0YXR1cyA9PSA0MDEpIHtcclxuICAgICAgLy8gdGhpcy5sb2dpbmZhaWxlZCA9IHRydWVcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChyZXMuZGF0YS5lcnJvcnMubWVzc2FnZVswXSA9PSAnRW1haWwgVW52ZXJpZmllZCcpIHtcclxuICAgICAgICAvLyB0aGlzLnVudmVyaWZpZWQgPSB0cnVlXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gb3RoZXIga2luZHMgb2YgZXJyb3IgcmV0dXJuZWQgZnJvbSBzZXJ2ZXJcclxuICAgICAgICBmb3IgKHZhciBlcnJvciBpbiByZXMuZGF0YS5lcnJvcnMpIHtcclxuICAgICAgICAgIC8vIHRoaXMubG9naW5mYWlsZWRlcnJvciArPSByZXMuZGF0YS5lcnJvcnNbZXJyb3JdICsgJyAnXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIHByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xyXG4gICAgdGhpcy5sb2dpbkZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgJ2VtYWlsJzogWycnLCBbXHJcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxyXG4gICAgICAgICAgVmFsaWRhdG9ycy5taW5MZW5ndGgoNiksXHJcbiAgICAgICAgICBpbnZhbGlkRW1haWxWYWxpZGF0b3IoKVxyXG4gICAgICAgIF1cclxuICAgICAgXSxcclxuICAgICAncGFzc3dvcmQnOiBbJycsIFtcclxuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXHJcbiAgICAgICAgICBWYWxpZGF0b3JzLm1pbkxlbmd0aCg2KSxcclxuICAgICAgICBdXHJcbiAgICAgIF1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5lbWFpbCA9IHRoaXMubG9naW5Gb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgdGhpcy5wYXNzd29yZCA9IHRoaXMubG9naW5Gb3JtLmNvbnRyb2xzWydwYXNzd29yZCddO1xyXG4gIFxyXG4gICAgdGhpcy5sb2dpbkZvcm0udmFsdWVDaGFuZ2VzXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGRhdGEpKTtcclxuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTsgLy8gKHJlKXNldCB2YWxpZGF0aW9uIG1lc3NhZ2VzIG5vd1xyXG4gIH1cclxuICBwcml2YXRlIG9uVmFsdWVDaGFuZ2VkKGRhdGE/OiBhbnkpIHtcclxuICAgIGlmICghdGhpcy5sb2dpbkZvcm0pIHsgcmV0dXJuOyB9XHJcbiAgICBjb25zdCBmb3JtID0gdGhpcy5sb2dpbkZvcm07XHJcblxyXG4gICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcclxuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxyXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XHJcbiAgICAgIGNvbnN0IGNvbnRyb2wgPSBmb3JtLmdldChmaWVsZCk7XHJcblxyXG4gICAgICBpZiAoY29udHJvbCAmJiBjb250cm9sLmRpcnR5ICYmICFjb250cm9sLnZhbGlkKSB7XHJcbiAgICAgICAgY29uc3QgbWVzc2FnZXMgPSB0aGlzLnZhbGlkYXRpb25NZXNzYWdlc1tmaWVsZF07XHJcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJvbC5lcnJvcnMpIHtcclxuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZvcm1FcnJvcnMgPSB7XHJcbiAgICAnZW1haWwnOiBbXSxcclxuICAgICdwYXNzd29yZCc6IFtdXHJcbiAgIFxyXG4gIH07XHJcblxyXG4gIHZhbGlkYXRpb25NZXNzYWdlcyA9IHtcclxuICAgICdlbWFpbCc6IHtcclxuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIGVtYWlsIGlzIHJlcXVpcmVkLicsXHJcbiAgICAgICdtaW5sZW5ndGgnOiAgICAgJ1RoZSBlbWFpbCBtdXN0IGJlIGF0IGxlYXN0IDYgY2hhcmFjdGVycyBsb25nLicsXHJcbiAgICAgICdpbnZhbGlkRW1haWwnOiAgJ1RoZSBlbWFpbCBtdXN0IGJlIGEgdmFsaWQgZW1haWwgYWRkcmVzcy4nLFxyXG4gICAgfSxcclxuICAgICdwYXNzd29yZCc6IHtcclxuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIHBhc3N3b3JkIGlzIHJlcXVpcmVkLicsXHJcbiAgICAgICdtaW5sZW5ndGgnOiAgICAgJ1RoZSBwYXNzd29yZCBtdXN0IGJlIGF0IGxlYXN0IDYgY2hhcmFjdGVycyBsb25nLicsXHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgLypzZWxlY3Qgc3RhcnQqL1xyXG5cclxucHJpdmF0ZSB2YWx1ZTphbnkgPSB7fTtcclxuICBwcml2YXRlIF9kaXNhYmxlZFY6c3RyaW5nID0gJzAnO1xyXG4gIHByaXZhdGUgZGlzYWJsZWQ6Ym9vbGVhbiA9IGZhbHNlO1xyXG4gXHJcbiAgcHJpdmF0ZSBnZXQgZGlzYWJsZWRWKCk6c3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZFY7XHJcbiAgfVxyXG4gXHJcbiAgcHJpdmF0ZSBzZXQgZGlzYWJsZWRWKHZhbHVlOnN0cmluZykge1xyXG4gICAgdGhpcy5fZGlzYWJsZWRWID0gdmFsdWU7XHJcbiAgICB0aGlzLmRpc2FibGVkID0gdGhpcy5fZGlzYWJsZWRWID09PSAnMSc7XHJcbiAgfVxyXG4gXHJcbiAgcHVibGljIHNlbGVjdGVkKHZhbHVlOmFueSk6dm9pZCB7XHJcbiAgICBsZXQgbGFuZ2Nob29zZW4gPSAnZW4nO1xyXG4gICAgaWYodmFsdWUuaWQgPT0gMil7XHJcbiAgICAgIGxhbmdjaG9vc2VuID0gJ3ZpJztcclxuICAgIH1cclxuICAgIGlmKHZhbHVlLmlkID09IDMpe1xyXG4gICAgICBsYW5nY2hvb3NlbiA9ICdqYSc7XHJcbiAgICB9XHJcbiAgICBpZihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbGFuZ0Nob29zZW4nKSE9dW5kZWZpbmVkICYmIGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsYW5nQ2hvb3NlbicpIT1udWxsICYmIGxhbmdjaG9vc2VuIT1sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbGFuZ0Nob29zZW4nKSl7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdsYW5nQ2hvb3NlbicsIGxhbmdjaG9vc2VuKTsgXHJcbiAgICAgIHRoaXMudHJhbnNsYXRlU2VydmljZS5zZXREZWZhdWx0TGFuZyhsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbGFuZ0Nob29zZW4nKSk7XHJcbiAgICAgIHRoaXMudHJhbnNsYXRlU2VydmljZS51c2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xhbmdDaG9vc2VuJykpO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coJ1NlbGVjdGVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gXHJcbiAgcHVibGljIHJlbW92ZWQodmFsdWU6YW55KTp2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdSZW1vdmVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gXHJcbiAgcHVibGljIHR5cGVkKHZhbHVlOmFueSk6dm9pZCB7XHJcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xyXG4gIH1cclxuIFxyXG4gIHB1YmxpYyByZWZyZXNoVmFsdWUodmFsdWU6YW55KTp2b2lkIHtcclxuICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbn1cclxuIl19
