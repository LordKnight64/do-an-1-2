"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var ng2_select_1 = require("ng2-select");
var optionValueFoodItem_service_1 = require("app/services/optionValueFoodItem.service");
var optionFoodItem_service_1 = require("app/services/optionFoodItem.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var CreateOptionValueFoodItemComponent = (function () {
    function CreateOptionValueFoodItemComponent(fb, OptionValueFoodItemService, OptionFoodItemService, StorageService, authServ, notif, router, route) {
        this.fb = fb;
        this.OptionValueFoodItemService = OptionValueFoodItemService;
        this.OptionFoodItemService = OptionFoodItemService;
        this.StorageService = StorageService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.isHidden = false;
        this.items = [];
        this.formErrors = {
            'optionName': [],
            'name': []
        };
        this.validationMessages = {
            'optionName': {
                'required': 'The optionName is required.'
            },
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.',
            }
        };
        /*select start*/
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    CreateOptionValueFoodItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
        if (this.id != undefined && this.id != null) {
            this.optionFoodItem = this.StorageService.getScope();
            if (this.optionFoodItem == false) {
                this.fetchAllOptionValueFoodItems(this.restId, this.id);
            }
            else {
                this.createOptionValueItemForm.controls['name'].setValue(this.optionFoodItem.name);
                this.name = this.createOptionValueItemForm.controls['name'];
                this.optionFoodValueItem = this.StorageService.getScope();
                this.optionFoodItemId = this.optionFoodValueItem.option_id;
                this.fetchAllOptionFoodItems(this.restId);
            }
        }
        else {
            this.optionFoodItemId = this.StorageService.getScope();
            this.fetchAllOptionFoodItems(this.restId);
        }
    };
    CreateOptionValueFoodItemComponent.prototype.fetchAllOptionValueFoodItems = function (restId, id) {
        var _this = this;
        this.OptionValueFoodItemService.getOptionValueFoodItems(restId, id).then(function (response) {
            console.log(response.options);
            if (response.options != null && response.options.length > 0) {
                _this.optionValueFoodItem = response.options[0];
                _this.createOptionValueItemForm.controls['name'].setValue(_this.optionValueFoodItem.name);
                _this.createOptionValueItemForm.controls['optionName'].setValue(_this.optionValueFoodItem.option_id);
                _this.optionName = _this.createOptionValueItemForm.controls['optionName'];
                _this.name = _this.createOptionValueItemForm.controls['name'];
                _this.optionFoodItemId = _this.optionValueFoodItem.option_id;
                _this.fetchAllOptionFoodItems(_this.restId);
            }
            else {
                _this.notif.error('Food value item had delete');
                _this.router.navigateByUrl('option-value-food-items/list');
            }
        }, function (error) {
            console.log(error);
        });
    };
    CreateOptionValueFoodItemComponent.prototype.fetchAllOptionFoodItems = function (restId) {
        var _this = this;
        this.flgItemSelect = 0;
        this.OptionFoodItemService.getOptionFoodItems(restId, null).then(function (response) {
            _this.optionFoodItems = response.options;
            _this.items.push({
                id: 0,
                text: "choose value"
            });
            for (var i = 0; i < _this.optionFoodItems.length; i++) {
                _this.items.push({
                    id: _this.optionFoodItems[i].id,
                    text: "" + _this.optionFoodItems[i].name
                });
                if (_this.optionFoodItemId != undefined && _this.optionFoodItems[i].id == _this.optionFoodItemId) {
                    _this.flgItemSelect = i + 1;
                    _this.createOptionValueItemForm.controls['optionName'].setValue(_this.optionFoodItems[i].id);
                    _this.optionName = _this.createOptionValueItemForm.controls['optionName'];
                }
            }
            _this.select.items = _this.items;
            if (_this.flgItemSelect != undefined && _this.flgItemSelect != null) {
                _this.value = _this.items[_this.flgItemSelect];
                _this.valueChoose = _this.value.id;
            }
            if (_this.optionFoodItemId == undefined || _this.optionFoodItemId == false) {
                _this.value = _this.items[0];
                _this.createOptionValueItemForm.controls['optionName'].setValue(null);
                _this.optionName = _this.createOptionValueItemForm.controls['optionName'];
            }
            console.log(response.options);
        }, function (error) {
            console.log(error);
        });
    };
    CreateOptionValueFoodItemComponent.prototype.buildForm = function () {
        var _this = this;
        this.createOptionValueItemForm = this.fb.group({
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ],
            'optionName': ['', [
                    forms_1.Validators.required,
                ]
            ]
        });
        this.name = this.createOptionValueItemForm.controls['name'];
        this.optionName = this.createOptionValueItemForm.controls['optionName'];
        this.createOptionValueItemForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    CreateOptionValueFoodItemComponent.prototype.onValueChanged = function (data) {
        if (!this.createOptionValueItemForm) {
            return;
        }
        var form = this.createOptionValueItemForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    CreateOptionValueFoodItemComponent.prototype.optionValueItemCreate = function () {
        var _this = this;
        var id = '-1';
        if (this.id != null) {
            id = this.id;
        }
        var option_id = null;
        if (this.valueChoose != 0 && this.valueChoose != null) {
            option_id = this.value.id;
        }
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.optionFoodValueItem = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'option': {
                'id': id,
                'name': this.createOptionValueItemForm.value.name,
                'option_id': option_id,
                'is_delete': '0'
            }
        };
        this.OptionValueFoodItemService.createOptionValueFoodItems(this.optionFoodValueItem).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    CreateOptionValueFoodItemComponent.prototype.processResult = function (response) {
        if (response.message == undefined || response.message == 'OK') {
            this.notif.success('New Option item has been added');
            this.router.navigateByUrl('/option-value-food-items/list');
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.name) {
                    this.formErrors['name'] = this.error.name;
                }
                if (this.error.option_id) {
                    this.formErrors['optionName'] = this.error.option_id;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    CreateOptionValueFoodItemComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    Object.defineProperty(CreateOptionValueFoodItemComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    CreateOptionValueFoodItemComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
        this.createOptionValueItemForm.controls['optionName'].setValue(value.id);
        this.optionName = this.createOptionValueItemForm.controls['optionName'];
    };
    CreateOptionValueFoodItemComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    CreateOptionValueFoodItemComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    CreateOptionValueFoodItemComponent.prototype.refreshValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.value = value;
            this.valueChoose = value.id;
        }
        else {
            this.valueChoose = null;
        }
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], CreateOptionValueFoodItemComponent.prototype, "select", void 0);
    CreateOptionValueFoodItemComponent = __decorate([
        core_1.Component({
            selector: 'create-option-value-food-item',
            templateUrl: utils_1.default.getView('app/components/create-option-value-food-item/create-option-value-food-item.component.html'),
            styleUrls: [utils_1.default.getView('app/components/create-option-value-food-item/create-option-value-food-item.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' },
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            optionValueFoodItem_service_1.OptionValueFoodItemService,
            optionFoodItem_service_1.OptionFoodItemService,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            router_2.ActivatedRoute])
    ], CreateOptionValueFoodItemComponent);
    return CreateOptionValueFoodItemComponent;
}());
exports.CreateOptionValueFoodItemComponent = CreateOptionValueFoodItemComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NyZWF0ZS1vcHRpb24tdmFsdWUtZm9vZC1pdGVtL2NyZWF0ZS1vcHRpb24tdmFsdWUtZm9vZC1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUNBLHNDQUFnRjtBQUNoRix3Q0FBNEY7QUFDNUYseUNBQW9DO0FBR3BDLDZEQUEyRDtBQUMzRCx5Q0FBd0Q7QUFDeEQsd0ZBQXNGO0FBQ3RGLDhFQUE0RTtBQUM1RSwwREFBeUQ7QUFDekQsMkNBQThDO0FBQzlDLDBFQUF3RTtBQUN4RSwwQ0FBeUM7QUFDekMsMENBQWlEO0FBQ2pELGdFQUE4RDtBQVE5RDtJQW9CRSw0Q0FBb0IsRUFBZSxFQUMzQiwwQkFBc0QsRUFDdEQscUJBQTRDLEVBQzVDLGNBQThCLEVBQzlCLFFBQXFCLEVBQ3JCLEtBQTBCLEVBQzFCLE1BQWMsRUFDZCxLQUFxQjtRQVBULE9BQUUsR0FBRixFQUFFLENBQWE7UUFDM0IsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUN0RCwwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFVBQUssR0FBTCxLQUFLLENBQXFCO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQXJCckIsYUFBUSxHQUFVLEtBQUssQ0FBQztRQVl4QixVQUFLLEdBQWMsRUFBRSxDQUFDO1FBK0hoQyxlQUFVLEdBQUc7WUFDVCxZQUFZLEVBQUUsRUFBRTtZQUNoQixNQUFNLEVBQUUsRUFBRTtTQUNYLENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNwQixZQUFZLEVBQUU7Z0JBQ1gsVUFBVSxFQUFPLDZCQUE2QjthQUMvQztZQUNELE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQU8sdUJBQXVCO2dCQUN4QyxXQUFXLEVBQU0sK0NBQStDO2FBQ2pFO1NBQ0YsQ0FBQztRQTZESixnQkFBZ0I7UUFFUixVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2IsZUFBVSxHQUFVLEdBQUcsQ0FBQztRQUN4QixhQUFRLEdBQVcsS0FBSyxDQUFDO0lBbk1qQyxDQUFDO0lBRUQscURBQVEsR0FBUjtRQUFBLGlCQXVCQztRQXRCQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUM3QyxLQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUUsU0FBUyxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNwQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDckQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsQ0FBQSxDQUFDO2dCQUMvQixJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFekQsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNGLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQzFELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDO2dCQUMzRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlDLENBQUM7UUFDSCxDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN2RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pELENBQUM7SUFDTCxDQUFDO0lBQ08seUVBQTRCLEdBQXBDLFVBQXFDLE1BQU0sRUFBQyxFQUFFO1FBQTlDLGlCQW1CRTtRQWxCQSxJQUFJLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FDdEQsVUFBQSxRQUFRO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBRSxJQUFJLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDckQsS0FBSSxDQUFDLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDeEYsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNuRyxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3hFLEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDNUQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUM7Z0JBQzNELEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUMsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNMLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDLENBQUM7WUFDNUQsQ0FBQztRQUNILENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLG9FQUF1QixHQUEvQixVQUFnQyxNQUFNO1FBQXRDLGlCQWtDRTtRQWpDQSxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMscUJBQXFCLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDOUMsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO1lBQ3RDLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNSLEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxjQUFjO2FBQ3JCLENBQUMsQ0FBQztZQUNYLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDaEQsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7b0JBQ2QsRUFBRSxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDOUIsSUFBSSxFQUFFLEtBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFNO2lCQUN4QyxDQUFDLENBQUM7Z0JBQ0gsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLGdCQUFnQixJQUFFLFNBQVMsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQSxDQUFDO29CQUN0RixLQUFJLENBQUMsYUFBYSxHQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzFCLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzNGLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDNUUsQ0FBQztZQUNOLENBQUM7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDO1lBQy9CLEVBQUUsQ0FBQSxDQUFDLEtBQUksQ0FBQyxhQUFhLElBQUksU0FBUyxJQUFJLEtBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDL0QsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDNUMsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztZQUNwQyxDQUFDO1lBQ0YsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLGdCQUFnQixJQUFFLFNBQVMsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLElBQUUsS0FBSyxDQUFDLENBQUEsQ0FBQztnQkFDbkUsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixLQUFJLENBQUMseUJBQXlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckUsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMseUJBQXlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzFFLENBQUM7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDSSxzREFBUyxHQUFqQjtRQUFBLGlCQWtCRztRQWpCQyxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDOUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNSLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUMxQjthQUNGO1lBQ0QsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNmLGtCQUFVLENBQUMsUUFBUTtpQkFDcEI7YUFDRjtTQUNGLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVk7YUFDeEMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDO1FBRWhELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLGtDQUFrQztJQUMzRCxDQUFDO0lBQ1EsMkRBQWMsR0FBdEIsVUFBdUIsSUFBVTtRQUNoQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQ2hELElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQztRQUU1QyxHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyx3Q0FBd0M7WUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDNUIsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUVoQyxFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hELEdBQUcsQ0FBQyxDQUFDLElBQU0sR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQWdCSCxrRUFBcUIsR0FBckI7UUFBQSxpQkE0Qkc7UUEzQkQsSUFBSSxFQUFFLEdBQUksSUFBSSxDQUFDO1FBQ2YsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2hCLEVBQUUsR0FBSSxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxJQUFJLFNBQVMsR0FBSSxJQUFJLENBQUM7UUFDdEIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBRSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQzlDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUM5QixDQUFDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLHdDQUF3QztZQUN2QyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNuQyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0osQ0FBQztRQUNDLElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUNqQixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3JCLFFBQVEsRUFBQztnQkFDUCxJQUFJLEVBQUMsRUFBRTtnQkFDUCxNQUFNLEVBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLEtBQUssQ0FBQyxJQUFJO2dCQUNoRCxXQUFXLEVBQUMsU0FBUztnQkFDckIsV0FBVyxFQUFDLEdBQUc7YUFDaEI7U0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLDBCQUEwQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLElBQUksQ0FDNUUsVUFBQSxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixFQUNwQyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUNPLDBEQUFhLEdBQXJCLFVBQXNCLFFBQVE7UUFFNUIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUM5RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDN0IsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQzVDLENBQUM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUN2RCxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLHFCQUFJLENBQ0YsY0FBYyxFQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQ2IsT0FBTyxDQUNSLENBQUMsS0FBSyxDQUFDLHFCQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckIsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBQ08seURBQVksR0FBcEIsVUFBc0IsR0FBRztRQUN2QixxQkFBSSxDQUNFLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBUUQsc0JBQVkseURBQVM7YUFBckI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDO2FBRUQsVUFBc0IsS0FBWTtZQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssR0FBRyxDQUFDO1FBQzFDLENBQUM7OztPQUxBO0lBT00scURBQVEsR0FBZixVQUFnQixLQUFTO1FBRXZCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRU0sb0RBQU8sR0FBZCxVQUFlLEtBQVM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sa0RBQUssR0FBWixVQUFhLEtBQVM7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0seURBQVksR0FBbkIsVUFBb0IsS0FBUztRQUMxQixFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUUsU0FBUyxJQUFJLEtBQUssSUFBRSxJQUFJLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3RELElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUM3QixDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDSixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUMxQixDQUFDO0lBQ0osQ0FBQztJQS9Qc0I7UUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7a0NBQWdCLDRCQUFlO3NFQUFDO0lBRDNDLGtDQUFrQztRQVA5QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLCtCQUErQjtZQUN6QyxXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQywyRkFBMkYsQ0FBQztZQUN2SCxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDBGQUEwRixDQUFDLENBQUM7WUFDdEgsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0FxQndCLG1CQUFXO1lBQ0Msd0RBQTBCO1lBQy9CLDhDQUFxQjtZQUM1QixnQ0FBYztZQUNwQiwwQkFBVztZQUNkLDBDQUFtQjtZQUNsQixlQUFNO1lBQ1AsdUJBQWM7T0EzQmxCLGtDQUFrQyxDQW1ROUM7SUFBRCx5Q0FBQztDQW5RRCxBQW1RQyxJQUFBO0FBblFZLGdGQUFrQyIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTlRfVFlQRSB9IGZyb20gJ0Bhbmd1bGFyL2NvbXBpbGVyL3NyYy9vdXRwdXQvb3V0cHV0X2FzdCc7XG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCxWaWV3Q2hpbGQsVmlld0VuY2Fwc3VsYXRpb24gICB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtICxBYnN0cmFjdENvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUgfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyByb3V0ZXJUcmFuc2l0aW9uIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHtTZWxlY3RNb2R1bGUsU2VsZWN0Q29tcG9uZW50fSBmcm9tICduZzItc2VsZWN0JztcbmltcG9ydCB7IE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL29wdGlvblZhbHVlRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgeyBPcHRpb25Gb29kSXRlbVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvb3B0aW9uRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbScsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJyd9LFxufSlcbmV4cG9ydCBjbGFzcyBDcmVhdGVPcHRpb25WYWx1ZUZvb2RJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQFZpZXdDaGlsZCgnU2VsZWN0SWQnKSBwdWJsaWMgc2VsZWN0OiBTZWxlY3RDb21wb25lbnQ7XG4gIHByaXZhdGUgbXltb2RlbDogYW55O1xuXHRwcml2YXRlIGNyZWF0ZU9wdGlvblZhbHVlSXRlbUZvcm06IEZvcm1Hcm91cDtcblx0cHJpdmF0ZSBuYW1lOiBBYnN0cmFjdENvbnRyb2w7XG5cdHByaXZhdGUgb3B0aW9uTmFtZTogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIGlzSGlkZGVuOiBCb29sZWFuPWZhbHNlO1xuICBwcml2YXRlIGZsZ0l0ZW1TZWxlY3Q6IGFueTtcbiAgcHJpdmF0ZSB1c2VySWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgcmVzdElkIDogU3RyaW5nO1xuICBwcml2YXRlIGlkIDogc3RyaW5nO1xuICBwcml2YXRlIG9wdGlvbkZvb2RJdGVtOiBhbnk7XG4gIHByaXZhdGUgb3B0aW9uRm9vZEl0ZW1JZDogYW55O1xuICBwcml2YXRlIG9wdGlvbkZvb2RJdGVtcyA6IGFueTtcbiAgcHJpdmF0ZSBvcHRpb25Gb29kVmFsdWVJdGVtIDogYW55O1xuICBwcml2YXRlIHN1YjogYW55O1xuICBwcml2YXRlIGVycm9yOiBhbnk7XG4gIHByaXZhdGUgb3B0aW9uVmFsdWVGb29kSXRlbSA6IGFueTtcbiAgcHJpdmF0ZSBpdGVtczpBcnJheTxhbnk+ID0gW107XG4gIHByaXZhdGUgdmFsdWVDaG9vc2U6IGFueTtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gIHByaXZhdGUgT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2U6IE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlLFxuICBwcml2YXRlIE9wdGlvbkZvb2RJdGVtU2VydmljZTogT3B0aW9uRm9vZEl0ZW1TZXJ2aWNlLCBcbiAgcHJpdmF0ZSBTdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UsIFxuICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcbiAgcHJpdmF0ZSBub3RpZjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHsgXG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBsZXQgdXNlckluZm8gPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICBcdHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgdGhpcy5zdWIgPSB0aGlzLnJvdXRlLnBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcbiAgICB0aGlzLmlkID0gcGFyYW1zWydpZCddOyB9KTtcbiAgICBpZih0aGlzLmlkIT11bmRlZmluZWQgJiYgdGhpcy5pZCE9bnVsbCl7XG4gICAgICAgIHRoaXMub3B0aW9uRm9vZEl0ZW0gPSB0aGlzLlN0b3JhZ2VTZXJ2aWNlLmdldFNjb3BlKCk7XG4gICAgICAgIGlmKHRoaXMub3B0aW9uRm9vZEl0ZW0gPT0gZmFsc2Upe1xuICAgICAgICAgIHRoaXMuZmV0Y2hBbGxPcHRpb25WYWx1ZUZvb2RJdGVtcyh0aGlzLnJlc3RJZCx0aGlzLmlkKTtcblxuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS5jb250cm9sc1snbmFtZSddLnNldFZhbHVlKHRoaXMub3B0aW9uRm9vZEl0ZW0ubmFtZSk7XG4gICAgICAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZU9wdGlvblZhbHVlSXRlbUZvcm0uY29udHJvbHNbJ25hbWUnXTtcbiAgICAgICAgICAgIHRoaXMub3B0aW9uRm9vZFZhbHVlSXRlbSA9IHRoaXMuU3RvcmFnZVNlcnZpY2UuZ2V0U2NvcGUoKTtcbiAgICAgICAgICAgIHRoaXMub3B0aW9uRm9vZEl0ZW1JZCA9IHRoaXMub3B0aW9uRm9vZFZhbHVlSXRlbS5vcHRpb25faWQ7XG4gICAgICAgICAgICB0aGlzLmZldGNoQWxsT3B0aW9uRm9vZEl0ZW1zKHRoaXMucmVzdElkKTtcbiAgICAgICAgfVxuICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgdGhpcy5vcHRpb25Gb29kSXRlbUlkID0gdGhpcy5TdG9yYWdlU2VydmljZS5nZXRTY29wZSgpO1xuICAgICAgICAgICAgIHRoaXMuZmV0Y2hBbGxPcHRpb25Gb29kSXRlbXModGhpcy5yZXN0SWQpO1xuICAgICAgfVxuICB9XG4gIHByaXZhdGUgZmV0Y2hBbGxPcHRpb25WYWx1ZUZvb2RJdGVtcyhyZXN0SWQsaWQpIHtcbiAgXHRcdHRoaXMuT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2UuZ2V0T3B0aW9uVmFsdWVGb29kSXRlbXMocmVzdElkLGlkKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlLm9wdGlvbnMpOyBcbiAgICAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2Uub3B0aW9ucyE9bnVsbCAmJiByZXNwb25zZS5vcHRpb25zLmxlbmd0aD4wKXtcbiAgICAgICAgICAgICAgICAgICAgICAgXHQgIHRoaXMub3B0aW9uVmFsdWVGb29kSXRlbSA9IHJlc3BvbnNlLm9wdGlvbnNbMF07XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS5jb250cm9sc1snbmFtZSddLnNldFZhbHVlKHRoaXMub3B0aW9uVmFsdWVGb29kSXRlbS5uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVPcHRpb25WYWx1ZUl0ZW1Gb3JtLmNvbnRyb2xzWydvcHRpb25OYW1lJ10uc2V0VmFsdWUodGhpcy5vcHRpb25WYWx1ZUZvb2RJdGVtLm9wdGlvbl9pZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9uTmFtZSA9IHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS5jb250cm9sc1snb3B0aW9uTmFtZSddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZU9wdGlvblZhbHVlSXRlbUZvcm0uY29udHJvbHNbJ25hbWUnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25Gb29kSXRlbUlkID0gdGhpcy5vcHRpb25WYWx1ZUZvb2RJdGVtLm9wdGlvbl9pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmLmVycm9yKCdGb29kIHZhbHVlIGl0ZW0gaGFkIGRlbGV0ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zL2xpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG4gIHByaXZhdGUgZmV0Y2hBbGxPcHRpb25Gb29kSXRlbXMocmVzdElkKSB7XG4gICAgdGhpcy5mbGdJdGVtU2VsZWN0ID0gMDtcbiAgXHRcdHRoaXMuT3B0aW9uRm9vZEl0ZW1TZXJ2aWNlLmdldE9wdGlvbkZvb2RJdGVtcyhyZXN0SWQsbnVsbCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLm9wdGlvbkZvb2RJdGVtcyA9IHJlc3BvbnNlLm9wdGlvbnM7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1zLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogYGNob29zZSB2YWx1ZWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMub3B0aW9uRm9vZEl0ZW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1zLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogdGhpcy5vcHRpb25Gb29kSXRlbXNbaV0uaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IGAke3RoaXMub3B0aW9uRm9vZEl0ZW1zW2ldLm5hbWV9YFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbkZvb2RJdGVtSWQhPXVuZGVmaW5lZCAmJiB0aGlzLm9wdGlvbkZvb2RJdGVtc1tpXS5pZD09dGhpcy5vcHRpb25Gb29kSXRlbUlkKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZsZ0l0ZW1TZWxlY3Q9IGkgKyAxO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS5jb250cm9sc1snb3B0aW9uTmFtZSddLnNldFZhbHVlKHRoaXMub3B0aW9uRm9vZEl0ZW1zW2ldLmlkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbk5hbWUgPSB0aGlzLmNyZWF0ZU9wdGlvblZhbHVlSXRlbUZvcm0uY29udHJvbHNbJ29wdGlvbk5hbWUnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0Lml0ZW1zID0gdGhpcy5pdGVtcztcbiAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5mbGdJdGVtU2VsZWN0ICE9IHVuZGVmaW5lZCAmJiB0aGlzLmZsZ0l0ZW1TZWxlY3QgIT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLml0ZW1zW3RoaXMuZmxnSXRlbVNlbGVjdF07XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWVDaG9vc2UgPSB0aGlzLnZhbHVlLmlkO1xuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5vcHRpb25Gb29kSXRlbUlkPT11bmRlZmluZWQgfHwgdGhpcy5vcHRpb25Gb29kSXRlbUlkPT1mYWxzZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5pdGVtc1swXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS5jb250cm9sc1snb3B0aW9uTmFtZSddLnNldFZhbHVlKG51bGwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25OYW1lID0gdGhpcy5jcmVhdGVPcHRpb25WYWx1ZUl0ZW1Gb3JtLmNvbnRyb2xzWydvcHRpb25OYW1lJ107XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgXHRjb25zb2xlLmxvZyhyZXNwb25zZS5vcHRpb25zKTsgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbnByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xuICAgIHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgIFx0J25hbWUnOiBbJycsIFtcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICAgIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NiksXG4gICAgICAgIF1cbiAgICAgIF0sXG4gICAgICAnb3B0aW9uTmFtZSc6IFsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgIF1cbiAgICAgIF1cbiAgICB9KTtcbiAgIFx0dGhpcy5uYW1lID0gdGhpcy5jcmVhdGVPcHRpb25WYWx1ZUl0ZW1Gb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgdGhpcy5vcHRpb25OYW1lID0gdGhpcy5jcmVhdGVPcHRpb25WYWx1ZUl0ZW1Gb3JtLmNvbnRyb2xzWydvcHRpb25OYW1lJ107XG4gICAgdGhpcy5jcmVhdGVPcHRpb25WYWx1ZUl0ZW1Gb3JtLnZhbHVlQ2hhbmdlc1xuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMub25WYWx1ZUNoYW5nZWQoZGF0YSkpO1xuXG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCgpOyAvLyAocmUpc2V0IHZhbGlkYXRpb24gbWVzc2FnZXMgbm93XG4gIH1cbiAgIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQoZGF0YT86IGFueSkge1xuICAgIGlmICghdGhpcy5jcmVhdGVPcHRpb25WYWx1ZUl0ZW1Gb3JtKSB7IHJldHVybjsgfVxuICAgIGNvbnN0IGZvcm0gPSB0aGlzLmNyZWF0ZU9wdGlvblZhbHVlSXRlbUZvcm07XG5cbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxuICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IFtdO1xuICAgICAgY29uc3QgY29udHJvbCA9IGZvcm0uZ2V0KGZpZWxkKTtcblxuICAgICAgaWYgKGNvbnRyb2wgJiYgY29udHJvbC5kaXJ0eSAmJiAhY29udHJvbC52YWxpZCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlcyA9IHRoaXMudmFsaWRhdGlvbk1lc3NhZ2VzW2ZpZWxkXTtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJvbC5lcnJvcnMpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdLnB1c2gobWVzc2FnZXNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbmZvcm1FcnJvcnMgPSB7XG4gICAgJ29wdGlvbk5hbWUnOiBbXSxcbiAgICAnbmFtZSc6IFtdXG4gIH07XG5cbiAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICBcdCdvcHRpb25OYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIG9wdGlvbk5hbWUgaXMgcmVxdWlyZWQuJ1xuICAgIH0sXG4gICAgJ25hbWUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgbmFtZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIG5hbWUgbXVzdCBiZSBhdCBsZXNzIDI1NiBjaGFyYWN0ZXJzIGxvbmcuJyxcbiAgICB9XG4gIH07XG5cbm9wdGlvblZhbHVlSXRlbUNyZWF0ZSgpe1xuICBsZXQgaWQgID0gJy0xJztcbiAgaWYodGhpcy5pZCE9bnVsbCl7XG4gICAgaWQgID0gdGhpcy5pZDtcbiAgfVxuICBsZXQgb3B0aW9uX2lkICA9IG51bGw7XG4gIGlmKHRoaXMudmFsdWVDaG9vc2UhPTAgJiYgdGhpcy52YWx1ZUNob29zZSE9bnVsbCl7XG4gICAgICBvcHRpb25faWQgPSB0aGlzLnZhbHVlLmlkO1xuICB9XG4gIHRoaXMub25WYWx1ZUNoYW5nZWQoKTtcbiAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIC8vIGNsZWFyIHByZXZpb3VzIGVycm9yIG1lc3NhZ2UgKGlmIGFueSlcbiAgICAgICBpZih0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+MCl7XG4gICAgICAgICByZXR1cm47XG4gICAgICAgfVxuICAgIH1cbiAgICAgIHRoaXMub3B0aW9uRm9vZFZhbHVlSXRlbSA9IHtcbiAgICAgICAgICAgICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICAgICAnb3B0aW9uJzp7XG4gICAgICAgICAgICAgICAgICAnaWQnOmlkLFxuICAgICAgICAgICAgICAgICAgJ25hbWUnOnRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS52YWx1ZS5uYW1lLFxuICAgICAgICAgICAgICAgICAgJ29wdGlvbl9pZCc6b3B0aW9uX2lkLFxuICAgICAgICAgICAgICAgICAgJ2lzX2RlbGV0ZSc6JzAnXG4gICAgICAgICAgICAgICAgfX07XG4gICAgICBcdHRoaXMuT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2UuY3JlYXRlT3B0aW9uVmFsdWVGb29kSXRlbXModGhpcy5vcHRpb25Gb29kVmFsdWVJdGVtKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiAgdGhpcy5mYWlsZWRDcmVhdGUuYmluZChlcnJvcikpO1xuICB9XG4gIHByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXNwb25zZSkge1xuXG4gICAgaWYgKHJlc3BvbnNlLm1lc3NhZ2UgPT0gdW5kZWZpbmVkIHx8IHJlc3BvbnNlLm1lc3NhZ2UgPT0gJ09LJykge1xuICAgICAgdGhpcy5ub3RpZi5zdWNjZXNzKCdOZXcgT3B0aW9uIGl0ZW0gaGFzIGJlZW4gYWRkZWQnKTtcbiAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3B0aW9uLXZhbHVlLWZvb2QtaXRlbXMvbGlzdCcpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmVycm9yID0gcmVzcG9uc2UuZXJyb3JzO1xuICAgICAgaWYgKHJlc3BvbnNlLmNvZGUgPT0gNDIyKSB7XG4gICAgICAgIGlmICh0aGlzLmVycm9yLm5hbWUpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ25hbWUnXSA9IHRoaXMuZXJyb3IubmFtZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5lcnJvci5vcHRpb25faWQpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ29wdGlvbk5hbWUnXSA9IHRoaXMuZXJyb3Iub3B0aW9uX2lkO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzd2FsKFxuICAgICAgICAgICdDcmVhdGUgRmFpbCEnLFxuICAgICAgICAgIHRoaXMuZXJyb3JbMF0sXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICB9XG5cbi8qc2VsZWN0IHN0YXJ0Ki9cblxucHJpdmF0ZSB2YWx1ZTphbnkgPSB7fTtcbiAgcHJpdmF0ZSBfZGlzYWJsZWRWOnN0cmluZyA9ICcwJztcbiAgcHJpdmF0ZSBkaXNhYmxlZDpib29sZWFuID0gZmFsc2U7XG4gXG4gIHByaXZhdGUgZ2V0IGRpc2FibGVkVigpOnN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX2Rpc2FibGVkVjtcbiAgfVxuIFxuICBwcml2YXRlIHNldCBkaXNhYmxlZFYodmFsdWU6c3RyaW5nKSB7XG4gICAgdGhpcy5fZGlzYWJsZWRWID0gdmFsdWU7XG4gICAgdGhpcy5kaXNhYmxlZCA9IHRoaXMuX2Rpc2FibGVkViA9PT0gJzEnO1xuICB9XG4gXG4gIHB1YmxpYyBzZWxlY3RlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIFxuICAgIGNvbnNvbGUubG9nKCdTZWxlY3RlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICAgIHRoaXMuY3JlYXRlT3B0aW9uVmFsdWVJdGVtRm9ybS5jb250cm9sc1snb3B0aW9uTmFtZSddLnNldFZhbHVlKHZhbHVlLmlkKTtcbiAgICB0aGlzLm9wdGlvbk5hbWUgPSB0aGlzLmNyZWF0ZU9wdGlvblZhbHVlSXRlbUZvcm0uY29udHJvbHNbJ29wdGlvbk5hbWUnXTtcbiAgfVxuIFxuICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdSZW1vdmVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XG4gIH1cbiBcbiAgcHVibGljIHR5cGVkKHZhbHVlOmFueSk6dm9pZCB7XG4gICAgY29uc29sZS5sb2coJ05ldyBzZWFyY2ggaW5wdXQ6ICcsIHZhbHVlKTtcbiAgfVxuIFxuICBwdWJsaWMgcmVmcmVzaFZhbHVlKHZhbHVlOmFueSk6dm9pZCB7XG4gICAgIGlmKHZhbHVlIT11bmRlZmluZWQgJiYgdmFsdWUhPW51bGwgJiYgdmFsdWUubGVuZ3RoIT0wKXtcbiAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICAgIHRoaXMudmFsdWVDaG9vc2UgPSB2YWx1ZS5pZDtcbiAgICAgfWVsc2V7XG4gICAgICAgdGhpcy52YWx1ZUNob29zZSA9IG51bGw7XG4gICAgIH1cbiAgfVxuXG4gIFxufVxuIl19
