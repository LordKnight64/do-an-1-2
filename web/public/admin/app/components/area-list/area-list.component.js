"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("app/router.animations");
var area_service_1 = require("app/services/area.service");
var auth_service_1 = require("app/services/auth.service");
var notification_service_1 = require("app/services/notification.service");
var sweetalert2_1 = require("sweetalert2");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var AreaListComponent = (function () {
    function AreaListComponent(areaService, authServ, notif, router, StorageService) {
        this.areaService = areaService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.StorageService = StorageService;
    }
    AreaListComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllAreas(this.restId);
    };
    AreaListComponent.prototype.fetchAllAreas = function (restId) {
        var _this = this;
        console.log('hihi');
        this.areaService.getAreas(restId, null).then(function (response) {
            _this.areas = response.areas;
            console.log(response.areas);
        }, function (error) {
            console.log(error);
        });
    };
    AreaListComponent.prototype.updateArea = function (item, isActive) {
        var _this = this;
        this.areaObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'area': {
                'id': item.id,
                'name': item.name,
                'thumb': item.thumb,
                'is_active': isActive,
                'is_delete': '0'
            }
        };
        this.areaService.createArea(this.areaObject).then(function (response) { return _this.processResult(response, null); }, function (error) { return _this.failedCreate.bind(error); });
    };
    AreaListComponent.prototype.deleteArea = function (item, isActive) {
        var _this = this;
        var action = 'update_areas_by_id';
        this.areaObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'area': {
                'id': item.id,
                'name': item.name,
                'thumb': item.thumb,
                'is_active': isActive,
                'is_delete': '1'
            }
        };
        this.areaService.createArea(this.areaObject).then(function (response) { return _this.processResult(response, item.thumb); }, function (error) { return _this.failedCreate.bind(error); });
    };
    AreaListComponent.prototype.processResult = function (response, thumb) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            if (thumb != null) {
                this.areaService.deleteFile({ 'thumb': thumb }).then(function (response) {
                    _this.notif.success('News has been deleted');
                }, function (error) {
                    console.log(error);
                });
            }
            this.notif.success('New area has been added');
            this.fetchAllAreas(this.restId);
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    AreaListComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    AreaListComponent.prototype.createArea = function () {
        //   this.userService.currentUser.subscribe((user) => {
        //    console.log('user', user);
        // });
        this.StorageService.setScope(null);
        this.router.navigateByUrl('/area/create');
    };
    AreaListComponent.prototype.editArea = function (item) {
        this.StorageService.setScope(item);
        this.router.navigateByUrl('/area/create/' + item.id);
    };
    AreaListComponent = __decorate([
        core_1.Component({
            selector: 'area-list',
            templateUrl: utils_1.default.getView('app/components/area-list/area-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/area-list/area-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [area_service_1.AreaService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            storage_service_1.StorageService])
    ], AreaListComponent);
    return AreaListComponent;
}());
exports.AreaListComponent = AreaListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2FyZWEtbGlzdC9hcmVhLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHlDQUFvQztBQUdwQywyREFBeUQ7QUFDekQsMERBQXdEO0FBQ3hELDBEQUF3RDtBQUN4RCwwRUFBd0U7QUFDeEUsMkNBQThDO0FBQzlDLDBDQUF5QztBQUN6QyxnRUFBOEQ7QUFTOUQ7SUFPRSwyQkFBb0IsV0FBd0IsRUFDcEMsUUFBcUIsRUFDckIsS0FBMEIsRUFDMUIsTUFBYyxFQUNkLGNBQThCO1FBSmxCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3BDLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsVUFBSyxHQUFMLEtBQUssQ0FBcUI7UUFDMUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUFLLENBQUM7SUFFNUMsb0NBQVEsR0FBUjtRQUVFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBRTVELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUVqQyxDQUFDO0lBRVEseUNBQWEsR0FBckIsVUFBc0IsTUFBTTtRQUE1QixpQkFTQztRQVJFLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDMUIsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVKLHNDQUFVLEdBQVYsVUFBVyxJQUFJLEVBQUMsUUFBUTtRQUF4QixpQkFjQztRQWJDLElBQUksQ0FBQyxVQUFVLEdBQUc7WUFDUixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3JCLE1BQU0sRUFBQztnQkFDTCxJQUFJLEVBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ1osTUFBTSxFQUFDLElBQUksQ0FBQyxJQUFJO2dCQUNoQixPQUFPLEVBQUMsSUFBSSxDQUFDLEtBQUs7Z0JBQ2xCLFdBQVcsRUFBQyxRQUFRO2dCQUNwQixXQUFXLEVBQUMsR0FBRzthQUNoQjtTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUNwQyxVQUFBLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFDLElBQUksQ0FBQyxFQUFqQyxDQUFpQyxFQUN6QyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHNDQUFVLEdBQVYsVUFBVyxJQUFJLEVBQUMsUUFBUTtRQUF4QixpQkFlQztRQWRDLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUc7WUFDUixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3JCLE1BQU0sRUFBQztnQkFDTCxJQUFJLEVBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ1osTUFBTSxFQUFDLElBQUksQ0FBQyxJQUFJO2dCQUNoQixPQUFPLEVBQUMsSUFBSSxDQUFDLEtBQUs7Z0JBQ2xCLFdBQVcsRUFBQyxRQUFRO2dCQUNwQixXQUFXLEVBQUMsR0FBRzthQUNoQjtTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUNwQyxVQUFBLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBdkMsQ0FBdUMsRUFDL0MsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFDTyx5Q0FBYSxHQUFyQixVQUFzQixRQUFRLEVBQUMsS0FBSztRQUFwQyxpQkFrQkc7UUFoQkMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlELEVBQUUsQ0FBQSxDQUFDLEtBQUssSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUNkLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtvQkFDM0QsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQztnQkFDOUMsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixDQUFDLENBQUMsQ0FBQztZQUNMLENBQUM7WUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLHFCQUFJLENBQ0EsY0FBYyxFQUNkLE9BQU8sQ0FDUixDQUFDLEtBQUssQ0FBQyxxQkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7SUFDSCxDQUFDO0lBQ08sd0NBQVksR0FBcEIsVUFBc0IsR0FBRztRQUN2QixxQkFBSSxDQUNFLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBQ0Qsc0NBQVUsR0FBVjtRQUNFLHVEQUF1RDtRQUN2RCxnQ0FBZ0M7UUFFaEMsTUFBTTtRQUNKLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFDRCxvQ0FBUSxHQUFSLFVBQVMsSUFBSTtRQUNULElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsR0FBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQXRHVSxpQkFBaUI7UUFQNUIsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLG1EQUFtRCxDQUFDO1lBQy9FLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsa0RBQWtELENBQUMsQ0FBQztZQUM5RSxVQUFVLEVBQUUsQ0FBQyxvQ0FBZ0IsRUFBRSxDQUFDO1lBQ2hDLElBQUksRUFBRSxFQUFDLHFCQUFxQixFQUFFLEVBQUUsRUFBQztTQUNsQyxDQUFDO3lDQVFpQywwQkFBVztZQUMxQiwwQkFBVztZQUNkLDBDQUFtQjtZQUNsQixlQUFNO1lBQ0UsZ0NBQWM7T0FYM0IsaUJBQWlCLENBdUc3QjtJQUFELHdCQUFDO0NBdkdELEFBdUdDLElBQUE7QUF2R1ksOENBQWlCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL2FyZWEtbGlzdC9hcmVhLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUgfSBmcm9tICdhcHAvcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJ2FwcC9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBBcmVhU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9hcmVhLnNlcnZpY2UnO1xuaW1wb3J0ICB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2F1dGguc2VydmljZSdcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgZGVmYXVsdCBhcyBzd2FsIH0gZnJvbSAnc3dlZXRhbGVydDInO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5cbiBAQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcmVhLWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvYXJlYS1saXN0L2FyZWEtbGlzdC5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9hcmVhLWxpc3QvYXJlYS1saXN0LmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ31cbn0pXG5leHBvcnQgY2xhc3MgQXJlYUxpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuIHByaXZhdGUgYXJlYXMgOiBhbnk7XG4gIHByaXZhdGUgdG9rZW4gOiBhbnk7XG4gIHByaXZhdGUgdXNlcklkIDogU3RyaW5nO1xuICBwcml2YXRlIHJlc3RJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSBhcmVhT2JqZWN0IDogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYXJlYVNlcnZpY2U6IEFyZWFTZXJ2aWNlLFxuICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcbiAgcHJpdmF0ZSBub3RpZjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgcHJpdmF0ZSBTdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UsKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcblxuICAgIGxldCB1c2VySW5mbyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuXG4gICAgdGhpcy5yZXN0SWQgPSB1c2VySW5mby5yZXN0YXVyYW50c1swXS5pZDtcbiAgICB0aGlzLnVzZXJJZCA9IHVzZXJJbmZvLnVzZXJfaW5mby5pZDtcbiAgXHR0aGlzLmZldGNoQWxsQXJlYXModGhpcy5yZXN0SWQpO1xuXG4gIH1cblxuICBcdHByaXZhdGUgZmV0Y2hBbGxBcmVhcyhyZXN0SWQpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdoaWhpJyk7XG4gIFx0XHR0aGlzLmFyZWFTZXJ2aWNlLmdldEFyZWFzKHJlc3RJZCxudWxsKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHtcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5hcmVhcyA9IHJlc3BvbnNlLmFyZWFzO1xuICAgICAgICAgICAgICAgICAgICAgXHRjb25zb2xlLmxvZyhyZXNwb25zZS5hcmVhcyk7XG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cblxudXBkYXRlQXJlYShpdGVtLGlzQWN0aXZlKXtcbiAgdGhpcy5hcmVhT2JqZWN0ID0ge1xuICAgICAgICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICdhcmVhJzp7XG4gICAgICAgICAgICAgICdpZCc6aXRlbS5pZCxcbiAgICAgICAgICAgICAgJ25hbWUnOml0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgJ3RodW1iJzppdGVtLnRodW1iLFxuICAgICAgICAgICAgICAnaXNfYWN0aXZlJzppc0FjdGl2ZSxcbiAgICAgICAgICAgICAgJ2lzX2RlbGV0ZSc6JzAnXG4gICAgICAgICAgICB9fTtcbiAgXHR0aGlzLmFyZWFTZXJ2aWNlLmNyZWF0ZUFyZWEodGhpcy5hcmVhT2JqZWN0KS50aGVuKFxuICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UsbnVsbCksXG4gICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiAgdGhpcy5mYWlsZWRDcmVhdGUuYmluZChlcnJvcikpO1xufVxuXG5kZWxldGVBcmVhKGl0ZW0saXNBY3RpdmUpe1xuICB2YXIgYWN0aW9uID0gJ3VwZGF0ZV9hcmVhc19ieV9pZCc7XG4gIHRoaXMuYXJlYU9iamVjdCA9IHtcbiAgICAgICAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAgICAgICAndXNlcl9pZCc6dGhpcy51c2VySWQsXG4gICAgICAgICAgICAnYXJlYSc6e1xuICAgICAgICAgICAgICAnaWQnOml0ZW0uaWQsXG4gICAgICAgICAgICAgICduYW1lJzppdGVtLm5hbWUsXG4gICAgICAgICAgICAgICd0aHVtYic6aXRlbS50aHVtYixcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6aXNBY3RpdmUsXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOicxJ1xuICAgICAgICAgICAgfX07XG4gIFx0dGhpcy5hcmVhU2VydmljZS5jcmVhdGVBcmVhKHRoaXMuYXJlYU9iamVjdCkudGhlbihcbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlLGl0ZW0udGh1bWIpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcbn1cbnByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXNwb25zZSx0aHVtYikge1xuXG4gICAgaWYgKHJlc3BvbnNlLm1lc3NhZ2UgPT0gdW5kZWZpbmVkIHx8IHJlc3BvbnNlLm1lc3NhZ2UgPT0gJ09LJykge1xuICAgICAgaWYodGh1bWIhPW51bGwpe1xuICAgICAgICB0aGlzLmFyZWFTZXJ2aWNlLmRlbGV0ZUZpbGUoeyAndGh1bWInOiB0aHVtYiB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICB0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ld3MgaGFzIGJlZW4gZGVsZXRlZCcpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHRoaXMubm90aWYuc3VjY2VzcygnTmV3IGFyZWEgaGFzIGJlZW4gYWRkZWQnKTtcbiAgICAgIHRoaXMuZmV0Y2hBbGxBcmVhcyh0aGlzLnJlc3RJZCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN3YWwoXG4gICAgICAgICAgJ1VwZGF0ZSBGYWlsIScsXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICB9XG4gIGNyZWF0ZUFyZWEoKXtcbiAgICAvLyAgIHRoaXMudXNlclNlcnZpY2UuY3VycmVudFVzZXIuc3Vic2NyaWJlKCh1c2VyKSA9PiB7XG4gICAgLy8gICAgY29uc29sZS5sb2coJ3VzZXInLCB1c2VyKTtcblxuICAgIC8vIH0pO1xuICAgICAgdGhpcy5TdG9yYWdlU2VydmljZS5zZXRTY29wZShudWxsKTtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9hcmVhL2NyZWF0ZScpO1xuICB9XG4gIGVkaXRBcmVhKGl0ZW0pe1xuICAgICAgdGhpcy5TdG9yYWdlU2VydmljZS5zZXRTY29wZShpdGVtKTtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9hcmVhL2NyZWF0ZS8nKyBpdGVtLmlkKTtcbiAgfVxufVxuIl19
