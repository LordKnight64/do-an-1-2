"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var breadcrumb_service_1 = require("app/services/breadcrumb.service");
var messages_service_1 = require("app/services/messages.service");
var auth_service_1 = require("app/services/auth.service");
var user_1 = require("app/models/user");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var moment = require("moment");
var HomeComponent = (function () {
    function HomeComponent(msgServ, breadServ, authServ) {
        this.msgServ = msgServ;
        this.breadServ = breadServ;
        this.authServ = authServ;
        // public date: Date = new Date();
        this.dt = new Date();
        this.minDate = void 0;
        this.formats = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY',
            'shortDate'];
        this.format = this.formats[0];
        this.dateOptions = {
            formatYear: 'YY',
            startingDay: 1
        };
        this.opened = false;
        // TODO
        (this.minDate = new Date()).setDate(this.minDate.getDate() - 1000);
        (this.tomorrow = new Date()).setDate(this.tomorrow.getDate() + 1);
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // setttings the header for the home
        this.authServ.getToken().then(function (response) { return _this.processResult(response); }, function (error) { return _this.handleError(error); });
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        // removing the header
        this.breadServ.clear();
    };
    HomeComponent.prototype.processResult = function (res) {
        this.breadServ.set({
            description: 'HomePage',
            display: true,
            header: 'Home',
            levels: [
                {
                    icon: 'dashboard',
                    link: ['/'],
                    title: 'Home',
                    state: 'app'
                }
            ]
        });
        // defining some test users
        var user1 = new user_1.User({
            avatarUrl: 'public/assets/img/user2-160x160.jpg',
            email: 'weber.antoine.pro@gmail.com',
            firstname: 'WEBER',
            lastname: 'Antoine'
        });
        var user2 = new user_1.User({
            avatarUrl: 'public/assets/img/user2-160x160.jpg',
            email: 'EMAIL',
            firstname: 'FIRSTNAME',
            lastname: 'LASTNAME'
        });
        // sending a test message
        // this.msgServ.addMessage( new Message( {
        //     author: user2,
        //     content: 'le contenu d\'un message d\'une importance extreme',
        //     destination: user1,
        //     title: 'un message super important'
        // }) );
    };
    HomeComponent.prototype.handleError = function (error) {
        console.log(error);
    };
    HomeComponent.prototype.today = function () {
        this.dt = new Date();
    };
    HomeComponent.prototype.d20090824 = function () {
        this.dt = moment('2009-08-24', 'YYYY-MM-DD')
            .toDate();
    };
    HomeComponent.prototype.clear = function () {
        this.dt = void 0;
        this.dateDisabled = undefined;
    };
    HomeComponent.prototype.toggleMin = function () {
        this.dt = new Date(this.minDate.valueOf());
    };
    HomeComponent.prototype.disableTomorrow = function () {
        this.dateDisabled = [{ date: this.tomorrow, mode: 'day' }];
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'app-home',
            styleUrls: [utils_1.default.getView('app/components/home/home.component.css')],
            templateUrl: utils_1.default.getView('app/components/home/home.component.html'),
            animations: [router_animations_1.routerFade()],
            host: { '[@routerFade]': '' },
        }),
        __metadata("design:paramtypes", [messages_service_1.MessagesService,
            breadcrumb_service_1.BreadcrumbService,
            auth_service_1.AuthService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBNkQ7QUFDN0Qsc0VBQW9FO0FBRXBFLGtFQUFnRTtBQUNoRSwwREFBd0Q7QUFDeEQsd0NBQXVDO0FBQ3ZDLHlDQUFvQztBQUNwQyw2REFBcUQ7QUFDckQsK0JBQWtDO0FBU2xDO0lBaUJFLHVCQUNVLE9BQXdCLEVBQ3hCLFNBQTRCLEVBQzVCLFFBQXFCO1FBRnJCLFlBQU8sR0FBUCxPQUFPLENBQWlCO1FBQ3hCLGNBQVMsR0FBVCxTQUFTLENBQW1CO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFuQi9CLGtDQUFrQztRQUMzQixPQUFFLEdBQVMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN0QixZQUFPLEdBQVMsS0FBSyxDQUFDLENBQUM7UUFLdkIsWUFBTyxHQUFhLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxZQUFZO1lBQ2xFLFdBQVcsQ0FBQyxDQUFDO1FBQ1IsV0FBTSxHQUFXLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakMsZ0JBQVcsR0FBUTtZQUN4QixVQUFVLEVBQUUsSUFBSTtZQUNoQixXQUFXLEVBQUUsQ0FBQztTQUNmLENBQUM7UUFDTSxXQUFNLEdBQVksS0FBSyxDQUFDO1FBTzlCLE9BQU87UUFDUCxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ25FLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVNLGdDQUFRLEdBQWY7UUFBQSxpQkFNQztRQUxDLG9DQUFvQztRQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FDMUIsVUFBQSxRQUFRLElBQUksT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixFQUN4QyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCLENBQ2xDLENBQUM7SUFDSixDQUFDO0lBRU0sbUNBQVcsR0FBbEI7UUFDRSxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRU8scUNBQWEsR0FBckIsVUFBc0IsR0FBRztRQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztZQUNqQixXQUFXLEVBQUUsVUFBVTtZQUN2QixPQUFPLEVBQUUsSUFBSTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsTUFBTSxFQUFFO2dCQUNOO29CQUNFLElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUM7b0JBQ1gsS0FBSyxFQUFFLE1BQU07b0JBQ2IsS0FBSyxFQUFFLEtBQUs7aUJBQ2I7YUFDRjtTQUNGLENBQUMsQ0FBQztRQUVILDJCQUEyQjtRQUMzQixJQUFJLEtBQUssR0FBRyxJQUFJLFdBQUksQ0FBRTtZQUNsQixTQUFTLEVBQUUscUNBQXFDO1lBQ2hELEtBQUssRUFBRSw2QkFBNkI7WUFDcEMsU0FBUyxFQUFFLE9BQU87WUFDbEIsUUFBUSxFQUFFLFNBQVM7U0FDdEIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxLQUFLLEdBQUcsSUFBSSxXQUFJLENBQUU7WUFDbEIsU0FBUyxFQUFFLHFDQUFxQztZQUNoRCxLQUFLLEVBQUUsT0FBTztZQUNkLFNBQVMsRUFBRSxXQUFXO1lBQ3RCLFFBQVEsRUFBRSxVQUFVO1NBQ3ZCLENBQUMsQ0FBQztRQUNILHlCQUF5QjtRQUN6QiwwQ0FBMEM7UUFDMUMscUJBQXFCO1FBQ3JCLHFFQUFxRTtRQUNyRSwwQkFBMEI7UUFDMUIsMENBQTBDO1FBQzFDLFFBQVE7SUFDVixDQUFDO0lBRU8sbUNBQVcsR0FBbkIsVUFBb0IsS0FBSztRQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFTSw2QkFBSyxHQUFaO1FBQ0UsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFTSxpQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLFlBQVksRUFBRSxZQUFZLENBQUM7YUFDekMsTUFBTSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRU0sNkJBQUssR0FBWjtRQUNFLElBQUksQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7SUFDaEMsQ0FBQztJQUVNLGlDQUFTLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVNLHVDQUFlLEdBQXRCO1FBQ0UsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQXJHVSxhQUFhO1FBUHpCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsVUFBVTtZQUNwQixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7WUFDcEUsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMseUNBQXlDLENBQUM7WUFDckUsVUFBVSxFQUFFLENBQUMsOEJBQVUsRUFBRSxDQUFDO1lBQzFCLElBQUksRUFBRSxFQUFDLGVBQWUsRUFBRSxFQUFFLEVBQUM7U0FDNUIsQ0FBQzt5Q0FtQm1CLGtDQUFlO1lBQ2Isc0NBQWlCO1lBQ2xCLDBCQUFXO09BcEJwQixhQUFhLENBdUd6QjtJQUFELG9CQUFDO0NBdkdELEFBdUdDLElBQUE7QUF2R1ksc0NBQWEiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWVzc2FnZSB9IGZyb20gJ2FwcC9tb2RlbHMvbWVzc2FnZSc7XHJcbmltcG9ydCB7IE1lc3NhZ2VzU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9tZXNzYWdlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlciB9IGZyb20gJ2FwcC9tb2RlbHMvdXNlcic7XHJcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xyXG5pbXBvcnQgeyByb3V0ZXJGYWRlIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xyXG5pbXBvcnQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1ob21lJyxcclxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcycpXSxcclxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5odG1sJyksXHJcbiAgYW5pbWF0aW9uczogW3JvdXRlckZhZGUoKV0sXHJcbiAgaG9zdDogeydbQHJvdXRlckZhZGVdJzogJyd9LFxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAvLyBwdWJsaWMgZGF0ZTogRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgcHVibGljIGR0OiBEYXRlID0gbmV3IERhdGUoKTtcclxuICBwdWJsaWMgbWluRGF0ZTogRGF0ZSA9IHZvaWQgMDtcclxuICBwdWJsaWMgZXZlbnRzOiBhbnlbXTtcclxuICBwdWJsaWMgdG9tb3Jyb3c6IERhdGU7XHJcbiAgcHVibGljIGFmdGVyVG9tb3Jyb3c6IERhdGU7XHJcbiAgcHVibGljIGRhdGVEaXNhYmxlZDoge2RhdGU6IERhdGUsIG1vZGU6IHN0cmluZ31bXTtcclxuICBwdWJsaWMgZm9ybWF0czogc3RyaW5nW10gPSBbJ0RELU1NLVlZWVknLCAnWVlZWS9NTS9ERCcsICdERC5NTS5ZWVlZJyxcclxuICAgICdzaG9ydERhdGUnXTtcclxuICBwdWJsaWMgZm9ybWF0OiBzdHJpbmcgPSB0aGlzLmZvcm1hdHNbMF07XHJcbiAgcHVibGljIGRhdGVPcHRpb25zOiBhbnkgPSB7XHJcbiAgICBmb3JtYXRZZWFyOiAnWVknLFxyXG4gICAgc3RhcnRpbmdEYXk6IDFcclxuICB9O1xyXG4gIHByaXZhdGUgb3BlbmVkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIG1zZ1NlcnY6IE1lc3NhZ2VzU2VydmljZSxcclxuICAgIHByaXZhdGUgYnJlYWRTZXJ2OiBCcmVhZGNydW1iU2VydmljZSxcclxuICAgIHByaXZhdGUgYXV0aFNlcnY6IEF1dGhTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICAvLyBUT0RPXHJcbiAgICAodGhpcy5taW5EYXRlID0gbmV3IERhdGUoKSkuc2V0RGF0ZSh0aGlzLm1pbkRhdGUuZ2V0RGF0ZSgpIC0gMTAwMCk7XHJcbiAgICAodGhpcy50b21vcnJvdyA9IG5ldyBEYXRlKCkpLnNldERhdGUodGhpcy50b21vcnJvdy5nZXREYXRlKCkgKyAxKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBuZ09uSW5pdCgpIHtcclxuICAgIC8vIHNldHR0aW5ncyB0aGUgaGVhZGVyIGZvciB0aGUgaG9tZVxyXG4gICAgdGhpcy5hdXRoU2Vydi5nZXRUb2tlbigpLnRoZW4oXHJcbiAgICAgICByZXNwb25zZSA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UpLFxyXG4gICAgICAgZXJyb3IgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcilcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAvLyByZW1vdmluZyB0aGUgaGVhZGVyXHJcbiAgICB0aGlzLmJyZWFkU2Vydi5jbGVhcigpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlcyl7XHJcbiAgICB0aGlzLmJyZWFkU2Vydi5zZXQoe1xyXG4gICAgICBkZXNjcmlwdGlvbjogJ0hvbWVQYWdlJyxcclxuICAgICAgZGlzcGxheTogdHJ1ZSxcclxuICAgICAgaGVhZGVyOiAnSG9tZScsXHJcbiAgICAgIGxldmVsczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIGljb246ICdkYXNoYm9hcmQnLFxyXG4gICAgICAgICAgbGluazogWycvJ10sXHJcbiAgICAgICAgICB0aXRsZTogJ0hvbWUnLFxyXG4gICAgICAgICAgc3RhdGU6ICdhcHAnXHJcbiAgICAgICAgfVxyXG4gICAgICBdXHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBkZWZpbmluZyBzb21lIHRlc3QgdXNlcnNcclxuICAgIGxldCB1c2VyMSA9IG5ldyBVc2VyKCB7XHJcbiAgICAgICAgYXZhdGFyVXJsOiAncHVibGljL2Fzc2V0cy9pbWcvdXNlcjItMTYweDE2MC5qcGcnLFxyXG4gICAgICAgIGVtYWlsOiAnd2ViZXIuYW50b2luZS5wcm9AZ21haWwuY29tJyxcclxuICAgICAgICBmaXJzdG5hbWU6ICdXRUJFUicsXHJcbiAgICAgICAgbGFzdG5hbWU6ICdBbnRvaW5lJ1xyXG4gICAgfSk7XHJcbiAgICBsZXQgdXNlcjIgPSBuZXcgVXNlcigge1xyXG4gICAgICAgIGF2YXRhclVybDogJ3B1YmxpYy9hc3NldHMvaW1nL3VzZXIyLTE2MHgxNjAuanBnJyxcclxuICAgICAgICBlbWFpbDogJ0VNQUlMJyxcclxuICAgICAgICBmaXJzdG5hbWU6ICdGSVJTVE5BTUUnLFxyXG4gICAgICAgIGxhc3RuYW1lOiAnTEFTVE5BTUUnXHJcbiAgICB9KTtcclxuICAgIC8vIHNlbmRpbmcgYSB0ZXN0IG1lc3NhZ2VcclxuICAgIC8vIHRoaXMubXNnU2Vydi5hZGRNZXNzYWdlKCBuZXcgTWVzc2FnZSgge1xyXG4gICAgLy8gICAgIGF1dGhvcjogdXNlcjIsXHJcbiAgICAvLyAgICAgY29udGVudDogJ2xlIGNvbnRlbnUgZFxcJ3VuIG1lc3NhZ2UgZFxcJ3VuZSBpbXBvcnRhbmNlIGV4dHJlbWUnLFxyXG4gICAgLy8gICAgIGRlc3RpbmF0aW9uOiB1c2VyMSxcclxuICAgIC8vICAgICB0aXRsZTogJ3VuIG1lc3NhZ2Ugc3VwZXIgaW1wb3J0YW50J1xyXG4gICAgLy8gfSkgKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3Ipe1xyXG4gICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHRvZGF5KCk6IHZvaWQge1xyXG4gICAgdGhpcy5kdCA9IG5ldyBEYXRlKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZDIwMDkwODI0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5kdCA9IG1vbWVudCgnMjAwOS0wOC0yNCcsICdZWVlZLU1NLUREJylcclxuICAgICAgLnRvRGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsZWFyKCk6IHZvaWQge1xyXG4gICAgdGhpcy5kdCA9IHZvaWQgMDtcclxuICAgIHRoaXMuZGF0ZURpc2FibGVkID0gdW5kZWZpbmVkO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHRvZ2dsZU1pbigpOiB2b2lkIHtcclxuICAgIHRoaXMuZHQgPSBuZXcgRGF0ZSh0aGlzLm1pbkRhdGUudmFsdWVPZigpKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBkaXNhYmxlVG9tb3Jyb3coKTogdm9pZCB7XHJcbiAgICB0aGlzLmRhdGVEaXNhYmxlZCA9IFt7ZGF0ZTogdGhpcy50b21vcnJvdywgbW9kZTogJ2RheSd9XTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==
