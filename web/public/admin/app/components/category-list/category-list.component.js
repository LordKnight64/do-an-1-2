"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("app/router.animations");
var category_service_1 = require("app/services/category.service");
var auth_service_1 = require("app/services/auth.service");
var notification_service_1 = require("app/services/notification.service");
var sweetalert2_1 = require("sweetalert2");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var CategoryListComponent = (function () {
    function CategoryListComponent(categoryService, authServ, notif, router, StorageService) {
        this.categoryService = categoryService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.StorageService = StorageService;
    }
    CategoryListComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllCategories(this.restId);
    };
    CategoryListComponent.prototype.fetchAllCategories = function (restId) {
        var _this = this;
        this.categoryService.getCategories(restId, null).then(function (response) {
            _this.categories = response.categories;
            console.log(response.categories);
        }, function (error) {
            console.log(error);
        });
    };
    CategoryListComponent.prototype.updateCategory = function (item, isActive) {
        var _this = this;
        var action = 'update_categories_by_id';
        this.categoryObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'category': {
                'id': item.id,
                'name': item.name,
                'thumb': item.thumb,
                'description': item.description,
                'is_active': isActive,
                'is_delete': '0'
            }
        };
        this.categoryService.createCategory(this.categoryObject).then(function (response) { return _this.processResult(response, null); }, function (error) { return _this.failedCreate.bind(error); });
    };
    CategoryListComponent.prototype.deleteCategory = function (item, isActive) {
        var _this = this;
        var action = 'update_categories_by_id';
        this.categoryObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'category': {
                'id': item.id,
                'name': item.name,
                'thumb': item.thumb,
                'description': item.description,
                'is_active': isActive,
                'is_delete': '1'
            }
        };
        this.categoryService.createCategory(this.categoryObject).then(function (response) { return _this.processResult(response, item.thumb); }, function (error) { return _this.failedCreate.bind(error); });
    };
    CategoryListComponent.prototype.processResult = function (response, thumb) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            if (thumb != null) {
                this.categoryService.deleteFile({ 'thumb': thumb }).then(function (response) {
                    _this.notif.success('News has been deleted');
                }, function (error) {
                    console.log(error);
                });
            }
            this.notif.success('New Category has been added');
            this.fetchAllCategories(this.restId);
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    CategoryListComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    CategoryListComponent.prototype.createCategory = function () {
        //   this.userService.currentUser.subscribe((user) => {
        //    console.log('user', user);
        // });
        this.StorageService.setScope(null);
        this.router.navigateByUrl('/category/create');
    };
    CategoryListComponent.prototype.editCategory = function (item) {
        this.StorageService.setScope(item);
        this.router.navigateByUrl('/category/create/' + item.id);
    };
    CategoryListComponent = __decorate([
        core_1.Component({
            selector: 'category-list',
            templateUrl: utils_1.default.getView('app/components/category-list/category-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/category-list/category-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [category_service_1.CategoryService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            storage_service_1.StorageService])
    ], CategoryListComponent);
    return CategoryListComponent;
}());
exports.CategoryListComponent = CategoryListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NhdGVnb3J5LWxpc3QvY2F0ZWdvcnktbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQseUNBQW9DO0FBR3BDLDJEQUF5RDtBQUN6RCxrRUFBZ0U7QUFDaEUsMERBQXdEO0FBQ3hELDBFQUF3RTtBQUN4RSwyQ0FBOEM7QUFDOUMsMENBQXlDO0FBQ3pDLGdFQUE4RDtBQVM5RDtJQU9FLCtCQUFvQixlQUFnQyxFQUM1QyxRQUFxQixFQUNyQixLQUEwQixFQUMxQixNQUFjLEVBQ2QsY0FBOEI7UUFKbEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQzVDLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsVUFBSyxHQUFMLEtBQUssQ0FBcUI7UUFDMUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUFLLENBQUM7SUFFNUMsd0NBQVEsR0FBUjtRQUVFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBRTVELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRXRDLENBQUM7SUFFUSxrREFBa0IsR0FBMUIsVUFBMkIsTUFBTTtRQUFqQyxpQkFRQztRQVBBLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQ25DLFVBQUEsUUFBUTtZQUNQLEtBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQztZQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQyxDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSiw4Q0FBYyxHQUFkLFVBQWUsSUFBSSxFQUFDLFFBQVE7UUFBNUIsaUJBZ0JDO1FBZkMsSUFBSSxNQUFNLEdBQUcseUJBQXlCLENBQUM7UUFDdkMsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNaLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUMsSUFBSSxDQUFDLE1BQU07WUFDckIsVUFBVSxFQUFDO2dCQUNULElBQUksRUFBQyxJQUFJLENBQUMsRUFBRTtnQkFDWixNQUFNLEVBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ2hCLE9BQU8sRUFBQyxJQUFJLENBQUMsS0FBSztnQkFDbEIsYUFBYSxFQUFDLElBQUksQ0FBQyxXQUFXO2dCQUM5QixXQUFXLEVBQUMsUUFBUTtnQkFDcEIsV0FBVyxFQUFDLEdBQUc7YUFDaEI7U0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FDaEQsVUFBQSxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBQyxJQUFJLENBQUMsRUFBakMsQ0FBaUMsRUFDekMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFDRCw4Q0FBYyxHQUFkLFVBQWUsSUFBSSxFQUFDLFFBQVE7UUFBNUIsaUJBZ0JDO1FBZkMsSUFBSSxNQUFNLEdBQUcseUJBQXlCLENBQUM7UUFDdkMsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNaLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUMsSUFBSSxDQUFDLE1BQU07WUFDckIsVUFBVSxFQUFDO2dCQUNULElBQUksRUFBQyxJQUFJLENBQUMsRUFBRTtnQkFDWixNQUFNLEVBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ2hCLE9BQU8sRUFBQyxJQUFJLENBQUMsS0FBSztnQkFDbEIsYUFBYSxFQUFDLElBQUksQ0FBQyxXQUFXO2dCQUM5QixXQUFXLEVBQUMsUUFBUTtnQkFDcEIsV0FBVyxFQUFDLEdBQUc7YUFDaEI7U0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FDaEQsVUFBQSxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQXZDLENBQXVDLEVBQy9DLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBQ08sNkNBQWEsR0FBckIsVUFBc0IsUUFBUSxFQUFDLEtBQUs7UUFBcEMsaUJBa0JHO1FBaEJDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksU0FBUyxJQUFJLFFBQVEsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUM5RCxFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7b0JBQy9ELEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQzlDLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLHFCQUFJLENBQ0EsY0FBYyxFQUNkLE9BQU8sQ0FDUixDQUFDLEtBQUssQ0FBQyxxQkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7SUFDSCxDQUFDO0lBQ08sNENBQVksR0FBcEIsVUFBc0IsR0FBRztRQUN2QixxQkFBSSxDQUNFLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBQ0QsOENBQWMsR0FBZDtRQUNFLHVEQUF1RDtRQUN2RCxnQ0FBZ0M7UUFFaEMsTUFBTTtRQUNKLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUNELDRDQUFZLEdBQVosVUFBYSxJQUFJO1FBQ2IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEdBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUF2R1UscUJBQXFCO1FBUmpDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQywyREFBMkQsQ0FBQztZQUN2RixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDBEQUEwRCxDQUFDLENBQUM7WUFDdEYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0FTcUMsa0NBQWU7WUFDbEMsMEJBQVc7WUFDZCwwQ0FBbUI7WUFDbEIsZUFBTTtZQUNFLGdDQUFjO09BWDNCLHFCQUFxQixDQXdHakM7SUFBRCw0QkFBQztDQXhHRCxBQXdHQyxJQUFBO0FBeEdZLHNEQUFxQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9jYXRlZ29yeS1saXN0L2NhdGVnb3J5LWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUgfSBmcm9tICdhcHAvcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJ2FwcC9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJ1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBkZWZhdWx0IGFzIHN3YWwgfSBmcm9tICdzd2VldGFsZXJ0Mic7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlJztcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NhdGVnb3J5LWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvY2F0ZWdvcnktbGlzdC9jYXRlZ29yeS1saXN0LmNvbXBvbmVudC5odG1sJyksXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2NhdGVnb3J5LWxpc3QvY2F0ZWdvcnktbGlzdC5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJyd9XG59KVxuXG5leHBvcnQgY2xhc3MgQ2F0ZWdvcnlMaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgcHJpdmF0ZSBjYXRlZ29yaWVzIDogYW55O1xuICBwcml2YXRlIHRva2VuIDogYW55O1xuICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgY2F0ZWdvcnlPYmplY3QgOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXRlZ29yeVNlcnZpY2U6IENhdGVnb3J5U2VydmljZSxcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG4gIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLCkgeyB9XG4gXG4gIG5nT25Jbml0KCkge1xuICAgXG4gICAgbGV0IHVzZXJJbmZvID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgXG4gICAgdGhpcy5yZXN0SWQgPSB1c2VySW5mby5yZXN0YXVyYW50c1swXS5pZDtcbiAgICB0aGlzLnVzZXJJZCA9IHVzZXJJbmZvLnVzZXJfaW5mby5pZDtcbiAgXHR0aGlzLmZldGNoQWxsQ2F0ZWdvcmllcyh0aGlzLnJlc3RJZCk7XG4gIFxuICB9XG5cbiAgXHRwcml2YXRlIGZldGNoQWxsQ2F0ZWdvcmllcyhyZXN0SWQpIHtcbiAgXHRcdHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldENhdGVnb3JpZXMocmVzdElkLG51bGwpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5jYXRlZ29yaWVzID0gcmVzcG9uc2UuY2F0ZWdvcmllcztcbiAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2UuY2F0ZWdvcmllcyk7IFxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG5cbnVwZGF0ZUNhdGVnb3J5KGl0ZW0saXNBY3RpdmUpe1xuICB2YXIgYWN0aW9uID0gJ3VwZGF0ZV9jYXRlZ29yaWVzX2J5X2lkJztcbiAgdGhpcy5jYXRlZ29yeU9iamVjdCA9IHtcbiAgICAgICAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAgICAgICAndXNlcl9pZCc6dGhpcy51c2VySWQsXG4gICAgICAgICAgICAnY2F0ZWdvcnknOntcbiAgICAgICAgICAgICAgJ2lkJzppdGVtLmlkLFxuICAgICAgICAgICAgICAnbmFtZSc6aXRlbS5uYW1lLFxuICAgICAgICAgICAgICAndGh1bWInOml0ZW0udGh1bWIsXG4gICAgICAgICAgICAgICdkZXNjcmlwdGlvbic6aXRlbS5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6aXNBY3RpdmUsXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOicwJ1xuICAgICAgICAgICAgfX07XG4gIFx0dGhpcy5jYXRlZ29yeVNlcnZpY2UuY3JlYXRlQ2F0ZWdvcnkodGhpcy5jYXRlZ29yeU9iamVjdCkudGhlbihcbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlLG51bGwpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcbn1cbmRlbGV0ZUNhdGVnb3J5KGl0ZW0saXNBY3RpdmUpe1xuICB2YXIgYWN0aW9uID0gJ3VwZGF0ZV9jYXRlZ29yaWVzX2J5X2lkJztcbiAgdGhpcy5jYXRlZ29yeU9iamVjdCA9IHtcbiAgICAgICAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAgICAgICAndXNlcl9pZCc6dGhpcy51c2VySWQsXG4gICAgICAgICAgICAnY2F0ZWdvcnknOntcbiAgICAgICAgICAgICAgJ2lkJzppdGVtLmlkLFxuICAgICAgICAgICAgICAnbmFtZSc6aXRlbS5uYW1lLFxuICAgICAgICAgICAgICAndGh1bWInOml0ZW0udGh1bWIsXG4gICAgICAgICAgICAgICdkZXNjcmlwdGlvbic6aXRlbS5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6aXNBY3RpdmUsXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOicxJ1xuICAgICAgICAgICAgfX07XG4gIFx0dGhpcy5jYXRlZ29yeVNlcnZpY2UuY3JlYXRlQ2F0ZWdvcnkodGhpcy5jYXRlZ29yeU9iamVjdCkudGhlbihcbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlLGl0ZW0udGh1bWIpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcbn1cbnByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXNwb25zZSx0aHVtYikge1xuXG4gICAgaWYgKHJlc3BvbnNlLm1lc3NhZ2UgPT0gdW5kZWZpbmVkIHx8IHJlc3BvbnNlLm1lc3NhZ2UgPT0gJ09LJykge1xuICAgICAgaWYodGh1bWIhPW51bGwpe1xuICAgICAgICB0aGlzLmNhdGVnb3J5U2VydmljZS5kZWxldGVGaWxlKHsgJ3RodW1iJzogdGh1bWIgfSkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgdGhpcy5ub3RpZi5zdWNjZXNzKCdOZXdzIGhhcyBiZWVuIGRlbGV0ZWQnKTtcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICB0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyBDYXRlZ29yeSBoYXMgYmVlbiBhZGRlZCcpO1xuICAgICAgdGhpcy5mZXRjaEFsbENhdGVnb3JpZXModGhpcy5yZXN0SWQpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICAgIH1cbiAgfVxuICBwcml2YXRlIGZhaWxlZENyZWF0ZSAocmVzKSB7XG4gICAgc3dhbChcbiAgICAgICAgICAnVXBkYXRlIEZhaWwhJyxcbiAgICAgICAgICAnZXJyb3InXG4gICAgICAgICkuY2F0Y2goc3dhbC5ub29wKTtcbiAgfVxuICBjcmVhdGVDYXRlZ29yeSgpe1xuICAgIC8vICAgdGhpcy51c2VyU2VydmljZS5jdXJyZW50VXNlci5zdWJzY3JpYmUoKHVzZXIpID0+IHtcbiAgICAvLyAgICBjb25zb2xlLmxvZygndXNlcicsIHVzZXIpO1xuXG4gICAgLy8gfSk7XG4gICAgICB0aGlzLlN0b3JhZ2VTZXJ2aWNlLnNldFNjb3BlKG51bGwpO1xuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL2NhdGVnb3J5L2NyZWF0ZScpO1xuICB9XG4gIGVkaXRDYXRlZ29yeShpdGVtKXtcbiAgICAgIHRoaXMuU3RvcmFnZVNlcnZpY2Uuc2V0U2NvcGUoaXRlbSk7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvY2F0ZWdvcnkvY3JlYXRlLycrIGl0ZW0uaWQpO1xuICB9XG59XG4iXX0=
