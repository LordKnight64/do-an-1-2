"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_select_1 = require("ng2-select");
var router_1 = require("@angular/router");
var utils_1 = require("app/utils/utils");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var order_service_1 = require("app/services/order.service");
var comCode_service_1 = require("app/services/comCode.service");
var Observable_1 = require("rxjs/Observable");
var notification_service_1 = require("app/services/notification.service");
var OrdersListComponent = (function () {
    function OrdersListComponent(userServ, router, orderServ, storageServ, notifyServ, comCodeServ) {
        this.userServ = userServ;
        this.router = router;
        this.orderServ = orderServ;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.comCodeServ = comCodeServ;
        this.order_sts = null;
        this.fromDate = null;
        this.toDate = null;
        this.disabled = false;
        this.paymentMethodItems = [];
        this.order = {};
        this.isShow = true;
    }
    OrdersListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.paymentMethodItems.push({ id: 1, text: "Me" });
        this.paymentMethodItems.push({ id: 2, text: "Other" });
        this.orderPersionSelect.items = this.paymentMethodItems;
        if (this.router.url.indexOf('/my-orders') == 0) {
            this.disabled = true;
            this.userIdSearch = "1";
            this.orderPersionSelect.active = [{ id: 1, text: "Me" }];
        }
        this.comCodeServ.getCode({ 'cd_group': '5' }).then(function (response) {
            _this.statusSelect.items = response.code.map(function (x) {
                return { id: x.cd.toString(), text: x.cd_name };
            });
        }, function (error) {
            console.log(error);
        });
        this.doSearch(true);
        // long polling
        this.subscription = Observable_1.Observable.interval(3000)
            .map(function (x) { return x + 1; })
            .subscribe(function (x) {
            _this.doSearch(false);
        });
    };
    OrdersListComponent.prototype.getDeliveryMethod = function () {
        var _this = this;
        this.comCodeServ.getCode({ 'cd_group': '0' }).then(function (response) {
            console.log('response ', response);
            var codes = response.code;
            for (var i = 0; i < codes.length; i++) {
                if (_this.order.delivery_type == +codes[i].cd) {
                    _this.deliveryMethod = codes[i].cd_name;
                    console.log(_this.deliveryMethod);
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    OrdersListComponent.prototype.viewDetail = function (row) {
        this.order = row;
        this.getDeliveryMethod();
    };
    OrdersListComponent.prototype.doUpdateOrder = function (order, orderStatus) {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'order_id': order.id,
            'status': orderStatus
        };
        this.orderServ.updateOrder(params).then(function (response) {
            _this.notifyServ.success('Order has been updated');
            _this.router.navigateByUrl('order/list');
            //this.goBack();
        }, function (error) {
            console.log(error);
        });
    };
    OrdersListComponent.prototype.orderFood = function () {
        this.router.navigateByUrl('order-food');
    };
    OrdersListComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    OrdersListComponent.prototype.doSearch = function (isSelect) {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'order_sts': this.order_sts,
            'delivery_ts_to': this.fromDate,
            'delivery_ts_from': this.toDate,
            'user_id_search': this.userIdSearch
        };
        this.orderServ.getOrderList(params).then(function (response) {
            _this.orderList = response.orders;
            _this.requestOrderList = _this.orderList.filter(function (x) { return x.order_sts == 0; });
            _this.requestOrderDelivery = _this.requestOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.requestOrderPickup = _this.requestOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.verifiedOrderList = _this.orderList.filter(function (x) { return x.order_sts == 1; });
            _this.verifiedOrderDelivery = _this.verifiedOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.verifiedOrderPickup = _this.verifiedOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.pendingOrderList = _this.orderList.filter(function (x) { return x.order_sts == 99; });
            _this.pendingOrderDelivery = _this.pendingOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.pendingOrderPickup = _this.pendingOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.deliveryOrderList = _this.orderList.filter(function (x) { return x.order_sts == 2; });
            _this.deliveryOrderDelivery = _this.deliveryOrderList.filter(function (x) { return x.delivery_type == 0; });
            _this.deliveryOrderPickup = _this.deliveryOrderList.filter(function (x) { return x.delivery_type == 1; });
            _this.requestOrderTempList = _this.requestOrderList;
            _this.verifiedOrderTempList = _this.verifiedOrderList;
            _this.pendingOrderTempList = _this.pendingOrderList;
            _this.deliveryOrderTempList = _this.deliveryOrderList;
            if (isSelect == true)
                _this.order = _this.requestOrderTempList[0];
            _this.getDeliveryMethod();
            if (_this.requestOrderList.length != 0)
                _this.isShow = true;
            else
                _this.isShow = false;
        }, function (error) {
            console.log(error);
        });
    };
    OrdersListComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    OrdersListComponent.prototype.doModelDisplay = function (orderType, deliveryType) {
        //this.storageServ.setScope(obj);
        if (orderType == 0) {
            if (deliveryType == 0) {
                this.requestOrderList = this.requestOrderTempList;
            }
            else if (deliveryType == 1) {
                this.requestOrderList = this.requestOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.requestOrderList = this.requestOrderPickup;
            }
        }
        else if (orderType == 1) {
            if (deliveryType == 0) {
                this.verifiedOrderList = this.verifiedOrderTempList;
            }
            else if (deliveryType == 1) {
                this.verifiedOrderList = this.verifiedOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.verifiedOrderList = this.verifiedOrderPickup;
            }
        }
        else if (orderType == 2) {
            if (deliveryType == 0) {
                this.deliveryOrderList = this.deliveryOrderTempList;
            }
            else if (deliveryType == 1) {
                this.deliveryOrderList = this.deliveryOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.deliveryOrderList = this.deliveryOrderPickup;
            }
        }
        else if (orderType == 99) {
            if (deliveryType == 0) {
                this.pendingOrderList = this.pendingOrderTempList;
            }
            else if (deliveryType == 1) {
                this.pendingOrderList = this.pendingOrderDelivery;
            }
            else if (deliveryType == 2) {
                this.pendingOrderList = this.pendingOrderPickup;
            }
        }
    };
    OrdersListComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
    };
    OrdersListComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    OrdersListComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    OrdersListComponent.prototype.refreshValue = function (value) {
        this.order_sts = value.id;
        this.doSearch(false);
        console.log('Refresh value is: ', value);
    };
    OrdersListComponent.prototype.onSelectFromDate = function (value) {
        this.doSearch(false);
        console.log('New search input: ', value);
    };
    OrdersListComponent.prototype.onSelectToDate = function (value) {
        this.doSearch(false);
        console.log('New search input: ', value);
    };
    OrdersListComponent.prototype.refreshValuePersion = function (value) {
        this.userIdSearch = value.id;
        this.doSearch(false);
        console.log('Refresh value is: ', value);
    };
    __decorate([
        core_1.ViewChild('StatusSelect'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], OrdersListComponent.prototype, "statusSelect", void 0);
    __decorate([
        core_1.ViewChild('OrderPersionSelect'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], OrdersListComponent.prototype, "orderPersionSelect", void 0);
    OrdersListComponent = __decorate([
        core_1.Component({
            selector: 'orders-list',
            templateUrl: utils_1.default.getView('app/components/orders-list/orders-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/orders-list/orders-list.component.css')]
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            router_1.Router,
            order_service_1.OrderService,
            storage_service_1.StorageService,
            notification_service_1.NotificationService,
            comCode_service_1.ComCodeService])
    ], OrdersListComponent);
    return OrdersListComponent;
}());
exports.OrdersListComponent = OrdersListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL29yZGVycy1saXN0L29yZGVycy1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUE2RDtBQUM3RCx5Q0FBNkM7QUFDN0MsMENBQXlDO0FBRXpDLHlDQUFvQztBQUNwQywwREFBd0Q7QUFDeEQsZ0VBQThEO0FBQzlELDREQUEwRDtBQUMxRCxnRUFBOEQ7QUFDOUQsOENBQTJDO0FBQzNDLDBFQUF3RTtBQU14RTtJQWlDRSw2QkFDVSxRQUFxQixFQUNyQixNQUFjLEVBQ2QsU0FBdUIsRUFDdkIsV0FBMkIsRUFDMUIsVUFBK0IsRUFDaEMsV0FBMkI7UUFMM0IsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2QixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFDaEMsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBakI3QixjQUFTLEdBQVEsSUFBSSxDQUFDO1FBQ3RCLGFBQVEsR0FBUSxJQUFJLENBQUM7UUFDckIsV0FBTSxHQUFRLElBQUksQ0FBQztRQUNuQixhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLHVCQUFrQixHQUFlLEVBQUUsQ0FBQztRQUduQyxVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2YsV0FBTSxHQUFPLElBQUksQ0FBQztJQVV2QixDQUFDO0lBRUwsc0NBQVEsR0FBUjtRQUFBLGlCQWlDQztRQS9CQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUMzRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3pCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxQyxDQUFDO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDeEQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUM7WUFDeEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUMzRCxDQUFDO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ3pELEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQztnQkFDckQsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNsRCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVwQixlQUFlO1FBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsdUJBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2FBQ3pDLEdBQUcsQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsR0FBQyxDQUFDLEVBQUgsQ0FBRyxDQUFDO2FBQ2YsU0FBUyxDQUFDLFVBQUMsQ0FBQztZQUNaLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7SUFFTCxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCO1FBQUEsaUJBY0M7UUFiQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDM0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbkMsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUMxQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDdEMsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRyxDQUFDLENBQUMsQ0FBQztvQkFDOUMsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO29CQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUUsQ0FBQztnQkFDbkMsQ0FBQztZQUNGLENBQUM7UUFFRixDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFUSx3Q0FBVSxHQUFsQixVQUFtQixHQUFHO1FBRXJCLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBRTNCLENBQUM7SUFFUSwyQ0FBYSxHQUFyQixVQUFzQixLQUFLLEVBQUUsV0FBVztRQUF4QyxpQkFjRDtRQVpBLElBQUksTUFBTSxHQUFHO1lBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNwQixRQUFRLEVBQUUsV0FBVztTQUNyQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUMvQyxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ2xELEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3hDLGdCQUFnQjtRQUNqQixDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFQSx1Q0FBUyxHQUFUO1FBQ0csSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFFRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUFFTyxzQ0FBUSxHQUFoQixVQUFpQixRQUFRO1FBQXpCLGlCQW1DQztRQWpDQyxJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQzNCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxRQUFRO1lBQy9CLGtCQUFrQixFQUFFLElBQUksQ0FBQyxNQUFNO1lBQy9CLGdCQUFnQixFQUFFLElBQUksQ0FBQyxZQUFZO1NBQ3BDLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQy9DLEtBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUNqQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBaEIsQ0FBZ0IsQ0FBQyxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGFBQWEsSUFBSSxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztZQUNwRixLQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUM7WUFDbEYsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQWhCLENBQWdCLENBQUMsQ0FBQztZQUN0RSxLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUM7WUFDdEYsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsYUFBYSxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1lBQ3BGLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFqQixDQUFpQixDQUFDLENBQUM7WUFDdEUsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsYUFBYSxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1lBQ3BGLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGFBQWEsSUFBSSxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztZQUNsRixLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBaEIsQ0FBZ0IsQ0FBQyxDQUFDO1lBQ3RFLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGFBQWEsSUFBSSxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztZQUN0RixLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUM7WUFDcEYsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNsRCxLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDO1lBQ3BELEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDbEQsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQTtZQUNuRCxFQUFFLENBQUEsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO2dCQUFDLEtBQUksQ0FBQyxLQUFLLEdBQUksS0FBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hFLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3pCLEVBQUUsQ0FBQSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLElBQUksQ0FBRSxDQUFDO2dCQUFDLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzFELElBQUk7Z0JBQUMsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sd0NBQVUsR0FBbEIsVUFBbUIsR0FBRztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRU8sNENBQWMsR0FBdEIsVUFBdUIsU0FBUyxFQUFFLFlBQVk7UUFDNUMsaUNBQWlDO1FBQ2pDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25CLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1lBQ3BELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDcEQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUNsRCxDQUFDO1FBQ0gsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQixFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztZQUN0RCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDO1lBQ3RELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDcEQsQ0FBQztRQUNILENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUM7WUFDdEQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztZQUN0RCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3BELENBQUM7UUFDSCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNCLEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1lBQ3BELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDcEQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUNsRCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFFTSxzQ0FBUSxHQUFmLFVBQWdCLEtBQVU7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU0scUNBQU8sR0FBZCxVQUFlLEtBQVU7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sbUNBQUssR0FBWixVQUFhLEtBQVU7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sMENBQVksR0FBbkIsVUFBb0IsS0FBVTtRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSw4Q0FBZ0IsR0FBdkIsVUFBd0IsS0FBVTtRQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLDRDQUFjLEdBQXJCLFVBQXNCLEtBQVU7UUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSxpREFBbUIsR0FBMUIsVUFBMkIsS0FBVTtRQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUE1TTBCO1FBQTFCLGdCQUFTLENBQUMsY0FBYyxDQUFDO2tDQUF1Qiw0QkFBZTs2REFBQztJQUNoQztRQUFoQyxnQkFBUyxDQUFDLG9CQUFvQixDQUFDO2tDQUE2Qiw0QkFBZTttRUFBQztJQWhDbEUsbUJBQW1CO1FBTC9CLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsYUFBYTtZQUN2QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx1REFBdUQsQ0FBQztZQUNuRixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHNEQUFzRCxDQUFDLENBQUM7U0FDbkYsQ0FBQzt5Q0FtQ29CLDBCQUFXO1lBQ2IsZUFBTTtZQUNILDRCQUFZO1lBQ1YsZ0NBQWM7WUFDZCwwQ0FBbUI7WUFDbkIsZ0NBQWM7T0F2QzFCLG1CQUFtQixDQTRPL0I7SUFBRCwwQkFBQztDQTVPRCxBQTRPQyxJQUFBO0FBNU9ZLGtEQUFtQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9vcmRlcnMtbGlzdC9vcmRlcnMtbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTZWxlY3RDb21wb25lbnQgfSBmcm9tICduZzItc2VsZWN0JztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0sIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZVwiO1xuaW1wb3J0IHsgT3JkZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tQ29kZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY29tQ29kZS5zZXJ2aWNlJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdvcmRlcnMtbGlzdCcsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcmRlcnMtbGlzdC9vcmRlcnMtbGlzdC5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcmRlcnMtbGlzdC9vcmRlcnMtbGlzdC5jb21wb25lbnQuY3NzJyldXG59KVxuZXhwb3J0IGNsYXNzIE9yZGVyc0xpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHByaXZhdGUgcmVzdElkO1xuICBwcml2YXRlIHVzZXJJZDtcbiAgcHJpdmF0ZSB1c2VySWRTZWFyY2g7XG4gIHByaXZhdGUgb3JkZXJMaXN0OiBhbnk7XG4gIHByaXZhdGUgcmVxdWVzdE9yZGVyTGlzdDogYW55O1xuICBwcml2YXRlIHJlcXVlc3RPcmRlclRlbXBMaXN0OiBhbnk7XG4gIHByaXZhdGUgcmVxdWVzdE9yZGVyRGVsaXZlcnk6IGFueTtcbiAgcHJpdmF0ZSByZXF1ZXN0T3JkZXJQaWNrdXA6IGFueTtcbiAgcHJpdmF0ZSB2ZXJpZmllZE9yZGVyTGlzdDogYW55O1xuICBwcml2YXRlIHZlcmlmaWVkT3JkZXJUZW1wTGlzdDogYW55O1xuICBwcml2YXRlIHZlcmlmaWVkT3JkZXJEZWxpdmVyeTogYW55O1xuICBwcml2YXRlIHZlcmlmaWVkT3JkZXJQaWNrdXA6IGFueTtcbiAgcHJpdmF0ZSBwZW5kaW5nT3JkZXJMaXN0OiBhbnk7XG4gIHByaXZhdGUgcGVuZGluZ09yZGVyVGVtcExpc3Q6IGFueTtcbiAgcHJpdmF0ZSBwZW5kaW5nT3JkZXJEZWxpdmVyeTogYW55O1xuICBwcml2YXRlIHBlbmRpbmdPcmRlclBpY2t1cDogYW55O1xuICBwcml2YXRlIGRlbGl2ZXJ5T3JkZXJMaXN0OiBhbnk7XG4gIHByaXZhdGUgZGVsaXZlcnlPcmRlclRlbXBMaXN0OiBhbnk7XG4gIHByaXZhdGUgZGVsaXZlcnlPcmRlckRlbGl2ZXJ5OiBhbnk7XG4gIHByaXZhdGUgZGVsaXZlcnlPcmRlclBpY2t1cDogYW55O1xuICBwcml2YXRlIG9yZGVyX3N0czogYW55ID0gbnVsbDtcbiAgcHJpdmF0ZSBmcm9tRGF0ZTogYW55ID0gbnVsbDtcbiAgcHJpdmF0ZSB0b0RhdGU6IGFueSA9IG51bGw7XG4gIHByaXZhdGUgZGlzYWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgcHJpdmF0ZSBwYXltZW50TWV0aG9kSXRlbXM6IEFycmF5PGFueT4gPSBbXTtcbiAgcHJpdmF0ZSBzdWJzY3JpcHRpb246YW55O1xuICBwcml2YXRlIGRlbGl2ZXJ5TWV0aG9kOiBhbnk7XG4gICBwcml2YXRlIG9yZGVyOmFueSA9IHt9O1xuICAgcHJpdmF0ZSBpc1Nob3c6YW55ID0gdHJ1ZTtcbiAgQFZpZXdDaGlsZCgnU3RhdHVzU2VsZWN0JykgcHJpdmF0ZSBzdGF0dXNTZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcbiAgQFZpZXdDaGlsZCgnT3JkZXJQZXJzaW9uU2VsZWN0JykgcHJpdmF0ZSBvcmRlclBlcnNpb25TZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIG9yZGVyU2VydjogT3JkZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIFx0cHJpdmF0ZSBub3RpZnlTZXJ2OiBOb3RpZmljYXRpb25TZXJ2aWNlLFxuICAgIHByaXZhdGUgY29tQ29kZVNlcnY6IENvbUNvZGVTZXJ2aWNlXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgICBsZXQgdXNlcktleSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgIGlmICghdXNlcktleSkge1xuICAgICAgdGhpcy51c2VyU2Vydi5sb2dvdXQoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy51c2VySWQgPSB1c2VyS2V5LnVzZXJfaW5mby5pZDtcbiAgICAgIHRoaXMucmVzdElkID0gdXNlcktleS5yZXN0YXVyYW50c1swXS5pZDtcbiAgICB9XG4gICAgdGhpcy5wYXltZW50TWV0aG9kSXRlbXMucHVzaCh7IGlkOiAxLCB0ZXh0OiBgTWVgIH0pO1xuICAgIHRoaXMucGF5bWVudE1ldGhvZEl0ZW1zLnB1c2goeyBpZDogMiwgdGV4dDogYE90aGVyYCB9KTtcbiAgICB0aGlzLm9yZGVyUGVyc2lvblNlbGVjdC5pdGVtcyA9IHRoaXMucGF5bWVudE1ldGhvZEl0ZW1zO1xuICAgIGlmICh0aGlzLnJvdXRlci51cmwuaW5kZXhPZignL215LW9yZGVycycpID09IDApIHtcbiAgICAgIHRoaXMuZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgdGhpcy51c2VySWRTZWFyY2ggPSBcIjFcIjtcbiAgICAgIHRoaXMub3JkZXJQZXJzaW9uU2VsZWN0LmFjdGl2ZSA9IFt7IGlkOiAxLCB0ZXh0OiBgTWVgIH1dO1xuICAgIH1cbiAgICB0aGlzLmNvbUNvZGVTZXJ2LmdldENvZGUoeyAnY2RfZ3JvdXAnOiAnNScgfSkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICB0aGlzLnN0YXR1c1NlbGVjdC5pdGVtcyA9IHJlc3BvbnNlLmNvZGUubWFwKGZ1bmN0aW9uICh4KSB7XG4gICAgICAgIHJldHVybiB7IGlkOiB4LmNkLnRvU3RyaW5nKCksIHRleHQ6IHguY2RfbmFtZSB9O1xuICAgICAgfSk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICAgIHRoaXMuZG9TZWFyY2godHJ1ZSk7XG5cbiAgICAvLyBsb25nIHBvbGxpbmdcbiAgIHRoaXMuc3Vic2NyaXB0aW9uID0gT2JzZXJ2YWJsZS5pbnRlcnZhbCgzMDAwKVxuICAgICAgLm1hcCgoeCkgPT4geCsxKVxuICAgICAgLnN1YnNjcmliZSgoeCkgPT4ge1xuICAgICAgIHRoaXMuZG9TZWFyY2goZmFsc2UpO1xuICAgIH0pO1xuXG4gIH1cblxuICBnZXREZWxpdmVyeU1ldGhvZCgpIHtcbiAgICB0aGlzLmNvbUNvZGVTZXJ2LmdldENvZGUoeyAnY2RfZ3JvdXAnOiAnMCcgfSkudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKCdyZXNwb25zZSAnLCByZXNwb25zZSk7IFxuXHRcdFx0XHRsZXQgY29kZXMgPSByZXNwb25zZS5jb2RlO1xuXHRcdFx0XHRmb3IobGV0IGkgPSAwOyBpIDwgY29kZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRpZih0aGlzLm9yZGVyLmRlbGl2ZXJ5X3R5cGUgPT0gK2NvZGVzW2ldLmNkICkge1xuXHRcdFx0XHRcdFx0dGhpcy5kZWxpdmVyeU1ldGhvZCA9IGNvZGVzW2ldLmNkX25hbWU7XG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyh0aGlzLmRlbGl2ZXJ5TWV0aG9kICk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFxuXHRcdFx0fSwgZXJyb3IgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XG5cdFx0XHR9KTtcbiAgfVxuXG4gICBwcml2YXRlIHZpZXdEZXRhaWwocm93KSB7XG4gICBcbiAgICB0aGlzLm9yZGVyID0gcm93O1xuICAgIHRoaXMuZ2V0RGVsaXZlcnlNZXRob2QoKTtcbiAgICAgIFxuICB9XG5cbiAgXHRwcml2YXRlIGRvVXBkYXRlT3JkZXIob3JkZXIsIG9yZGVyU3RhdHVzKSB7XG5cblx0XHR2YXIgcGFyYW1zID0ge1xuXHRcdFx0J3VzZXJfaWQnOiB0aGlzLnVzZXJJZCxcblx0XHRcdCdvcmRlcl9pZCc6IG9yZGVyLmlkLFxuXHRcdFx0J3N0YXR1cyc6IG9yZGVyU3RhdHVzXG5cdFx0fTtcblx0XHR0aGlzLm9yZGVyU2Vydi51cGRhdGVPcmRlcihwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0dGhpcy5ub3RpZnlTZXJ2LnN1Y2Nlc3MoJ09yZGVyIGhhcyBiZWVuIHVwZGF0ZWQnKTtcblx0XHRcdHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ29yZGVyL2xpc3QnKTtcblx0XHRcdC8vdGhpcy5nb0JhY2soKTtcblx0XHR9LCBlcnJvciA9PiB7XG5cdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XG5cdFx0fSk7XG5cdH1cblxuICBvcmRlckZvb2QoKSB7XG4gICAgXHR0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCdvcmRlci1mb29kJyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpe1xuXG4gICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZWFyY2goaXNTZWxlY3QpIHtcblxuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAndXNlcl9pZCc6IHRoaXMudXNlcklkLFxuICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICdvcmRlcl9zdHMnOiB0aGlzLm9yZGVyX3N0cyxcbiAgICAgICdkZWxpdmVyeV90c190byc6IHRoaXMuZnJvbURhdGUsXG4gICAgICAnZGVsaXZlcnlfdHNfZnJvbSc6IHRoaXMudG9EYXRlLFxuICAgICAgJ3VzZXJfaWRfc2VhcmNoJzogdGhpcy51c2VySWRTZWFyY2hcbiAgICB9O1xuICAgIHRoaXMub3JkZXJTZXJ2LmdldE9yZGVyTGlzdChwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgdGhpcy5vcmRlckxpc3QgPSByZXNwb25zZS5vcmRlcnM7XG4gICAgICB0aGlzLnJlcXVlc3RPcmRlckxpc3QgPSB0aGlzLm9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4Lm9yZGVyX3N0cyA9PSAwKTtcbiAgICAgIHRoaXMucmVxdWVzdE9yZGVyRGVsaXZlcnkgPSB0aGlzLnJlcXVlc3RPcmRlckxpc3QuZmlsdGVyKHggPT4geC5kZWxpdmVyeV90eXBlID09IDApO1xuICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJQaWNrdXAgPSB0aGlzLnJlcXVlc3RPcmRlckxpc3QuZmlsdGVyKHggPT4geC5kZWxpdmVyeV90eXBlID09IDEpO1xuICAgICAgdGhpcy52ZXJpZmllZE9yZGVyTGlzdCA9IHRoaXMub3JkZXJMaXN0LmZpbHRlcih4ID0+IHgub3JkZXJfc3RzID09IDEpO1xuICAgICAgdGhpcy52ZXJpZmllZE9yZGVyRGVsaXZlcnkgPSB0aGlzLnZlcmlmaWVkT3JkZXJMaXN0LmZpbHRlcih4ID0+IHguZGVsaXZlcnlfdHlwZSA9PSAwKTtcbiAgICAgIHRoaXMudmVyaWZpZWRPcmRlclBpY2t1cCA9IHRoaXMudmVyaWZpZWRPcmRlckxpc3QuZmlsdGVyKHggPT4geC5kZWxpdmVyeV90eXBlID09IDEpO1xuICAgICAgdGhpcy5wZW5kaW5nT3JkZXJMaXN0ID0gdGhpcy5vcmRlckxpc3QuZmlsdGVyKHggPT4geC5vcmRlcl9zdHMgPT0gOTkpO1xuICAgICAgdGhpcy5wZW5kaW5nT3JkZXJEZWxpdmVyeSA9IHRoaXMucGVuZGluZ09yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMCk7XG4gICAgICB0aGlzLnBlbmRpbmdPcmRlclBpY2t1cCA9IHRoaXMucGVuZGluZ09yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMSk7XG4gICAgICB0aGlzLmRlbGl2ZXJ5T3JkZXJMaXN0ID0gdGhpcy5vcmRlckxpc3QuZmlsdGVyKHggPT4geC5vcmRlcl9zdHMgPT0gMik7XG4gICAgICB0aGlzLmRlbGl2ZXJ5T3JkZXJEZWxpdmVyeSA9IHRoaXMuZGVsaXZlcnlPcmRlckxpc3QuZmlsdGVyKHggPT4geC5kZWxpdmVyeV90eXBlID09IDApO1xuICAgICAgdGhpcy5kZWxpdmVyeU9yZGVyUGlja3VwID0gdGhpcy5kZWxpdmVyeU9yZGVyTGlzdC5maWx0ZXIoeCA9PiB4LmRlbGl2ZXJ5X3R5cGUgPT0gMSk7XG4gICAgICB0aGlzLnJlcXVlc3RPcmRlclRlbXBMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJMaXN0O1xuICAgICAgdGhpcy52ZXJpZmllZE9yZGVyVGVtcExpc3QgPSB0aGlzLnZlcmlmaWVkT3JkZXJMaXN0O1xuICAgICAgdGhpcy5wZW5kaW5nT3JkZXJUZW1wTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyTGlzdDtcbiAgICAgIHRoaXMuZGVsaXZlcnlPcmRlclRlbXBMaXN0ID0gdGhpcy5kZWxpdmVyeU9yZGVyTGlzdFxuICAgICAgaWYoaXNTZWxlY3QgPT0gdHJ1ZSkgdGhpcy5vcmRlciA9ICB0aGlzLnJlcXVlc3RPcmRlclRlbXBMaXN0WzBdO1xuICAgICAgdGhpcy5nZXREZWxpdmVyeU1ldGhvZCgpO1xuICAgICAgaWYodGhpcy5yZXF1ZXN0T3JkZXJMaXN0Lmxlbmd0aCAhPSAwICkgdGhpcy5pc1Nob3cgPSB0cnVlO1xuICAgICAgZWxzZSB0aGlzLmlzU2hvdyA9IGZhbHNlO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZXRTY29wZShvYmopIHtcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2LnNldFNjb3BlKG9iaik7XG4gIH1cblxuICBwcml2YXRlIGRvTW9kZWxEaXNwbGF5KG9yZGVyVHlwZSwgZGVsaXZlcnlUeXBlKSB7XG4gICAgLy90aGlzLnN0b3JhZ2VTZXJ2LnNldFNjb3BlKG9iaik7XG4gICAgaWYgKG9yZGVyVHlwZSA9PSAwKSB7XG4gICAgICBpZiAoZGVsaXZlcnlUeXBlID09IDApIHtcbiAgICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJUZW1wTGlzdDtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDEpIHtcbiAgICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJEZWxpdmVyeTtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDIpIHtcbiAgICAgICAgdGhpcy5yZXF1ZXN0T3JkZXJMaXN0ID0gdGhpcy5yZXF1ZXN0T3JkZXJQaWNrdXA7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChvcmRlclR5cGUgPT0gMSkge1xuICAgICAgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAwKSB7XG4gICAgICAgIHRoaXMudmVyaWZpZWRPcmRlckxpc3QgPSB0aGlzLnZlcmlmaWVkT3JkZXJUZW1wTGlzdDtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDEpIHtcbiAgICAgICAgdGhpcy52ZXJpZmllZE9yZGVyTGlzdCA9IHRoaXMudmVyaWZpZWRPcmRlckRlbGl2ZXJ5O1xuICAgICAgfSBlbHNlIGlmIChkZWxpdmVyeVR5cGUgPT0gMikge1xuICAgICAgICB0aGlzLnZlcmlmaWVkT3JkZXJMaXN0ID0gdGhpcy52ZXJpZmllZE9yZGVyUGlja3VwO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAob3JkZXJUeXBlID09IDIpIHtcbiAgICAgIGlmIChkZWxpdmVyeVR5cGUgPT0gMCkge1xuICAgICAgICB0aGlzLmRlbGl2ZXJ5T3JkZXJMaXN0ID0gdGhpcy5kZWxpdmVyeU9yZGVyVGVtcExpc3Q7XG4gICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAxKSB7XG4gICAgICAgIHRoaXMuZGVsaXZlcnlPcmRlckxpc3QgPSB0aGlzLmRlbGl2ZXJ5T3JkZXJEZWxpdmVyeTtcbiAgICAgIH0gZWxzZSBpZiAoZGVsaXZlcnlUeXBlID09IDIpIHtcbiAgICAgICAgdGhpcy5kZWxpdmVyeU9yZGVyTGlzdCA9IHRoaXMuZGVsaXZlcnlPcmRlclBpY2t1cDtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKG9yZGVyVHlwZSA9PSA5OSkge1xuICAgICAgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAwKSB7XG4gICAgICAgIHRoaXMucGVuZGluZ09yZGVyTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyVGVtcExpc3Q7XG4gICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAxKSB7XG4gICAgICAgIHRoaXMucGVuZGluZ09yZGVyTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyRGVsaXZlcnk7XG4gICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJ5VHlwZSA9PSAyKSB7XG4gICAgICAgIHRoaXMucGVuZGluZ09yZGVyTGlzdCA9IHRoaXMucGVuZGluZ09yZGVyUGlja3VwO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZWxlY3RlZCh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ1NlbGVjdGVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XG4gIH1cblxuICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ1JlbW92ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxuXG4gIHB1YmxpYyB0eXBlZCh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ05ldyBzZWFyY2ggaW5wdXQ6ICcsIHZhbHVlKTtcbiAgfVxuXG4gIHB1YmxpYyByZWZyZXNoVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMub3JkZXJfc3RzID0gdmFsdWUuaWQ7XG4gICAgdGhpcy5kb1NlYXJjaChmYWxzZSk7XG4gICAgY29uc29sZS5sb2coJ1JlZnJlc2ggdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxuXG4gIHB1YmxpYyBvblNlbGVjdEZyb21EYXRlKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLmRvU2VhcmNoKGZhbHNlKTtcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIG9uU2VsZWN0VG9EYXRlKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLmRvU2VhcmNoKGZhbHNlKTtcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIHJlZnJlc2hWYWx1ZVBlcnNpb24odmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMudXNlcklkU2VhcmNoID0gdmFsdWUuaWQ7XG4gICAgdGhpcy5kb1NlYXJjaChmYWxzZSk7XG4gICAgY29uc29sZS5sb2coJ1JlZnJlc2ggdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxufVxuIl19
