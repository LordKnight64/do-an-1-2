"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var restaurant_service_1 = require("app/services/restaurant.service");
var notification_service_1 = require("app/services/notification.service");
var user_service_1 = require("app/services/user.service");
var FeedbackComponent = (function () {
    function FeedbackComponent(restServ, fb, userServ, notifyServ) {
        this.restServ = restServ;
        this.fb = fb;
        this.userServ = userServ;
        this.notifyServ = notifyServ;
        this.content = null;
        this.formErrors = {
            'content': []
        };
        this.validationMessages = {
            'content': {
                'maxlength': 'The about us may not be greater than 1536 characters.'
            }
        };
    }
    FeedbackComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.buildForm();
        this.mymodel = {
            content: null,
        };
    };
    FeedbackComponent.prototype.buildForm = function () {
        var _this = this;
        this.dtForm = this.fb.group({
            'content': ['', [forms_1.Validators.maxLength(1536)]]
        });
        this.content = this.dtForm.controls['content'];
        this.dtForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
    };
    FeedbackComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.dtForm) {
            return;
        }
        var form1 = this.dtForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form1.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    FeedbackComponent.prototype.sendFeedback = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        var params = {
            'user_id': this.userId,
            'content': this.content.value
        };
        this.restServ.sendFeeback(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            _this.notifyServ.success('your feedback has been sent');
        }, function (error) {
            console.log(error);
        });
    };
    FeedbackComponent = __decorate([
        core_1.Component({
            selector: 'feedback',
            templateUrl: utils_1.default.getView('app/components/feedback/feedback.component.html'),
            styleUrls: [utils_1.default.getView('app/components/feedback/feedback.component.css')]
        }),
        __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
            forms_1.FormBuilder,
            user_service_1.UserService,
            notification_service_1.NotificationService])
    ], FeedbackComponent);
    return FeedbackComponent;
}());
exports.FeedbackComponent = FeedbackComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2ZlZWRiYWNrL2ZlZWRiYWNrLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx3Q0FBNkY7QUFDN0YseUNBQW9DO0FBQ3BDLHNFQUFvRTtBQUNwRSwwRUFBd0U7QUFDeEUsMERBQXdEO0FBTXhEO0lBT0UsMkJBQ1UsUUFBMkIsRUFDM0IsRUFBZSxFQUNmLFFBQXFCLEVBQ3JCLFVBQStCO1FBSC9CLGFBQVEsR0FBUixRQUFRLENBQW1CO1FBQzNCLE9BQUUsR0FBRixFQUFFLENBQWE7UUFDZixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGVBQVUsR0FBVixVQUFVLENBQXFCO1FBTmpDLFlBQU8sR0FBb0IsSUFBSSxDQUFDO1FBdUR4QyxlQUFVLEdBQUc7WUFDWCxTQUFTLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNuQixTQUFTLEVBQUU7Z0JBQ1QsV0FBVyxFQUFFLHVEQUF1RDthQUNyRTtTQUNGLENBQUM7SUF4REUsQ0FBQztJQUVMLG9DQUFRLEdBQVI7UUFFRSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUMzRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3pCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxQyxDQUFDO1FBRUQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDYixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUM7SUFFSixDQUFDO0lBRU8scUNBQVMsR0FBakI7UUFBQSxpQkFVQztRQVJDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDMUIsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUM5QyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRS9DLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWTthQUNyQixTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO0lBRXpELENBQUM7SUFFTywwQ0FBYyxHQUF0QixVQUF1QixPQUFPLEVBQUUsSUFBVTtRQUV4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUM3QixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFNLE9BQU8sR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQzdELElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEQsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBYU8sd0NBQVksR0FBcEI7UUFBQSxpQkEyQkM7UUExQkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0gsQ0FBQztRQUVELElBQUksTUFBTSxHQUFHO1lBQ1gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7U0FDOUIsQ0FBQztRQUdGLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDN0MsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEQsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsTUFBTSxDQUFDO1lBQ1QsQ0FBQztZQUNELEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFDekQsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBbEdVLGlCQUFpQjtRQUw1QixnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLFVBQVU7WUFDcEIsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsaURBQWlELENBQUM7WUFDN0UsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO1NBQzdFLENBQUM7eUNBU29CLHNDQUFpQjtZQUN2QixtQkFBVztZQUNMLDBCQUFXO1lBQ1QsMENBQW1CO09BWDlCLGlCQUFpQixDQW9HN0I7SUFBRCx3QkFBQztDQXBHRCxBQW9HQyxJQUFBO0FBcEdZLDhDQUFpQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9mZWVkYmFjay9mZWVkYmFjay5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IFJlc3RhdXJhbnRTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3Jlc3RhdXJhbnQuc2VydmljZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbiBAQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmZWVkYmFjaycsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9mZWVkYmFjay9mZWVkYmFjay5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9mZWVkYmFjay9mZWVkYmFjay5jb21wb25lbnQuY3NzJyldXG59KVxuZXhwb3J0IGNsYXNzIEZlZWRiYWNrQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgZHRGb3JtOiBGb3JtR3JvdXA7XG4gIHByaXZhdGUgY29udGVudDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJlc3RTZXJ2OiBSZXN0YXVyYW50U2VydmljZSxcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIG5vdGlmeVNlcnY6IE5vdGlmaWNhdGlvblNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcblxuICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVzZXJJZCA9IHVzZXJLZXkudXNlcl9pbmZvLmlkO1xuICAgICAgdGhpcy5yZXN0SWQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIH1cblxuICAgIHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgdGhpcy5teW1vZGVsID0ge1xuICAgICAgY29udGVudDogbnVsbCwgICAgXG4gICAgfTsgXG4gICBcbiAgfSBcblxuICBwcml2YXRlIGJ1aWxkRm9ybSgpOiB2b2lkIHtcblxuICAgIHRoaXMuZHRGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAnY29udGVudCc6IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDE1MzYpXV0gICAgIFxuICAgIH0pO1xuICAgIHRoaXMuY29udGVudCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydjb250ZW50J107XG4gIFxuICAgIHRoaXMuZHRGb3JtLnZhbHVlQ2hhbmdlc1xuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMub25WYWx1ZUNoYW5nZWQoZmFsc2UsIGRhdGEpKTtcbiAgICBcbiAgfVxuXG4gIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQodW5EaXJ0eSwgZGF0YT86IGFueSkge1xuXG4gICAgaWYgKCF0aGlzLmR0Rm9ybSkgeyByZXR1cm47IH1cbiAgICBjb25zdCBmb3JtMSA9IHRoaXMuZHRGb3JtO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybTEuZ2V0KGZpZWxkKTtcbiAgICAgIGlmIChjb250cm9sICYmIChjb250cm9sLmRpcnR5IHx8IHVuRGlydHkpICYmIGNvbnRyb2wuaW52YWxpZCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlcyA9IHRoaXMudmFsaWRhdGlvbk1lc3NhZ2VzW2ZpZWxkXTtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJvbC5lcnJvcnMpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdLnB1c2gobWVzc2FnZXNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmb3JtRXJyb3JzID0ge1xuICAgICdjb250ZW50JzogW10gICBcbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ2NvbnRlbnQnOiB7XG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBhYm91dCB1cyBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxNTM2IGNoYXJhY3RlcnMuJ1xuICAgIH1cbiAgfTtcblxuICAgXG4gIHByaXZhdGUgc2VuZEZlZWRiYWNrKCkge1xuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQodHJ1ZSk7XG4gICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIGlmICh0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAndXNlcl9pZCc6IHRoaXMudXNlcklkLCAgICAgXG4gICAgICAnY29udGVudCc6IHRoaXMuY29udGVudC52YWx1ZSAgICBcbiAgICB9O1xuICAgIFxuXG4gICAgdGhpcy5yZXN0U2Vydi5zZW5kRmVlYmFjayhwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgbGV0IGhhckVycm9yID0gZmFsc2U7XG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHJlc3BvbnNlLmVycm9ycykge1xuICAgICAgICBoYXJFcnJvciA9IHRydWU7XG4gICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSByZXNwb25zZS5lcnJvcnNbZmllbGRdO1xuICAgICAgfVxuICAgICAgaWYgKGhhckVycm9yKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCd5b3VyIGZlZWRiYWNrIGhhcyBiZWVuIHNlbnQnKTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH1cblxufVxuIl19
