"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var optionFoodItem_service_1 = require("app/services/optionFoodItem.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var OptionFoodItemsListComponent = (function () {
    function OptionFoodItemsListComponent(OptionFoodItemService, authServ, notif, router, StorageService) {
        this.OptionFoodItemService = OptionFoodItemService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.StorageService = StorageService;
    }
    OptionFoodItemsListComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllOptionFoodItems(this.restId);
    };
    OptionFoodItemsListComponent.prototype.fetchAllOptionFoodItems = function (restId) {
        var _this = this;
        this.OptionFoodItemService.getOptionFoodItems(restId, null).then(function (response) {
            _this.optionFoodItems = response.options;
            console.log(response.options);
        }, function (error) {
            console.log(error);
        });
    };
    OptionFoodItemsListComponent.prototype.createOptionFoodItem = function () {
        this.StorageService.setScope(null);
        this.router.navigateByUrl('/option-food-item/create');
    };
    OptionFoodItemsListComponent.prototype.editOptionFoodItem = function (item) {
        this.StorageService.setScope(item);
        this.router.navigateByUrl('/option-food-item/create/' + item.id);
    };
    OptionFoodItemsListComponent.prototype.deleteOptionFoodItem = function (item, isActive) {
        var _this = this;
        this.optionFoodItem = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'option': {
                'id': item.id,
                'name': item.name,
                'is_delete': '1'
            }
        };
        this.OptionFoodItemService.createOptionFoodItems(this.optionFoodItem).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    OptionFoodItemsListComponent.prototype.processResult = function (response) {
        if (response.message == undefined || response.message == 'OK') {
            this.notif.success('New Option item has been added');
            this.fetchAllOptionFoodItems(this.restId);
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    OptionFoodItemsListComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    OptionFoodItemsListComponent = __decorate([
        core_1.Component({
            selector: 'option-food-items-list',
            templateUrl: utils_1.default.getView('app/components/option-food-items-list/option-food-items-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/option-food-items-list/option-food-items-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [optionFoodItem_service_1.OptionFoodItemService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            storage_service_1.StorageService])
    ], OptionFoodItemsListComponent);
    return OptionFoodItemsListComponent;
}());
exports.OptionFoodItemsListComponent = OptionFoodItemsListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL29wdGlvbi1mb29kLWl0ZW1zLWxpc3Qvb3B0aW9uLWZvb2QtaXRlbXMtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQseUNBQW9DO0FBR3BDLDZEQUEyRDtBQUMzRCw4RUFBNEU7QUFDNUUsMERBQXlEO0FBQ3pELDJDQUE4QztBQUM5QywwRUFBd0U7QUFDeEUsMENBQXlDO0FBRXpDLGdFQUE4RDtBQVE5RDtJQU1FLHNDQUFvQixxQkFBNEMsRUFDeEQsUUFBcUIsRUFDckIsS0FBMEIsRUFDMUIsTUFBYyxFQUNkLGNBQThCO1FBSmxCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFDeEQsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQUMxQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO0lBQUksQ0FBQztJQUUzQywrQ0FBUSxHQUFSO1FBQ0ssSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUNNLDhEQUF1QixHQUEvQixVQUFnQyxNQUFNO1FBQXRDLGlCQVFHO1FBUEEsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQzlDLFVBQUEsUUFBUTtZQUNQLEtBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDQSwyREFBb0IsR0FBcEI7UUFDRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFDRCx5REFBa0IsR0FBbEIsVUFBbUIsSUFBSTtRQUNyQixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQywyQkFBMkIsR0FBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUNELDJEQUFvQixHQUFwQixVQUFxQixJQUFJLEVBQUMsUUFBUTtRQUFsQyxpQkFZRDtRQVhHLElBQUksQ0FBQyxjQUFjLEdBQUc7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3JCLFFBQVEsRUFBQztnQkFDUCxJQUFJLEVBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ1osTUFBTSxFQUFDLElBQUksQ0FBQyxJQUFJO2dCQUNoQixXQUFXLEVBQUMsR0FBRzthQUNoQjtTQUFDLENBQUM7UUFDWixJQUFJLENBQUMscUJBQXFCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FDN0QsVUFBQSxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixFQUNwQyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUNPLG9EQUFhLEdBQXJCLFVBQXNCLFFBQVE7UUFFNUIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixxQkFBSSxDQUNBLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixDQUFDO0lBQ0gsQ0FBQztJQUNPLG1EQUFZLEdBQXBCLFVBQXNCLEdBQUc7UUFDdkIscUJBQUksQ0FDRSxjQUFjLEVBQ2QsT0FBTyxDQUNSLENBQUMsS0FBSyxDQUFDLHFCQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQWpFVSw0QkFBNEI7UUFQeEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSx3QkFBd0I7WUFDbEMsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsNkVBQTZFLENBQUM7WUFDekcsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyw0RUFBNEUsQ0FBQyxDQUFDO1lBQ3hHLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUMscUJBQXFCLEVBQUUsRUFBRSxFQUFDO1NBQ2xDLENBQUM7eUNBTzJDLDhDQUFxQjtZQUM5QywwQkFBVztZQUNkLDBDQUFtQjtZQUNsQixlQUFNO1lBQ0UsZ0NBQWM7T0FWM0IsNEJBQTRCLENBa0V4QztJQUFELG1DQUFDO0NBbEVELEFBa0VDLElBQUE7QUFsRVksb0VBQTRCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL29wdGlvbi1mb29kLWl0ZW1zLWxpc3Qvb3B0aW9uLWZvb2QtaXRlbXMtbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBPcHRpb25Gb29kSXRlbVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvb3B0aW9uRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdvcHRpb24tZm9vZC1pdGVtcy1saXN0JyxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL29wdGlvbi1mb29kLWl0ZW1zLWxpc3Qvb3B0aW9uLWZvb2QtaXRlbXMtbGlzdC5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcHRpb24tZm9vZC1pdGVtcy1saXN0L29wdGlvbi1mb29kLWl0ZW1zLWxpc3QuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJUcmFuc2l0aW9uXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBPcHRpb25Gb29kSXRlbXNMaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgXG4gIHByaXZhdGUgdXNlcklkIDogU3RyaW5nO1xuICBwcml2YXRlIHJlc3RJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSBvcHRpb25Gb29kSXRlbXMgOiBhbnk7XG4gIHByaXZhdGUgb3B0aW9uRm9vZEl0ZW0gOiBhbnk7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgT3B0aW9uRm9vZEl0ZW1TZXJ2aWNlOiBPcHRpb25Gb29kSXRlbVNlcnZpY2UsXG4gIHByaXZhdGUgYXV0aFNlcnY6IEF1dGhTZXJ2aWNlLFxuICBwcml2YXRlIG5vdGlmOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxuICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICBwcml2YXRlIFN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgICAgbGV0IHVzZXJJbmZvID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgICAgdGhpcy5yZXN0SWQgPSB1c2VySW5mby5yZXN0YXVyYW50c1swXS5pZDtcbiAgICAgICB0aGlzLnVzZXJJZCA9IHVzZXJJbmZvLnVzZXJfaW5mby5pZDtcbiAgICBcdCB0aGlzLmZldGNoQWxsT3B0aW9uRm9vZEl0ZW1zKHRoaXMucmVzdElkKTtcbiAgfVxuXHRwcml2YXRlIGZldGNoQWxsT3B0aW9uRm9vZEl0ZW1zKHJlc3RJZCkge1xuICBcdFx0dGhpcy5PcHRpb25Gb29kSXRlbVNlcnZpY2UuZ2V0T3B0aW9uRm9vZEl0ZW1zKHJlc3RJZCxudWxsKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAgICAgICAgICBcdHRoaXMub3B0aW9uRm9vZEl0ZW1zID0gcmVzcG9uc2Uub3B0aW9ucztcbiAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2Uub3B0aW9ucyk7IFxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG4gICAgY3JlYXRlT3B0aW9uRm9vZEl0ZW0oKXtcbiAgICAgIHRoaXMuU3RvcmFnZVNlcnZpY2Uuc2V0U2NvcGUobnVsbCk7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3B0aW9uLWZvb2QtaXRlbS9jcmVhdGUnKTtcbiAgICB9XG4gICAgZWRpdE9wdGlvbkZvb2RJdGVtKGl0ZW0pe1xuICAgICAgdGhpcy5TdG9yYWdlU2VydmljZS5zZXRTY29wZShpdGVtKTtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vcHRpb24tZm9vZC1pdGVtL2NyZWF0ZS8nKyBpdGVtLmlkKTtcbiAgICB9XG4gICAgZGVsZXRlT3B0aW9uRm9vZEl0ZW0oaXRlbSxpc0FjdGl2ZSl7XG4gICAgICB0aGlzLm9wdGlvbkZvb2RJdGVtID0ge1xuICAgICAgICAgICAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAgICAgICAgICAgJ3VzZXJfaWQnOnRoaXMudXNlcklkLFxuICAgICAgICAgICAgICAgICdvcHRpb24nOntcbiAgICAgICAgICAgICAgICAgICdpZCc6aXRlbS5pZCxcbiAgICAgICAgICAgICAgICAgICduYW1lJzppdGVtLm5hbWUsXG4gICAgICAgICAgICAgICAgICAnaXNfZGVsZXRlJzonMSdcbiAgICAgICAgICAgICAgICB9fTtcbiAgICAgIFx0dGhpcy5PcHRpb25Gb29kSXRlbVNlcnZpY2UuY3JlYXRlT3B0aW9uRm9vZEl0ZW1zKHRoaXMub3B0aW9uRm9vZEl0ZW0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UpLFxuICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG4gIH1cbiAgcHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgICB0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyBPcHRpb24gaXRlbSBoYXMgYmVlbiBhZGRlZCcpO1xuICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN3YWwoXG4gICAgICAgICAgJ1VwZGF0ZSBGYWlsIScsXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICB9XG59XG4iXX0=
