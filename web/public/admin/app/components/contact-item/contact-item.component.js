"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var contact_service_1 = require("app/services/contact.service");
var number_validator_directive_1 = require("app/utils/number-validator.directive");
var email_validator_directive_1 = require("app/utils/email-validator.directive");
var ContactItemComponent = (function () {
    function ContactItemComponent(contactServ, userServ, fb, router, activatedRoute, storageServ, notifyServ) {
        this.contactServ = contactServ;
        this.userServ = userServ;
        this.fb = fb;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.email = null;
        this.phone = null;
        this.address = null;
        this.formErrors = {
            'name': [],
            'email': [],
            'phone': [],
            'address': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name field is required.',
                'maxlength': 'The name may not be greater than 256 characters.'
            },
            'email': {
                'required': 'The email field is required.',
                'maxlength': 'The email may not be greater than 256 characters.',
                'invalidEmail': 'The email must be a valid email address.',
                'minlength': 'The email must be at least 6 characters long.'
            },
            'phone': {
                'maxlength': 'The phone may not be greater than 32 characters.',
                'invalidNumber': 'he phone must be number'
            },
            'address': {
                'maxlength': 'The address may not be greater than 1536 characters.'
            }
        };
    }
    ContactItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.buildForm();
        this.mymodel = {
            id: null,
            name: null,
            email: null,
            phone: null,
            address: null,
            is_delete: 0
        };
        if (this.router.url.indexOf('/contact/edit') == 0) {
            this.action = ['Edit', 'UPDATE', 'updated'];
            if (this.storageServ.scope) {
                this.mymodel = this.storageServ.scope;
                this.setVar();
            }
            else {
                var params = {
                    'id': this.activatedRoute.snapshot.params['id']
                };
                this.contactServ.getContact(params).then(function (response) {
                    _this.mymodel = response.contacts;
                    _this.setVar();
                }, function (error) {
                    console.log(error);
                });
            }
        }
        if (this.router.url.indexOf('/contact/create') == 0) {
            this.action = ['Add', 'ADD', 'added'];
            this.storageServ.setScope(null);
        }
        console.log('init contact item component');
    };
    ContactItemComponent.prototype.setVar = function () {
        if (this.mymodel != null) {
            this.mymodel = {
                id: this.mymodel.id,
                name: this.mymodel.name,
                email: this.mymodel.email,
                phone: this.mymodel.phone,
                address: this.mymodel.address,
                is_delete: 0
            };
            this.dtForm.controls['name'].setValue(this.mymodel.name);
            this.dtForm.controls['email'].setValue(this.mymodel.email);
            this.dtForm.controls['phone'].setValue(this.mymodel.phone);
            this.dtForm.controls['address'].setValue(this.mymodel.address);
            this.name = this.dtForm.controls['name'];
            this.email = this.dtForm.controls['email'];
            this.phone = this.dtForm.controls['phone'];
            this.address = this.dtForm.controls['address'];
        }
    };
    ContactItemComponent.prototype.buildForm = function () {
        var _this = this;
        this.dtForm = this.fb.group({
            'name': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(256)]],
            'email': ['', [forms_1.Validators.required, forms_1.Validators.minLength(6), forms_1.Validators.maxLength(256), email_validator_directive_1.invalidEmailValidator()]],
            'phone': ['', [forms_1.Validators.maxLength(32), number_validator_directive_1.invalidNumberValidator()]],
            'address': ['', [forms_1.Validators.maxLength(1536)]]
        });
        this.name = this.dtForm.controls['name'];
        this.email = this.dtForm.controls['email'];
        this.phone = this.dtForm.controls['phone'];
        this.address = this.dtForm.controls['address'];
        this.dtForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
        //this.onValueChanged(); // (re)set validation messages now
    };
    ContactItemComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.dtForm) {
            return;
        }
        var form = this.dtForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    ContactItemComponent.prototype.doSaveContact = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        //dto
        if (this.action[0] === 'Add') {
            this.mymodel.id = -1;
        }
        this.mymodel.name = this.name.value;
        this.mymodel.email = this.email.value;
        this.mymodel.phone = this.phone.value;
        this.mymodel.address = this.address.value;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'contact': this.mymodel
        };
        this.contactServ.updateContact(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            _this.notifyServ.success('Contact has been ' + _this.action[2]);
            _this.router.navigateByUrl('contact');
        }, function (error) {
            console.log(error);
        });
    };
    ;
    ContactItemComponent = __decorate([
        core_1.Component({
            selector: 'contact-item',
            templateUrl: utils_1.default.getView('app/components/contact-item/contact-item.component.html'),
            styleUrls: [utils_1.default.getView('app/components/contact-item/contact-item.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [contact_service_1.ContactService,
            user_service_1.UserService,
            forms_1.FormBuilder,
            router_1.Router,
            router_1.ActivatedRoute,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], ContactItemComponent);
    return ContactItemComponent;
}());
exports.ContactItemComponent = ContactItemComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NvbnRhY3QtaXRlbS9jb250YWN0LWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELDBDQUFpRTtBQUNqRSx3Q0FBNkY7QUFDN0YseUNBQW9DO0FBRXBDLDZEQUF1RTtBQUN2RSwwREFBd0Q7QUFDeEQsZ0VBQThEO0FBQzlELDBFQUF3RTtBQUN4RSxnRUFBOEQ7QUFDOUQsbUZBQThFO0FBQzlFLGlGQUE0RTtBQVE1RTtJQVdFLDhCQUNVLFdBQTJCLEVBQzNCLFFBQXFCLEVBQ3JCLEVBQWUsRUFDZixNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsV0FBMkIsRUFDM0IsVUFBK0I7UUFOL0IsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzNCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzNCLGVBQVUsR0FBVixVQUFVLENBQXFCO1FBVmpDLFVBQUssR0FBb0IsSUFBSSxDQUFDO1FBQzlCLFVBQUssR0FBb0IsSUFBSSxDQUFDO1FBQzlCLFlBQU8sR0FBb0IsSUFBSSxDQUFDO1FBK0d4QyxlQUFVLEdBQUc7WUFDWCxNQUFNLEVBQUUsRUFBRTtZQUNWLE9BQU8sRUFBRSxFQUFFO1lBQ1gsT0FBTyxFQUFFLEVBQUU7WUFDWCxTQUFTLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNuQixNQUFNLEVBQUU7Z0JBQ04sVUFBVSxFQUFFLDZCQUE2QjtnQkFDekMsV0FBVyxFQUFFLGtEQUFrRDthQUNoRTtZQUNELE9BQU8sRUFBRTtnQkFDUCxVQUFVLEVBQUUsOEJBQThCO2dCQUMxQyxXQUFXLEVBQUUsbURBQW1EO2dCQUNoRSxjQUFjLEVBQUcsMENBQTBDO2dCQUMzRCxXQUFXLEVBQU0sK0NBQStDO2FBQ2pFO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLFdBQVcsRUFBRSxrREFBa0Q7Z0JBQy9ELGVBQWUsRUFBRSx5QkFBeUI7YUFDM0M7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsV0FBVyxFQUFFLHNEQUFzRDthQUNwRTtTQUNGLENBQUM7SUEvSEUsQ0FBQztJQUVMLHVDQUFRLEdBQVI7UUFBQSxpQkEwQ0M7UUF4Q0MsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUMsQ0FBQztRQUNELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsRUFBRSxFQUFFLElBQUk7WUFDUixJQUFJLEVBQUUsSUFBSTtZQUNWLEtBQUssRUFBRSxJQUFJO1lBQ1gsS0FBSyxFQUFFLElBQUk7WUFDWCxPQUFPLEVBQUUsSUFBSTtZQUNiLFNBQVMsRUFBRSxDQUFDO1NBQ2IsQ0FBQztRQUNGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQzVDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQkFDdEMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2hCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixJQUFJLE1BQU0sR0FBRztvQkFDWCxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztpQkFDaEQsQ0FBQztnQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3RDLFVBQUEsUUFBUTtvQkFDTixLQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDaEIsQ0FBQyxFQUNELFVBQUEsS0FBSztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7UUFDSCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQyxDQUFDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFTyxxQ0FBTSxHQUFkO1FBRUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUc7Z0JBQ2IsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDbkIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDdkIsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDekIsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDekIsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTztnQkFDN0IsU0FBUyxFQUFFLENBQUM7YUFDYixDQUFDO1lBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNqRCxDQUFDO0lBQ0gsQ0FBQztJQUVPLHdDQUFTLEdBQWpCO1FBQUEsaUJBZUM7UUFiQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzFCLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDOUQsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUMsaURBQXFCLEVBQUUsQ0FBQyxDQUFDO1lBQ2hILE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUFDLG1EQUFzQixFQUFFLENBQUMsQ0FBQztZQUNsRSxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQzlDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZO2FBQ3JCLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUM7UUFDdkQsMkRBQTJEO0lBQzdELENBQUM7SUFFTyw2Q0FBYyxHQUF0QixVQUF1QixPQUFPLEVBQUUsSUFBVTtRQUV4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUM3QixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQzdELElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEQsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBNkJPLDRDQUFhLEdBQXJCO1FBQUEsaUJBa0NDO1FBakNDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxDQUFDO1lBQ1QsQ0FBQztRQUNILENBQUM7UUFDRCxLQUFLO1FBQ0wsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUMxQyxJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPO1NBQ3hCLENBQUM7UUFDRixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ2xELElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNyQixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDcEMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDaEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xELENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNiLE1BQU0sQ0FBQztZQUNULENBQUM7WUFDRCxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdkMsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQUEsQ0FBQztJQXRMUyxvQkFBb0I7UUFQaEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLHlEQUF5RCxDQUFDO1lBQ3JGLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsd0RBQXdELENBQUMsQ0FBQztZQUNwRixVQUFVLEVBQUUsQ0FBQyxvQ0FBZ0IsRUFBRSxDQUFDO1lBQ2hDLElBQUksRUFBRSxFQUFFLHFCQUFxQixFQUFFLEVBQUUsRUFBRTtTQUNwQyxDQUFDO3lDQWF1QixnQ0FBYztZQUNqQiwwQkFBVztZQUNqQixtQkFBVztZQUNQLGVBQU07WUFDRSx1QkFBYztZQUNqQixnQ0FBYztZQUNmLDBDQUFtQjtPQWxCOUIsb0JBQW9CLENBdUxoQztJQUFELDJCQUFDO0NBdkxELEFBdUxDLElBQUE7QUF2TFksb0RBQW9CIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL2NvbnRhY3QtaXRlbS9jb250YWN0LWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0sIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IENvbnRhY3RTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2NvbnRhY3Quc2VydmljZSc7XG5pbXBvcnQgeyBpbnZhbGlkTnVtYmVyVmFsaWRhdG9yIH0gZnJvbSBcImFwcC91dGlscy9udW1iZXItdmFsaWRhdG9yLmRpcmVjdGl2ZVwiO1xuaW1wb3J0IHsgaW52YWxpZEVtYWlsVmFsaWRhdG9yIH0gZnJvbSBcImFwcC91dGlscy9lbWFpbC12YWxpZGF0b3IuZGlyZWN0aXZlXCI7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb250YWN0LWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvY29udGFjdC1pdGVtL2NvbnRhY3QtaXRlbS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jb250YWN0LWl0ZW0vY29udGFjdC1pdGVtLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7ICdbQHJvdXRlclRyYW5zaXRpb25dJzogJycgfVxufSlcbmV4cG9ydCBjbGFzcyBDb250YWN0SXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgYWN0aW9uO1xuICBwcml2YXRlIGR0Rm9ybTogRm9ybUdyb3VwO1xuICBwcml2YXRlIG5hbWU6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBlbWFpbDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBwaG9uZTogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBhZGRyZXNzOiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGNvbnRhY3RTZXJ2OiBDb250YWN0U2VydmljZSxcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnY6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gICAgbGV0IHVzZXJLZXkgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICBpZiAoIXVzZXJLZXkpIHtcbiAgICAgIHRoaXMudXNlclNlcnYubG9nb3V0KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMudXNlcklkID0gdXNlcktleS51c2VyX2luZm8uaWQ7XG4gICAgICB0aGlzLnJlc3RJZCA9IHVzZXJLZXkucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgfVxuICAgIHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgdGhpcy5teW1vZGVsID0ge1xuICAgICAgaWQ6IG51bGwsXG4gICAgICBuYW1lOiBudWxsLFxuICAgICAgZW1haWw6IG51bGwsXG4gICAgICBwaG9uZTogbnVsbCxcbiAgICAgIGFkZHJlc3M6IG51bGwsXG4gICAgICBpc19kZWxldGU6IDBcbiAgICB9O1xuICAgIGlmICh0aGlzLnJvdXRlci51cmwuaW5kZXhPZignL2NvbnRhY3QvZWRpdCcpID09IDApIHtcbiAgICAgIHRoaXMuYWN0aW9uID0gWydFZGl0JywgJ1VQREFURScsICd1cGRhdGVkJ107XG4gICAgICBpZiAodGhpcy5zdG9yYWdlU2Vydi5zY29wZSkge1xuICAgICAgICB0aGlzLm15bW9kZWwgPSB0aGlzLnN0b3JhZ2VTZXJ2LnNjb3BlO1xuICAgICAgICB0aGlzLnNldFZhcigpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICAgICAnaWQnOiB0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90LnBhcmFtc1snaWQnXVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmNvbnRhY3RTZXJ2LmdldENvbnRhY3QocGFyYW1zKS50aGVuKFxuICAgICAgICAgIHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgIHRoaXMubXltb2RlbCA9IHJlc3BvbnNlLmNvbnRhY3RzO1xuICAgICAgICAgICAgdGhpcy5zZXRWYXIoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKHRoaXMucm91dGVyLnVybC5pbmRleE9mKCcvY29udGFjdC9jcmVhdGUnKSA9PSAwKSB7XG4gICAgICB0aGlzLmFjdGlvbiA9IFsnQWRkJywgJ0FERCcsICdhZGRlZCddO1xuICAgICAgdGhpcy5zdG9yYWdlU2Vydi5zZXRTY29wZShudWxsKTtcbiAgICB9XG4gICAgY29uc29sZS5sb2coJ2luaXQgY29udGFjdCBpdGVtIGNvbXBvbmVudCcpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRWYXIoKTogdm9pZCB7XG5cbiAgICBpZiAodGhpcy5teW1vZGVsICE9IG51bGwpIHtcbiAgICAgIHRoaXMubXltb2RlbCA9IHtcbiAgICAgICAgaWQ6IHRoaXMubXltb2RlbC5pZCxcbiAgICAgICAgbmFtZTogdGhpcy5teW1vZGVsLm5hbWUsXG4gICAgICAgIGVtYWlsOiB0aGlzLm15bW9kZWwuZW1haWwsXG4gICAgICAgIHBob25lOiB0aGlzLm15bW9kZWwucGhvbmUsXG4gICAgICAgIGFkZHJlc3M6IHRoaXMubXltb2RlbC5hZGRyZXNzLFxuICAgICAgICBpc19kZWxldGU6IDBcbiAgICAgIH07XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snbmFtZSddLnNldFZhbHVlKHRoaXMubXltb2RlbC5uYW1lKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydlbWFpbCddLnNldFZhbHVlKHRoaXMubXltb2RlbC5lbWFpbCk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1sncGhvbmUnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwucGhvbmUpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2FkZHJlc3MnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuYWRkcmVzcyk7XG4gICAgICB0aGlzLm5hbWUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgICAgdGhpcy5lbWFpbCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydlbWFpbCddO1xuICAgICAgdGhpcy5waG9uZSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydwaG9uZSddO1xuICAgICAgdGhpcy5hZGRyZXNzID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2FkZHJlc3MnXTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGJ1aWxkRm9ybSgpOiB2b2lkIHtcblxuICAgIHRoaXMuZHRGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAnbmFtZSc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NildXSxcbiAgICAgICdlbWFpbCc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDYpLCBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTYpLGludmFsaWRFbWFpbFZhbGlkYXRvcigpXV0sXG4gICAgICAncGhvbmUnOiBbJycsIFtWYWxpZGF0b3JzLm1heExlbmd0aCgzMiksaW52YWxpZE51bWJlclZhbGlkYXRvcigpXV0sXG4gICAgICAnYWRkcmVzcyc6IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDE1MzYpXV1cbiAgICB9KTtcbiAgICB0aGlzLm5hbWUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgIHRoaXMuZW1haWwgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZW1haWwnXTtcbiAgICB0aGlzLnBob25lID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ3Bob25lJ107XG4gICAgdGhpcy5hZGRyZXNzID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2FkZHJlc3MnXTtcbiAgICB0aGlzLmR0Rm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGZhbHNlLCBkYXRhKSk7XG4gICAgLy90aGlzLm9uVmFsdWVDaGFuZ2VkKCk7IC8vIChyZSlzZXQgdmFsaWRhdGlvbiBtZXNzYWdlcyBub3dcbiAgfVxuXG4gIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQodW5EaXJ0eSwgZGF0YT86IGFueSkge1xuXG4gICAgaWYgKCF0aGlzLmR0Rm9ybSkgeyByZXR1cm47IH1cbiAgICBjb25zdCBmb3JtID0gdGhpcy5kdEZvcm07XG4gICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIC8vIGNsZWFyIHByZXZpb3VzIGVycm9yIG1lc3NhZ2UgKGlmIGFueSlcbiAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSBbXTtcbiAgICAgIGNvbnN0IGNvbnRyb2wgPSBmb3JtLmdldChmaWVsZCk7XG4gICAgICBpZiAoY29udHJvbCAmJiAoY29udHJvbC5kaXJ0eSB8fCB1bkRpcnR5KSAmJiBjb250cm9sLmludmFsaWQpIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZXMgPSB0aGlzLnZhbGlkYXRpb25NZXNzYWdlc1tmaWVsZF07XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIGNvbnRyb2wuZXJyb3JzKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5wdXNoKG1lc3NhZ2VzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZm9ybUVycm9ycyA9IHtcbiAgICAnbmFtZSc6IFtdLFxuICAgICdlbWFpbCc6IFtdLFxuICAgICdwaG9uZSc6IFtdLFxuICAgICdhZGRyZXNzJzogW11cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ25hbWUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIG5hbWUgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIG5hbWUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMjU2IGNoYXJhY3RlcnMuJ1xuICAgIH0sXG4gICAgJ2VtYWlsJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBlbWFpbCBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgZW1haWwgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMjU2IGNoYXJhY3RlcnMuJyxcbiAgICAgICdpbnZhbGlkRW1haWwnOiAgJ1RoZSBlbWFpbCBtdXN0IGJlIGEgdmFsaWQgZW1haWwgYWRkcmVzcy4nLFxuICAgICAgJ21pbmxlbmd0aCc6ICAgICAnVGhlIGVtYWlsIG11c3QgYmUgYXQgbGVhc3QgNiBjaGFyYWN0ZXJzIGxvbmcuJ1xuICAgIH0sXG4gICAgJ3Bob25lJzoge1xuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgcGhvbmUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMzIgY2hhcmFjdGVycy4nLFxuICAgICAgJ2ludmFsaWROdW1iZXInOiAnaGUgcGhvbmUgbXVzdCBiZSBudW1iZXInXG4gICAgfSxcbiAgICAnYWRkcmVzcyc6IHtcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIGFkZHJlc3MgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTUzNiBjaGFyYWN0ZXJzLidcbiAgICB9XG4gIH07XG5cbiAgcHJpdmF0ZSBkb1NhdmVDb250YWN0KCkge1xuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQodHJ1ZSk7XG4gICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIGlmICh0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH1cbiAgICAvL2R0b1xuICAgIGlmICh0aGlzLmFjdGlvblswXSA9PT0gJ0FkZCcpIHtcbiAgICAgIHRoaXMubXltb2RlbC5pZCA9IC0xO1xuICAgIH1cbiAgICB0aGlzLm15bW9kZWwubmFtZSA9IHRoaXMubmFtZS52YWx1ZTtcbiAgICB0aGlzLm15bW9kZWwuZW1haWwgPSB0aGlzLmVtYWlsLnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5waG9uZSA9IHRoaXMucGhvbmUudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmFkZHJlc3MgPSB0aGlzLmFkZHJlc3MudmFsdWU7XG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICd1c2VyX2lkJzogdGhpcy51c2VySWQsXG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgJ2NvbnRhY3QnOiB0aGlzLm15bW9kZWxcbiAgICB9O1xuICAgIHRoaXMuY29udGFjdFNlcnYudXBkYXRlQ29udGFjdChwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgbGV0IGhhckVycm9yID0gZmFsc2U7XG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHJlc3BvbnNlLmVycm9ycykge1xuICAgICAgICBoYXJFcnJvciA9IHRydWU7XG4gICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSByZXNwb25zZS5lcnJvcnNbZmllbGRdO1xuICAgICAgfVxuICAgICAgaWYgKGhhckVycm9yKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCdDb250YWN0IGhhcyBiZWVuICcgKyB0aGlzLmFjdGlvblsyXSk7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCdjb250YWN0Jyk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9O1xufVxuIl19
