"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Moiz.Kachwala on 02-06-2016.
 */
var core_1 = require("@angular/core");
var hero_service_1 = require("../../services/hero.service");
var server_service_1 = require("../../services/server.service");
// import { Router } from '@angular/router';
var HeroesComponent = (function () {
    function HeroesComponent(
        // private router: Router,
        heroService, serverService) {
        this.heroService = heroService;
        this.serverService = serverService;
    }
    HeroesComponent.prototype.getHeroes = function () {
        var _this = this;
        // this.heroService.getHeroes().then(heroes => this.heroes = heroes);
        var action = 'test2';
        var param = {
            'id': 123
        };
        this.serverService.doPost(action, param)
            .then(
        // hero  => this.heroes.push(hero),
        function (hero) { return console.log('-------getHeroes-------', hero); }, function (error) { return _this.error = error; });
    };
    HeroesComponent.prototype.ngOnInit = function () {
        this.getHeroes();
    };
    HeroesComponent.prototype.onSelect = function (hero) { this.selectedHero = hero; };
    HeroesComponent.prototype.gotoDetail = function () {
        // this.router.navigate(['/detail', this.selectedHero._id]);
    };
    HeroesComponent.prototype.addHero = function () {
        var _this = this;
        this.selectedHero = null;
        // this.router.navigate(['/detail', 'new']);
        var action = 'test2';
        var param = {
            'id': 123
        };
        this.serverService.doPost(action, param)
            .then(
        // hero  => this.heroes.push(hero),
        function (hero) { return console.log('-------getHeroes-------', hero); }, function (error) { return _this.error = error; });
    };
    HeroesComponent.prototype.deleteHero = function (hero, event) {
        var _this = this;
        event.stopPropagation();
        this.heroService
            .delete(hero)
            .then(function (res) {
            _this.heroes = _this.heroes.filter(function (h) { return h !== hero; });
            if (_this.selectedHero === hero) {
                _this.selectedHero = null;
            }
        })
            .catch(function (error) { return _this.error = error; });
    };
    HeroesComponent = __decorate([
        core_1.Component({
            selector: 'my-heroes',
            templateUrl: './admin/app/components/heroes/heroes.component.html'
        }),
        __metadata("design:paramtypes", [hero_service_1.HeroService,
            server_service_1.ServerService])
    ], HeroesComponent);
    return HeroesComponent;
}());
exports.HeroesComponent = HeroesComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2hlcm9lcy9oZXJvZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0dBRUc7QUFDSCxzQ0FBZ0Q7QUFDaEQsNERBQXdEO0FBQ3hELGdFQUE0RDtBQUU1RCw0Q0FBNEM7QUFPNUM7SUFNSTtRQUNJLDBCQUEwQjtRQUNsQixXQUF3QixFQUN4QixhQUE0QjtRQUQ1QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtJQUFJLENBQUM7SUFDN0MsbUNBQVMsR0FBVDtRQUFBLGlCQVlDO1FBWEcscUVBQXFFO1FBQ3JFLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztRQUNyQixJQUFJLEtBQUssR0FBRztZQUNSLElBQUksRUFBRSxHQUFHO1NBQ1osQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUM7YUFDNUIsSUFBSTtRQUNILG1DQUFtQztRQUNuQyxVQUFBLElBQUksSUFBSyxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLEVBQTVDLENBQTRDLEVBQ3JELFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssR0FBUSxLQUFLLEVBQXZCLENBQXVCLENBQUMsQ0FBQztJQUVwRCxDQUFDO0lBQ0Qsa0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBQ0Qsa0NBQVEsR0FBUixVQUFTLElBQVUsSUFBSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7SUFFbEQsb0NBQVUsR0FBVjtRQUNJLDREQUE0RDtJQUNoRSxDQUFDO0lBRUQsaUNBQU8sR0FBUDtRQUFBLGlCQWFDO1FBWkcsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsNENBQTRDO1FBRTNDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztRQUN0QixJQUFJLEtBQUssR0FBRztZQUNSLElBQUksRUFBRSxHQUFHO1NBQ1osQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUM7YUFDNUIsSUFBSTtRQUNILG1DQUFtQztRQUNuQyxVQUFBLElBQUksSUFBSyxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLEVBQTVDLENBQTRDLEVBQ3JELFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssR0FBUSxLQUFLLEVBQXZCLENBQXVCLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsb0NBQVUsR0FBVixVQUFXLElBQVUsRUFBRSxLQUFVO1FBQWpDLGlCQVNDO1FBUkcsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXO2FBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQzthQUNaLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDTCxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLElBQUksRUFBVixDQUFVLENBQUMsQ0FBQztZQUNsRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQUMsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFBQyxDQUFDO1FBQ2pFLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxFQUFsQixDQUFrQixDQUFDLENBQUM7SUFDNUMsQ0FBQztJQXhEUSxlQUFlO1FBTDNCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsV0FBVztZQUNyQixXQUFXLEVBQUUscURBQXFEO1NBQ3JFLENBQUM7eUNBVTJCLDBCQUFXO1lBQ1QsOEJBQWE7T0FUL0IsZUFBZSxDQXlEM0I7SUFBRCxzQkFBQztDQXpERCxBQXlEQyxJQUFBO0FBekRZLDBDQUFlIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL2hlcm9lcy9oZXJvZXMuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IE1vaXouS2FjaHdhbGEgb24gMDItMDYtMjAxNi5cbiAqL1xuaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0hlcm9TZXJ2aWNlfSBmcm9tIFwiLi4vLi4vc2VydmljZXMvaGVyby5zZXJ2aWNlXCI7XG5pbXBvcnQge1NlcnZlclNlcnZpY2V9IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy9zZXJ2ZXIuc2VydmljZVwiO1xuaW1wb3J0IHtIZXJvfSBmcm9tIFwiLi4vLi4vbW9kZWxzL2hlcm9cIjtcbi8vIGltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnbXktaGVyb2VzJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vYWRtaW4vYXBwL2NvbXBvbmVudHMvaGVyb2VzL2hlcm9lcy5jb21wb25lbnQuaHRtbCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBIZXJvZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgaGVyb2VzOiBIZXJvW107XG4gICAgc2VsZWN0ZWRIZXJvOiBIZXJvO1xuICAgIGVycm9yOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgLy8gcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICAgICAgcHJpdmF0ZSBoZXJvU2VydmljZTogSGVyb1NlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgc2VydmVyU2VydmljZTogU2VydmVyU2VydmljZSkgeyB9XG4gICAgZ2V0SGVyb2VzKCkge1xuICAgICAgICAvLyB0aGlzLmhlcm9TZXJ2aWNlLmdldEhlcm9lcygpLnRoZW4oaGVyb2VzID0+IHRoaXMuaGVyb2VzID0gaGVyb2VzKTtcbiAgICAgICAgdmFyIGFjdGlvbiA9ICd0ZXN0Mic7XG4gICAgICAgIHZhciBwYXJhbSA9IHtcbiAgICAgICAgICAgICdpZCc6IDEyM1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNlcnZlclNlcnZpY2UuZG9Qb3N0KGFjdGlvbiwgcGFyYW0pXG4gICAgICAgICAgICAgICAgICAgLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICAvLyBoZXJvICA9PiB0aGlzLmhlcm9lcy5wdXNoKGhlcm8pLFxuICAgICAgICAgICAgICAgICAgICAgaGVybyAgPT4gY29uc29sZS5sb2coJy0tLS0tLS1nZXRIZXJvZXMtLS0tLS0tJywgaGVybyksXG4gICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiAgdGhpcy5lcnJvciA9IDxhbnk+ZXJyb3IpO1xuXG4gICAgfVxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmdldEhlcm9lcygpO1xuICAgIH1cbiAgICBvblNlbGVjdChoZXJvOiBIZXJvKSB7IHRoaXMuc2VsZWN0ZWRIZXJvID0gaGVybzsgfVxuXG4gICAgZ290b0RldGFpbCgpIHtcbiAgICAgICAgLy8gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvZGV0YWlsJywgdGhpcy5zZWxlY3RlZEhlcm8uX2lkXSk7XG4gICAgfVxuXG4gICAgYWRkSGVybygpIHtcbiAgICAgICAgdGhpcy5zZWxlY3RlZEhlcm8gPSBudWxsO1xuICAgICAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9kZXRhaWwnLCAnbmV3J10pO1xuXG4gICAgICAgICB2YXIgYWN0aW9uID0gJ3Rlc3QyJztcbiAgICAgICAgdmFyIHBhcmFtID0ge1xuICAgICAgICAgICAgJ2lkJzogMTIzXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2VydmVyU2VydmljZS5kb1Bvc3QoYWN0aW9uLCBwYXJhbSlcbiAgICAgICAgICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIC8vIGhlcm8gID0+IHRoaXMuaGVyb2VzLnB1c2goaGVybyksXG4gICAgICAgICAgICAgICAgICAgICBoZXJvICA9PiBjb25zb2xlLmxvZygnLS0tLS0tLWdldEhlcm9lcy0tLS0tLS0nLCBoZXJvKSxcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmVycm9yID0gPGFueT5lcnJvcik7XG4gICAgfVxuXG4gICAgZGVsZXRlSGVybyhoZXJvOiBIZXJvLCBldmVudDogYW55KSB7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICB0aGlzLmhlcm9TZXJ2aWNlXG4gICAgICAgICAgICAuZGVsZXRlKGhlcm8pXG4gICAgICAgICAgICAudGhlbihyZXMgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuaGVyb2VzID0gdGhpcy5oZXJvZXMuZmlsdGVyKGggPT4gaCAhPT0gaGVybyk7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRIZXJvID09PSBoZXJvKSB7IHRoaXMuc2VsZWN0ZWRIZXJvID0gbnVsbDsgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB0aGlzLmVycm9yID0gZXJyb3IpO1xuICAgIH1cbn1cbiJdfQ==
