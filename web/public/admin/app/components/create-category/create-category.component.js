"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var category_service_1 = require("app/services/category.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var CreateCategoryComponent = (function () {
    function CreateCategoryComponent(fb, categoryService, StorageService, authServ, notif, router, route) {
        this.fb = fb;
        this.categoryService = categoryService;
        this.StorageService = StorageService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.formErrors = {
            'name': [],
            'description': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            },
            'description': {
                'maxlength': 'The name must be at less 512 characters long.'
            }
        };
        /*upload file*/
        this.sactiveColor = 'green';
        this.baseColor = '#ccc';
        this.overlayColor = 'rgba(255,255,255,0.5)';
        this.dragging = false;
        this.loaded = false;
        this.imageLoaded = false;
        this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
        this.flgImageChoose = false;
        this.iconColor = '';
        this.borderColor = '';
        this.activeColor = '';
        this.hiddenImage = false;
    }
    CreateCategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            _this.isCheckRadio = '1';
            _this.createCategoryForm.controls['nameRadio'].setValue(1);
            if (_this.id != null) {
                _this.categoryItem = _this.StorageService.getScope();
                if (_this.categoryItem == false) {
                    _this.fetchAllCategories(_this.restId, _this.id);
                }
                else {
                    _this.createCategoryForm.controls['name'].setValue(_this.categoryItem.name);
                    _this.createCategoryForm.controls['description'].setValue(_this.categoryItem.description);
                    _this.createCategoryForm.controls['nameRadio'].setValue(_this.categoryItem.is_active);
                    _this.name = _this.createCategoryForm.controls['name'];
                    _this.nameRadio = _this.createCategoryForm.controls['nameRadio'];
                    _this.description = _this.createCategoryForm.controls['description'];
                    _this.isCheckRadio = _this.categoryItem.is_active;
                    _this.imageSrc = _this.categoryItem.thumb;
                    if (_this.imageSrc == null) {
                        _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                    }
                }
            }
        });
    };
    CreateCategoryComponent.prototype.checkFlg = function (value) {
        if (this.isCheckRadio == undefined || this.isCheckRadio == null) {
            this.isCheckRadio = '1';
        }
        if (value == this.isCheckRadio) {
            return true;
        }
    };
    CreateCategoryComponent.prototype.changenameRadio = function (value) {
        this.isCheckRadio = value;
    };
    CreateCategoryComponent.prototype.fetchAllCategories = function (restId, id) {
        var _this = this;
        this.categoryService.getCategories(restId, id).then(function (response) {
            if (response.categories != null && response.categories.length > 0) {
                _this.categoryItem = response.categories[0];
                _this.createCategoryForm.controls['name'].setValue(_this.categoryItem.name);
                _this.createCategoryForm.controls['description'].setValue(_this.categoryItem.description);
                _this.createCategoryForm.controls['nameRadio'].setValue(_this.categoryItem.is_active);
                _this.name = _this.createCategoryForm.controls['name'];
                _this.nameRadio = _this.createCategoryForm.controls['nameRadio'];
                _this.description = _this.createCategoryForm.controls['description'];
                _this.isCheckRadio = _this.categoryItem.is_active;
                _this.imageSrc = _this.categoryItem.thumb;
                if (_this.imageSrc == null) {
                    _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                }
            }
            else {
                _this.notif.error('Category had delete');
                _this.router.navigateByUrl('category/list');
            }
        }, function (error) {
            console.log(error);
        });
    };
    CreateCategoryComponent.prototype.buildForm = function () {
        var _this = this;
        this.createCategoryForm = this.fb.group({
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ], 'nameRadio': [],
            'description': []
        });
        this.name = this.createCategoryForm.controls['name'];
        this.nameRadio = this.createCategoryForm.controls['nameRadio'];
        this.description = this.createCategoryForm.controls['description'];
        this.createCategoryForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    CreateCategoryComponent.prototype.onValueChanged = function (data) {
        if (!this.createCategoryForm) {
            return;
        }
        var form = this.createCategoryForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    CreateCategoryComponent.prototype.handleDragEnter = function () {
        this.dragging = true;
    };
    CreateCategoryComponent.prototype.handleDragLeave = function () {
        this.dragging = false;
    };
    CreateCategoryComponent.prototype.handleDrop = function (e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    };
    CreateCategoryComponent.prototype.handleImageLoad = function () {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if (this.imageLoaded = true) {
            this.hiddenImage = true;
        }
    };
    CreateCategoryComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        this.loaded = false;
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose = true;
    };
    CreateCategoryComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    };
    CreateCategoryComponent.prototype._setActive = function () {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    };
    CreateCategoryComponent.prototype._setInactive = function () {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    };
    CreateCategoryComponent.prototype.createCategory = function () {
        var _this = this;
        var id = '-1';
        if (this.id != null) {
            id = this.id;
        }
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.categoryObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'category': {
                'id': id,
                'name': this.createCategoryForm.value.name,
                'thumb': '',
                'description': this.createCategoryForm.value.description,
                'is_active': this.isCheckRadio,
                'is_delete': '0'
            }
        };
        this.categoryService.createCategory(this.categoryObject).then(
        //          response  => { 
        //          	this.categories = response;
        //          	console.log(response, this.categories); 
        //          },
        //  error => {console.log(error)
        function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
        // });
    };
    CreateCategoryComponent.prototype.processResult = function (response) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            var imagePost = '';
            if (this.flgImageChoose == true) {
                imagePost = this.imageSrc;
            }
            this.categories = response;
            var categoryUpload = {
                'id': this.categories.category_id,
                'thumb': imagePost
            };
            this.categoryService.categoryUploadFile(categoryUpload).then(function (response) {
                _this.notif.success('New Category has been added');
                _this.router.navigateByUrl('category/list');
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.name) {
                    this.formErrors['name'] = this.error.name;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    CreateCategoryComponent.prototype.failedCreate = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    CreateCategoryComponent = __decorate([
        core_1.Component({
            selector: 'create-category',
            templateUrl: utils_1.default.getView('app/components/create-category/create-category.component.html'),
            styleUrls: [utils_1.default.getView('app/components/create-category/create-category.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': ''
            }
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            category_service_1.CategoryService,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router, router_2.ActivatedRoute])
    ], CreateCategoryComponent);
    return CreateCategoryComponent;
}());
exports.CreateCategoryComponent = CreateCategoryComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NyZWF0ZS1jYXRlZ29yeS9jcmVhdGUtY2F0ZWdvcnkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHdDQUE0RjtBQUM1Rix5Q0FBb0M7QUFHcEMsNkRBQTJEO0FBQzNELGtFQUFnRTtBQUNoRSwwREFBeUQ7QUFDekQsMkNBQThDO0FBQzlDLDBFQUF3RTtBQUN4RSwwQ0FBeUM7QUFDekMsMENBQWlEO0FBQ2pELGdFQUE4RDtBQVM5RDtJQVlFLGlDQUFvQixFQUFlLEVBQzNCLGVBQWdDLEVBQ2hDLGNBQThCLEVBQzlCLFFBQXFCLEVBQ3JCLEtBQTBCLEVBQ3pCLE1BQWMsRUFBUyxLQUFxQjtRQUxqQyxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQzNCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFzR3ZELGVBQVUsR0FBRztZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsYUFBYSxFQUFDLEVBQUU7U0FDakIsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQU8sdUJBQXVCO2dCQUN4QyxXQUFXLEVBQU0sK0NBQStDO2FBQ2pFO1lBQ0QsYUFBYSxFQUFFO2dCQUNiLFdBQVcsRUFBTSwrQ0FBK0M7YUFDakU7U0FDRixDQUFDO1FBRUYsZUFBZTtRQUNSLGlCQUFZLEdBQVcsT0FBTyxDQUFDO1FBQzVCLGNBQVMsR0FBVyxNQUFNLENBQUM7UUFDM0IsaUJBQVksR0FBVyx1QkFBdUIsQ0FBQztRQUUvQyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLFdBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFDN0IsYUFBUSxHQUFXLGlFQUFpRSxDQUFDO1FBQ3JGLG1CQUFjLEdBQVcsS0FBSyxDQUFDO1FBQy9CLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFDdEIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBUyxLQUFLLENBQUM7SUFqSXBDLENBQUM7SUFFRCwwQ0FBUSxHQUFSO1FBQUEsaUJBNEJDO1FBM0JFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQzVDLEtBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFELEVBQUUsQ0FBQSxDQUFDLEtBQUksQ0FBQyxFQUFFLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDaEIsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNuRCxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssQ0FBQyxDQUFBLENBQUM7b0JBQzdCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDL0MsQ0FBQztnQkFBQSxJQUFJLENBQUEsQ0FBQztvQkFDRixLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxRSxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN4RixLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUNwRixLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3JELEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDL0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUNuRSxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO29CQUNoRCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO29CQUN4QyxFQUFFLENBQUEsQ0FBRSxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7d0JBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsaUVBQWlFLENBQUM7b0JBQ3JGLENBQUM7Z0JBQ0wsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCwwQ0FBUSxHQUFSLFVBQVMsS0FBSztRQUNWLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUUsU0FBUyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUMxRCxJQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQztRQUMxQixDQUFDO1FBQ0QsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQSxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztJQUNMLENBQUM7SUFDRCxpREFBZSxHQUFmLFVBQWdCLEtBQUs7UUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRSxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUNPLG9EQUFrQixHQUExQixVQUEyQixNQUFNLEVBQUMsRUFBRTtRQUFwQyxpQkF1QkU7UUF0QkEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FDakMsVUFBQSxRQUFRO1lBQ04sRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFVBQVUsSUFBRSxJQUFJLElBQUksUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDN0QsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxRSxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN2RixLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNyRixLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3JELEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDL0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNuRSxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUNoRCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO2dCQUN4QyxFQUFFLENBQUEsQ0FBRSxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7b0JBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsaUVBQWlFLENBQUM7Z0JBQ3JGLENBQUM7WUFDRixDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0osS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDeEMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDN0MsQ0FBQztRQUNILENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNJLDJDQUFTLEdBQWpCO1FBQUEsaUJBaUJHO1FBaEJDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUN0QyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ1Qsa0JBQVUsQ0FBQyxRQUFRO29CQUNuQixrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUM7aUJBQzFCO2FBRUYsRUFBQyxXQUFXLEVBQUMsRUFBRTtZQUNoQixhQUFhLEVBQUMsRUFBRTtTQUNqQixDQUFDLENBQUM7UUFDQSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWTthQUNqQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUF6QixDQUF5QixDQUFDLENBQUM7UUFFaEQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsa0NBQWtDO0lBQzNELENBQUM7SUFDUSxnREFBYyxHQUF0QixVQUF1QixJQUFVO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFDekMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO1FBRXJDLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRWhDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEQsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBbUNDLGlEQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQsaURBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRCw0Q0FBVSxHQUFWLFVBQVcsQ0FBQztRQUNSLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELGlEQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLENBQUM7SUFDQyxDQUFDO0lBRUQsbURBQWlCLEdBQWpCLFVBQWtCLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVwQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFFLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQscURBQW1CLEdBQW5CLFVBQW9CLENBQUM7UUFDakIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELDRDQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3BDLENBQUM7SUFDTCxDQUFDO0lBQ0wsZ0RBQWMsR0FBZDtRQUFBLGlCQWlDQztRQWhDQyxJQUFJLEVBQUUsR0FBSSxJQUFJLENBQUM7UUFDZixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDaEIsRUFBRSxHQUFJLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDaEIsQ0FBQztRQUNELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNsQyx3Q0FBd0M7WUFDdkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDbkMsTUFBTSxDQUFDO1lBQ1QsQ0FBQztRQUNKLENBQUM7UUFDSCxJQUFJLENBQUMsY0FBYyxHQUFHO1lBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBQyxJQUFJLENBQUMsTUFBTTtZQUNyQixVQUFVLEVBQUM7Z0JBQ1QsSUFBSSxFQUFDLEVBQUU7Z0JBQ1AsTUFBTSxFQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsSUFBSTtnQkFDekMsT0FBTyxFQUFDLEVBQUU7Z0JBQ1YsYUFBYSxFQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsV0FBVztnQkFDdkQsV0FBVyxFQUFDLElBQUksQ0FBQyxZQUFZO2dCQUM3QixXQUFXLEVBQUMsR0FBRzthQUNoQjtTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSTtRQUNwRCwyQkFBMkI7UUFDM0Isd0NBQXdDO1FBQ3hDLHFEQUFxRDtRQUNyRCxjQUFjO1FBQ25CLGdDQUFnQztRQUN2QixVQUFBLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEVBQTVCLENBQTRCLEVBQ3BDLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLENBQUMsQ0FBQztRQUN2RCxNQUFNO0lBRWIsQ0FBQztJQUNPLCtDQUFhLEdBQXJCLFVBQXNCLFFBQVE7UUFBOUIsaUJBa0NHO1FBaENDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksU0FBUyxJQUFJLFFBQVEsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMvRCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFDbkIsRUFBRSxDQUFBLENBQUUsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUMvQixTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUM1QixDQUFDO1lBQ0EsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7WUFDM0IsSUFBSSxjQUFjLEdBQUc7Z0JBQ2xCLElBQUksRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVc7Z0JBQ2hDLE9BQU8sRUFBQyxTQUFTO2FBQ25CLENBQUE7WUFDRixJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FDNUMsVUFBQSxRQUFRO2dCQUNQLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLENBQUM7Z0JBQ2xELEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzVDLENBQUMsRUFDZCxVQUFBLEtBQUs7Z0JBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUU3QixDQUFDLENBQUMsQ0FBQztRQUNOLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUM3QixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztnQkFDNUMsQ0FBQztZQUNILENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixxQkFBSSxDQUNGLGNBQWMsRUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUNiLE9BQU8sQ0FDUixDQUFDLEtBQUssQ0FBQyxxQkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JCLENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUNPLDhDQUFZLEdBQXBCLFVBQXNCLEdBQUc7UUFDdkIsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLDBCQUEwQjtRQUM1QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksa0JBQWtCLENBQUMsQ0FBQyxDQUFDO2dCQUNyRCx5QkFBeUI7WUFDM0IsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLDRDQUE0QztnQkFDNUMsR0FBRyxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNsQyx3REFBd0Q7Z0JBQzFELENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUF0U1UsdUJBQXVCO1FBUm5DLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLCtEQUErRCxDQUFDO1lBQzNGLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsOERBQThELENBQUMsQ0FBQztZQUMxRixVQUFVLEVBQUUsQ0FBQyxvQ0FBZ0IsRUFBRSxDQUFDO1lBQ2hDLElBQUksRUFBRSxFQUFDLHFCQUFxQixFQUFFLEVBQUU7YUFDakM7U0FDQSxDQUFDO3lDQWF3QixtQkFBVztZQUNWLGtDQUFlO1lBQ2hCLGdDQUFjO1lBQ3BCLDBCQUFXO1lBQ2QsMENBQW1CO1lBQ2pCLGVBQU0sRUFBZ0IsdUJBQWM7T0FqQjFDLHVCQUF1QixDQXVTbkM7SUFBRCw4QkFBQztDQXZTRCxBQXVTQyxJQUFBO0FBdlNZLDBEQUF1QiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9jcmVhdGUtY2F0ZWdvcnkvY3JlYXRlLWNhdGVnb3J5LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0gLEFic3RyYWN0Q29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjcmVhdGUtY2F0ZWdvcnknLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvY3JlYXRlLWNhdGVnb3J5L2NyZWF0ZS1jYXRlZ29yeS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtY2F0ZWdvcnkvY3JlYXRlLWNhdGVnb3J5LmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ1xufVxufSlcbmV4cG9ydCBjbGFzcyBDcmVhdGVDYXRlZ29yeUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIFxuXHRwcml2YXRlIGlkOiBzdHJpbmc7XG4gIHByaXZhdGUgc3ViOiBhbnk7XG5cdHByaXZhdGUgbXltb2RlbDogYW55O1xuXHRwcml2YXRlIGNyZWF0ZUNhdGVnb3J5Rm9ybTogRm9ybUdyb3VwO1xuXHRwcml2YXRlIG5hbWU6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBuYW1lUmFkaW86IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBkZXNjcmlwdGlvbjogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIHJlc3RJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSBpc0NoZWNrUmFkaW8gOiBzdHJpbmc7XG4gIHByaXZhdGUgY2F0ZWdvcnlJdGVtIDogYW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgcHJpdmF0ZSBjYXRlZ29yeVNlcnZpY2U6IENhdGVnb3J5U2VydmljZSwgXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLCBcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG4gICBwcml2YXRlIHJvdXRlcjogUm91dGVyLHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7IFxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgIGxldCB1c2VySW5mbyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgICB0aGlzLnVzZXJJZCA9IHVzZXJJbmZvLnVzZXJfaW5mby5pZDtcbiAgXHQgdGhpcy5idWlsZEZvcm0oKTtcbiAgICBcdHRoaXMuc3ViID0gdGhpcy5yb3V0ZS5wYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG4gICAgICB0aGlzLmlkID0gcGFyYW1zWydpZCddOyBcbiAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gJzEnO1xuICAgICAgdGhpcy5jcmVhdGVDYXRlZ29yeUZvcm0uY29udHJvbHNbJ25hbWVSYWRpbyddLnNldFZhbHVlKDEpO1xuICAgICAgaWYodGhpcy5pZCE9bnVsbCl7XG4gICAgICAgIHRoaXMuY2F0ZWdvcnlJdGVtID0gdGhpcy5TdG9yYWdlU2VydmljZS5nZXRTY29wZSgpO1xuICAgICAgICBpZih0aGlzLmNhdGVnb3J5SXRlbSA9PSBmYWxzZSl7XG4gICAgICAgICAgdGhpcy5mZXRjaEFsbENhdGVnb3JpZXModGhpcy5yZXN0SWQsdGhpcy5pZCk7ICAgICAgIFxuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlQ2F0ZWdvcnlGb3JtLmNvbnRyb2xzWyduYW1lJ10uc2V0VmFsdWUodGhpcy5jYXRlZ29yeUl0ZW0ubmFtZSk7XG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXS5zZXRWYWx1ZSh0aGlzLmNhdGVnb3J5SXRlbS5kZXNjcmlwdGlvbik7XG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snbmFtZVJhZGlvJ10uc2V0VmFsdWUodGhpcy5jYXRlZ29yeUl0ZW0uaXNfYWN0aXZlKTtcbiAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlQ2F0ZWdvcnlGb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgICAgICAgICB0aGlzLm5hbWVSYWRpbyA9IHRoaXMuY3JlYXRlQ2F0ZWdvcnlGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXTtcbiAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXTtcbiAgICAgICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gdGhpcy5jYXRlZ29yeUl0ZW0uaXNfYWN0aXZlO1xuICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9IHRoaXMuY2F0ZWdvcnlJdGVtLnRodW1iOyAgIFxuICAgICAgICAgICAgaWYoIHRoaXMuaW1hZ2VTcmM9PW51bGwpe1xuICAgICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgY2hlY2tGbGcodmFsdWUpe1xuICAgICAgaWYodGhpcy5pc0NoZWNrUmFkaW89PXVuZGVmaW5lZCB8fCB0aGlzLmlzQ2hlY2tSYWRpbz09bnVsbCl7XG4gICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gJzEnO1xuICAgICAgfVxuICAgICAgaWYodmFsdWU9PXRoaXMuaXNDaGVja1JhZGlvKXtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgfVxuICBjaGFuZ2VuYW1lUmFkaW8odmFsdWUpe1xuICAgIHRoaXMuaXNDaGVja1JhZGlvID12YWx1ZTtcbiAgfVxuICBwcml2YXRlIGZldGNoQWxsQ2F0ZWdvcmllcyhyZXN0SWQsaWQpIHtcbiAgXHRcdHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldENhdGVnb3JpZXMocmVzdElkLGlkKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlLmNhdGVnb3JpZXMhPW51bGwgJiYgcmVzcG9uc2UuY2F0ZWdvcmllcy5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5jYXRlZ29yeUl0ZW0gPSByZXNwb25zZS5jYXRlZ29yaWVzWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICBcdHRoaXMuY3JlYXRlQ2F0ZWdvcnlGb3JtLmNvbnRyb2xzWyduYW1lJ10uc2V0VmFsdWUodGhpcy5jYXRlZ29yeUl0ZW0ubmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXS5zZXRWYWx1ZSh0aGlzLmNhdGVnb3J5SXRlbS5kZXNjcmlwdGlvbik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVDYXRlZ29yeUZvcm0uY29udHJvbHNbJ25hbWVSYWRpbyddLnNldFZhbHVlKHRoaXMuY2F0ZWdvcnlJdGVtLmlzX2FjdGl2ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5uYW1lUmFkaW8gPSB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snbmFtZVJhZGlvJ107XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlc2NyaXB0aW9uID0gdGhpcy5jcmVhdGVDYXRlZ29yeUZvcm0uY29udHJvbHNbJ2Rlc2NyaXB0aW9uJ107XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQ2hlY2tSYWRpbyA9IHRoaXMuY2F0ZWdvcnlJdGVtLmlzX2FjdGl2ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VTcmMgPSB0aGlzLmNhdGVnb3J5SXRlbS50aHVtYjsgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCB0aGlzLmltYWdlU3JjPT1udWxsKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VTcmMgPSAnaHR0cDovL3d3dy5wbGFjZWhvbGQuaXQvMjAweDE1MC9FRkVGRUYvQUFBQUFBJmFtcDt0ZXh0PW5vK2ltYWdlJztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZi5lcnJvcignQ2F0ZWdvcnkgaGFkIGRlbGV0ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ2NhdGVnb3J5L2xpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG5wcml2YXRlIGJ1aWxkRm9ybSgpOiB2b2lkIHtcbiAgICB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgICAgJ25hbWUnOiBbJycsIFtcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICAgIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NiksXG4gICAgICAgIF1cblxuICAgICAgXSwnbmFtZVJhZGlvJzpbXSxcbiAgICAgICdkZXNjcmlwdGlvbic6W11cbiAgICB9KTtcbiAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgICAgIHRoaXMubmFtZVJhZGlvID0gdGhpcy5jcmVhdGVDYXRlZ29yeUZvcm0uY29udHJvbHNbJ25hbWVSYWRpbyddO1xuICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXTtcbiAgICB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGRhdGEpKTtcblxuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTsgLy8gKHJlKXNldCB2YWxpZGF0aW9uIG1lc3NhZ2VzIG5vd1xuICB9XG4gICBwcml2YXRlIG9uVmFsdWVDaGFuZ2VkKGRhdGE/OiBhbnkpIHtcbiAgICBpZiAoIXRoaXMuY3JlYXRlQ2F0ZWdvcnlGb3JtKSB7IHJldHVybjsgfVxuICAgIGNvbnN0IGZvcm0gPSB0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybTtcblxuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuXG4gICAgICBpZiAoY29udHJvbCAmJiBjb250cm9sLmRpcnR5ICYmICFjb250cm9sLnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuZm9ybUVycm9ycyA9IHtcbiAgICAnbmFtZSc6IFtdLFxuICAgICdkZXNjcmlwdGlvbic6W11cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ25hbWUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgbmFtZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIG5hbWUgbXVzdCBiZSBhdCBsZXNzIDI1NiBjaGFyYWN0ZXJzIGxvbmcuJ1xuICAgIH0sXG4gICAgJ2Rlc2NyaXB0aW9uJzoge1xuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIG5hbWUgbXVzdCBiZSBhdCBsZXNzIDUxMiBjaGFyYWN0ZXJzIGxvbmcuJ1xuICAgIH1cbiAgfTtcblxuICAvKnVwbG9hZCBmaWxlKi9cblx0cHJpdmF0ZSBzYWN0aXZlQ29sb3I6IHN0cmluZyA9ICdncmVlbic7XG4gICAgcHJpdmF0ZSBiYXNlQ29sb3I6IHN0cmluZyA9ICcjY2NjJztcbiAgICBwcml2YXRlIG92ZXJsYXlDb2xvcjogc3RyaW5nID0gJ3JnYmEoMjU1LDI1NSwyNTUsMC41KSc7XG4gICAgXG4gICAgcHJpdmF0ZSBkcmFnZ2luZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgbG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpbWFnZUxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaW1hZ2VTcmM6IHN0cmluZyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgIHByaXZhdGUgZmxnSW1hZ2VDaG9vc2U6Ym9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaWNvbkNvbG9yOiBzdHJpbmcgPSAnJztcbiAgICBwcml2YXRlICBib3JkZXJDb2xvcjogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSAgYWN0aXZlQ29sb3I6IHN0cmluZyA9ICcnO1xuICAgIHByaXZhdGUgaGlkZGVuSW1hZ2U6Ym9vbGVhbj1mYWxzZTtcbiAgICBwcml2YXRlIGNhdGVnb3JpZXMgOiBhbnk7XG4gICAgcHJpdmF0ZSBjYXRlZ29yeU9iamVjdCA6IGFueTtcbiAgICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgICBwcml2YXRlIGZpbGVDaG9vc2UgOiBGaWxlO1xuICAgIHByaXZhdGUgZXJyb3I6IGFueTtcbiAgICBoYW5kbGVEcmFnRW50ZXIoKSB7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSB0cnVlO1xuICAgIH1cbiAgICBcbiAgICBoYW5kbGVEcmFnTGVhdmUoKSB7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSBmYWxzZTtcbiAgICB9XG4gICAgXG4gICAgaGFuZGxlRHJvcChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLmhhbmRsZUlucHV0Q2hhbmdlKGUpO1xuICAgIH1cbiAgICBcbiAgICBoYW5kbGVJbWFnZUxvYWQoKSB7XG4gICAgICAgIHRoaXMuaW1hZ2VMb2FkZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLmljb25Db2xvciA9IHRoaXMub3ZlcmxheUNvbG9yO1xuICAgICAgICBpZih0aGlzLmltYWdlTG9hZGVkID0gdHJ1ZSl7XG5cdFx0XHR0aGlzLmhpZGRlbkltYWdlID0gdHJ1ZTtcblx0XHR9XG4gICAgfVxuXG4gICAgaGFuZGxlSW5wdXRDaGFuZ2UoZSkge1xuICAgICAgICB2YXIgZmlsZSA9IGUuZGF0YVRyYW5zZmVyID8gZS5kYXRhVHJhbnNmZXIuZmlsZXNbMF0gOiBlLnRhcmdldC5maWxlc1swXTtcbiAgICAgICAgdGhpcy5maWxlQ2hvb3NlID0gZmlsZTtcbiAgICAgICAgdmFyIHBhdHRlcm4gPSAvaW1hZ2UtKi87XG4gICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXG4gICAgICAgIGlmICghZmlsZS50eXBlLm1hdGNoKHBhdHRlcm4pKSB7XG4gICAgICAgICAgICBhbGVydCgnaW52YWxpZCBmb3JtYXQnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubG9hZGVkID0gZmFsc2U7XG5cbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9IHRoaXMuX2hhbmRsZVJlYWRlckxvYWRlZC5iaW5kKHRoaXMpO1xuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcbiAgICAgICAgdGhpcy5mbGdJbWFnZUNob29zZT0gdHJ1ZTtcbiAgICB9XG4gICAgXG4gICAgX2hhbmRsZVJlYWRlckxvYWRlZChlKSB7XG4gICAgICAgIHZhciByZWFkZXIgPSBlLnRhcmdldDtcbiAgICAgICAgdGhpcy5pbWFnZVNyYyA9IHJlYWRlci5yZXN1bHQ7XG4gICAgICAgIHRoaXMubG9hZGVkID0gdHJ1ZTtcbiAgICB9XG4gICAgXG4gICAgX3NldEFjdGl2ZSgpIHtcbiAgICAgICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYWN0aXZlQ29sb3I7XG4gICAgICAgIGlmICh0aGlzLmltYWdlU3JjLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLmFjdGl2ZUNvbG9yO1xuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIF9zZXRJbmFjdGl2ZSgpIHtcbiAgICAgICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYmFzZUNvbG9yO1xuICAgICAgICBpZiAodGhpcy5pbWFnZVNyYy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5iYXNlQ29sb3I7XG4gICAgICAgIH1cbiAgICB9XG5jcmVhdGVDYXRlZ29yeSgpe1xuICBsZXQgaWQgID0gJy0xJztcbiAgaWYodGhpcy5pZCE9bnVsbCl7XG4gICAgaWQgID0gdGhpcy5pZDtcbiAgfVxuICB0aGlzLm9uVmFsdWVDaGFuZ2VkKCk7XG4gIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICAgaWYodGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5sZW5ndGggPjApe1xuICAgICAgICAgcmV0dXJuO1xuICAgICAgIH1cbiAgICB9XG4gIHRoaXMuY2F0ZWdvcnlPYmplY3QgPSB7XG4gICAgICAgICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgICAgICAgJ3VzZXJfaWQnOnRoaXMudXNlcklkLFxuICAgICAgICAgICAgJ2NhdGVnb3J5Jzp7XG4gICAgICAgICAgICAgICdpZCc6aWQsXG4gICAgICAgICAgICAgICduYW1lJzp0aGlzLmNyZWF0ZUNhdGVnb3J5Rm9ybS52YWx1ZS5uYW1lLFxuICAgICAgICAgICAgICAndGh1bWInOicnLFxuICAgICAgICAgICAgICAnZGVzY3JpcHRpb24nOnRoaXMuY3JlYXRlQ2F0ZWdvcnlGb3JtLnZhbHVlLmRlc2NyaXB0aW9uLFxuICAgICAgICAgICAgICAnaXNfYWN0aXZlJzp0aGlzLmlzQ2hlY2tSYWRpbyxcbiAgICAgICAgICAgICAgJ2lzX2RlbGV0ZSc6JzAnXG4gICAgICAgICAgICB9fTtcbiAgXHR0aGlzLmNhdGVnb3J5U2VydmljZS5jcmVhdGVDYXRlZ29yeSh0aGlzLmNhdGVnb3J5T2JqZWN0KS50aGVuKFxuICAgICAgICAgICAgLy8gICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAvLyAgICAgICAgICBcdHRoaXMuY2F0ZWdvcmllcyA9IHJlc3BvbnNlO1xuICAgICAgICAgICAgLy8gICAgICAgICAgXHRjb25zb2xlLmxvZyhyZXNwb25zZSwgdGhpcy5jYXRlZ29yaWVzKTsgXG4gICAgICAgICAgICAvLyAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0Ly8gIGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSxcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG4gIFx0XHRcdFx0XHQvLyB9KTtcblxufVxucHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgIGxldCBpbWFnZVBvc3QgPSAnJztcbiAgICAgaWYoIHRoaXMuZmxnSW1hZ2VDaG9vc2UgPT0gdHJ1ZSl7XG4gICAgICAgaW1hZ2VQb3N0ID0gdGhpcy5pbWFnZVNyYztcbiAgICAgfVxuICAgICAgdGhpcy5jYXRlZ29yaWVzID0gcmVzcG9uc2U7ICBcbiAgICAgIGxldCBjYXRlZ29yeVVwbG9hZCA9IHtcbiAgICAgICAgICdpZCc6dGhpcy5jYXRlZ29yaWVzLmNhdGVnb3J5X2lkLFxuICAgICAgICAgJ3RodW1iJzppbWFnZVBvc3RcbiAgICAgIH1cblx0ICAgIHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmNhdGVnb3J5VXBsb2FkRmlsZShjYXRlZ29yeVVwbG9hZCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyBDYXRlZ29yeSBoYXMgYmVlbiBhZGRlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ2NhdGVnb3J5L2xpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICAgICAgICAgICAgXG4gIFx0XHRcdFx0XHR9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lcnJvciA9IHJlc3BvbnNlLmVycm9ycztcbiAgICAgIGlmIChyZXNwb25zZS5jb2RlID09IDQyMikge1xuICAgICAgICBpZiAodGhpcy5lcnJvci5uYW1lKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWyduYW1lJ10gPSB0aGlzLmVycm9yLm5hbWU7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN3YWwoXG4gICAgICAgICAgJ0NyZWF0ZSBGYWlsIScsXG4gICAgICAgICAgdGhpcy5lcnJvclswXSxcbiAgICAgICAgICAnZXJyb3InXG4gICAgICAgICkuY2F0Y2goc3dhbC5ub29wKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBmYWlsZWRDcmVhdGUgKHJlcykge1xuICAgIGlmIChyZXMuc3RhdHVzID09IDQwMSkge1xuICAgICAgLy8gdGhpcy5sb2dpbmZhaWxlZCA9IHRydWVcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHJlcy5kYXRhLmVycm9ycy5tZXNzYWdlWzBdID09ICdFbWFpbCBVbnZlcmlmaWVkJykge1xuICAgICAgICAvLyB0aGlzLnVudmVyaWZpZWQgPSB0cnVlXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBvdGhlciBraW5kcyBvZiBlcnJvciByZXR1cm5lZCBmcm9tIHNlcnZlclxuICAgICAgICBmb3IgKHZhciBlcnJvciBpbiByZXMuZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICAvLyB0aGlzLmxvZ2luZmFpbGVkZXJyb3IgKz0gcmVzLmRhdGEuZXJyb3JzW2Vycm9yXSArICcgJ1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=
