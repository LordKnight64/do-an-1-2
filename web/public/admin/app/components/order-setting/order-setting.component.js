"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_select_1 = require("ng2-select");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var notification_service_1 = require("app/services/notification.service");
var restaurant_service_1 = require("app/services/restaurant.service");
var user_service_1 = require("app/services/user.service");
var comCode_service_1 = require("app/services/comCode.service");
var decimal_validator_directive_1 = require("app/utils/decimal-validator.directive");
var ng2_translate_1 = require("ng2-translate");
var OrderSettingComponent = (function () {
    function OrderSettingComponent(restServ, userServ, fb, notifyServ, comCodeServ, translateService) {
        this.restServ = restServ;
        this.userServ = userServ;
        this.fb = fb;
        this.notifyServ = notifyServ;
        this.comCodeServ = comCodeServ;
        this.translateService = translateService;
        this.delivery_charge = null;
        this.minimum_order = null;
        this.payment_method = null;
        this.delivery_methods = null;
        this.delivery_time = null;
        this.default_currency = null;
        this.food_ordering_system_type = null;
        this.unit_point = null;
        this.formErrors = {
            'payment_fee': [],
            'delivery_charge': [],
            'minimum_order': [],
            'payment_method': [],
            'delivery_methods': [],
            'delivery_time': [],
            'default_currency': [],
            'food_ordering_system_type': [],
            'unit_point': []
        };
        this.validationMessages = {
            'payment_fee': {
                'required': 'The payment fee field is required.',
                'maxlength': 'The payment fee may not be greater than 256 characters.',
                'pattern': 'The payment fee must be a number.'
            },
            'delivery_charge': {
                'required': 'The delivery charge field is required.',
                'maxlength': 'The delivery charge may not be greater than 256 characters.',
                'pattern': 'The delivery charge must be a number.'
            },
            'minimum_order': {
                'required': 'The minimum order field is required.',
                'maxlength': 'The payment fee may not be greater than 20 characters.',
                'pattern': 'The minimum order must be a number.',
                'invalidDecimal': 'The order may not decimal ( 15 inerger , 4 decimal )'
            },
            'payment_method': {
                'required': 'The payment method field is required.',
                'maxlength': 'The payment method may not be greater than 4 characters.'
            },
            'delivery_methods': {
                'required': 'The delivery methods field is required.',
                'maxlength': 'The delivery methods may not be greater than 4 characters.'
            },
            'delivery_time': {
                'required': 'The delivery time field is required / formatNumber.'
            },
            'default_currency': {
                'required': 'The default currency field is required.',
                'maxlength': 'The default currency may not be greater than 4 characters.'
            },
            'food_ordering_system_type': {
                'maxlength': 'The food ordering system type may not be greater than 4 characters.'
            },
            'unit_point': {
                'maxlength': 'The food ordering system type may not be greater than 11 characters.'
            }
        };
    }
    OrderSettingComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem('langChoosen') != undefined && localStorage.getItem('langChoosen') != null) {
            this.translateService.setDefaultLang(localStorage.getItem('langChoosen'));
            this.translateService.use(localStorage.getItem('langChoosen'));
        }
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.comCodeServ.getCode({ 'cd_group': '0' }).then(function (response) {
            _this.deliveryMethodSelect.items = response.code.map(function (x) {
                return { id: x.cd.toString(), text: x.cd_name };
            });
        }, function (error) {
            console.log(error);
        });
        this.comCodeServ.getCode({ 'cd_group': '1' }).then(function (response) {
            _this.paymentMethodSelect.items = response.code.map(function (x) {
                return { id: x.cd.toString(), text: x.cd_name };
            });
        }, function (error) {
            console.log(error);
        });
        this.comCodeServ.getCode({ 'cd_group': '2' }).then(function (response) {
            _this.select.items = response.code.map(function (x) {
                return { id: x.cd.toString(), text: x.cd_name };
            });
        }, function (error) {
            console.log(error);
        });
        this.buildForm();
        this.mymodel = {
            payment_fee: null,
            delivery_charge: null,
            minimum_order: null,
            payment_method: null,
            delivery_methods: null,
            delivery_time: null,
            default_currency: null,
            unit_point: null,
            food_ordering_system_type: null
        };
        var params = {
            'rest_id': this.restId
        };
        this.restServ.getOrderSetting(params).then(function (response) {
            _this.mymodel = response.ordering_setting;
            _this.setVar();
        }, function (error) {
            console.log(error);
        });
        console.log('init order setting component');
    };
    OrderSettingComponent.prototype.setVar = function () {
        var _this = this;
        if (this.mymodel != null) {
            this.mymodel = {
                payment_fee: this.mymodel.payment_fee,
                delivery_charge: this.mymodel.delivery_charge,
                minimum_order: this.mymodel.minimum_order,
                payment_method: this.mymodel.payment_method,
                delivery_methods: this.mymodel.delivery_methods,
                delivery_time: this.mymodel.delivery_time,
                default_currency: this.mymodel.default_currency,
                unit_point: this.mymodel.unit_point,
                food_ordering_system_type: this.mymodel.food_ordering_system_type
            };
            this.dtForm.controls['payment_fee'].setValue(this.mymodel.payment_fee);
            this.dtForm.controls['delivery_charge'].setValue(this.mymodel.delivery_charge);
            this.dtForm.controls['minimum_order'].setValue(this.mymodel.minimum_order);
            this.dtForm.controls['payment_method'].setValue(this.mymodel.payment_method);
            this.dtForm.controls['delivery_methods'].setValue(this.mymodel.delivery_methods);
            this.dtForm.controls['unit_point'].setValue(this.mymodel.unit_point);
            var tg = this.mymodel.delivery_time.match(/(\d+)\:(\d+)\:(\d+)/);
            var ng = new Date();
            ng.setHours(tg[1]);
            ng.setMinutes(tg[2]);
            ng.setSeconds(tg[3]);
            this.dtForm.controls['delivery_time'].setValue(ng);
            this.dtForm.controls['default_currency'].setValue(this.mymodel.default_currency);
            this.dtForm.controls['food_ordering_system_type'].setValue(this.mymodel.food_ordering_system_type);
            this.payment_fee = this.dtForm.controls['payment_fee'];
            this.delivery_charge = this.dtForm.controls['delivery_charge'];
            this.minimum_order = this.dtForm.controls['minimum_order'];
            this.payment_method = this.dtForm.controls['payment_method'];
            this.delivery_methods = this.dtForm.controls['delivery_methods'];
            this.delivery_time = this.dtForm.controls['delivery_time'];
            this.default_currency = this.dtForm.controls['default_currency'];
            this.unit_point = this.dtForm.controls['unit_point'];
            this.food_ordering_system_type = this.dtForm.controls['food_ordering_system_type'];
            this.select.active = this.select.itemObjects.filter(function (x) {
                if (_this.mymodel.default_currency === x.text) {
                    return true;
                }
            });
            this.paymentMethodSelect.active = this.paymentMethodSelect.itemObjects.filter(function (x) {
                if (_this.mymodel.payment_method.split(',').filter(function (y) { return y === x.id; }).length > 0) {
                    return true;
                }
            });
            this.deliveryMethodSelect.active = this.deliveryMethodSelect.itemObjects.filter(function (x) {
                if (_this.mymodel.delivery_methods.split(',').filter(function (y) { return y === x.id; }).length > 0) {
                    return true;
                }
            });
        }
    };
    OrderSettingComponent.prototype.buildForm = function () {
        var _this = this;
        this.dtForm = this.fb.group({
            'payment_fee': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(256)]],
            'delivery_charge': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(256)]],
            'minimum_order': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(20), decimal_validator_directive_1.invalidDecimalValidator()]],
            'payment_method': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(20)]],
            'delivery_methods': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(20)]],
            'delivery_time': ['', [forms_1.Validators.required]],
            'default_currency': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(4)]],
            'food_ordering_system_type': ['', [forms_1.Validators.maxLength(4)]],
            'unit_point': ['', [forms_1.Validators.maxLength(11)]],
        });
        this.payment_fee = this.dtForm.controls['payment_fee'];
        this.delivery_charge = this.dtForm.controls['delivery_charge'];
        this.minimum_order = this.dtForm.controls['minimum_order'];
        this.payment_method = this.dtForm.controls['payment_method'];
        this.delivery_methods = this.dtForm.controls['delivery_methods'];
        this.delivery_time = this.dtForm.controls['delivery_time'];
        this.default_currency = this.dtForm.controls['default_currency'];
        this.food_ordering_system_type = this.dtForm.controls['food_ordering_system_type'];
        this.unit_point = this.dtForm.controls['unit_point'];
        this.dtForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
        //this.onValueChanged(); // (re)set validation messages now
    };
    OrderSettingComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.dtForm) {
            return;
        }
        var form = this.dtForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    OrderSettingComponent.prototype.doSaveOrderSetting = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.mymodel.payment_fee = this.payment_fee.value;
        this.mymodel.delivery_charge = this.delivery_charge.value;
        this.mymodel.minimum_order = this.minimum_order.value;
        this.mymodel.payment_method = this.payment_method.value;
        this.mymodel.delivery_methods = this.delivery_methods.value;
        this.mymodel.delivery_time = this.delivery_time.value.toLocaleTimeString([], { hour12: false });
        this.mymodel.default_currency = this.default_currency.value;
        this.mymodel.food_ordering_system_type = this.food_ordering_system_type.value;
        this.mymodel.unit_point = this.unit_point.value;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'order_setting': this.mymodel
        };
        this.restServ.updateOrderSetting(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            _this.notifyServ.success('Contact has been updated');
        }, function (error) {
            console.log(error);
        });
    };
    ;
    OrderSettingComponent.prototype.onChange = function (value) {
        console.log('Changed value is: ', value);
    };
    OrderSettingComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
    };
    OrderSettingComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    OrderSettingComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    OrderSettingComponent.prototype.refreshValue = function (value) {
        this.dtForm.controls['default_currency'].setValue(value.text);
        this.default_currency = this.dtForm.controls['default_currency'];
    };
    OrderSettingComponent.prototype.refreshPaymentMethodValue = function (value) {
        this.dtForm.controls['payment_method'].setValue(value.map(function (elem) { return elem.id; }).join(','));
        this.payment_method = this.dtForm.controls['payment_method'];
    };
    OrderSettingComponent.prototype.refreshDeliveryMethodValue = function (value) {
        this.dtForm.controls['delivery_methods'].setValue(value.map(function (elem) { return elem.id; }).join(','));
        this.delivery_methods = this.dtForm.controls['delivery_methods'];
    };
    OrderSettingComponent.prototype.onChangeDeliveryValue = function (event) {
        var value1 = event.target.value;
        console.log('Changed value is: ', event.target.value);
        if (value1 == "" || value1 == null) {
            this.dtForm.controls['delivery_time'].setValue(null);
            this.delivery_time = this.dtForm.controls['delivery_time'];
        }
        var nameRe = new RegExp(/^[0-9]+$/i);
        if (!nameRe.test(value1) || value1.length > 2) {
            this.dtForm.controls['delivery_time'].setValue(null);
            this.delivery_time = this.dtForm.controls['delivery_time'];
        }
    };
    __decorate([
        core_1.ViewChild('Select'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], OrderSettingComponent.prototype, "select", void 0);
    __decorate([
        core_1.ViewChild('PaymentMethodSelect'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], OrderSettingComponent.prototype, "paymentMethodSelect", void 0);
    __decorate([
        core_1.ViewChild('DeliveryMethodSelect'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], OrderSettingComponent.prototype, "deliveryMethodSelect", void 0);
    OrderSettingComponent = __decorate([
        core_1.Component({
            selector: 'order-setting',
            templateUrl: utils_1.default.getView('app/components/order-setting/order-setting.component.html'),
            styleUrls: [utils_1.default.getView('app/components/order-setting/order-setting.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
            user_service_1.UserService,
            forms_1.FormBuilder,
            notification_service_1.NotificationService,
            comCode_service_1.ComCodeService,
            ng2_translate_1.TranslateService])
    ], OrderSettingComponent);
    return OrderSettingComponent;
}());
exports.OrderSettingComponent = OrderSettingComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL29yZGVyLXNldHRpbmcvb3JkZXItc2V0dGluZy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBNkQ7QUFDN0QseUNBQTZDO0FBQzdDLHdDQUEwRztBQUMxRyx5Q0FBb0M7QUFFcEMsNkRBQXVFO0FBQ3ZFLDBFQUF3RTtBQUN4RSxzRUFBb0U7QUFDcEUsMERBQXdEO0FBQ3hELGdFQUE4RDtBQUM5RCxxRkFBZ0Y7QUFDaEYsK0NBQWlEO0FBUWpEO0lBNkJFLCtCQUNVLFFBQTJCLEVBQzNCLFFBQXFCLEVBQ3JCLEVBQWUsRUFDZixVQUErQixFQUMvQixXQUEyQixFQUMzQixnQkFBa0M7UUFMbEMsYUFBUSxHQUFSLFFBQVEsQ0FBbUI7UUFDM0IsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFDL0IsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzNCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUE1QnBDLG9CQUFlLEdBQW9CLElBQUksQ0FBQztRQUN4QyxrQkFBYSxHQUFvQixJQUFJLENBQUM7UUFDdEMsbUJBQWMsR0FBb0IsSUFBSSxDQUFDO1FBQ3ZDLHFCQUFnQixHQUFvQixJQUFJLENBQUM7UUFDekMsa0JBQWEsR0FBb0IsSUFBSSxDQUFDO1FBQ3RDLHFCQUFnQixHQUFvQixJQUFJLENBQUM7UUFDekMsOEJBQXlCLEdBQW9CLElBQUksQ0FBQztRQUNsRCxlQUFVLEdBQW1CLElBQUksQ0FBQztRQXFMMUMsZUFBVSxHQUFHO1lBQ1gsYUFBYSxFQUFFLEVBQUU7WUFDakIsaUJBQWlCLEVBQUUsRUFBRTtZQUNyQixlQUFlLEVBQUUsRUFBRTtZQUNuQixnQkFBZ0IsRUFBRSxFQUFFO1lBQ3BCLGtCQUFrQixFQUFFLEVBQUU7WUFDdEIsZUFBZSxFQUFFLEVBQUU7WUFDbkIsa0JBQWtCLEVBQUUsRUFBRTtZQUN0QiwyQkFBMkIsRUFBRSxFQUFFO1lBQy9CLFlBQVksRUFBRSxFQUFFO1NBRWpCLENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNuQixhQUFhLEVBQUU7Z0JBQ2IsVUFBVSxFQUFFLG9DQUFvQztnQkFDaEQsV0FBVyxFQUFFLHlEQUF5RDtnQkFDdEUsU0FBUyxFQUFFLG1DQUFtQzthQUMvQztZQUNELGlCQUFpQixFQUFFO2dCQUNqQixVQUFVLEVBQUUsd0NBQXdDO2dCQUNwRCxXQUFXLEVBQUUsNkRBQTZEO2dCQUMxRSxTQUFTLEVBQUUsdUNBQXVDO2FBQ25EO1lBQ0QsZUFBZSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxzQ0FBc0M7Z0JBQ2xELFdBQVcsRUFBRSx3REFBd0Q7Z0JBQ3JFLFNBQVMsRUFBRSxxQ0FBcUM7Z0JBQ2hELGdCQUFnQixFQUFFLHNEQUFzRDthQUN6RTtZQUNELGdCQUFnQixFQUFFO2dCQUNoQixVQUFVLEVBQUUsdUNBQXVDO2dCQUNuRCxXQUFXLEVBQUUsMERBQTBEO2FBQ3hFO1lBQ0Qsa0JBQWtCLEVBQUU7Z0JBQ2xCLFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3JELFdBQVcsRUFBRSw0REFBNEQ7YUFDMUU7WUFDRCxlQUFlLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLHFEQUFxRDthQUNsRTtZQUNELGtCQUFrQixFQUFFO2dCQUNsQixVQUFVLEVBQUUseUNBQXlDO2dCQUNyRCxXQUFXLEVBQUUsNERBQTREO2FBQzFFO1lBQ0QsMkJBQTJCLEVBQUU7Z0JBQzNCLFdBQVcsRUFBRSxxRUFBcUU7YUFDbkY7WUFDRCxZQUFZLEVBQUU7Z0JBQ1osV0FBVyxFQUFFLHNFQUFzRTthQUNwRjtTQUVGLENBQUM7SUFuTkUsQ0FBQztJQUVMLHdDQUFRLEdBQVI7UUFBQSxpQkF1REM7UUF0REMsRUFBRSxDQUFBLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBRSxTQUFTLElBQUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQzlGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7UUFDRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUMzRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3pCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxQyxDQUFDO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ3pELEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDO2dCQUM3RCxNQUFNLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2xELENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDekQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7Z0JBQzVELE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUN6RCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7Z0JBQy9DLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsV0FBVyxFQUFFLElBQUk7WUFDakIsZUFBZSxFQUFFLElBQUk7WUFDckIsYUFBYSxFQUFFLElBQUk7WUFDbkIsY0FBYyxFQUFFLElBQUk7WUFDcEIsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixhQUFhLEVBQUUsSUFBSTtZQUNuQixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLHlCQUF5QixFQUFFLElBQUk7U0FDaEMsQ0FBQztRQUNGLElBQUksTUFBTSxHQUFHO1lBQ1gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3ZCLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ2pELEtBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDO1lBQ3pDLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNoQixDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU8sc0NBQU0sR0FBZDtRQUFBLGlCQXNEQztRQXBEQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRztnQkFDYixXQUFXLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxlQUFlLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlO2dCQUM3QyxhQUFhLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhO2dCQUN6QyxjQUFjLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjO2dCQUMzQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQjtnQkFDL0MsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYTtnQkFDekMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0I7Z0JBQy9DLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVU7Z0JBQ25DLHlCQUF5QixFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMseUJBQXlCO2FBQ2xFLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN2RSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQy9FLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzNFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDN0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ2pGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3JFLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2pFLElBQUksRUFBRSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7WUFDcEIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQixFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNqRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDbkcsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDakUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3JELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1lBRW5GLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUM7Z0JBQ3BELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzdDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUM7Z0JBQzlFLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBVixDQUFVLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDaEYsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDZCxDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQztnQkFDaEYsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQVYsQ0FBVSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xGLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztJQUNILENBQUM7SUFFTyx5Q0FBUyxHQUFqQjtRQUFBLGlCQXlCQztRQXZCQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzFCLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDckUsaUJBQWlCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3pFLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUFFLHFEQUF1QixFQUFFLENBQUMsQ0FBQztZQUNqRyxnQkFBZ0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdkUsa0JBQWtCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pFLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDNUMsa0JBQWtCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLDJCQUEyQixFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1RCxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQy9DLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7YUFDckIsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztRQUN2RCwyREFBMkQ7SUFDN0QsQ0FBQztJQUVPLDhDQUFjLEdBQXRCLFVBQXVCLE9BQU8sRUFBRSxJQUFVO1FBRXhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQzdCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDekIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDN0QsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUF3RE8sa0RBQWtCLEdBQTFCO1FBQUEsaUJBa0NDO1FBakNDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxDQUFDO1lBQ1QsQ0FBQztRQUNILENBQUM7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztRQUNsRCxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQztRQUMxRCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUN0RCxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQztRQUN4RCxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7UUFDNUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDaEcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO1FBQzVELElBQUksQ0FBQyxPQUFPLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLEtBQUssQ0FBQztRQUM3RSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztRQUNqRCxJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsZUFBZSxFQUFFLElBQUksQ0FBQyxPQUFPO1NBQzlCLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDcEQsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEQsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsTUFBTSxDQUFDO1lBQ1QsQ0FBQztZQUNELEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDdEQsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQUEsQ0FBQztJQUVLLHdDQUFRLEdBQWYsVUFBZ0IsS0FBVTtRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSx3Q0FBUSxHQUFmLFVBQWdCLEtBQVU7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU0sdUNBQU8sR0FBZCxVQUFlLEtBQVU7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0scUNBQUssR0FBWixVQUFhLEtBQVU7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sNENBQVksR0FBbkIsVUFBb0IsS0FBVTtRQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVNLHlEQUF5QixHQUFoQyxVQUFpQyxLQUFVO1FBQ3pDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBVSxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUMxRyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVNLDBEQUEwQixHQUFqQyxVQUFrQyxLQUFVO1FBQzFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBVSxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM1RyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBQ08scURBQXFCLEdBQTVCLFVBQTZCLEtBQVU7UUFDdEMsSUFBSSxNQUFNLEdBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RELEVBQUUsQ0FBQSxDQUFDLE1BQU0sSUFBRSxFQUFFLElBQUcsTUFBTSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDNUQsQ0FBQztRQUNELElBQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLEVBQUUsQ0FBQSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDMUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDNUQsQ0FBQztJQUNILENBQUM7SUE3U29CO1FBQXBCLGdCQUFTLENBQUMsUUFBUSxDQUFDO2tDQUFpQiw0QkFBZTt5REFBQztJQUNuQjtRQUFqQyxnQkFBUyxDQUFDLHFCQUFxQixDQUFDO2tDQUE4Qiw0QkFBZTtzRUFBQztJQUM1QztRQUFsQyxnQkFBUyxDQUFDLHNCQUFzQixDQUFDO2tDQUErQiw0QkFBZTt1RUFBQztJQTVCdEUscUJBQXFCO1FBUGpDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQywyREFBMkQsQ0FBQztZQUN2RixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDBEQUEwRCxDQUFDLENBQUM7WUFDdEYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBRSxxQkFBcUIsRUFBRSxFQUFFLEVBQUU7U0FDcEMsQ0FBQzt5Q0ErQm9CLHNDQUFpQjtZQUNqQiwwQkFBVztZQUNqQixtQkFBVztZQUNILDBDQUFtQjtZQUNsQixnQ0FBYztZQUNULGdDQUFnQjtPQW5DakMscUJBQXFCLENBd1VqQztJQUFELDRCQUFDO0NBeFVELEFBd1VDLElBQUE7QUF4VVksc0RBQXFCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL29yZGVyLXNldHRpbmcvb3JkZXItc2V0dGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTZWxlY3RDb21wb25lbnQgfSBmcm9tICduZzItc2VsZWN0JztcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0sIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFJlc3RhdXJhbnRTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3Jlc3RhdXJhbnQuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBDb21Db2RlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9jb21Db2RlLnNlcnZpY2UnO1xuaW1wb3J0IHsgaW52YWxpZERlY2ltYWxWYWxpZGF0b3IgfSBmcm9tIFwiYXBwL3V0aWxzL2RlY2ltYWwtdmFsaWRhdG9yLmRpcmVjdGl2ZVwiO1xuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ25nMi10cmFuc2xhdGUnO1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnb3JkZXItc2V0dGluZycsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcmRlci1zZXR0aW5nL29yZGVyLXNldHRpbmcuY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvb3JkZXItc2V0dGluZy9vcmRlci1zZXR0aW5nLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7ICdbQHJvdXRlclRyYW5zaXRpb25dJzogJycgfVxufSlcbmV4cG9ydCBjbGFzcyBPcmRlclNldHRpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHByaXZhdGUgbXltb2RlbDtcbiAgcHJpdmF0ZSByZXN0SWQ7XG4gIHByaXZhdGUgdXNlcklkO1xuICBwcml2YXRlIGR0Rm9ybTogRm9ybUdyb3VwO1xuICBwcml2YXRlIHBheW1lbnRfZmVlOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgZGVsaXZlcnlfY2hhcmdlOiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIG1pbmltdW1fb3JkZXI6IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgcGF5bWVudF9tZXRob2Q6IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgZGVsaXZlcnlfbWV0aG9kczogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBkZWxpdmVyeV90aW1lOiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIGRlZmF1bHRfY3VycmVuY3k6IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZTogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSB1bml0X3BvaW50OkFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIC8vIHByaXZhdGUgaXRlbXM6IEFycmF5PHN0cmluZz4gPSBbJ1ZORCcsICdVU0QnLCAnRVVSJywgJ0dCUCcsXG4gIC8vICAgJ0lOUicsICdBVVInLCAnQ0FEJywgJ1NHRCcsICdKUFknXTtcbiAgLy8gcHJpdmF0ZSBwYXltZW50TWV0aG9kSXRlbXM6IEFycmF5PGFueT4gPSBbXG4gIC8vICAgeyBpZDogJzEnLCB0ZXh0OiAnQ2FzaCcgfSxcbiAgLy8gICB7IGlkOiAnMicsIHRleHQ6ICdBVE0nIH0sXG4gIC8vICAgeyBpZDogJzMnLCB0ZXh0OiAnQ3JlZGl0IENhcmQnIH1cbiAgLy8gXTtcbiAgLy8gcHJpdmF0ZSBkZWxpdmVyeU1ldGhvZEl0ZW1zOiBBcnJheTxhbnk+ID0gW1xuICAvLyAgIHsgaWQ6ICcxJywgdGV4dDogJ0hvbWUnIH0sXG4gIC8vICAgeyBpZDogJzInLCB0ZXh0OiAnVGFrZUF3YXknIH1cbiAgLy8gXTtcbiAgQFZpZXdDaGlsZCgnU2VsZWN0JykgcHJpdmF0ZSBzZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcbiAgQFZpZXdDaGlsZCgnUGF5bWVudE1ldGhvZFNlbGVjdCcpIHByaXZhdGUgcGF5bWVudE1ldGhvZFNlbGVjdDogU2VsZWN0Q29tcG9uZW50O1xuICBAVmlld0NoaWxkKCdEZWxpdmVyeU1ldGhvZFNlbGVjdCcpIHByaXZhdGUgZGVsaXZlcnlNZXRob2RTZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByZXN0U2VydjogUmVzdGF1cmFudFNlcnZpY2UsXG4gICAgcHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gICAgcHJpdmF0ZSBub3RpZnlTZXJ2OiBOb3RpZmljYXRpb25TZXJ2aWNlLFxuICAgIHByaXZhdGUgY29tQ29kZVNlcnY6IENvbUNvZGVTZXJ2aWNlLFxuICAgIHByaXZhdGUgdHJhbnNsYXRlU2VydmljZTogVHJhbnNsYXRlU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGlmKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsYW5nQ2hvb3NlbicpIT11bmRlZmluZWQgJiYgbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xhbmdDaG9vc2VuJykhPW51bGwpe1xuICAgICAgdGhpcy50cmFuc2xhdGVTZXJ2aWNlLnNldERlZmF1bHRMYW5nKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsYW5nQ2hvb3NlbicpKTtcbiAgICAgIHRoaXMudHJhbnNsYXRlU2VydmljZS51c2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xhbmdDaG9vc2VuJykpO1xuICAgIH1cbiAgICBsZXQgdXNlcktleSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgIGlmICghdXNlcktleSkge1xuICAgICAgdGhpcy51c2VyU2Vydi5sb2dvdXQoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy51c2VySWQgPSB1c2VyS2V5LnVzZXJfaW5mby5pZDtcbiAgICAgIHRoaXMucmVzdElkID0gdXNlcktleS5yZXN0YXVyYW50c1swXS5pZDtcbiAgICB9XG4gICAgdGhpcy5jb21Db2RlU2Vydi5nZXRDb2RlKHsgJ2NkX2dyb3VwJzogJzAnIH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgdGhpcy5kZWxpdmVyeU1ldGhvZFNlbGVjdC5pdGVtcyA9IHJlc3BvbnNlLmNvZGUubWFwKGZ1bmN0aW9uICh4KSB7XG4gICAgICAgIHJldHVybiB7IGlkOiB4LmNkLnRvU3RyaW5nKCksIHRleHQ6IHguY2RfbmFtZSB9O1xuICAgICAgfSk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICAgIHRoaXMuY29tQ29kZVNlcnYuZ2V0Q29kZSh7ICdjZF9ncm91cCc6ICcxJyB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMucGF5bWVudE1ldGhvZFNlbGVjdC5pdGVtcyA9IHJlc3BvbnNlLmNvZGUubWFwKGZ1bmN0aW9uICh4KSB7XG4gICAgICAgIHJldHVybiB7IGlkOiB4LmNkLnRvU3RyaW5nKCksIHRleHQ6IHguY2RfbmFtZSB9O1xuICAgICAgfSk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICAgIHRoaXMuY29tQ29kZVNlcnYuZ2V0Q29kZSh7ICdjZF9ncm91cCc6ICcyJyB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMuc2VsZWN0Lml0ZW1zID0gcmVzcG9uc2UuY29kZS5tYXAoZnVuY3Rpb24gKHgpIHtcbiAgICAgICAgcmV0dXJuIHsgaWQ6IHguY2QudG9TdHJpbmcoKSwgdGV4dDogeC5jZF9uYW1lIH07XG4gICAgICB9KTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gICAgdGhpcy5idWlsZEZvcm0oKTtcbiAgICB0aGlzLm15bW9kZWwgPSB7XG4gICAgICBwYXltZW50X2ZlZTogbnVsbCxcbiAgICAgIGRlbGl2ZXJ5X2NoYXJnZTogbnVsbCxcbiAgICAgIG1pbmltdW1fb3JkZXI6IG51bGwsXG4gICAgICBwYXltZW50X21ldGhvZDogbnVsbCxcbiAgICAgIGRlbGl2ZXJ5X21ldGhvZHM6IG51bGwsXG4gICAgICBkZWxpdmVyeV90aW1lOiBudWxsLFxuICAgICAgZGVmYXVsdF9jdXJyZW5jeTogbnVsbCxcbiAgICAgIHVuaXRfcG9pbnQ6IG51bGwsXG4gICAgICBmb29kX29yZGVyaW5nX3N5c3RlbV90eXBlOiBudWxsXG4gICAgfTtcbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZFxuICAgIH07XG4gICAgdGhpcy5yZXN0U2Vydi5nZXRPcmRlclNldHRpbmcocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMubXltb2RlbCA9IHJlc3BvbnNlLm9yZGVyaW5nX3NldHRpbmc7XG4gICAgICB0aGlzLnNldFZhcigpO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgICBjb25zb2xlLmxvZygnaW5pdCBvcmRlciBzZXR0aW5nIGNvbXBvbmVudCcpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRWYXIoKTogdm9pZCB7XG5cbiAgICBpZiAodGhpcy5teW1vZGVsICE9IG51bGwpIHtcbiAgICAgIHRoaXMubXltb2RlbCA9IHtcbiAgICAgICAgcGF5bWVudF9mZWU6IHRoaXMubXltb2RlbC5wYXltZW50X2ZlZSxcbiAgICAgICAgZGVsaXZlcnlfY2hhcmdlOiB0aGlzLm15bW9kZWwuZGVsaXZlcnlfY2hhcmdlLFxuICAgICAgICBtaW5pbXVtX29yZGVyOiB0aGlzLm15bW9kZWwubWluaW11bV9vcmRlcixcbiAgICAgICAgcGF5bWVudF9tZXRob2Q6IHRoaXMubXltb2RlbC5wYXltZW50X21ldGhvZCxcbiAgICAgICAgZGVsaXZlcnlfbWV0aG9kczogdGhpcy5teW1vZGVsLmRlbGl2ZXJ5X21ldGhvZHMsXG4gICAgICAgIGRlbGl2ZXJ5X3RpbWU6IHRoaXMubXltb2RlbC5kZWxpdmVyeV90aW1lLFxuICAgICAgICBkZWZhdWx0X2N1cnJlbmN5OiB0aGlzLm15bW9kZWwuZGVmYXVsdF9jdXJyZW5jeSxcbiAgICAgICAgdW5pdF9wb2ludDogdGhpcy5teW1vZGVsLnVuaXRfcG9pbnQsXG4gICAgICAgIGZvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGU6IHRoaXMubXltb2RlbC5mb29kX29yZGVyaW5nX3N5c3RlbV90eXBlXG4gICAgICB9O1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ3BheW1lbnRfZmVlJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLnBheW1lbnRfZmVlKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV9jaGFyZ2UnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuZGVsaXZlcnlfY2hhcmdlKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydtaW5pbXVtX29yZGVyJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLm1pbmltdW1fb3JkZXIpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ3BheW1lbnRfbWV0aG9kJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLnBheW1lbnRfbWV0aG9kKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV9tZXRob2RzJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLmRlbGl2ZXJ5X21ldGhvZHMpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ3VuaXRfcG9pbnQnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwudW5pdF9wb2ludCk7XG4gICAgICBsZXQgdGcgPSB0aGlzLm15bW9kZWwuZGVsaXZlcnlfdGltZS5tYXRjaCgvKFxcZCspXFw6KFxcZCspXFw6KFxcZCspLyk7XG4gICAgICBsZXQgbmcgPSBuZXcgRGF0ZSgpO1xuICAgICAgbmcuc2V0SG91cnModGdbMV0pO1xuICAgICAgbmcuc2V0TWludXRlcyh0Z1syXSk7XG4gICAgICBuZy5zZXRTZWNvbmRzKHRnWzNdKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV90aW1lJ10uc2V0VmFsdWUobmcpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuZGVmYXVsdF9jdXJyZW5jeSk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZSddLnNldFZhbHVlKHRoaXMubXltb2RlbC5mb29kX29yZGVyaW5nX3N5c3RlbV90eXBlKTtcbiAgICAgIHRoaXMucGF5bWVudF9mZWUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1sncGF5bWVudF9mZWUnXTtcbiAgICAgIHRoaXMuZGVsaXZlcnlfY2hhcmdlID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlbGl2ZXJ5X2NoYXJnZSddO1xuICAgICAgdGhpcy5taW5pbXVtX29yZGVyID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ21pbmltdW1fb3JkZXInXTtcbiAgICAgIHRoaXMucGF5bWVudF9tZXRob2QgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1sncGF5bWVudF9tZXRob2QnXTtcbiAgICAgIHRoaXMuZGVsaXZlcnlfbWV0aG9kcyA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV9tZXRob2RzJ107XG4gICAgICB0aGlzLmRlbGl2ZXJ5X3RpbWUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZGVsaXZlcnlfdGltZSddO1xuICAgICAgdGhpcy5kZWZhdWx0X2N1cnJlbmN5ID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXTtcbiAgICAgIHRoaXMudW5pdF9wb2ludCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWyd1bml0X3BvaW50J107XG4gICAgICB0aGlzLmZvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZSddO1xuXG4gICAgICB0aGlzLnNlbGVjdC5hY3RpdmUgPSB0aGlzLnNlbGVjdC5pdGVtT2JqZWN0cy5maWx0ZXIoKHgpID0+IHtcbiAgICAgICAgaWYgKHRoaXMubXltb2RlbC5kZWZhdWx0X2N1cnJlbmN5ID09PSB4LnRleHQpIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICB0aGlzLnBheW1lbnRNZXRob2RTZWxlY3QuYWN0aXZlID0gdGhpcy5wYXltZW50TWV0aG9kU2VsZWN0Lml0ZW1PYmplY3RzLmZpbHRlcigoeCkgPT4ge1xuICAgICAgICBpZiAodGhpcy5teW1vZGVsLnBheW1lbnRfbWV0aG9kLnNwbGl0KCcsJykuZmlsdGVyKCh5KSA9PiB5ID09PSB4LmlkKS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgdGhpcy5kZWxpdmVyeU1ldGhvZFNlbGVjdC5hY3RpdmUgPSB0aGlzLmRlbGl2ZXJ5TWV0aG9kU2VsZWN0Lml0ZW1PYmplY3RzLmZpbHRlcigoeCkgPT4ge1xuICAgICAgICBpZiAodGhpcy5teW1vZGVsLmRlbGl2ZXJ5X21ldGhvZHMuc3BsaXQoJywnKS5maWx0ZXIoKHkpID0+IHkgPT09IHguaWQpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBidWlsZEZvcm0oKTogdm9pZCB7XG5cbiAgICB0aGlzLmR0Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgICAgJ3BheW1lbnRfZmVlJzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KV1dLFxuICAgICAgJ2RlbGl2ZXJ5X2NoYXJnZSc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NildXSxcbiAgICAgICdtaW5pbXVtX29yZGVyJzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjApLCBpbnZhbGlkRGVjaW1hbFZhbGlkYXRvcigpXV0sXG4gICAgICAncGF5bWVudF9tZXRob2QnOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgyMCldXSxcbiAgICAgICdkZWxpdmVyeV9tZXRob2RzJzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjApXV0sXG4gICAgICAnZGVsaXZlcnlfdGltZSc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWRdXSxcbiAgICAgICdkZWZhdWx0X2N1cnJlbmN5JzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNCldXSxcbiAgICAgICdmb29kX29yZGVyaW5nX3N5c3RlbV90eXBlJzogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoNCldXSxcbiAgICAgICd1bml0X3BvaW50JzogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTEpXV0sXG4gICAgfSk7XG4gICAgdGhpcy5wYXltZW50X2ZlZSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydwYXltZW50X2ZlZSddO1xuICAgIHRoaXMuZGVsaXZlcnlfY2hhcmdlID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlbGl2ZXJ5X2NoYXJnZSddO1xuICAgIHRoaXMubWluaW11bV9vcmRlciA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydtaW5pbXVtX29yZGVyJ107XG4gICAgdGhpcy5wYXltZW50X21ldGhvZCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydwYXltZW50X21ldGhvZCddO1xuICAgIHRoaXMuZGVsaXZlcnlfbWV0aG9kcyA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV9tZXRob2RzJ107XG4gICAgdGhpcy5kZWxpdmVyeV90aW1lID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlbGl2ZXJ5X3RpbWUnXTtcbiAgICB0aGlzLmRlZmF1bHRfY3VycmVuY3kgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZGVmYXVsdF9jdXJyZW5jeSddO1xuICAgIHRoaXMuZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydmb29kX29yZGVyaW5nX3N5c3RlbV90eXBlJ107XG4gICAgIHRoaXMudW5pdF9wb2ludCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWyd1bml0X3BvaW50J107XG4gICAgdGhpcy5kdEZvcm0udmFsdWVDaGFuZ2VzXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5vblZhbHVlQ2hhbmdlZChmYWxzZSwgZGF0YSkpO1xuICAgIC8vdGhpcy5vblZhbHVlQ2hhbmdlZCgpOyAvLyAocmUpc2V0IHZhbGlkYXRpb24gbWVzc2FnZXMgbm93XG4gIH1cblxuICBwcml2YXRlIG9uVmFsdWVDaGFuZ2VkKHVuRGlydHksIGRhdGE/OiBhbnkpIHtcblxuICAgIGlmICghdGhpcy5kdEZvcm0pIHsgcmV0dXJuOyB9XG4gICAgY29uc3QgZm9ybSA9IHRoaXMuZHRGb3JtO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuICAgICAgaWYgKGNvbnRyb2wgJiYgKGNvbnRyb2wuZGlydHkgfHwgdW5EaXJ0eSkgJiYgY29udHJvbC5pbnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZvcm1FcnJvcnMgPSB7XG4gICAgJ3BheW1lbnRfZmVlJzogW10sXG4gICAgJ2RlbGl2ZXJ5X2NoYXJnZSc6IFtdLFxuICAgICdtaW5pbXVtX29yZGVyJzogW10sXG4gICAgJ3BheW1lbnRfbWV0aG9kJzogW10sXG4gICAgJ2RlbGl2ZXJ5X21ldGhvZHMnOiBbXSxcbiAgICAnZGVsaXZlcnlfdGltZSc6IFtdLFxuICAgICdkZWZhdWx0X2N1cnJlbmN5JzogW10sXG4gICAgJ2Zvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGUnOiBbXSxcbiAgICAndW5pdF9wb2ludCc6IFtdXG5cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ3BheW1lbnRfZmVlJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBwYXltZW50IGZlZSBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgcGF5bWVudCBmZWUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMjU2IGNoYXJhY3RlcnMuJyxcbiAgICAgICdwYXR0ZXJuJzogJ1RoZSBwYXltZW50IGZlZSBtdXN0IGJlIGEgbnVtYmVyLidcbiAgICB9LFxuICAgICdkZWxpdmVyeV9jaGFyZ2UnOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIGRlbGl2ZXJ5IGNoYXJnZSBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgZGVsaXZlcnkgY2hhcmdlIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDI1NiBjaGFyYWN0ZXJzLicsXG4gICAgICAncGF0dGVybic6ICdUaGUgZGVsaXZlcnkgY2hhcmdlIG11c3QgYmUgYSBudW1iZXIuJ1xuICAgIH0sXG4gICAgJ21pbmltdW1fb3JkZXInOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIG1pbmltdW0gb3JkZXIgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHBheW1lbnQgZmVlIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDIwIGNoYXJhY3RlcnMuJyxcbiAgICAgICdwYXR0ZXJuJzogJ1RoZSBtaW5pbXVtIG9yZGVyIG11c3QgYmUgYSBudW1iZXIuJyxcbiAgICAgICdpbnZhbGlkRGVjaW1hbCc6ICdUaGUgb3JkZXIgbWF5IG5vdCBkZWNpbWFsICggMTUgaW5lcmdlciAsIDQgZGVjaW1hbCApJ1xuICAgIH0sXG4gICAgJ3BheW1lbnRfbWV0aG9kJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBwYXltZW50IG1ldGhvZCBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgcGF5bWVudCBtZXRob2QgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gNCBjaGFyYWN0ZXJzLidcbiAgICB9LFxuICAgICdkZWxpdmVyeV9tZXRob2RzJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBkZWxpdmVyeSBtZXRob2RzIGZpZWxkIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBkZWxpdmVyeSBtZXRob2RzIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDQgY2hhcmFjdGVycy4nXG4gICAgfSxcbiAgICAnZGVsaXZlcnlfdGltZSc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICdUaGUgZGVsaXZlcnkgdGltZSBmaWVsZCBpcyByZXF1aXJlZCAvIGZvcm1hdE51bWJlci4nXG4gICAgfSxcbiAgICAnZGVmYXVsdF9jdXJyZW5jeSc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICdUaGUgZGVmYXVsdCBjdXJyZW5jeSBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgZGVmYXVsdCBjdXJyZW5jeSBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiA0IGNoYXJhY3RlcnMuJ1xuICAgIH0sXG4gICAgJ2Zvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGUnOiB7XG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBmb29kIG9yZGVyaW5nIHN5c3RlbSB0eXBlIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDQgY2hhcmFjdGVycy4nXG4gICAgfSxcbiAgICAndW5pdF9wb2ludCc6IHtcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIGZvb2Qgb3JkZXJpbmcgc3lzdGVtIHR5cGUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nXG4gICAgfVxuXG4gIH07XG5cbiAgcHJpdmF0ZSBkb1NhdmVPcmRlclNldHRpbmcoKSB7XG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCh0cnVlKTtcbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgaWYgKHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ubGVuZ3RoID4gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMubXltb2RlbC5wYXltZW50X2ZlZSA9IHRoaXMucGF5bWVudF9mZWUudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmRlbGl2ZXJ5X2NoYXJnZSA9IHRoaXMuZGVsaXZlcnlfY2hhcmdlLnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5taW5pbXVtX29yZGVyID0gdGhpcy5taW5pbXVtX29yZGVyLnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5wYXltZW50X21ldGhvZCA9IHRoaXMucGF5bWVudF9tZXRob2QudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmRlbGl2ZXJ5X21ldGhvZHMgPSB0aGlzLmRlbGl2ZXJ5X21ldGhvZHMudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmRlbGl2ZXJ5X3RpbWUgPSB0aGlzLmRlbGl2ZXJ5X3RpbWUudmFsdWUudG9Mb2NhbGVUaW1lU3RyaW5nKFtdLCB7IGhvdXIxMjogZmFsc2UgfSk7XG4gICAgdGhpcy5teW1vZGVsLmRlZmF1bHRfY3VycmVuY3kgPSB0aGlzLmRlZmF1bHRfY3VycmVuY3kudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmZvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGUgPSB0aGlzLmZvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGUudmFsdWU7XG4gICAgIHRoaXMubXltb2RlbC51bml0X3BvaW50ID0gdGhpcy51bml0X3BvaW50LnZhbHVlO1xuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAndXNlcl9pZCc6IHRoaXMudXNlcklkLFxuICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICdvcmRlcl9zZXR0aW5nJzogdGhpcy5teW1vZGVsXG4gICAgfTtcbiAgICB0aGlzLnJlc3RTZXJ2LnVwZGF0ZU9yZGVyU2V0dGluZyhwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgbGV0IGhhckVycm9yID0gZmFsc2U7XG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHJlc3BvbnNlLmVycm9ycykge1xuICAgICAgICBoYXJFcnJvciA9IHRydWU7XG4gICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSByZXNwb25zZS5lcnJvcnNbZmllbGRdO1xuICAgICAgfVxuICAgICAgaWYgKGhhckVycm9yKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCdDb250YWN0IGhhcyBiZWVuIHVwZGF0ZWQnKTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH07XG5cbiAgcHVibGljIG9uQ2hhbmdlKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnQ2hhbmdlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIHNlbGVjdGVkKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnU2VsZWN0ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxuXG4gIHB1YmxpYyByZW1vdmVkKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnUmVtb3ZlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIHR5cGVkKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIHJlZnJlc2hWYWx1ZSh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXS5zZXRWYWx1ZSh2YWx1ZS50ZXh0KTtcbiAgICB0aGlzLmRlZmF1bHRfY3VycmVuY3kgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZGVmYXVsdF9jdXJyZW5jeSddO1xuICB9XG5cbiAgcHVibGljIHJlZnJlc2hQYXltZW50TWV0aG9kVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydwYXltZW50X21ldGhvZCddLnNldFZhbHVlKHZhbHVlLm1hcChmdW5jdGlvbiAoZWxlbSkgeyByZXR1cm4gZWxlbS5pZDsgfSkuam9pbignLCcpKTtcbiAgICB0aGlzLnBheW1lbnRfbWV0aG9kID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ3BheW1lbnRfbWV0aG9kJ107XG4gIH1cblxuICBwdWJsaWMgcmVmcmVzaERlbGl2ZXJ5TWV0aG9kVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV9tZXRob2RzJ10uc2V0VmFsdWUodmFsdWUubWFwKGZ1bmN0aW9uIChlbGVtKSB7IHJldHVybiBlbGVtLmlkOyB9KS5qb2luKCcsJykpO1xuICAgIHRoaXMuZGVsaXZlcnlfbWV0aG9kcyA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV9tZXRob2RzJ107XG4gIH1cbiAgIHB1YmxpYyBvbkNoYW5nZURlbGl2ZXJ5VmFsdWUoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCB2YWx1ZTE9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICBjb25zb2xlLmxvZygnQ2hhbmdlZCB2YWx1ZSBpczogJywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICBpZih2YWx1ZTE9PVwiXCJ8fCB2YWx1ZTE9PW51bGwpe1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlbGl2ZXJ5X3RpbWUnXS5zZXRWYWx1ZShudWxsKTtcbiAgICAgICB0aGlzLmRlbGl2ZXJ5X3RpbWU9dGhpcy5kdEZvcm0uY29udHJvbHNbJ2RlbGl2ZXJ5X3RpbWUnXTtcbiAgICB9XG4gICAgY29uc3QgbmFtZVJlID0gbmV3IFJlZ0V4cCgvXlswLTldKyQvaSk7XG4gICAgaWYoIW5hbWVSZS50ZXN0KHZhbHVlMSkgfHwgdmFsdWUxLmxlbmd0aD4yKXtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV90aW1lJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgICAgdGhpcy5kZWxpdmVyeV90aW1lPXRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWxpdmVyeV90aW1lJ107XG4gICAgfVxuICB9XG59Il19
