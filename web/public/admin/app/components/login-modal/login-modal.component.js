"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_1 = require("app/models/user");
var user_service_1 = require("app/services/user.service");
var server_service_1 = require("app/services/server.service");
var router_animations_1 = require("../../router.animations");
var utils_1 = require("app/utils/utils");
var auth_service_1 = require("app/services/auth.service");
var ng2_translate_1 = require("ng2-translate");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var router_1 = require("@angular/router");
var LoginModalComponent = (function () {
    function LoginModalComponent(serverServ, userServ, authServ, router, translateService) {
        this.serverServ = serverServ;
        this.userServ = userServ;
        this.authServ = authServ;
        this.router = router;
        this.translateService = translateService;
    }
    LoginModalComponent.prototype.ngOnInit = function () {
    };
    LoginModalComponent.prototype.show = function () {
        this.childModal.show();
    };
    LoginModalComponent.prototype.hide = function () {
        this.childModal.hide();
    };
    LoginModalComponent.prototype.gotoDashboard = function (rest) {
        var restaurant = [rest];
        console.log('restaurant ', restaurant);
        var loginedUser = new user_1.User({
            email: this.user.email,
            firstname: this.user.first_name,
            lastname: this.user.last_name,
            roles: this.user.roles,
            phone: this.user.phone,
            tel: this.user.tel
        }, restaurant);
        // set token
        this.authServ.setToken(this.response.token);
        loginedUser.isInit = true;
        console.log('loginedUser ', loginedUser);
        this.userServ.setCurrentUser(loginedUser);
        this.response.restaurants = restaurant;
        this.userServ.setUserInfo(this.response);
        this.childModal.hide();
        this.router.navigateByUrl('/dashboard');
    };
    __decorate([
        core_1.ViewChild('childModal'),
        __metadata("design:type", ngx_bootstrap_1.ModalDirective)
    ], LoginModalComponent.prototype, "childModal", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoginModalComponent.prototype, "title", void 0);
    LoginModalComponent = __decorate([
        core_1.Component({
            selector: 'login-modal',
            styleUrls: [utils_1.default.getView('app/components/login-modal/login-modal.component.css')],
            templateUrl: utils_1.default.getView('app/components/login-modal/login-modal.component.html'),
            animations: [router_animations_1.routerGrow()],
            host: { '[@routerGrow]': '' }
        }),
        __metadata("design:paramtypes", [server_service_1.ServerService,
            user_service_1.UserService,
            auth_service_1.AuthService,
            router_1.Router,
            ng2_translate_1.TranslateService])
    ], LoginModalComponent);
    return LoginModalComponent;
}());
exports.LoginModalComponent = LoginModalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2xvZ2luLW1vZGFsL2xvZ2luLW1vZGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUErRTtBQUUvRSx3Q0FBdUM7QUFDdkMsMERBQXdEO0FBQ3hELDhEQUE0RDtBQUM1RCw2REFBcUQ7QUFHckQseUNBQW9DO0FBRXBDLDBEQUF3RDtBQUl4RCwrQ0FBaUQ7QUFFakQsK0NBQTREO0FBQzVELDBDQUF5QztBQVl6QztJQU9FLDZCQUNTLFVBQXlCLEVBQ3hCLFFBQXFCLEVBQ3JCLFFBQXFCLEVBQ3JCLE1BQWMsRUFFZCxnQkFBa0M7UUFMbkMsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUN4QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUVkLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFHNUMsQ0FBQztJQUVNLHNDQUFRLEdBQWY7SUFFQSxDQUFDO0lBRUQsa0NBQUksR0FBSjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUNELGtDQUFJLEdBQUo7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCwyQ0FBYSxHQUFiLFVBQWUsSUFBSTtRQUNmLElBQUksVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDdkMsSUFBSSxXQUFXLEdBQUcsSUFBSSxXQUFJLENBQUU7WUFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSztZQUN0QixTQUFTLEVBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVO1lBQ2hDLFFBQVEsRUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVM7WUFDOUIsS0FBSyxFQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSztZQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLO1lBQ3RCLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUc7U0FDckIsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUVqQixZQUFZO1FBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBRSxXQUFXLENBQUUsQ0FBQztRQUU1QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7UUFFdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBRSxDQUFDO1FBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQW5Ed0I7UUFBeEIsZ0JBQVMsQ0FBQyxZQUFZLENBQUM7a0NBQW1CLDhCQUFjOzJEQUFDO0lBQ2pEO1FBQVIsWUFBSyxFQUFFOztzREFBZTtJQUhaLG1CQUFtQjtRQVIvQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGFBQWE7WUFDdkIsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxzREFBc0QsQ0FBQyxDQUFDO1lBQ2xGLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLHVEQUF1RCxDQUFDO1lBQ25GLFVBQVUsRUFBRSxDQUFDLDhCQUFVLEVBQUUsQ0FBQztZQUMxQixJQUFJLEVBQUUsRUFBQyxlQUFlLEVBQUUsRUFBRSxFQUFDO1NBRTVCLENBQUM7eUNBU3FCLDhCQUFhO1lBQ2QsMEJBQVc7WUFDWCwwQkFBVztZQUNiLGVBQU07WUFFSSxnQ0FBZ0I7T0FiakMsbUJBQW1CLENBdUQvQjtJQUFELDBCQUFDO0NBdkRELEFBdURDLElBQUE7QUF2RFksa0RBQW1CIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL2xvZ2luLW1vZGFsL2xvZ2luLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIFZpZXdDaGlsZCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtICxBYnN0cmFjdENvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgVXNlciB9IGZyb20gJ2FwcC9tb2RlbHMvdXNlcic7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc2VydmVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyByb3V0ZXJHcm93IH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xyXG5cclxuXHJcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xyXG5pbXBvcnQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50JylcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL2F1dGguc2VydmljZVwiO1xyXG5pbXBvcnQgeyBpbnZhbGlkRW1haWxWYWxpZGF0b3IgfSBmcm9tIFwiYXBwL3V0aWxzL2VtYWlsLXZhbGlkYXRvci5kaXJlY3RpdmVcIjtcclxuaW1wb3J0IHsgZGVmYXVsdCBhcyBzd2FsIH0gZnJvbSAnc3dlZXRhbGVydDInO1xyXG5pbXBvcnQgeyBTZWxlY3RDb21wb25lbnQgfSBmcm9tICduZzItc2VsZWN0JztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ25nMi10cmFuc2xhdGUnO1xyXG5pbXBvcnQgeyBFTlZJUk9OTUVOVCB9IGZyb20gJ2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XHJcbmltcG9ydCB7IE1vZGFsTW9kdWxlICwgTW9kYWxEaXJlY3RpdmV9IGZyb20gJ25neC1ib290c3RyYXAnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xvZ2luLW1vZGFsJyxcclxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9sb2dpbi1tb2RhbC9sb2dpbi1tb2RhbC5jb21wb25lbnQuY3NzJyldLFxyXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9sb2dpbi1tb2RhbC9sb2dpbi1tb2RhbC5jb21wb25lbnQuaHRtbCcpLFxyXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJHcm93KCldLFxyXG4gIGhvc3Q6IHsnW0Byb3V0ZXJHcm93XSc6ICcnfVxyXG5cclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ2luTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBAVmlld0NoaWxkKCdjaGlsZE1vZGFsJykgcHVibGljIGNoaWxkTW9kYWw6TW9kYWxEaXJlY3RpdmU7XHJcbiAgQElucHV0KCkgdGl0bGU/OnN0cmluZztcclxuICBwdWJsaWMgdXNlcjogYW55O1xyXG4gIHB1YmxpYyByZXN0YXVyYW50czogYW55IFtdO1xyXG4gIHB1YmxpYyByZXNwb25zZTogYW55O1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcclxuICAgIHByaXZhdGUgYXV0aFNlcnY6IEF1dGhTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgXHJcbiAgICBwcml2YXRlIHRyYW5zbGF0ZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2VcclxuICApIHtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgcHVibGljIG5nT25Jbml0KCkge1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBzaG93KCl7XHJcbiAgICB0aGlzLmNoaWxkTW9kYWwuc2hvdygpO1xyXG4gIH1cclxuICBoaWRlKCl7XHJcbiAgICB0aGlzLmNoaWxkTW9kYWwuaGlkZSgpO1xyXG4gIH1cclxuXHJcbiAgZ290b0Rhc2hib2FyZCAocmVzdCkge1xyXG4gICAgICB2YXIgcmVzdGF1cmFudCA9IFtyZXN0XTtcclxuICAgICAgY29uc29sZS5sb2coJ3Jlc3RhdXJhbnQgJywgcmVzdGF1cmFudCk7XHJcbiAgICAgIGxldCBsb2dpbmVkVXNlciA9IG5ldyBVc2VyKCB7ICAgICAgICAgICBcclxuICAgICAgICAgICAgZW1haWw6IHRoaXMudXNlci5lbWFpbCxcclxuICAgICAgICAgICAgZmlyc3RuYW1lOiAgdGhpcy51c2VyLmZpcnN0X25hbWUsXHJcbiAgICAgICAgICAgIGxhc3RuYW1lOiAgdGhpcy51c2VyLmxhc3RfbmFtZSxcclxuICAgICAgICAgICAgcm9sZXM6ICB0aGlzLnVzZXIucm9sZXMsXHJcbiAgICAgICAgICAgIHBob25lOiB0aGlzLnVzZXIucGhvbmUsXHJcbiAgICAgICAgICAgIHRlbDogdGhpcy51c2VyLnRlbFxyXG4gICAgICAgIH0sIHJlc3RhdXJhbnQpO1xyXG5cclxuICAgICAgLy8gc2V0IHRva2VuXHJcbiAgICAgIHRoaXMuYXV0aFNlcnYuc2V0VG9rZW4odGhpcy5yZXNwb25zZS50b2tlbik7IFxyXG4gICAgICBsb2dpbmVkVXNlci5pc0luaXQgPSB0cnVlO1xyXG4gICAgICBjb25zb2xlLmxvZygnbG9naW5lZFVzZXIgJywgbG9naW5lZFVzZXIpO1xyXG4gICAgICB0aGlzLnVzZXJTZXJ2LnNldEN1cnJlbnRVc2VyKCBsb2dpbmVkVXNlciApO1xyXG4gICAgICBcclxuICAgICAgdGhpcy5yZXNwb25zZS5yZXN0YXVyYW50cyA9IHJlc3RhdXJhbnQ7XHJcbiAgXHJcbiAgICAgIHRoaXMudXNlclNlcnYuc2V0VXNlckluZm8oIHRoaXMucmVzcG9uc2UgKTtcclxuICAgICAgdGhpcy5jaGlsZE1vZGFsLmhpZGUoKTtcclxuXHJcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9kYXNoYm9hcmQnKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==
