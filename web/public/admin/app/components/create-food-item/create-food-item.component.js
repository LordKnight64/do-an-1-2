"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var category_service_1 = require("app/services/category.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var foodItem_service_1 = require("app/services/foodItem.service");
var ng2_select_1 = require("ng2-select");
var number_validator_directive_1 = require("app/utils/number-validator.directive");
var optionValueFoodItem_service_1 = require("app/services/optionValueFoodItem.service");
var optionFoodItem_service_1 = require("app/services/optionFoodItem.service");
var CreateFoodItemComponent = (function () {
    // private description:string;
    function CreateFoodItemComponent(OptionFoodItemService, OptionValueFoodItemService, fb, FoodItemService, categoryService, StorageService, authServ, notif, router, route) {
        this.OptionFoodItemService = OptionFoodItemService;
        this.OptionValueFoodItemService = OptionValueFoodItemService;
        this.fb = fb;
        this.FoodItemService = FoodItemService;
        this.categoryService = categoryService;
        this.StorageService = StorageService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.optionALLItemValueFoodItems = [];
        this.optionItemLst = [];
        this.optionItemTempLst = [];
        this.formErrors = {
            'name': [],
            'price': [],
            'category_id': [],
            'sku': [],
            'point': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            },
            'price': {
                'required': 'The price is required.',
                'maxlength': 'The price must be at less 10 characters long.',
                'invalidNumber': 'he price must be number'
            },
            'category_id': {
                'required': 'The category is required.'
            },
            'sku': {
                'required': 'The sku is required.',
                'maxlength': 'The name must be at less 128 characters long.'
            },
            'point': {
                'required': 'The point is required.',
                'maxlength': 'The point must be at less 10 characters long.',
                'invalidNumber': 'he point must be number'
            }
        };
        /*upload file*/
        this.sactiveColor = 'green';
        this.baseColor = '#ccc';
        this.overlayColor = 'rgba(255,255,255,0.5)';
        this.dragging = false;
        this.loaded = false;
        this.imageLoaded = false;
        this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
        this.flgImageChoose = false;
        this.iconColor = '';
        this.borderColor = '';
        this.activeColor = '';
        this.hiddenImage = false;
        /*select start*/
        this.items = [];
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    CreateFoodItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.fetchAllCategories(this.restId);
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
        this.foodType = '1';
        this.isAlcoho = '1';
        if (this.id != undefined && this.id != null) {
            this.foodItem = this.StorageService.getScope();
            if (this.foodItem == false) {
                this.fetchAllItem(this.restId, null, this.id);
            }
            else {
                this.createItemForm.controls['name'].setValue(this.foodItem.name);
                this.createItemForm.controls['price'].setValue(this.foodItem.price);
                this.createItemForm.controls['category_id'].setValue(this.foodItem.category_id);
                this.createItemForm.controls['sku'].setValue(this.foodItem.sku);
                this.createItemForm.controls['point'].setValue(this.foodItem.point);
                this.createItemForm.controls['description'].setValue(this.foodItem.description);
                this.description = this.createItemForm.controls['description'];
                this.name = this.createItemForm.controls['name'];
                this.price = this.createItemForm.controls['price'];
                this.category_id = this.createItemForm.controls['category_id'];
                this.sku = this.createItemForm.controls['sku'];
                this.point = this.createItemForm.controls['point'];
                this.imageSrc = this.foodItem.thumb;
                this.foodType = this.foodItem.food_type;
                this.isAlcoho = this.foodItem.is_alcoho;
                // this.description=this.foodItem.description;
                this.nameRadio = this.foodItem.active_flg;
                if (this.imageSrc == null) {
                    this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                }
                this.nameRadio = this.foodItem.active_flg;
                this.isCheckRadio = this.foodItem.active_flg;
                if (this.foodItem != null) {
                    this.optionItemLst = this.foodItem.option_items;
                    //console.log('optionItemLst ', this.optionItemLst);
                    if (this.optionItemLst != null && this.optionItemLst.length > 0) {
                        for (var i = 0; i < this.optionItemLst.length; i++) {
                            this.optionItemTempLst.push({
                                option_id: this.optionItemLst[i].option_id,
                                option_value_id: this.optionItemLst[i].option_value_id,
                                option_name: this.optionItemLst[i].option_name,
                                option_value_name: this.optionItemLst[i].option_value_name,
                                add_price: this.optionItemLst[i].add_price,
                                point: this.optionItemLst[i].point,
                                active_flg: this.optionItemLst[i].active_flg,
                                old_flg: 1,
                                delete_flg: 0,
                            });
                        }
                    }
                }
                this.fetchAllOptionFoodItems(this.restId);
            }
        }
        else {
            this.fetchAllOptionFoodItems(this.restId);
        }
    };
    CreateFoodItemComponent.prototype.fetchAllCategories = function (restId) {
        var _this = this;
        this.categoryService.getCategories(restId, null).then(function (response) {
            _this.categories = response.categories;
            _this.items.push({
                id: 0,
                text: "choose value"
            });
            var flgitemChoose = null;
            for (var i = 0; i < _this.categories.length; i++) {
                _this.items.push({
                    id: _this.categories[i].id,
                    text: "" + _this.categories[i].name
                });
                if (_this.foodItem != undefined && _this.foodItem != null && _this.foodItem.category_id == _this.categories[i].id) {
                    flgitemChoose = i + 1;
                }
            }
            _this.select.items = _this.items;
            if (flgitemChoose != null) {
                _this.value = _this.items[flgitemChoose];
            }
            else {
                _this.value = _this.items[0];
                _this.createItemForm.controls['category_id'].setValue(null);
                _this.category_id = _this.createItemForm.controls['category_id'];
            }
            console.log(response.categories);
        }, function (error) {
            console.log(error);
        });
    };
    CreateFoodItemComponent.prototype.fetchAllItem = function (restId, categoryId, id) {
        var _this = this;
        this.FoodItemService.getFoodItems(restId, categoryId, id).then(function (response) {
            _this.foodItem = response.food_items[0];
            console.log('this.foodItem', _this.foodItem);
            _this.fetchAllCategories(_this.restId);
            _this.fetchAllOptionFoodItems(_this.restId);
            if (_this.foodItem != null) {
                _this.optionItemLst = _this.foodItem.option_items;
                _this.createItemForm.controls['name'].setValue(_this.foodItem.name);
                _this.createItemForm.controls['price'].setValue(_this.foodItem.price);
                _this.createItemForm.controls['category_id'].setValue(_this.foodItem.category_id);
                _this.createItemForm.controls['sku'].setValue(_this.foodItem.sku);
                _this.createItemForm.controls['point'].setValue(_this.foodItem.point);
                _this.createItemForm.controls['description'].setValue(_this.foodItem.description);
                _this.description = _this.createItemForm.controls['description'];
                _this.name = _this.createItemForm.controls['name'];
                _this.price = _this.createItemForm.controls['price'];
                _this.point = _this.createItemForm.controls['point'];
                _this.category_id = _this.createItemForm.controls['category_id'];
                _this.sku = _this.createItemForm.controls['sku'];
                _this.imageSrc = _this.foodItem.thumb;
                _this.foodType = _this.foodItem.food_type;
                _this.isAlcoho = _this.foodItem.is_alcoho;
                // this.description=this.foodItem.description;
                _this.nameRadio = _this.foodItem.active_flg;
                if (_this.imageSrc == null) {
                    _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                }
                _this.isCheckRadio = _this.foodItem.active_flg;
                _this.value = _this.items[_this.foodItem.category_id];
                if (_this.optionItemLst != null && _this.optionItemLst.length > 0) {
                    for (var i = 0; i < _this.optionItemLst.length; i++) {
                        _this.optionItemTempLst.push({
                            option_id: _this.optionItemLst[i].option_id,
                            option_value_id: _this.optionItemLst[i].option_value_id,
                            option_name: _this.optionItemLst[i].option_name,
                            option_value_name: _this.optionItemLst[i].option_value_name,
                            add_price: _this.optionItemLst[i].add_price,
                            point: _this.optionItemLst[i].point,
                            active_flg: _this.optionItemLst[i].active_flg,
                            old_flg: 1,
                            delete_flg: 0,
                        });
                    }
                }
            }
            console.log(response.foodItem);
        }, function (error) {
            console.log(error);
        });
    };
    CreateFoodItemComponent.prototype.fetchAllOptionFoodItems = function (restId) {
        var _this = this;
        this.OptionFoodItemService.getOptionFoodItems(restId, null).then(function (response) {
            _this.optionFoodItems = response.options;
            _this.fetchAllOptionValueFoodItems(_this.restId);
            console.log(response.options);
        }, function (error) {
            console.log(error);
        });
    };
    CreateFoodItemComponent.prototype.fetchAllOptionValueFoodItems = function (restId) {
        var _this = this;
        this.OptionValueFoodItemService.getOptionValueFoodItems(restId, null).then(function (response) {
            _this.optionValueFoodItems = response.options;
            if (_this.optionFoodItems != null && _this.optionFoodItems.length > 0 && _this.optionValueFoodItems != null && _this.optionValueFoodItems.length > 0) {
                for (var i = 0; i < _this.optionFoodItems.length; i++) {
                    for (var j = 0; j < _this.optionValueFoodItems.length; j++) {
                        if (_this.optionFoodItems[i].id == _this.optionValueFoodItems[j].option_id) {
                            _this.optionALLItemValueFoodItems.push({
                                option_id: _this.optionFoodItems[i].id,
                                option_value_id: _this.optionValueFoodItems[j].id,
                                option_name: _this.optionFoodItems[i].name,
                                option_value_name: _this.optionValueFoodItems[j].name,
                                add_price: null,
                                point: null,
                                active_flg: 1,
                                old_flg: 0,
                                delete_flg: 0,
                                checkbox_flg: false
                            });
                        }
                    }
                }
            }
            if (_this.foodItem != null) {
                _this.optionItemLst = _this.foodItem.option_items;
                console.log('optionItemLst ', _this.optionItemLst);
                if (_this.optionItemLst != null && _this.optionItemLst.length > 0) {
                    for (var i = 0; i < _this.optionItemLst.length; i++) {
                        for (var j = 0; j < _this.optionALLItemValueFoodItems.length; j++) {
                            if (_this.optionItemLst[i].option_id == _this.optionALLItemValueFoodItems[j].option_id && _this.optionItemLst[i].option_value_id == _this.optionALLItemValueFoodItems[j].option_value_id) {
                                _this.optionALLItemValueFoodItems[j].add_price = _this.optionItemLst[i].add_price;
                                _this.optionALLItemValueFoodItems[j].point = _this.optionItemLst[i].point;
                                _this.optionALLItemValueFoodItems[j].active_flg = _this.optionItemLst[i].active_flg;
                                _this.optionALLItemValueFoodItems[j].old_flg = _this.optionItemLst[i].old_flg;
                                _this.optionALLItemValueFoodItems[j].checkbox_flg = true;
                            }
                        }
                    }
                }
            }
            console.log(response.options);
        }, function (error) {
            console.log(error);
        });
    };
    CreateFoodItemComponent.prototype.buildForm = function () {
        var _this = this;
        this.createItemForm = this.fb.group({
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ], 'price': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(10),
                    number_validator_directive_1.invalidNumberValidator()
                ]
            ],
            'category_id': ['', [
                    forms_1.Validators.required
                ]
            ],
            'sku': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(128),
                ]
            ],
            'point': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(128),
                ]
            ],
            'description': []
        });
        this.name = this.createItemForm.controls['name'];
        this.price = this.createItemForm.controls['price'];
        this.category_id = this.createItemForm.controls['category_id'];
        this.sku = this.createItemForm.controls['sku'];
        this.point = this.createItemForm.controls['point'];
        this.description = this.createItemForm.controls['description'];
        this.createItemForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    CreateFoodItemComponent.prototype.onValueChanged = function (data) {
        if (!this.createItemForm) {
            return;
        }
        var form = this.createItemForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    CreateFoodItemComponent.prototype.numberValue = function (max) {
        return function (control) {
            var input = control.value, isValid = input > max;
            if (isValid)
                return { 'maxValue': { max: max } };
            else
                return null;
        };
    };
    CreateFoodItemComponent.prototype.createOptionValueFoodItem = function () {
        this.router.navigateByUrl('option-food-items/list');
    };
    CreateFoodItemComponent.prototype.checkFlg = function (value) {
        if (this.isCheckRadio == undefined || this.isCheckRadio == null) {
            this.isCheckRadio = '1';
            this.nameRadio = '1';
        }
        if (value == this.isCheckRadio) {
            return true;
        }
    };
    CreateFoodItemComponent.prototype.changenameRadio = function (value) {
        this.nameRadio = value;
    };
    CreateFoodItemComponent.prototype.checkfoodType = function () {
        if (this.foodType == '1') {
            return true;
        }
        return false;
    };
    CreateFoodItemComponent.prototype.changefoodType = function (event) {
        var valuecheck = event.target.checked;
        if (valuecheck == true) {
            this.foodType = '1';
        }
        else {
            this.foodType = '0';
        }
    };
    CreateFoodItemComponent.prototype.checkIsAlcoho = function () {
        if (this.isAlcoho == '1') {
            return true;
        }
        return false;
    };
    CreateFoodItemComponent.prototype.changeIsAlcoho = function (event) {
        var valuecheck = event.target.checked;
        if (valuecheck == true) {
            this.isAlcoho = '1';
        }
        else {
            this.isAlcoho = '0';
        }
    };
    CreateFoodItemComponent.prototype.checkActiveFlg = function (value, optionValue) {
        //TODO
        if (this.optionItemTempLst != null && this.optionItemTempLst.length > 0) {
            for (var i = 0; i < this.optionItemTempLst.length; i++) {
                if (this.optionItemTempLst[i].option_id == optionValue.option_id && this.optionItemTempLst[i].option_value_id == optionValue.option_value_id) {
                    if (value == this.optionItemTempLst[i].active_flg) {
                        return true;
                    }
                }
            }
        }
        return false;
    };
    CreateFoodItemComponent.prototype.checkboxFlg = function (optionValue) {
        if (this.optionItemTempLst != null && this.optionItemTempLst.length > 0) {
            for (var i = 0; i < this.optionItemTempLst.length; i++) {
                if (this.optionItemTempLst[i].option_id == optionValue.option_id && this.optionItemTempLst[i].option_value_id == optionValue.option_value_id) {
                    return true;
                }
            }
        }
        return false;
    };
    CreateFoodItemComponent.prototype.setValuePrice = function (optionId, optionValueId) {
        if (this.optionItemTempLst != null && this.optionItemTempLst.length > 0) {
            for (var i = 0; i < this.optionItemTempLst.length; i++) {
                if (this.optionItemTempLst[i].option_id == optionId && this.optionItemTempLst[i].option_value_id == optionValueId) {
                    return this.optionItemTempLst[i].add_price;
                }
            }
        }
        return null;
    };
    CreateFoodItemComponent.prototype.setValuePoint = function (optionId, optionValueId) {
        if (this.optionItemTempLst != null && this.optionItemTempLst.length > 0) {
            for (var i = 0; i < this.optionItemTempLst.length; i++) {
                if (this.optionItemTempLst[i].option_id == optionId && this.optionItemTempLst[i].option_value_id == optionValueId) {
                    return this.optionItemTempLst[i].point;
                }
            }
        }
        return null;
    };
    CreateFoodItemComponent.prototype.changeCheckboxFlg = function (event, optionValueItem) {
        var valuecheck = event.target.checked;
        var itemCheck = this.checkExit(this.optionItemTempLst, optionValueItem);
        if (itemCheck != null) {
            if (valuecheck != true) {
                this.optionItemTempLst.splice(itemCheck, 1);
            }
        }
        else {
            if (valuecheck == true) {
                this.optionItemTempLst.push({
                    option_id: optionValueItem.option_id,
                    option_value_id: optionValueItem.option_value_id,
                    option_name: optionValueItem.option_name,
                    option_value_name: optionValueItem.option_value_name,
                    add_price: 0,
                    point: 0,
                    active_flg: 1,
                    old_flg: 0,
                    delete_flg: 0
                });
                console.log(this.optionItemTempLst);
            }
        }
        return false;
    };
    CreateFoodItemComponent.prototype.changeActiveFlg = function (event, value, optionValueItem) {
        var valuecheck = event.target.checked;
        var itemCheck = this.checkExit(this.optionItemTempLst, optionValueItem);
        if (itemCheck != null) {
            if (value == 1) {
                this.optionItemTempLst[itemCheck].active_flg = 1;
            }
            else {
                this.optionItemTempLst[itemCheck].active_flg = 0;
            }
        }
    };
    CreateFoodItemComponent.prototype.changeaddPrice = function (event, optionValueItem) {
        var value = event.target.value;
        var itemCheck = this.checkExit(this.optionItemTempLst, optionValueItem);
        if (itemCheck != null) {
            if (value != null) {
                this.optionItemTempLst[itemCheck].add_price = value;
            }
            else {
                this.optionItemTempLst[itemCheck].value = 0;
            }
        }
    };
    CreateFoodItemComponent.prototype.changeaddPoint = function (event, optionValueItem) {
        var value = event.target.value;
        var itemCheck = this.checkExit(this.optionItemTempLst, optionValueItem);
        if (itemCheck != null) {
            if (value != null) {
                this.optionItemTempLst[itemCheck].point = value;
            }
            else {
                this.optionItemTempLst[itemCheck].value = 0;
            }
        }
    };
    CreateFoodItemComponent.prototype.checkExit = function (optionItemLst, optionValueItem) {
        if (optionItemLst != null && optionItemLst.length > 0) {
            for (var i = 0; i < optionItemLst.length; i++) {
                if (optionItemLst[i].option_id == optionValueItem.option_id && optionItemLst[i].option_value_id == optionValueItem.option_value_id) {
                    return i;
                }
            }
        }
        return null;
    };
    CreateFoodItemComponent.prototype.isDisabled = function (optionValue) {
        if (this.optionItemTempLst != null && this.optionItemTempLst.length > 0) {
            for (var i = 0; i < this.optionItemTempLst.length; i++) {
                if (this.optionItemTempLst[i].option_id == optionValue.option_id && this.optionItemTempLst[i].option_value_id == optionValue.option_value_id) {
                    return false;
                }
            }
        }
        return true;
    };
    CreateFoodItemComponent.prototype.createItem = function () {
        var _this = this;
        var id = '-1';
        if (this.id != null) {
            id = this.id;
        }
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        var params = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'item': {
                'id': id,
                'category_id': this.createItemForm.value.category_id,
                'name': this.createItemForm.value.name,
                'price': this.createItemForm.value.price,
                'thumb': '',
                'sku': this.createItemForm.value.sku,
                'point': this.createItemForm.value.point,
                'is_active': this.nameRadio,
                'is_alcoho': this.isAlcoho,
                'food_type': this.foodType,
                'description': this.createItemForm.value.description,
                'is_delete': 0,
                'update_in_lst_flg': 0,
                'option_items': this.optionItemTempLst,
            }
        };
        this.FoodItemService.createFoodItems(params).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    CreateFoodItemComponent.prototype.processResult = function (response) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            var imagePost = '';
            if (this.flgImageChoose == true) {
                imagePost = this.imageSrc;
            }
            var itemUpload = {
                'id': response.item_id,
                'thumb': imagePost
            };
            this.FoodItemService.itemUploadFile(itemUpload).then(function (response) {
                _this.notif.success('New Food Item has been added');
                _this.router.navigateByUrl('food-items/list');
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.name) {
                    this.formErrors['name'] = this.error.name;
                }
                if (this.error.price) {
                    this.formErrors['price'] = this.error.price;
                }
                if (this.error.category_id) {
                    this.formErrors['category_id'] = this.error.category_id;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    CreateFoodItemComponent.prototype.failedCreate = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    CreateFoodItemComponent.prototype.handleDragEnter = function () {
        this.dragging = true;
    };
    CreateFoodItemComponent.prototype.handleDragLeave = function () {
        this.dragging = false;
    };
    CreateFoodItemComponent.prototype.handleDrop = function (e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    };
    CreateFoodItemComponent.prototype.handleImageLoad = function () {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if (this.imageLoaded = true) {
            this.hiddenImage = true;
        }
    };
    CreateFoodItemComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        this.loaded = false;
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose = true;
    };
    CreateFoodItemComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    };
    CreateFoodItemComponent.prototype._setActive = function () {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    };
    CreateFoodItemComponent.prototype._setInactive = function () {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    };
    Object.defineProperty(CreateFoodItemComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    CreateFoodItemComponent.prototype.selected = function (value) {
        this.createItemForm.controls['category_id'].setValue(value.id);
        this.category_id = this.createItemForm.controls['category_id'];
        console.log('Selected value is: ', value);
    };
    CreateFoodItemComponent.prototype.removed = function (value) {
        this.createItemForm.controls['category_id'].setValue(null);
        this.category_id = this.createItemForm.controls['category_id'];
        console.log('Removed value is: ', value);
    };
    CreateFoodItemComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    CreateFoodItemComponent.prototype.refreshValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.value = value;
        }
        // this.value = value;
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], CreateFoodItemComponent.prototype, "select", void 0);
    CreateFoodItemComponent = __decorate([
        core_1.Component({
            selector: 'create-food-item',
            templateUrl: utils_1.default.getView('app/components/create-food-item/create-food-item.component.html'),
            styleUrls: [utils_1.default.getView('app/components/create-food-item/create-food-item.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [optionFoodItem_service_1.OptionFoodItemService,
            optionValueFoodItem_service_1.OptionValueFoodItemService,
            forms_1.FormBuilder, foodItem_service_1.FoodItemService,
            category_service_1.CategoryService,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router, router_2.ActivatedRoute])
    ], CreateFoodItemComponent);
    return CreateFoodItemComponent;
}());
exports.CreateFoodItemComponent = CreateFoodItemComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NyZWF0ZS1mb29kLWl0ZW0vY3JlYXRlLWZvb2QtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBOEU7QUFDOUUsd0NBQTRGO0FBQzVGLHlDQUFvQztBQUdwQyw2REFBMkQ7QUFDM0Qsa0VBQWdFO0FBQ2hFLDBEQUF5RDtBQUN6RCwyQ0FBOEM7QUFDOUMsMEVBQXdFO0FBQ3hFLDBDQUF5QztBQUN6QywwQ0FBaUQ7QUFDakQsZ0VBQThEO0FBQzlELGtFQUFnRTtBQUNoRSx5Q0FBd0Q7QUFDeEQsbUZBQThFO0FBQzlFLHdGQUFzRjtBQUN0Riw4RUFBNEU7QUFTNUU7SUFzQkUsOEJBQThCO0lBQzlCLGlDQUFvQixxQkFBNEMsRUFDeEQsMEJBQXNELEVBQ3RELEVBQWUsRUFBUyxlQUErQixFQUN2RCxlQUFnQyxFQUNoQyxjQUE4QixFQUM5QixRQUFxQixFQUNyQixLQUEwQixFQUN6QixNQUFjLEVBQVMsS0FBcUI7UUFQakMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUN4RCwrQkFBMEIsR0FBMUIsMEJBQTBCLENBQTRCO1FBQ3RELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBUyxvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFDdkQsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFVBQUssR0FBTCxLQUFLLENBQXFCO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQWQ3QyxnQ0FBMkIsR0FBZ0IsRUFBRSxDQUFDO1FBQzlDLGtCQUFhLEdBQWdCLEVBQUUsQ0FBQztRQUNoQyxzQkFBaUIsR0FBZ0IsRUFBRSxDQUFDO1FBK1I5QyxlQUFVLEdBQUc7WUFDVCxNQUFNLEVBQUUsRUFBRTtZQUNWLE9BQU8sRUFBQyxFQUFFO1lBQ1YsYUFBYSxFQUFDLEVBQUU7WUFDaEIsS0FBSyxFQUFDLEVBQUU7WUFDUCxPQUFPLEVBQUMsRUFBRTtTQUNaLENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNuQixNQUFNLEVBQUU7Z0JBQ04sVUFBVSxFQUFPLHVCQUF1QjtnQkFDeEMsV0FBVyxFQUFNLCtDQUErQzthQUNqRTtZQUNBLE9BQU8sRUFBRTtnQkFDUixVQUFVLEVBQU8sd0JBQXdCO2dCQUN6QyxXQUFXLEVBQU0sK0NBQStDO2dCQUNoRSxlQUFlLEVBQUUseUJBQXlCO2FBQzNDO1lBQ0EsYUFBYSxFQUFFO2dCQUNkLFVBQVUsRUFBTywyQkFBMkI7YUFDN0M7WUFDQSxLQUFLLEVBQUU7Z0JBQ04sVUFBVSxFQUFPLHNCQUFzQjtnQkFDdkMsV0FBVyxFQUFNLCtDQUErQzthQUNqRTtZQUNBLE9BQU8sRUFBRTtnQkFDUixVQUFVLEVBQU8sd0JBQXdCO2dCQUN6QyxXQUFXLEVBQU0sK0NBQStDO2dCQUNoRSxlQUFlLEVBQUUseUJBQXlCO2FBQzNDO1NBQ0YsQ0FBQztRQStRSixlQUFlO1FBQ04saUJBQVksR0FBVyxPQUFPLENBQUM7UUFDNUIsY0FBUyxHQUFXLE1BQU0sQ0FBQztRQUMzQixpQkFBWSxHQUFXLHVCQUF1QixDQUFDO1FBRS9DLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFDMUIsV0FBTSxHQUFZLEtBQUssQ0FBQztRQUN4QixnQkFBVyxHQUFZLEtBQUssQ0FBQztRQUM3QixhQUFRLEdBQVcsaUVBQWlFLENBQUM7UUFDckYsbUJBQWMsR0FBVyxLQUFLLENBQUM7UUFDL0IsY0FBUyxHQUFXLEVBQUUsQ0FBQztRQUN0QixnQkFBVyxHQUFXLEVBQUUsQ0FBQztRQUN6QixnQkFBVyxHQUFXLEVBQUUsQ0FBQztRQUMxQixnQkFBVyxHQUFTLEtBQUssQ0FBQztRQWtFbEMsZ0JBQWdCO1FBQ1YsVUFBSyxHQUFjLEVBQUUsQ0FBQztRQUN0QixVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2YsZUFBVSxHQUFVLEdBQUcsQ0FBQztRQUN4QixhQUFRLEdBQVcsS0FBSyxDQUFDO0lBbnBCd0IsQ0FBQztJQUUxRCwwQ0FBUSxHQUFSO1FBQUEsaUJBcUVDO1FBcEVDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDN0MsS0FBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFBQyxDQUFDLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNwQixJQUFJLENBQUMsUUFBUSxHQUFJLEdBQUcsQ0FBQztRQUNyQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFFLFNBQVMsSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFFcEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBRy9DLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUUsS0FBSyxDQUFDLENBQUEsQ0FBQztnQkFFdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDOUMsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2hGLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2hGLElBQUksQ0FBQyxXQUFXLEdBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzdELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hDLDhDQUE4QztnQkFDOUMsSUFBSSxDQUFDLFNBQVMsR0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsRUFBRSxDQUFBLENBQUUsSUFBSSxDQUFDLFFBQVEsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO29CQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLGlFQUFpRSxDQUFDO2dCQUNyRixDQUFDO2dCQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7Z0JBQzFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7Z0JBQzdDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztvQkFDcEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztvQkFFaEQsb0RBQW9EO29CQUNwRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFFLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO3dCQUM1RCxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7NEJBQzlDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7Z0NBQ2hDLFNBQVMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVM7Z0NBQzFDLGVBQWUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWU7Z0NBQ3RELFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVc7Z0NBQzlDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCO2dDQUMxRCxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTO2dDQUMxQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLO2dDQUNsQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVO2dDQUM1QyxPQUFPLEVBQUUsQ0FBQztnQ0FDVixVQUFVLEVBQUUsQ0FBQzs2QkFDWixDQUFDLENBQUM7d0JBQ0gsQ0FBQztvQkFDTCxDQUFDO2dCQUNMLENBQUM7Z0JBQ0QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUU5QyxDQUFDO1FBRUwsQ0FBQztRQUFBLElBQUksQ0FBQSxDQUFDO1lBQ0osSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QyxDQUFDO0lBRUgsQ0FBQztJQUVLLG9EQUFrQixHQUExQixVQUEyQixNQUFNO1FBQWpDLGlCQStCSTtRQTlCQSxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUNuQyxVQUFBLFFBQVE7WUFDUCxLQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUM7WUFDckMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQ1AsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxFQUFFLGNBQWM7YUFDckIsQ0FBQyxDQUFDO1lBQ1YsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDNUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7b0JBQ2QsRUFBRSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDekIsSUFBSSxFQUFFLEtBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFNO2lCQUNuQyxDQUFDLENBQUM7Z0JBRVAsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLFFBQVEsSUFBRSxTQUFTLElBQUksS0FBSSxDQUFDLFFBQVEsSUFBRSxJQUFJLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLElBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQSxDQUFDO29CQUN0RyxhQUFhLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEIsQ0FBQztZQUNGLENBQUM7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDO1lBQy9CLEVBQUUsQ0FBQSxDQUFDLGFBQWEsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUN0QixLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDMUMsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzRCxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2hFLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQyxDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDTSw4Q0FBWSxHQUFwQixVQUFxQixNQUFNLEVBQUMsVUFBVSxFQUFDLEVBQUU7UUFBekMsaUJBc0RFO1FBckRBLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBQyxVQUFVLEVBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUMzQyxVQUFBLFFBQVE7WUFDUCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTdDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDckMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2QyxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7Z0JBQ2hELEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsRSxLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2hGLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMvRCxLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2hGLEtBQUksQ0FBQyxXQUFXLEdBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzdELEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2pELEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQy9ELEtBQUksQ0FBQyxHQUFHLEdBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7Z0JBQ3BDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hDLDhDQUE4QztnQkFDOUMsS0FBSSxDQUFDLFNBQVMsR0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsRUFBRSxDQUFBLENBQUUsS0FBSSxDQUFDLFFBQVEsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO29CQUNyQixLQUFJLENBQUMsUUFBUSxHQUFHLGlFQUFpRSxDQUFDO2dCQUN0RixDQUFDO2dCQUNELEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7Z0JBQzdDLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNyRCxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsYUFBYSxJQUFFLElBQUksSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUUxRCxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFJLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7d0JBQ2pELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7NEJBQzdCLFNBQVMsRUFBRSxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVM7NEJBQzFDLGVBQWUsRUFBRSxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWU7NEJBQ3RELFdBQVcsRUFBRSxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVc7NEJBQzlDLGlCQUFpQixFQUFFLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCOzRCQUMxRCxTQUFTLEVBQUUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTOzRCQUMxQyxLQUFLLEVBQUUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLOzRCQUNsQyxVQUFVLEVBQUUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVOzRCQUM1QyxPQUFPLEVBQUUsQ0FBQzs0QkFDVixVQUFVLEVBQUUsQ0FBQzt5QkFDZCxDQUFDLENBQUM7b0JBQ0wsQ0FBQztnQkFDSCxDQUFDO1lBRU4sQ0FBQztZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLHlEQUF1QixHQUEvQixVQUFnQyxNQUFNO1FBQXRDLGlCQVNFO1FBUkEsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQzlDLFVBQUEsUUFBUTtZQUNQLEtBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUN4QyxLQUFJLENBQUMsNEJBQTRCLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLDhEQUE0QixHQUFwQyxVQUFxQyxNQUFNO1FBQTNDLGlCQStDRTtRQTdDQSxJQUFJLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDeEQsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLG9CQUFvQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDNUMsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLGVBQWUsSUFBRSxJQUFJLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsSUFBRSxJQUFJLElBQUksS0FBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUN0SSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7b0JBQ3RELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksS0FBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO3dCQUM1RCxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQzs0QkFDdkUsS0FBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQztnQ0FDL0IsU0FBUyxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQ0FDckMsZUFBZSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dDQUNoRCxXQUFXLEVBQUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO2dDQUN6QyxpQkFBaUIsRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtnQ0FDcEQsU0FBUyxFQUFFLElBQUk7Z0NBQ2YsS0FBSyxFQUFFLElBQUk7Z0NBQ1gsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsT0FBTyxFQUFFLENBQUM7Z0NBQ1YsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsWUFBWSxFQUFFLEtBQUs7NkJBQ3BCLENBQUMsQ0FBQzt3QkFDWCxDQUFDO29CQUNDLENBQUM7Z0JBQ0gsQ0FBQztZQUNKLENBQUM7WUFDQSxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ2hCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7Z0JBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNwRCxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsYUFBYSxJQUFFLElBQUksSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUMxRCxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFJLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7d0JBQ2pELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksS0FBSSxDQUFDLDJCQUEyQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDOzRCQUNsRSxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsSUFBRSxLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxJQUFFLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQSxDQUFDO2dDQUMvSyxLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO2dDQUM3RSxLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dDQUN2RSxLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDO2dDQUNoRixLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dDQUMxRSxLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFDLElBQUksQ0FBQzs0QkFDekQsQ0FBQzt3QkFDRixDQUFDO29CQUNOLENBQUM7Z0JBQ0gsQ0FBQztZQUVOLENBQUM7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDSSwyQ0FBUyxHQUFqQjtRQUFBLGlCQXVDRztRQXRDQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ2xDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDVCxrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUI7YUFFRixFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUUsRUFBRTtvQkFDWCxrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztvQkFDeEIsbURBQXNCLEVBQUU7aUJBQ3pCO2FBQ0Y7WUFDRCxhQUFhLEVBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ2Ysa0JBQVUsQ0FBQyxRQUFRO2lCQUNwQjthQUNGO1lBQ0QsS0FBSyxFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNQLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUMxQjthQUNGO1lBQ0EsT0FBTyxFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNWLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUMxQjthQUNGO1lBQ0QsYUFBYSxFQUFDLEVBQUU7U0FDakIsQ0FBQyxDQUFDO1FBQ0EsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsS0FBSyxHQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxXQUFXLEdBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZO2FBQ2hDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQXpCLENBQXlCLENBQUMsQ0FBQztRQUVoRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxrQ0FBa0M7SUFDM0QsQ0FBQztJQUNRLGdEQUFjLEdBQXRCLFVBQXVCLElBQVU7UUFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFDckMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUVqQyxHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyx3Q0FBd0M7WUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDNUIsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUVoQyxFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hELEdBQUcsQ0FBQyxDQUFDLElBQU0sR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQWdDRCw2Q0FBVyxHQUFYLFVBQVksR0FBVztRQUN2QixNQUFNLENBQUMsVUFBQyxPQUF3QjtZQUM5QixJQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxFQUNyQixPQUFPLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQztZQUM1QixFQUFFLENBQUEsQ0FBQyxPQUFPLENBQUM7Z0JBQ1AsTUFBTSxDQUFDLEVBQUUsVUFBVSxFQUFFLEVBQUMsR0FBRyxLQUFBLEVBQUMsRUFBRSxDQUFBO1lBQ2hDLElBQUk7Z0JBQ0EsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNsQixDQUFDLENBQUM7SUFDSixDQUFDO0lBQ0MsMkRBQXlCLEdBQXpCO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBQ0QsMENBQVEsR0FBUixVQUFTLEtBQUs7UUFDVixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFFLFNBQVMsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDMUQsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUM7WUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBQyxHQUFHLENBQUM7UUFDckIsQ0FBQztRQUNELEVBQUUsQ0FBQSxDQUFDLEtBQUssSUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUEsQ0FBQztZQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7SUFDTCxDQUFDO0lBQ0QsaURBQWUsR0FBZixVQUFnQixLQUFLO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFDRCwrQ0FBYSxHQUFiO1FBQ0UsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBRSxHQUFHLENBQUMsQ0FBQSxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBQ0QsZ0RBQWMsR0FBZCxVQUFlLEtBQUs7UUFDaEIsSUFBSSxVQUFVLEdBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDcEMsRUFBRSxDQUFBLENBQUMsVUFBVSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDeEIsQ0FBQztRQUFBLElBQUksQ0FBQSxDQUFDO1lBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDekIsQ0FBQztJQUNMLENBQUM7SUFDQSwrQ0FBYSxHQUFiO1FBQ0MsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBRSxHQUFHLENBQUMsQ0FBQSxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBQ0QsZ0RBQWMsR0FBZCxVQUFlLEtBQUs7UUFDaEIsSUFBSSxVQUFVLEdBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDcEMsRUFBRSxDQUFBLENBQUMsVUFBVSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDeEIsQ0FBQztRQUFBLElBQUksQ0FBQSxDQUFDO1lBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDekIsQ0FBQztJQUNMLENBQUM7SUFDRCxnREFBYyxHQUFkLFVBQWUsS0FBSyxFQUFDLFdBQVc7UUFDNUIsTUFBTTtRQUNGLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ2xFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUN6RCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFFLFdBQVcsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsSUFBRSxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUEsQ0FBQztvQkFDdEksRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQSxDQUFDO3dCQUM1QyxNQUFNLENBQUMsSUFBSSxDQUFDO29CQUNoQixDQUFDO2dCQUNILENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUNELDZDQUFXLEdBQVgsVUFBWSxXQUFXO1FBQ2pCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3BFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUN0RCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFFLFdBQVcsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsSUFBRSxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUEsQ0FBQztvQkFDckksTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDaEIsQ0FBQztZQUNELENBQUM7UUFDTCxDQUFDO1FBQ0MsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBQ0QsK0NBQWEsR0FBYixVQUFjLFFBQVEsRUFBQyxhQUFhO1FBQ2xDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQzVELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUN0RCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFFLFFBQVEsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxJQUFFLGFBQWEsQ0FBQyxDQUFBLENBQUM7b0JBQzFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUMvQyxDQUFDO1lBQ0QsQ0FBQztRQUNMLENBQUM7UUFDQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFFQSwrQ0FBYSxHQUFiLFVBQWMsUUFBUSxFQUFDLGFBQWE7UUFDbkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFFLElBQUksSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDNUQsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQ3RELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUUsUUFBUSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLElBQUUsYUFBYSxDQUFDLENBQUEsQ0FBQztvQkFDMUcsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzNDLENBQUM7WUFDRCxDQUFDO1FBQ0wsQ0FBQztRQUNDLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDeEIsQ0FBQztJQUNELG1EQUFpQixHQUFqQixVQUFrQixLQUFLLEVBQUMsZUFBZTtRQUNuQyxJQUFJLFVBQVUsR0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUNwQyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQyxlQUFlLENBQUMsQ0FBQztRQUNwRSxFQUFFLENBQUEsQ0FBQyxTQUFTLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNqQixFQUFFLENBQUEsQ0FBQyxVQUFVLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDYixJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwRCxDQUFDO1FBQ0osQ0FBQztRQUFBLElBQUksQ0FBQSxDQUFDO1lBQ0gsRUFBRSxDQUFBLENBQUMsVUFBVSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7b0JBQ0osU0FBUyxFQUFFLGVBQWUsQ0FBQyxTQUFTO29CQUNwQyxlQUFlLEVBQUUsZUFBZSxDQUFDLGVBQWU7b0JBQ2hELFdBQVcsRUFBRSxlQUFlLENBQUMsV0FBVztvQkFDeEMsaUJBQWlCLEVBQUUsZUFBZSxDQUFDLGlCQUFpQjtvQkFDcEQsU0FBUyxFQUFFLENBQUM7b0JBQ1gsS0FBSyxFQUFFLENBQUM7b0JBQ1QsVUFBVSxFQUFFLENBQUM7b0JBQ2IsT0FBTyxFQUFFLENBQUM7b0JBQ1YsVUFBVSxFQUFFLENBQUM7aUJBQ2QsQ0FBQyxDQUFDO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3RDLENBQUM7UUFDTCxDQUFDO1FBRU4sTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFDRCxpREFBZSxHQUFmLFVBQWdCLEtBQUssRUFBQyxLQUFLLEVBQUMsZUFBZTtRQUN6QyxJQUFJLFVBQVUsR0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUNsQyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQyxlQUFlLENBQUMsQ0FBQztRQUN2RSxFQUFFLENBQUEsQ0FBQyxTQUFTLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNoQixFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDVCxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsVUFBVSxHQUFDLENBQUMsQ0FBQztZQUNuRCxDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsR0FBQyxDQUFDLENBQUM7WUFDbkQsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBQ0QsZ0RBQWMsR0FBZCxVQUFlLEtBQUssRUFBQyxlQUFlO1FBQ2pDLElBQUksS0FBSyxHQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQzlCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RFLEVBQUUsQ0FBQSxDQUFDLFNBQVMsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2hCLEVBQUUsQ0FBQSxDQUFDLEtBQUssSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUNaLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLEdBQUMsS0FBSyxDQUFDO1lBQ3RELENBQUM7WUFBQSxJQUFJLENBQUEsQ0FBQztnQkFDRixJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxHQUFDLENBQUMsQ0FBQztZQUM5QyxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFQSxnREFBYyxHQUFkLFVBQWUsS0FBSyxFQUFDLGVBQWU7UUFDbEMsSUFBSSxLQUFLLEdBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDOUIsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUMsZUFBZSxDQUFDLENBQUM7UUFDdEUsRUFBRSxDQUFBLENBQUMsU0FBUyxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDaEIsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ1osSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQUssR0FBQyxLQUFLLENBQUM7WUFDbEQsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNGLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDO1lBQzlDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELDJDQUFTLEdBQVQsVUFBVSxhQUFhLEVBQUMsZUFBZTtRQUNyQyxFQUFFLENBQUEsQ0FBQyxhQUFhLElBQUUsSUFBSSxJQUFJLGFBQWEsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUM5QyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDN0MsRUFBRSxDQUFBLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsSUFBRSxlQUFlLENBQUMsU0FBUyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLElBQUUsZUFBZSxDQUFDLGVBQWUsQ0FBQyxDQUFBLENBQUM7b0JBQzNILE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDO0lBQ0EsNENBQVUsR0FBVixVQUFXLFdBQVc7UUFDaEIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFFLElBQUksSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDcEUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQ3RELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUUsV0FBVyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxJQUFFLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQSxDQUFDO29CQUNySSxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNqQixDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNELDRDQUFVLEdBQVY7UUFBQSxpQkFtQ0Q7UUFsQ0MsSUFBSSxFQUFFLEdBQUksSUFBSSxDQUFDO1FBQ2YsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2hCLEVBQUUsR0FBSSxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsd0NBQXdDO1lBQ3ZDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25DLE1BQU0sQ0FBQztZQUNULENBQUM7UUFDSixDQUFDO1FBQ0gsSUFBSSxNQUFNLEdBQUc7WUFDSCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3JCLE1BQU0sRUFBQztnQkFDTCxJQUFJLEVBQUMsRUFBRTtnQkFDUCxhQUFhLEVBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsV0FBVztnQkFDbkQsTUFBTSxFQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLElBQUk7Z0JBQ3JDLE9BQU8sRUFBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxLQUFLO2dCQUN2QyxPQUFPLEVBQUMsRUFBRTtnQkFDVixLQUFLLEVBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRztnQkFDbkMsT0FBTyxFQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEtBQUs7Z0JBQ3ZDLFdBQVcsRUFBQyxJQUFJLENBQUMsU0FBUztnQkFDMUIsV0FBVyxFQUFDLElBQUksQ0FBQyxRQUFRO2dCQUN6QixXQUFXLEVBQUMsSUFBSSxDQUFDLFFBQVE7Z0JBQ3pCLGFBQWEsRUFBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxXQUFXO2dCQUNuRCxXQUFXLEVBQUMsQ0FBQztnQkFDYixtQkFBbUIsRUFBQyxDQUFDO2dCQUNyQixjQUFjLEVBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN0QztTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3BDLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBNUIsQ0FBNEIsRUFDcEMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO0lBRTlELENBQUM7SUFDTywrQ0FBYSxHQUFyQixVQUFzQixRQUFRO1FBQTlCLGlCQXdDRztRQXRDQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLFNBQVMsSUFBSSxRQUFRLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLEVBQUUsQ0FBQSxDQUFFLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDL0IsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDNUIsQ0FBQztZQUNBLElBQUksVUFBVSxHQUFHO2dCQUNkLElBQUksRUFBQyxRQUFRLENBQUMsT0FBTztnQkFDckIsT0FBTyxFQUFDLFNBQVM7YUFDbkIsQ0FBQTtZQUNGLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FDcEMsVUFBQSxRQUFRO2dCQUNQLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDOUMsQ0FBQyxFQUNkLFVBQUEsS0FBSztnQkFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBRTdCLENBQUMsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO1lBQzdCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUM1QyxDQUFDO2dCQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztnQkFDOUMsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7Z0JBQzFELENBQUM7WUFFSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04scUJBQUksQ0FDRixjQUFjLEVBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFDTyw4Q0FBWSxHQUFwQixVQUFzQixHQUFHO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0QiwwQkFBMEI7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQztnQkFDckQseUJBQXlCO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTiw0Q0FBNEM7Z0JBQzVDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsd0RBQXdEO2dCQUMxRCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBcUJDLGlEQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQsaURBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRCw0Q0FBVSxHQUFWLFVBQVcsQ0FBQztRQUNSLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELGlEQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLENBQUM7SUFDQyxDQUFDO0lBRUQsbURBQWlCLEdBQWpCLFVBQWtCLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVwQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFFLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQscURBQW1CLEdBQW5CLFVBQW9CLENBQUM7UUFDakIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELDRDQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3BDLENBQUM7SUFDTCxDQUFDO0lBUUgsc0JBQVksOENBQVM7YUFBckI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDO2FBRUQsVUFBc0IsS0FBWTtZQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssR0FBRyxDQUFDO1FBQzFDLENBQUM7OztPQUxBO0lBT00sMENBQVEsR0FBZixVQUFnQixLQUFTO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFTSx5Q0FBTyxHQUFkLFVBQWUsS0FBUztRQUN0QixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSx1Q0FBSyxHQUFaLFVBQWEsS0FBUztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSw4Q0FBWSxHQUFuQixVQUFvQixLQUFTO1FBQzNCLEVBQUUsQ0FBQSxDQUFDLEtBQUssSUFBRSxTQUFTLElBQUksS0FBSyxJQUFFLElBQUksSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDckQsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQztRQUNELHNCQUFzQjtJQUN4QixDQUFDO0lBaHRCc0I7UUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7a0NBQWdCLDRCQUFlOzJEQUFDO0lBRDNDLHVCQUF1QjtRQVBuQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxpRUFBaUUsQ0FBQztZQUM3RixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLGdFQUFnRSxDQUFDLENBQUM7WUFDNUYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0F3QjJDLDhDQUFxQjtZQUM1Qix3REFBMEI7WUFDbEQsbUJBQVcsRUFBeUIsa0NBQWU7WUFDdEMsa0NBQWU7WUFDaEIsZ0NBQWM7WUFDcEIsMEJBQVc7WUFDZCwwQ0FBbUI7WUFDakIsZUFBTSxFQUFnQix1QkFBYztPQTlCMUMsdUJBQXVCLENBa3RCbkM7SUFBRCw4QkFBQztDQWx0QkQsQUFrdEJDLElBQUE7QUFsdEJZLDBEQUF1QiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9jcmVhdGUtZm9vZC1pdGVtL2NyZWF0ZS1mb29kLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsVmlld0NoaWxkLFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0gLEFic3RyYWN0Q29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5pbXBvcnQgeyBGb29kSXRlbVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvZm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQge1NlbGVjdE1vZHVsZSxTZWxlY3RDb21wb25lbnR9IGZyb20gJ25nMi1zZWxlY3QnO1xuaW1wb3J0IHsgaW52YWxpZE51bWJlclZhbGlkYXRvciB9IGZyb20gXCJhcHAvdXRpbHMvbnVtYmVyLXZhbGlkYXRvci5kaXJlY3RpdmVcIjtcbmltcG9ydCB7IE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL29wdGlvblZhbHVlRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgeyBPcHRpb25Gb29kSXRlbVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvb3B0aW9uRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgeyBMaW1pdFRvRGlyZWN0aXZlIH0gZnJvbSBcImFwcC93aWRnZXRzL2xpbWl0LXRvLmRpcmVjdGl2ZVwiO1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnY3JlYXRlLWZvb2QtaXRlbScsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtZm9vZC1pdGVtL2NyZWF0ZS1mb29kLWl0ZW0uY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvY3JlYXRlLWZvb2QtaXRlbS9jcmVhdGUtZm9vZC1pdGVtLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ31cbn0pXG5leHBvcnQgY2xhc3MgQ3JlYXRlRm9vZEl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKCdTZWxlY3RJZCcpIHB1YmxpYyBzZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgY3JlYXRlSXRlbUZvcm06IEZvcm1Hcm91cDtcbiAgcHJpdmF0ZSBuYW1lOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgcHJpY2U6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBza3U6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBwb2ludDogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIGNhdGVnb3J5X2lkOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgZGVzY3JpcHRpb246IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBpc0NoZWNrUmFkaW8gOiBzdHJpbmc7XG4gIHByaXZhdGUgc3ViOiBhbnk7XG4gIHByaXZhdGUgaWQgOiBzdHJpbmc7XG4gIHByaXZhdGUgZm9vZEl0ZW06IGFueTtcbiAgcHJpdmF0ZSBvcHRpb25WYWx1ZUZvb2RJdGVtcyA6IGFueTtcbiAgcHJpdmF0ZSBvcHRpb25Gb29kSXRlbXMgOiBhbnk7XG4gIHByaXZhdGUgb3B0aW9uQUxMSXRlbVZhbHVlRm9vZEl0ZW1zIDogQXJyYXk8YW55PiA9IFtdO1xuICBwcml2YXRlIG9wdGlvbkl0ZW1Mc3QgOiBBcnJheTxhbnk+ID0gW107XG4gIHByaXZhdGUgb3B0aW9uSXRlbVRlbXBMc3QgOiBBcnJheTxhbnk+ID0gW107XG4gIHByaXZhdGUgbmFtZVJhZGlvOiBzdHJpbmc7XG4gIHByaXZhdGUgZm9vZFR5cGU6IHN0cmluZztcbiAgcHJpdmF0ZSBpc0FsY29obzogc3RyaW5nO1xuICAvLyBwcml2YXRlIGRlc2NyaXB0aW9uOnN0cmluZztcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBPcHRpb25Gb29kSXRlbVNlcnZpY2U6IE9wdGlvbkZvb2RJdGVtU2VydmljZSxcbiAgcHJpdmF0ZSBPcHRpb25WYWx1ZUZvb2RJdGVtU2VydmljZTogT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2UsXG4gIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLHByaXZhdGUgRm9vZEl0ZW1TZXJ2aWNlOkZvb2RJdGVtU2VydmljZSxcbiAgcHJpdmF0ZSBjYXRlZ29yeVNlcnZpY2U6IENhdGVnb3J5U2VydmljZSwgXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLCBcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG4gICBwcml2YXRlIHJvdXRlcjogUm91dGVyLHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBsZXQgdXNlckluZm8gPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICBcdHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgdGhpcy5mZXRjaEFsbENhdGVnb3JpZXModGhpcy5yZXN0SWQpO1xuICAgIHRoaXMuc3ViID0gdGhpcy5yb3V0ZS5wYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG4gICAgdGhpcy5pZCA9IHBhcmFtc1snaWQnXTsgfSk7XG4gICAgdGhpcy5mb29kVHlwZSA9ICcxJzsgIFxuICAgIHRoaXMuaXNBbGNvaG8gPSAgJzEnOyAgXG4gICAgaWYodGhpcy5pZCE9dW5kZWZpbmVkICYmIHRoaXMuaWQhPW51bGwpe1xuICAgICBcbiAgICAgICAgdGhpcy5mb29kSXRlbSA9IHRoaXMuU3RvcmFnZVNlcnZpY2UuZ2V0U2NvcGUoKTtcbiAgICAgIFxuICAgICAgXG4gICAgICAgIGlmKHRoaXMuZm9vZEl0ZW09PWZhbHNlKXtcbiAgICAgICAgIFxuICAgICAgICAgIHRoaXMuZmV0Y2hBbGxJdGVtKHRoaXMucmVzdElkLG51bGwsdGhpcy5pZCk7XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snbmFtZSddLnNldFZhbHVlKHRoaXMuZm9vZEl0ZW0ubmFtZSk7XG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydwcmljZSddLnNldFZhbHVlKHRoaXMuZm9vZEl0ZW0ucHJpY2UpO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snY2F0ZWdvcnlfaWQnXS5zZXRWYWx1ZSh0aGlzLmZvb2RJdGVtLmNhdGVnb3J5X2lkKTtcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ3NrdSddLnNldFZhbHVlKHRoaXMuZm9vZEl0ZW0uc2t1KTtcbiAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydwb2ludCddLnNldFZhbHVlKHRoaXMuZm9vZEl0ZW0ucG9pbnQpO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXS5zZXRWYWx1ZSh0aGlzLmZvb2RJdGVtLmRlc2NyaXB0aW9uKTtcbiAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb249dGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXTtcbiAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ25hbWUnXTtcbiAgICAgICAgICAgIHRoaXMucHJpY2UgPSB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydwcmljZSddO1xuICAgICAgICAgICAgdGhpcy5jYXRlZ29yeV9pZCA9IHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2NhdGVnb3J5X2lkJ107XG4gICAgICAgICAgICB0aGlzLnNrdT10aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydza3UnXTtcbiAgICAgICAgICAgIHRoaXMucG9pbnQ9dGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1sncG9pbnQnXTtcbiAgICAgICAgICAgIHRoaXMuaW1hZ2VTcmMgPSB0aGlzLmZvb2RJdGVtLnRodW1iOyBcbiAgICAgICAgICAgIHRoaXMuZm9vZFR5cGUgPSB0aGlzLmZvb2RJdGVtLmZvb2RfdHlwZTsgIFxuICAgICAgICAgICAgdGhpcy5pc0FsY29obyA9IHRoaXMuZm9vZEl0ZW0uaXNfYWxjb2hvO1xuICAgICAgICAgICAgLy8gdGhpcy5kZXNjcmlwdGlvbj10aGlzLmZvb2RJdGVtLmRlc2NyaXB0aW9uO1xuICAgICAgICAgICAgdGhpcy5uYW1lUmFkaW89dGhpcy5mb29kSXRlbS5hY3RpdmVfZmxnO1xuICAgICAgICAgICAgaWYoIHRoaXMuaW1hZ2VTcmM9PW51bGwpe1xuICAgICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5uYW1lUmFkaW8gPSB0aGlzLmZvb2RJdGVtLmFjdGl2ZV9mbGc7XG4gICAgICAgICAgICB0aGlzLmlzQ2hlY2tSYWRpbyA9IHRoaXMuZm9vZEl0ZW0uYWN0aXZlX2ZsZztcbiAgICAgICAgICAgIGlmKHRoaXMuZm9vZEl0ZW0hPW51bGwpe1xuICAgICAgICAgICAgICAgIHRoaXMub3B0aW9uSXRlbUxzdCA9IHRoaXMuZm9vZEl0ZW0ub3B0aW9uX2l0ZW1zO1xuICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKCdvcHRpb25JdGVtTHN0ICcsIHRoaXMub3B0aW9uSXRlbUxzdCk7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcHRpb25JdGVtTHN0IT1udWxsICYmIHRoaXMub3B0aW9uSXRlbUxzdC5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAgdGhpcy5vcHRpb25JdGVtTHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0LnB1c2goe1xuICAgICAgICAgICAgICAgICAgICBvcHRpb25faWQ6IHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25faWQsXG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbl92YWx1ZV9pZDogdGhpcy5vcHRpb25JdGVtTHN0W2ldLm9wdGlvbl92YWx1ZV9pZCxcbiAgICAgICAgICAgICAgICAgICAgb3B0aW9uX25hbWU6IHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25fbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgb3B0aW9uX3ZhbHVlX25hbWU6IHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25fdmFsdWVfbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgYWRkX3ByaWNlOiB0aGlzLm9wdGlvbkl0ZW1Mc3RbaV0uYWRkX3ByaWNlLFxuICAgICAgICAgICAgICAgICAgICBwb2ludDogdGhpcy5vcHRpb25JdGVtTHN0W2ldLnBvaW50LFxuICAgICAgICAgICAgICAgICAgICBhY3RpdmVfZmxnOiB0aGlzLm9wdGlvbkl0ZW1Mc3RbaV0uYWN0aXZlX2ZsZyxcbiAgICAgICAgICAgICAgICAgICAgb2xkX2ZsZzogMSwvL2VkaXQgaGFkIGluIGRhdGFiYXNlXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZV9mbGc6IDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7ICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgXG4gICAgICAgIH1cblxuICAgIH1lbHNle1xuICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgfVxuICAgIFxuICB9XG5cbnByaXZhdGUgZmV0Y2hBbGxDYXRlZ29yaWVzKHJlc3RJZCkge1xuICBcdFx0dGhpcy5jYXRlZ29yeVNlcnZpY2UuZ2V0Q2F0ZWdvcmllcyhyZXN0SWQsbnVsbCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLmNhdGVnb3JpZXMgPSByZXNwb25zZS5jYXRlZ29yaWVzO1xuICAgICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1zLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogYGNob29zZSB2YWx1ZWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICBsZXQgZmxnaXRlbUNob29zZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMuY2F0ZWdvcmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHRoaXMuY2F0ZWdvcmllc1tpXS5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogYCR7dGhpcy5jYXRlZ29yaWVzW2ldLm5hbWV9YFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5mb29kSXRlbSE9dW5kZWZpbmVkICYmIHRoaXMuZm9vZEl0ZW0hPW51bGwgJiYgdGhpcy5mb29kSXRlbS5jYXRlZ29yeV9pZD09dGhpcy5jYXRlZ29yaWVzW2ldLmlkKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGdpdGVtQ2hvb3NlID0gaSArIDE7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0Lml0ZW1zID0gdGhpcy5pdGVtczsgXG4gICAgICAgICAgICAgICAgICAgICAgIGlmKGZsZ2l0ZW1DaG9vc2UhPW51bGwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLml0ZW1zW2ZsZ2l0ZW1DaG9vc2VdOyAgXG4gICAgICAgICAgICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5pdGVtc1swXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2NhdGVnb3J5X2lkJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3J5X2lkID0gdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snY2F0ZWdvcnlfaWQnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgXHRjb25zb2xlLmxvZyhyZXNwb25zZS5jYXRlZ29yaWVzKTsgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbiAgcHJpdmF0ZSBmZXRjaEFsbEl0ZW0ocmVzdElkLGNhdGVnb3J5SWQsaWQpIHtcbiAgXHRcdHRoaXMuRm9vZEl0ZW1TZXJ2aWNlLmdldEZvb2RJdGVtcyhyZXN0SWQsY2F0ZWdvcnlJZCxpZCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLmZvb2RJdGVtID0gcmVzcG9uc2UuZm9vZF9pdGVtc1swXTtcbiAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygndGhpcy5mb29kSXRlbScsIFx0dGhpcy5mb29kSXRlbSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZldGNoQWxsQ2F0ZWdvcmllcyh0aGlzLnJlc3RJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5mb29kSXRlbSE9bnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9uSXRlbUxzdCA9IHRoaXMuZm9vZEl0ZW0ub3B0aW9uX2l0ZW1zO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWyduYW1lJ10uc2V0VmFsdWUodGhpcy5mb29kSXRlbS5uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1sncHJpY2UnXS5zZXRWYWx1ZSh0aGlzLmZvb2RJdGVtLnByaWNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snY2F0ZWdvcnlfaWQnXS5zZXRWYWx1ZSh0aGlzLmZvb2RJdGVtLmNhdGVnb3J5X2lkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snc2t1J10uc2V0VmFsdWUodGhpcy5mb29kSXRlbS5za3UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1sncG9pbnQnXS5zZXRWYWx1ZSh0aGlzLmZvb2RJdGVtLnBvaW50KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXS5zZXRWYWx1ZSh0aGlzLmZvb2RJdGVtLmRlc2NyaXB0aW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbj10aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydkZXNjcmlwdGlvbiddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJpY2UgPSB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydwcmljZSddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnBvaW50ID0gdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1sncG9pbnQnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYXRlZ29yeV9pZCA9IHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2NhdGVnb3J5X2lkJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2t1PXRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ3NrdSddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlU3JjID0gdGhpcy5mb29kSXRlbS50aHVtYjsgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9vZFR5cGUgPSB0aGlzLmZvb2RJdGVtLmZvb2RfdHlwZTsgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWxjb2hvID0gdGhpcy5mb29kSXRlbS5pc19hbGNvaG87XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoaXMuZGVzY3JpcHRpb249dGhpcy5mb29kSXRlbS5kZXNjcmlwdGlvbjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5uYW1lUmFkaW89dGhpcy5mb29kSXRlbS5hY3RpdmVfZmxnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiggdGhpcy5pbWFnZVNyYz09bnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlU3JjID0gJ2h0dHA6Ly93d3cucGxhY2Vob2xkLml0LzIwMHgxNTAvRUZFRkVGL0FBQUFBQSZhbXA7dGV4dD1ubytpbWFnZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0NoZWNrUmFkaW8gPSB0aGlzLmZvb2RJdGVtLmFjdGl2ZV9mbGc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLml0ZW1zW3RoaXMuZm9vZEl0ZW0uY2F0ZWdvcnlfaWRdOyAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbkl0ZW1Mc3QhPW51bGwgJiYgdGhpcy5vcHRpb25JdGVtTHN0Lmxlbmd0aD4wKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMub3B0aW9uSXRlbUxzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9uSXRlbVRlbXBMc3QucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25faWQ6IHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25faWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25fdmFsdWVfaWQ6IHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25fdmFsdWVfaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25fbmFtZTogdGhpcy5vcHRpb25JdGVtTHN0W2ldLm9wdGlvbl9uYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uX3ZhbHVlX25hbWU6IHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25fdmFsdWVfbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZF9wcmljZTogdGhpcy5vcHRpb25JdGVtTHN0W2ldLmFkZF9wcmljZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvaW50OiB0aGlzLm9wdGlvbkl0ZW1Mc3RbaV0ucG9pbnQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmVfZmxnOiB0aGlzLm9wdGlvbkl0ZW1Mc3RbaV0uYWN0aXZlX2ZsZyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9sZF9mbGc6IDEsLy9lZGl0IGhhZCBpbiBkYXRhYmFzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlX2ZsZzogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlLmZvb2RJdGVtKTsgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH0gXG4gIHByaXZhdGUgZmV0Y2hBbGxPcHRpb25Gb29kSXRlbXMocmVzdElkKSB7XG4gIFx0XHR0aGlzLk9wdGlvbkZvb2RJdGVtU2VydmljZS5nZXRPcHRpb25Gb29kSXRlbXMocmVzdElkLG51bGwpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5vcHRpb25Gb29kSXRlbXMgPSByZXNwb25zZS5vcHRpb25zO1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmV0Y2hBbGxPcHRpb25WYWx1ZUZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgICAgICAgICAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlLm9wdGlvbnMpOyBcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICBcdFx0XHRcdFx0fSk7XG4gIFx0fVxuICBwcml2YXRlIGZldGNoQWxsT3B0aW9uVmFsdWVGb29kSXRlbXMocmVzdElkKSB7XG4gICBcbiAgXHRcdHRoaXMuT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2UuZ2V0T3B0aW9uVmFsdWVGb29kSXRlbXMocmVzdElkLG51bGwpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5vcHRpb25WYWx1ZUZvb2RJdGVtcyA9IHJlc3BvbnNlLm9wdGlvbnM7XG4gICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMub3B0aW9uRm9vZEl0ZW1zIT1udWxsICYmIHRoaXMub3B0aW9uRm9vZEl0ZW1zLmxlbmd0aD4wICYmIHRoaXMub3B0aW9uVmFsdWVGb29kSXRlbXMhPW51bGwgJiYgdGhpcy5vcHRpb25WYWx1ZUZvb2RJdGVtcy5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMub3B0aW9uRm9vZEl0ZW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCAgdGhpcy5vcHRpb25WYWx1ZUZvb2RJdGVtcy5sZW5ndGg7IGorKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbkZvb2RJdGVtc1tpXS5pZD09dGhpcy5vcHRpb25WYWx1ZUZvb2RJdGVtc1tqXS5vcHRpb25faWQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbkFMTEl0ZW1WYWx1ZUZvb2RJdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbl9pZDogdGhpcy5vcHRpb25Gb29kSXRlbXNbaV0uaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25fdmFsdWVfaWQ6IHRoaXMub3B0aW9uVmFsdWVGb29kSXRlbXNbal0uaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25fbmFtZTogdGhpcy5vcHRpb25Gb29kSXRlbXNbaV0ubmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbl92YWx1ZV9uYW1lOiB0aGlzLm9wdGlvblZhbHVlRm9vZEl0ZW1zW2pdLm5hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZGRfcHJpY2U6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb2ludDogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGl2ZV9mbGc6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbGRfZmxnOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlX2ZsZzogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrYm94X2ZsZzogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLmZvb2RJdGVtIT1udWxsKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25JdGVtTHN0ID0gdGhpcy5mb29kSXRlbS5vcHRpb25faXRlbXM7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdvcHRpb25JdGVtTHN0ICcsIHRoaXMub3B0aW9uSXRlbUxzdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbkl0ZW1Mc3QhPW51bGwgJiYgdGhpcy5vcHRpb25JdGVtTHN0Lmxlbmd0aD4wKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAgdGhpcy5vcHRpb25JdGVtTHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCAgdGhpcy5vcHRpb25BTExJdGVtVmFsdWVGb29kSXRlbXMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMub3B0aW9uSXRlbUxzdFtpXS5vcHRpb25faWQ9PXRoaXMub3B0aW9uQUxMSXRlbVZhbHVlRm9vZEl0ZW1zW2pdLm9wdGlvbl9pZCAmJiB0aGlzLm9wdGlvbkl0ZW1Mc3RbaV0ub3B0aW9uX3ZhbHVlX2lkPT10aGlzLm9wdGlvbkFMTEl0ZW1WYWx1ZUZvb2RJdGVtc1tqXS5vcHRpb25fdmFsdWVfaWQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbkFMTEl0ZW1WYWx1ZUZvb2RJdGVtc1tqXS5hZGRfcHJpY2U9dGhpcy5vcHRpb25JdGVtTHN0W2ldLmFkZF9wcmljZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9uQUxMSXRlbVZhbHVlRm9vZEl0ZW1zW2pdLnBvaW50PXRoaXMub3B0aW9uSXRlbUxzdFtpXS5wb2ludDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25BTExJdGVtVmFsdWVGb29kSXRlbXNbal0uYWN0aXZlX2ZsZz10aGlzLm9wdGlvbkl0ZW1Mc3RbaV0uYWN0aXZlX2ZsZztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25BTExJdGVtVmFsdWVGb29kSXRlbXNbal0ub2xkX2ZsZz10aGlzLm9wdGlvbkl0ZW1Mc3RbaV0ub2xkX2ZsZztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25BTExJdGVtVmFsdWVGb29kSXRlbXNbal0uY2hlY2tib3hfZmxnPXRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgXHRjb25zb2xlLmxvZyhyZXNwb25zZS5vcHRpb25zKTsgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH0gXG5wcml2YXRlIGJ1aWxkRm9ybSgpOiB2b2lkIHtcbiAgICB0aGlzLmNyZWF0ZUl0ZW1Gb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAnbmFtZSc6IFsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgICAgXVxuXG4gICAgICBdLCdwcmljZSc6WycnLCBbXG4gICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZCxcbiAgICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCgxMCksXG4gICAgICAgICAgaW52YWxpZE51bWJlclZhbGlkYXRvcigpXG4gICAgICAgIF1cbiAgICAgIF0sXG4gICAgICAnY2F0ZWdvcnlfaWQnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWRcbiAgICAgICAgXVxuICAgICAgXSxcbiAgICAgICdza3UnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTI4KSxcbiAgICAgICAgXVxuICAgICAgXSxcbiAgICAgICAncG9pbnQnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTI4KSxcbiAgICAgICAgXVxuICAgICAgXSxcbiAgICAgICdkZXNjcmlwdGlvbic6W11cbiAgICB9KTtcbiAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgICAgdGhpcy5wcmljZSA9IHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ3ByaWNlJ107XG4gICAgICAgdGhpcy5jYXRlZ29yeV9pZCA9IHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2NhdGVnb3J5X2lkJ107XG4gICAgICAgdGhpcy5za3U9dGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snc2t1J107XG4gICAgICAgIHRoaXMucG9pbnQ9dGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1sncG9pbnQnXTtcbiAgICAgICB0aGlzLmRlc2NyaXB0aW9uPXRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2Rlc2NyaXB0aW9uJ107XG4gICAgICAgdGhpcy5jcmVhdGVJdGVtRm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGRhdGEpKTtcblxuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTsgLy8gKHJlKXNldCB2YWxpZGF0aW9uIG1lc3NhZ2VzIG5vd1xuICB9XG4gICBwcml2YXRlIG9uVmFsdWVDaGFuZ2VkKGRhdGE/OiBhbnkpIHtcbiAgICBpZiAoIXRoaXMuY3JlYXRlSXRlbUZvcm0pIHsgcmV0dXJuOyB9XG4gICAgY29uc3QgZm9ybSA9IHRoaXMuY3JlYXRlSXRlbUZvcm07XG5cbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxuICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IFtdO1xuICAgICAgY29uc3QgY29udHJvbCA9IGZvcm0uZ2V0KGZpZWxkKTtcblxuICAgICAgaWYgKGNvbnRyb2wgJiYgY29udHJvbC5kaXJ0eSAmJiAhY29udHJvbC52YWxpZCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlcyA9IHRoaXMudmFsaWRhdGlvbk1lc3NhZ2VzW2ZpZWxkXTtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJvbC5lcnJvcnMpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdLnB1c2gobWVzc2FnZXNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbmZvcm1FcnJvcnMgPSB7XG4gICAgJ25hbWUnOiBbXSxcbiAgICAncHJpY2UnOltdLFxuICAgICdjYXRlZ29yeV9pZCc6W10sXG4gICAgJ3NrdSc6W10sXG4gICAgICdwb2ludCc6W11cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ25hbWUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgbmFtZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIG5hbWUgbXVzdCBiZSBhdCBsZXNzIDI1NiBjaGFyYWN0ZXJzIGxvbmcuJ1xuICAgIH0sXG4gICAgICdwcmljZSc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICAgICAgJ1RoZSBwcmljZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIHByaWNlIG11c3QgYmUgYXQgbGVzcyAxMCBjaGFyYWN0ZXJzIGxvbmcuJyxcbiAgICAgICdpbnZhbGlkTnVtYmVyJzogJ2hlIHByaWNlIG11c3QgYmUgbnVtYmVyJ1xuICAgIH0sXG4gICAgICdjYXRlZ29yeV9pZCc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICAgICAgJ1RoZSBjYXRlZ29yeSBpcyByZXF1aXJlZC4nXG4gICAgfSxcbiAgICAgJ3NrdSc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICAgICAgJ1RoZSBza3UgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBuYW1lIG11c3QgYmUgYXQgbGVzcyAxMjggY2hhcmFjdGVycyBsb25nLidcbiAgICB9ICxcbiAgICAgJ3BvaW50Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIHBvaW50IGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogICAgICdUaGUgcG9pbnQgbXVzdCBiZSBhdCBsZXNzIDEwIGNoYXJhY3RlcnMgbG9uZy4nLFxuICAgICAgJ2ludmFsaWROdW1iZXInOiAnaGUgcG9pbnQgbXVzdCBiZSBudW1iZXInXG4gICAgfVxuICB9O1xuICBudW1iZXJWYWx1ZShtYXg6IE51bWJlcik6IFZhbGlkYXRvcnMge1xuICByZXR1cm4gKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCk6IHtba2V5OiBzdHJpbmddOiBhbnl9ID0+IHtcbiAgICBjb25zdCBpbnB1dCA9IGNvbnRyb2wudmFsdWUsXG4gICAgICAgICAgaXNWYWxpZCA9IGlucHV0ID4gbWF4O1xuICAgIGlmKGlzVmFsaWQpIFxuICAgICAgICByZXR1cm4geyAnbWF4VmFsdWUnOiB7bWF4fSB9XG4gICAgZWxzZSBcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gIH07XG59XG4gIGNyZWF0ZU9wdGlvblZhbHVlRm9vZEl0ZW0oKXtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCdvcHRpb24tZm9vZC1pdGVtcy9saXN0Jyk7XG4gIH1cbiAgY2hlY2tGbGcodmFsdWUpe1xuICAgICAgaWYodGhpcy5pc0NoZWNrUmFkaW89PXVuZGVmaW5lZCB8fCB0aGlzLmlzQ2hlY2tSYWRpbz09bnVsbCl7XG4gICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gJzEnO1xuICAgICAgICB0aGlzLm5hbWVSYWRpbz0nMSc7XG4gICAgICB9XG4gICAgICBpZih2YWx1ZT09dGhpcy5pc0NoZWNrUmFkaW8pe1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICB9XG4gIGNoYW5nZW5hbWVSYWRpbyh2YWx1ZSl7XG4gICAgIHRoaXMubmFtZVJhZGlvID0gdmFsdWU7XG4gIH1cbiAgY2hlY2tmb29kVHlwZSgpe1xuICAgIGlmKHRoaXMuZm9vZFR5cGU9PScxJyl7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgY2hhbmdlZm9vZFR5cGUoZXZlbnQpe1xuICAgICAgbGV0IHZhbHVlY2hlY2s9ZXZlbnQudGFyZ2V0LmNoZWNrZWQ7XG4gICAgICBpZih2YWx1ZWNoZWNrPT10cnVlKXtcbiAgICAgICAgICB0aGlzLmZvb2RUeXBlID0gJzEnO1xuICAgICAgfWVsc2V7XG4gICAgICAgICAgIHRoaXMuZm9vZFR5cGUgPSAnMCc7XG4gICAgICB9XG4gIH1cbiAgIGNoZWNrSXNBbGNvaG8oKXtcbiAgICBpZih0aGlzLmlzQWxjb2hvPT0nMScpe1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIGNoYW5nZUlzQWxjb2hvKGV2ZW50KXtcbiAgICAgIGxldCB2YWx1ZWNoZWNrPWV2ZW50LnRhcmdldC5jaGVja2VkO1xuICAgICAgaWYodmFsdWVjaGVjaz09dHJ1ZSl7XG4gICAgICAgICAgdGhpcy5pc0FsY29obyA9ICcxJztcbiAgICAgIH1lbHNle1xuICAgICAgICAgICB0aGlzLmlzQWxjb2hvID0gJzAnO1xuICAgICAgfVxuICB9XG4gIGNoZWNrQWN0aXZlRmxnKHZhbHVlLG9wdGlvblZhbHVlKXtcbiAgICAgIC8vVE9ET1xuICAgICAgICAgIGlmKHRoaXMub3B0aW9uSXRlbVRlbXBMc3QhPW51bGwgJiYgdGhpcy5vcHRpb25JdGVtVGVtcExzdC5sZW5ndGg+MCl7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8ICB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgaWYodGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5vcHRpb25faWQ9PW9wdGlvblZhbHVlLm9wdGlvbl9pZCAmJiB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0W2ldLm9wdGlvbl92YWx1ZV9pZD09b3B0aW9uVmFsdWUub3B0aW9uX3ZhbHVlX2lkKXtcbiAgICAgICAgICAgICAgICBpZih2YWx1ZT09dGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5hY3RpdmVfZmxnKXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIGNoZWNrYm94RmxnKG9wdGlvblZhbHVlKXtcbiAgICAgICAgaWYodGhpcy5vcHRpb25JdGVtVGVtcExzdCE9bnVsbCAmJiB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0Lmxlbmd0aD4wKXtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAgdGhpcy5vcHRpb25JdGVtVGVtcExzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYodGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5vcHRpb25faWQ9PW9wdGlvblZhbHVlLm9wdGlvbl9pZCAmJiB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0W2ldLm9wdGlvbl92YWx1ZV9pZD09b3B0aW9uVmFsdWUub3B0aW9uX3ZhbHVlX2lkKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICBzZXRWYWx1ZVByaWNlKG9wdGlvbklkLG9wdGlvblZhbHVlSWQpe1xuICAgIGlmKHRoaXMub3B0aW9uSXRlbVRlbXBMc3QhPW51bGwgJiYgdGhpcy5vcHRpb25JdGVtVGVtcExzdC5sZW5ndGg+MCl7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8ICB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5vcHRpb25faWQ9PW9wdGlvbklkICYmIHRoaXMub3B0aW9uSXRlbVRlbXBMc3RbaV0ub3B0aW9uX3ZhbHVlX2lkPT1vcHRpb25WYWx1ZUlkKXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uSXRlbVRlbXBMc3RbaV0uYWRkX3ByaWNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgIHNldFZhbHVlUG9pbnQob3B0aW9uSWQsb3B0aW9uVmFsdWVJZCl7XG4gICAgaWYodGhpcy5vcHRpb25JdGVtVGVtcExzdCE9bnVsbCAmJiB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0Lmxlbmd0aD4wKXtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMub3B0aW9uSXRlbVRlbXBMc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0W2ldLm9wdGlvbl9pZD09b3B0aW9uSWQgJiYgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5vcHRpb25fdmFsdWVfaWQ9PW9wdGlvblZhbHVlSWQpe1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5wb2ludDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgfVxuICBjaGFuZ2VDaGVja2JveEZsZyhldmVudCxvcHRpb25WYWx1ZUl0ZW0pe1xuICAgICAgbGV0IHZhbHVlY2hlY2s9ZXZlbnQudGFyZ2V0LmNoZWNrZWQ7XG4gICAgICBsZXQgaXRlbUNoZWNrID0gdGhpcy5jaGVja0V4aXQodGhpcy5vcHRpb25JdGVtVGVtcExzdCxvcHRpb25WYWx1ZUl0ZW0pO1xuICAgICAgICAgaWYoaXRlbUNoZWNrIT1udWxsKXtcbiAgICAgICAgICAgIGlmKHZhbHVlY2hlY2shPXRydWUpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0LnNwbGljZShpdGVtQ2hlY2ssIDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBpZih2YWx1ZWNoZWNrPT10cnVlKXtcbiAgICAgICAgICAgIHRoaXMub3B0aW9uSXRlbVRlbXBMc3QucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25faWQ6IG9wdGlvblZhbHVlSXRlbS5vcHRpb25faWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25fdmFsdWVfaWQ6IG9wdGlvblZhbHVlSXRlbS5vcHRpb25fdmFsdWVfaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25fbmFtZTogb3B0aW9uVmFsdWVJdGVtLm9wdGlvbl9uYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uX3ZhbHVlX25hbWU6IG9wdGlvblZhbHVlSXRlbS5vcHRpb25fdmFsdWVfbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZF9wcmljZTogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb2ludDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGl2ZV9mbGc6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbGRfZmxnOiAwLC8vZWRpdCBoYWQgaW4gZGF0YWJhc2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGV0ZV9mbGc6IDBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMub3B0aW9uSXRlbVRlbXBMc3QpOyBcbiAgICAgICAgICAgICB9XG4gICAgICAgICB9XG4gICAgICAgIFxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICBjaGFuZ2VBY3RpdmVGbGcoZXZlbnQsdmFsdWUsb3B0aW9uVmFsdWVJdGVtKXtcbiAgICBsZXQgdmFsdWVjaGVjaz1ldmVudC50YXJnZXQuY2hlY2tlZDtcbiAgICAgIGxldCBpdGVtQ2hlY2sgPSB0aGlzLmNoZWNrRXhpdCh0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0LG9wdGlvblZhbHVlSXRlbSk7XG4gICAgICBpZihpdGVtQ2hlY2shPW51bGwpe1xuICAgICAgICAgIGlmKHZhbHVlPT0xKXtcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpdGVtQ2hlY2tdLmFjdGl2ZV9mbGc9MTtcbiAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpdGVtQ2hlY2tdLmFjdGl2ZV9mbGc9MDtcbiAgICAgICAgICB9XG4gICAgICB9XG4gIH1cbiAgY2hhbmdlYWRkUHJpY2UoZXZlbnQsb3B0aW9uVmFsdWVJdGVtKXtcbiAgICAgbGV0IHZhbHVlPSBldmVudC50YXJnZXQudmFsdWU7XG4gICAgIGxldCBpdGVtQ2hlY2sgPSB0aGlzLmNoZWNrRXhpdCh0aGlzLm9wdGlvbkl0ZW1UZW1wTHN0LG9wdGlvblZhbHVlSXRlbSk7XG4gICAgICBpZihpdGVtQ2hlY2shPW51bGwpe1xuICAgICAgICAgIGlmKHZhbHVlIT1udWxsKXtcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpdGVtQ2hlY2tdLmFkZF9wcmljZT12YWx1ZTtcbiAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpdGVtQ2hlY2tdLnZhbHVlPTA7XG4gICAgICAgICAgfVxuICAgICAgfVxuICB9XG5cbiAgIGNoYW5nZWFkZFBvaW50KGV2ZW50LG9wdGlvblZhbHVlSXRlbSl7XG4gICAgIGxldCB2YWx1ZT0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuICAgICBsZXQgaXRlbUNoZWNrID0gdGhpcy5jaGVja0V4aXQodGhpcy5vcHRpb25JdGVtVGVtcExzdCxvcHRpb25WYWx1ZUl0ZW0pO1xuICAgICAgaWYoaXRlbUNoZWNrIT1udWxsKXtcbiAgICAgICAgICBpZih2YWx1ZSE9bnVsbCl7XG4gICAgICAgICAgICAgIHRoaXMub3B0aW9uSXRlbVRlbXBMc3RbaXRlbUNoZWNrXS5wb2ludD12YWx1ZTtcbiAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpdGVtQ2hlY2tdLnZhbHVlPTA7XG4gICAgICAgICAgfVxuICAgICAgfVxuICB9XG5cbiAgY2hlY2tFeGl0KG9wdGlvbkl0ZW1Mc3Qsb3B0aW9uVmFsdWVJdGVtKXtcbiAgICBpZihvcHRpb25JdGVtTHN0IT1udWxsICYmIG9wdGlvbkl0ZW1Mc3QubGVuZ3RoPjApe1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8ICBvcHRpb25JdGVtTHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZihvcHRpb25JdGVtTHN0W2ldLm9wdGlvbl9pZD09b3B0aW9uVmFsdWVJdGVtLm9wdGlvbl9pZCAmJiBvcHRpb25JdGVtTHN0W2ldLm9wdGlvbl92YWx1ZV9pZD09b3B0aW9uVmFsdWVJdGVtLm9wdGlvbl92YWx1ZV9pZCl7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuIH1cbiAgaXNEaXNhYmxlZChvcHRpb25WYWx1ZSl7XG4gICAgICAgIGlmKHRoaXMub3B0aW9uSXRlbVRlbXBMc3QhPW51bGwgJiYgdGhpcy5vcHRpb25JdGVtVGVtcExzdC5sZW5ndGg+MCl7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMub3B0aW9uSXRlbVRlbXBMc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmKHRoaXMub3B0aW9uSXRlbVRlbXBMc3RbaV0ub3B0aW9uX2lkPT1vcHRpb25WYWx1ZS5vcHRpb25faWQgJiYgdGhpcy5vcHRpb25JdGVtVGVtcExzdFtpXS5vcHRpb25fdmFsdWVfaWQ9PW9wdGlvblZhbHVlLm9wdGlvbl92YWx1ZV9pZCl7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG4gIGNyZWF0ZUl0ZW0oKXtcbiAgbGV0IGlkICA9ICctMSc7XG4gIGlmKHRoaXMuaWQhPW51bGwpe1xuICAgIGlkICA9IHRoaXMuaWQ7XG4gIH1cbiAgdGhpcy5vblZhbHVlQ2hhbmdlZCgpO1xuICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxuICAgICAgIGlmKHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ubGVuZ3RoID4wKXtcbiAgICAgICAgIHJldHVybjtcbiAgICAgICB9XG4gICAgfVxuICBsZXQgcGFyYW1zID0ge1xuICAgICAgICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICdpdGVtJzp7XG4gICAgICAgICAgICAgICdpZCc6aWQsXG4gICAgICAgICAgICAgICdjYXRlZ29yeV9pZCc6dGhpcy5jcmVhdGVJdGVtRm9ybS52YWx1ZS5jYXRlZ29yeV9pZCxcbiAgICAgICAgICAgICAgJ25hbWUnOnRoaXMuY3JlYXRlSXRlbUZvcm0udmFsdWUubmFtZSxcbiAgICAgICAgICAgICAgJ3ByaWNlJzp0aGlzLmNyZWF0ZUl0ZW1Gb3JtLnZhbHVlLnByaWNlLFxuICAgICAgICAgICAgICAndGh1bWInOicnLFxuICAgICAgICAgICAgICAnc2t1Jzp0aGlzLmNyZWF0ZUl0ZW1Gb3JtLnZhbHVlLnNrdSxcbiAgICAgICAgICAgICAgJ3BvaW50Jzp0aGlzLmNyZWF0ZUl0ZW1Gb3JtLnZhbHVlLnBvaW50LFxuICAgICAgICAgICAgICAnaXNfYWN0aXZlJzp0aGlzLm5hbWVSYWRpbyxcbiAgICAgICAgICAgICAgJ2lzX2FsY29obyc6dGhpcy5pc0FsY29obyxcbiAgICAgICAgICAgICAgJ2Zvb2RfdHlwZSc6dGhpcy5mb29kVHlwZSxcbiAgICAgICAgICAgICAgJ2Rlc2NyaXB0aW9uJzp0aGlzLmNyZWF0ZUl0ZW1Gb3JtLnZhbHVlLmRlc2NyaXB0aW9uLFxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzowLFxuICAgICAgICAgICAgICAndXBkYXRlX2luX2xzdF9mbGcnOjAsXG4gICAgICAgICAgICAgICdvcHRpb25faXRlbXMnOnRoaXMub3B0aW9uSXRlbVRlbXBMc3QsXG4gICAgICAgICAgICB9fTtcbiAgXHR0aGlzLkZvb2RJdGVtU2VydmljZS5jcmVhdGVGb29kSXRlbXMocGFyYW1zKS50aGVuKFxuICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcblxufVxucHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgIGxldCBpbWFnZVBvc3QgPSAnJztcbiAgICAgaWYoIHRoaXMuZmxnSW1hZ2VDaG9vc2UgPT0gdHJ1ZSl7XG4gICAgICAgaW1hZ2VQb3N0ID0gdGhpcy5pbWFnZVNyYztcbiAgICAgfVxuICAgICAgbGV0IGl0ZW1VcGxvYWQgPSB7XG4gICAgICAgICAnaWQnOnJlc3BvbnNlLml0ZW1faWQsXG4gICAgICAgICAndGh1bWInOmltYWdlUG9zdFxuICAgICAgfVxuXHQgICAgdGhpcy5Gb29kSXRlbVNlcnZpY2UuaXRlbVVwbG9hZEZpbGUoaXRlbVVwbG9hZCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyBGb29kIEl0ZW0gaGFzIGJlZW4gYWRkZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCdmb29kLWl0ZW1zL2xpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICAgICAgICAgICAgXG4gIFx0XHRcdFx0XHR9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lcnJvciA9IHJlc3BvbnNlLmVycm9ycztcbiAgICAgIGlmIChyZXNwb25zZS5jb2RlID09IDQyMikge1xuICAgICAgICBpZiAodGhpcy5lcnJvci5uYW1lKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWyduYW1lJ10gPSB0aGlzLmVycm9yLm5hbWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuZXJyb3IucHJpY2UpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ3ByaWNlJ10gPSB0aGlzLmVycm9yLnByaWNlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmVycm9yLmNhdGVnb3J5X2lkKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWydjYXRlZ29yeV9pZCddID0gdGhpcy5lcnJvci5jYXRlZ29yeV9pZDtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN3YWwoXG4gICAgICAgICAgJ0NyZWF0ZSBGYWlsIScsXG4gICAgICAgICAgdGhpcy5lcnJvclswXSxcbiAgICAgICAgICAnZXJyb3InXG4gICAgICAgICkuY2F0Y2goc3dhbC5ub29wKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBmYWlsZWRDcmVhdGUgKHJlcykge1xuICAgIGlmIChyZXMuc3RhdHVzID09IDQwMSkge1xuICAgICAgLy8gdGhpcy5sb2dpbmZhaWxlZCA9IHRydWVcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHJlcy5kYXRhLmVycm9ycy5tZXNzYWdlWzBdID09ICdFbWFpbCBVbnZlcmlmaWVkJykge1xuICAgICAgICAvLyB0aGlzLnVudmVyaWZpZWQgPSB0cnVlXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBvdGhlciBraW5kcyBvZiBlcnJvciByZXR1cm5lZCBmcm9tIHNlcnZlclxuICAgICAgICBmb3IgKHZhciBlcnJvciBpbiByZXMuZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICAvLyB0aGlzLmxvZ2luZmFpbGVkZXJyb3IgKz0gcmVzLmRhdGEuZXJyb3JzW2Vycm9yXSArICcgJ1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbi8qdXBsb2FkIGZpbGUqL1xuXHRwcml2YXRlIHNhY3RpdmVDb2xvcjogc3RyaW5nID0gJ2dyZWVuJztcbiAgICBwcml2YXRlIGJhc2VDb2xvcjogc3RyaW5nID0gJyNjY2MnO1xuICAgIHByaXZhdGUgb3ZlcmxheUNvbG9yOiBzdHJpbmcgPSAncmdiYSgyNTUsMjU1LDI1NSwwLjUpJztcbiAgICBcbiAgICBwcml2YXRlIGRyYWdnaW5nOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBsb2FkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwcml2YXRlIGltYWdlTG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpbWFnZVNyYzogc3RyaW5nID0gJ2h0dHA6Ly93d3cucGxhY2Vob2xkLml0LzIwMHgxNTAvRUZFRkVGL0FBQUFBQSZhbXA7dGV4dD1ubytpbWFnZSc7XG4gICAgcHJpdmF0ZSBmbGdJbWFnZUNob29zZTpib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpY29uQ29sb3I6IHN0cmluZyA9ICcnO1xuICAgIHByaXZhdGUgIGJvcmRlckNvbG9yOiBzdHJpbmcgPSAnJztcbiAgICBwcml2YXRlICBhY3RpdmVDb2xvcjogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSBoaWRkZW5JbWFnZTpib29sZWFuPWZhbHNlO1xuICAgIHByaXZhdGUgY2F0ZWdvcmllcyA6IGFueTtcbiAgICBwcml2YXRlIGNhdGVnb3J5T2JqZWN0IDogYW55O1xuICAgIHByaXZhdGUgdXNlcklkIDogU3RyaW5nO1xuICAgIHByaXZhdGUgZmlsZUNob29zZSA6IEZpbGU7XG4gICAgcHJpdmF0ZSBlcnJvcjogYW55O1xuICAgIGhhbmRsZURyYWdFbnRlcigpIHtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IHRydWU7XG4gICAgfVxuICAgIFxuICAgIGhhbmRsZURyYWdMZWF2ZSgpIHtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgIH1cbiAgICBcbiAgICBoYW5kbGVEcm9wKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB0aGlzLmRyYWdnaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuaGFuZGxlSW5wdXRDaGFuZ2UoZSk7XG4gICAgfVxuICAgIFxuICAgIGhhbmRsZUltYWdlTG9hZCgpIHtcbiAgICAgICAgdGhpcy5pbWFnZUxvYWRlZCA9IHRydWU7XG4gICAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5vdmVybGF5Q29sb3I7XG4gICAgICAgIGlmKHRoaXMuaW1hZ2VMb2FkZWQgPSB0cnVlKXtcblx0XHRcdHRoaXMuaGlkZGVuSW1hZ2UgPSB0cnVlO1xuXHRcdH1cbiAgICB9XG5cbiAgICBoYW5kbGVJbnB1dENoYW5nZShlKSB7XG4gICAgICAgIHZhciBmaWxlID0gZS5kYXRhVHJhbnNmZXIgPyBlLmRhdGFUcmFuc2Zlci5maWxlc1swXSA6IGUudGFyZ2V0LmZpbGVzWzBdO1xuICAgICAgICB0aGlzLmZpbGVDaG9vc2UgPSBmaWxlO1xuICAgICAgICB2YXIgcGF0dGVybiA9IC9pbWFnZS0qLztcbiAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cbiAgICAgICAgaWYgKCFmaWxlLnR5cGUubWF0Y2gocGF0dGVybikpIHtcbiAgICAgICAgICAgIGFsZXJ0KCdpbnZhbGlkIGZvcm1hdCcpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5sb2FkZWQgPSBmYWxzZTtcblxuICAgICAgICByZWFkZXIub25sb2FkID0gdGhpcy5faGFuZGxlUmVhZGVyTG9hZGVkLmJpbmQodGhpcyk7XG4gICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuICAgICAgICB0aGlzLmZsZ0ltYWdlQ2hvb3NlPSB0cnVlO1xuICAgIH1cbiAgICBcbiAgICBfaGFuZGxlUmVhZGVyTG9hZGVkKGUpIHtcbiAgICAgICAgdmFyIHJlYWRlciA9IGUudGFyZ2V0O1xuICAgICAgICB0aGlzLmltYWdlU3JjID0gcmVhZGVyLnJlc3VsdDtcbiAgICAgICAgdGhpcy5sb2FkZWQgPSB0cnVlO1xuICAgIH1cbiAgICBcbiAgICBfc2V0QWN0aXZlKCkge1xuICAgICAgICB0aGlzLmJvcmRlckNvbG9yID0gdGhpcy5hY3RpdmVDb2xvcjtcbiAgICAgICAgaWYgKHRoaXMuaW1hZ2VTcmMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICB0aGlzLmljb25Db2xvciA9IHRoaXMuYWN0aXZlQ29sb3I7XG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgX3NldEluYWN0aXZlKCkge1xuICAgICAgICB0aGlzLmJvcmRlckNvbG9yID0gdGhpcy5iYXNlQ29sb3I7XG4gICAgICAgIGlmICh0aGlzLmltYWdlU3JjLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLmJhc2VDb2xvcjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qc2VsZWN0IHN0YXJ0Ki9cbiAgcHJpdmF0ZSBpdGVtczpBcnJheTxhbnk+ID0gW107XG4gIHByaXZhdGUgdmFsdWU6YW55ID0ge307XG4gIHByaXZhdGUgX2Rpc2FibGVkVjpzdHJpbmcgPSAnMCc7XG4gIHByaXZhdGUgZGlzYWJsZWQ6Ym9vbGVhbiA9IGZhbHNlO1xuIFxuICBwcml2YXRlIGdldCBkaXNhYmxlZFYoKTpzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZFY7XG4gIH1cbiBcbiAgcHJpdmF0ZSBzZXQgZGlzYWJsZWRWKHZhbHVlOnN0cmluZykge1xuICAgIHRoaXMuX2Rpc2FibGVkViA9IHZhbHVlO1xuICAgIHRoaXMuZGlzYWJsZWQgPSB0aGlzLl9kaXNhYmxlZFYgPT09ICcxJztcbiAgfVxuIFxuICBwdWJsaWMgc2VsZWN0ZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICB0aGlzLmNyZWF0ZUl0ZW1Gb3JtLmNvbnRyb2xzWydjYXRlZ29yeV9pZCddLnNldFZhbHVlKHZhbHVlLmlkKTtcbiAgICB0aGlzLmNhdGVnb3J5X2lkID0gdGhpcy5jcmVhdGVJdGVtRm9ybS5jb250cm9sc1snY2F0ZWdvcnlfaWQnXTtcbiAgICBjb25zb2xlLmxvZygnU2VsZWN0ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxuIFxuICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2NhdGVnb3J5X2lkJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgdGhpcy5jYXRlZ29yeV9pZCA9IHRoaXMuY3JlYXRlSXRlbUZvcm0uY29udHJvbHNbJ2NhdGVnb3J5X2lkJ107XG4gICAgY29uc29sZS5sb2coJ1JlbW92ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxuIFxuICBwdWJsaWMgdHlwZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICB9XG4gXG4gIHB1YmxpYyByZWZyZXNoVmFsdWUodmFsdWU6YW55KTp2b2lkIHtcbiAgICBpZih2YWx1ZSE9dW5kZWZpbmVkICYmIHZhbHVlIT1udWxsICYmIHZhbHVlLmxlbmd0aCE9MCl7XG4gICAgICB0aGlzLnZhbHVlID0gdmFsdWU7XG4gICAgfVxuICAgIC8vIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgfVxufVxuIl19
