"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("app/services/user.service");
var angular2_toaster_1 = require("angular2-toaster/angular2-toaster");
var translate_service_1 = require("app/services/translate.service");
var utils_1 = require("app/utils/utils");
var LayoutsAuthComponent = (function () {
    function LayoutsAuthComponent(userServ, toastr, translate) {
        var _this = this;
        this.userServ = userServ;
        this.toastr = toastr;
        this.translate = translate;
        this.mylinks = [];
        this.isGetUser = true;
        this.toastrConfig = new angular2_toaster_1.ToasterConfig({
            newestOnTop: true,
            showCloseButton: true,
            tapToDismiss: false
        });
        // this.translate = translate.getTranslate();
        // this.logger = new LoggerService( this.translate );
        this.userServ.currentUser.subscribe(function (user) {
            if (!user.email) {
                console.log('user', user);
                _this.isGetUser = false;
            }
        });
    }
    LayoutsAuthComponent.prototype.ngOnInit = function () {
        // this.userServ.currentUser.subscribe((user) => {
        //     if(!user.email){
        //       console.log('user', user);
        //        this.userServ.getAuthenticatedUser();
        //     }
        // });
        $(function () {
            /*$(document).on('click', '.sub-item', function (e) {
              
                 let elems: HTMLElement = document.getElementsByClassName('treeview-menu');
                
                 var i;
                 for (i = 0; i < elems.length; i++) {
                     elems[i].style.display = "none";
                 }
                 
             });
       
             $(document).on('click', '.app-content', function (e) {
                let elems: HTMLElement = document.getElementsByClassName('treeview-menu');
                
                 var i;
                 for (i = 0; i < elems.length; i++) {
                     elems[i].style.display = "none";
                 }
                
             });*/
            // nav
            $(document).on('click', '[ui-nav] a', function (e) {
                var $this = $(e.target), $active;
                $this.is('a') || ($this = $this.closest('a'));
                $active = $this.parent().siblings(".active");
                $active && $active.toggleClass('active').find('> ul:visible').slideUp(200);
                ($this.parent().hasClass('active') && $this.next().slideUp(200)) || $this.next().slideDown(200);
                $this.parent().toggleClass('active');
                $this.next().is('ul') && e.preventDefault();
            });
            $(document).on('click', '[ui-toggle-class]', function (e) {
                e.preventDefault();
                var $this = $(e.target);
                $this.attr('ui-toggle-class') || ($this = $this.closest('[ui-toggle-class]'));
                var classes = $this.attr('ui-toggle-class').split(','), targets = ($this.attr('target') && $this.attr('target').split(',')) || Array($this), key = 0;
                $.each(classes, function (index, value) {
                    var target = targets[(targets.length && key)];
                    $(target).toggleClass(classes[index]);
                    key++;
                });
                $this.toggleClass('active');
            });
        });
        if (this.isGetUser) {
            console.log('user');
            this.userServ.getAuthenticatedUser();
            this.isGetUser = false;
        }
        //  sedding the resize event, for AdminLTE to place the height
        var ie = this.detectIE();
        if (!ie) {
            window.dispatchEvent(new Event('resize'));
        }
        else {
            // solution for IE from @hakonamatata
            var event_1 = document.createEvent('Event');
            event_1.initEvent('resize', false, true);
            window.dispatchEvent(event_1);
        }
        // define here your own links menu structure
        this.mylinks = [
            {
                'title': 'Dashboard',
                'icon': 'fa-home',
                'link': ['/dashboard'],
                'state': 'dashboard'
            },
            {
                'title': 'Category',
                'icon': 'fa-cube',
                'link': ['/category/list'],
                'state': 'category-list'
            },
            {
                'title': 'Food Item',
                'icon': 'fa-cutlery',
                'link': ['/food-items/list'],
                'state': 'food-items-list'
            },
            {
                'title': 'Option Food Item',
                'icon': 'fa-sitemap',
                'link': ['/option-food-items/list'],
                'state': 'option-food-items-list'
            },
            {
                'title': 'Option Value Item',
                'icon': 'fa-plus-circle  ',
                'link': ['/option-value-food-items/list'],
                'state': 'option-food-items-list'
            },
            {
                'title': 'Table',
                'icon': 'fa-table',
                'link': ['/table'],
                'state': 'table',
                'class': 'table',
                'sublinks': [
                    {
                        'title': 'Area',
                        'link': ['/area-list'],
                        'icon': 'fa-list-alt',
                        'external': false,
                        'target': '_blank'
                    },
                    {
                        'title': 'Table List',
                        'link': ['/table-list'],
                        'icon': 'fa-list-alt',
                        'external': false,
                        'target': '_blank'
                    },
                    {
                        'title': 'Reservation',
                        'link': ['/table-booking'],
                        'icon': 'fa-calendar',
                        'external': false,
                        'target': '_blank'
                    }
                ]
            },
            {
                'title': 'Contacts',
                'icon': 'fa-address-book',
                'link': ['/contact'],
                'state': 'contact-list'
            },
            {
                'title': 'News',
                'icon': 'fa-newspaper-o',
                'link': ['/news'],
                'state': 'news-list'
            },
            {
                'title': 'Order Setting',
                'icon': 'fa-cogs',
                'link': ['/order-setting'],
                'state': 'order-setting'
            },
            {
                'title': 'Marketing',
                'icon': 'fa-envelope',
                'state': 'marketing',
                'class': 'marketing',
                'sublinks': [
                    {
                        'title': 'Email Marketing',
                        'link': ['/email-marketing-list'],
                        'icon': 'fa-envelope',
                        'external': false,
                        'target': '_blank'
                    },
                    {
                        'title': 'Subscribe',
                        'link': ['/subscribe-list'],
                        'icon': 'fa-envelope-open',
                        'external': false,
                        'target': '_blank'
                    },
                    {
                        'title': 'Search',
                        'link': ['/search-items'],
                        'icon': 'fa-search',
                        'external': false,
                        'target': '_blank'
                    },
                    {
                        'title': 'Reivews',
                        'link': ['/review-item'],
                        'icon': 'fa-star',
                        'external': false,
                        'target': '_blank'
                    }
                ]
            },
            {
                'title': 'Contents',
                'icon': 'fa-file-text-o',
                'link': ['/contents'],
                'state': 'contents'
            },
            {
                'title': 'Feedback',
                'icon': 'fa-comments',
                'link': ['/feedback'],
                'state': 'feedback'
            }
        ];
    };
    LayoutsAuthComponent.prototype.detectIE = function () {
        var ua = window.navigator.userAgent;
        // Test values; Uncomment to check result …
        // IE 10
        // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
        // IE 11
        // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
        // IE 12 / Spartan
        // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
        // Edge (IE 12+)
        // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)
        // Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        // other browser
        return false;
    };
    LayoutsAuthComponent = __decorate([
        core_1.Component({
            selector: 'app-layouts-auth',
            // templateUrl: './auth.html'
            styleUrls: [utils_1.default.getView('app/components/layouts/auth/auth.component.css')],
            templateUrl: utils_1.default.getView('app/components/layouts/auth/auth.html'),
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            angular2_toaster_1.ToasterService,
            translate_service_1.AdminLTETranslateService])
    ], LayoutsAuthComponent);
    return LayoutsAuthComponent;
}());
exports.LayoutsAuthComponent = LayoutsAuthComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2xheW91dHMvYXV0aC9hdXRoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQXFFO0FBRXJFLDBEQUF3RDtBQUd4RCxzRUFBa0Y7QUFDbEYsb0VBQTBFO0FBQzFFLHlDQUFvQztBQVNwQztJQU1FLDhCQUNVLFFBQXFCLEVBQ3JCLE1BQXNCLEVBQ3RCLFNBQW1DO1FBSDdDLGlCQW1CQztRQWxCUyxhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQTBCO1FBTnJDLFlBQU8sR0FBZSxFQUFFLENBQUM7UUFDekIsY0FBUyxHQUFZLElBQUksQ0FBQztRQU9oQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksZ0NBQWEsQ0FBQztZQUNwQyxXQUFXLEVBQUUsSUFBSTtZQUNqQixlQUFlLEVBQUUsSUFBSTtZQUNyQixZQUFZLEVBQUUsS0FBSztTQUNwQixDQUFDLENBQUM7UUFDSCw2Q0FBNkM7UUFDN0MscURBQXFEO1FBRXJELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDdkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSx1Q0FBUSxHQUFmO1FBRUUsa0RBQWtEO1FBQ2xELHVCQUF1QjtRQUN2QixtQ0FBbUM7UUFDbkMsK0NBQStDO1FBQy9DLFFBQVE7UUFDUixNQUFNO1FBQ04sQ0FBQyxDQUFDO1lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0JBbUJNO1lBRUwsTUFBTTtZQUNOLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFlBQVksRUFBRSxVQUFVLENBQUM7Z0JBRS9DLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsT0FBTyxDQUFDO2dCQUNqQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFFOUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdDLE9BQU8sSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRTNFLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDaEcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFFckMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDOUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxVQUFVLENBQUM7Z0JBQ3RELENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDbkIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDeEIsS0FBSyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO2dCQUU5RSxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUNwRCxPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUNuRixHQUFHLEdBQUcsQ0FBQyxDQUFDO2dCQUNWLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsS0FBSyxFQUFFLEtBQUs7b0JBQ3BDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDOUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDdEMsR0FBRyxFQUFFLENBQUM7Z0JBQ1IsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUU5QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDekIsQ0FBQztRQUNELDhEQUE4RDtRQUM5RCxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDekIsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzVDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLHFDQUFxQztZQUNyQyxJQUFJLE9BQUssR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFDLE9BQUssQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN2QyxNQUFNLENBQUMsYUFBYSxDQUFDLE9BQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFRCw0Q0FBNEM7UUFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRztZQUNiO2dCQUNFLE9BQU8sRUFBRSxXQUFXO2dCQUNwQixNQUFNLEVBQUUsU0FBUztnQkFDakIsTUFBTSxFQUFFLENBQUMsWUFBWSxDQUFDO2dCQUN0QixPQUFPLEVBQUUsV0FBVzthQUNyQjtZQUNEO2dCQUNFLE9BQU8sRUFBRSxVQUFVO2dCQUNuQixNQUFNLEVBQUUsU0FBUztnQkFDakIsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7Z0JBQzFCLE9BQU8sRUFBRSxlQUFlO2FBQ3pCO1lBQ0Q7Z0JBQ0UsT0FBTyxFQUFFLFdBQVc7Z0JBQ3BCLE1BQU0sRUFBRSxZQUFZO2dCQUNwQixNQUFNLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztnQkFDNUIsT0FBTyxFQUFFLGlCQUFpQjthQUMzQjtZQUNEO2dCQUNFLE9BQU8sRUFBRSxrQkFBa0I7Z0JBQzNCLE1BQU0sRUFBRSxZQUFZO2dCQUNwQixNQUFNLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztnQkFDbkMsT0FBTyxFQUFFLHdCQUF3QjthQUNsQztZQUNEO2dCQUNFLE9BQU8sRUFBRSxtQkFBbUI7Z0JBQzVCLE1BQU0sRUFBRSxrQkFBa0I7Z0JBQzFCLE1BQU0sRUFBRSxDQUFDLCtCQUErQixDQUFDO2dCQUN6QyxPQUFPLEVBQUUsd0JBQXdCO2FBQ2xDO1lBQ0Q7Z0JBQ0UsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLE1BQU0sRUFBRSxVQUFVO2dCQUNsQixNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ2xCLE9BQU8sRUFBRSxPQUFPO2dCQUNmLE9BQU8sRUFBRSxPQUFPO2dCQUNqQixVQUFVLEVBQUU7b0JBQ1o7d0JBQ0ksT0FBTyxFQUFFLE1BQU07d0JBQ2YsTUFBTSxFQUFFLENBQUMsWUFBWSxDQUFDO3dCQUN0QixNQUFNLEVBQUUsYUFBYTt3QkFDckIsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLFFBQVEsRUFBRyxRQUFRO3FCQUV0QjtvQkFDRDt3QkFDSSxPQUFPLEVBQUUsWUFBWTt3QkFDckIsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO3dCQUN2QixNQUFNLEVBQUUsYUFBYTt3QkFDckIsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLFFBQVEsRUFBRyxRQUFRO3FCQUVwQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUUsYUFBYTt3QkFDdEIsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7d0JBQzFCLE1BQU0sRUFBRSxhQUFhO3dCQUNyQixVQUFVLEVBQUUsS0FBSzt3QkFDaEIsUUFBUSxFQUFHLFFBQVE7cUJBRXJCO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxPQUFPLEVBQUUsVUFBVTtnQkFDbkIsTUFBTSxFQUFFLGlCQUFpQjtnQkFDekIsTUFBTSxFQUFFLENBQUMsVUFBVSxDQUFDO2dCQUNwQixPQUFPLEVBQUUsY0FBYzthQUN4QjtZQUNEO2dCQUNFLE9BQU8sRUFBRSxNQUFNO2dCQUNmLE1BQU0sRUFBRSxnQkFBZ0I7Z0JBQ3hCLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQztnQkFDakIsT0FBTyxFQUFFLFdBQVc7YUFDckI7WUFDRDtnQkFDRSxPQUFPLEVBQUUsZUFBZTtnQkFDeEIsTUFBTSxFQUFFLFNBQVM7Z0JBQ2pCLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixDQUFDO2dCQUMxQixPQUFPLEVBQUUsZUFBZTthQUN6QjtZQUNBO2dCQUNDLE9BQU8sRUFBRSxXQUFXO2dCQUNwQixNQUFNLEVBQUUsYUFBYTtnQkFDckIsT0FBTyxFQUFFLFdBQVc7Z0JBQ3BCLE9BQU8sRUFBRSxXQUFXO2dCQUNwQixVQUFVLEVBQUU7b0JBQ1o7d0JBQ0ksT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsTUFBTSxFQUFFLENBQUMsdUJBQXVCLENBQUM7d0JBQ2pDLE1BQU0sRUFBRSxhQUFhO3dCQUNyQixVQUFVLEVBQUUsS0FBSzt3QkFDaEIsUUFBUSxFQUFHLFFBQVE7cUJBRXJCO29CQUNEO3dCQUNFLE9BQU8sRUFBRSxXQUFXO3dCQUNwQixNQUFNLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDM0IsTUFBTSxFQUFFLGtCQUFrQjt3QkFDMUIsVUFBVSxFQUFFLEtBQUs7d0JBQ2hCLFFBQVEsRUFBRyxRQUFRO3FCQUVyQjtvQkFDQTt3QkFDQyxPQUFPLEVBQUUsUUFBUTt3QkFDakIsTUFBTSxFQUFFLENBQUMsZUFBZSxDQUFDO3dCQUN6QixNQUFNLEVBQUUsV0FBVzt3QkFDbkIsVUFBVSxFQUFFLEtBQUs7d0JBQ2hCLFFBQVEsRUFBRyxRQUFRO3FCQUVyQjtvQkFDQzt3QkFDQSxPQUFPLEVBQUUsU0FBUzt3QkFDbEIsTUFBTSxFQUFFLENBQUMsY0FBYyxDQUFDO3dCQUN4QixNQUFNLEVBQUUsU0FBUzt3QkFDakIsVUFBVSxFQUFFLEtBQUs7d0JBQ2hCLFFBQVEsRUFBRyxRQUFRO3FCQUVyQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsT0FBTyxFQUFFLFVBQVU7Z0JBQ25CLE1BQU0sRUFBRSxnQkFBZ0I7Z0JBQ3hCLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDckIsT0FBTyxFQUFFLFVBQVU7YUFDcEI7WUFDRDtnQkFDRSxPQUFPLEVBQUUsVUFBVTtnQkFDbkIsTUFBTSxFQUFFLGFBQWE7Z0JBQ3JCLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDckIsT0FBTyxFQUFFLFVBQVU7YUFDcEI7U0FDRixDQUFDO0lBQ0osQ0FBQztJQUVTLHVDQUFRLEdBQWxCO1FBQ0UsSUFBSSxFQUFFLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7UUFFcEMsMkNBQTJDO1FBQzNDLFFBQVE7UUFDUiwyRUFBMkU7UUFDM0UsUUFBUTtRQUNSLHdFQUF3RTtRQUN4RSxrQkFBa0I7UUFDbEIsa0lBQWtJO1FBQ2xJLGdCQUFnQjtRQUNoQix5RkFBeUY7UUFDekYsbURBQW1EO1FBRW5ELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDL0IsRUFBRSxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDYiwwQ0FBMEM7WUFDMUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNyRSxDQUFDO1FBRUQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoQixpQ0FBaUM7WUFDakMsSUFBSSxFQUFFLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzQixNQUFNLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7UUFFRCxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLEVBQUUsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2IseUNBQXlDO1lBQ3pDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDckUsQ0FBQztRQUVELGdCQUFnQjtRQUNoQixNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQXZSVSxvQkFBb0I7UUFQaEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsNkJBQTZCO1lBQzdCLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsZ0RBQWdELENBQUMsQ0FBQztZQUM1RSxXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx1Q0FBdUMsQ0FBQztZQUNuRSxhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtTQUN0QyxDQUFDO3lDQVFvQiwwQkFBVztZQUNiLGlDQUFjO1lBQ1gsNENBQXdCO09BVGxDLG9CQUFvQixDQXlSaEM7SUFBRCwyQkFBQztDQXpSRCxBQXlSQyxJQUFBO0FBelJZLG9EQUFvQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9sYXlvdXRzL2F1dGgvYXV0aC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnYXBwL21vZGVscy91c2VyJztcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nZ2VyU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9sb2dnZXIuc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSwgVG9hc3RlckNvbmZpZyB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXIvYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IEFkbWluTFRFVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy90cmFuc2xhdGUuc2VydmljZSc7XHJcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbGF5b3V0cy1hdXRoJyxcclxuICAvLyB0ZW1wbGF0ZVVybDogJy4vYXV0aC5odG1sJ1xyXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2xheW91dHMvYXV0aC9hdXRoLmNvbXBvbmVudC5jc3MnKV0sXHJcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2xheW91dHMvYXV0aC9hdXRoLmh0bWwnKSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMYXlvdXRzQXV0aENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcHJpdmF0ZSB0b2FzdHJDb25maWc6IFRvYXN0ZXJDb25maWc7XHJcbiAgcHJpdmF0ZSBsb2dnZXI6IExvZ2dlclNlcnZpY2U7XHJcbiAgcHJpdmF0ZSBteWxpbmtzOiBBcnJheTxhbnk+ID0gW107XHJcbiAgcHJpdmF0ZSBpc0dldFVzZXI6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSB0b2FzdHI6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSB0cmFuc2xhdGU6IEFkbWluTFRFVHJhbnNsYXRlU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy50b2FzdHJDb25maWcgPSBuZXcgVG9hc3RlckNvbmZpZyh7XHJcbiAgICAgIG5ld2VzdE9uVG9wOiB0cnVlLFxyXG4gICAgICBzaG93Q2xvc2VCdXR0b246IHRydWUsXHJcbiAgICAgIHRhcFRvRGlzbWlzczogZmFsc2VcclxuICAgIH0pO1xyXG4gICAgLy8gdGhpcy50cmFuc2xhdGUgPSB0cmFuc2xhdGUuZ2V0VHJhbnNsYXRlKCk7XHJcbiAgICAvLyB0aGlzLmxvZ2dlciA9IG5ldyBMb2dnZXJTZXJ2aWNlKCB0aGlzLnRyYW5zbGF0ZSApO1xyXG5cclxuICAgIHRoaXMudXNlclNlcnYuY3VycmVudFVzZXIuc3Vic2NyaWJlKCh1c2VyKSA9PiB7XHJcbiAgICAgIGlmICghdXNlci5lbWFpbCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCd1c2VyJywgdXNlcik7XHJcbiAgICAgICAgdGhpcy5pc0dldFVzZXIgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgbmdPbkluaXQoKSB7XHJcblxyXG4gICAgLy8gdGhpcy51c2VyU2Vydi5jdXJyZW50VXNlci5zdWJzY3JpYmUoKHVzZXIpID0+IHtcclxuICAgIC8vICAgICBpZighdXNlci5lbWFpbCl7XHJcbiAgICAvLyAgICAgICBjb25zb2xlLmxvZygndXNlcicsIHVzZXIpO1xyXG4gICAgLy8gICAgICAgIHRoaXMudXNlclNlcnYuZ2V0QXV0aGVudGljYXRlZFVzZXIoKTtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyB9KTtcclxuICAgICQoZnVuY3Rpb24gKCkge1xyXG4gICAgXHJcbiAgICAgLyokKGRvY3VtZW50KS5vbignY2xpY2snLCAnLnN1Yi1pdGVtJywgZnVuY3Rpb24gKGUpIHsgXHJcbiAgICAgICBcclxuICAgICAgICAgIGxldCBlbGVtczogSFRNTEVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd0cmVldmlldy1tZW51Jyk7XHJcbiAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIGk7XHJcbiAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgZWxlbXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICBlbGVtc1tpXS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmFwcC1jb250ZW50JywgZnVuY3Rpb24gKGUpIHsgXHJcbiAgICAgICAgIGxldCBlbGVtczogSFRNTEVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd0cmVldmlldy1tZW51Jyk7XHJcbiAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIGk7XHJcbiAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgZWxlbXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICBlbGVtc1tpXS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgIFxyXG4gICAgICB9KTsqL1xyXG5cclxuICAgICAgLy8gbmF2XHJcbiAgICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICdbdWktbmF2XSBhJywgZnVuY3Rpb24gKGUpIHtcclxuICAgICAgXHJcbiAgICAgICAgdmFyICR0aGlzID0gJChlLnRhcmdldCksICRhY3RpdmU7XHJcbiAgICAgICAgJHRoaXMuaXMoJ2EnKSB8fCAoJHRoaXMgPSAkdGhpcy5jbG9zZXN0KCdhJykpO1xyXG5cclxuICAgICAgICAkYWN0aXZlID0gJHRoaXMucGFyZW50KCkuc2libGluZ3MoXCIuYWN0aXZlXCIpO1xyXG4gICAgICAgICRhY3RpdmUgJiYgJGFjdGl2ZS50b2dnbGVDbGFzcygnYWN0aXZlJykuZmluZCgnPiB1bDp2aXNpYmxlJykuc2xpZGVVcCgyMDApO1xyXG5cclxuICAgICAgICAoJHRoaXMucGFyZW50KCkuaGFzQ2xhc3MoJ2FjdGl2ZScpICYmICR0aGlzLm5leHQoKS5zbGlkZVVwKDIwMCkpIHx8ICR0aGlzLm5leHQoKS5zbGlkZURvd24oMjAwKTtcclxuICAgICAgICAkdGhpcy5wYXJlbnQoKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcblxyXG4gICAgICAgICR0aGlzLm5leHQoKS5pcygndWwnKSAmJiBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIH0pO1xyXG4gICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnW3VpLXRvZ2dsZS1jbGFzc10nLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB2YXIgJHRoaXMgPSAkKGUudGFyZ2V0KTtcclxuICAgICAgICAkdGhpcy5hdHRyKCd1aS10b2dnbGUtY2xhc3MnKSB8fCAoJHRoaXMgPSAkdGhpcy5jbG9zZXN0KCdbdWktdG9nZ2xlLWNsYXNzXScpKTtcclxuXHJcbiAgICAgICAgdmFyIGNsYXNzZXMgPSAkdGhpcy5hdHRyKCd1aS10b2dnbGUtY2xhc3MnKS5zcGxpdCgnLCcpLFxyXG4gICAgICAgICAgdGFyZ2V0cyA9ICgkdGhpcy5hdHRyKCd0YXJnZXQnKSAmJiAkdGhpcy5hdHRyKCd0YXJnZXQnKS5zcGxpdCgnLCcpKSB8fCBBcnJheSgkdGhpcyksXHJcbiAgICAgICAgICBrZXkgPSAwO1xyXG4gICAgICAgICQuZWFjaChjbGFzc2VzLCBmdW5jdGlvbiAoaW5kZXgsIHZhbHVlKSB7XHJcbiAgICAgICAgICB2YXIgdGFyZ2V0ID0gdGFyZ2V0c1sodGFyZ2V0cy5sZW5ndGggJiYga2V5KV07XHJcbiAgICAgICAgICAkKHRhcmdldCkudG9nZ2xlQ2xhc3MoY2xhc3Nlc1tpbmRleF0pO1xyXG4gICAgICAgICAga2V5Kys7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJHRoaXMudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAodGhpcy5pc0dldFVzZXIpIHtcclxuICAgICAgY29uc29sZS5sb2coJ3VzZXInKTtcclxuICAgICAgdGhpcy51c2VyU2Vydi5nZXRBdXRoZW50aWNhdGVkVXNlcigpO1xyXG4gICAgICB0aGlzLmlzR2V0VXNlciA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgLy8gIHNlZGRpbmcgdGhlIHJlc2l6ZSBldmVudCwgZm9yIEFkbWluTFRFIHRvIHBsYWNlIHRoZSBoZWlnaHRcclxuICAgIGxldCBpZSA9IHRoaXMuZGV0ZWN0SUUoKTtcclxuICAgIGlmICghaWUpIHtcclxuICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCdyZXNpemUnKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBzb2x1dGlvbiBmb3IgSUUgZnJvbSBAaGFrb25hbWF0YXRhXHJcbiAgICAgIGxldCBldmVudCA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdFdmVudCcpO1xyXG4gICAgICBldmVudC5pbml0RXZlbnQoJ3Jlc2l6ZScsIGZhbHNlLCB0cnVlKTtcclxuICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGRlZmluZSBoZXJlIHlvdXIgb3duIGxpbmtzIG1lbnUgc3RydWN0dXJlXHJcbiAgICB0aGlzLm15bGlua3MgPSBbXHJcbiAgICAgIHtcclxuICAgICAgICAndGl0bGUnOiAnRGFzaGJvYXJkJyxcclxuICAgICAgICAnaWNvbic6ICdmYS1ob21lJyxcclxuICAgICAgICAnbGluayc6IFsnL2Rhc2hib2FyZCddLFxyXG4gICAgICAgICdzdGF0ZSc6ICdkYXNoYm9hcmQnXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICAndGl0bGUnOiAnQ2F0ZWdvcnknLFxyXG4gICAgICAgICdpY29uJzogJ2ZhLWN1YmUnLFxyXG4gICAgICAgICdsaW5rJzogWycvY2F0ZWdvcnkvbGlzdCddLFxyXG4gICAgICAgICdzdGF0ZSc6ICdjYXRlZ29yeS1saXN0J1xyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgJ3RpdGxlJzogJ0Zvb2QgSXRlbScsXHJcbiAgICAgICAgJ2ljb24nOiAnZmEtY3V0bGVyeScsXHJcbiAgICAgICAgJ2xpbmsnOiBbJy9mb29kLWl0ZW1zL2xpc3QnXSxcclxuICAgICAgICAnc3RhdGUnOiAnZm9vZC1pdGVtcy1saXN0J1xyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgJ3RpdGxlJzogJ09wdGlvbiBGb29kIEl0ZW0nLFxyXG4gICAgICAgICdpY29uJzogJ2ZhLXNpdGVtYXAnLFxyXG4gICAgICAgICdsaW5rJzogWycvb3B0aW9uLWZvb2QtaXRlbXMvbGlzdCddLFxyXG4gICAgICAgICdzdGF0ZSc6ICdvcHRpb24tZm9vZC1pdGVtcy1saXN0J1xyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgJ3RpdGxlJzogJ09wdGlvbiBWYWx1ZSBJdGVtJyxcclxuICAgICAgICAnaWNvbic6ICdmYS1wbHVzLWNpcmNsZSAgJyxcclxuICAgICAgICAnbGluayc6IFsnL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zL2xpc3QnXSxcclxuICAgICAgICAnc3RhdGUnOiAnb3B0aW9uLWZvb2QtaXRlbXMtbGlzdCdcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgICd0aXRsZSc6ICdUYWJsZScsXHJcbiAgICAgICAgJ2ljb24nOiAnZmEtdGFibGUnLFxyXG4gICAgICAgICdsaW5rJzogWycvdGFibGUnXSxcclxuICAgICAgICAnc3RhdGUnOiAndGFibGUnLFxyXG4gICAgICAgICAnY2xhc3MnOiAndGFibGUnLFxyXG4gICAgICAgICdzdWJsaW5rcyc6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgICd0aXRsZSc6ICdBcmVhJyxcclxuICAgICAgICAgICAgJ2xpbmsnOiBbJy9hcmVhLWxpc3QnXSxcclxuICAgICAgICAgICAgJ2ljb24nOiAnZmEtbGlzdC1hbHQnLFxyXG4gICAgICAgICAgICAnZXh0ZXJuYWwnOiBmYWxzZSxcclxuICAgICAgICAgICAgJ3RhcmdldCcgOiAnX2JsYW5rJ1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICAndGl0bGUnOiAnVGFibGUgTGlzdCcsXHJcbiAgICAgICAgICAgICdsaW5rJzogWycvdGFibGUtbGlzdCddLFxyXG4gICAgICAgICAgICAnaWNvbic6ICdmYS1saXN0LWFsdCcsXHJcbiAgICAgICAgICAgICdleHRlcm5hbCc6IGZhbHNlLFxyXG4gICAgICAgICAgICAndGFyZ2V0JyA6ICdfYmxhbmsnXHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICAndGl0bGUnOiAnUmVzZXJ2YXRpb24nLFxyXG4gICAgICAgICAgICAnbGluayc6IFsnL3RhYmxlLWJvb2tpbmcnXSxcclxuICAgICAgICAgICAgJ2ljb24nOiAnZmEtY2FsZW5kYXInLFxyXG4gICAgICAgICAgICAnZXh0ZXJuYWwnOiBmYWxzZSxcclxuICAgICAgICAgICAgICd0YXJnZXQnIDogJ19ibGFuaydcclxuICAgICAgICAgICBcclxuICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICAndGl0bGUnOiAnQ29udGFjdHMnLFxyXG4gICAgICAgICdpY29uJzogJ2ZhLWFkZHJlc3MtYm9vaycsXHJcbiAgICAgICAgJ2xpbmsnOiBbJy9jb250YWN0J10sXHJcbiAgICAgICAgJ3N0YXRlJzogJ2NvbnRhY3QtbGlzdCdcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgICd0aXRsZSc6ICdOZXdzJyxcclxuICAgICAgICAnaWNvbic6ICdmYS1uZXdzcGFwZXItbycsXHJcbiAgICAgICAgJ2xpbmsnOiBbJy9uZXdzJ10sXHJcbiAgICAgICAgJ3N0YXRlJzogJ25ld3MtbGlzdCdcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgICd0aXRsZSc6ICdPcmRlciBTZXR0aW5nJyxcclxuICAgICAgICAnaWNvbic6ICdmYS1jb2dzJyxcclxuICAgICAgICAnbGluayc6IFsnL29yZGVyLXNldHRpbmcnXSxcclxuICAgICAgICAnc3RhdGUnOiAnb3JkZXItc2V0dGluZydcclxuICAgICAgfSxcclxuICAgICAgIHtcclxuICAgICAgICAndGl0bGUnOiAnTWFya2V0aW5nJyxcclxuICAgICAgICAnaWNvbic6ICdmYS1lbnZlbG9wZScsXHJcbiAgICAgICAgJ3N0YXRlJzogJ21hcmtldGluZycsXHJcbiAgICAgICAgJ2NsYXNzJzogJ21hcmtldGluZycsXHJcbiAgICAgICAgJ3N1YmxpbmtzJzogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgJ3RpdGxlJzogJ0VtYWlsIE1hcmtldGluZycsXHJcbiAgICAgICAgICAgICdsaW5rJzogWycvZW1haWwtbWFya2V0aW5nLWxpc3QnXSxcclxuICAgICAgICAgICAgJ2ljb24nOiAnZmEtZW52ZWxvcGUnLFxyXG4gICAgICAgICAgICAnZXh0ZXJuYWwnOiBmYWxzZSxcclxuICAgICAgICAgICAgICd0YXJnZXQnIDogJ19ibGFuaydcclxuICAgICAgICAgICBcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgICd0aXRsZSc6ICdTdWJzY3JpYmUnLFxyXG4gICAgICAgICAgICAnbGluayc6IFsnL3N1YnNjcmliZS1saXN0J10sXHJcbiAgICAgICAgICAgICdpY29uJzogJ2ZhLWVudmVsb3BlLW9wZW4nLFxyXG4gICAgICAgICAgICAnZXh0ZXJuYWwnOiBmYWxzZSxcclxuICAgICAgICAgICAgICd0YXJnZXQnIDogJ19ibGFuaydcclxuICAgICAgICAgICBcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAge1xyXG4gICAgICAgICAgICAndGl0bGUnOiAnU2VhcmNoJyxcclxuICAgICAgICAgICAgJ2xpbmsnOiBbJy9zZWFyY2gtaXRlbXMnXSxcclxuICAgICAgICAgICAgJ2ljb24nOiAnZmEtc2VhcmNoJyxcclxuICAgICAgICAgICAgJ2V4dGVybmFsJzogZmFsc2UsXHJcbiAgICAgICAgICAgICAndGFyZ2V0JyA6ICdfYmxhbmsnXHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICd0aXRsZSc6ICdSZWl2ZXdzJyxcclxuICAgICAgICAgICAgJ2xpbmsnOiBbJy9yZXZpZXctaXRlbSddLFxyXG4gICAgICAgICAgICAnaWNvbic6ICdmYS1zdGFyJyxcclxuICAgICAgICAgICAgJ2V4dGVybmFsJzogZmFsc2UsXHJcbiAgICAgICAgICAgICAndGFyZ2V0JyA6ICdfYmxhbmsnXHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgJ3RpdGxlJzogJ0NvbnRlbnRzJyxcclxuICAgICAgICAnaWNvbic6ICdmYS1maWxlLXRleHQtbycsXHJcbiAgICAgICAgJ2xpbmsnOiBbJy9jb250ZW50cyddLFxyXG4gICAgICAgICdzdGF0ZSc6ICdjb250ZW50cydcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgICd0aXRsZSc6ICdGZWVkYmFjaycsXHJcbiAgICAgICAgJ2ljb24nOiAnZmEtY29tbWVudHMnLFxyXG4gICAgICAgICdsaW5rJzogWycvZmVlZGJhY2snXSxcclxuICAgICAgICAnc3RhdGUnOiAnZmVlZGJhY2snXHJcbiAgICAgIH1cclxuICAgIF07XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgZGV0ZWN0SUUoKTogYW55IHtcclxuICAgIGxldCB1YSA9IHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50O1xyXG5cclxuICAgIC8vIFRlc3QgdmFsdWVzOyBVbmNvbW1lbnQgdG8gY2hlY2sgcmVzdWx0IOKAplxyXG4gICAgLy8gSUUgMTBcclxuICAgIC8vIHVhID0gJ01vemlsbGEvNS4wIChjb21wYXRpYmxlOyBNU0lFIDEwLjA7IFdpbmRvd3MgTlQgNi4yOyBUcmlkZW50LzYuMCknO1xyXG4gICAgLy8gSUUgMTFcclxuICAgIC8vIHVhID0gJ01vemlsbGEvNS4wIChXaW5kb3dzIE5UIDYuMzsgVHJpZGVudC83LjA7IHJ2OjExLjApIGxpa2UgR2Vja28nO1xyXG4gICAgLy8gSUUgMTIgLyBTcGFydGFuXHJcbiAgICAvLyB1YSA9ICdNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXT1c2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzM5LjAuMjE3MS43MSBTYWZhcmkvNTM3LjM2IEVkZ2UvMTIuMCc7XHJcbiAgICAvLyBFZGdlIChJRSAxMispXHJcbiAgICAvLyB1YSA9ICdNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKVxyXG4gICAgLy8gQ2hyb21lLzQ2LjAuMjQ4Ni4wIFNhZmFyaS81MzcuMzYgRWRnZS8xMy4xMDU4Nic7XHJcblxyXG4gICAgbGV0IG1zaWUgPSB1YS5pbmRleE9mKCdNU0lFICcpO1xyXG4gICAgaWYgKG1zaWUgPiAwKSB7XHJcbiAgICAgIC8vIElFIDEwIG9yIG9sZGVyID0+IHJldHVybiB2ZXJzaW9uIG51bWJlclxyXG4gICAgICByZXR1cm4gcGFyc2VJbnQodWEuc3Vic3RyaW5nKG1zaWUgKyA1LCB1YS5pbmRleE9mKCcuJywgbXNpZSkpLCAxMCk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHRyaWRlbnQgPSB1YS5pbmRleE9mKCdUcmlkZW50LycpO1xyXG4gICAgaWYgKHRyaWRlbnQgPiAwKSB7XHJcbiAgICAgIC8vIElFIDExID0+IHJldHVybiB2ZXJzaW9uIG51bWJlclxyXG4gICAgICBsZXQgcnYgPSB1YS5pbmRleE9mKCdydjonKTtcclxuICAgICAgcmV0dXJuIHBhcnNlSW50KHVhLnN1YnN0cmluZyhydiArIDMsIHVhLmluZGV4T2YoJy4nLCBydikpLCAxMCk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGVkZ2UgPSB1YS5pbmRleE9mKCdFZGdlLycpO1xyXG4gICAgaWYgKGVkZ2UgPiAwKSB7XHJcbiAgICAgIC8vIEVkZ2UgKElFIDEyKykgPT4gcmV0dXJuIHZlcnNpb24gbnVtYmVyXHJcbiAgICAgIHJldHVybiBwYXJzZUludCh1YS5zdWJzdHJpbmcoZWRnZSArIDUsIHVhLmluZGV4T2YoJy4nLCBlZGdlKSksIDEwKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBvdGhlciBicm93c2VyXHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=
