"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var PageNotFoundComponent = (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = __decorate([
        core_1.Component({
            selector: 'page-not-found',
            // templateUrl: './page-not-found.component.html',
            // styleUrls: ['./page-not-found.component.css']
            styleUrls: [utils_1.default.getView('app/components/page-not-found/page-not-found.component.css')],
            templateUrl: utils_1.default.getView('app/components/page-not-found/page-not-found.component.html'),
            animations: [router_animations_1.routerGrow()],
            host: { '[@routerGrow]': '' }
        }),
        __metadata("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());
exports.PageNotFoundComponent = PageNotFoundComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFDcEMsNkRBQXFEO0FBVXJEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQix3Q0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUxVLHFCQUFxQjtRQVRqQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixrREFBa0Q7WUFDbEQsZ0RBQWdEO1lBQ2hELFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsNERBQTRELENBQUMsQ0FBQztZQUN4RixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyw2REFBNkQsQ0FBQztZQUN6RixVQUFVLEVBQUUsQ0FBQyw4QkFBVSxFQUFFLENBQUM7WUFDMUIsSUFBSSxFQUFFLEVBQUMsZUFBZSxFQUFFLEVBQUUsRUFBQztTQUM1QixDQUFDOztPQUNXLHFCQUFxQixDQU9qQztJQUFELDRCQUFDO0NBUEQsQUFPQyxJQUFBO0FBUFksc0RBQXFCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IHJvdXRlckdyb3cgfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwYWdlLW5vdC1mb3VuZCcsXG4gIC8vIHRlbXBsYXRlVXJsOiAnLi9wYWdlLW5vdC1mb3VuZC5jb21wb25lbnQuaHRtbCcsXG4gIC8vIHN0eWxlVXJsczogWycuL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5jc3MnXVxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9wYWdlLW5vdC1mb3VuZC9wYWdlLW5vdC1mb3VuZC5jb21wb25lbnQuY3NzJyldLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvcGFnZS1ub3QtZm91bmQvcGFnZS1ub3QtZm91bmQuY29tcG9uZW50Lmh0bWwnKSxcbiAgYW5pbWF0aW9uczogW3JvdXRlckdyb3coKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJHcm93XSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBQYWdlTm90Rm91bmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19
