"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var table_service_1 = require("app/services/table.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var number_validator_directive_1 = require("app/utils/number-validator.directive");
var CreateTableComponent = (function () {
    function CreateTableComponent(fb, tableService, StorageService, authServ, notif, router, route) {
        this.fb = fb;
        this.tableService = tableService;
        this.StorageService = StorageService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.formErrors = {
            'name': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            },
            'slots': {
                'required': 'The slot is required.',
                'maxlength': 'The slot must be at less 2 characters long.',
                'invalidNumber': 'The slot must be number'
            }
        };
        /*upload file*/
        this.sactiveColor = 'green';
        this.baseColor = '#ccc';
        this.overlayColor = 'rgba(255,255,255,0.5)';
        this.dragging = false;
        this.loaded = false;
        this.imageLoaded = false;
        this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
        this.flgImageChoose = false;
        this.iconColor = '';
        this.borderColor = '';
        this.activeColor = '';
        this.hiddenImage = false;
    }
    CreateTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            _this.isCheckRadio = '1';
            _this.createTableForm.controls['nameRadio'].setValue(1);
            if (_this.id != null) {
                _this.tableItem = _this.StorageService.getScope();
                if (_this.tableItem == false) {
                    _this.fetchAllTables(_this.restId, _this.id);
                }
                else {
                    _this.createTableForm.controls['name'].setValue(_this.tableItem.name);
                    _this.createTableForm.controls['nameRadio'].setValue(_this.tableItem.is_active);
                    _this.createTableForm.controls['slots'].setValue(_this.tableItem.slots);
                    _this.createTableForm.controls['description'].setValue(_this.tableItem.content);
                    _this.name = _this.createTableForm.controls['name'];
                    _this.nameRadio = _this.createTableForm.controls['nameRadio'];
                    _this.description = _this.createTableForm.controls['description'];
                    _this.slots = _this.createTableForm.controls['slots'];
                    _this.isCheckRadio = _this.tableItem.is_active;
                    _this.imageSrc = _this.tableItem.thumb;
                    if (_this.imageSrc == null) {
                        _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                    }
                }
            }
        });
    };
    CreateTableComponent.prototype.checkFlg = function (value) {
        if (this.isCheckRadio == undefined || this.isCheckRadio == null) {
            this.isCheckRadio = '1';
        }
        if (value == this.isCheckRadio) {
            return true;
        }
    };
    CreateTableComponent.prototype.changenameRadio = function (value) {
        this.isCheckRadio = value;
    };
    CreateTableComponent.prototype.fetchAllTables = function (restId, id) {
        var _this = this;
        this.tableService.getTables(restId, id, null).then(function (response) {
            if (response.tables != null && response.tables.length > 0) {
                _this.tableItem = response.tables[0];
                _this.createTableForm.controls['name'].setValue(_this.tableItem.name);
                _this.createTableForm.controls['nameRadio'].setValue(_this.tableItem.is_active);
                _this.createTableForm.controls['description'].setValue(_this.tableItem.content);
                _this.createTableForm.controls['slots'].setValue(_this.tableItem.slots);
                _this.name = _this.createTableForm.controls['name'];
                _this.description = _this.createTableForm.controls['description'];
                _this.nameRadio = _this.createTableForm.controls['nameRadio'];
                _this.slots = _this.createTableForm.controls['slots'];
                _this.isCheckRadio = _this.tableItem.is_active;
                _this.imageSrc = _this.tableItem.thumb;
                if (_this.imageSrc == null) {
                    _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                }
            }
            else {
                _this.notif.error('table had delete');
                _this.router.navigateByUrl('table-list');
            }
        }, function (error) {
            console.log(error);
        });
    };
    CreateTableComponent.prototype.buildForm = function () {
        var _this = this;
        this.createTableForm = this.fb.group({
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ],
            'slots': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(2),
                    number_validator_directive_1.invalidNumberValidator()
                ]
            ], 'nameRadio': [],
            'description': []
        });
        this.name = this.createTableForm.controls['name'];
        this.nameRadio = this.createTableForm.controls['nameRadio'];
        this.description = this.createTableForm.controls['description'];
        this.slots = this.createTableForm.controls['slots'];
        this.createTableForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    CreateTableComponent.prototype.onValueChanged = function (data) {
        if (!this.createTableForm) {
            return;
        }
        var form = this.createTableForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    CreateTableComponent.prototype.handleDragEnter = function () {
        this.dragging = true;
    };
    CreateTableComponent.prototype.handleDragLeave = function () {
        this.dragging = false;
    };
    CreateTableComponent.prototype.handleDrop = function (e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    };
    CreateTableComponent.prototype.handleImageLoad = function () {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if (this.imageLoaded = true) {
            this.hiddenImage = true;
        }
    };
    CreateTableComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        this.loaded = false;
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose = true;
    };
    CreateTableComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    };
    CreateTableComponent.prototype._setActive = function () {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    };
    CreateTableComponent.prototype._setInactive = function () {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    };
    CreateTableComponent.prototype.createTable = function () {
        var _this = this;
        var id = '-1';
        if (this.id != null) {
            id = this.id;
        }
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.tableObject = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'table': {
                'id': id,
                'name': this.createTableForm.value.name,
                'content': this.createTableForm.value.description,
                'thumb': '',
                'slots': this.createTableForm.value.slots,
                'is_active': this.isCheckRadio,
                'is_delete': '0'
            }
        };
        this.tableService.createTable(this.tableObject).then(
        //          response  => {
        //          	this.tables = response;
        //          	console.log(response, this.tables);
        //          },
        //  error => {console.log(error)
        function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
        // });
    };
    CreateTableComponent.prototype.processResult = function (response) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            var imagePost = '';
            if (this.flgImageChoose == true) {
                imagePost = this.imageSrc;
            }
            this.tables = response;
            var tableUpload = {
                'id': this.tables.table_id,
                'thumb': imagePost
            };
            this.tableService.tableUploadFile(tableUpload).then(function (response) {
                _this.notif.success('New table has been added');
                _this.router.navigateByUrl('table-list');
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.name) {
                    this.formErrors['name'] = this.error.name;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    CreateTableComponent.prototype.failedCreate = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    CreateTableComponent.prototype.goBack = function () {
        this.router.navigate(['/table-list'], { relativeTo: this.route });
    };
    CreateTableComponent = __decorate([
        core_1.Component({
            selector: 'create-table',
            templateUrl: utils_1.default.getView('app/components/create-table/create-table.component.html'),
            styleUrls: [utils_1.default.getView('app/components/create-table/create-table.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': ''
            }
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            table_service_1.TableService,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router, router_2.ActivatedRoute])
    ], CreateTableComponent);
    return CreateTableComponent;
}());
exports.CreateTableComponent = CreateTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NyZWF0ZS10YWJsZS9jcmVhdGUtdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHdDQUE0RjtBQUM1Rix5Q0FBb0M7QUFHcEMsNkRBQTJEO0FBQzNELDREQUEwRDtBQUMxRCwwREFBeUQ7QUFDekQsMkNBQThDO0FBQzlDLDBFQUF3RTtBQUN4RSwwQ0FBeUM7QUFDekMsMENBQWlEO0FBQ2pELGdFQUE4RDtBQUM5RCxtRkFBOEU7QUFVOUU7SUFjRSw4QkFBb0IsRUFBZSxFQUMzQixZQUEwQixFQUMxQixjQUE4QixFQUM5QixRQUFxQixFQUNyQixLQUEwQixFQUN6QixNQUFjLEVBQVMsS0FBcUI7UUFMakMsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUMzQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFxSHZELGVBQVUsR0FBRztZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1gsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQU8sdUJBQXVCO2dCQUN4QyxXQUFXLEVBQU0sK0NBQStDO2FBQ2pFO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLFVBQVUsRUFBTyx1QkFBdUI7Z0JBQ3hDLFdBQVcsRUFBTSw2Q0FBNkM7Z0JBQzlELGVBQWUsRUFBRSx5QkFBeUI7YUFFM0M7U0FDRixDQUFDO1FBRUYsZUFBZTtRQUNSLGlCQUFZLEdBQVcsT0FBTyxDQUFDO1FBQzVCLGNBQVMsR0FBVyxNQUFNLENBQUM7UUFDM0IsaUJBQVksR0FBVyx1QkFBdUIsQ0FBQztRQUUvQyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLFdBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFDN0IsYUFBUSxHQUFXLGlFQUFpRSxDQUFDO1FBQ3JGLG1CQUFjLEdBQVcsS0FBSyxDQUFDO1FBQy9CLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFDdEIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBUyxLQUFLLENBQUM7SUFsSnBDLENBQUM7SUFFRCx1Q0FBUSxHQUFSO1FBQUEsaUJBK0JDO1FBOUJFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQzVDLEtBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RCxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsRUFBRSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ2hCLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDaEQsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLENBQUMsQ0FBQSxDQUFDO29CQUMxQixLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUMzQyxDQUFDO2dCQUFBLElBQUksQ0FBQSxDQUFDO29CQUNGLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwRSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3RFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUU5RSxLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNsRCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUM1RCxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUNoRSxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNwRCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDO29CQUM3QyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO29CQUNyQyxFQUFFLENBQUEsQ0FBRSxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7d0JBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsaUVBQWlFLENBQUM7b0JBQ3JGLENBQUM7Z0JBQ0wsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCx1Q0FBUSxHQUFSLFVBQVMsS0FBSztRQUNWLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUUsU0FBUyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUMxRCxJQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQztRQUMxQixDQUFDO1FBQ0QsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQSxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztJQUNMLENBQUM7SUFDRCw4Q0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRSxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUNPLDZDQUFjLEdBQXRCLFVBQXVCLE1BQU0sRUFBQyxFQUFFO1FBQWhDLGlCQTBCRTtRQXpCQSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDL0IsVUFBQSxRQUFRO1lBQ04sRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBRSxJQUFJLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDckQsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFdkUsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbEQsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDaEUsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDNUQsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDcEQsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQztnQkFDN0MsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQztnQkFDckMsRUFBRSxDQUFBLENBQUUsS0FBSSxDQUFDLFFBQVEsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO29CQUN0QixLQUFJLENBQUMsUUFBUSxHQUFHLGlFQUFpRSxDQUFDO2dCQUNyRixDQUFDO1lBQ0YsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQ3JDLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzFDLENBQUM7UUFDSCxDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDSSx3Q0FBUyxHQUFqQjtRQUFBLGlCQXlCRztRQXhCQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ25DLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDVCxrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUI7YUFDRjtZQUNELE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDVixrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDdkIsbURBQXNCLEVBQUU7aUJBQ3pCO2FBQ0YsRUFBQyxXQUFXLEVBQUMsRUFBRTtZQUNoQixhQUFhLEVBQUMsRUFBRTtTQUVqQixDQUFDLENBQUM7UUFDQSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXZELElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWTthQUM5QixTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUF6QixDQUF5QixDQUFDLENBQUM7UUFFaEQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsa0NBQWtDO0lBQzNELENBQUM7SUFFUSw2Q0FBYyxHQUF0QixVQUF1QixJQUFVO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQ3RDLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7UUFFbEMsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDL0MsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFxQ0MsOENBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ3pCLENBQUM7SUFFRCw4Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUVELHlDQUFVLEdBQVYsVUFBVyxDQUFDO1FBQ1IsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsOENBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNuQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDakMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDekIsQ0FBQztJQUNDLENBQUM7SUFFRCxnREFBaUIsR0FBakIsVUFBa0IsQ0FBQztRQUNmLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxPQUFPLEdBQUcsU0FBUyxDQUFDO1FBQ3hCLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFFOUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDeEIsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBRXBCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRCxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLEdBQUUsSUFBSSxDQUFDO0lBQzlCLENBQUM7SUFFRCxrREFBbUIsR0FBbkIsVUFBb0IsQ0FBQztRQUNqQixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUN2QixDQUFDO0lBRUQseUNBQVUsR0FBVjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNwQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUN0QyxDQUFDO0lBQ0wsQ0FBQztJQUVELDJDQUFZLEdBQVo7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDbEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDcEMsQ0FBQztJQUNMLENBQUM7SUFDTCwwQ0FBVyxHQUFYO1FBQUEsaUJBa0NDO1FBakNDLElBQUksRUFBRSxHQUFJLElBQUksQ0FBQztRQUNmLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNoQixFQUFFLEdBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNoQixDQUFDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLHdDQUF3QztZQUN2QyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNuQyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0osQ0FBQztRQUNILElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDVCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3JCLE9BQU8sRUFBQztnQkFDTixJQUFJLEVBQUMsRUFBRTtnQkFDUCxNQUFNLEVBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSTtnQkFDdEMsU0FBUyxFQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVc7Z0JBQ2hELE9BQU8sRUFBQyxFQUFFO2dCQUNWLE9BQU8sRUFBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLO2dCQUN4QyxXQUFXLEVBQUMsSUFBSSxDQUFDLFlBQVk7Z0JBQzdCLFdBQVcsRUFBQyxHQUFHO2FBQ2hCO1NBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJO1FBQzNDLDBCQUEwQjtRQUMxQixvQ0FBb0M7UUFDcEMsZ0RBQWdEO1FBQ2hELGNBQWM7UUFDbkIsZ0NBQWdDO1FBQ3ZCLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBNUIsQ0FBNEIsRUFDcEMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO1FBQ3ZELE1BQU07SUFFYixDQUFDO0lBRU8sNENBQWEsR0FBckIsVUFBc0IsUUFBUTtRQUE5QixpQkFrQ0c7UUFoQ0MsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQy9ELElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNuQixFQUFFLENBQUEsQ0FBRSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQy9CLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQzVCLENBQUM7WUFDQSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQztZQUN2QixJQUFJLFdBQVcsR0FBRztnQkFDZixJQUFJLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRO2dCQUN6QixPQUFPLEVBQUMsU0FBUzthQUNuQixDQUFBO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUNuQyxVQUFBLFFBQVE7Z0JBQ1AsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsMEJBQTBCLENBQUMsQ0FBQztnQkFDL0MsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUNkLFVBQUEsS0FBSztnQkFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBRTdCLENBQUMsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO1lBQzdCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUM1QyxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLHFCQUFJLENBQ0YsY0FBYyxFQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQ2IsT0FBTyxDQUNSLENBQUMsS0FBSyxDQUFDLHFCQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckIsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBQ08sMkNBQVksR0FBcEIsVUFBc0IsR0FBRztRQUN2QixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdEIsMEJBQTBCO1FBQzVCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JELHlCQUF5QjtZQUMzQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sNENBQTRDO2dCQUM1QyxHQUFHLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2xDLHdEQUF3RDtnQkFDMUQsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUVELHFDQUFNLEdBQU47UUFDQSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUEvVFUsb0JBQW9CO1FBUmhDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsY0FBYztZQUN4QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx5REFBeUQsQ0FBQztZQUNyRixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHdEQUF3RCxDQUFDLENBQUM7WUFDcEYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFO2FBQ2pDO1NBQ0EsQ0FBQzt5Q0Fld0IsbUJBQVc7WUFDYiw0QkFBWTtZQUNWLGdDQUFjO1lBQ3BCLDBCQUFXO1lBQ2QsMENBQW1CO1lBQ2pCLGVBQU0sRUFBZ0IsdUJBQWM7T0FuQjFDLG9CQUFvQixDQWdVaEM7SUFBRCwyQkFBQztDQWhVRCxBQWdVQyxJQUFBO0FBaFVZLG9EQUFvQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9jcmVhdGUtdGFibGUvY3JlYXRlLXRhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0gLEFic3RyYWN0Q29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgcm91dGVyRmFkZSB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBUYWJsZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvdGFibGUuc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5pbXBvcnQgeyBpbnZhbGlkTnVtYmVyVmFsaWRhdG9yIH0gZnJvbSBcImFwcC91dGlscy9udW1iZXItdmFsaWRhdG9yLmRpcmVjdGl2ZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjcmVhdGUtdGFibGUnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvY3JlYXRlLXRhYmxlL2NyZWF0ZS10YWJsZS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jcmVhdGUtdGFibGUvY3JlYXRlLXRhYmxlLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ1xufVxufSlcbmV4cG9ydCBjbGFzcyBDcmVhdGVUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0cHJpdmF0ZSBpZDogc3RyaW5nO1xuICBwcml2YXRlIHN1YjogYW55O1xuXHRwcml2YXRlIG15bW9kZWw6IGFueTtcblx0cHJpdmF0ZSBjcmVhdGVUYWJsZUZvcm06IEZvcm1Hcm91cDtcblx0cHJpdmF0ZSBuYW1lOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgc2xvdHM6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBkZXNjcmlwdGlvbjogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIG5hbWVSYWRpbzogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIHJlc3RJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSBpc0NoZWNrUmFkaW8gOiBzdHJpbmc7XG4gIHByaXZhdGUgdGFibGVJdGVtIDogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLFxuICBwcml2YXRlIHRhYmxlU2VydmljZTogVGFibGVTZXJ2aWNlLFxuICBwcml2YXRlIFN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG4gICBwcml2YXRlIHJvdXRlcjogUm91dGVyLHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICAgbGV0IHVzZXJJbmZvID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgIHRoaXMucmVzdElkID0gdXNlckluZm8ucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICBcdCB0aGlzLmJ1aWxkRm9ybSgpO1xuICAgIFx0dGhpcy5zdWIgPSB0aGlzLnJvdXRlLnBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcbiAgICAgIHRoaXMuaWQgPSBwYXJhbXNbJ2lkJ107XG4gICAgICB0aGlzLmlzQ2hlY2tSYWRpbyA9ICcxJztcbiAgICAgIHRoaXMuY3JlYXRlVGFibGVGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXS5zZXRWYWx1ZSgxKTtcbiAgICAgIGlmKHRoaXMuaWQhPW51bGwpe1xuICAgICAgICB0aGlzLnRhYmxlSXRlbSA9IHRoaXMuU3RvcmFnZVNlcnZpY2UuZ2V0U2NvcGUoKTtcbiAgICAgICAgaWYodGhpcy50YWJsZUl0ZW0gPT0gZmFsc2Upe1xuICAgICAgICAgIHRoaXMuZmV0Y2hBbGxUYWJsZXModGhpcy5yZXN0SWQsdGhpcy5pZCk7XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ25hbWUnXS5zZXRWYWx1ZSh0aGlzLnRhYmxlSXRlbS5uYW1lKTtcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVGFibGVGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXS5zZXRWYWx1ZSh0aGlzLnRhYmxlSXRlbS5pc19hY3RpdmUpO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ3Nsb3RzJ10uc2V0VmFsdWUodGhpcy50YWJsZUl0ZW0uc2xvdHMpO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ2Rlc2NyaXB0aW9uJ10uc2V0VmFsdWUodGhpcy50YWJsZUl0ZW0uY29udGVudCk7XG5cbiAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlVGFibGVGb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgICAgICAgICB0aGlzLm5hbWVSYWRpbyA9IHRoaXMuY3JlYXRlVGFibGVGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXTtcbiAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXTtcbiAgICAgICAgICAgIHRoaXMuc2xvdHMgPSB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snc2xvdHMnXTtcbiAgICAgICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gdGhpcy50YWJsZUl0ZW0uaXNfYWN0aXZlO1xuICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9IHRoaXMudGFibGVJdGVtLnRodW1iO1xuICAgICAgICAgICAgaWYoIHRoaXMuaW1hZ2VTcmM9PW51bGwpe1xuICAgICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgY2hlY2tGbGcodmFsdWUpe1xuICAgICAgaWYodGhpcy5pc0NoZWNrUmFkaW89PXVuZGVmaW5lZCB8fCB0aGlzLmlzQ2hlY2tSYWRpbz09bnVsbCl7XG4gICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gJzEnO1xuICAgICAgfVxuICAgICAgaWYodmFsdWU9PXRoaXMuaXNDaGVja1JhZGlvKXtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgfVxuICBjaGFuZ2VuYW1lUmFkaW8odmFsdWUpe1xuICAgIHRoaXMuaXNDaGVja1JhZGlvID12YWx1ZTtcbiAgfVxuICBwcml2YXRlIGZldGNoQWxsVGFibGVzKHJlc3RJZCxpZCkge1xuICBcdFx0dGhpcy50YWJsZVNlcnZpY2UuZ2V0VGFibGVzKHJlc3RJZCxpZCxudWxsKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2UudGFibGVzIT1udWxsICYmIHJlc3BvbnNlLnRhYmxlcy5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy50YWJsZUl0ZW0gPSByZXNwb25zZS50YWJsZXNbMF07XG4gICAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ25hbWUnXS5zZXRWYWx1ZSh0aGlzLnRhYmxlSXRlbS5uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snbmFtZVJhZGlvJ10uc2V0VmFsdWUodGhpcy50YWJsZUl0ZW0uaXNfYWN0aXZlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXS5zZXRWYWx1ZSh0aGlzLnRhYmxlSXRlbS5jb250ZW50KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snc2xvdHMnXS5zZXRWYWx1ZSh0aGlzLnRhYmxlSXRlbS5zbG90cyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubmFtZSA9IHRoaXMuY3JlYXRlVGFibGVGb3JtLmNvbnRyb2xzWyduYW1lJ107XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlc2NyaXB0aW9uID0gdGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ2Rlc2NyaXB0aW9uJ107XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5hbWVSYWRpbyA9IHRoaXMuY3JlYXRlVGFibGVGb3JtLmNvbnRyb2xzWyduYW1lUmFkaW8nXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2xvdHMgPSB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snc2xvdHMnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNDaGVja1JhZGlvID0gdGhpcy50YWJsZUl0ZW0uaXNfYWN0aXZlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9IHRoaXMudGFibGVJdGVtLnRodW1iO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYoIHRoaXMuaW1hZ2VTcmM9PW51bGwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVNyYyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmLmVycm9yKCd0YWJsZSBoYWQgZGVsZXRlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgndGFibGUtbGlzdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbnByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xuICAgIHRoaXMuY3JlYXRlVGFibGVGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAnbmFtZSc6IFsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgICAgXVxuICAgICAgXSxcbiAgICAgICdzbG90cyc6IFsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMiksXG4gICAgICAgICAgaW52YWxpZE51bWJlclZhbGlkYXRvcigpXG4gICAgICAgIF1cbiAgICAgIF0sJ25hbWVSYWRpbyc6W10sXG4gICAgICAnZGVzY3JpcHRpb24nOltdXG5cbiAgICB9KTtcbiAgICAgICB0aGlzLm5hbWUgPSB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgICAgIHRoaXMubmFtZVJhZGlvID0gdGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ25hbWVSYWRpbyddO1xuICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSB0aGlzLmNyZWF0ZVRhYmxlRm9ybS5jb250cm9sc1snZGVzY3JpcHRpb24nXTtcbiAgICAgICB0aGlzLnNsb3RzID0gdGhpcy5jcmVhdGVUYWJsZUZvcm0uY29udHJvbHNbJ3Nsb3RzJ107XG5cbiAgICB0aGlzLmNyZWF0ZVRhYmxlRm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGRhdGEpKTtcblxuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTsgLy8gKHJlKXNldCB2YWxpZGF0aW9uIG1lc3NhZ2VzIG5vd1xuICB9XG5cbiAgIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQoZGF0YT86IGFueSkge1xuICAgIGlmICghdGhpcy5jcmVhdGVUYWJsZUZvcm0pIHsgcmV0dXJuOyB9XG4gICAgY29uc3QgZm9ybSA9IHRoaXMuY3JlYXRlVGFibGVGb3JtO1xuXG4gICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIC8vIGNsZWFyIHByZXZpb3VzIGVycm9yIG1lc3NhZ2UgKGlmIGFueSlcbiAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSBbXTtcbiAgICAgIGNvbnN0IGNvbnRyb2wgPSBmb3JtLmdldChmaWVsZCk7XG5cbiAgICAgIGlmIChjb250cm9sICYmIGNvbnRyb2wuZGlydHkgJiYgIWNvbnRyb2wudmFsaWQpIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZXMgPSB0aGlzLnZhbGlkYXRpb25NZXNzYWdlc1tmaWVsZF07XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIGNvbnRyb2wuZXJyb3JzKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5wdXNoKG1lc3NhZ2VzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5mb3JtRXJyb3JzID0ge1xuICAgICduYW1lJzogW11cbiAgfTtcblxuICB2YWxpZGF0aW9uTWVzc2FnZXMgPSB7XG4gICAgJ25hbWUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgbmFtZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIG5hbWUgbXVzdCBiZSBhdCBsZXNzIDI1NiBjaGFyYWN0ZXJzIGxvbmcuJ1xuICAgIH0sXG4gICAgJ3Nsb3RzJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIHNsb3QgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBzbG90IG11c3QgYmUgYXQgbGVzcyAyIGNoYXJhY3RlcnMgbG9uZy4nLFxuICAgICAgJ2ludmFsaWROdW1iZXInOiAnVGhlIHNsb3QgbXVzdCBiZSBudW1iZXInXG5cbiAgICB9XG4gIH07XG5cbiAgLyp1cGxvYWQgZmlsZSovXG5cdHByaXZhdGUgc2FjdGl2ZUNvbG9yOiBzdHJpbmcgPSAnZ3JlZW4nO1xuICAgIHByaXZhdGUgYmFzZUNvbG9yOiBzdHJpbmcgPSAnI2NjYyc7XG4gICAgcHJpdmF0ZSBvdmVybGF5Q29sb3I6IHN0cmluZyA9ICdyZ2JhKDI1NSwyNTUsMjU1LDAuNSknO1xuXG4gICAgcHJpdmF0ZSBkcmFnZ2luZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgbG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpbWFnZUxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaW1hZ2VTcmM6IHN0cmluZyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgIHByaXZhdGUgZmxnSW1hZ2VDaG9vc2U6Ym9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaWNvbkNvbG9yOiBzdHJpbmcgPSAnJztcbiAgICBwcml2YXRlICBib3JkZXJDb2xvcjogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSAgYWN0aXZlQ29sb3I6IHN0cmluZyA9ICcnO1xuICAgIHByaXZhdGUgaGlkZGVuSW1hZ2U6Ym9vbGVhbj1mYWxzZTtcbiAgICBwcml2YXRlIHRhYmxlcyA6IGFueTtcbiAgICBwcml2YXRlIHRhYmxlT2JqZWN0IDogYW55O1xuICAgIHByaXZhdGUgdXNlcklkIDogU3RyaW5nO1xuICAgIHByaXZhdGUgZmlsZUNob29zZSA6IEZpbGU7XG4gICAgcHJpdmF0ZSBlcnJvcjogYW55O1xuICAgIGhhbmRsZURyYWdFbnRlcigpIHtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IHRydWU7XG4gICAgfVxuXG4gICAgaGFuZGxlRHJhZ0xlYXZlKCkge1xuICAgICAgICB0aGlzLmRyYWdnaW5nID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaGFuZGxlRHJvcChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLmhhbmRsZUlucHV0Q2hhbmdlKGUpO1xuICAgIH1cblxuICAgIGhhbmRsZUltYWdlTG9hZCgpIHtcbiAgICAgICAgdGhpcy5pbWFnZUxvYWRlZCA9IHRydWU7XG4gICAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5vdmVybGF5Q29sb3I7XG4gICAgICAgIGlmKHRoaXMuaW1hZ2VMb2FkZWQgPSB0cnVlKXtcblx0XHRcdHRoaXMuaGlkZGVuSW1hZ2UgPSB0cnVlO1xuXHRcdH1cbiAgICB9XG5cbiAgICBoYW5kbGVJbnB1dENoYW5nZShlKSB7XG4gICAgICAgIHZhciBmaWxlID0gZS5kYXRhVHJhbnNmZXIgPyBlLmRhdGFUcmFuc2Zlci5maWxlc1swXSA6IGUudGFyZ2V0LmZpbGVzWzBdO1xuICAgICAgICB0aGlzLmZpbGVDaG9vc2UgPSBmaWxlO1xuICAgICAgICB2YXIgcGF0dGVybiA9IC9pbWFnZS0qLztcbiAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cbiAgICAgICAgaWYgKCFmaWxlLnR5cGUubWF0Y2gocGF0dGVybikpIHtcbiAgICAgICAgICAgIGFsZXJ0KCdpbnZhbGlkIGZvcm1hdCcpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5sb2FkZWQgPSBmYWxzZTtcblxuICAgICAgICByZWFkZXIub25sb2FkID0gdGhpcy5faGFuZGxlUmVhZGVyTG9hZGVkLmJpbmQodGhpcyk7XG4gICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuICAgICAgICB0aGlzLmZsZ0ltYWdlQ2hvb3NlPSB0cnVlO1xuICAgIH1cblxuICAgIF9oYW5kbGVSZWFkZXJMb2FkZWQoZSkge1xuICAgICAgICB2YXIgcmVhZGVyID0gZS50YXJnZXQ7XG4gICAgICAgIHRoaXMuaW1hZ2VTcmMgPSByZWFkZXIucmVzdWx0O1xuICAgICAgICB0aGlzLmxvYWRlZCA9IHRydWU7XG4gICAgfVxuXG4gICAgX3NldEFjdGl2ZSgpIHtcbiAgICAgICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYWN0aXZlQ29sb3I7XG4gICAgICAgIGlmICh0aGlzLmltYWdlU3JjLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLmFjdGl2ZUNvbG9yO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgX3NldEluYWN0aXZlKCkge1xuICAgICAgICB0aGlzLmJvcmRlckNvbG9yID0gdGhpcy5iYXNlQ29sb3I7XG4gICAgICAgIGlmICh0aGlzLmltYWdlU3JjLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLmJhc2VDb2xvcjtcbiAgICAgICAgfVxuICAgIH1cbmNyZWF0ZVRhYmxlKCl7XG4gIGxldCBpZCAgPSAnLTEnO1xuICBpZih0aGlzLmlkIT1udWxsKXtcbiAgICBpZCAgPSB0aGlzLmlkO1xuICB9XG4gIHRoaXMub25WYWx1ZUNoYW5nZWQoKTtcbiAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIC8vIGNsZWFyIHByZXZpb3VzIGVycm9yIG1lc3NhZ2UgKGlmIGFueSlcbiAgICAgICBpZih0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+MCl7XG4gICAgICAgICByZXR1cm47XG4gICAgICAgfVxuICAgIH1cbiAgdGhpcy50YWJsZU9iamVjdCA9IHtcbiAgICAgICAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAgICAgICAndXNlcl9pZCc6dGhpcy51c2VySWQsXG4gICAgICAgICAgICAndGFibGUnOntcbiAgICAgICAgICAgICAgJ2lkJzppZCxcbiAgICAgICAgICAgICAgJ25hbWUnOnRoaXMuY3JlYXRlVGFibGVGb3JtLnZhbHVlLm5hbWUsXG4gICAgICAgICAgICAgICdjb250ZW50Jzp0aGlzLmNyZWF0ZVRhYmxlRm9ybS52YWx1ZS5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgJ3RodW1iJzonJyxcbiAgICAgICAgICAgICAgJ3Nsb3RzJzp0aGlzLmNyZWF0ZVRhYmxlRm9ybS52YWx1ZS5zbG90cyxcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6dGhpcy5pc0NoZWNrUmFkaW8sXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOicwJ1xuICAgICAgICAgICAgfX07XG4gIFx0dGhpcy50YWJsZVNlcnZpY2UuY3JlYXRlVGFibGUodGhpcy50YWJsZU9iamVjdCkudGhlbihcbiAgICAgICAgICAgIC8vICAgICAgICAgIHJlc3BvbnNlICA9PiB7XG4gICAgICAgICAgICAvLyAgICAgICAgICBcdHRoaXMudGFibGVzID0gcmVzcG9uc2U7XG4gICAgICAgICAgICAvLyAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlLCB0aGlzLnRhYmxlcyk7XG4gICAgICAgICAgICAvLyAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0Ly8gIGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSxcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG4gIFx0XHRcdFx0XHQvLyB9KTtcblxufVxuXG5wcml2YXRlIHByb2Nlc3NSZXN1bHQocmVzcG9uc2UpIHtcblxuICAgIGlmIChyZXNwb25zZS5tZXNzYWdlID09IHVuZGVmaW5lZCB8fCByZXNwb25zZS5tZXNzYWdlID09ICdPSycpIHtcbiAgICAgbGV0IGltYWdlUG9zdCA9ICcnO1xuICAgICBpZiggdGhpcy5mbGdJbWFnZUNob29zZSA9PSB0cnVlKXtcbiAgICAgICBpbWFnZVBvc3QgPSB0aGlzLmltYWdlU3JjO1xuICAgICB9XG4gICAgICB0aGlzLnRhYmxlcyA9IHJlc3BvbnNlO1xuICAgICAgbGV0IHRhYmxlVXBsb2FkID0ge1xuICAgICAgICAgJ2lkJzp0aGlzLnRhYmxlcy50YWJsZV9pZCxcbiAgICAgICAgICd0aHVtYic6aW1hZ2VQb3N0XG4gICAgICB9XG5cdCAgICB0aGlzLnRhYmxlU2VydmljZS50YWJsZVVwbG9hZEZpbGUodGFibGVVcGxvYWQpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyB0YWJsZSBoYXMgYmVlbiBhZGRlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ3RhYmxlLWxpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuXG4gIFx0XHRcdFx0XHR9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lcnJvciA9IHJlc3BvbnNlLmVycm9ycztcbiAgICAgIGlmIChyZXNwb25zZS5jb2RlID09IDQyMikge1xuICAgICAgICBpZiAodGhpcy5lcnJvci5uYW1lKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWyduYW1lJ10gPSB0aGlzLmVycm9yLm5hbWU7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN3YWwoXG4gICAgICAgICAgJ0NyZWF0ZSBGYWlsIScsXG4gICAgICAgICAgdGhpcy5lcnJvclswXSxcbiAgICAgICAgICAnZXJyb3InXG4gICAgICAgICkuY2F0Y2goc3dhbC5ub29wKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBmYWlsZWRDcmVhdGUgKHJlcykge1xuICAgIGlmIChyZXMuc3RhdHVzID09IDQwMSkge1xuICAgICAgLy8gdGhpcy5sb2dpbmZhaWxlZCA9IHRydWVcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHJlcy5kYXRhLmVycm9ycy5tZXNzYWdlWzBdID09ICdFbWFpbCBVbnZlcmlmaWVkJykge1xuICAgICAgICAvLyB0aGlzLnVudmVyaWZpZWQgPSB0cnVlXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBvdGhlciBraW5kcyBvZiBlcnJvciByZXR1cm5lZCBmcm9tIHNlcnZlclxuICAgICAgICBmb3IgKHZhciBlcnJvciBpbiByZXMuZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICAvLyB0aGlzLmxvZ2luZmFpbGVkZXJyb3IgKz0gcmVzLmRhdGEuZXJyb3JzW2Vycm9yXSArICcgJ1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZ29CYWNrKCk6IHZvaWQge1xuICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy90YWJsZS1saXN0J10sIHsgcmVsYXRpdmVUbzogdGhpcy5yb3V0ZSB9KTtcbiAgfVxufVxuIl19
