"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var restaurant_service_1 = require("app/services/restaurant.service");
var notification_service_1 = require("app/services/notification.service");
var user_service_1 = require("app/services/user.service");
var router_animations_1 = require("../../router.animations");
var ng2_select_1 = require("ng2-select");
var area_service_1 = require("app/services/area.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var number_validator_directive_1 = require("app/utils/number-validator.directive");
var email_validator_directive_1 = require("app/utils/email-validator.directive");
var reserve_service_1 = require("app/services/reserve.service");
var table_service_1 = require("app/services/table.service");
var sweetalert2_1 = require("sweetalert2");
var ReserveTableComponent = (function () {
    function ReserveTableComponent(areaService, reserveService, restServ, fb, router, route, tableService, userServ, notifyServ) {
        this.areaService = areaService;
        this.reserveService = reserveService;
        this.restServ = restServ;
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.tableService = tableService;
        this.userServ = userServ;
        this.notifyServ = notifyServ;
        this.areaItems = [];
        this.tableItems = [];
        this.tableReserve = [];
        this.tablesRemoved = [];
        this.valueArea = {};
        this.valueTable = {};
        this.content = null;
        this.fromDate = null;
        this.formErrors = {
            'name': [],
            'phone': [],
            'email': [],
            'date': [],
            'open_time': [],
            'close_time': [],
            'reserve_date': [],
            'address': [],
            'people_count': [],
            'comment': []
        };
        this.validationMessages = {
            'name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            },
            'email': {
                'minlength': 'The email must be at least 256 characters long.',
                'invalidEmail': 'The email must be a valid email address.',
            },
            'phone': {
                'required': 'The phone is required.',
                'maxlength': 'The phone must be at less 32 characters long.',
                'invalidNumber': 'The phone must be number'
            },
            'address': {
                'required': 'The address is required.',
                'maxlength': 'The address must be at less 256 characters long.'
            },
            'date': {
                'required': 'The date is required.'
            },
            'open_time': {
                'required': 'The open time is required.'
            },
            'close_time': {
                'required': 'The close time is required.'
            },
            'people_count': {
                'required': 'The the number of people is required.',
                'maxlength': 'The number of people must be at less 2 characters long.',
                'invalidNumber': 'The phone must be number'
            },
            'comment': {
                'required': 'The comment is required.',
                'maxlength': 'The comment must be at less 256 characters long.'
            },
        };
    }
    ReserveTableComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.buildForm();
        this.minDate = new Date();
        this.fetchAllAreasAndTables(this.restId);
    };
    ReserveTableComponent.prototype.fetchAllAreasAndTables = function (restId) {
        var _this = this;
        this.areaService.getAreas(restId, null).then(function (response) {
            _this.areas = response.areas;
            for (var i = 0; i < _this.areas.length; i++) {
                _this.areaItems.push({
                    id: _this.areas[i].id,
                    text: "" + _this.areas[i].name
                });
            }
            _this.selectAreas.items = _this.areaItems;
            _this.valueArea = '';
            _this.fetchAllTables(restId, _this.valueArea.id);
            console.log(response.areas);
        }, function (error) {
            console.log(error);
        });
    };
    ReserveTableComponent.prototype.fetchAllTables = function (restId, area_id) {
        var _this = this;
        this.tableService.getTables(restId, null, null).then(function (response) {
            _this.tables = response.tables;
            _this.tableItems = [];
            for (var i = 0; i < _this.tables.length; i++) {
                if (_this.tables[i].area_id == area_id) {
                    _this.tableItems.push({
                        id: _this.tables[i].id,
                        text: "" + _this.tables[i].name
                    });
                }
            }
            _this.slots = 0;
            _this.fetchTableByArea(null);
            _this.valueTable = '';
            console.log(response.areas);
        }, function (error) {
            console.log(error);
        });
    };
    ReserveTableComponent.prototype.fetchTableByArea = function (area_id) {
        this.tableItems = [];
        if (area_id != null) {
            for (var i = 0; i < this.tables.length; i++) {
                if (this.tables[i].area_id == area_id) {
                    this.tableItems.push({
                        id: this.tables[i].id,
                        text: "" + this.tables[i].name
                    });
                }
            }
        }
        else {
            for (var i = 0; i < this.tables.length; i++) {
                this.tableItems.push({
                    id: this.tables[i].id,
                    text: "" + this.tables[i].name
                });
            }
        }
        this.selectTables.items = this.tableItems;
        this.valueTable = this.tableItems[0];
    };
    ReserveTableComponent.prototype.selectedAreas = function (value) {
        console.log('Selected value is: ', value);
        var areaId = null;
        if (this.valueArea.id != undefined && this.valueArea.id != null && this.valueArea.id != 0) {
            areaId = this.valueArea.id;
            this.valueTable = '';
        }
    };
    ReserveTableComponent.prototype.selectedTables = function (value) {
        console.log('Selected value is: ', value);
        var tableId = null;
        if (this.valueTable.id != undefined && this.valueTable.id != null && this.valueTable.id != 0) {
            tableId = this.valueTable.id;
        }
        var tableAreaId = null;
        var areaItem;
        for (var i = 0; i < this.tables.length; i++) {
            if (value.id == this.tables[i].id) {
                console.log(this.tables[i].id);
                this.slots = this.tables[i].slots;
                for (var j = 0; j < this.areaItems.length; j++) {
                    if (this.areaItems[j].id == this.tables[i].area_id) {
                        areaItem = {
                            id: this.areaItems[j].id,
                            text: "" + this.areaItems[j].text
                        };
                    }
                }
                this.valueArea = areaItem;
                return;
            }
        }
    };
    ReserveTableComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
        this.slots = 0;
        this.valueArea = '';
        this.valueTable = '';
    };
    ReserveTableComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    ReserveTableComponent.prototype.refreshAreaValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.valueArea = value;
            var areaId = null;
            if (this.valueArea.id != undefined && this.valueArea.id != null && this.valueArea.id != 0) {
                areaId = this.valueArea.id;
            }
            this.fetchTableByArea(areaId);
        }
        else {
            this.fetchTableByArea(null);
        }
    };
    ReserveTableComponent.prototype.refreshTableValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.valueTable = value;
        }
    };
    ReserveTableComponent.prototype.onChangeOpen = function (value) {
        console.log('Changed value is: ', value);
    };
    ReserveTableComponent.prototype.onChangeOpenValue = function (event) {
        var value1 = event.target.value;
        console.log('Changed value is: ', event.target.value);
        if (value1 == "" || value1 == null) {
            this.createReserveForm.controls['open_time'].setValue(null);
            this.open_time = this.createReserveForm.controls['open_time'];
        }
        var nameRe = new RegExp(/^[0-9]+$/i);
        if (!nameRe.test(value1) || value1.length > 2) {
            this.createReserveForm.controls['open_time'].setValue(null);
            this.open_time = this.createReserveForm.controls['open_time'];
        }
    };
    ReserveTableComponent.prototype.onChangeCloseValue = function (event) {
        var value1 = event.target.value;
        console.log('Changed value is: ', event.target.value);
        if (value1 == "" || value1 == null) {
            this.createReserveForm.controls['close_time'].setValue(null);
            this.close_time = this.createReserveForm.controls['close_time'];
        }
        var nameRe = new RegExp(/^[0-9]+$/i);
        if (!nameRe.test(value1) || value1.length > 2) {
            this.createReserveForm.controls['close_time'].setValue(null);
            this.close_time = this.createReserveForm.controls['close_time'];
        }
    };
    ReserveTableComponent.prototype.onChangeClose = function (value1) {
        console.log('Changed value is: ', value1);
    };
    ReserveTableComponent.prototype.onSelectFromDate = function (value) {
        console.log('New search input: ', value);
    };
    ReserveTableComponent.prototype.check = function () {
        console.log(this.close_time.value);
        console.log(this.open_time.value);
        console.log(this.date.value);
    };
    ReserveTableComponent.prototype.chooseSpaceTime = function () {
        alert('click');
    };
    ReserveTableComponent.prototype.buildForm = function () {
        var _this = this;
        this.createReserveForm = this.fb.group({
            'open_time': ['', [
                    forms_1.Validators.required
                ]],
            'close_time': ['', [
                    forms_1.Validators.required
                ]],
            'date': ['', [
                    forms_1.Validators.required
                ]],
            'name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]],
            'phone': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(32),
                    number_validator_directive_1.invalidNumberValidator()
                ]],
            'email': ['', [
                    forms_1.Validators.maxLength(256),
                    email_validator_directive_1.invalidEmailValidator()
                ]],
            'address': ['', [
                    forms_1.Validators.maxLength(256),
                ]],
            'comment': ['', [
                    forms_1.Validators.maxLength(256),
                ]],
            'people_count': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(99),
                    number_validator_directive_1.invalidNumberValidator()
                ]],
        });
        var ng = new Date();
        var ng1 = new Date();
        this.createReserveForm.controls['open_time'].setValue(ng);
        this.open_time = this.createReserveForm.controls['open_time'];
        this.createReserveForm.controls['close_time'].setValue(ng1);
        this.close_time = this.createReserveForm.controls['close_time'];
        this.phone = this.createReserveForm.controls['phone'];
        this.email = this.createReserveForm.controls['email'];
        this.address = this.createReserveForm.controls['address'];
        this.people_count = this.createReserveForm.controls['people_count'];
        this.date = this.createReserveForm.controls['date'];
        this.name = this.createReserveForm.controls['name'];
        this.comment = this.createReserveForm.controls['comment'];
        this.createReserveForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    ReserveTableComponent.prototype.onValueChanged = function (data) {
        if (!this.createReserveForm) {
            return;
        }
        var form = this.createReserveForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    ReserveTableComponent.prototype.addToReserve = function () {
        if (this.valueArea == null || this.valueTable == null || this.slots == 0
            || this.valueArea == '' || this.valueTable == '' || this.slots == '') {
        }
        else {
            for (var _i = 0, _a = this.tables; _i < _a.length; _i++) {
                var v = _a[_i];
                if (v.id == this.valueTable.id) {
                    this.tables.splice(this.tables.indexOf(v), 1);
                    this.tablesRemoved.push(v);
                }
            }
            var itemReserve = {
                area_id: this.valueArea.id,
                textArea: this.valueArea.text,
                id: this.valueTable.id,
                name: this.valueTable.text,
                slots: this.slots
            };
            this.tableReserve.unshift(itemReserve);
            this.valueArea = '';
            this.slots = 0;
            this.fetchTableByArea(null);
            this.valueTable = '';
        }
    };
    ReserveTableComponent.prototype.removeTable = function (item) {
        console.log(item);
        this.tableReserve.splice(this.tableReserve.indexOf(item), 1);
        this.tables.unshift(item);
        this.fetchTableByArea(null);
        this.valueArea = '';
        this.valueTable = '';
    };
    ReserveTableComponent.prototype.createReserve = function () {
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                sweetalert2_1.default("Please check all the information again!", "", "error");
                return;
            }
            if (!this.open_time.value) {
                this.formErrors.open_time.push("The open time is required.");
            }
            if (!this.close_time.value) {
                this.formErrors.close_time.push("The close time is required.");
            }
            if (this.name.value == "") {
                this.formErrors.name.push("The name is required.");
            }
            if (this.phone.value == "") {
                this.formErrors.phone.push("The phone is required.");
            }
            if (this.people_count.value == "") {
                this.formErrors.people_count.push("The number of person is required.");
            }
            if (this.date.value == "") {
                this.formErrors.date.push("The date is required.");
            }
            if (!this.open_time.value || !this.close_time.value
                || this.name.value == "" || this.phone.value == ""
                || this.people_count.value == ""
                || this.date.value == "") {
                sweetalert2_1.default("Please fill all the blank!", "", "error");
                return;
            }
        }
        console.log("name: " + this.name.value);
        console.log("phone: " + this.phone.value);
        console.log("email: " + this.email.value);
        console.log("address: " + this.address.value);
        console.log("people_count: " + this.people_count.value);
        console.log("date: " + this.date.value);
        console.log("comment: " + this.comment.value);
        console.log("open_time: " + this.open_time.value.toLocaleTimeString([], { hour12: false }));
        console.log("close_time: " + this.close_time.value.toLocaleTimeString([], { hour12: false }));
        console.log(this.tableReserve);
        var sum = 0;
        var param = {
            'user': {
                'name': this.name.value,
                'phone': this.phone.value,
                'email': this.email.value,
                'address': this.address.value,
                'people_count': this.people_count.value,
                'date': this.date.value,
                'comment': this.comment.value,
                'open_time': this.open_time.value.toLocaleTimeString([], { hour12: false }),
                'close_time': this.close_time.value.toLocaleTimeString([], { hour12: false }),
            },
            'table': this.tableReserve
        };
        var that = this;
        for (var _i = 0, _a = this.tableReserve; _i < _a.length; _i++) {
            var v = _a[_i];
            sum += Number(v.slots);
        }
        if (sum < Number(this.people_count.value)) {
            sweetalert2_1.default({
                title: 'Bạn có chắc không ?',
                text: "Số người tham dự nhiều hơn số bàn đặt!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Không',
                confirmButtonText: 'Chấp nhận!'
            }).then(function () {
                sweetalert2_1.default('Thành công !', 'Bàn của bạn đã được đặt thành công.', 'success');
                that.reserveService.createReserve(param);
                return;
            });
        }
    };
    ReserveTableComponent.prototype.goBack = function () {
        this.router.navigate(['/table-booking'], { relativeTo: this.route });
    };
    __decorate([
        core_1.ViewChild('SelectAreasId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], ReserveTableComponent.prototype, "selectAreas", void 0);
    __decorate([
        core_1.ViewChild('SelectTablesId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], ReserveTableComponent.prototype, "selectTables", void 0);
    ReserveTableComponent = __decorate([
        core_1.Component({
            selector: 'reserve-table',
            templateUrl: utils_1.default.getView('app/components/reserve-table/reserve-table.component.html'),
            styleUrls: [utils_1.default.getView('app/components/reserve-table/reserve-table.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [area_service_1.AreaService,
            reserve_service_1.ReserveService,
            restaurant_service_1.RestaurantService,
            forms_1.FormBuilder,
            router_1.Router,
            router_2.ActivatedRoute,
            table_service_1.TableService,
            user_service_1.UserService,
            notification_service_1.NotificationService])
    ], ReserveTableComponent);
    return ReserveTableComponent;
}());
exports.ReserveTableComponent = ReserveTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3Jlc2VydmUtdGFibGUvcmVzZXJ2ZS10YWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBaUY7QUFDakYsd0NBQTZGO0FBQzdGLHlDQUFvQztBQUNwQyxzRUFBb0U7QUFDcEUsMEVBQXdFO0FBQ3hFLDBEQUF3RDtBQUN4RCw2REFBMkQ7QUFFM0QseUNBQXlEO0FBQ3pELDBEQUF3RDtBQUN4RCwwQ0FBeUM7QUFDekMsMENBQWlEO0FBQ2pELG1GQUE4RTtBQUM5RSxpRkFBNEU7QUFDNUUsZ0VBQThEO0FBRTlELDREQUEwRDtBQUMxRCwyQ0FBOEM7QUFTOUM7SUF1Q0UsK0JBQ1UsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsUUFBMkIsRUFDM0IsRUFBZSxFQUNmLE1BQWMsRUFDZCxLQUFxQixFQUNyQixZQUEwQixFQUMxQixRQUFxQixFQUNyQixVQUErQjtRQVIvQixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsYUFBUSxHQUFSLFFBQVEsQ0FBbUI7UUFDM0IsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNyQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGVBQVUsR0FBVixVQUFVLENBQXFCO1FBM0JqQyxjQUFTLEdBQWUsRUFBRSxDQUFDO1FBQzNCLGVBQVUsR0FBZSxFQUFFLENBQUM7UUFDNUIsaUJBQVksR0FBZSxFQUFFLENBQUM7UUFDOUIsa0JBQWEsR0FBZSxFQUFFLENBQUM7UUFJL0IsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUNwQixlQUFVLEdBQVEsRUFBRSxDQUFDO1FBSXJCLFlBQU8sR0FBb0IsSUFBSSxDQUFDO1FBSWhDLGFBQVEsR0FBUSxJQUFJLENBQUM7UUFpUzdCLGVBQVUsR0FBRztZQUNYLE1BQU0sRUFBRSxFQUFFO1lBQ1gsT0FBTyxFQUFFLEVBQUU7WUFDVixPQUFPLEVBQUUsRUFBRTtZQUNYLE1BQU0sRUFBRSxFQUFFO1lBQ1YsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtZQUNoQixjQUFjLEVBQUUsRUFBRTtZQUNsQixTQUFTLEVBQUUsRUFBRTtZQUNiLGNBQWMsRUFBRSxFQUFFO1lBQ2xCLFNBQVMsRUFBRSxFQUFFO1NBQ2QsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQUUsdUJBQXVCO2dCQUNuQyxXQUFXLEVBQUUsK0NBQStDO2FBQzdEO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLFdBQVcsRUFBRSxpREFBaUQ7Z0JBQzlELGNBQWMsRUFBRSwwQ0FBMEM7YUFDM0Q7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsVUFBVSxFQUFFLHdCQUF3QjtnQkFDcEMsV0FBVyxFQUFFLCtDQUErQztnQkFDNUQsZUFBZSxFQUFFLDBCQUEwQjthQUM1QztZQUNELFNBQVMsRUFBRTtnQkFDVCxVQUFVLEVBQUUsMEJBQTBCO2dCQUN0QyxXQUFXLEVBQUUsa0RBQWtEO2FBQ2hFO1lBQ0QsTUFBTSxFQUFFO2dCQUNOLFVBQVUsRUFBRSx1QkFBdUI7YUFDcEM7WUFDRCxXQUFXLEVBQUU7Z0JBQ1gsVUFBVSxFQUFFLDRCQUE0QjthQUN6QztZQUNELFlBQVksRUFBRTtnQkFDWixVQUFVLEVBQUUsNkJBQTZCO2FBQzFDO1lBQ0QsY0FBYyxFQUFFO2dCQUNkLFVBQVUsRUFBRSx1Q0FBdUM7Z0JBQ25ELFdBQVcsRUFBRSx5REFBeUQ7Z0JBQ3RFLGVBQWUsRUFBRSwwQkFBMEI7YUFDNUM7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsVUFBVSxFQUFFLDBCQUEwQjtnQkFDdEMsV0FBVyxFQUFFLGtEQUFrRDthQUNoRTtTQUNGLENBQUM7SUF0VUUsQ0FBQztJQUVMLHdDQUFRLEdBQVI7UUFDRSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRTNDLENBQUM7SUFFTyxzREFBc0IsR0FBOUIsVUFBK0IsTUFBTTtRQUFyQyxpQkFtQkM7UUFsQkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDMUMsVUFBQSxRQUFRO1lBQ04sS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1lBRTVCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDM0MsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7b0JBQ2xCLEVBQUUsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3BCLElBQUksRUFBRSxLQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBTTtpQkFDOUIsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztZQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUM7WUFDeEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFDcEIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLEVBQ0QsVUFBQSxLQUFLO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyw4Q0FBYyxHQUF0QixVQUF1QixNQUFNLEVBQUUsT0FBTztRQUF0QyxpQkF1QkM7UUF0QkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQ2xELFVBQUEsUUFBUTtZQUNOLEtBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUVyQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzVDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO3dCQUNuQixFQUFFLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUNyQixJQUFJLEVBQUUsS0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQU07cUJBQy9CLENBQUMsQ0FBQztnQkFDTCxDQUFDO1lBQ0gsQ0FBQztZQUNELEtBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVCLEtBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1lBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsRUFDRCxVQUFBLEtBQUs7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUVNLGdEQUFnQixHQUF2QixVQUF3QixPQUFPO1FBQzdCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDNUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDdEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7d0JBQ25CLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3JCLElBQUksRUFBRSxLQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBTTtxQkFDL0IsQ0FBQyxDQUFDO2dCQUNMLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQkFDbkIsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDckIsSUFBSSxFQUFFLEtBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFNO2lCQUMvQixDQUFDLENBQUM7WUFDTCxDQUFDO1FBQ0gsQ0FBQztRQUNELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFHTSw2Q0FBYSxHQUFwQixVQUFxQixLQUFVO1FBRTdCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRixNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDdkIsQ0FBQztJQUNILENBQUM7SUFFTSw4Q0FBYyxHQUFyQixVQUFzQixLQUFVO1FBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ25CLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3RixPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUM7UUFDL0IsQ0FBQztRQUNELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLFFBQVEsQ0FBQztRQUNiLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUM1QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUMvQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNsQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7b0JBQy9DLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDbkQsUUFBUSxHQUFHOzRCQUNULEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7NEJBQ3hCLElBQUksRUFBRSxLQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBTTt5QkFDbEMsQ0FBQTtvQkFDSCxDQUFDO2dCQUNILENBQUM7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7Z0JBQzFCLE1BQU0sQ0FBQztZQUNULENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUVNLHVDQUFPLEdBQWQsVUFBZSxLQUFVO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU0scUNBQUssR0FBWixVQUFhLEtBQVU7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sZ0RBQWdCLEdBQXZCLFVBQXdCLEtBQVU7UUFDaEMsRUFBRSxDQUFDLENBQUMsS0FBSyxJQUFJLFNBQVMsSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7WUFDN0IsQ0FBQztZQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUIsQ0FBQztJQUNILENBQUM7SUFFTSxpREFBaUIsR0FBeEIsVUFBeUIsS0FBVTtRQUNqQyxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksU0FBUyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQzFCLENBQUM7SUFDSCxDQUFDO0lBRU0sNENBQVksR0FBbkIsVUFBb0IsS0FBVTtRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDTSxpREFBaUIsR0FBeEIsVUFBeUIsS0FBVTtRQUNqQyxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEQsRUFBRSxDQUFDLENBQUMsTUFBTSxJQUFJLEVBQUUsSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEUsQ0FBQztRQUNELElBQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hFLENBQUM7SUFDSCxDQUFDO0lBQ00sa0RBQWtCLEdBQXpCLFVBQTBCLEtBQVU7UUFDbEMsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RELEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxFQUFFLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2xFLENBQUM7UUFDRCxJQUFNLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN2QyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNsRSxDQUFDO0lBRUgsQ0FBQztJQUNNLDZDQUFhLEdBQXBCLFVBQXFCLE1BQVc7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU0sZ0RBQWdCLEdBQXZCLFVBQXdCLEtBQVU7UUFFaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0scUNBQUssR0FBWjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRy9CLENBQUM7SUFFTSwrQ0FBZSxHQUF0QjtRQUNFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNqQixDQUFDO0lBRU8seUNBQVMsR0FBakI7UUFBQSxpQkF5REM7UUF4REMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBRXJDLFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDaEIsa0JBQVUsQ0FBQyxRQUFRO2lCQUNwQixDQUFDO1lBQ0YsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNqQixrQkFBVSxDQUFDLFFBQVE7aUJBQ3BCLENBQUM7WUFDRixNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ1gsa0JBQVUsQ0FBQyxRQUFRO2lCQUNwQixDQUFDO1lBQ0YsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNYLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUMxQixDQUFDO1lBQ0YsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNaLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO29CQUN4QixtREFBc0IsRUFBRTtpQkFFekIsQ0FBQztZQUNGLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDWixrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUM7b0JBQ3pCLGlEQUFxQixFQUFFO2lCQUN4QixDQUFDO1lBQ0YsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNkLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUIsQ0FBQztZQUNGLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDZCxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUM7aUJBQzFCLENBQUM7WUFDRixjQUFjLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ25CLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO29CQUN4QixtREFBc0IsRUFBRTtpQkFDekIsQ0FBQztTQUNILENBQUMsQ0FBQztRQUNILElBQUksRUFBRSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDcEIsSUFBSSxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUVyQixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUUxRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWTthQUNoQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUF6QixDQUF5QixDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsa0NBQWtDO0lBRTNELENBQUM7SUFFTyw4Q0FBYyxHQUF0QixVQUF1QixJQUFVO1FBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFDeEMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBRXBDLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRWhDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEQsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBcURPLDRDQUFZLEdBQXBCO1FBQ0UsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDO2VBQ25FLElBQUksQ0FBQyxTQUFTLElBQUksRUFBRSxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUV6RSxDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDSixHQUFHLENBQUMsQ0FBVSxVQUFXLEVBQVgsS0FBQSxJQUFJLENBQUMsTUFBTSxFQUFYLGNBQVcsRUFBWCxJQUFXO2dCQUFwQixJQUFJLENBQUMsU0FBQTtnQkFDUixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDL0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixDQUFDO2FBQ0Y7WUFDRCxJQUFJLFdBQVcsR0FDYjtnQkFDRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUMxQixRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJO2dCQUM3QixFQUFFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUN0QixJQUFJLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJO2dCQUMxQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7YUFDbEIsQ0FBQTtZQUVILElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7SUFDSCxDQUFDO0lBRU8sMkNBQVcsR0FBbkIsVUFBb0IsSUFBSTtRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU8sNkNBQWEsR0FBckI7UUFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLHFCQUFJLENBQUMseUNBQXlDLEVBQUMsRUFBRSxFQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMzRCxNQUFNLENBQUM7WUFDVCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUMxQixDQUFDO2dCQUNDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1lBQy9ELENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQzNCLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDakUsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFFLEVBQUUsQ0FBQyxDQUN4QixDQUFDO2dCQUNDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ3JELENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBRSxFQUFFLENBQUMsQ0FDekIsQ0FBQztnQkFDQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUN2RCxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUUsRUFBRSxDQUFDLENBQ2hDLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDekUsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFFLEVBQUUsQ0FBQyxDQUN4QixDQUFDO2dCQUNDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ3JELENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLO21CQUM5QyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRSxFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUUsRUFBRTttQkFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUUsRUFBRTttQkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUUsRUFDdEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0QscUJBQUksQ0FBQyw0QkFBNEIsRUFBQyxFQUFFLEVBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQztZQUNULENBQUM7UUFHSCxDQUFDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDNUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5RixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMvQixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDWixJQUFJLEtBQUssR0FBRztZQUNWLE1BQU0sRUFBQztnQkFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUN2QixPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLO2dCQUN6QixPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLO2dCQUN6QixTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUM3QixjQUFjLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLO2dCQUN2QyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUN2QixTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUM3QixXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2dCQUMzRSxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2FBQzlFO1lBQ0QsT0FBTyxFQUFFLElBQUksQ0FBQyxZQUFZO1NBQzNCLENBQUE7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsR0FBRyxDQUFDLENBQVUsVUFBaUIsRUFBakIsS0FBQSxJQUFJLENBQUMsWUFBWSxFQUFqQixjQUFpQixFQUFqQixJQUFpQjtZQUExQixJQUFJLENBQUMsU0FBQTtZQUNSLEdBQUcsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3hCO1FBQ0QsRUFBRSxDQUFBLENBQUMsR0FBRyxHQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQ3hDLENBQUM7WUFDQyxxQkFBSSxDQUFDO2dCQUNILEtBQUssRUFBRSxxQkFBcUI7Z0JBQzVCLElBQUksRUFBRSx3Q0FBd0M7Z0JBQzlDLElBQUksRUFBRSxTQUFTO2dCQUNmLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLGtCQUFrQixFQUFFLFNBQVM7Z0JBQzdCLGlCQUFpQixFQUFFLE1BQU07Z0JBQ3pCLGdCQUFnQixFQUFDLE9BQU87Z0JBQ3hCLGlCQUFpQixFQUFFLFlBQVk7YUFDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDTixxQkFBSSxDQUNGLGNBQWMsRUFDZCxxQ0FBcUMsRUFDckMsU0FBUyxDQUNWLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3pDLE1BQU0sQ0FBQztZQUNULENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztJQUNILENBQUM7SUFFRCxzQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFqZ0IyQjtRQUEzQixnQkFBUyxDQUFDLGVBQWUsQ0FBQztrQ0FBcUIsNEJBQWU7OERBQUM7SUFDbkM7UUFBNUIsZ0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztrQ0FBc0IsNEJBQWU7K0RBQUM7SUFGdkQscUJBQXFCO1FBUGpDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQywyREFBMkQsQ0FBQztZQUN2RixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDBEQUEwRCxDQUFDLENBQUM7WUFDdEYsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBRSxxQkFBcUIsRUFBRSxFQUFFLEVBQUU7U0FDcEMsQ0FBQzt5Q0F5Q3VCLDBCQUFXO1lBQ1IsZ0NBQWM7WUFDcEIsc0NBQWlCO1lBQ3ZCLG1CQUFXO1lBQ1AsZUFBTTtZQUNQLHVCQUFjO1lBQ1AsNEJBQVk7WUFDaEIsMEJBQVc7WUFDVCwwQ0FBbUI7T0FoRDlCLHFCQUFxQixDQW9nQmpDO0lBQUQsNEJBQUM7Q0FwZ0JELEFBb2dCQyxJQUFBO0FBcGdCWSxzREFBcUIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvcmVzZXJ2ZS10YWJsZS9yZXNlcnZlLXRhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uICB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IFJlc3RhdXJhbnRTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3Jlc3RhdXJhbnQuc2VydmljZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG5pbXBvcnQge1NlbGVjdE1vZHVsZSwgU2VsZWN0Q29tcG9uZW50fSBmcm9tICduZzItc2VsZWN0JztcbmltcG9ydCB7IEFyZWFTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2FyZWEuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgaW52YWxpZE51bWJlclZhbGlkYXRvciB9IGZyb20gXCJhcHAvdXRpbHMvbnVtYmVyLXZhbGlkYXRvci5kaXJlY3RpdmVcIjtcbmltcG9ydCB7IGludmFsaWRFbWFpbFZhbGlkYXRvciB9IGZyb20gXCJhcHAvdXRpbHMvZW1haWwtdmFsaWRhdG9yLmRpcmVjdGl2ZVwiO1xuaW1wb3J0IHsgUmVzZXJ2ZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvcmVzZXJ2ZS5zZXJ2aWNlJztcblxuaW1wb3J0IHsgVGFibGVTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3RhYmxlLnNlcnZpY2UnO1xuaW1wb3J0IHsgZGVmYXVsdCBhcyBzd2FsIH0gZnJvbSAnc3dlZXRhbGVydDInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdyZXNlcnZlLXRhYmxlJyxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL3Jlc2VydmUtdGFibGUvcmVzZXJ2ZS10YWJsZS5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9yZXNlcnZlLXRhYmxlL3Jlc2VydmUtdGFibGUuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsgJ1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJyB9XG59KVxuZXhwb3J0IGNsYXNzIFJlc2VydmVUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBWaWV3Q2hpbGQoJ1NlbGVjdEFyZWFzSWQnKSBwdWJsaWMgc2VsZWN0QXJlYXM6IFNlbGVjdENvbXBvbmVudDtcbiAgQFZpZXdDaGlsZCgnU2VsZWN0VGFibGVzSWQnKSBwdWJsaWMgc2VsZWN0VGFibGVzOiBTZWxlY3RDb21wb25lbnQ7XG5cbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgb3Blbl90aW1lOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgY2xvc2VfdGltZTogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIHBob25lOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgZW1haWw6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBkYXRlOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgbmFtZTogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIGFkZHJlc3M6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBwZW9wbGVfY291bnQ6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBjb21tZW50OiBBYnN0cmFjdENvbnRyb2w7XG5cblxuXG5cbiAgcHJpdmF0ZSBtaW5EYXRlOiBEYXRlO1xuICBwcml2YXRlIGFyZWFJdGVtczogQXJyYXk8YW55PiA9IFtdO1xuICBwcml2YXRlIHRhYmxlSXRlbXM6IEFycmF5PGFueT4gPSBbXTtcbiAgcHJpdmF0ZSB0YWJsZVJlc2VydmU6IEFycmF5PGFueT4gPSBbXTtcbiAgcHJpdmF0ZSB0YWJsZXNSZW1vdmVkOiBBcnJheTxhbnk+ID0gW107XG5cbiAgcHJpdmF0ZSBhcmVhczogYW55O1xuICBwcml2YXRlIHRhYmxlczogYW55O1xuICBwcml2YXRlIHZhbHVlQXJlYTogYW55ID0ge307XG4gIHByaXZhdGUgdmFsdWVUYWJsZTogYW55ID0ge307XG4gIHByaXZhdGUgc2xvdHM6IGFueTtcblxuICBwcml2YXRlIGNyZWF0ZVJlc2VydmVGb3JtOiBGb3JtR3JvdXA7XG4gIHByaXZhdGUgY29udGVudDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBTZXR0aW5nczogYW55O1xuICBwcml2YXRlIHN0YWRpdW1MaXN0OiBhbnlbXTtcbiAgcHJpdmF0ZSB0aW1lTGlzdDogYW55W107XG4gIHByaXZhdGUgZnJvbURhdGU6IGFueSA9IG51bGw7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBhcmVhU2VydmljZTogQXJlYVNlcnZpY2UsXG4gICAgcHJpdmF0ZSByZXNlcnZlU2VydmljZTogUmVzZXJ2ZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSByZXN0U2VydjogUmVzdGF1cmFudFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHRhYmxlU2VydmljZTogVGFibGVTZXJ2aWNlLFxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGxldCB1c2VySW5mbyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgIHRoaXMucmVzdElkID0gdXNlckluZm8ucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgdGhpcy51c2VySWQgPSB1c2VySW5mby51c2VyX2luZm8uaWQ7XG4gICAgdGhpcy5idWlsZEZvcm0oKTtcbiAgICB0aGlzLm1pbkRhdGUgPSBuZXcgRGF0ZSgpO1xuICAgIHRoaXMuZmV0Y2hBbGxBcmVhc0FuZFRhYmxlcyh0aGlzLnJlc3RJZCk7XG5cbiAgfVxuXG4gIHByaXZhdGUgZmV0Y2hBbGxBcmVhc0FuZFRhYmxlcyhyZXN0SWQpIHtcbiAgICB0aGlzLmFyZWFTZXJ2aWNlLmdldEFyZWFzKHJlc3RJZCwgbnVsbCkudGhlbihcbiAgICAgIHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5hcmVhcyA9IHJlc3BvbnNlLmFyZWFzO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5hcmVhcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIHRoaXMuYXJlYUl0ZW1zLnB1c2goe1xuICAgICAgICAgICAgaWQ6IHRoaXMuYXJlYXNbaV0uaWQsXG4gICAgICAgICAgICB0ZXh0OiBgJHt0aGlzLmFyZWFzW2ldLm5hbWV9YFxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2VsZWN0QXJlYXMuaXRlbXMgPSB0aGlzLmFyZWFJdGVtcztcbiAgICAgICAgdGhpcy52YWx1ZUFyZWEgPSAnJztcbiAgICAgICAgdGhpcy5mZXRjaEFsbFRhYmxlcyhyZXN0SWQsIHRoaXMudmFsdWVBcmVhLmlkKTtcbiAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UuYXJlYXMpO1xuICAgICAgfSxcbiAgICAgIGVycm9yID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcbiAgICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBmZXRjaEFsbFRhYmxlcyhyZXN0SWQsIGFyZWFfaWQpIHtcbiAgICB0aGlzLnRhYmxlU2VydmljZS5nZXRUYWJsZXMocmVzdElkLCBudWxsLCBudWxsKS50aGVuKFxuICAgICAgcmVzcG9uc2UgPT4ge1xuICAgICAgICB0aGlzLnRhYmxlcyA9IHJlc3BvbnNlLnRhYmxlcztcbiAgICAgICAgdGhpcy50YWJsZUl0ZW1zID0gW107XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnRhYmxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGlmICh0aGlzLnRhYmxlc1tpXS5hcmVhX2lkID09IGFyZWFfaWQpIHtcbiAgICAgICAgICAgIHRoaXMudGFibGVJdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgaWQ6IHRoaXMudGFibGVzW2ldLmlkLFxuICAgICAgICAgICAgICB0ZXh0OiBgJHt0aGlzLnRhYmxlc1tpXS5uYW1lfWBcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNsb3RzID0gMDtcbiAgICAgICAgdGhpcy5mZXRjaFRhYmxlQnlBcmVhKG51bGwpO1xuICAgICAgICB0aGlzLnZhbHVlVGFibGUgPSAnJztcbiAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UuYXJlYXMpO1xuICAgICAgfSxcbiAgICAgIGVycm9yID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcbiAgICAgIH0pO1xuXG4gIH1cblxuICBwdWJsaWMgZmV0Y2hUYWJsZUJ5QXJlYShhcmVhX2lkKSB7XG4gICAgdGhpcy50YWJsZUl0ZW1zID0gW107XG4gICAgaWYgKGFyZWFfaWQgIT0gbnVsbCkge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnRhYmxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAodGhpcy50YWJsZXNbaV0uYXJlYV9pZCA9PSBhcmVhX2lkKSB7XG4gICAgICAgICAgdGhpcy50YWJsZUl0ZW1zLnB1c2goe1xuICAgICAgICAgICAgaWQ6IHRoaXMudGFibGVzW2ldLmlkLFxuICAgICAgICAgICAgdGV4dDogYCR7dGhpcy50YWJsZXNbaV0ubmFtZX1gXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMudGFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHRoaXMudGFibGVJdGVtcy5wdXNoKHtcbiAgICAgICAgICBpZDogdGhpcy50YWJsZXNbaV0uaWQsXG4gICAgICAgICAgdGV4dDogYCR7dGhpcy50YWJsZXNbaV0ubmFtZX1gXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLnNlbGVjdFRhYmxlcy5pdGVtcyA9IHRoaXMudGFibGVJdGVtcztcbiAgICB0aGlzLnZhbHVlVGFibGUgPSB0aGlzLnRhYmxlSXRlbXNbMF07XG4gIH1cblxuXG4gIHB1YmxpYyBzZWxlY3RlZEFyZWFzKHZhbHVlOiBhbnkpOiB2b2lkIHtcblxuICAgIGNvbnNvbGUubG9nKCdTZWxlY3RlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICAgIGxldCBhcmVhSWQgPSBudWxsO1xuICAgIGlmICh0aGlzLnZhbHVlQXJlYS5pZCAhPSB1bmRlZmluZWQgJiYgdGhpcy52YWx1ZUFyZWEuaWQgIT0gbnVsbCAmJiB0aGlzLnZhbHVlQXJlYS5pZCAhPSAwKSB7XG4gICAgICBhcmVhSWQgPSB0aGlzLnZhbHVlQXJlYS5pZDtcbiAgICAgIHRoaXMudmFsdWVUYWJsZSA9ICcnO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZWxlY3RlZFRhYmxlcyh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ1NlbGVjdGVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XG4gICAgbGV0IHRhYmxlSWQgPSBudWxsO1xuICAgIGlmICh0aGlzLnZhbHVlVGFibGUuaWQgIT0gdW5kZWZpbmVkICYmIHRoaXMudmFsdWVUYWJsZS5pZCAhPSBudWxsICYmIHRoaXMudmFsdWVUYWJsZS5pZCAhPSAwKSB7XG4gICAgICB0YWJsZUlkID0gdGhpcy52YWx1ZVRhYmxlLmlkO1xuICAgIH1cbiAgICBsZXQgdGFibGVBcmVhSWQgPSBudWxsO1xuICAgIGxldCBhcmVhSXRlbTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudGFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAodmFsdWUuaWQgPT0gdGhpcy50YWJsZXNbaV0uaWQpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy50YWJsZXNbaV0uaWQpO1xuICAgICAgICB0aGlzLnNsb3RzID0gdGhpcy50YWJsZXNbaV0uc2xvdHM7XG4gICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgdGhpcy5hcmVhSXRlbXMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBpZiAodGhpcy5hcmVhSXRlbXNbal0uaWQgPT0gdGhpcy50YWJsZXNbaV0uYXJlYV9pZCkge1xuICAgICAgICAgICAgYXJlYUl0ZW0gPSB7XG4gICAgICAgICAgICAgIGlkOiB0aGlzLmFyZWFJdGVtc1tqXS5pZCxcbiAgICAgICAgICAgICAgdGV4dDogYCR7dGhpcy5hcmVhSXRlbXNbal0udGV4dH1gXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMudmFsdWVBcmVhID0gYXJlYUl0ZW07XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ1JlbW92ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgICB0aGlzLnNsb3RzID0gMDtcbiAgICB0aGlzLnZhbHVlQXJlYSA9ICcnO1xuICAgIHRoaXMudmFsdWVUYWJsZSA9ICcnO1xuICB9XG5cbiAgcHVibGljIHR5cGVkKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIHJlZnJlc2hBcmVhVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh2YWx1ZSAhPSB1bmRlZmluZWQgJiYgdmFsdWUgIT0gbnVsbCAmJiB2YWx1ZS5sZW5ndGggIT0gMCkge1xuICAgICAgdGhpcy52YWx1ZUFyZWEgPSB2YWx1ZTtcbiAgICAgIGxldCBhcmVhSWQgPSBudWxsO1xuICAgICAgaWYgKHRoaXMudmFsdWVBcmVhLmlkICE9IHVuZGVmaW5lZCAmJiB0aGlzLnZhbHVlQXJlYS5pZCAhPSBudWxsICYmIHRoaXMudmFsdWVBcmVhLmlkICE9IDApIHtcbiAgICAgICAgYXJlYUlkID0gdGhpcy52YWx1ZUFyZWEuaWQ7XG4gICAgICB9XG4gICAgICB0aGlzLmZldGNoVGFibGVCeUFyZWEoYXJlYUlkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5mZXRjaFRhYmxlQnlBcmVhKG51bGwpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyByZWZyZXNoVGFibGVWYWx1ZSh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHZhbHVlICE9IHVuZGVmaW5lZCAmJiB2YWx1ZSAhPSBudWxsICYmIHZhbHVlLmxlbmd0aCAhPSAwKSB7XG4gICAgICB0aGlzLnZhbHVlVGFibGUgPSB2YWx1ZTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgb25DaGFuZ2VPcGVuKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnQ2hhbmdlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICB9XG4gIHB1YmxpYyBvbkNoYW5nZU9wZW5WYWx1ZShldmVudDogYW55KTogdm9pZCB7XG4gICAgbGV0IHZhbHVlMSA9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICBjb25zb2xlLmxvZygnQ2hhbmdlZCB2YWx1ZSBpczogJywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICBpZiAodmFsdWUxID09IFwiXCIgfHwgdmFsdWUxID09IG51bGwpIHtcbiAgICAgIHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0uY29udHJvbHNbJ29wZW5fdGltZSddLnNldFZhbHVlKG51bGwpO1xuICAgICAgdGhpcy5vcGVuX3RpbWUgPSB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydvcGVuX3RpbWUnXTtcbiAgICB9XG4gICAgY29uc3QgbmFtZVJlID0gbmV3IFJlZ0V4cCgvXlswLTldKyQvaSk7XG4gICAgaWYgKCFuYW1lUmUudGVzdCh2YWx1ZTEpIHx8IHZhbHVlMS5sZW5ndGggPiAyKSB7XG4gICAgICB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydvcGVuX3RpbWUnXS5zZXRWYWx1ZShudWxsKTtcbiAgICAgIHRoaXMub3Blbl90aW1lID0gdGhpcy5jcmVhdGVSZXNlcnZlRm9ybS5jb250cm9sc1snb3Blbl90aW1lJ107XG4gICAgfVxuICB9XG4gIHB1YmxpYyBvbkNoYW5nZUNsb3NlVmFsdWUoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCB2YWx1ZTEgPSBldmVudC50YXJnZXQudmFsdWU7XG4gICAgY29uc29sZS5sb2coJ0NoYW5nZWQgdmFsdWUgaXM6ICcsIGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgaWYgKHZhbHVlMSA9PSBcIlwiIHx8IHZhbHVlMSA9PSBudWxsKSB7XG4gICAgICB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydjbG9zZV90aW1lJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgICB0aGlzLmNsb3NlX3RpbWUgPSB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydjbG9zZV90aW1lJ107XG4gICAgfVxuICAgIGNvbnN0IG5hbWVSZSA9IG5ldyBSZWdFeHAoL15bMC05XSskL2kpO1xuICAgIGlmICghbmFtZVJlLnRlc3QodmFsdWUxKSB8fCB2YWx1ZTEubGVuZ3RoID4gMikge1xuICAgICAgdGhpcy5jcmVhdGVSZXNlcnZlRm9ybS5jb250cm9sc1snY2xvc2VfdGltZSddLnNldFZhbHVlKG51bGwpO1xuICAgICAgdGhpcy5jbG9zZV90aW1lID0gdGhpcy5jcmVhdGVSZXNlcnZlRm9ybS5jb250cm9sc1snY2xvc2VfdGltZSddO1xuICAgIH1cblxuICB9XG4gIHB1YmxpYyBvbkNoYW5nZUNsb3NlKHZhbHVlMTogYW55KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ0NoYW5nZWQgdmFsdWUgaXM6ICcsIHZhbHVlMSk7XG4gIH1cblxuICBwdWJsaWMgb25TZWxlY3RGcm9tRGF0ZSh2YWx1ZTogYW55KTogdm9pZCB7XG5cbiAgICBjb25zb2xlLmxvZygnTmV3IHNlYXJjaCBpbnB1dDogJywgdmFsdWUpO1xuICB9XG5cbiAgcHVibGljIGNoZWNrKCkge1xuICAgIGNvbnNvbGUubG9nKHRoaXMuY2xvc2VfdGltZS52YWx1ZSk7XG4gICAgY29uc29sZS5sb2codGhpcy5vcGVuX3RpbWUudmFsdWUpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0ZS52YWx1ZSk7XG5cblxuICB9XG5cbiAgcHVibGljIGNob29zZVNwYWNlVGltZSgpIHtcbiAgICBhbGVydCgnY2xpY2snKTtcbiAgfVxuXG4gIHByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xuICAgIHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcblxuICAgICAgJ29wZW5fdGltZSc6IFsnJywgW1xuICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkXG4gICAgICBdXSxcbiAgICAgICdjbG9zZV90aW1lJzogWycnLCBbXG4gICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWRcbiAgICAgIF1dLFxuICAgICAgJ2RhdGUnOiBbJycsIFtcbiAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZFxuICAgICAgXV0sXG4gICAgICAnbmFtZSc6IFsnJywgW1xuICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTYpLFxuICAgICAgXV0sXG4gICAgICAncGhvbmUnOiBbJycsIFtcbiAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZCxcbiAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzIpLFxuICAgICAgICBpbnZhbGlkTnVtYmVyVmFsaWRhdG9yKClcblxuICAgICAgXV0sXG4gICAgICAnZW1haWwnOiBbJycsIFtcbiAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgICAgaW52YWxpZEVtYWlsVmFsaWRhdG9yKClcbiAgICAgIF1dLFxuICAgICAgJ2FkZHJlc3MnOiBbJycsIFtcbiAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgIF1dLFxuICAgICAgJ2NvbW1lbnQnOiBbJycsIFtcbiAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgIF1dLFxuICAgICAgJ3Blb3BsZV9jb3VudCc6IFsnJywgW1xuICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCg5OSksXG4gICAgICAgIGludmFsaWROdW1iZXJWYWxpZGF0b3IoKVxuICAgICAgXV0sXG4gICAgfSk7XG4gICAgbGV0IG5nID0gbmV3IERhdGUoKTtcbiAgICBsZXQgbmcxID0gbmV3IERhdGUoKTtcblxuICAgIHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0uY29udHJvbHNbJ29wZW5fdGltZSddLnNldFZhbHVlKG5nKTtcbiAgICB0aGlzLm9wZW5fdGltZSA9IHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0uY29udHJvbHNbJ29wZW5fdGltZSddO1xuICAgIHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0uY29udHJvbHNbJ2Nsb3NlX3RpbWUnXS5zZXRWYWx1ZShuZzEpO1xuICAgIHRoaXMuY2xvc2VfdGltZSA9IHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0uY29udHJvbHNbJ2Nsb3NlX3RpbWUnXTtcbiAgICB0aGlzLnBob25lID0gdGhpcy5jcmVhdGVSZXNlcnZlRm9ybS5jb250cm9sc1sncGhvbmUnXTtcbiAgICB0aGlzLmVtYWlsID0gdGhpcy5jcmVhdGVSZXNlcnZlRm9ybS5jb250cm9sc1snZW1haWwnXTtcbiAgICB0aGlzLmFkZHJlc3MgPSB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydhZGRyZXNzJ107XG4gICAgdGhpcy5wZW9wbGVfY291bnQgPSB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydwZW9wbGVfY291bnQnXTtcbiAgICB0aGlzLmRhdGUgPSB0aGlzLmNyZWF0ZVJlc2VydmVGb3JtLmNvbnRyb2xzWydkYXRlJ107XG4gICAgdGhpcy5uYW1lID0gdGhpcy5jcmVhdGVSZXNlcnZlRm9ybS5jb250cm9sc1snbmFtZSddO1xuICAgIHRoaXMuY29tbWVudCA9IHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0uY29udHJvbHNbJ2NvbW1lbnQnXTtcblxuICAgIHRoaXMuY3JlYXRlUmVzZXJ2ZUZvcm0udmFsdWVDaGFuZ2VzXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5vblZhbHVlQ2hhbmdlZChkYXRhKSk7XG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCgpOyAvLyAocmUpc2V0IHZhbGlkYXRpb24gbWVzc2FnZXMgbm93XG5cbiAgfVxuXG4gIHByaXZhdGUgb25WYWx1ZUNoYW5nZWQoZGF0YT86IGFueSkge1xuICAgIGlmICghdGhpcy5jcmVhdGVSZXNlcnZlRm9ybSkgeyByZXR1cm47IH1cbiAgICBjb25zdCBmb3JtID0gdGhpcy5jcmVhdGVSZXNlcnZlRm9ybTtcblxuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuXG4gICAgICBpZiAoY29udHJvbCAmJiBjb250cm9sLmRpcnR5ICYmICFjb250cm9sLnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZvcm1FcnJvcnMgPSB7XG4gICAgJ25hbWUnOiBbXSxcbiAgXHQncGhvbmUnOiBbXSxcbiAgICAnZW1haWwnOiBbXSxcbiAgICAnZGF0ZSc6IFtdLFxuICAgICdvcGVuX3RpbWUnOiBbXSxcbiAgICAnY2xvc2VfdGltZSc6IFtdLFxuICAgICdyZXNlcnZlX2RhdGUnOiBbXSxcbiAgICAnYWRkcmVzcyc6IFtdLFxuICAgICdwZW9wbGVfY291bnQnOiBbXSxcbiAgICAnY29tbWVudCc6IFtdXG4gIH07XG5cbiAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICduYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBuYW1lIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBuYW1lIG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICAgICdlbWFpbCc6IHtcbiAgICAgICdtaW5sZW5ndGgnOiAnVGhlIGVtYWlsIG11c3QgYmUgYXQgbGVhc3QgMjU2IGNoYXJhY3RlcnMgbG9uZy4nLFxuICAgICAgJ2ludmFsaWRFbWFpbCc6ICdUaGUgZW1haWwgbXVzdCBiZSBhIHZhbGlkIGVtYWlsIGFkZHJlc3MuJyxcbiAgICB9LFxuICAgICdwaG9uZSc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICdUaGUgcGhvbmUgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHBob25lIG11c3QgYmUgYXQgbGVzcyAzMiBjaGFyYWN0ZXJzIGxvbmcuJyxcbiAgICAgICdpbnZhbGlkTnVtYmVyJzogJ1RoZSBwaG9uZSBtdXN0IGJlIG51bWJlcidcbiAgICB9LFxuICAgICdhZGRyZXNzJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBhZGRyZXNzIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBhZGRyZXNzIG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICAgICdkYXRlJzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBkYXRlIGlzIHJlcXVpcmVkLidcbiAgICB9LFxuICAgICdvcGVuX3RpbWUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIG9wZW4gdGltZSBpcyByZXF1aXJlZC4nXG4gICAgfSxcbiAgICAnY2xvc2VfdGltZSc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICdUaGUgY2xvc2UgdGltZSBpcyByZXF1aXJlZC4nXG4gICAgfSxcbiAgICAncGVvcGxlX2NvdW50Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSB0aGUgbnVtYmVyIG9mIHBlb3BsZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgbnVtYmVyIG9mIHBlb3BsZSBtdXN0IGJlIGF0IGxlc3MgMiBjaGFyYWN0ZXJzIGxvbmcuJyxcbiAgICAgICdpbnZhbGlkTnVtYmVyJzogJ1RoZSBwaG9uZSBtdXN0IGJlIG51bWJlcidcbiAgICB9LFxuICAgICdjb21tZW50Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBjb21tZW50IGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSBjb21tZW50IG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICB9O1xuXG4gIHByaXZhdGUgYWRkVG9SZXNlcnZlKCkge1xuICAgIGlmICh0aGlzLnZhbHVlQXJlYSA9PSBudWxsIHx8IHRoaXMudmFsdWVUYWJsZSA9PSBudWxsIHx8IHRoaXMuc2xvdHMgPT0gMFxuICAgICAgfHwgdGhpcy52YWx1ZUFyZWEgPT0gJycgfHwgdGhpcy52YWx1ZVRhYmxlID09ICcnIHx8IHRoaXMuc2xvdHMgPT0gJycpIHtcblxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGZvciAodmFyIHYgb2YgdGhpcy50YWJsZXMpIHtcbiAgICAgICAgaWYgKHYuaWQgPT0gdGhpcy52YWx1ZVRhYmxlLmlkKSB7XG4gICAgICAgICAgdGhpcy50YWJsZXMuc3BsaWNlKHRoaXMudGFibGVzLmluZGV4T2YodiksIDEpO1xuICAgICAgICAgIHRoaXMudGFibGVzUmVtb3ZlZC5wdXNoKHYpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBsZXQgaXRlbVJlc2VydmUgPVxuICAgICAgICB7XG4gICAgICAgICAgYXJlYV9pZDogdGhpcy52YWx1ZUFyZWEuaWQsXG4gICAgICAgICAgdGV4dEFyZWE6IHRoaXMudmFsdWVBcmVhLnRleHQsXG4gICAgICAgICAgaWQ6IHRoaXMudmFsdWVUYWJsZS5pZCxcbiAgICAgICAgICBuYW1lOiB0aGlzLnZhbHVlVGFibGUudGV4dCxcbiAgICAgICAgICBzbG90czogdGhpcy5zbG90c1xuICAgICAgICB9XG5cbiAgICAgIHRoaXMudGFibGVSZXNlcnZlLnVuc2hpZnQoaXRlbVJlc2VydmUpO1xuICAgICAgdGhpcy52YWx1ZUFyZWEgPSAnJztcbiAgICAgIHRoaXMuc2xvdHMgPSAwO1xuICAgICAgdGhpcy5mZXRjaFRhYmxlQnlBcmVhKG51bGwpO1xuICAgICAgdGhpcy52YWx1ZVRhYmxlID0gJyc7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSByZW1vdmVUYWJsZShpdGVtKSB7XG4gICAgY29uc29sZS5sb2coaXRlbSk7XG4gICAgdGhpcy50YWJsZVJlc2VydmUuc3BsaWNlKHRoaXMudGFibGVSZXNlcnZlLmluZGV4T2YoaXRlbSksIDEpO1xuICAgIHRoaXMudGFibGVzLnVuc2hpZnQoaXRlbSk7XG4gICAgdGhpcy5mZXRjaFRhYmxlQnlBcmVhKG51bGwpO1xuICAgIHRoaXMudmFsdWVBcmVhID0gJyc7XG4gICAgdGhpcy52YWx1ZVRhYmxlID0gJyc7XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZVJlc2VydmUoKSB7XG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCgpO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICBpZiAodGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5sZW5ndGggPiAwKSB7XG4gICAgICAgIHN3YWwoXCJQbGVhc2UgY2hlY2sgYWxsIHRoZSBpbmZvcm1hdGlvbiBhZ2FpbiFcIixcIlwiLFwiZXJyb3JcIik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLm9wZW5fdGltZS52YWx1ZSlcbiAgICAgIHtcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JzLm9wZW5fdGltZS5wdXNoKFwiVGhlIG9wZW4gdGltZSBpcyByZXF1aXJlZC5cIik7XG4gICAgICB9XG4gICAgICBpZiAoIXRoaXMuY2xvc2VfdGltZS52YWx1ZSlcbiAgICAgIHtcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JzLmNsb3NlX3RpbWUucHVzaChcIlRoZSBjbG9zZSB0aW1lIGlzIHJlcXVpcmVkLlwiKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLm5hbWUudmFsdWU9PVwiXCIpXG4gICAgICB7XG4gICAgICAgIHRoaXMuZm9ybUVycm9ycy5uYW1lLnB1c2goXCJUaGUgbmFtZSBpcyByZXF1aXJlZC5cIik7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5waG9uZS52YWx1ZT09XCJcIilcbiAgICAgIHtcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JzLnBob25lLnB1c2goXCJUaGUgcGhvbmUgaXMgcmVxdWlyZWQuXCIpO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMucGVvcGxlX2NvdW50LnZhbHVlPT1cIlwiKVxuICAgICAge1xuICAgICAgICB0aGlzLmZvcm1FcnJvcnMucGVvcGxlX2NvdW50LnB1c2goXCJUaGUgbnVtYmVyIG9mIHBlcnNvbiBpcyByZXF1aXJlZC5cIik7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5kYXRlLnZhbHVlPT1cIlwiKVxuICAgICAge1xuICAgICAgICB0aGlzLmZvcm1FcnJvcnMuZGF0ZS5wdXNoKFwiVGhlIGRhdGUgaXMgcmVxdWlyZWQuXCIpO1xuICAgICAgfVxuXG4gICAgICBpZiAoIXRoaXMub3Blbl90aW1lLnZhbHVlIHx8ICF0aGlzLmNsb3NlX3RpbWUudmFsdWVcbiAgICAgICAgfHwgdGhpcy5uYW1lLnZhbHVlPT1cIlwiIHx8IHRoaXMucGhvbmUudmFsdWU9PVwiXCJcbiAgICAgICAgfHwgdGhpcy5wZW9wbGVfY291bnQudmFsdWU9PVwiXCJcbiAgICAgICAgfHwgdGhpcy5kYXRlLnZhbHVlPT1cIlwiXG4gICAgICApIHtcbiAgICAgICAgc3dhbChcIlBsZWFzZSBmaWxsIGFsbCB0aGUgYmxhbmshXCIsXCJcIixcImVycm9yXCIpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cblxuICAgIH1cbiAgICBjb25zb2xlLmxvZyhcIm5hbWU6IFwiICsgdGhpcy5uYW1lLnZhbHVlKTtcbiAgICBjb25zb2xlLmxvZyhcInBob25lOiBcIiArIHRoaXMucGhvbmUudmFsdWUpO1xuICAgIGNvbnNvbGUubG9nKFwiZW1haWw6IFwiICsgdGhpcy5lbWFpbC52YWx1ZSk7XG4gICAgY29uc29sZS5sb2coXCJhZGRyZXNzOiBcIiArIHRoaXMuYWRkcmVzcy52YWx1ZSk7XG4gICAgY29uc29sZS5sb2coXCJwZW9wbGVfY291bnQ6IFwiICsgdGhpcy5wZW9wbGVfY291bnQudmFsdWUpO1xuICAgIGNvbnNvbGUubG9nKFwiZGF0ZTogXCIgKyB0aGlzLmRhdGUudmFsdWUpO1xuICAgIGNvbnNvbGUubG9nKFwiY29tbWVudDogXCIgKyB0aGlzLmNvbW1lbnQudmFsdWUpO1xuICAgIGNvbnNvbGUubG9nKFwib3Blbl90aW1lOiBcIiArIHRoaXMub3Blbl90aW1lLnZhbHVlLnRvTG9jYWxlVGltZVN0cmluZyhbXSwgeyBob3VyMTI6IGZhbHNlIH0pKTtcbiAgICBjb25zb2xlLmxvZyhcImNsb3NlX3RpbWU6IFwiICsgdGhpcy5jbG9zZV90aW1lLnZhbHVlLnRvTG9jYWxlVGltZVN0cmluZyhbXSwgeyBob3VyMTI6IGZhbHNlIH0pKTtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnRhYmxlUmVzZXJ2ZSk7XG4gICAgdmFyIHN1bSA9IDA7XG4gICAgdmFyIHBhcmFtID0ge1xuICAgICAgJ3VzZXInOntcbiAgICAgICAgJ25hbWUnOiB0aGlzLm5hbWUudmFsdWUsXG4gICAgICAgICdwaG9uZSc6IHRoaXMucGhvbmUudmFsdWUsXG4gICAgICAgICdlbWFpbCc6IHRoaXMuZW1haWwudmFsdWUsXG4gICAgICAgICdhZGRyZXNzJzogdGhpcy5hZGRyZXNzLnZhbHVlLFxuICAgICAgICAncGVvcGxlX2NvdW50JzogdGhpcy5wZW9wbGVfY291bnQudmFsdWUsXG4gICAgICAgICdkYXRlJzogdGhpcy5kYXRlLnZhbHVlLFxuICAgICAgICAnY29tbWVudCc6IHRoaXMuY29tbWVudC52YWx1ZSxcbiAgICAgICAgJ29wZW5fdGltZSc6IHRoaXMub3Blbl90aW1lLnZhbHVlLnRvTG9jYWxlVGltZVN0cmluZyhbXSwgeyBob3VyMTI6IGZhbHNlIH0pLFxuICAgICAgICAnY2xvc2VfdGltZSc6IHRoaXMuY2xvc2VfdGltZS52YWx1ZS50b0xvY2FsZVRpbWVTdHJpbmcoW10sIHsgaG91cjEyOiBmYWxzZSB9KSxcbiAgICAgIH0sXG4gICAgICAndGFibGUnOiB0aGlzLnRhYmxlUmVzZXJ2ZVxuICAgIH1cbiAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgZm9yICh2YXIgdiBvZiB0aGlzLnRhYmxlUmVzZXJ2ZSkge1xuICAgICAgc3VtICs9IE51bWJlcih2LnNsb3RzKTtcbiAgICB9XG4gICAgaWYoc3VtPCBOdW1iZXIodGhpcy5wZW9wbGVfY291bnQudmFsdWUpKVxuICAgIHtcbiAgICAgIHN3YWwoe1xuICAgICAgICB0aXRsZTogJ0LhuqFuIGPDsyBjaOG6r2Mga2jDtG5nID8nLFxuICAgICAgICB0ZXh0OiBcIlPhu5EgbmfGsOG7nWkgdGhhbSBk4buxIG5oaeG7gXUgaMahbiBz4buRIGLDoG4gxJHhurd0IVwiLFxuICAgICAgICB0eXBlOiAnd2FybmluZycsXG4gICAgICAgIHNob3dDYW5jZWxCdXR0b246IHRydWUsXG4gICAgICAgIGNvbmZpcm1CdXR0b25Db2xvcjogJyMzMDg1ZDYnLFxuICAgICAgICBjYW5jZWxCdXR0b25Db2xvcjogJyNkMzMnLFxuICAgICAgICBjYW5jZWxCdXR0b25UZXh0OidLaMO0bmcnLFxuICAgICAgICBjb25maXJtQnV0dG9uVGV4dDogJ0No4bqlcCBuaOG6rW4hJ1xuICAgICAgfSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHN3YWwoXG4gICAgICAgICAgJ1Row6BuaCBjw7RuZyAhJyxcbiAgICAgICAgICAnQsOgbiBj4bunYSBi4bqhbiDEkcOjIMSRxrDhu6NjIMSR4bq3dCB0aMOgbmggY8O0bmcuJyxcbiAgICAgICAgICAnc3VjY2VzcydcbiAgICAgICAgKTtcbiAgICAgICAgdGhhdC5yZXNlcnZlU2VydmljZS5jcmVhdGVSZXNlcnZlKHBhcmFtKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgZ29CYWNrKCk6IHZvaWQge1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL3RhYmxlLWJvb2tpbmcnXSwgeyByZWxhdGl2ZVRvOiB0aGlzLnJvdXRlIH0pO1xuICB9XG5cbn1cbiJdfQ==
