"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var contact_service_1 = require("app/services/contact.service");
var ContactListComponent = (function () {
    function ContactListComponent(contactServ, userServ, storageServ) {
        this.contactServ = contactServ;
        this.userServ = userServ;
        this.storageServ = storageServ;
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    ContactListComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.doSearch();
    };
    ContactListComponent.prototype.doSearch = function () {
        var _this = this;
        var params = {
            'rest_id': this.restId
        };
        this.contactServ.getContactList(params).then(function (response) {
            _this.contactList = response.contacts;
            _this.totalItems = _this.contactList.length;
            _this.pageChanged({ page: _this.currentPage, itemsPerPage: _this.itemsPerPage });
        }, function (error) {
            console.log(error);
        });
    };
    ContactListComponent.prototype.pageChanged = function (event) {
        var start = (event.page - 1) * event.itemsPerPage;
        var end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.contactList.length;
        this.data = this.contactList.slice(start, end);
    };
    ContactListComponent.prototype.doDeleteContact = function (obj) {
        var _this = this;
        obj.is_delete = 1;
        var params = {
            'user_id': this.userId,
            'rest_id': this.restId,
            'contact': obj
        };
        this.contactServ.updateContact(params).then(function (response) {
            _this.doSearch();
        }, function (error) {
            console.log(error);
        });
    };
    ContactListComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    ContactListComponent = __decorate([
        core_1.Component({
            selector: 'contact-list',
            templateUrl: utils_1.default.getView('app/components/contact-list/contact-list.component.html'),
            styleUrls: [utils_1.default.getView('app/components/contact-list/contact-list.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [contact_service_1.ContactService,
            user_service_1.UserService,
            storage_service_1.StorageService])
    ], ContactListComponent);
    return ContactListComponent;
}());
exports.ContactListComponent = ContactListComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL2NvbnRhY3QtbGlzdC9jb250YWN0LWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHlDQUFvQztBQUVwQyw2REFBdUU7QUFDdkUsMERBQXdEO0FBQ3hELGdFQUE4RDtBQUU5RCxnRUFBOEQ7QUFTOUQ7SUFXRSw4QkFDVSxXQUEyQixFQUMzQixRQUFxQixFQUNyQixXQUEyQjtRQUYzQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDM0IsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFSN0IsZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFFeEIsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFXLEVBQUUsQ0FBQztJQU16QixDQUFDO0lBRUwsdUNBQVEsR0FBUjtRQUVFLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFDLENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVPLHVDQUFRLEdBQWhCO1FBQUEsaUJBWUM7UUFWQyxJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN2QixDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUNuRCxLQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDckMsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUMxQyxLQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQ2hGLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLDBDQUFXLEdBQWxCLFVBQW1CLEtBQVU7UUFDM0IsSUFBSSxLQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7UUFDbEQsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7UUFDM0YsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVPLDhDQUFlLEdBQXZCLFVBQXdCLEdBQUc7UUFBM0IsaUJBWUM7UUFYQyxHQUFHLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUNsQixJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtZQUN0QixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLEdBQUc7U0FDZixDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUNsRCxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8seUNBQVUsR0FBbEIsVUFBbUIsR0FBRztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBbEVVLG9CQUFvQjtRQVBoQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGNBQWM7WUFDeEIsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMseURBQXlELENBQUM7WUFDckYsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyx3REFBd0QsQ0FBQyxDQUFDO1lBQ3BGLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUUscUJBQXFCLEVBQUUsRUFBRSxFQUFFO1NBQ3BDLENBQUM7eUNBYXVCLGdDQUFjO1lBQ2pCLDBCQUFXO1lBQ1IsZ0NBQWM7T0FkMUIsb0JBQW9CLENBbUVoQztJQUFELDJCQUFDO0NBbkVELEFBbUVDLElBQUE7QUFuRVksb0RBQW9CIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL2NvbnRhY3QtbGlzdC9jb250YWN0LWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IENvbnRhY3RTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2NvbnRhY3Quc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NvbnRhY3QtbGlzdCcsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9jb250YWN0LWxpc3QvY29udGFjdC1saXN0LmNvbXBvbmVudC5odG1sJyksXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL2NvbnRhY3QtbGlzdC9jb250YWN0LWxpc3QuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsgJ1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJyB9XG59KVxuZXhwb3J0IGNsYXNzIENvbnRhY3RMaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgY29udGFjdExpc3Q6IGFueTtcbiAgcHJpdmF0ZSBkYXRhOiBhbnk7XG4gIHByaXZhdGUgY3VycmVudFBhZ2U6IG51bWJlciA9IDE7XG4gIHByaXZhdGUgdG90YWxJdGVtczogbnVtYmVyO1xuICBwcml2YXRlIGl0ZW1zUGVyUGFnZTogbnVtYmVyID0gMTA7XG4gIHByaXZhdGUgbWF4U2l6ZTogbnVtYmVyID0gMTA7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb250YWN0U2VydjogQ29udGFjdFNlcnZpY2UsXG4gICAgcHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSBzdG9yYWdlU2VydjogU3RvcmFnZVNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcblxuICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgaWYgKCF1c2VyS2V5KSB7XG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVzZXJJZCA9IHVzZXJLZXkudXNlcl9pbmZvLmlkO1xuICAgICAgdGhpcy5yZXN0SWQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIH1cblxuICAgIHRoaXMuZG9TZWFyY2goKTtcbiAgfVxuXG4gIHByaXZhdGUgZG9TZWFyY2goKSB7XG5cbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZFxuICAgIH07XG4gICAgdGhpcy5jb250YWN0U2Vydi5nZXRDb250YWN0TGlzdChwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgdGhpcy5jb250YWN0TGlzdCA9IHJlc3BvbnNlLmNvbnRhY3RzO1xuICAgICAgdGhpcy50b3RhbEl0ZW1zID0gdGhpcy5jb250YWN0TGlzdC5sZW5ndGg7XG4gICAgICB0aGlzLnBhZ2VDaGFuZ2VkKHsgcGFnZTogdGhpcy5jdXJyZW50UGFnZSwgaXRlbXNQZXJQYWdlOiB0aGlzLml0ZW1zUGVyUGFnZSB9KTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgcGFnZUNoYW5nZWQoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCBzdGFydCA9IChldmVudC5wYWdlIC0gMSkgKiBldmVudC5pdGVtc1BlclBhZ2U7XG4gICAgbGV0IGVuZCA9IGV2ZW50Lml0ZW1zUGVyUGFnZSA+IC0xID8gKHN0YXJ0ICsgZXZlbnQuaXRlbXNQZXJQYWdlKSA6IHRoaXMuY29udGFjdExpc3QubGVuZ3RoO1xuICAgIHRoaXMuZGF0YSA9IHRoaXMuY29udGFjdExpc3Quc2xpY2Uoc3RhcnQsIGVuZCk7XG4gIH1cblxuICBwcml2YXRlIGRvRGVsZXRlQ29udGFjdChvYmopIHtcbiAgICBvYmouaXNfZGVsZXRlID0gMTtcbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgJ3VzZXJfaWQnOiB0aGlzLnVzZXJJZCxcbiAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAnY29udGFjdCc6IG9ialxuICAgIH07XG4gICAgdGhpcy5jb250YWN0U2Vydi51cGRhdGVDb250YWN0KHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICB0aGlzLmRvU2VhcmNoKCk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb1NldFNjb3BlKG9iaikge1xuICAgIHRoaXMuc3RvcmFnZVNlcnYuc2V0U2NvcGUob2JqKTtcbiAgfVxufVxuIl19
