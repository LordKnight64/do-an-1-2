"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("app/services/user.service");
var router_animations_1 = require("../../router.animations");
var utils_1 = require("app/utils/utils");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var subscribe_service_1 = require("app/services/subscribe.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var SubscribeListModalComponent = (function () {
    function SubscribeListModalComponent(subscribeServ, userServ, storageServ, notifyServ) {
        this.subscribeServ = subscribeServ;
        this.userServ = userServ;
        this.storageServ = storageServ;
        this.notifyServ = notifyServ;
        this.newsList = [];
        this.data = [];
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.maxSize = 10;
    }
    SubscribeListModalComponent.prototype.ngOnInit = function () {
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        this.newsList = this.data = [];
        this.stringmail = "";
        this.doSearch();
    };
    SubscribeListModalComponent.prototype.show = function (sendMailCmp) {
        this.sendMailCmp = sendMailCmp;
        this.childModal.show();
    };
    SubscribeListModalComponent.prototype.hide = function () {
        this.childModal.hide();
    };
    SubscribeListModalComponent.prototype.doSearch = function () {
        console.log(this.newsList);
        this.totalItems = this.newsList.length;
        this.pageChanged({ page: this.currentPage, itemsPerPage: this.itemsPerPage });
    };
    SubscribeListModalComponent.prototype.pageChanged = function (event) {
        var start = (event.page - 1) * event.itemsPerPage;
        var end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
        this.data = this.newsList.slice(start, end);
    };
    SubscribeListModalComponent.prototype.doSetScope = function (obj) {
        this.storageServ.setScope(obj);
    };
    SubscribeListModalComponent.prototype.addEmail = function (obj) {
        var mailAddresses = {
            'sendTo': $("#sendto").val(),
            'sendCC': $("#sendcc").val(),
            'sendBCC': $("#sendbcc").val(),
            'subject': $("#subject").val(),
            'content': $("#content").val()
        };
        //alert("OK");
        var email = obj.email;
        console.log(this.stringmail);
        obj.added = true;
        if (this.choose == 1) {
            this.stringmail = $("#sendto").val() + email + ";";
            mailAddresses.sendTo = this.stringmail;
        }
        if (this.choose == 2) {
            this.stringmail = $("#sendcc").val() + email + ";";
            mailAddresses.sendCC = this.stringmail;
        }
        if (this.choose == 3) {
            this.stringmail = $("#sendbcc").val() + email + ";";
            mailAddresses.sendBCC = this.stringmail;
        }
        this.sendMailCmp.setVar(mailAddresses);
    };
    __decorate([
        core_1.ViewChild('childModal'),
        __metadata("design:type", ngx_bootstrap_1.ModalDirective)
    ], SubscribeListModalComponent.prototype, "childModal", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], SubscribeListModalComponent.prototype, "title", void 0);
    SubscribeListModalComponent = __decorate([
        core_1.Component({
            selector: 'subscribe-list-modal',
            styleUrls: [utils_1.default.getView('app/components/subscribe-list-modal/subscribe-list-modal.component.css')],
            templateUrl: utils_1.default.getView('app/components/subscribe-list-modal/subscribe-list-modal.component.html'),
            animations: [router_animations_1.routerGrow()],
            host: { '[@routerGrow]': '' }
        }),
        __metadata("design:paramtypes", [subscribe_service_1.SubscribeService,
            user_service_1.UserService,
            storage_service_1.StorageService,
            notification_service_1.NotificationService])
    ], SubscribeListModalComponent);
    return SubscribeListModalComponent;
}());
exports.SubscribeListModalComponent = SubscribeListModalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3N1YnNjcmliZS1saXN0LW1vZGFsL3N1YnNjcmliZS1saXN0LW1vZGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUErRTtBQUcvRSwwREFBd0Q7QUFFeEQsNkRBQXFEO0FBR3JELHlDQUFvQztBQVFwQywrQ0FBNEQ7QUFFNUQsb0VBQWtFO0FBQ2xFLGdFQUE4RDtBQUM5RCwwRUFBd0U7QUFXeEU7SUFlRSxxQ0FDVSxhQUErQixFQUMvQixRQUFxQixFQUNyQixXQUEyQixFQUMzQixVQUErQjtRQUgvQixrQkFBYSxHQUFiLGFBQWEsQ0FBa0I7UUFDL0IsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDM0IsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFabEMsYUFBUSxHQUFRLEVBQUUsQ0FBQztRQUNsQixTQUFJLEdBQVEsRUFBRSxDQUFDO1FBRWYsZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFFeEIsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFXLEVBQUUsQ0FBQztJQVF6QixDQUFDO0lBR0UsOENBQVEsR0FBZjtRQUNFLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCwwQ0FBSSxHQUFKLFVBQUssV0FBVztRQUNkLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUNELDBDQUFJLEdBQUo7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFTyw4Q0FBUSxHQUFoQjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUNsRixDQUFDO0lBRU0saURBQVcsR0FBbEIsVUFBbUIsS0FBVTtRQUMzQixJQUFJLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQztRQUNsRCxJQUFJLEdBQUcsR0FBRyxLQUFLLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUN4RixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU8sZ0RBQVUsR0FBbEIsVUFBbUIsR0FBRztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRU8sOENBQVEsR0FBaEIsVUFBaUIsR0FBRztRQUVsQixJQUFJLGFBQWEsR0FBRztZQUNsQixRQUFRLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRTtZQUM1QixRQUFRLEVBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRTtZQUM5QixTQUFTLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsRUFBRTtZQUM5QixTQUFTLEVBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsRUFBRTtZQUM3QixTQUFTLEVBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsRUFBRTtTQUM5QixDQUFBO1FBQ0QsY0FBYztRQUNkLElBQUksS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDN0IsR0FBRyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FDcEIsQ0FBQztZQUNDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEtBQUssR0FBRSxHQUFHLENBQUM7WUFDbEQsYUFBYSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pDLENBQUM7UUFDRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUNwQixDQUFDO1lBQ0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsS0FBSyxHQUFFLEdBQUcsQ0FBQztZQUNqRCxhQUFhLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDMUMsQ0FBQztRQUNELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQ3BCLENBQUM7WUFDQyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxLQUFLLEdBQUUsR0FBRyxDQUFDO1lBQ2xELGFBQWEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQyxDQUFDO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQXRGd0I7UUFBeEIsZ0JBQVMsQ0FBQyxZQUFZLENBQUM7a0NBQW1CLDhCQUFjO21FQUFDO0lBQ2pEO1FBQVIsWUFBSyxFQUFFOzs4REFBZTtJQUhaLDJCQUEyQjtRQVJ2QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHdFQUF3RSxDQUFDLENBQUM7WUFDcEcsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMseUVBQXlFLENBQUM7WUFDckcsVUFBVSxFQUFFLENBQUMsOEJBQVUsRUFBRSxDQUFDO1lBQzFCLElBQUksRUFBRSxFQUFDLGVBQWUsRUFBRSxFQUFFLEVBQUM7U0FFNUIsQ0FBQzt5Q0FpQnlCLG9DQUFnQjtZQUNyQiwwQkFBVztZQUNSLGdDQUFjO1lBQ2YsMENBQW1CO09BbkI5QiwyQkFBMkIsQ0EyRnZDO0lBQUQsa0NBQUM7Q0EzRkQsQUEyRkMsSUFBQTtBQTNGWSxrRUFBMkIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3QtbW9kYWwvc3Vic2NyaWJlLWxpc3QtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgVmlld0NoaWxkLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0gLEFic3RyYWN0Q29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnYXBwL21vZGVscy91c2VyJztcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9zZXJ2ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IHJvdXRlckdyb3cgfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XHJcblxyXG5cclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcbmltcG9ydCBtb21lbnQgPSByZXF1aXJlKCdtb21lbnQnKVxyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IGludmFsaWRFbWFpbFZhbGlkYXRvciB9IGZyb20gXCJhcHAvdXRpbHMvZW1haWwtdmFsaWRhdG9yLmRpcmVjdGl2ZVwiO1xyXG5pbXBvcnQgeyBkZWZhdWx0IGFzIHN3YWwgfSBmcm9tICdzd2VldGFsZXJ0Mic7XHJcbmltcG9ydCB7IFNlbGVjdENvbXBvbmVudCB9IGZyb20gJ25nMi1zZWxlY3QnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnbmcyLXRyYW5zbGF0ZSc7XHJcbmltcG9ydCB7IEVOVklST05NRU5UIH0gZnJvbSAnZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcclxuaW1wb3J0IHsgTW9kYWxNb2R1bGUgLCBNb2RhbERpcmVjdGl2ZX0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFN1YnNjcmliZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc3Vic2NyaWJlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZW5kRW1haWxNYXJrZXRpbmdDb21wb25lbnQgfSBmcm9tICdhcHAvY29tcG9uZW50cy9zZW5kLWVtYWlsLW1hcmtldGluZy9zZW5kLWVtYWlsLW1hcmtldGluZy5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdzdWJzY3JpYmUtbGlzdC1tb2RhbCcsXHJcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3QtbW9kYWwvc3Vic2NyaWJlLWxpc3QtbW9kYWwuY29tcG9uZW50LmNzcycpXSxcclxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3QtbW9kYWwvc3Vic2NyaWJlLWxpc3QtbW9kYWwuY29tcG9uZW50Lmh0bWwnKSxcclxuICBhbmltYXRpb25zOiBbcm91dGVyR3JvdygpXSxcclxuICBob3N0OiB7J1tAcm91dGVyR3Jvd10nOiAnJ31cclxuXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdWJzY3JpYmVMaXN0TW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBAVmlld0NoaWxkKCdjaGlsZE1vZGFsJykgcHVibGljIGNoaWxkTW9kYWw6TW9kYWxEaXJlY3RpdmU7XHJcbiAgQElucHV0KCkgdGl0bGU/OnN0cmluZztcclxuICBwcml2YXRlIHJlc3RJZDtcclxuICBwcml2YXRlIHVzZXJJZDtcclxuICBwdWJsaWMgY2hvb3NlO1xyXG4gIHB1YmxpYyBuZXdzTGlzdDogYW55ID0gW107XHJcbiAgcHJpdmF0ZSBkYXRhOiBhbnkgPSBbXTtcclxuICBwcml2YXRlIHN0cmluZ21haWw7XHJcbiAgcHJpdmF0ZSBjdXJyZW50UGFnZTogbnVtYmVyID0gMTtcclxuICBwcml2YXRlIHRvdGFsSXRlbXM6IG51bWJlcjtcclxuICBwcml2YXRlIGl0ZW1zUGVyUGFnZTogbnVtYmVyID0gMTA7XHJcbiAgcHJpdmF0ZSBtYXhTaXplOiBudW1iZXIgPSAxMDtcclxuIHByaXZhdGUgc2VuZE1haWxDbXA6IFNlbmRFbWFpbE1hcmtldGluZ0NvbXBvbmVudFxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBzdWJzY3JpYmVTZXJ2OiBTdWJzY3JpYmVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2OiBTdG9yYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICBcclxuICApIHsgfVxyXG4gICAgXHJcbiAgXHJcbiAgcHVibGljIG5nT25Jbml0KCkge1xyXG4gICAgbGV0IHVzZXJLZXkgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcclxuICAgIGlmICghdXNlcktleSkge1xyXG4gICAgICB0aGlzLnVzZXJTZXJ2LmxvZ291dCgpO1xyXG4gICAgfSBcclxuICAgIHRoaXMubmV3c0xpc3QgPSB0aGlzLmRhdGEgPSBbXTtcclxuICAgIHRoaXMuc3RyaW5nbWFpbCA9IFwiXCI7XHJcbiAgICB0aGlzLmRvU2VhcmNoKCk7XHJcbiAgfVxyXG4gIFxyXG4gIHNob3coc2VuZE1haWxDbXApe1xyXG4gICAgdGhpcy5zZW5kTWFpbENtcCA9IHNlbmRNYWlsQ21wO1xyXG4gICAgdGhpcy5jaGlsZE1vZGFsLnNob3coKTtcclxuICB9XHJcbiAgaGlkZSgpe1xyXG4gICAgdGhpcy5jaGlsZE1vZGFsLmhpZGUoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZG9TZWFyY2goKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMubmV3c0xpc3QpO1xyXG4gICAgICB0aGlzLnRvdGFsSXRlbXMgPSB0aGlzLm5ld3NMaXN0Lmxlbmd0aDtcclxuICAgICAgdGhpcy5wYWdlQ2hhbmdlZCh7IHBhZ2U6IHRoaXMuY3VycmVudFBhZ2UsIGl0ZW1zUGVyUGFnZTogdGhpcy5pdGVtc1BlclBhZ2UgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcGFnZUNoYW5nZWQoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgbGV0IHN0YXJ0ID0gKGV2ZW50LnBhZ2UgLSAxKSAqIGV2ZW50Lml0ZW1zUGVyUGFnZTtcclxuICAgIGxldCBlbmQgPSBldmVudC5pdGVtc1BlclBhZ2UgPiAtMSA/IChzdGFydCArIGV2ZW50Lml0ZW1zUGVyUGFnZSkgOiB0aGlzLm5ld3NMaXN0Lmxlbmd0aDtcclxuICAgIHRoaXMuZGF0YSA9IHRoaXMubmV3c0xpc3Quc2xpY2Uoc3RhcnQsIGVuZCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGRvU2V0U2NvcGUob2JqKSB7XHJcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2LnNldFNjb3BlKG9iaik7XHJcbiAgfVxyXG4gIFxyXG4gIHByaXZhdGUgYWRkRW1haWwob2JqKSB7XHJcblxyXG4gICAgbGV0IG1haWxBZGRyZXNzZXMgPSB7XHJcbiAgICAgICdzZW5kVG8nIDokKFwiI3NlbmR0b1wiKS52YWwoKSxcclxuICAgICAgJ3NlbmRDQycgOiAgJChcIiNzZW5kY2NcIikudmFsKCksXHJcbiAgICAgICdzZW5kQkNDJyA6JChcIiNzZW5kYmNjXCIpLnZhbCgpLFxyXG4gICAgICAnc3ViamVjdCc6JChcIiNzdWJqZWN0XCIpLnZhbCgpLFxyXG4gICAgICAnY29udGVudCc6JChcIiNjb250ZW50XCIpLnZhbCgpXHJcbiAgICB9XHJcbiAgICAvL2FsZXJ0KFwiT0tcIik7XHJcbiAgICBsZXQgZW1haWwgPSBvYmouZW1haWw7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnN0cmluZ21haWwpO1xyXG4gICAgb2JqLmFkZGVkID0gdHJ1ZTtcclxuICAgIGlmKHRoaXMuY2hvb3NlID09IDEpXHJcbiAgICB7XHJcbiAgICAgIHRoaXMuc3RyaW5nbWFpbCA9ICQoXCIjc2VuZHRvXCIpLnZhbCgpICsgZW1haWwgK1wiO1wiO1xyXG4gICAgICBtYWlsQWRkcmVzc2VzLnNlbmRUbyA9IHRoaXMuc3RyaW5nbWFpbDtcclxuICAgIH1cclxuICAgIGlmKHRoaXMuY2hvb3NlID09IDIpXHJcbiAgICB7XHJcbiAgICAgIHRoaXMuc3RyaW5nbWFpbCA9ICQoXCIjc2VuZGNjXCIpLnZhbCgpICsgZW1haWwgK1wiO1wiO1xyXG4gICAgICAgbWFpbEFkZHJlc3Nlcy5zZW5kQ0MgPSB0aGlzLnN0cmluZ21haWw7XHJcbiAgICB9XHJcbiAgICBpZih0aGlzLmNob29zZSA9PSAzKVxyXG4gICAge1xyXG4gICAgICB0aGlzLnN0cmluZ21haWwgPSAkKFwiI3NlbmRiY2NcIikudmFsKCkgKyBlbWFpbCArXCI7XCI7XHJcbiAgICAgICBtYWlsQWRkcmVzc2VzLnNlbmRCQ0MgPSB0aGlzLnN0cmluZ21haWw7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zZW5kTWFpbENtcC5zZXRWYXIobWFpbEFkZHJlc3Nlcyk7XHJcbiAgfVxyXG4gIFxyXG5cclxufVxyXG4iXX0=
