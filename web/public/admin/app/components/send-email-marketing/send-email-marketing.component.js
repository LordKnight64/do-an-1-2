"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var restaurant_service_1 = require("app/services/restaurant.service");
var notification_service_1 = require("app/services/notification.service");
var user_service_1 = require("app/services/user.service");
var email_service_1 = require("app/services/email.service");
var router_1 = require("@angular/router");
var subscribe_list_modal_component_1 = require("app/components/subscribe-list-modal/subscribe-list-modal.component");
var subscribe_service_1 = require("app/services/subscribe.service");
var SendEmailMarketingComponent = (function () {
    function SendEmailMarketingComponent(restServ, fb, userServ, notifyServ, sendMailSer, router, subscribeServ) {
        this.restServ = restServ;
        this.fb = fb;
        this.userServ = userServ;
        this.notifyServ = notifyServ;
        this.sendMailSer = sendMailSer;
        this.router = router;
        this.subscribeServ = subscribeServ;
        this.subject = null;
        this.sendto = null;
        this.sendbcc = null;
        this.sendcc = null;
        this.content = null;
        this.formErrors = {
            'content': [],
            'subject': [],
            'sendbcc': [],
            'sendcc': [],
            'sendto': []
        };
        this.validationMessages = {
            'subject': {
                'required': 'The subject field is required.',
                'maxlength': 'The title may not be greater than 1536 characters.'
            },
            'content': {
                'required': 'The subject field is required.',
                'maxlength': 'The title may not be greater than 1536 characters.'
            },
            'sendto': {
                'required': 'The Send to field is required.'
            },
            'sendbcc': {
                'maxlength': 'The title may not be greater than 1536 characters.'
            },
            'sendcc': {
                'maxlength': 'The title may not be greater than 1536 characters.'
            }
        };
    }
    SendEmailMarketingComponent.prototype.ngOnInit = function () {
        //alert("OK");
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.buildForm();
        this.mymodel = {
            content: null,
            sendbcc: null,
            sendto: null,
            sendcc: null,
            subject: null
        };
    };
    SendEmailMarketingComponent.prototype.openModal = function (object) {
        var _this = this;
        var params = {};
        this.subscribeServ.getSubscribeList(params).then(function (response) {
            console.log('response ', response.subscribe);
            _this.mailModal.newsList = response.subscribe;
            _this.mailModal.choose = object;
            _this.mailModal.show(_this);
        }, function (error) {
            console.log(error);
        });
    };
    SendEmailMarketingComponent.prototype.sendEmail = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        console.log("SEND MAIL STARTED");
        var params = {
            'send_to': this.sendMarketingMail.value.sendto,
            'send_bb': this.sendMarketingMail.value.sendbcc,
            'send_cc': this.sendMarketingMail.value.sendcc,
            'subject': this.sendMarketingMail.value.subject,
            'content': this.sendMarketingMail.value.content,
            'user_id': this.userId,
            'rest_id': this.restId
        };
        console.log(params);
        this.sendMailSer.sendEmailMarketingList(params).then(function (response) {
            _this.notifyServ.success('Mail has been sent');
            _this.router.navigateByUrl('email-marketing-list');
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            console.log("SENT");
        }, function (error) {
            console.log(error);
        });
    };
    SendEmailMarketingComponent.prototype.buildForm = function () {
        var _this = this;
        this.sendMarketingMail = this.fb.group({
            'content': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(1536)]],
            'subject': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(1536)]],
            'sendbcc': ['', [forms_1.Validators.maxLength(1536)]],
            'sendcc': ['', [forms_1.Validators.maxLength(1536)]],
            'sendto': ['', [forms_1.Validators.required]]
        });
        this.subject = this.sendMarketingMail.controls['subject'];
        this.sendto = this.sendMarketingMail.controls['sendto'];
        this.sendbcc = this.sendMarketingMail.controls['sendbcc'];
        this.sendcc = this.sendMarketingMail.controls['sendcc'];
        this.content = this.sendMarketingMail.controls['content'];
        this.sendMarketingMail.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
    };
    SendEmailMarketingComponent.prototype.setVar = function (emailAddresses) {
        this.mymodel = {
            content: emailAddresses.content,
            sendbcc: emailAddresses.sendBCC,
            sendto: emailAddresses.sendTo,
            sendcc: emailAddresses.sendCC,
            subject: emailAddresses.subject
        };
        if (this.mymodel != null) {
            this.sendMarketingMail.controls['content'].setValue(this.mymodel.content);
            this.sendMarketingMail.controls['sendbcc'].setValue(this.mymodel.sendbcc);
            this.sendMarketingMail.controls['sendto'].setValue(this.mymodel.sendto);
            this.sendMarketingMail.controls['sendcc'].setValue(this.mymodel.sendcc);
            this.sendMarketingMail.controls['subject'].setValue(this.mymodel.subject);
            this.subject = this.sendMarketingMail.controls['subject'];
            this.sendto = this.sendMarketingMail.controls['sendto'];
            this.sendbcc = this.sendMarketingMail.controls['sendbcc'];
            this.sendcc = this.sendMarketingMail.controls['sendcc'];
            this.content = this.sendMarketingMail.controls['content'];
        }
    };
    SendEmailMarketingComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.sendMarketingMail) {
            return;
        }
        var form1 = this.sendMarketingMail;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form1.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    SendEmailMarketingComponent.prototype.sendFeedback = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        var params = {
            'user_id': this.userId,
            'content': this.content.value
        };
        this.restServ.sendFeeback(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            _this.notifyServ.success('your feedback has been sent');
        }, function (error) {
            console.log(error);
        });
    };
    __decorate([
        core_1.ViewChild('childModal'),
        __metadata("design:type", subscribe_list_modal_component_1.SubscribeListModalComponent)
    ], SendEmailMarketingComponent.prototype, "mailModal", void 0);
    SendEmailMarketingComponent = __decorate([
        core_1.Component({
            selector: 'table-list',
            templateUrl: utils_1.default.getView('app/components/send-email-marketing/send-email-marketing.component.html'),
            styleUrls: [utils_1.default.getView('app/components/send-email-marketing/send-email-marketing.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
            forms_1.FormBuilder,
            user_service_1.UserService,
            notification_service_1.NotificationService,
            email_service_1.EmailService,
            router_1.Router,
            subscribe_service_1.SubscribeService])
    ], SendEmailMarketingComponent);
    return SendEmailMarketingComponent;
}());
exports.SendEmailMarketingComponent = SendEmailMarketingComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3NlbmQtZW1haWwtbWFya2V0aW5nL3NlbmQtZW1haWwtbWFya2V0aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUEwRjtBQUMxRix3Q0FBNkY7QUFDN0YseUNBQW9DO0FBQ3BDLDZEQUEyRDtBQUMzRCxzRUFBb0U7QUFDcEUsMEVBQXdFO0FBQ3hFLDBEQUF3RDtBQUN4RCw0REFBMEQ7QUFDMUQsMENBQWlFO0FBQ2pFLHFIQUFpSDtBQUNqSCxvRUFBa0U7QUFRbEU7SUFZRSxxQ0FDVSxRQUEyQixFQUMzQixFQUFlLEVBQ2YsUUFBcUIsRUFDckIsVUFBK0IsRUFDL0IsV0FBeUIsRUFDekIsTUFBYyxFQUNkLGFBQStCO1FBTi9CLGFBQVEsR0FBUixRQUFRLENBQW1CO1FBQzNCLE9BQUUsR0FBRixFQUFFLENBQWE7UUFDZixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGVBQVUsR0FBVixVQUFVLENBQXFCO1FBQy9CLGdCQUFXLEdBQVgsV0FBVyxDQUFjO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxrQkFBYSxHQUFiLGFBQWEsQ0FBa0I7UUFiakMsWUFBTyxHQUFtQixJQUFJLENBQUM7UUFDL0IsV0FBTSxHQUFtQixJQUFJLENBQUM7UUFDOUIsWUFBTyxHQUFtQixJQUFJLENBQUM7UUFDL0IsV0FBTSxHQUFtQixJQUFJLENBQUM7UUFDOUIsWUFBTyxHQUFvQixJQUFJLENBQUM7UUFzSnhDLGVBQVUsR0FBRztZQUNYLFNBQVMsRUFBRSxFQUFFO1lBQ2IsU0FBUyxFQUFDLEVBQUU7WUFDWixTQUFTLEVBQUMsRUFBRTtZQUNaLFFBQVEsRUFBQyxFQUFFO1lBQ1gsUUFBUSxFQUFDLEVBQUU7U0FDWixDQUFDO1FBRUYsdUJBQWtCLEdBQUc7WUFDbkIsU0FBUyxFQUFFO2dCQUNULFVBQVUsRUFBRSxnQ0FBZ0M7Z0JBQzVDLFdBQVcsRUFBRSxvREFBb0Q7YUFDbEU7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsVUFBVSxFQUFFLGdDQUFnQztnQkFDNUMsV0FBVyxFQUFFLG9EQUFvRDthQUNsRTtZQUNELFFBQVEsRUFBQztnQkFDUCxVQUFVLEVBQUUsZ0NBQWdDO2FBQzdDO1lBQ0QsU0FBUyxFQUFFO2dCQUNULFdBQVcsRUFBRSxvREFBb0Q7YUFDbEU7WUFDRCxRQUFRLEVBQUU7Z0JBQ1IsV0FBVyxFQUFFLG9EQUFvRDthQUNsRTtTQUNGLENBQUM7SUF0S0UsQ0FBQztJQUVMLDhDQUFRLEdBQVI7UUFDRSxjQUFjO1FBQ2QsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUMsQ0FBQztRQUVELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsT0FBTyxFQUFFLElBQUk7WUFDYixPQUFPLEVBQUUsSUFBSTtZQUNiLE1BQU0sRUFBRSxJQUFJO1lBQ1osTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUM7SUFFSixDQUFDO0lBRUQsK0NBQVMsR0FBVCxVQUFVLE1BQU07UUFBaEIsaUJBYUM7UUFYQyxJQUFJLE1BQU0sR0FBRyxFQUNaLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7WUFDdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdDLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7WUFDN0MsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQy9CLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVELCtDQUFTLEdBQVQ7UUFBQSxpQkFrQ0M7UUFqQ0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0gsQ0FBQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNqQyxJQUFJLE1BQU0sR0FBRztZQUNYLFNBQVMsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDOUMsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTztZQUMvQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxNQUFNO1lBQzlDLFNBQVMsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU87WUFDL0MsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTztZQUMvQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3ZCLENBQUM7UUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXBCLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUMzRCxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDbEQsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEQsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsTUFBTSxDQUFDO1lBQ1QsQ0FBQztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sK0NBQVMsR0FBaEI7UUFBQSxpQkFrQkM7UUFoQkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3JDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbEUsU0FBUyxFQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNqRSxTQUFTLEVBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzNDLFFBQVEsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLGtCQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDMUMsUUFBUSxFQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNyQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsTUFBTSxHQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRTFELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZO2FBQ2hDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUM7SUFFekQsQ0FBQztJQUVNLDRDQUFNLEdBQWIsVUFBYyxjQUFjO1FBRXpCLElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsY0FBYyxDQUFDLE9BQU87WUFDL0IsT0FBTyxFQUFFLGNBQWMsQ0FBQyxPQUFPO1lBQy9CLE1BQU0sRUFBRSxjQUFjLENBQUMsTUFBTTtZQUM3QixNQUFNLEVBQUUsY0FBYyxDQUFDLE1BQU07WUFDN0IsT0FBTyxFQUFFLGNBQWMsQ0FBQyxPQUFPO1NBQ2hDLENBQUM7UUFHRixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFFekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN4RSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRTFFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzFELElBQUksQ0FBQyxNQUFNLEdBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFNUQsQ0FBQztJQUVILENBQUM7SUFFTyxvREFBYyxHQUF0QixVQUF1QixPQUFPLEVBQUUsSUFBVTtRQUV4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQ3hDLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNyQyxHQUFHLENBQUMsQ0FBQyxJQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQyx3Q0FBd0M7WUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDNUIsSUFBTSxPQUFPLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUM3RCxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hELEdBQUcsQ0FBQyxDQUFDLElBQU0sR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQStCTyxrREFBWSxHQUFwQjtRQUFBLGlCQTJCQztRQTFCQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLE1BQU0sQ0FBQztZQUNULENBQUM7UUFDSCxDQUFDO1FBRUQsSUFBSSxNQUFNLEdBQUc7WUFDWCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSztTQUM5QixDQUFDO1FBR0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUM3QyxJQUFJLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDckIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsRCxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDYixNQUFNLENBQUM7WUFDVCxDQUFDO1lBQ0QsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsQ0FBQztRQUN6RCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUE3TXdCO1FBQXhCLGdCQUFTLENBQUMsWUFBWSxDQUFDO2tDQUFvQiw0REFBMkI7a0VBQUM7SUFYN0QsMkJBQTJCO1FBUHRDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyx5RUFBeUUsQ0FBQztZQUNyRyxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLHdFQUF3RSxDQUFDLENBQUM7WUFDcEcsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0Fjb0Isc0NBQWlCO1lBQ3ZCLG1CQUFXO1lBQ0wsMEJBQVc7WUFDVCwwQ0FBbUI7WUFDbEIsNEJBQVk7WUFDakIsZUFBTTtZQUNDLG9DQUFnQjtPQW5COUIsMkJBQTJCLENBME52QztJQUFELGtDQUFDO0NBMU5ELEFBME5DLElBQUE7QUExTlksa0VBQTJCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL3NlbmQtZW1haWwtbWFya2V0aW5nL3NlbmQtZW1haWwtbWFya2V0aW5nLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBSZXN0YXVyYW50U2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9yZXN0YXVyYW50LnNlcnZpY2UnO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCJhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBFbWFpbFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvZW1haWwuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgU3Vic2NyaWJlTGlzdE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnYXBwL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3QtbW9kYWwvc3Vic2NyaWJlLWxpc3QtbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IFN1YnNjcmliZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc3Vic2NyaWJlLnNlcnZpY2UnO1xuIEBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3RhYmxlLWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvc2VuZC1lbWFpbC1tYXJrZXRpbmcvc2VuZC1lbWFpbC1tYXJrZXRpbmcuY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvc2VuZC1lbWFpbC1tYXJrZXRpbmcvc2VuZC1lbWFpbC1tYXJrZXRpbmcuY29tcG9uZW50LmNzcycpXSxcbiAgYW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJUcmFuc2l0aW9uXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBTZW5kRW1haWxNYXJrZXRpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBwcml2YXRlIG15bW9kZWw7XG4gIHByaXZhdGUgcmVzdElkO1xuICBwcml2YXRlIHVzZXJJZDtcbiAgcHJpdmF0ZSBkdEZvcm06IEZvcm1Hcm91cDtcbiAgcHVibGljIHNlbmRNYXJrZXRpbmdNYWlsOkZvcm1Hcm91cFxuICBwcml2YXRlIHN1YmplY3Q6QWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBzZW5kdG86QWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBzZW5kYmNjOkFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgc2VuZGNjOkFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgY29udGVudDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgQFZpZXdDaGlsZCgnY2hpbGRNb2RhbCcpIHByaXZhdGUgbWFpbE1vZGFsOiBTdWJzY3JpYmVMaXN0TW9kYWxDb21wb25lbnQ7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcmVzdFNlcnY6IFJlc3RhdXJhbnRTZXJ2aWNlLFxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLFxuICAgIHByaXZhdGUgdXNlclNlcnY6IFVzZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgbm90aWZ5U2VydjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgICBwcml2YXRlIHNlbmRNYWlsU2VyOiBFbWFpbFNlcnZpY2UsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIHN1YnNjcmliZVNlcnY6IFN1YnNjcmliZVNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICAvL2FsZXJ0KFwiT0tcIik7XG4gICAgbGV0IHVzZXJLZXkgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyX2tleScpKTtcbiAgICBpZiAoIXVzZXJLZXkpIHtcbiAgICAgIHRoaXMudXNlclNlcnYubG9nb3V0KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMudXNlcklkID0gdXNlcktleS51c2VyX2luZm8uaWQ7XG4gICAgICB0aGlzLnJlc3RJZCA9IHVzZXJLZXkucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgfVxuXG4gICAgdGhpcy5idWlsZEZvcm0oKTtcbiAgICB0aGlzLm15bW9kZWwgPSB7XG4gICAgICBjb250ZW50OiBudWxsLFxuICAgICAgc2VuZGJjYzogbnVsbCxcbiAgICAgIHNlbmR0bzogbnVsbCxcbiAgICAgIHNlbmRjYzogbnVsbCxcbiAgICAgIHN1YmplY3Q6IG51bGxcbiAgICB9OyBcbiAgIFxuICB9IFxuICBcbiAgb3Blbk1vZGFsKG9iamVjdClcbiAge1xuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgfTtcbiAgICB0aGlzLnN1YnNjcmliZVNlcnYuZ2V0U3Vic2NyaWJlTGlzdChwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgY29uc29sZS5sb2coJ3Jlc3BvbnNlICcsIHJlc3BvbnNlLnN1YnNjcmliZSk7XG4gICAgICB0aGlzLm1haWxNb2RhbC5uZXdzTGlzdCA9IHJlc3BvbnNlLnN1YnNjcmliZTtcbiAgICAgIHRoaXMubWFpbE1vZGFsLmNob29zZSA9IG9iamVjdDtcbiAgICAgIHRoaXMubWFpbE1vZGFsLnNob3codGhpcyk7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICAgIFxuICB9XG5cbiAgc2VuZEVtYWlsKCl7XG4gICAgdGhpcy5vblZhbHVlQ2hhbmdlZCh0cnVlKTtcbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgaWYgKHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ubGVuZ3RoID4gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKFwiU0VORCBNQUlMIFNUQVJURURcIik7XG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICdzZW5kX3RvJzogdGhpcy5zZW5kTWFya2V0aW5nTWFpbC52YWx1ZS5zZW5kdG8sICAgICBcbiAgICAgICdzZW5kX2JiJzogdGhpcy5zZW5kTWFya2V0aW5nTWFpbC52YWx1ZS5zZW5kYmNjLFxuICAgICAgJ3NlbmRfY2MnOiB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsLnZhbHVlLnNlbmRjYyxcbiAgICAgICdzdWJqZWN0JzogdGhpcy5zZW5kTWFya2V0aW5nTWFpbC52YWx1ZS5zdWJqZWN0LFxuICAgICAgJ2NvbnRlbnQnOiB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsLnZhbHVlLmNvbnRlbnQsXG4gICAgICAndXNlcl9pZCc6IHRoaXMudXNlcklkLFxuICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZFxuICAgIH07XG4gICAgY29uc29sZS5sb2cocGFyYW1zKTtcbiAgICBcbiAgICB0aGlzLnNlbmRNYWlsU2VyLnNlbmRFbWFpbE1hcmtldGluZ0xpc3QocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCdNYWlsIGhhcyBiZWVuIHNlbnQnKTtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ2VtYWlsLW1hcmtldGluZy1saXN0Jyk7XG4gICAgICBsZXQgaGFyRXJyb3IgPSBmYWxzZTtcbiAgICAgIGZvciAoY29uc3QgZmllbGQgaW4gcmVzcG9uc2UuZXJyb3JzKSB7XG4gICAgICAgIGhhckVycm9yID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IHJlc3BvbnNlLmVycm9yc1tmaWVsZF07XG4gICAgICB9XG4gICAgICBpZiAoaGFyRXJyb3IpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgY29uc29sZS5sb2coXCJTRU5UXCIpO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBidWlsZEZvcm0oKTogdm9pZCB7XG5cbiAgICB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAnY29udGVudCc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDE1MzYpXV0sXG4gICAgICAnc3ViamVjdCc6WycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTUzNildXSxcbiAgICAgICdzZW5kYmNjJzpbJycsW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDE1MzYpXV0sXG4gICAgICAnc2VuZGNjJzpbJycsW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDE1MzYpXV0sXG4gICAgICAnc2VuZHRvJzpbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkXV0gICAgIFxuICAgIH0pO1xuICAgIHRoaXMuc3ViamVjdCA9IHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ3N1YmplY3QnXTtcbiAgICB0aGlzLnNlbmR0byA9IHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ3NlbmR0byddO1xuICAgIHRoaXMuc2VuZGJjYyA9IHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ3NlbmRiY2MnXTtcbiAgICB0aGlzLnNlbmRjYz0gdGhpcy5zZW5kTWFya2V0aW5nTWFpbC5jb250cm9sc1snc2VuZGNjJ107XG4gICAgdGhpcy5jb250ZW50ID0gdGhpcy5zZW5kTWFya2V0aW5nTWFpbC5jb250cm9sc1snY29udGVudCddO1xuICBcbiAgICB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsLnZhbHVlQ2hhbmdlc1xuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMub25WYWx1ZUNoYW5nZWQoZmFsc2UsIGRhdGEpKTtcbiAgICBcbiAgfVxuXG4gIHB1YmxpYyBzZXRWYXIoZW1haWxBZGRyZXNzZXMpOiB2b2lkIHtcbiAgICBcbiAgICAgdGhpcy5teW1vZGVsID0ge1xuICAgICAgY29udGVudDogZW1haWxBZGRyZXNzZXMuY29udGVudCxcbiAgICAgIHNlbmRiY2M6IGVtYWlsQWRkcmVzc2VzLnNlbmRCQ0MsXG4gICAgICBzZW5kdG86IGVtYWlsQWRkcmVzc2VzLnNlbmRUbyxcbiAgICAgIHNlbmRjYzogZW1haWxBZGRyZXNzZXMuc2VuZENDLFxuICAgICAgc3ViamVjdDogZW1haWxBZGRyZXNzZXMuc3ViamVjdFxuICAgIH07IFxuICAgIFxuICAgIFxuICAgIGlmICh0aGlzLm15bW9kZWwgIT0gbnVsbCkge1xuICAgICBcbiAgICAgIHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ2NvbnRlbnQnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuY29udGVudCk7XG4gICAgICB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsLmNvbnRyb2xzWydzZW5kYmNjJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLnNlbmRiY2MpO1xuICAgICAgdGhpcy5zZW5kTWFya2V0aW5nTWFpbC5jb250cm9sc1snc2VuZHRvJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLnNlbmR0byk7XG4gICAgICB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsLmNvbnRyb2xzWydzZW5kY2MnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuc2VuZGNjKTtcbiAgICAgIHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ3N1YmplY3QnXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuc3ViamVjdCk7XG4gICAgICAgXG4gICAgICB0aGlzLnN1YmplY3QgPSB0aGlzLnNlbmRNYXJrZXRpbmdNYWlsLmNvbnRyb2xzWydzdWJqZWN0J107XG4gICAgICB0aGlzLnNlbmR0byA9IHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ3NlbmR0byddO1xuICAgICAgdGhpcy5zZW5kYmNjID0gdGhpcy5zZW5kTWFya2V0aW5nTWFpbC5jb250cm9sc1snc2VuZGJjYyddO1xuICAgICAgdGhpcy5zZW5kY2M9IHRoaXMuc2VuZE1hcmtldGluZ01haWwuY29udHJvbHNbJ3NlbmRjYyddO1xuICAgICAgdGhpcy5jb250ZW50ID0gdGhpcy5zZW5kTWFya2V0aW5nTWFpbC5jb250cm9sc1snY29udGVudCddO1xuICAgIFxuICAgIH1cblxuICB9XG5cbiAgcHJpdmF0ZSBvblZhbHVlQ2hhbmdlZCh1bkRpcnR5LCBkYXRhPzogYW55KSB7XG4gICAgXG4gICAgaWYgKCF0aGlzLnNlbmRNYXJrZXRpbmdNYWlsKSB7IHJldHVybjsgfVxuICAgIGNvbnN0IGZvcm0xID0gdGhpcy5zZW5kTWFya2V0aW5nTWFpbDtcbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxuICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IFtdO1xuICAgICAgY29uc3QgY29udHJvbCA9IGZvcm0xLmdldChmaWVsZCk7XG4gICAgICBpZiAoY29udHJvbCAmJiAoY29udHJvbC5kaXJ0eSB8fCB1bkRpcnR5KSAmJiBjb250cm9sLmludmFsaWQpIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZXMgPSB0aGlzLnZhbGlkYXRpb25NZXNzYWdlc1tmaWVsZF07XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIGNvbnRyb2wuZXJyb3JzKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5wdXNoKG1lc3NhZ2VzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZm9ybUVycm9ycyA9IHtcbiAgICAnY29udGVudCc6IFtdLFxuICAgICdzdWJqZWN0JzpbXSxcbiAgICAnc2VuZGJjYyc6W10sXG4gICAgJ3NlbmRjYyc6W10sXG4gICAgJ3NlbmR0byc6W10gICAgIFxuICB9O1xuXG4gIHZhbGlkYXRpb25NZXNzYWdlcyA9IHtcbiAgICAnc3ViamVjdCc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICdUaGUgc3ViamVjdCBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgdGl0bGUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTUzNiBjaGFyYWN0ZXJzLidcbiAgICB9LFxuICAgICdjb250ZW50Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBzdWJqZWN0IGZpZWxkIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSB0aXRsZSBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxNTM2IGNoYXJhY3RlcnMuJ1xuICAgIH0sXG4gICAgJ3NlbmR0byc6e1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSBTZW5kIHRvIGZpZWxkIGlzIHJlcXVpcmVkLidcbiAgICB9LFxuICAgICdzZW5kYmNjJzoge1xuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgdGl0bGUgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTUzNiBjaGFyYWN0ZXJzLidcbiAgICB9LFxuICAgICdzZW5kY2MnOiB7XG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSB0aXRsZSBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxNTM2IGNoYXJhY3RlcnMuJ1xuICAgIH1cbiAgfTtcblxuICAgXG4gIHByaXZhdGUgc2VuZEZlZWRiYWNrKCkge1xuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQodHJ1ZSk7XG4gICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcnMpIHtcbiAgICAgIGlmICh0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBwYXJhbXMgPSB7XG4gICAgICAndXNlcl9pZCc6IHRoaXMudXNlcklkLCAgICAgXG4gICAgICAnY29udGVudCc6IHRoaXMuY29udGVudC52YWx1ZSAgICBcbiAgICB9O1xuICAgIFxuXG4gICAgdGhpcy5yZXN0U2Vydi5zZW5kRmVlYmFjayhwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgbGV0IGhhckVycm9yID0gZmFsc2U7XG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHJlc3BvbnNlLmVycm9ycykge1xuICAgICAgICBoYXJFcnJvciA9IHRydWU7XG4gICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0gPSByZXNwb25zZS5lcnJvcnNbZmllbGRdO1xuICAgICAgfVxuICAgICAgaWYgKGhhckVycm9yKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMubm90aWZ5U2Vydi5zdWNjZXNzKCd5b3VyIGZlZWRiYWNrIGhhcyBiZWVuIHNlbnQnKTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfSk7XG4gIH1cblxufVxuIl19
