"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var restaurant_service_1 = require("app/services/restaurant.service");
var notification_service_1 = require("app/services/notification.service");
var user_service_1 = require("app/services/user.service");
var router_animations_1 = require("../../router.animations");
var router_1 = require("@angular/router");
var TableBookingComponent = (function () {
    function TableBookingComponent(restServ, fb, userServ, router, notifyServ) {
        this.restServ = restServ;
        this.fb = fb;
        this.userServ = userServ;
        this.router = router;
        this.notifyServ = notifyServ;
        this.content = null;
        this.fromDate = null;
        this.Settings = {
            statusDateEdit: {
                t2: false,
                t3: false,
                t4: false,
                t5: false,
                t6: false,
                t7: false,
                cn: false,
                hn: false
            },
            list: null,
            currentPage: 0,
            filter: {
                title: "",
                category_id: "",
                created_at_from: "",
                created_at_to: ""
            },
            showFilter: {
                'tinh_trang': '1',
                'loaiKhach': '-1'
            },
            deletePopover: {
                content: 'Bạn có thực sự muốn xóa không?',
                Yes: 'Có',
                No: 'Không',
                flag: -1
            },
            timeMstList: ['07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'],
            dayInWeek: {
                t2: 'Thứ Hai',
                t3: 'Thứ Ba',
                t4: 'Thứ Tư',
                t5: 'Thứ Năm',
                t6: 'Thứ Sáu',
                t7: 'Thứ Bảy',
                cn: 'Chủ nhật'
            }
        };
        this.timeList = [];
        for (var i = 0; i < this.Settings.timeMstList.length; i++) {
            var timeMst = this.Settings.timeMstList[i];
            if (timeMst != '24') {
                var time1 = {
                    stTime: timeMst + ":00",
                    endTime: timeMst + ":30",
                    cusName: "",
                    checkFlg: 0,
                    endFlg: 0,
                    loaiKhach: '0'
                };
                this.timeList.push(time1);
                var end = '' + timeMst + 1;
                if ((end + '').length == 1) {
                    end = '0' + end;
                }
                var time1 = {
                    stTime: timeMst + ":30",
                    endTime: end + ":00",
                    cusName: "",
                    checkFlg: 0,
                    endFlg: 0,
                    loaiKhach: '0'
                };
                this.timeList.push(time1);
            }
        }
        this.stadiumList = [
            { 'ten_khu_vuc': 'Floor 1', 'stadiumName': 'Table 1 - 5 Slots' },
            { 'ten_khu_vuc': 'Floor 1', 'stadiumName': 'Table 2 - 4 Slots' },
            { 'ten_khu_vuc': 'Floor 1', 'stadiumName': 'Table 3 - 3 Slots' },
            { 'ten_khu_vuc': 'Floor 2', 'stadiumName': 'Table 4 - 2 Slots ' },
            { 'ten_khu_vuc': 'Floor 2', 'stadiumName': 'Table 5 - 4 Slots' },
            { 'ten_khu_vuc': 'Floor 1', 'stadiumName': 'Table 6 - 5 Slots' }
        ];
        console.log(' this.stadiumList ', this.stadiumList);
    }
    TableBookingComponent.prototype.ngOnInit = function () {
    };
    TableBookingComponent.prototype.onSelectFromDate = function (value) {
    };
    TableBookingComponent.prototype.reserveTable = function () {
        this.router.navigateByUrl('/reserve-table/create/');
    };
    TableBookingComponent = __decorate([
        core_1.Component({
            selector: 'table-booking',
            templateUrl: utils_1.default.getView('app/components/table-booking/table-booking.component.html'),
            styleUrls: [utils_1.default.getView('app/components/table-booking/table-booking.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
            forms_1.FormBuilder,
            user_service_1.UserService,
            router_1.Router,
            notification_service_1.NotificationService])
    ], TableBookingComponent);
    return TableBookingComponent;
}());
exports.TableBookingComponent = TableBookingComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3RhYmxlLWJvb2tpbmcvdGFibGUtYm9va2luZy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQsd0NBQTZGO0FBQzdGLHlDQUFvQztBQUNwQyxzRUFBb0U7QUFDcEUsMEVBQXdFO0FBQ3hFLDBEQUF3RDtBQUN4RCw2REFBMkQ7QUFDM0QsMENBQXlDO0FBUXpDO0lBVUUsK0JBQ1UsUUFBMkIsRUFDM0IsRUFBZSxFQUNmLFFBQXFCLEVBQ3BCLE1BQWMsRUFDZixVQUErQjtRQUovQixhQUFRLEdBQVIsUUFBUSxDQUFtQjtRQUMzQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNwQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2YsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFWakMsWUFBTyxHQUFvQixJQUFJLENBQUM7UUFJL0IsYUFBUSxHQUFRLElBQUksQ0FBQztRQVM5QixJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ2IsY0FBYyxFQUFDO2dCQUNkLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2dCQUNSLEVBQUUsRUFBQyxLQUFLO2FBQ1I7WUFDRSxJQUFJLEVBQUUsSUFBSTtZQUNWLFdBQVcsRUFBRSxDQUFDO1lBQ2QsTUFBTSxFQUFFO2dCQUNKLEtBQUssRUFBRSxFQUFFO2dCQUNULFdBQVcsRUFBRSxFQUFFO2dCQUNmLGVBQWUsRUFBRSxFQUFFO2dCQUNuQixhQUFhLEVBQUUsRUFBRTthQUNwQjtZQUNELFVBQVUsRUFBQztnQkFDUCxZQUFZLEVBQUcsR0FBRztnQkFDbEIsV0FBVyxFQUFDLElBQUk7YUFDbkI7WUFFRCxhQUFhLEVBQUU7Z0JBQ1gsT0FBTyxFQUFFLGdDQUFnQztnQkFDekMsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsRUFBRSxFQUFFLE9BQU87Z0JBQ1gsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUNYO1lBQ0QsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFDLElBQUksRUFBQyxJQUFJLENBQUM7WUFDdkgsU0FBUyxFQUFFO2dCQUNQLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxRQUFRO2dCQUNaLEVBQUUsRUFBRSxRQUFRO2dCQUNaLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEVBQUUsRUFBRSxVQUFVO2FBQ2pCO1NBQ0osQ0FBQztRQUVILElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekQsSUFBSSxPQUFPLEdBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFNUMsRUFBRSxDQUFBLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLElBQUksS0FBSyxHQUFHO29CQUNELE1BQU0sRUFBRSxPQUFPLEdBQUcsS0FBSztvQkFDdkIsT0FBTyxFQUFFLE9BQU8sR0FBRyxLQUFLO29CQUN4QixPQUFPLEVBQUUsRUFBRTtvQkFDWCxRQUFRLEVBQUUsQ0FBQztvQkFDWCxNQUFNLEVBQUUsQ0FBQztvQkFDVCxTQUFTLEVBQUUsR0FBRztpQkFDakIsQ0FBQztnQkFFRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxHQUFHLEdBQUcsRUFBRSxHQUFFLE9BQU8sR0FBRyxDQUFDLENBQUM7Z0JBQzFCLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN6QixHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztnQkFDcEIsQ0FBQztnQkFDRCxJQUFJLEtBQUssR0FBRztvQkFDUixNQUFNLEVBQUUsT0FBTyxHQUFHLEtBQUs7b0JBQ3ZCLE9BQU8sRUFBRSxHQUFHLEdBQUcsS0FBSztvQkFDcEIsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLENBQUM7b0JBQ1gsTUFBTSxFQUFFLENBQUM7b0JBQ1QsU0FBUyxFQUFFLEdBQUc7aUJBQ2pCLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEMsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHO1lBQ2IsRUFBQyxhQUFhLEVBQUUsU0FBUyxFQUFDLGFBQWEsRUFBRSxtQkFBbUIsRUFBQztZQUM3RCxFQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUMsYUFBYSxFQUFFLG1CQUFtQixFQUFDO1lBQzdELEVBQUMsYUFBYSxFQUFFLFNBQVMsRUFBQyxhQUFhLEVBQUUsbUJBQW1CLEVBQUM7WUFDN0QsRUFBQyxhQUFhLEVBQUUsU0FBUyxFQUFDLGFBQWEsRUFBRSxvQkFBb0IsRUFBQztZQUM5RCxFQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUMsYUFBYSxFQUFFLG1CQUFtQixFQUFDO1lBQzdELEVBQUMsYUFBYSxFQUFFLFNBQVMsRUFBQyxhQUFhLEVBQUUsbUJBQW1CLEVBQUM7U0FDaEUsQ0FBQztRQUVGLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBRXpELENBQUM7SUFFRCx3Q0FBUSxHQUFSO0lBRUEsQ0FBQztJQUVPLGdEQUFnQixHQUF2QixVQUF3QixLQUFVO0lBRW5DLENBQUM7SUFHTyw0Q0FBWSxHQUFuQjtRQUNHLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDdkQsQ0FBQztJQW5IUyxxQkFBcUI7UUFQaEMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLDJEQUEyRCxDQUFDO1lBQ3ZGLFNBQVMsRUFBRSxDQUFDLGVBQUssQ0FBQyxPQUFPLENBQUMsMERBQTBELENBQUMsQ0FBQztZQUNwRixVQUFVLEVBQUUsQ0FBQyxvQ0FBZ0IsRUFBRSxDQUFDO1lBQ2xDLElBQUksRUFBRSxFQUFDLHFCQUFxQixFQUFFLEVBQUUsRUFBQztTQUNsQyxDQUFDO3lDQVlvQixzQ0FBaUI7WUFDdkIsbUJBQVc7WUFDTCwwQkFBVztZQUNaLGVBQU07WUFDSCwwQ0FBbUI7T0FmOUIscUJBQXFCLENBb0hqQztJQUFELDRCQUFDO0NBcEhELEFBb0hDLElBQUE7QUFwSFksc0RBQXFCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL3RhYmxlLWJvb2tpbmcvdGFibGUtYm9va2luZy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycywgTmdGb3JtLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IFJlc3RhdXJhbnRTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3Jlc3RhdXJhbnQuc2VydmljZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuIEBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3RhYmxlLWJvb2tpbmcnLFxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvdGFibGUtYm9va2luZy90YWJsZS1ib29raW5nLmNvbXBvbmVudC5odG1sJyksXG4gIHN0eWxlVXJsczogW1V0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL3RhYmxlLWJvb2tpbmcvdGFibGUtYm9va2luZy5jb21wb25lbnQuY3NzJyldLFxuICAgIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ31cbn0pXG5leHBvcnQgY2xhc3MgVGFibGVCb29raW5nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHJlc3RJZDtcbiAgcHJpdmF0ZSB1c2VySWQ7XG4gIHByaXZhdGUgZHRGb3JtOiBGb3JtR3JvdXA7XG4gIHByaXZhdGUgY29udGVudDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBTZXR0aW5nczphbnk7XG4gIHByaXZhdGUgc3RhZGl1bUxpc3Q6IGFueVtdO1xuICBwcml2YXRlIHRpbWVMaXN0OmFueVtdO1xuICAgcHJpdmF0ZSBmcm9tRGF0ZTogYW55ID0gbnVsbDtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByZXN0U2VydjogUmVzdGF1cmFudFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gICAgcHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UsXG4gICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJpdmF0ZSBub3RpZnlTZXJ2OiBOb3RpZmljYXRpb25TZXJ2aWNlXG4gICkgeyBcbiAgXG4gIHRoaXMuU2V0dGluZ3MgPSB7XG4gICAgXHRzdGF0dXNEYXRlRWRpdDp7XG4gICAgXHRcdHQyOmZhbHNlLFxuICAgIFx0XHR0MzpmYWxzZSxcbiAgICBcdFx0dDQ6ZmFsc2UsXG4gICAgXHRcdHQ1OmZhbHNlLFxuICAgIFx0XHR0NjpmYWxzZSxcbiAgICBcdFx0dDc6ZmFsc2UsXG4gICAgXHRcdGNuOmZhbHNlLFxuICAgIFx0XHRobjpmYWxzZVxuICAgIFx0fSxcbiAgICAgICAgbGlzdDogbnVsbCxcbiAgICAgICAgY3VycmVudFBhZ2U6IDAsXG4gICAgICAgIGZpbHRlcjoge1xuICAgICAgICAgICAgdGl0bGU6IFwiXCIsXG4gICAgICAgICAgICBjYXRlZ29yeV9pZDogXCJcIixcbiAgICAgICAgICAgIGNyZWF0ZWRfYXRfZnJvbTogXCJcIixcbiAgICAgICAgICAgIGNyZWF0ZWRfYXRfdG86IFwiXCIgXG4gICAgICAgIH0sXG4gICAgICAgIHNob3dGaWx0ZXI6e1xuICAgICAgICAgICAgJ3RpbmhfdHJhbmcnIDogJzEnLFxuICAgICAgICAgICAgJ2xvYWlLaGFjaCc6Jy0xJ1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlbGV0ZVBvcG92ZXI6IHtcbiAgICAgICAgICAgIGNvbnRlbnQ6ICdC4bqhbiBjw7MgdGjhu7FjIHPhu7EgbXXhu5FuIHjDs2Ega2jDtG5nPycsXG4gICAgICAgICAgICBZZXM6ICdDw7MnLFxuICAgICAgICAgICAgTm86ICdLaMO0bmcnLFxuICAgICAgICAgICAgZmxhZzogLTFcbiAgICAgICAgfSxcbiAgICAgICAgdGltZU1zdExpc3Q6IFsnMDcnLCAnMDgnLCAnMDknLCAnMTAnLCAnMTEnLCAnMTInLCAnMTMnLCAnMTQnLCAnMTUnLCAnMTYnLCAnMTcnLCAnMTgnLCAnMTknLCAnMjAnLCAnMjEnLCAnMjInLCcyMycsJzI0J10sXG4gICAgICAgIGRheUluV2Vlazoge1xuICAgICAgICAgICAgdDI6ICdUaOG7qSBIYWknLFxuICAgICAgICAgICAgdDM6ICdUaOG7qSBCYScsXG4gICAgICAgICAgICB0NDogJ1Ro4bupIFTGsCcsXG4gICAgICAgICAgICB0NTogJ1Ro4bupIE7Eg20nLFxuICAgICAgICAgICAgdDY6ICdUaOG7qSBTw6F1JyxcbiAgICAgICAgICAgIHQ3OiAnVGjhu6kgQuG6o3knLFxuICAgICAgICAgICAgY246ICdDaOG7pyBuaOG6rXQnXG4gICAgICAgIH1cbiAgICB9O1xuXG4gICB0aGlzLnRpbWVMaXN0ID0gW107XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCAgdGhpcy5TZXR0aW5ncy50aW1lTXN0TGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgdGltZU1zdCA9ICB0aGlzLlNldHRpbmdzLnRpbWVNc3RMaXN0W2ldO1xuXG4gICAgICAgIGlmKHRpbWVNc3QgIT0gJzI0Jyl7XG4gICAgICAgIFx0dmFyIHRpbWUxID0ge1xuICAgICAgICAgICAgICAgICAgICBzdFRpbWU6IHRpbWVNc3QgKyBcIjowMFwiLFxuICAgICAgICAgICAgICAgICAgICBlbmRUaW1lOiB0aW1lTXN0ICsgXCI6MzBcIixcbiAgICAgICAgICAgICAgICAgICAgY3VzTmFtZTogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgY2hlY2tGbGc6IDAsXG4gICAgICAgICAgICAgICAgICAgIGVuZEZsZzogMCxcbiAgICAgICAgICAgICAgICAgICAgbG9haUtoYWNoOiAnMCdcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgdGhpcy50aW1lTGlzdC5wdXNoKHRpbWUxKTtcbiAgICAgICAgICAgICAgICB2YXIgZW5kID0gJycgK3RpbWVNc3QgKyAxO1xuICAgICAgICAgICAgICAgIGlmICgoZW5kICsgJycpLmxlbmd0aCA9PSAxKSB7XG4gICAgICAgICAgICAgICAgICAgIGVuZCA9ICcwJyArIGVuZDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIHRpbWUxID0ge1xuICAgICAgICAgICAgICAgICAgICBzdFRpbWU6IHRpbWVNc3QgKyBcIjozMFwiLFxuICAgICAgICAgICAgICAgICAgICBlbmRUaW1lOiBlbmQgKyBcIjowMFwiLFxuICAgICAgICAgICAgICAgICAgICBjdXNOYW1lOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgICBjaGVja0ZsZzogMCxcbiAgICAgICAgICAgICAgICAgICAgZW5kRmxnOiAwLFxuICAgICAgICAgICAgICAgICAgICBsb2FpS2hhY2g6ICcwJ1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgdGhpcy50aW1lTGlzdC5wdXNoKHRpbWUxKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuc3RhZGl1bUxpc3QgPSBbXG4gICAgICAgICAgeyd0ZW5fa2h1X3Z1Yyc6ICdGbG9vciAxJywnc3RhZGl1bU5hbWUnOiAnVGFibGUgMSAtIDUgU2xvdHMnfSwgXG4gICAgICAgICAgeyd0ZW5fa2h1X3Z1Yyc6ICdGbG9vciAxJywnc3RhZGl1bU5hbWUnOiAnVGFibGUgMiAtIDQgU2xvdHMnfSxcbiAgICAgICAgICB7J3Rlbl9raHVfdnVjJzogJ0Zsb29yIDEnLCdzdGFkaXVtTmFtZSc6ICdUYWJsZSAzIC0gMyBTbG90cyd9LFxuICAgICAgICAgIHsndGVuX2todV92dWMnOiAnRmxvb3IgMicsJ3N0YWRpdW1OYW1lJzogJ1RhYmxlIDQgLSAyIFNsb3RzICd9LFxuICAgICAgICAgIHsndGVuX2todV92dWMnOiAnRmxvb3IgMicsJ3N0YWRpdW1OYW1lJzogJ1RhYmxlIDUgLSA0IFNsb3RzJ30sXG4gICAgICAgICAgeyd0ZW5fa2h1X3Z1Yyc6ICdGbG9vciAxJywnc3RhZGl1bU5hbWUnOiAnVGFibGUgNiAtIDUgU2xvdHMnfVxuICAgICAgXTtcblxuICAgICAgY29uc29sZS5sb2coJyB0aGlzLnN0YWRpdW1MaXN0ICcsICB0aGlzLnN0YWRpdW1MaXN0KTtcblxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICBcbiAgfSBcblxuICAgcHVibGljIG9uU2VsZWN0RnJvbURhdGUodmFsdWU6IGFueSk6IHZvaWQge1xuICAgIFxuICB9XG4gIFxuICAgXG4gICBwdWJsaWMgcmVzZXJ2ZVRhYmxlKCkge1xuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL3Jlc2VydmUtdGFibGUvY3JlYXRlLycpO1xuICAgfVxufVxuIl19
