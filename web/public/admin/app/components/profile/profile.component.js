"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var profile_service_1 = require("app/services/profile.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var ng2_select_1 = require("ng2-select");
var number_validator_directive_1 = require("app/utils/number-validator.directive");
var forms_1 = require("@angular/forms");
var email_validator_directive_1 = require("app/utils/email-validator.directive");
var comCode_service_1 = require("app/services/comCode.service");
var user_service_1 = require("app/services/user.service");
var ProfileComponent = (function () {
    function ProfileComponent(fb, comCodeService, profileService, StorageService, userService, authServ, notif, router, route) {
        this.fb = fb;
        this.comCodeService = comCodeService;
        this.profileService = profileService;
        this.StorageService = StorageService;
        this.userService = userService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.route = route;
        this.tabChangeFisrt = true;
        this.open_time = null;
        this.close_time = null;
        this.lat = null;
        this.lon = null;
        this.changelonlatFlag = false;
        this.items = [];
        this.itemCurrencys = [];
        this.markers = {};
        this.idmarker = 0;
        this.formErrors = {
            'first_name': [],
            'last_name': [],
            'email': [],
            'phone': [],
            'tel': [],
            'password': [],
            'confirm_password': [],
            'restaurant_name': [],
            'restaurant_address': [],
            'open_time': [],
            'close_time': [],
            'website': [],
            'default_currency': [],
            'rest_type': [],
        };
        this.validationMessages = {
            'first_name': {
                'required': 'The name is required.',
                'maxlength': 'The name must be at less 256 characters long.'
            },
            'last_name': {
                'required': 'The last_name is required.',
                'maxlength': 'The last_name must be at less 256 characters long.'
            },
            'email': {
                'required': 'The email is required.',
                'minlength': 'The email must be at least 256 characters long.',
                'invalidEmail': 'The email must be a valid email address.',
            },
            'phone': {
                'required': 'The phone is required.',
                'maxlength': 'The phone must be at less 32 characters long.',
                'invalidNumber': 'he phone must be number'
            },
            'tel': {
                'required': 'The tel is required.',
                'maxlength': 'The tel must be at less 32 characters long.'
            },
            'password': {
                'required': 'The password is required.',
                'minlength': 'The password must be at least 6 characters long.',
                'maxlength': 'The last_name must be at less 256 characters long.'
            },
            'confirm_password': {
                'required': 'The password is required.',
                'minlength': 'The password must be at least 6 characters long.',
                'maxlength': 'The last_name must be at less 256 characters long.'
            },
            'restaurant_name': {
                'required': 'The restaurant_name is required.',
                'maxlength': 'The restaurant_name must be at less 256 characters long.'
            },
            'restaurant_address': {
                'required': 'The restaurant_address is required.',
                'maxlength': 'The restaurant_address must be at less 1024 characters long.'
            },
            'open_time': {
                'required': 'The close_time is required / formatNumber.'
            },
            'close_time': {
                'required': 'The close_time is required / formatNumber.'
            },
            'website': {
                'required': 'The website is required.'
            },
            'default_currency': {
                'required': 'The default_currency is required.'
            },
            'rest_type': {
                'required': 'The required is required.'
            }
        };
        /*select start*/
        this.value = {};
        this.valueCurrentcy = {};
        this._disabledV = '0';
        this.disabled = false;
        /*upload file*/
        this.sactiveColor = 'green';
        this.baseColor = '#ccc';
        this.overlayColor = 'rgba(255,255,255,0.5)';
        this.dragging = false;
        this.loaded = false;
        this.imageLoaded = false;
        this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
        this.flgImageChoose = false;
        this.iconColor = '';
        this.borderColor = '';
        this.activeColor = '';
        this.hiddenImage = false;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.buildForm();
        var userLogin = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userLogin.restaurants[0].id;
        this.userId = userLogin.user_info.id;
        console.log(userLogin);
        this.userInfo = userLogin.user_info;
        this.createProfileForm.controls['first_name'].setValue(this.userInfo.first_name);
        this.createProfileForm.controls['last_name'].setValue(this.userInfo.last_name);
        this.createProfileForm.controls['email'].setValue(this.userInfo.email);
        this.createProfileForm.controls['phone'].setValue(this.userInfo.phone);
        this.createProfileForm.controls['tel'].setValue(this.userInfo.tel);
        this.first_name = this.createProfileForm.controls['first_name'];
        this.last_name = this.createProfileForm.controls['last_name'];
        this.email = this.createProfileForm.controls['email'];
        this.phone = this.createProfileForm.controls['phone'];
        this.fetchProfileRetaurant();
    };
    ProfileComponent.prototype.fetchCode = function (cd) {
        var _this = this;
        var param = {
            'cd_group': cd
        };
        this.comCodeService.getCode(param).then(function (response) {
            if (response.code != null && response.code.length > 0) {
                for (var i = 0; i < response.code.length; i++) {
                    _this.items.push({
                        id: response.code[i].cd.toString(),
                        text: response.code[i].cd_name
                    });
                }
                _this.select.items = _this.items;
                _this.value = _this.items[_this.rest_type.value];
            }
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.fetchCurrentCyCode = function (cd) {
        var _this = this;
        var param = {
            'cd_group': cd
        };
        this.comCodeService.getCode(param).then(function (response) {
            if (response.code != null && response.code.length > 0) {
                var cdCurrentcy = null;
                for (var i = 0; i < response.code.length; i++) {
                    _this.itemCurrencys.push({
                        id: response.code[i].cd,
                        text: "" + response.code[i].cd_name
                    });
                    if (_this.default_currency.value == response.code[i].cd_name) {
                        cdCurrentcy = response.code[i].cd;
                    }
                }
                _this.selectCurrency.items = _this.itemCurrencys;
                _this.valueCurrentcy = _this.itemCurrencys[cdCurrentcy];
            }
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.fetchProfileRetaurant = function () {
        var _this = this;
        this.profileService.getProfileRetaurant(this.restId, this.userId).then(function (response) {
            if (response.restaurant_info != null && response.restaurant_info.length > 0) {
                _this.restaurantInfo = response.restaurant_info[0];
                console.log(_this.restaurantInfo);
                _this.createProfileForm.controls['restaurant_name'].setValue(_this.restaurantInfo.name);
                _this.createProfileForm.controls['restaurant_address'].setValue(_this.restaurantInfo.address);
                _this.createProfileForm.controls['restaurant_tel_number'].setValue(_this.restaurantInfo.tel);
                _this.createProfileForm.controls['restaurant_mobile_number'].setValue(_this.restaurantInfo.mobile);
                _this.createProfileForm.controls['restaurant_other_email'].setValue(_this.restaurantInfo.email);
                _this.createProfileForm.controls['website'].setValue(_this.restaurantInfo.website);
                _this.createProfileForm.controls['promotion'].setValue(_this.restaurantInfo.promotion);
                _this.createProfileForm.controls['promo_discount'].setValue(_this.restaurantInfo.promo_discount);
                _this.createProfileForm.controls['facebook_link'].setValue(_this.restaurantInfo.facebook_link);
                _this.createProfileForm.controls['twitter_link'].setValue(_this.restaurantInfo.twitter_link);
                _this.createProfileForm.controls['linkedIn_link'].setValue(_this.restaurantInfo.linkedIn_link);
                _this.createProfileForm.controls['google_plus_link'].setValue(_this.restaurantInfo.google_plus_link);
                _this.createProfileForm.controls['rest_type'].setValue(_this.restaurantInfo.rest_type);
                _this.restaurant_name = _this.createProfileForm.controls['restaurant_name'];
                _this.restaurant_address = _this.createProfileForm.controls['restaurant_address'];
                _this.restaurant_tel_number = _this.createProfileForm.controls['restaurant_tel_number'];
                _this.restaurant_mobile_number = _this.createProfileForm.controls['restaurant_mobile_number'];
                _this.restaurant_other_email = _this.createProfileForm.controls['restaurant_other_email'];
                _this.restaurant_other_email = _this.createProfileForm.controls['restaurant_other_email'];
                _this.promotion = _this.createProfileForm.controls['promotion'];
                _this.promo_discount = _this.createProfileForm.controls['promo_discount'];
                _this.facebook_link = _this.createProfileForm.controls['facebook_link'];
                _this.twitter_link = _this.createProfileForm.controls['twitter_link'];
                _this.linkedIn_link = _this.createProfileForm.controls['linkedIn_link'];
                _this.google_plus_link = _this.createProfileForm.controls['google_plus_link'];
                _this.rest_type = _this.createProfileForm.controls['rest_type'];
                _this.lat = _this.restaurantInfo.latitue;
                _this.lon = _this.restaurantInfo.longtitue;
                _this.imageSrc = _this.restaurantInfo.logo;
                if (_this.imageSrc == null) {
                    _this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                }
                _this.fetchCode(3);
                var tg = _this.restaurantInfo.open_time.match(/(\d+)\:(\d+)\:(\d+)/);
                var ng = new Date();
                ng.setHours(tg[1]);
                ng.setMinutes(tg[2]);
                ng.setSeconds(tg[3]);
                _this.createProfileForm.controls['open_time'].setValue(ng);
                _this.open_time = _this.createProfileForm.controls['open_time'];
                var tg2 = _this.restaurantInfo.close_time.match(/(\d+)\:(\d+)\:(\d+)/);
                var ng2 = new Date();
                ng2.setHours(tg2[1]);
                ng2.setMinutes(tg2[2]);
                ng2.setSeconds(tg2[3]);
                _this.createProfileForm.controls['close_time'].setValue(ng2);
                _this.close_time = _this.createProfileForm.controls['close_time'];
                _this.fetchOrderSeetingProfile();
            }
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.fetchOrderSeetingProfile = function () {
        var _this = this;
        this.profileService.getOrderSeetingProfile(this.restId).then(function (response) {
            if (response.ordering_setting != null) {
                _this.orderSetting = response.ordering_setting;
                _this.payment_fee = _this.orderSetting.payment_fee;
                _this.delivery_charge = _this.orderSetting.delivery_charge;
                _this.minimum_order = _this.orderSetting.minimum_order;
                _this.payment_method = _this.orderSetting.payment_method;
                _this.delivery_methods = _this.orderSetting.delivery_methods;
                _this.delivery_time = _this.orderSetting.delivery_time;
                _this.food_ordering_system_type = _this.orderSetting.food_ordering_system_type;
                _this.createProfileForm.controls['default_currency'].setValue(_this.orderSetting.default_currency);
                _this.default_currency = _this.createProfileForm.controls['default_currency'];
                _this.fetchCurrentCyCode(2);
            }
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.buildForm = function () {
        var _this = this;
        this.createProfileForm = this.fb.group({
            'first_name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                ]
            ], 'last_name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256)
                ]
            ], 'email': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256),
                    email_validator_directive_1.invalidEmailValidator()
                ]
            ], 'phone': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(32),
                    number_validator_directive_1.invalidNumberValidator()
                ]
            ], 'tel': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(32)
                ]
            ], 'password': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(6),
                    forms_1.Validators.maxLength(256),
                ]],
            'confirm_password': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(6),
                    forms_1.Validators.maxLength(256),
                ]],
            'restaurant_name': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(256)
                ]
            ],
            'restaurant_address': ['', [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(1024)
                ]
            ], 'restaurant_tel_number': [],
            'restaurant_mobile_number': [],
            'restaurant_other_email': [],
            'open_time': ['', [
                    forms_1.Validators.required
                ]],
            'close_time': ['', [
                    forms_1.Validators.required
                ]],
            'website': ['', [
                    forms_1.Validators.required
                ]],
            'promotion': [],
            'promo_discount': [],
            'default_currency': ['', [
                    forms_1.Validators.required
                ]],
            'rest_type': ['', [
                    forms_1.Validators.required
                ]],
            'facebook_link': [],
            'twitter_link': [],
            'linkedIn_link': [],
            'google_plus_link': []
        });
        this.first_name = this.createProfileForm.controls['first_name'];
        this.last_name = this.createProfileForm.controls['last_name'];
        this.email = this.createProfileForm.controls['email'];
        this.phone = this.createProfileForm.controls['phone'];
        this.tel = this.createProfileForm.controls['tel'];
        this.password = this.createProfileForm.controls['password'];
        this.confirm_password = this.createProfileForm.controls['confirm_password'];
        this.restaurant_name = this.createProfileForm.controls['restaurant_name'];
        this.restaurant_address = this.createProfileForm.controls['restaurant_address'];
        this.restaurant_tel_number = this.createProfileForm.controls['restaurant_tel_number'];
        this.restaurant_mobile_number = this.createProfileForm.controls['restaurant_mobile_number'];
        this.restaurant_other_email = this.createProfileForm.controls['restaurant_other_email'];
        this.open_time = this.createProfileForm.controls['open_time'];
        this.close_time = this.createProfileForm.controls['close_time'];
        this.website = this.createProfileForm.controls['website'];
        this.promotion = this.createProfileForm.controls['promotion'];
        this.promo_discount = this.createProfileForm.controls['promo_discount'];
        this.default_currency = this.createProfileForm.controls['default_currency'];
        this.facebook_link = this.createProfileForm.controls['facebook_link'];
        this.twitter_link = this.createProfileForm.controls['twitter_link'];
        this.linkedIn_link = this.createProfileForm.controls['linkedIn_link'];
        this.google_plus_link = this.createProfileForm.controls['google_plus_link'];
        this.rest_type = this.createProfileForm.controls['rest_type'];
        this.createProfileForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    ProfileComponent.prototype.onValueChanged = function (data) {
        if (!this.createProfileForm) {
            return;
        }
        var form = this.createProfileForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    ProfileComponent.prototype.createProfile = function () {
        var _this = this;
        // this.open_time = this.open_time.value.toLocaleTimeString([], { hour12: false });
        // this.close_time = this.close_time.value.toLocaleTimeString([], { hour12: false });
        // this.fetchLatLon(this.restaurant_address.value);
        this.onValueChanged();
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        var param = {
            'address': this.restaurant_address.value
        };
        this.profileService.getLatlon(param).then(function (response) {
            _this.lat = response.lat_val;
            _this.lon = response.long_val;
            var city = null;
            var street = null;
            var province = null;
            var country = null;
            if (response.city_ln != undefined) {
                city = response.city_ln;
            }
            if (response.street_ln != undefined) {
                street = response.street_ln;
            }
            if (response.province_ln != undefined) {
                province = response.province_ln;
            }
            if (response.country_ln != undefined) {
                country = response.country_ln;
            }
            if (_this.lat == null || _this.lon == null) {
                sweetalert2_1.default('get lat lon no value', _this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
            var object = {
                'user_id': _this.userId,
                'user_info': {
                    'id': _this.userInfo.id,
                    'phone': _this.createProfileForm.value.phone,
                    'email': _this.createProfileForm.value.email,
                    'first_name': _this.createProfileForm.value.first_name,
                    'last_name': _this.createProfileForm.value.last_name,
                    'password': _this.createProfileForm.value.password,
                    'confirm_password': _this.createProfileForm.value.confirm_password,
                    'roles': _this.userInfo.roles,
                    'tel': _this.createProfileForm.value.tel,
                },
                'restaurant_info': {
                    'id': _this.restId,
                    'name': _this.createProfileForm.value.restaurant_name,
                    'address': _this.createProfileForm.value.restaurant_address,
                    'tel': _this.createProfileForm.value.restaurant_tel_number,
                    'mobile': _this.createProfileForm.value.restaurant_mobile_number,
                    'email': _this.createProfileForm.value.restaurant_other_email,
                    'latitue': _this.lat,
                    'longtitue': _this.lon,
                    'website': _this.createProfileForm.value.website,
                    'logo': '',
                    'open_time': _this.open_time.value.toLocaleTimeString([], { hour12: false }),
                    'close_time': _this.close_time.value.toLocaleTimeString([], { hour12: false }),
                    'promotion': _this.createProfileForm.value.promotion,
                    'rest_type': _this.createProfileForm.value.rest_type,
                    'facebook_link': _this.createProfileForm.value.facebook_link,
                    'twitter_link': _this.createProfileForm.value.twitter_link,
                    'linkedIn_link': _this.createProfileForm.value.linkedIn_link,
                    'google_plus_link': _this.createProfileForm.value.google_plus_link,
                    'promo_discount': _this.createProfileForm.value.promo_discount,
                    'city': city,
                    'province': province,
                    'country': country,
                    'street': street,
                    'ordering_setting': {
                        'payment_fee': _this.payment_fee,
                        'delivery_charge': _this.delivery_charge,
                        'minimum_order': _this.minimum_order,
                        'payment_method': _this.payment_method,
                        'delivery_methods': _this.delivery_methods,
                        'delivery_time': _this.delivery_time,
                        'default_currency': _this.createProfileForm.value.default_currency,
                        'food_ordering_system_type': _this.food_ordering_system_type,
                    }
                }
            };
            _this.profileService.createProfile(object).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
        });
    };
    ProfileComponent.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    ProfileComponent.prototype.clearMarkers = function () {
        this.setMapOnAll(null);
    };
    ProfileComponent.prototype.processResult = function (response) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            var imagePost = '';
            if (this.flgImageChoose == true) {
                imagePost = this.imageSrc;
            }
            var profileUpload = {
                'id': response.id,
                'thumb': imagePost
            };
            this.profileService.profileUploadFile(profileUpload).then(function (response) {
                _this.notif.success('New File has been added');
                // this.router.navigateByUrl('');
                _this.clearMarkers();
                _this.userService.logout();
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.error = response.errors;
            if (response.code == 422) {
                if (this.error.first_name) {
                    this.formErrors['first_name'] = this.error.first_name;
                }
                if (this.error.last_name) {
                    this.formErrors['last_name'] = this.error.last_name;
                }
                if (this.error.tel) {
                    this.formErrors['tel'] = this.error.tel;
                }
                if (this.error.phone) {
                    this.formErrors['phone'] = this.error.phone;
                }
                if (this.error.password) {
                    this.formErrors['password'] = this.error.password;
                }
                if (this.error.confirm_password) {
                    this.formErrors['confirm_password'] = this.error.confirm_password;
                }
                if (this.error.name) {
                    this.formErrors['restaurant_name'] = this.error.name;
                }
                if (this.error.restaurant_address) {
                    this.formErrors['address'] = this.error.restaurant_address;
                }
                if (this.error.default_currency) {
                    this.formErrors['default_currency'] = this.error.default_currency;
                }
                if (this.error.website) {
                    this.formErrors['website'] = this.error.website;
                }
                if (this.error.rest_type) {
                    this.formErrors['rest_type'] = this.error.rest_type;
                }
            }
            else {
                sweetalert2_1.default('Create Fail!', this.error[0], 'error').catch(sweetalert2_1.default.noop);
            }
        }
    };
    ProfileComponent.prototype.failedCreate = function (res) {
        if (res.status == 401) {
            // this.loginfailed = true
        }
        else {
            if (res.data.errors.message[0] == 'Email Unverified') {
                // this.unverified = true
            }
            else {
                // other kinds of error returned from server
                for (var error in res.data.errors) {
                    // this.loginfailederror += res.data.errors[error] + ' '
                }
            }
        }
    };
    ProfileComponent.prototype.initMap = function () {
        var latmap = this.lat;
        var lonMap = this.lon;
        this.mapObj = new google.maps.Map(document.getElementById('order-map'), {
            center: { lat: +latmap, lng: +lonMap },
            zoom: 15
        });
        //getCurrentPositionBrower(); // not run un localhost 	
        // set markers current Location and store location for test only start
        var idMarker = this.idmarker;
        var positionCurrent = new google.maps.LatLng(+latmap, +lonMap); //(user used web on DB)
        this.markerCurrentLocation = new google.maps.Marker({
            id: idMarker,
            draggable: true,
            position: positionCurrent,
            map: this.mapObj,
            title: 'Hello World!'
        });
        this.markers[+idMarker] = this.markerCurrentLocation;
        // google.maps.event.addListener(this.mapObj, 'click', function(event) {
        //     alert(event.latLng);
        // });
        var positionMarker = null;
        var self = this;
        google.maps.event.addListener(this.markerCurrentLocation, 'dragend', function (event) {
            // alert(event.latLng +"/"+ this.getPosition());
            positionMarker = this.getPosition();
            self.fetchAddress(this.getPosition().lat(), this.getPosition().lng());
            this.lat = this.getPosition().lat();
            this.long = this.getPosition().lng();
            this.changelonlatFlag = true;
        });
    };
    ProfileComponent.prototype.SearchMapChange = function () {
        this.fetchLatLon(this.restaurant_address.value);
    };
    ProfileComponent.prototype.fetchAddress = function (lat, lon) {
        var _this = this;
        var object = {
            'lat': lat,
            'lon': lon,
        };
        this.profileService.getAddress(object).then(function (response) {
            _this.addressInfo = response;
            _this.createProfileForm.controls['restaurant_address'].setValue(_this.addressInfo.address);
            _this.restaurant_address = _this.createProfileForm.controls['restaurant_address'];
            console.log(response);
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.fetchLatLon = function (address) {
        var _this = this;
        var object = {
            'address': address
        };
        this.profileService.getLatlon(object).then(function (response) {
            _this.lat = response.lat_val;
            _this.lon = response.long_val;
            console.log(response);
            var latmap = _this.lat;
            var lonMap = _this.lon;
            var positionCurrent = new google.maps.LatLng(+latmap, +lonMap); //(user used web on DB)
            var idMarker = +_this.idmarker + 1;
            _this.markerCurrentLocation = new google.maps.Marker({
                draggable: true,
                position: positionCurrent,
                map: _this.mapObj,
                title: 'Hello World!'
            });
            _this.markers[+idMarker] = _this.markerCurrentLocation;
            _this.mapObj.panTo(_this.markerCurrentLocation.position);
            var marker = _this.markers[+_this.idmarker]; // find the marker by given id
            _this.idmarker = idMarker;
            marker.setMap(null);
            // google.maps.event.addListener(this.mapObj, 'click', function(event) {
            //     alert(event.latLng);
            // });
            var positionMarker = null;
            var self = _this;
            google.maps.event.addListener(_this.markerCurrentLocation, 'dragend', function (event) {
                // alert(event.latLng +"/"+ this.getPosition());
                positionMarker = this.getPosition();
                self.fetchAddress(this.getPosition().lat(), this.getPosition().lng());
                this.lat = this.getPosition().lat();
                this.long = this.getPosition().lng();
                this.changelonlatFlag = true;
            });
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.handleLocationError = function (browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    };
    ProfileComponent.prototype.chagetab = function (value) {
        var _this = this;
        if (this.tabChangeFisrt == true) {
            setTimeout(function () {
                _this.initMap();
            }, 800);
            this.tabChangeFisrt = false;
        }
    };
    ProfileComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
    };
    ProfileComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
        this.createProfileForm.controls['rest_type'].setValue(null);
        this.rest_type = this.createProfileForm.controls['rest_type'];
    };
    ProfileComponent.prototype.currencyremoved = function (value) {
        console.log('Removed value is: ', value);
        this.createProfileForm.controls['default_currency'].setValue(null);
        this.default_currency = this.createProfileForm.controls['default_currency'];
    };
    ProfileComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    ProfileComponent.prototype.refreshValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.value = value;
            this.createProfileForm.controls['rest_type'].setValue(value.id);
            this.rest_type = this.createProfileForm.controls['rest_type'];
        }
        else {
            this.createProfileForm.controls['rest_type'].setValue(null);
            this.rest_type = this.createProfileForm.controls['rest_type'];
        }
    };
    ProfileComponent.prototype.refreshCurrencyValue = function (value) {
        // this.valueCurrentcy = value;
        if (value != undefined && value != null && value.length != 0) {
            this.valueCurrentcy = value;
            this.createProfileForm.controls['default_currency'].setValue(value.text);
            this.default_currency = this.createProfileForm.controls['default_currency'];
        }
        else {
            this.createProfileForm.controls['default_currency'].setValue(null);
            this.default_currency = this.createProfileForm.controls['default_currency'];
        }
    };
    ProfileComponent.prototype.onChangeOpen = function (value) {
        console.log('Changed value is: ', value);
    };
    ProfileComponent.prototype.onChangeOpenValue = function (event) {
        var value1 = event.target.value;
        console.log('Changed value is: ', event.target.value);
        if (value1 == "" || value1 == null) {
            this.createProfileForm.controls['open_time'].setValue(null);
            this.open_time = this.createProfileForm.controls['open_time'];
        }
        var nameRe = new RegExp(/^[0-9]+$/i);
        if (!nameRe.test(value1) || value1.length > 2) {
            this.createProfileForm.controls['open_time'].setValue(null);
            this.open_time = this.createProfileForm.controls['open_time'];
        }
    };
    ProfileComponent.prototype.onChangeCloseValue = function (event) {
        var value1 = event.target.value;
        console.log('Changed value is: ', event.target.value);
        if (value1 == "" || value1 == null) {
            this.createProfileForm.controls['close_time'].setValue(null);
            this.close_time = this.createProfileForm.controls['close_time'];
        }
        var nameRe = new RegExp(/^[0-9]+$/i);
        if (!nameRe.test(value1) || value1.length > 2) {
            this.createProfileForm.controls['close_time'].setValue(null);
            this.close_time = this.createProfileForm.controls['close_time'];
        }
    };
    ProfileComponent.prototype.onChangeClose = function (value1) {
        console.log('Changed value is: ', value1);
    };
    ProfileComponent.prototype.handleDragEnter = function () {
        this.dragging = true;
    };
    ProfileComponent.prototype.handleDragLeave = function () {
        this.dragging = false;
    };
    ProfileComponent.prototype.handleDrop = function (e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    };
    ProfileComponent.prototype.handleImageLoad = function () {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if (this.imageLoaded = true) {
            this.hiddenImage = true;
        }
    };
    ProfileComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        this.loaded = false;
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose = true;
    };
    ProfileComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    };
    ProfileComponent.prototype._setActive = function () {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    };
    ProfileComponent.prototype._setInactive = function () {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], ProfileComponent.prototype, "select", void 0);
    __decorate([
        core_1.ViewChild('SelectCurrencyId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], ProfileComponent.prototype, "selectCurrency", void 0);
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'profile',
            styleUrls: [utils_1.default.getView('app/components/profile/profile.component.css')],
            templateUrl: utils_1.default.getView('app/components/profile/profile.component.html'),
            animations: [router_animations_1.routerFade()],
            host: { '[@routerFade]': '' }
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            comCode_service_1.ComCodeService,
            profile_service_1.ProfileService,
            storage_service_1.StorageService,
            user_service_1.UserService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router, router_2.ActivatedRoute])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBOEU7QUFDOUUseUNBQW9DO0FBQ3BDLDZEQUF1RTtBQUV2RSxnRUFBOEQ7QUFDOUQsMERBQXlEO0FBQ3pELDJDQUE4QztBQUM5QywwRUFBd0U7QUFDeEUsMENBQXlDO0FBQ3pDLDBDQUFpRDtBQUNqRCxnRUFBOEQ7QUFDOUQseUNBQXdEO0FBQ3hELG1GQUE4RTtBQUU5RSx3Q0FBNEY7QUFDNUYsaUZBQTRFO0FBQzVFLGdFQUE4RDtBQUM5RCwwREFBd0Q7QUFXeEQ7SUErQ0UsMEJBQ1EsRUFBZSxFQUNoQixjQUE4QixFQUM3QixjQUE4QixFQUM5QixjQUE4QixFQUM5QixXQUF3QixFQUN4QixRQUFxQixFQUNyQixLQUEwQixFQUMxQixNQUFjLEVBQVMsS0FBcUI7UUFQNUMsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNoQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDN0IsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFVBQUssR0FBTCxLQUFLLENBQXFCO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQWpEN0MsbUJBQWMsR0FBWSxJQUFJLENBQUM7UUFxQjlCLGNBQVMsR0FBb0IsSUFBSSxDQUFDO1FBQ2xDLGVBQVUsR0FBb0IsSUFBSSxDQUFDO1FBU25DLFFBQUcsR0FBTSxJQUFJLENBQUM7UUFDZCxRQUFHLEdBQU0sSUFBSSxDQUFDO1FBQ2QscUJBQWdCLEdBQVUsS0FBSyxDQUFDO1FBaUJoQyxVQUFLLEdBQWMsRUFBRSxDQUFDO1FBQ3RCLGtCQUFhLEdBQWMsRUFBRSxDQUFDO1FBQzlCLFlBQU8sR0FBTyxFQUFFLENBQUM7UUFDakIsYUFBUSxHQUFPLENBQUMsQ0FBQztRQXNRM0IsZUFBVSxHQUFHO1lBQ1QsWUFBWSxFQUFFLEVBQUU7WUFDaEIsV0FBVyxFQUFDLEVBQUU7WUFDZCxPQUFPLEVBQUMsRUFBRTtZQUNaLE9BQU8sRUFBQyxFQUFFO1lBQ1IsS0FBSyxFQUFDLEVBQUU7WUFDUixVQUFVLEVBQUMsRUFBRTtZQUNiLGtCQUFrQixFQUFDLEVBQUU7WUFDckIsaUJBQWlCLEVBQUMsRUFBRTtZQUNwQixvQkFBb0IsRUFBQyxFQUFFO1lBQ3ZCLFdBQVcsRUFBQyxFQUFFO1lBQ2QsWUFBWSxFQUFDLEVBQUU7WUFDZixTQUFTLEVBQUMsRUFBRTtZQUNaLGtCQUFrQixFQUFDLEVBQUU7WUFDckIsV0FBVyxFQUFDLEVBQUU7U0FDZixDQUFDO1FBRUYsdUJBQWtCLEdBQUc7WUFDbkIsWUFBWSxFQUFFO2dCQUNaLFVBQVUsRUFBTyx1QkFBdUI7Z0JBQ3hDLFdBQVcsRUFBTSwrQ0FBK0M7YUFDakU7WUFDQSxXQUFXLEVBQUU7Z0JBQ1osVUFBVSxFQUFPLDRCQUE0QjtnQkFDN0MsV0FBVyxFQUFNLG9EQUFvRDthQUN0RTtZQUNBLE9BQU8sRUFBRTtnQkFDUixVQUFVLEVBQU8sd0JBQXdCO2dCQUN6QyxXQUFXLEVBQU0saURBQWlEO2dCQUNsRSxjQUFjLEVBQUcsMENBQTBDO2FBQzVEO1lBQ0EsT0FBTyxFQUFFO2dCQUNSLFVBQVUsRUFBTyx3QkFBd0I7Z0JBQ3pDLFdBQVcsRUFBTSwrQ0FBK0M7Z0JBQ2hFLGVBQWUsRUFBRSx5QkFBeUI7YUFDM0M7WUFDQSxLQUFLLEVBQUU7Z0JBQ04sVUFBVSxFQUFPLHNCQUFzQjtnQkFDdkMsV0FBVyxFQUFNLDZDQUE2QzthQUMvRDtZQUNELFVBQVUsRUFBRTtnQkFDVixVQUFVLEVBQU8sMkJBQTJCO2dCQUM1QyxXQUFXLEVBQU0sa0RBQWtEO2dCQUNuRSxXQUFXLEVBQU0sb0RBQW9EO2FBQ3RFO1lBQ0Qsa0JBQWtCLEVBQUU7Z0JBQ2xCLFVBQVUsRUFBTywyQkFBMkI7Z0JBQzVDLFdBQVcsRUFBTSxrREFBa0Q7Z0JBQ25FLFdBQVcsRUFBTSxvREFBb0Q7YUFDdEU7WUFDQSxpQkFBaUIsRUFBRTtnQkFDbEIsVUFBVSxFQUFPLGtDQUFrQztnQkFDbkQsV0FBVyxFQUFNLDBEQUEwRDthQUM1RTtZQUNBLG9CQUFvQixFQUFFO2dCQUNyQixVQUFVLEVBQU8scUNBQXFDO2dCQUN0RCxXQUFXLEVBQU0sOERBQThEO2FBQ2hGO1lBQ0EsV0FBVyxFQUFFO2dCQUNaLFVBQVUsRUFBTyw0Q0FBNEM7YUFDOUQ7WUFDQSxZQUFZLEVBQUU7Z0JBQ2IsVUFBVSxFQUFPLDRDQUE0QzthQUM5RDtZQUNBLFNBQVMsRUFBRTtnQkFDVixVQUFVLEVBQU8sMEJBQTBCO2FBQzVDO1lBQ0Esa0JBQWtCLEVBQUU7Z0JBQ25CLFVBQVUsRUFBTyxtQ0FBbUM7YUFDckQ7WUFDQSxXQUFXLEVBQUU7Z0JBQ1osVUFBVSxFQUFPLDJCQUEyQjthQUM3QztTQUNGLENBQUM7UUEyU0osZ0JBQWdCO1FBRVIsVUFBSyxHQUFPLEVBQUUsQ0FBQztRQUNmLG1CQUFjLEdBQU8sRUFBRSxDQUFDO1FBQ3RCLGVBQVUsR0FBVSxHQUFHLENBQUM7UUFDeEIsYUFBUSxHQUFXLEtBQUssQ0FBQztRQStFaEMsZUFBZTtRQUNULGlCQUFZLEdBQVcsT0FBTyxDQUFDO1FBQzVCLGNBQVMsR0FBVyxNQUFNLENBQUM7UUFDM0IsaUJBQVksR0FBVyx1QkFBdUIsQ0FBQztRQUUvQyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLFdBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFDN0IsYUFBUSxHQUFXLGlFQUFpRSxDQUFDO1FBQ3JGLG1CQUFjLEdBQVcsS0FBSyxDQUFDO1FBQy9CLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFDdEIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBUyxLQUFLLENBQUM7SUEvdEJvQixDQUFDO0lBS3pELG1DQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDZixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFDLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7UUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7UUFDcEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUl4RCxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUU5QixDQUFDO0lBQ08sb0NBQVMsR0FBakIsVUFBa0IsRUFBRTtRQUFwQixpQkFtQkM7UUFsQkMsSUFBSSxLQUFLLEdBQUc7WUFDQyxVQUFVLEVBQUUsRUFBRTtTQUMxQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUN0QixVQUFBLFFBQVE7WUFDakIsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLElBQUksSUFBRSxJQUFJLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDdkMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO29CQUMxRCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQzt3QkFDRSxFQUFFLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFO3dCQUNsQyxJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO3FCQUMvQixDQUFDLENBQUM7Z0JBQ3BCLENBQUM7Z0JBQ0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQztnQkFDL0IsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0MsQ0FBQztRQUNRLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUNVLDZDQUFrQixHQUExQixVQUEyQixFQUFFO1FBQTdCLGlCQXVCRjtRQXRCQyxJQUFJLEtBQUssR0FBRztZQUNDLFVBQVUsRUFBRSxFQUFFO1NBQzFCLENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQ3RCLFVBQUEsUUFBUTtZQUNqQixFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFFLElBQUksSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNyQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztvQkFDMUQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7d0JBQ04sRUFBRSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDdkIsSUFBSSxFQUFFLEtBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFTO3FCQUNwQyxDQUFDLENBQUM7b0JBQ0gsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssSUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFBLENBQUM7d0JBQ3BELFdBQVcsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDeEMsQ0FBQztnQkFDbEIsQ0FBQztnQkFDRCxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDO2dCQUMvQyxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdkQsQ0FBQztRQUNRLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBSyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUNNLGdEQUFxQixHQUE3QjtRQUFBLGlCQTRERztRQTNEQSxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDcEQsVUFBQSxRQUFRO1lBQ2pCLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxlQUFlLElBQUUsSUFBSSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQzdELEtBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxRQUFRLENBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdkYsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUM5RixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLENBQUMsUUFBUSxDQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVGLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxRQUFRLENBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbEcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsRixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNsRixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN0RixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ2hHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzlGLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsUUFBUSxDQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzVGLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzlGLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLENBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNwRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNuRyxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDekUsS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDaEYsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsQ0FBQztnQkFDdkYsS0FBSSxDQUFDLHdCQUF3QixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsMEJBQTBCLENBQUMsQ0FBQztnQkFDM0YsS0FBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDNUUsS0FBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDeEYsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM5RCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDeEUsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN0RSxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3BFLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDdEUsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDekYsS0FBSSxDQUFDLFNBQVMsR0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMvQyxLQUFJLENBQUMsR0FBRyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDO2dCQUN2QyxLQUFJLENBQUMsR0FBRyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDO2dCQUN6QyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO2dCQUN6QyxFQUFFLENBQUEsQ0FBRSxLQUFJLENBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7b0JBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsaUVBQWlFLENBQUM7Z0JBQ3JGLENBQUM7Z0JBRWQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDTCxJQUFJLEVBQUUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDcEUsSUFBSSxFQUFFLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztnQkFDcEIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzFELEtBQUksQ0FBQyxTQUFTLEdBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFFNUQsSUFBSSxHQUFHLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLENBQUM7Z0JBQ3RFLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7Z0JBQ3JCLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1RCxLQUFJLENBQUMsVUFBVSxHQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzlELEtBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1lBQzlDLENBQUM7UUFDUSxDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDUyxtREFBd0IsR0FBaEM7UUFBQSxpQkFtQkY7UUFsQkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUMzQyxVQUFBLFFBQVE7WUFDTCxFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDbkMsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ3hELEtBQUksQ0FBQyxXQUFXLEdBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7Z0JBQ3BDLEtBQUksQ0FBQyxlQUFlLEdBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUM7Z0JBQ3ZELEtBQUksQ0FBQyxhQUFhLEdBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxjQUFjLEdBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUM7Z0JBQ3JELEtBQUksQ0FBQyxnQkFBZ0IsR0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDO2dCQUN6RCxLQUFJLENBQUMsYUFBYSxHQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDO2dCQUNuRCxLQUFJLENBQUMseUJBQXlCLEdBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyx5QkFBeUIsQ0FBQztnQkFDM0UsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ2pHLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQzVFLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixDQUFDO1FBQ0osQ0FBQyxFQUNkLFVBQUEsS0FBSztZQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBQ00sb0NBQVMsR0FBakI7UUFBQSxpQkFnR0U7UUEvRkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3JDLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDZixrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUI7YUFFRixFQUFDLFdBQVcsRUFBQyxDQUFDLEVBQUUsRUFBRTtvQkFDZixrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUI7YUFDRixFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUUsRUFBRTtvQkFDWCxrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztvQkFDekIsaURBQXFCLEVBQUU7aUJBQ3hCO2FBQ0YsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ1gsa0JBQVUsQ0FBQyxRQUFRO29CQUNuQixrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7b0JBQ3hCLG1EQUFzQixFQUFFO2lCQUN6QjthQUNGLEVBQUMsS0FBSyxFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNULGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO2lCQUN6QjthQUNGLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNkLGtCQUFVLENBQUMsUUFBUTtvQkFDbkIsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUN2QixrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUM7aUJBQzFCLENBQUM7WUFDUCxrQkFBa0IsRUFBQyxDQUFDLEVBQUUsRUFBRTtvQkFDakIsa0JBQVUsQ0FBQyxRQUFRO29CQUNuQixrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUIsQ0FBQztZQUNQLGlCQUFpQixFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNoQixrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztpQkFDMUI7YUFDRjtZQUNKLG9CQUFvQixFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNuQixrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztpQkFDM0I7YUFDRixFQUFDLHVCQUF1QixFQUFDLEVBQUU7WUFDL0IsMEJBQTBCLEVBQUMsRUFBRTtZQUM3Qix3QkFBd0IsRUFBQyxFQUFFO1lBQzNCLFdBQVcsRUFBQyxDQUFDLEVBQUUsRUFBRTtvQkFDVixrQkFBVSxDQUFDLFFBQVE7aUJBQ3BCLENBQUM7WUFDUCxZQUFZLEVBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ1gsa0JBQVUsQ0FBQyxRQUFRO2lCQUNwQixDQUFDO1lBQ0osU0FBUyxFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNYLGtCQUFVLENBQUMsUUFBUTtpQkFDcEIsQ0FBQztZQUNKLFdBQVcsRUFBQyxFQUFFO1lBQ2QsZ0JBQWdCLEVBQUMsRUFBRTtZQUNuQixrQkFBa0IsRUFBQyxDQUFDLEVBQUUsRUFBRTtvQkFDcEIsa0JBQVUsQ0FBQyxRQUFRO2lCQUNwQixDQUFDO1lBQ0osV0FBVyxFQUFDLENBQUMsRUFBRSxFQUFFO29CQUNiLGtCQUFVLENBQUMsUUFBUTtpQkFDcEIsQ0FBQztZQUNKLGVBQWUsRUFBQyxFQUFFO1lBQ2xCLGNBQWMsRUFBQyxFQUFFO1lBQ2pCLGVBQWUsRUFBQyxFQUFFO1lBQ2xCLGtCQUFrQixFQUFDLEVBQUU7U0FDdEIsQ0FBQyxDQUFDO1FBQ0EsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDdEYsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUM1RixJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3JGLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZO2FBQ25DLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQXpCLENBQXlCLENBQUMsQ0FBQztRQUVoRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxrQ0FBa0M7SUFDM0QsQ0FBQztJQUNLLHlDQUFjLEdBQXRCLFVBQXVCLElBQVU7UUFDN0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUN4QyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFFcEMsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDL0MsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxHQUFHLENBQUMsQ0FBQyxJQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUEyRUYsd0NBQWEsR0FBYjtRQUFBLGlCQWdHQztRQS9GQSxtRkFBbUY7UUFDakYscUZBQXFGO1FBQ3JGLG1EQUFtRDtRQUNuRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsd0NBQXdDO1lBQ3ZDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25DLE1BQU0sQ0FBQztZQUNULENBQUM7UUFDSixDQUFDO1FBQ04sSUFBSSxLQUFLLEdBQUc7WUFDRyxTQUFTLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUs7U0FDckQsQ0FBQztRQUNBLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FDL0IsVUFBQSxRQUFRO1lBQ1IsS0FBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO1lBQzVCLEtBQUksQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUM3QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztZQUNwQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDbkIsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBRSxTQUFTLENBQUMsQ0FBQSxDQUFDO2dCQUM5QixJQUFJLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUMxQixDQUFDO1lBQ0EsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBRSxTQUFTLENBQUMsQ0FBQSxDQUFDO2dCQUNqQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztZQUM5QixDQUFDO1lBQ0EsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFdBQVcsSUFBRSxTQUFTLENBQUMsQ0FBQSxDQUFDO2dCQUNuQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztZQUNsQyxDQUFDO1lBQ0EsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLFVBQVUsSUFBRSxTQUFTLENBQUMsQ0FBQSxDQUFDO2dCQUNsQyxPQUFPLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQztZQUNoQyxDQUFDO1lBRUQsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLEdBQUcsSUFBRSxJQUFJLElBQUksS0FBSSxDQUFDLEdBQUcsSUFBRSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUMzQixxQkFBSSxDQUNWLHNCQUFzQixFQUN0QixLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUNiLE9BQU8sQ0FDTixDQUFDLEtBQUssQ0FBQyxxQkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZCLENBQUM7WUFDRCxJQUFJLE1BQU0sR0FBRztnQkFDWCxTQUFTLEVBQUUsS0FBSSxDQUFDLE1BQU07Z0JBQ3RCLFdBQVcsRUFBRTtvQkFDWCxJQUFJLEVBQUUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN0QixPQUFPLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxLQUFLO29CQUMzQyxPQUFPLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxLQUFLO29CQUMzQyxZQUFZLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxVQUFVO29CQUNyRCxXQUFXLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxTQUFTO29CQUNuRCxVQUFVLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxRQUFRO29CQUNqRCxrQkFBa0IsRUFBRSxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLGdCQUFnQjtvQkFDakUsT0FBTyxFQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSztvQkFDM0IsS0FBSyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsR0FBRztpQkFDeEM7Z0JBQ0QsaUJBQWlCLEVBQUM7b0JBQ2hCLElBQUksRUFBRSxLQUFJLENBQUMsTUFBTTtvQkFDakIsTUFBTSxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsZUFBZTtvQkFDcEQsU0FBUyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsa0JBQWtCO29CQUMxRCxLQUFLLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxxQkFBcUI7b0JBQ3pELFFBQVEsRUFBRSxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLHdCQUF3QjtvQkFDL0QsT0FBTyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsc0JBQXNCO29CQUM1RCxTQUFTLEVBQUUsS0FBSSxDQUFDLEdBQUc7b0JBQ25CLFdBQVcsRUFBRSxLQUFJLENBQUMsR0FBRztvQkFDckIsU0FBUyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTztvQkFDL0MsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsV0FBVyxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQztvQkFDM0UsWUFBWSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQztvQkFDN0UsV0FBVyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsU0FBUztvQkFDbkQsV0FBVyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsU0FBUztvQkFDbkQsZUFBZSxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsYUFBYTtvQkFDM0QsY0FBYyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsWUFBWTtvQkFDekQsZUFBZSxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsYUFBYTtvQkFDM0Qsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxnQkFBZ0I7b0JBQ2pFLGdCQUFnQixFQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsY0FBYztvQkFDNUQsTUFBTSxFQUFDLElBQUk7b0JBQ1gsVUFBVSxFQUFDLFFBQVE7b0JBQ25CLFNBQVMsRUFBQyxPQUFPO29CQUNqQixRQUFRLEVBQUMsTUFBTTtvQkFDZixrQkFBa0IsRUFBQzt3QkFDakIsYUFBYSxFQUFFLEtBQUksQ0FBQyxXQUFXO3dCQUMvQixpQkFBaUIsRUFBRSxLQUFJLENBQUMsZUFBZTt3QkFDdkMsZUFBZSxFQUFFLEtBQUksQ0FBQyxhQUFhO3dCQUNuQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsY0FBYzt3QkFDckMsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLGdCQUFnQjt3QkFDekMsZUFBZSxFQUFFLEtBQUksQ0FBQyxhQUFhO3dCQUNuQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLGdCQUFnQjt3QkFDakUsMkJBQTJCLEVBQUUsS0FBSSxDQUFDLHlCQUF5QjtxQkFDNUQ7aUJBQ1I7YUFDUixDQUFDO1lBQ0EsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUMzQixVQUFBLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEVBQTVCLENBQTRCLEVBQzNDLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLENBQUMsQ0FBQztRQUN2RCxDQUFDLENBQUMsQ0FBQztJQUVULENBQUM7SUFDQSxzQ0FBVyxHQUFYLFVBQVksR0FBRztRQUNULEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QixDQUFDO0lBQ0gsQ0FBQztJQUNMLHVDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFDTyx3Q0FBYSxHQUFyQixVQUFzQixRQUFRO1FBQTlCLGlCQWtFQztRQWhFQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLFNBQVMsSUFBSSxRQUFRLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLEVBQUUsQ0FBQSxDQUFFLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDL0IsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDNUIsQ0FBQztZQUNBLElBQUksYUFBYSxHQUFHO2dCQUNqQixJQUFJLEVBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2hCLE9BQU8sRUFBQyxTQUFTO2FBQ25CLENBQUE7WUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FDekMsVUFBQSxRQUFRO2dCQUNQLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUM7Z0JBQzlDLGlDQUFpQztnQkFDakMsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUNwQixLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzNCLENBQUMsRUFDZCxVQUFBLEtBQUs7Z0JBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUU3QixDQUFDLENBQUMsQ0FBQztRQUNOLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUM3QixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztnQkFDeEQsQ0FBQztnQkFDQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ3RELENBQUM7Z0JBQ0EsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO2dCQUMxQyxDQUFDO2dCQUNBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztnQkFDOUMsQ0FBQztnQkFDQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7Z0JBQ3BELENBQUM7Z0JBQ0EsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDO2dCQUNwRSxDQUFDO2dCQUNBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUN2RCxDQUFDO2dCQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO29CQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUM7Z0JBQzdELENBQUM7Z0JBQ0EsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDO2dCQUNwRSxDQUFDO2dCQUNBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztnQkFDbEQsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ3RELENBQUM7WUFFSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04scUJBQUksQ0FDRixjQUFjLEVBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFDTyx1Q0FBWSxHQUFwQixVQUFzQixHQUFHO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0QiwwQkFBMEI7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQztnQkFDckQseUJBQXlCO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTiw0Q0FBNEM7Z0JBQzVDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsd0RBQXdEO2dCQUMxRCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRUQsa0NBQU8sR0FBUDtRQUNBLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDdEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUN2QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUN0RSxNQUFNLEVBQUUsRUFBQyxHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFDO1lBQ3BDLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQyxDQUFDO1FBR0gsdURBQXVEO1FBQ3ZELHNFQUFzRTtRQUN0RSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzdCLElBQUksZUFBZSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFBLHVCQUF1QjtRQUNyRixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUMvQyxFQUFFLEVBQUMsUUFBUTtZQUNkLFNBQVMsRUFBRSxJQUFJO1lBQ2IsUUFBUSxFQUFFLGVBQWU7WUFDekIsR0FBRyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ2YsS0FBSyxFQUFFLGNBQWM7U0FDdEIsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBRTtRQUN2RCx3RUFBd0U7UUFDeEUsMkJBQTJCO1FBQzNCLE1BQU07UUFDTixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsU0FBUyxFQUFFLFVBQVMsS0FBSztZQUMxRSxnREFBZ0Q7WUFDbEQsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNsQyxJQUFJLENBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUMsSUFBSSxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO0lBRWYsQ0FBQztJQUNELDBDQUFlLEdBQWY7UUFDRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ08sdUNBQVksR0FBcEIsVUFBcUIsR0FBRyxFQUFDLEdBQUc7UUFBNUIsaUJBY0k7UUFiRCxJQUFJLE1BQU0sR0FBRztZQUNBLEtBQUssRUFBRSxHQUFHO1lBQ1YsS0FBSyxFQUFFLEdBQUc7U0FDdkIsQ0FBQztRQUNBLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDMUIsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUM7WUFDNUIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLFFBQVEsQ0FBRSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFGLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDaEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDUSxzQ0FBVyxHQUFuQixVQUFvQixPQUFPO1FBQTNCLGlCQXdDQTtRQXZDRCxJQUFJLE1BQU0sR0FBRztZQUNBLFNBQVMsRUFBRSxPQUFPO1NBQy9CLENBQUM7UUFDQSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3pCLFVBQUEsUUFBUTtZQUNSLEtBQUksQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUM1QixLQUFJLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QixJQUFJLE1BQU0sR0FBRyxLQUFJLENBQUMsR0FBRyxDQUFDO1lBQ3RCLElBQUksTUFBTSxHQUFHLEtBQUksQ0FBQyxHQUFHLENBQUM7WUFDdEIsSUFBSSxlQUFlLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sRUFBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUEsdUJBQXVCO1lBQ3JGLElBQUksUUFBUSxHQUFHLENBQUMsS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDbkMsS0FBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQ3BELFNBQVMsRUFBRSxJQUFJO2dCQUNiLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixHQUFHLEVBQUUsS0FBSSxDQUFDLE1BQU07Z0JBQ2YsS0FBSyxFQUFFLGNBQWM7YUFDdEIsQ0FBQyxDQUFDO1lBQ04sS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUksQ0FBQyxxQkFBcUIsQ0FBRTtZQUNyRCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdkQsSUFBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLDhCQUE4QjtZQUN6RSxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUN6QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JCLHdFQUF3RTtZQUN4RSwyQkFBMkI7WUFDM0IsTUFBTTtZQUNOLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQztZQUMxQixJQUFJLElBQUksR0FBRyxLQUFJLENBQUM7WUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxTQUFTLEVBQUUsVUFBUyxLQUFLO2dCQUM1RixnREFBZ0Q7Z0JBQzlDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUM3RCxJQUFJLENBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDbEMsSUFBSSxDQUFDLElBQUksR0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxnQkFBZ0IsR0FBQyxJQUFJLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQUM7UUFDRixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSiw4Q0FBbUIsR0FBbkIsVUFBb0IscUJBQXFCLEVBQUUsVUFBVSxFQUFFLEdBQUc7UUFDekQsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixVQUFVLENBQUMsVUFBVSxDQUFDLHFCQUFxQjtZQUNyQix3Q0FBd0M7WUFDeEMsbURBQW1ELENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBQ0QsbUNBQVEsR0FBUixVQUFTLEtBQUs7UUFBZCxpQkFPQztRQU5BLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUMxQixVQUFVLENBQUM7Z0JBQ2IsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ1gsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1osSUFBSSxDQUFDLGNBQWMsR0FBQyxLQUFLLENBQUM7UUFDNUIsQ0FBQztJQUNGLENBQUM7SUFVUSxtQ0FBUSxHQUFmLFVBQWdCLEtBQVM7UUFFdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU0sa0NBQU8sR0FBZCxVQUFlLEtBQVM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUNLLDBDQUFlLEdBQXRCLFVBQXVCLEtBQVM7UUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUNNLGdDQUFLLEdBQVosVUFBYSxLQUFTO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLHVDQUFZLEdBQW5CLFVBQW9CLEtBQVM7UUFDM0IsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLFNBQVMsSUFBSSxLQUFLLElBQUUsSUFBSSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNuQixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzlELENBQUM7UUFBQSxJQUFJLENBQUEsQ0FBQztZQUNKLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoRSxDQUFDO0lBRUgsQ0FBQztJQUVNLCtDQUFvQixHQUEzQixVQUE0QixLQUFTO1FBQ25DLCtCQUErQjtRQUM5QixFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUUsU0FBUyxJQUFJLEtBQUssSUFBRSxJQUFJLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3RELElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzVCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDN0UsQ0FBQztRQUFBLElBQUksQ0FBQSxDQUFDO1lBQ0osSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzlFLENBQUM7SUFFSixDQUFDO0lBQ08sdUNBQVksR0FBbkIsVUFBb0IsS0FBVTtRQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDTyw0Q0FBaUIsR0FBeEIsVUFBeUIsS0FBVTtRQUNsQyxJQUFJLE1BQU0sR0FBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEQsRUFBRSxDQUFBLENBQUMsTUFBTSxJQUFFLEVBQUUsSUFBRyxNQUFNLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUM1QixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsU0FBUyxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDL0QsQ0FBQztRQUNELElBQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLEVBQUUsQ0FBQSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDMUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9ELENBQUM7SUFDSCxDQUFDO0lBQ00sNkNBQWtCLEdBQXpCLFVBQTBCLEtBQVU7UUFDbEMsSUFBSSxNQUFNLEdBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RELEVBQUUsQ0FBQSxDQUFDLE1BQU0sSUFBRSxFQUFFLElBQUcsTUFBTSxJQUFFLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDNUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLFVBQVUsR0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2pFLENBQUM7UUFDRCxJQUFNLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN2QyxFQUFFLENBQUEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQzFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxVQUFVLEdBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRSxDQUFDO0lBRUgsQ0FBQztJQUNNLHdDQUFhLEdBQXBCLFVBQXFCLE1BQVc7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBbUJDLDBDQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQsMENBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRCxxQ0FBVSxHQUFWLFVBQVcsQ0FBQztRQUNSLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELDBDQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLENBQUM7SUFDQyxDQUFDO0lBRUQsNENBQWlCLEdBQWpCLFVBQWtCLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVwQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFFLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQsOENBQW1CLEdBQW5CLFVBQW9CLENBQUM7UUFDakIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELHFDQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFRCx1Q0FBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3BDLENBQUM7SUFDTCxDQUFDO0lBcDFCb0I7UUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7a0NBQWdCLDRCQUFlO29EQUFDO0lBQ3ZCO1FBQTlCLGdCQUFTLENBQUMsa0JBQWtCLENBQUM7a0NBQXdCLDRCQUFlOzREQUFDO0lBRjNELGdCQUFnQjtRQVA1QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFNBQVM7WUFDbkIsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQzFFLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLCtDQUErQyxDQUFDO1lBQzVFLFVBQVUsRUFBRSxDQUFDLDhCQUFVLEVBQUUsQ0FBQztZQUN6QixJQUFJLEVBQUUsRUFBQyxlQUFlLEVBQUUsRUFBRSxFQUFDO1NBQzVCLENBQUM7eUNBaURZLG1CQUFXO1lBQ0EsZ0NBQWM7WUFDYixnQ0FBYztZQUNkLGdDQUFjO1lBQ2pCLDBCQUFXO1lBQ2QsMEJBQVc7WUFDZCwwQ0FBbUI7WUFDbEIsZUFBTSxFQUFnQix1QkFBYztPQXZEekMsZ0JBQWdCLENBdTFCNUI7SUFBRCx1QkFBQztDQXYxQkQsQUF1MUJDLElBQUE7QUF2MUJZLDRDQUFnQiIsImZpbGUiOiJhcHAvY29tcG9uZW50cy9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsVmlld0NoaWxkLFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IHJvdXRlckZhZGUsIHJvdXRlclRyYW5zaXRpb24gfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvZmlsZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvcHJvZmlsZS5zZXJ2aWNlJztcbmltcG9ydCAgeyBBdXRoU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgZGVmYXVsdCBhcyBzd2FsIH0gZnJvbSAnc3dlZXRhbGVydDInO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlJztcbmltcG9ydCB7U2VsZWN0TW9kdWxlLFNlbGVjdENvbXBvbmVudH0gZnJvbSAnbmcyLXNlbGVjdCc7XG5pbXBvcnQgeyBpbnZhbGlkTnVtYmVyVmFsaWRhdG9yIH0gZnJvbSBcImFwcC91dGlscy9udW1iZXItdmFsaWRhdG9yLmRpcmVjdGl2ZVwiO1xuaW1wb3J0IHsgTGltaXRUb0RpcmVjdGl2ZSB9IGZyb20gXCJhcHAvd2lkZ2V0cy9saW1pdC10by5kaXJlY3RpdmVcIjtcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMsIE5nRm9ybSAsQWJzdHJhY3RDb250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBpbnZhbGlkRW1haWxWYWxpZGF0b3IgfSBmcm9tIFwiYXBwL3V0aWxzL2VtYWlsLXZhbGlkYXRvci5kaXJlY3RpdmVcIjtcbmltcG9ydCB7IENvbUNvZGVTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2NvbUNvZGUuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgVGltZXBpY2tlck1vZHVsZSxUaW1lcGlja2VyQ29tcG9uZW50IH0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyBFTlZJUk9OTUVOVCB9IGZyb20gJy4uLy4uLy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XG5kZWNsYXJlIHZhciBnb29nbGU6IGFueTtcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Byb2ZpbGUnLCBcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MnKV0sXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50Lmh0bWwnKSxcblx0YW5pbWF0aW9uczogW3JvdXRlckZhZGUoKV0sXG4gIGhvc3Q6IHsnW0Byb3V0ZXJGYWRlXSc6ICcnfVxufSlcbmV4cG9ydCBjbGFzcyBQcm9maWxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQFZpZXdDaGlsZCgnU2VsZWN0SWQnKSBwdWJsaWMgc2VsZWN0OiBTZWxlY3RDb21wb25lbnQ7XG4gIEBWaWV3Q2hpbGQoJ1NlbGVjdEN1cnJlbmN5SWQnKSBwdWJsaWMgc2VsZWN0Q3VycmVuY3k6IFNlbGVjdENvbXBvbmVudDtcbiAgcHJpdmF0ZSBtYXBPYmo6IGFueTtcbiAgcHJpdmF0ZSBtYXJrZXJDdXJyZW50TG9jYXRpb246IGFueTtcbiAgcHJpdmF0ZSBtYXJrZXJTdG9yZUxvY2F0aW9uOiBhbnk7IFxuXHRwcml2YXRlIHRhYkNoYW5nZUZpc3J0OiBCb29sZWFuID0gdHJ1ZTtcblx0cHJpdmF0ZSB1c2VySWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgcmVzdElkIDogU3RyaW5nO1xuXHRwcml2YXRlIHVzZXJJbmZvOiBhbnk7IFxuXHRwcml2YXRlIGNyZWF0ZVByb2ZpbGVGb3JtOiBGb3JtR3JvdXA7XG5cdHByaXZhdGUgZmlyc3RfbmFtZTogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIGxhc3RfbmFtZTogQWJzdHJhY3RDb250cm9sO1xuXHRwcml2YXRlIGVtYWlsOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgcGhvbmU6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSB0ZWw6IEFic3RyYWN0Q29udHJvbDtcblx0cHJpdmF0ZSBwYXNzd29yZDogQWJzdHJhY3RDb250cm9sO1xuICBwcml2YXRlIGNvbmZpcm1fcGFzc3dvcmQ6IEFic3RyYWN0Q29udHJvbDtcblx0cHJpdmF0ZSByZXN0YXVyYW50X25hbWU6IEFic3RyYWN0Q29udHJvbDtcblx0cHJpdmF0ZSByZXN0YXVyYW50X2FkZHJlc3M6IEFic3RyYWN0Q29udHJvbDtcblx0cHJpdmF0ZSByZXN0YXVyYW50X3RlbF9udW1iZXI6IEFic3RyYWN0Q29udHJvbDtcblx0cHJpdmF0ZSByZXN0YXVyYW50X21vYmlsZV9udW1iZXI6IEFic3RyYWN0Q29udHJvbDtcblx0cHJpdmF0ZSByZXN0YXVyYW50X290aGVyX2VtYWlsOiBBYnN0cmFjdENvbnRyb2w7XG5cdHByaXZhdGUgcmVzdF90eXBlOiBBYnN0cmFjdENvbnRyb2w7XG5cdHByaXZhdGUgYWRkcmVzc0luZm86IGFueTtcblx0cHJpdmF0ZSByZXN0YXVyYW50SW5mbyA6IGFueTtcbiAgcHJpdmF0ZSBvcmRlclNldHRpbmcgOiBhbnk7XG4gIHByaXZhdGUgb3Blbl90aW1lOiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIGNsb3NlX3RpbWU6IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgd2Vic2l0ZTpBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgcHJvbW90aW9uOkFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBwcm9tb19kaXNjb3VudDpBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgZGVmYXVsdF9jdXJyZW5jeTpBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgZmFjZWJvb2tfbGluazpBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgdHdpdHRlcl9saW5rOkFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBsaW5rZWRJbl9saW5rOkFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBnb29nbGVfcGx1c19saW5rOkFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBsYXQ6IGFueT1udWxsO1xuICBwcml2YXRlIGxvbjogYW55PW51bGw7XG4gIHByaXZhdGUgY2hhbmdlbG9ubGF0RmxhZzogQm9vbGVhbj1mYWxzZTtcbiAgcHJpdmF0ZSBwYXltZW50X2ZlZTogYW55O1xuICBwcml2YXRlIGRlbGl2ZXJ5X2NoYXJnZTogYW55O1xuICBwcml2YXRlIG1pbmltdW1fb3JkZXI6IGFueTtcbiAgcHJpdmF0ZSBwYXltZW50X21ldGhvZDogYW55O1xuICBwcml2YXRlIGRlbGl2ZXJ5X21ldGhvZHM6IGFueTtcbiAgcHJpdmF0ZSBkZWxpdmVyeV90aW1lOiBhbnk7XG4gIHByaXZhdGUgZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZTogYW55O1xuICBjb25zdHJ1Y3RvcihcbiAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG5cdHByaXZhdGUgY29tQ29kZVNlcnZpY2U6IENvbUNvZGVTZXJ2aWNlLCBcbiAgcHJpdmF0ZSBwcm9maWxlU2VydmljZTogUHJvZmlsZVNlcnZpY2UsIFxuICBwcml2YXRlIFN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSwgXG4gIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG4gIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIscHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHsgfVxuICBwcml2YXRlIGl0ZW1zOkFycmF5PGFueT4gPSBbXTtcbiAgcHJpdmF0ZSBpdGVtQ3VycmVuY3lzOkFycmF5PGFueT4gPSBbXTtcbiAgcHJpdmF0ZSBtYXJrZXJzOmFueSA9IHt9O1xuICBwcml2YXRlIGlkbWFya2VyOmFueSA9IDA7XG4gIG5nT25Jbml0KCkge1xuXHRcdCB0aGlzLmJ1aWxkRm9ybSgpO1xuICAgICBsZXQgdXNlckxvZ2luID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XG4gICAgIHRoaXMucmVzdElkID0gdXNlckxvZ2luLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgICB0aGlzLnVzZXJJZCA9IHVzZXJMb2dpbi51c2VyX2luZm8uaWQ7XG5cdFx0IGNvbnNvbGUubG9nKHVzZXJMb2dpbik7XG5cdFx0IHRoaXMudXNlckluZm8gPSB1c2VyTG9naW4udXNlcl9pbmZvO1xuXHRcdCB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydmaXJzdF9uYW1lJ10uc2V0VmFsdWUoIHRoaXMudXNlckluZm8uZmlyc3RfbmFtZSk7XG5cdFx0IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2xhc3RfbmFtZSddLnNldFZhbHVlKCB0aGlzLnVzZXJJbmZvLmxhc3RfbmFtZSk7XG5cdFx0IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2VtYWlsJ10uc2V0VmFsdWUoIHRoaXMudXNlckluZm8uZW1haWwpO1xuXHRcdCB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydwaG9uZSddLnNldFZhbHVlKCB0aGlzLnVzZXJJbmZvLnBob25lKTtcbiAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sndGVsJ10uc2V0VmFsdWUoIHRoaXMudXNlckluZm8udGVsKTtcblx0XHQgdGhpcy5maXJzdF9uYW1lID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snZmlyc3RfbmFtZSddO1xuICAgICB0aGlzLmxhc3RfbmFtZSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2xhc3RfbmFtZSddO1xuXHRcdCB0aGlzLmVtYWlsID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snZW1haWwnXTtcbiAgICAgdGhpcy5waG9uZSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Bob25lJ107XG4gICAgIFxuXG5cblx0XHQgdGhpcy5mZXRjaFByb2ZpbGVSZXRhdXJhbnQoKTtcbiAgICAgIFxuICB9XG5cdFx0cHJpdmF0ZSBmZXRjaENvZGUoY2QpIHtcblx0XHRcdFx0bGV0IHBhcmFtID0ge1xuXHRcdFx0ICAgICAgICAgICAgICAnY2RfZ3JvdXAnOiBjZFxuXHRcdFx0XHR9O1xuXHRcdFx0XHR0aGlzLmNvbUNvZGVTZXJ2aWNlLmdldENvZGUocGFyYW0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdCBpZihyZXNwb25zZS5jb2RlIT1udWxsICYmIHJlc3BvbnNlLmNvZGUubGVuZ3RoPjApe1xuXHQgICAgICAgICAgICAgICAgICAgICBcdGZvciAodmFyIGkgPSAwOyBpIDwgIHJlc3BvbnNlLmNvZGUubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0IHRoaXMuaXRlbXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiByZXNwb25zZS5jb2RlW2ldLmNkLnRvU3RyaW5nKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IHJlc3BvbnNlLmNvZGVbaV0uY2RfbmFtZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0aGlzLnNlbGVjdC5pdGVtcyA9IHRoaXMuaXRlbXM7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMudmFsdWUgPSB0aGlzLml0ZW1zW3RoaXMucmVzdF90eXBlLnZhbHVlXTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgfVxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcblx0XHR9XG4gICAgXHRwcml2YXRlIGZldGNoQ3VycmVudEN5Q29kZShjZCkge1xuXHRcdFx0XHRsZXQgcGFyYW0gPSB7XG5cdFx0XHQgICAgICAgICAgICAgICdjZF9ncm91cCc6IGNkXG5cdFx0XHRcdH07XG5cdFx0XHRcdHRoaXMuY29tQ29kZVNlcnZpY2UuZ2V0Q29kZShwYXJhbSkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0IGlmKHJlc3BvbnNlLmNvZGUhPW51bGwgJiYgcmVzcG9uc2UuY29kZS5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNkQ3VycmVudGN5ID0gbnVsbDtcblx0ICAgICAgICAgICAgICAgICAgICAgXHRmb3IgKHZhciBpID0gMDsgaSA8ICByZXNwb25zZS5jb2RlLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCB0aGlzLml0ZW1DdXJyZW5jeXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiByZXNwb25zZS5jb2RlW2ldLmNkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBgJHtyZXNwb25zZS5jb2RlW2ldLmNkX25hbWV9YFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLmRlZmF1bHRfY3VycmVuY3kudmFsdWU9PXJlc3BvbnNlLmNvZGVbaV0uY2RfbmFtZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZEN1cnJlbnRjeSA9IHJlc3BvbnNlLmNvZGVbaV0uY2Q7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0aGlzLnNlbGVjdEN1cnJlbmN5Lml0ZW1zID0gdGhpcy5pdGVtQ3VycmVuY3lzO1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0aGlzLnZhbHVlQ3VycmVudGN5ID0gdGhpcy5pdGVtQ3VycmVuY3lzW2NkQ3VycmVudGN5XTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgfVxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcblx0XHR9XG5cdHByaXZhdGUgZmV0Y2hQcm9maWxlUmV0YXVyYW50KCkge1xuICBcdFx0dGhpcy5wcm9maWxlU2VydmljZS5nZXRQcm9maWxlUmV0YXVyYW50KHRoaXMucmVzdElkLHRoaXMudXNlcklkKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0IGlmKHJlc3BvbnNlLnJlc3RhdXJhbnRfaW5mbyE9bnVsbCAmJiByZXNwb25zZS5yZXN0YXVyYW50X2luZm8ubGVuZ3RoPjApe1xuXHQgICAgICAgICAgICAgICAgICAgICBcdHRoaXMucmVzdGF1cmFudEluZm8gPSByZXNwb25zZS5yZXN0YXVyYW50X2luZm9bMF07XG5cdCAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2codGhpcy5yZXN0YXVyYW50SW5mbyk7IFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0ICB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydyZXN0YXVyYW50X25hbWUnXS5zZXRWYWx1ZSggdGhpcy5yZXN0YXVyYW50SW5mby5uYW1lKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9hZGRyZXNzJ10uc2V0VmFsdWUoIHRoaXMucmVzdGF1cmFudEluZm8uYWRkcmVzcyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Jlc3RhdXJhbnRfdGVsX251bWJlciddLnNldFZhbHVlKCB0aGlzLnJlc3RhdXJhbnRJbmZvLnRlbCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Jlc3RhdXJhbnRfbW9iaWxlX251bWJlciddLnNldFZhbHVlKCB0aGlzLnJlc3RhdXJhbnRJbmZvLm1vYmlsZSk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Jlc3RhdXJhbnRfb3RoZXJfZW1haWwnXS5zZXRWYWx1ZSggdGhpcy5yZXN0YXVyYW50SW5mby5lbWFpbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3dlYnNpdGUnXS5zZXRWYWx1ZSggdGhpcy5yZXN0YXVyYW50SW5mby53ZWJzaXRlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncHJvbW90aW9uJ10uc2V0VmFsdWUoIHRoaXMucmVzdGF1cmFudEluZm8ucHJvbW90aW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncHJvbW9fZGlzY291bnQnXS5zZXRWYWx1ZSggdGhpcy5yZXN0YXVyYW50SW5mby5wcm9tb19kaXNjb3VudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2ZhY2Vib29rX2xpbmsnXS5zZXRWYWx1ZSggdGhpcy5yZXN0YXVyYW50SW5mby5mYWNlYm9va19saW5rKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sndHdpdHRlcl9saW5rJ10uc2V0VmFsdWUoIHRoaXMucmVzdGF1cmFudEluZm8udHdpdHRlcl9saW5rKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snbGlua2VkSW5fbGluayddLnNldFZhbHVlKCB0aGlzLnJlc3RhdXJhbnRJbmZvLmxpbmtlZEluX2xpbmspO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydnb29nbGVfcGx1c19saW5rJ10uc2V0VmFsdWUoIHRoaXMucmVzdGF1cmFudEluZm8uZ29vZ2xlX3BsdXNfbGluayk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Jlc3RfdHlwZSddLnNldFZhbHVlKCB0aGlzLnJlc3RhdXJhbnRJbmZvLnJlc3RfdHlwZSk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMucmVzdGF1cmFudF9uYW1lID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9uYW1lJ107XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgIHRoaXMucmVzdGF1cmFudF9hZGRyZXNzID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9hZGRyZXNzJ107XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgIHRoaXMucmVzdGF1cmFudF90ZWxfbnVtYmVyID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF90ZWxfbnVtYmVyJ107XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMucmVzdGF1cmFudF9tb2JpbGVfbnVtYmVyID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9tb2JpbGVfbnVtYmVyJ107XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgIHRoaXMucmVzdGF1cmFudF9vdGhlcl9lbWFpbCA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Jlc3RhdXJhbnRfb3RoZXJfZW1haWwnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXN0YXVyYW50X290aGVyX2VtYWlsID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9vdGhlcl9lbWFpbCddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb21vdGlvbiA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Byb21vdGlvbiddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb21vX2Rpc2NvdW50ID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncHJvbW9fZGlzY291bnQnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mYWNlYm9va19saW5rID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snZmFjZWJvb2tfbGluayddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnR3aXR0ZXJfbGluayA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3R3aXR0ZXJfbGluayddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxpbmtlZEluX2xpbmsgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydsaW5rZWRJbl9saW5rJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ29vZ2xlX3BsdXNfbGluayA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2dvb2dsZV9wbHVzX2xpbmsnXTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhpcy5yZXN0X3R5cGU9dGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdF90eXBlJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGF0ID0gdGhpcy5yZXN0YXVyYW50SW5mby5sYXRpdHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvbiA9IHRoaXMucmVzdGF1cmFudEluZm8ubG9uZ3RpdHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlU3JjID0gdGhpcy5yZXN0YXVyYW50SW5mby5sb2dvOyAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiggdGhpcy5pbWFnZVNyYz09bnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VTcmMgPSAnaHR0cDovL3d3dy5wbGFjZWhvbGQuaXQvMjAweDE1MC9FRkVGRUYvQUFBQUFBJmFtcDt0ZXh0PW5vK2ltYWdlJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuZmV0Y2hDb2RlKDMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgdGcgPSB0aGlzLnJlc3RhdXJhbnRJbmZvLm9wZW5fdGltZS5tYXRjaCgvKFxcZCspXFw6KFxcZCspXFw6KFxcZCspLyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBuZyA9IG5ldyBEYXRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIG5nLnNldEhvdXJzKHRnWzFdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbmcuc2V0TWludXRlcyh0Z1syXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIG5nLnNldFNlY29uZHModGdbM10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydvcGVuX3RpbWUnXS5zZXRWYWx1ZShuZyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3Blbl90aW1lPXRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ29wZW5fdGltZSddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRnMiA9IHRoaXMucmVzdGF1cmFudEluZm8uY2xvc2VfdGltZS5tYXRjaCgvKFxcZCspXFw6KFxcZCspXFw6KFxcZCspLyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBuZzIgPSBuZXcgRGF0ZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICBuZzIuc2V0SG91cnModGcyWzFdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbmcyLnNldE1pbnV0ZXModGcyWzJdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbmcyLnNldFNlY29uZHModGcyWzNdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snY2xvc2VfdGltZSddLnNldFZhbHVlKG5nMik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VfdGltZT10aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydjbG9zZV90aW1lJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmV0Y2hPcmRlclNlZXRpbmdQcm9maWxlKCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0IH1cbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICBcdFx0XHRcdFx0fSk7XG4gIFx0fVxuICAgIFx0cHJpdmF0ZSBmZXRjaE9yZGVyU2VldGluZ1Byb2ZpbGUoKSB7XG5cdFx0XHRcdHRoaXMucHJvZmlsZVNlcnZpY2UuZ2V0T3JkZXJTZWV0aW5nUHJvZmlsZSh0aGlzLnJlc3RJZCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5vcmRlcmluZ19zZXR0aW5nIT1udWxsKXtcblx0ICAgICAgICAgICAgICAgICAgICAgXHQgIHRoaXMub3JkZXJTZXR0aW5nID0gcmVzcG9uc2Uub3JkZXJpbmdfc2V0dGluZztcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgICAgdGhpcy5wYXltZW50X2ZlZT10aGlzLm9yZGVyU2V0dGluZy5wYXltZW50X2ZlZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyeV9jaGFyZ2U9dGhpcy5vcmRlclNldHRpbmcuZGVsaXZlcnlfY2hhcmdlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1pbmltdW1fb3JkZXI9dGhpcy5vcmRlclNldHRpbmcubWluaW11bV9vcmRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wYXltZW50X21ldGhvZD10aGlzLm9yZGVyU2V0dGluZy5wYXltZW50X21ldGhvZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyeV9tZXRob2RzPXRoaXMub3JkZXJTZXR0aW5nLmRlbGl2ZXJ5X21ldGhvZHM7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcnlfdGltZT10aGlzLm9yZGVyU2V0dGluZy5kZWxpdmVyeV90aW1lO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvb2Rfb3JkZXJpbmdfc3lzdGVtX3R5cGU9dGhpcy5vcmRlclNldHRpbmcuZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snZGVmYXVsdF9jdXJyZW5jeSddLnNldFZhbHVlKHRoaXMub3JkZXJTZXR0aW5nLmRlZmF1bHRfY3VycmVuY3kpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlZmF1bHRfY3VycmVuY3kgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydkZWZhdWx0X2N1cnJlbmN5J107XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmV0Y2hDdXJyZW50Q3lDb2RlKDIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcblx0XHR9XG5cdHByaXZhdGUgYnVpbGRGb3JtKCk6IHZvaWQge1xuICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICAgICdmaXJzdF9uYW1lJzogWycnLCBbXG4gICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZCxcbiAgICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTYpLFxuICAgICAgICBdXG5cbiAgICAgIF0sJ2xhc3RfbmFtZSc6WycnLCBbXG4gICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZCxcbiAgICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTYpXG4gICAgICAgIF1cbiAgICAgIF0sJ2VtYWlsJzpbJycsIFtcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICAgIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NiksXG4gICAgICAgICAgaW52YWxpZEVtYWlsVmFsaWRhdG9yKClcbiAgICAgICAgXVxuICAgICAgXSwncGhvbmUnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzIpLFxuICAgICAgICAgIGludmFsaWROdW1iZXJWYWxpZGF0b3IoKVxuICAgICAgICBdXG4gICAgICBdLCd0ZWwnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzIpXG4gICAgICAgIF1cbiAgICAgIF0sJ3Bhc3N3b3JkJzpbJycsIFtcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICAgIFZhbGlkYXRvcnMubWluTGVuZ3RoKDYpLFxuICAgICAgICAgIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NiksXG4gICAgICAgIF1dLFxuXHRcdFx0J2NvbmZpcm1fcGFzc3dvcmQnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5taW5MZW5ndGgoNiksXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU2KSxcbiAgICAgICAgXV0sXG5cdFx0XHQncmVzdGF1cmFudF9uYW1lJzpbJycsIFtcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxuICAgICAgICAgIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDI1NilcbiAgICAgICAgXVxuICAgICAgXSxcblx0XHRcdCdyZXN0YXVyYW50X2FkZHJlc3MnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsXG4gICAgICAgICAgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAyNClcbiAgICAgICAgXVxuICAgICAgXSwncmVzdGF1cmFudF90ZWxfbnVtYmVyJzpbXSxcblx0XHRcdCdyZXN0YXVyYW50X21vYmlsZV9udW1iZXInOltdLFxuXHRcdFx0J3Jlc3RhdXJhbnRfb3RoZXJfZW1haWwnOltdLFxuXHRcdFx0J29wZW5fdGltZSc6WycnLCBbXG4gICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZFxuICAgICAgICBdXSxcblx0XHRcdCdjbG9zZV90aW1lJzpbJycsIFtcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkXG4gICAgICAgIF1dLFxuICAgICAgJ3dlYnNpdGUnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWRcbiAgICAgICAgXV0sXG4gICAgICAncHJvbW90aW9uJzpbXSxcbiAgICAgICdwcm9tb19kaXNjb3VudCc6W10sXG4gICAgICAnZGVmYXVsdF9jdXJyZW5jeSc6WycnLCBbXG4gICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZFxuICAgICAgICBdXSxcbiAgICAgICdyZXN0X3R5cGUnOlsnJywgW1xuICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWRcbiAgICAgICAgXV0sXG4gICAgICAnZmFjZWJvb2tfbGluayc6W10sXG4gICAgICAndHdpdHRlcl9saW5rJzpbXSxcbiAgICAgICdsaW5rZWRJbl9saW5rJzpbXSxcbiAgICAgICdnb29nbGVfcGx1c19saW5rJzpbXVxuICAgIH0pO1xuICAgICAgIHRoaXMuZmlyc3RfbmFtZSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2ZpcnN0X25hbWUnXTtcbiAgICAgICB0aGlzLmxhc3RfbmFtZSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2xhc3RfbmFtZSddO1xuXHRcdFx0IHRoaXMuZW1haWwgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydlbWFpbCddO1xuICAgICAgIHRoaXMucGhvbmUgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydwaG9uZSddO1xuICAgICAgIHRoaXMudGVsID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sndGVsJ107XG5cdFx0XHQgdGhpcy5wYXNzd29yZCA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Bhc3N3b3JkJ107XG4gICAgICAgdGhpcy5jb25maXJtX3Bhc3N3b3JkID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snY29uZmlybV9wYXNzd29yZCddO1xuXHRcdFx0IHRoaXMucmVzdGF1cmFudF9uYW1lID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9uYW1lJ107XG5cdFx0XHQgdGhpcy5yZXN0YXVyYW50X2FkZHJlc3MgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydyZXN0YXVyYW50X2FkZHJlc3MnXTtcblx0XHRcdCB0aGlzLnJlc3RhdXJhbnRfdGVsX251bWJlciA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Jlc3RhdXJhbnRfdGVsX251bWJlciddO1xuXHRcdFx0IHRoaXMucmVzdGF1cmFudF9tb2JpbGVfbnVtYmVyID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9tb2JpbGVfbnVtYmVyJ107XG5cdFx0XHQgdGhpcy5yZXN0YXVyYW50X290aGVyX2VtYWlsID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9vdGhlcl9lbWFpbCddO1xuICAgICAgIHRoaXMub3Blbl90aW1lID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snb3Blbl90aW1lJ107XG4gICAgICAgdGhpcy5jbG9zZV90aW1lID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snY2xvc2VfdGltZSddO1xuICAgICAgIHRoaXMud2Vic2l0ZSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3dlYnNpdGUnXTtcbiAgICAgICB0aGlzLnByb21vdGlvbiA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ3Byb21vdGlvbiddO1xuICAgICAgIHRoaXMucHJvbW9fZGlzY291bnQgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydwcm9tb19kaXNjb3VudCddO1xuICAgICAgIHRoaXMuZGVmYXVsdF9jdXJyZW5jeSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXTtcbiAgICAgICB0aGlzLmZhY2Vib29rX2xpbmsgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydmYWNlYm9va19saW5rJ107XG4gICAgICAgdGhpcy50d2l0dGVyX2xpbmsgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWyd0d2l0dGVyX2xpbmsnXTtcbiAgICAgICB0aGlzLmxpbmtlZEluX2xpbmsgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydsaW5rZWRJbl9saW5rJ107XG4gICAgICAgdGhpcy5nb29nbGVfcGx1c19saW5rID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snZ29vZ2xlX3BsdXNfbGluayddO1xuICAgICAgIHRoaXMucmVzdF90eXBlID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdF90eXBlJ107XG4gICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZUNoYW5nZXNcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGRhdGEpKTtcblxuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTsgLy8gKHJlKXNldCB2YWxpZGF0aW9uIG1lc3NhZ2VzIG5vd1xuICB9XG5wcml2YXRlIG9uVmFsdWVDaGFuZ2VkKGRhdGE/OiBhbnkpIHtcbiAgICBpZiAoIXRoaXMuY3JlYXRlUHJvZmlsZUZvcm0pIHsgcmV0dXJuOyB9XG4gICAgY29uc3QgZm9ybSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm07XG5cbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgLy8gY2xlYXIgcHJldmlvdXMgZXJyb3IgbWVzc2FnZSAoaWYgYW55KVxuICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IFtdO1xuICAgICAgY29uc3QgY29udHJvbCA9IGZvcm0uZ2V0KGZpZWxkKTtcblxuICAgICAgaWYgKGNvbnRyb2wgJiYgY29udHJvbC5kaXJ0eSAmJiAhY29udHJvbC52YWxpZCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlcyA9IHRoaXMudmFsaWRhdGlvbk1lc3NhZ2VzW2ZpZWxkXTtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJvbC5lcnJvcnMpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdLnB1c2gobWVzc2FnZXNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbmZvcm1FcnJvcnMgPSB7XG4gICAgJ2ZpcnN0X25hbWUnOiBbXSxcbiAgICAnbGFzdF9uYW1lJzpbXSxcbiAgICAnZW1haWwnOltdLFxuXHRcdCdwaG9uZSc6W10sXG4gICAgJ3RlbCc6W10sXG4gICAgJ3Bhc3N3b3JkJzpbXSxcbiAgICAnY29uZmlybV9wYXNzd29yZCc6W10sXG4gICAgJ3Jlc3RhdXJhbnRfbmFtZSc6W10sXG4gICAgJ3Jlc3RhdXJhbnRfYWRkcmVzcyc6W10sXG4gICAgJ29wZW5fdGltZSc6W10sXG4gICAgJ2Nsb3NlX3RpbWUnOltdLFxuICAgICd3ZWJzaXRlJzpbXSxcbiAgICAnZGVmYXVsdF9jdXJyZW5jeSc6W10sXG4gICAgJ3Jlc3RfdHlwZSc6W10sXG4gIH07XG5cbiAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICdmaXJzdF9uYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIG5hbWUgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBuYW1lIG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICAgICAnbGFzdF9uYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIGxhc3RfbmFtZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIGxhc3RfbmFtZSBtdXN0IGJlIGF0IGxlc3MgMjU2IGNoYXJhY3RlcnMgbG9uZy4nXG4gICAgfSxcbiAgICAgJ2VtYWlsJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIGVtYWlsIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWlubGVuZ3RoJzogICAgICdUaGUgZW1haWwgbXVzdCBiZSBhdCBsZWFzdCAyNTYgY2hhcmFjdGVycyBsb25nLicsXG4gICAgICAnaW52YWxpZEVtYWlsJzogICdUaGUgZW1haWwgbXVzdCBiZSBhIHZhbGlkIGVtYWlsIGFkZHJlc3MuJyxcbiAgICB9LFxuICAgICAncGhvbmUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgcGhvbmUgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBwaG9uZSBtdXN0IGJlIGF0IGxlc3MgMzIgY2hhcmFjdGVycyBsb25nLicsXG4gICAgICAnaW52YWxpZE51bWJlcic6ICdoZSBwaG9uZSBtdXN0IGJlIG51bWJlcidcbiAgICB9LFxuICAgICAndGVsJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIHRlbCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIHRlbCBtdXN0IGJlIGF0IGxlc3MgMzIgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICAgICdwYXNzd29yZCc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICAgICAgJ1RoZSBwYXNzd29yZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21pbmxlbmd0aCc6ICAgICAnVGhlIHBhc3N3b3JkIG11c3QgYmUgYXQgbGVhc3QgNiBjaGFyYWN0ZXJzIGxvbmcuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSBsYXN0X25hbWUgbXVzdCBiZSBhdCBsZXNzIDI1NiBjaGFyYWN0ZXJzIGxvbmcuJ1xuICAgIH0sXG4gICAgJ2NvbmZpcm1fcGFzc3dvcmQnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgcGFzc3dvcmQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtaW5sZW5ndGgnOiAgICAgJ1RoZSBwYXNzd29yZCBtdXN0IGJlIGF0IGxlYXN0IDYgY2hhcmFjdGVycyBsb25nLicsXG4gICAgICAnbWF4bGVuZ3RoJzogICAgICdUaGUgbGFzdF9uYW1lIG11c3QgYmUgYXQgbGVzcyAyNTYgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICAgICAncmVzdGF1cmFudF9uYW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIHJlc3RhdXJhbnRfbmFtZSBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICAgICAnVGhlIHJlc3RhdXJhbnRfbmFtZSBtdXN0IGJlIGF0IGxlc3MgMjU2IGNoYXJhY3RlcnMgbG9uZy4nXG4gICAgfSxcbiAgICAgJ3Jlc3RhdXJhbnRfYWRkcmVzcyc6IHtcbiAgICAgICdyZXF1aXJlZCc6ICAgICAgJ1RoZSByZXN0YXVyYW50X2FkZHJlc3MgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAgICAgJ1RoZSByZXN0YXVyYW50X2FkZHJlc3MgbXVzdCBiZSBhdCBsZXNzIDEwMjQgY2hhcmFjdGVycyBsb25nLidcbiAgICB9LFxuICAgICAnb3Blbl90aW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIGNsb3NlX3RpbWUgaXMgcmVxdWlyZWQgLyBmb3JtYXROdW1iZXIuJ1xuICAgIH0sXG4gICAgICdjbG9zZV90aW1lJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIGNsb3NlX3RpbWUgaXMgcmVxdWlyZWQgLyBmb3JtYXROdW1iZXIuJ1xuICAgIH0sXG4gICAgICd3ZWJzaXRlJzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIHdlYnNpdGUgaXMgcmVxdWlyZWQuJ1xuICAgIH0sXG4gICAgICdkZWZhdWx0X2N1cnJlbmN5Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogICAgICAnVGhlIGRlZmF1bHRfY3VycmVuY3kgaXMgcmVxdWlyZWQuJ1xuICAgIH0sXG4gICAgICdyZXN0X3R5cGUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAgICAgICdUaGUgcmVxdWlyZWQgaXMgcmVxdWlyZWQuJ1xuICAgIH1cbiAgfTtcblx0Y3JlYXRlUHJvZmlsZSgpe1xuXHRcdC8vIHRoaXMub3Blbl90aW1lID0gdGhpcy5vcGVuX3RpbWUudmFsdWUudG9Mb2NhbGVUaW1lU3RyaW5nKFtdLCB7IGhvdXIxMjogZmFsc2UgfSk7XG4gICAgLy8gdGhpcy5jbG9zZV90aW1lID0gdGhpcy5jbG9zZV90aW1lLnZhbHVlLnRvTG9jYWxlVGltZVN0cmluZyhbXSwgeyBob3VyMTI6IGZhbHNlIH0pO1xuICAgIC8vIHRoaXMuZmV0Y2hMYXRMb24odGhpcy5yZXN0YXVyYW50X2FkZHJlc3MudmFsdWUpO1xuICAgIHRoaXMub25WYWx1ZUNoYW5nZWQoKTtcbiAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9ycykge1xuICAgICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICAgICBpZih0aGlzLmZvcm1FcnJvcnNbZmllbGRdLmxlbmd0aCA+MCl7XG4gICAgICAgICAgIHJldHVybjtcbiAgICAgICAgIH1cbiAgICAgIH1cblx0bGV0IHBhcmFtID0ge1xuXHRcdCAgICAgICAgICAgICAgJ2FkZHJlc3MnOiB0aGlzLnJlc3RhdXJhbnRfYWRkcmVzcy52YWx1ZVxuXHRcdH07XG4gIFx0XHR0aGlzLnByb2ZpbGVTZXJ2aWNlLmdldExhdGxvbihwYXJhbSkudGhlbihcbiAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAgIHRoaXMubGF0ID0gcmVzcG9uc2UubGF0X3ZhbDtcbiAgICAgICAgICAgICAgdGhpcy5sb24gPSByZXNwb25zZS5sb25nX3ZhbDtcbiAgICAgICAgICAgICAgbGV0IGNpdHkgPSBudWxsO1xuICAgICAgICAgICAgICBsZXQgc3RyZWV0ID0gbnVsbDtcbiAgICAgICAgICAgICAgbGV0IHByb3ZpbmNlID0gbnVsbDtcbiAgICAgICAgICAgICAgbGV0IGNvdW50cnkgPSBudWxsO1xuICAgICAgICAgICAgICBpZihyZXNwb25zZS5jaXR5X2xuIT11bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgIGNpdHkgPSByZXNwb25zZS5jaXR5X2xuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdHJlZXRfbG4hPXVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgc3RyZWV0ID0gcmVzcG9uc2Uuc3RyZWV0X2xuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICBpZihyZXNwb25zZS5wcm92aW5jZV9sbiE9dW5kZWZpbmVkKXtcbiAgICAgICAgICAgICAgICBwcm92aW5jZSA9IHJlc3BvbnNlLnByb3ZpbmNlX2xuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICBpZihyZXNwb25zZS5jb3VudHJ5X2xuIT11bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgIGNvdW50cnkgPSByZXNwb25zZS5jb3VudHJ5X2xuO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgaWYodGhpcy5sYXQ9PW51bGwgfHwgdGhpcy5sb249PW51bGwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgc3dhbChcbiAgICAgICAgICAgICAgICAgICdnZXQgbGF0IGxvbiBubyB2YWx1ZScsXG4gICAgICAgICAgICAgICAgICB0aGlzLmVycm9yWzBdLFxuICAgICAgICAgICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICAgICAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGxldCBvYmplY3QgPSB7XG4gICAgICAgICAgICAgICAgJ3VzZXJfaWQnOiB0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICAgICAndXNlcl9pbmZvJzoge1xuICAgICAgICAgICAgICAgICAgJ2lkJzogdGhpcy51c2VySW5mby5pZCxcbiAgICAgICAgICAgICAgICAgICdwaG9uZSc6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUucGhvbmUsXG4gICAgICAgICAgICAgICAgICAnZW1haWwnOiB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLnZhbHVlLmVtYWlsLFxuICAgICAgICAgICAgICAgICAgJ2ZpcnN0X25hbWUnOiB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLnZhbHVlLmZpcnN0X25hbWUsXG4gICAgICAgICAgICAgICAgICAnbGFzdF9uYW1lJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS5sYXN0X25hbWUsXG4gICAgICAgICAgICAgICAgICAncGFzc3dvcmQnOiB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLnZhbHVlLnBhc3N3b3JkLFxuICAgICAgICAgICAgICAgICAgJ2NvbmZpcm1fcGFzc3dvcmQnOiB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLnZhbHVlLmNvbmZpcm1fcGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICAncm9sZXMnOnRoaXMudXNlckluZm8ucm9sZXMsXG4gICAgICAgICAgICAgICAgICAndGVsJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS50ZWwsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAncmVzdGF1cmFudF9pbmZvJzp7XG4gICAgICAgICAgICAgICAgICAnaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICAgICAgICAgICAgICduYW1lJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS5yZXN0YXVyYW50X25hbWUsXG4gICAgICAgICAgICAgICAgICAnYWRkcmVzcyc6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUucmVzdGF1cmFudF9hZGRyZXNzLFxuICAgICAgICAgICAgICAgICAgJ3RlbCc6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUucmVzdGF1cmFudF90ZWxfbnVtYmVyLFxuICAgICAgICAgICAgICAgICAgJ21vYmlsZSc6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUucmVzdGF1cmFudF9tb2JpbGVfbnVtYmVyLFxuICAgICAgICAgICAgICAgICAgJ2VtYWlsJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS5yZXN0YXVyYW50X290aGVyX2VtYWlsLFxuICAgICAgICAgICAgICAgICAgJ2xhdGl0dWUnOiB0aGlzLmxhdCxcbiAgICAgICAgICAgICAgICAgICdsb25ndGl0dWUnOiB0aGlzLmxvbixcbiAgICAgICAgICAgICAgICAgICd3ZWJzaXRlJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS53ZWJzaXRlLFxuICAgICAgICAgICAgICAgICAgJ2xvZ28nOiAnJyxcbiAgICAgICAgICAgICAgICAgICdvcGVuX3RpbWUnOiB0aGlzLm9wZW5fdGltZS52YWx1ZS50b0xvY2FsZVRpbWVTdHJpbmcoW10sIHsgaG91cjEyOiBmYWxzZSB9KSxcbiAgICAgICAgICAgICAgICAgICdjbG9zZV90aW1lJzogdGhpcy5jbG9zZV90aW1lLnZhbHVlLnRvTG9jYWxlVGltZVN0cmluZyhbXSwgeyBob3VyMTI6IGZhbHNlIH0pLFxuICAgICAgICAgICAgICAgICAgJ3Byb21vdGlvbic6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUucHJvbW90aW9uLFxuICAgICAgICAgICAgICAgICAgJ3Jlc3RfdHlwZSc6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUucmVzdF90eXBlLFxuICAgICAgICAgICAgICAgICAgJ2ZhY2Vib29rX2xpbmsnOiB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLnZhbHVlLmZhY2Vib29rX2xpbmssXG4gICAgICAgICAgICAgICAgICAndHdpdHRlcl9saW5rJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS50d2l0dGVyX2xpbmssXG4gICAgICAgICAgICAgICAgICAnbGlua2VkSW5fbGluayc6IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0udmFsdWUubGlua2VkSW5fbGluayxcbiAgICAgICAgICAgICAgICAgICdnb29nbGVfcGx1c19saW5rJzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS5nb29nbGVfcGx1c19saW5rLFxuICAgICAgICAgICAgICAgICAgJ3Byb21vX2Rpc2NvdW50Jzp0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLnZhbHVlLnByb21vX2Rpc2NvdW50LFxuICAgICAgICAgICAgICAgICAgJ2NpdHknOmNpdHksXG4gICAgICAgICAgICAgICAgICAncHJvdmluY2UnOnByb3ZpbmNlLFxuICAgICAgICAgICAgICAgICAgJ2NvdW50cnknOmNvdW50cnksXG4gICAgICAgICAgICAgICAgICAnc3RyZWV0JzpzdHJlZXQsXG4gICAgICAgICAgICAgICAgICAnb3JkZXJpbmdfc2V0dGluZyc6e1xuICAgICAgICAgICAgICAgICAgICAncGF5bWVudF9mZWUnOiB0aGlzLnBheW1lbnRfZmVlLFxuICAgICAgICAgICAgICAgICAgICAnZGVsaXZlcnlfY2hhcmdlJzogdGhpcy5kZWxpdmVyeV9jaGFyZ2UsXG4gICAgICAgICAgICAgICAgICAgICdtaW5pbXVtX29yZGVyJzogdGhpcy5taW5pbXVtX29yZGVyLFxuICAgICAgICAgICAgICAgICAgICAncGF5bWVudF9tZXRob2QnOiB0aGlzLnBheW1lbnRfbWV0aG9kLFxuICAgICAgICAgICAgICAgICAgICAnZGVsaXZlcnlfbWV0aG9kcyc6IHRoaXMuZGVsaXZlcnlfbWV0aG9kcyxcbiAgICAgICAgICAgICAgICAgICAgJ2RlbGl2ZXJ5X3RpbWUnOiB0aGlzLmRlbGl2ZXJ5X3RpbWUsXG4gICAgICAgICAgICAgICAgICAgICdkZWZhdWx0X2N1cnJlbmN5JzogdGhpcy5jcmVhdGVQcm9maWxlRm9ybS52YWx1ZS5kZWZhdWx0X2N1cnJlbmN5LFxuICAgICAgICAgICAgICAgICAgICAnZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZSc6IHRoaXMuZm9vZF9vcmRlcmluZ19zeXN0ZW1fdHlwZSxcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cdFx0fTtcbiAgXHRcdHRoaXMucHJvZmlsZVNlcnZpY2UuY3JlYXRlUHJvZmlsZShvYmplY3QpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICBcdCByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSxcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG4gIFx0XHRcdFx0XHR9KTtcbiAgICBcblx0fVxuICBzZXRNYXBPbkFsbChtYXApIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLm1hcmtlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICB0aGlzLm1hcmtlcnNbaV0uc2V0TWFwKG1hcCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgY2xlYXJNYXJrZXJzKCkge1xuICAgIHRoaXMuc2V0TWFwT25BbGwobnVsbCk7XG4gIH1cbiAgcHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgIGxldCBpbWFnZVBvc3QgPSAnJztcbiAgICAgaWYoIHRoaXMuZmxnSW1hZ2VDaG9vc2UgPT0gdHJ1ZSl7XG4gICAgICAgaW1hZ2VQb3N0ID0gdGhpcy5pbWFnZVNyYztcbiAgICAgfVxuICAgICAgbGV0IHByb2ZpbGVVcGxvYWQgPSB7XG4gICAgICAgICAnaWQnOnJlc3BvbnNlLmlkLFxuICAgICAgICAgJ3RodW1iJzppbWFnZVBvc3RcbiAgICAgIH1cblx0ICAgIHRoaXMucHJvZmlsZVNlcnZpY2UucHJvZmlsZVVwbG9hZEZpbGUocHJvZmlsZVVwbG9hZCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyBGaWxlIGhhcyBiZWVuIGFkZGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnJyk7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGVhck1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLmxvZ291dCgpO1xuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gICAgICAgICAgICBcbiAgXHRcdFx0XHRcdH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmVycm9yID0gcmVzcG9uc2UuZXJyb3JzO1xuICAgICAgaWYgKHJlc3BvbnNlLmNvZGUgPT0gNDIyKSB7XG4gICAgICAgIGlmICh0aGlzLmVycm9yLmZpcnN0X25hbWUpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ2ZpcnN0X25hbWUnXSA9IHRoaXMuZXJyb3IuZmlyc3RfbmFtZTtcbiAgICAgICAgfVxuICAgICAgICAgaWYgKHRoaXMuZXJyb3IubGFzdF9uYW1lKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWydsYXN0X25hbWUnXSA9IHRoaXMuZXJyb3IubGFzdF9uYW1lO1xuICAgICAgICB9XG4gICAgICAgICBpZiAodGhpcy5lcnJvci50ZWwpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ3RlbCddID0gdGhpcy5lcnJvci50ZWw7XG4gICAgICAgIH1cbiAgICAgICAgIGlmICh0aGlzLmVycm9yLnBob25lKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWydwaG9uZSddID0gdGhpcy5lcnJvci5waG9uZTtcbiAgICAgICAgfVxuICAgICAgICAgaWYgKHRoaXMuZXJyb3IucGFzc3dvcmQpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ3Bhc3N3b3JkJ10gPSB0aGlzLmVycm9yLnBhc3N3b3JkO1xuICAgICAgICB9XG4gICAgICAgICBpZiAodGhpcy5lcnJvci5jb25maXJtX3Bhc3N3b3JkKSB7XG4gICAgICAgICAgdGhpcy5mb3JtRXJyb3JzWydjb25maXJtX3Bhc3N3b3JkJ10gPSB0aGlzLmVycm9yLmNvbmZpcm1fcGFzc3dvcmQ7XG4gICAgICAgIH1cbiAgICAgICAgIGlmICh0aGlzLmVycm9yLm5hbWUpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ3Jlc3RhdXJhbnRfbmFtZSddID0gdGhpcy5lcnJvci5uYW1lO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmVycm9yLnJlc3RhdXJhbnRfYWRkcmVzcykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1snYWRkcmVzcyddID0gdGhpcy5lcnJvci5yZXN0YXVyYW50X2FkZHJlc3M7XG4gICAgICAgIH1cbiAgICAgICAgIGlmICh0aGlzLmVycm9yLmRlZmF1bHRfY3VycmVuY3kpIHtcbiAgICAgICAgICB0aGlzLmZvcm1FcnJvcnNbJ2RlZmF1bHRfY3VycmVuY3knXSA9IHRoaXMuZXJyb3IuZGVmYXVsdF9jdXJyZW5jeTtcbiAgICAgICAgfVxuICAgICAgICAgaWYgKHRoaXMuZXJyb3Iud2Vic2l0ZSkge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1snd2Vic2l0ZSddID0gdGhpcy5lcnJvci53ZWJzaXRlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmVycm9yLnJlc3RfdHlwZSkge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1sncmVzdF90eXBlJ10gPSB0aGlzLmVycm9yLnJlc3RfdHlwZTtcbiAgICAgICAgfVxuXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzd2FsKFxuICAgICAgICAgICdDcmVhdGUgRmFpbCEnLFxuICAgICAgICAgIHRoaXMuZXJyb3JbMF0sXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIHByaXZhdGUgZmFpbGVkQ3JlYXRlIChyZXMpIHtcbiAgICBpZiAocmVzLnN0YXR1cyA9PSA0MDEpIHtcbiAgICAgIC8vIHRoaXMubG9naW5mYWlsZWQgPSB0cnVlXG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChyZXMuZGF0YS5lcnJvcnMubWVzc2FnZVswXSA9PSAnRW1haWwgVW52ZXJpZmllZCcpIHtcbiAgICAgICAgLy8gdGhpcy51bnZlcmlmaWVkID0gdHJ1ZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gb3RoZXIga2luZHMgb2YgZXJyb3IgcmV0dXJuZWQgZnJvbSBzZXJ2ZXJcbiAgICAgICAgZm9yICh2YXIgZXJyb3IgaW4gcmVzLmRhdGEuZXJyb3JzKSB7XG4gICAgICAgICAgLy8gdGhpcy5sb2dpbmZhaWxlZGVycm9yICs9IHJlcy5kYXRhLmVycm9yc1tlcnJvcl0gKyAnICdcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGluaXRNYXAoKSB7XG4gIGxldCBsYXRtYXAgPSB0aGlzLmxhdDtcbiAgbGV0IGxvbk1hcCA9IHRoaXMubG9uO1xuXHR0aGlzLm1hcE9iaiA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ29yZGVyLW1hcCcpLCB7XG5cdCAgY2VudGVyOiB7bGF0OiArbGF0bWFwLCBsbmc6ICtsb25NYXB9LFxuXHQgIHpvb206IDE1XG5cdH0pO1xuXHRcblx0XG5cdC8vZ2V0Q3VycmVudFBvc2l0aW9uQnJvd2VyKCk7IC8vIG5vdCBydW4gdW4gbG9jYWxob3N0IFx0XG5cdC8vIHNldCBtYXJrZXJzIGN1cnJlbnQgTG9jYXRpb24gYW5kIHN0b3JlIGxvY2F0aW9uIGZvciB0ZXN0IG9ubHkgc3RhcnRcblx0bGV0IGlkTWFya2VyID0gdGhpcy5pZG1hcmtlcjtcblx0dmFyIHBvc2l0aW9uQ3VycmVudCA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoK2xhdG1hcCwrbG9uTWFwKTsvLyh1c2VyIHVzZWQgd2ViIG9uIERCKVxuXHR0aGlzLm1hcmtlckN1cnJlbnRMb2NhdGlvbiA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgaWQ6aWRNYXJrZXIsXG5cdFx0XHRkcmFnZ2FibGU6IHRydWUsXG4gICAgXHRwb3NpdGlvbjogcG9zaXRpb25DdXJyZW50LCAgICBcdFxuICAgIFx0bWFwOiB0aGlzLm1hcE9iaixcbiAgICAgIHRpdGxlOiAnSGVsbG8gV29ybGQhJ1xuICAgIH0pO1x0XHRcbiAgdGhpcy5tYXJrZXJzWytpZE1hcmtlcl0gPSB0aGlzLm1hcmtlckN1cnJlbnRMb2NhdGlvbiA7XG5cdC8vIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwT2JqLCAnY2xpY2snLCBmdW5jdGlvbihldmVudCkge1xuXHQvLyAgICAgYWxlcnQoZXZlbnQubGF0TG5nKTtcblx0Ly8gfSk7XG5cdGxldCBwb3NpdGlvbk1hcmtlciA9IG51bGw7XG5cdGxldCBzZWxmID0gdGhpcztcblx0Z29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXJrZXJDdXJyZW50TG9jYXRpb24sICdkcmFnZW5kJywgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRcdFx0XHQgIC8vIGFsZXJ0KGV2ZW50LmxhdExuZyArXCIvXCIrIHRoaXMuZ2V0UG9zaXRpb24oKSk7XG5cdFx0XHRcdFx0XHRcdFx0cG9zaXRpb25NYXJrZXIgPSB0aGlzLmdldFBvc2l0aW9uKCk7XG5cdFx0XHRcdFx0XHRcdFx0c2VsZi5mZXRjaEFkZHJlc3ModGhpcy5nZXRQb3NpdGlvbigpLmxhdCgpLHRoaXMuZ2V0UG9zaXRpb24oKS5sbmcoKSk7XG4gICAgICAgICAgICAgICAgdGhpcy5sYXQ9dGhpcy5nZXRQb3NpdGlvbigpLmxhdCgpO1xuICAgICAgICAgICAgICAgIHRoaXMubG9uZz10aGlzLmdldFBvc2l0aW9uKCkubG5nKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFuZ2Vsb25sYXRGbGFnPXRydWU7XG4gICAgICAgICAgICB9KTtcblxufVxuU2VhcmNoTWFwQ2hhbmdlKCl7XG4gICB0aGlzLmZldGNoTGF0TG9uKHRoaXMucmVzdGF1cmFudF9hZGRyZXNzLnZhbHVlKTtcbn1cbnByaXZhdGUgZmV0Y2hBZGRyZXNzKGxhdCxsb24pIHtcblx0XHRcdGxldCBvYmplY3QgPSB7XG5cdFx0ICAgICAgICAgICAgICAnbGF0JzogbGF0LFxuXHRcdCAgICAgICAgICAgICAgJ2xvbic6IGxvbixcblx0XHR9O1xuICBcdFx0dGhpcy5wcm9maWxlU2VydmljZS5nZXRBZGRyZXNzKG9iamVjdCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLmFkZHJlc3NJbmZvID0gcmVzcG9uc2U7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdGF1cmFudF9hZGRyZXNzJ10uc2V0VmFsdWUoIHRoaXMuYWRkcmVzc0luZm8uYWRkcmVzcyk7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXN0YXVyYW50X2FkZHJlc3MgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydyZXN0YXVyYW50X2FkZHJlc3MnXTtcbiAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2UpOyBcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICBcdFx0XHRcdFx0fSk7XG4gIFx0fVxuICAgIHByaXZhdGUgZmV0Y2hMYXRMb24oYWRkcmVzcykge1xuXHRcdFx0bGV0IG9iamVjdCA9IHtcblx0XHQgICAgICAgICAgICAgICdhZGRyZXNzJzogYWRkcmVzc1xuXHRcdH07XG4gIFx0XHR0aGlzLnByb2ZpbGVTZXJ2aWNlLmdldExhdGxvbihvYmplY3QpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgIHRoaXMubGF0ID0gcmVzcG9uc2UubGF0X3ZhbDtcbiAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9uID0gcmVzcG9uc2UubG9uZ192YWw7XG4gICAgICAgICAgICAgICAgICAgICBcdGNvbnNvbGUubG9nKHJlc3BvbnNlKTsgXG4gICAgICAgICAgICAgICAgICAgICAgbGV0IGxhdG1hcCA9IHRoaXMubGF0O1xuICAgICAgICAgICAgICAgICAgICAgIGxldCBsb25NYXAgPSB0aGlzLmxvbjtcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgcG9zaXRpb25DdXJyZW50ID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZygrbGF0bWFwLCtsb25NYXApOy8vKHVzZXIgdXNlZCB3ZWIgb24gREIpXG4gICAgICAgICAgICAgICAgICAgICAgbGV0IGlkTWFya2VyID0gK3RoaXMuaWRtYXJrZXIgKyAxO1xuICAgICAgICAgICAgICAgICAgXHQgIHRoaXMubWFya2VyQ3VycmVudExvY2F0aW9uID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgICBcdFx0XHRkcmFnZ2FibGU6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgXHRwb3NpdGlvbjogcG9zaXRpb25DdXJyZW50LCAgICBcdFxuICAgICAgICAgICAgICAgICAgICAgIFx0bWFwOiB0aGlzLm1hcE9iaiwgIFxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdIZWxsbyBXb3JsZCEnXG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICBcdHRoaXMubWFya2Vyc1sraWRNYXJrZXJdID0gdGhpcy5tYXJrZXJDdXJyZW50TG9jYXRpb24gO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1hcE9iai5wYW5Ubyh0aGlzLm1hcmtlckN1cnJlbnRMb2NhdGlvbi5wb3NpdGlvbik7XG4gICAgICAgICAgICAgICAgICAgIHZhciBtYXJrZXIgPSB0aGlzLm1hcmtlcnNbK3RoaXMuaWRtYXJrZXJdOyAvLyBmaW5kIHRoZSBtYXJrZXIgYnkgZ2l2ZW4gaWRcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pZG1hcmtlciA9IGlkTWFya2VyO1xuICAgICAgICAgICAgICAgICAgICBtYXJrZXIuc2V0TWFwKG51bGwpO1xuICAgICAgICAgICAgICAgICAgXHQvLyBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcE9iaiwgJ2NsaWNrJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgIFx0Ly8gICAgIGFsZXJ0KGV2ZW50LmxhdExuZyk7XG4gICAgICAgICAgICAgICAgICBcdC8vIH0pO1xuICAgICAgICAgICAgICAgICAgXHRsZXQgcG9zaXRpb25NYXJrZXIgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgXHRsZXQgc2VsZiA9IHRoaXM7XG4gICAgICAgICAgICAgICAgICBcdGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFya2VyQ3VycmVudExvY2F0aW9uLCAnZHJhZ2VuZCcsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdFx0XHRcdFx0ICAvLyBhbGVydChldmVudC5sYXRMbmcgK1wiL1wiKyB0aGlzLmdldFBvc2l0aW9uKCkpO1xuICAgIFx0XHRcdFx0XHRcdFx0XHRwb3NpdGlvbk1hcmtlciA9IHRoaXMuZ2V0UG9zaXRpb24oKTtcbiAgICBcdFx0XHRcdFx0XHRcdFx0c2VsZi5mZXRjaEFkZHJlc3ModGhpcy5nZXRQb3NpdGlvbigpLmxhdCgpLHRoaXMuZ2V0UG9zaXRpb24oKS5sbmcoKSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGF0PXRoaXMuZ2V0UG9zaXRpb24oKS5sYXQoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb25nPXRoaXMuZ2V0UG9zaXRpb24oKS5sbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGFuZ2Vsb25sYXRGbGFnPXRydWU7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG5cbmhhbmRsZUxvY2F0aW9uRXJyb3IoYnJvd3Nlckhhc0dlb2xvY2F0aW9uLCBpbmZvV2luZG93LCBwb3MpIHtcblx0aW5mb1dpbmRvdy5zZXRQb3NpdGlvbihwb3MpO1xuXHRpbmZvV2luZG93LnNldENvbnRlbnQoYnJvd3Nlckhhc0dlb2xvY2F0aW9uID9cblx0ICAgICAgICAgICAgICAgICAgICAgICdFcnJvcjogVGhlIEdlb2xvY2F0aW9uIHNlcnZpY2UgZmFpbGVkLicgOlxuXHQgICAgICAgICAgICAgICAgICAgICAgJ0Vycm9yOiBZb3VyIGJyb3dzZXIgZG9lc25cXCd0IHN1cHBvcnQgZ2VvbG9jYXRpb24uJyk7XG59XG5jaGFnZXRhYih2YWx1ZSl7XG5cdGlmKHRoaXMudGFiQ2hhbmdlRmlzcnQ9PXRydWUpe1xuICAgICBzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdHRoaXMuaW5pdE1hcCgpO1xuICAgICAgIH0sIDgwMCk7XG5cdFx0XHR0aGlzLnRhYkNoYW5nZUZpc3J0PWZhbHNlO1xuXHR9XG59XG5cbi8qc2VsZWN0IHN0YXJ0Ki9cblxucHJpdmF0ZSB2YWx1ZTphbnkgPSB7fTtcbnByaXZhdGUgdmFsdWVDdXJyZW50Y3k6YW55ID0ge307XG4gIHByaXZhdGUgX2Rpc2FibGVkVjpzdHJpbmcgPSAnMCc7XG4gIHByaXZhdGUgZGlzYWJsZWQ6Ym9vbGVhbiA9IGZhbHNlO1xuIFxuIFxuICBwdWJsaWMgc2VsZWN0ZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICBcbiAgICBjb25zb2xlLmxvZygnU2VsZWN0ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgfVxuIFxuICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdSZW1vdmVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XG4gICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdF90eXBlJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgdGhpcy5yZXN0X3R5cGUgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydyZXN0X3R5cGUnXTtcbiAgfVxuIHB1YmxpYyBjdXJyZW5jeXJlbW92ZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnUmVtb3ZlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXS5zZXRWYWx1ZShudWxsKTtcbiAgICB0aGlzLmRlZmF1bHRfY3VycmVuY3kgPSB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydkZWZhdWx0X2N1cnJlbmN5J107XG4gIH1cbiAgcHVibGljIHR5cGVkKHZhbHVlOmFueSk6dm9pZCB7XG4gICAgY29uc29sZS5sb2coJ05ldyBzZWFyY2ggaW5wdXQ6ICcsIHZhbHVlKTtcbiAgfVxuIFxuICBwdWJsaWMgcmVmcmVzaFZhbHVlKHZhbHVlOmFueSk6dm9pZCB7XG4gICAgaWYodmFsdWUhPXVuZGVmaW5lZCAmJiB2YWx1ZSE9bnVsbCAmJiB2YWx1ZS5sZW5ndGghPTApe1xuICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xuICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdF90eXBlJ10uc2V0VmFsdWUodmFsdWUuaWQpO1xuICAgIHRoaXMucmVzdF90eXBlID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdF90eXBlJ107XG4gICAgfWVsc2V7XG4gICAgICB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydyZXN0X3R5cGUnXS5zZXRWYWx1ZShudWxsKTtcbiAgICAgIHRoaXMucmVzdF90eXBlID0gdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1sncmVzdF90eXBlJ107XG4gICAgfVxuICAgIFxuICB9XG4gIFxuICBwdWJsaWMgcmVmcmVzaEN1cnJlbmN5VmFsdWUodmFsdWU6YW55KTp2b2lkIHtcbiAgICAvLyB0aGlzLnZhbHVlQ3VycmVudGN5ID0gdmFsdWU7XG4gICAgIGlmKHZhbHVlIT11bmRlZmluZWQgJiYgdmFsdWUhPW51bGwgJiYgdmFsdWUubGVuZ3RoIT0wKXtcbiAgICAgIHRoaXMudmFsdWVDdXJyZW50Y3kgPSB2YWx1ZTtcbiAgICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXS5zZXRWYWx1ZSh2YWx1ZS50ZXh0KTtcbiAgICAgIHRoaXMuZGVmYXVsdF9jdXJyZW5jeSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXTtcbiAgICAgfWVsc2V7XG4gICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snZGVmYXVsdF9jdXJyZW5jeSddLnNldFZhbHVlKG51bGwpO1xuICAgICAgIHRoaXMuZGVmYXVsdF9jdXJyZW5jeSA9IHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2RlZmF1bHRfY3VycmVuY3knXTtcbiAgICAgfVxuICAgIFxuICB9XG4gICBwdWJsaWMgb25DaGFuZ2VPcGVuKHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnQ2hhbmdlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICB9XG4gICBwdWJsaWMgb25DaGFuZ2VPcGVuVmFsdWUoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCB2YWx1ZTE9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICBjb25zb2xlLmxvZygnQ2hhbmdlZCB2YWx1ZSBpczogJywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICBpZih2YWx1ZTE9PVwiXCJ8fCB2YWx1ZTE9PW51bGwpe1xuICAgICAgdGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snb3Blbl90aW1lJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgICAgdGhpcy5vcGVuX3RpbWU9dGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snb3Blbl90aW1lJ107XG4gICAgfVxuICAgIGNvbnN0IG5hbWVSZSA9IG5ldyBSZWdFeHAoL15bMC05XSskL2kpO1xuICAgIGlmKCFuYW1lUmUudGVzdCh2YWx1ZTEpIHx8IHZhbHVlMS5sZW5ndGg+Mil7XG4gICAgICB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydvcGVuX3RpbWUnXS5zZXRWYWx1ZShudWxsKTtcbiAgICAgICB0aGlzLm9wZW5fdGltZT10aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydvcGVuX3RpbWUnXTtcbiAgICB9XG4gIH1cbiAgcHVibGljIG9uQ2hhbmdlQ2xvc2VWYWx1ZShldmVudDogYW55KTogdm9pZCB7XG4gICAgbGV0IHZhbHVlMT0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuICAgIGNvbnNvbGUubG9nKCdDaGFuZ2VkIHZhbHVlIGlzOiAnLCBldmVudC50YXJnZXQudmFsdWUpO1xuICAgIGlmKHZhbHVlMT09XCJcInx8IHZhbHVlMT09bnVsbCl7XG4gICAgICB0aGlzLmNyZWF0ZVByb2ZpbGVGb3JtLmNvbnRyb2xzWydjbG9zZV90aW1lJ10uc2V0VmFsdWUobnVsbCk7XG4gICAgICAgdGhpcy5jbG9zZV90aW1lPXRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2Nsb3NlX3RpbWUnXTtcbiAgICB9XG4gICAgY29uc3QgbmFtZVJlID0gbmV3IFJlZ0V4cCgvXlswLTldKyQvaSk7XG4gICAgaWYoIW5hbWVSZS50ZXN0KHZhbHVlMSkgfHwgdmFsdWUxLmxlbmd0aD4yKXtcbiAgICAgIHRoaXMuY3JlYXRlUHJvZmlsZUZvcm0uY29udHJvbHNbJ2Nsb3NlX3RpbWUnXS5zZXRWYWx1ZShudWxsKTtcbiAgICAgICB0aGlzLmNsb3NlX3RpbWU9dGhpcy5jcmVhdGVQcm9maWxlRm9ybS5jb250cm9sc1snY2xvc2VfdGltZSddO1xuICAgIH1cbiAgICBcbiAgfVxuICBwdWJsaWMgb25DaGFuZ2VDbG9zZSh2YWx1ZTE6IGFueSk6IHZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdDaGFuZ2VkIHZhbHVlIGlzOiAnLCB2YWx1ZTEpO1xuICB9XG4gICAvKnVwbG9hZCBmaWxlKi9cblx0cHJpdmF0ZSBzYWN0aXZlQ29sb3I6IHN0cmluZyA9ICdncmVlbic7XG4gICAgcHJpdmF0ZSBiYXNlQ29sb3I6IHN0cmluZyA9ICcjY2NjJztcbiAgICBwcml2YXRlIG92ZXJsYXlDb2xvcjogc3RyaW5nID0gJ3JnYmEoMjU1LDI1NSwyNTUsMC41KSc7XG4gICAgXG4gICAgcHJpdmF0ZSBkcmFnZ2luZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgbG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpbWFnZUxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaW1hZ2VTcmM6IHN0cmluZyA9ICdodHRwOi8vd3d3LnBsYWNlaG9sZC5pdC8yMDB4MTUwL0VGRUZFRi9BQUFBQUEmYW1wO3RleHQ9bm8raW1hZ2UnO1xuICAgIHByaXZhdGUgZmxnSW1hZ2VDaG9vc2U6Ym9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgaWNvbkNvbG9yOiBzdHJpbmcgPSAnJztcbiAgICBwcml2YXRlICBib3JkZXJDb2xvcjogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSAgYWN0aXZlQ29sb3I6IHN0cmluZyA9ICcnO1xuICAgIHByaXZhdGUgaGlkZGVuSW1hZ2U6Ym9vbGVhbj1mYWxzZTtcbiAgICBwcml2YXRlIGNhdGVnb3JpZXMgOiBhbnk7XG4gICAgcHJpdmF0ZSBjYXRlZ29yeU9iamVjdCA6IGFueTtcbiAgICBwcml2YXRlIGZpbGVDaG9vc2UgOiBGaWxlO1xuICAgIHByaXZhdGUgZXJyb3I6IGFueTtcbiAgICBoYW5kbGVEcmFnRW50ZXIoKSB7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSB0cnVlO1xuICAgIH1cbiAgICBcbiAgICBoYW5kbGVEcmFnTGVhdmUoKSB7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSBmYWxzZTtcbiAgICB9XG4gICAgXG4gICAgaGFuZGxlRHJvcChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLmhhbmRsZUlucHV0Q2hhbmdlKGUpO1xuICAgIH1cbiAgICBcbiAgICBoYW5kbGVJbWFnZUxvYWQoKSB7XG4gICAgICAgIHRoaXMuaW1hZ2VMb2FkZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLmljb25Db2xvciA9IHRoaXMub3ZlcmxheUNvbG9yO1xuICAgICAgICBpZih0aGlzLmltYWdlTG9hZGVkID0gdHJ1ZSl7XG5cdFx0XHR0aGlzLmhpZGRlbkltYWdlID0gdHJ1ZTtcblx0XHR9XG4gICAgfVxuXG4gICAgaGFuZGxlSW5wdXRDaGFuZ2UoZSkge1xuICAgICAgICB2YXIgZmlsZSA9IGUuZGF0YVRyYW5zZmVyID8gZS5kYXRhVHJhbnNmZXIuZmlsZXNbMF0gOiBlLnRhcmdldC5maWxlc1swXTtcbiAgICAgICAgdGhpcy5maWxlQ2hvb3NlID0gZmlsZTtcbiAgICAgICAgdmFyIHBhdHRlcm4gPSAvaW1hZ2UtKi87XG4gICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXG4gICAgICAgIGlmICghZmlsZS50eXBlLm1hdGNoKHBhdHRlcm4pKSB7XG4gICAgICAgICAgICBhbGVydCgnaW52YWxpZCBmb3JtYXQnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubG9hZGVkID0gZmFsc2U7XG5cbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9IHRoaXMuX2hhbmRsZVJlYWRlckxvYWRlZC5iaW5kKHRoaXMpO1xuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcbiAgICAgICAgdGhpcy5mbGdJbWFnZUNob29zZT0gdHJ1ZTtcbiAgICB9XG4gICAgXG4gICAgX2hhbmRsZVJlYWRlckxvYWRlZChlKSB7XG4gICAgICAgIHZhciByZWFkZXIgPSBlLnRhcmdldDtcbiAgICAgICAgdGhpcy5pbWFnZVNyYyA9IHJlYWRlci5yZXN1bHQ7XG4gICAgICAgIHRoaXMubG9hZGVkID0gdHJ1ZTtcbiAgICB9XG4gICAgXG4gICAgX3NldEFjdGl2ZSgpIHtcbiAgICAgICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYWN0aXZlQ29sb3I7XG4gICAgICAgIGlmICh0aGlzLmltYWdlU3JjLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5pY29uQ29sb3IgPSB0aGlzLmFjdGl2ZUNvbG9yO1xuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIF9zZXRJbmFjdGl2ZSgpIHtcbiAgICAgICAgdGhpcy5ib3JkZXJDb2xvciA9IHRoaXMuYmFzZUNvbG9yO1xuICAgICAgICBpZiAodGhpcy5pbWFnZVNyYy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuaWNvbkNvbG9yID0gdGhpcy5iYXNlQ29sb3I7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLy8gZW5kIHVwbG9hZCBmaWxlXG59XG4iXX0=
