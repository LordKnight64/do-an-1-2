"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var utils_1 = require("app/utils/utils");
var notification_service_1 = require("app/services/notification.service");
var router_animations_1 = require("../../router.animations");
var router_1 = require("@angular/router");
var auth_service_1 = require("app/services/auth.service");
var storage_service_1 = require("app/services/storage.service");
var turnover_service_1 = require("app/services/turnover.service");
var ng2_select_1 = require("ng2-select");
var TurnOverComponent = (function () {
    function TurnOverComponent(turnOverService, authServ, notifyService, router, fb, StorageService) {
        this.turnOverService = turnOverService;
        this.authServ = authServ;
        this.notifyService = notifyService;
        this.router = router;
        this.fb = fb;
        this.StorageService = StorageService;
        this.turnOverByMonth = [];
        this.february = null;
        this.march = null;
        this.april = null;
        this.may = null;
        this.june = null;
        this.july = null;
        this.august = null;
        this.september = null;
        this.october = null;
        this.november = null;
        this.december = null;
        this.value = {};
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.barChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'Planing' },
            { data: [], label: 'Actual' }
        ];
        this.lineChartData = [
            { data: [], label: 'Planing' },
            { data: [], label: 'Actual' }
        ];
        this.lineChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.lineChartOptions = {
            responsive: true
        };
        this.lineChartLegend = true;
        this.lineChartType = 'line';
        this.formErrors = {
            'january': [],
            'february': [],
            'march': [],
            'april': [],
            'may': [],
            'june': [],
            'july': [],
            'august': [],
            'september': [],
            'october': [],
            'november': [],
            'december': []
        };
        this.validationMessages = {
            'january': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'february': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'march': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'april': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'may': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'june': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'july': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'august': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'september': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'october': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'november': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            },
            'december': {
                'required': 'The turnover planing field is required.',
                'maxlength': 'The turnover planing may not be greater than 11 characters.',
                'pattern': 'The turnover planing must be a number.'
            }
        };
    }
    TurnOverComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.fetchAllYears();
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        var currentYear = (new Date()).getFullYear();
        this.selectedYear = currentYear;
        this.fetchTurnOverPlaning(this.restId, currentYear);
        this.fetchTurnOver(this.restId, currentYear);
        this.buildForm();
        this.mymodel = {
            january: 0,
            february: 0,
            march: 0,
            april: 0,
            may: 0,
            june: 0,
            july: 0,
            august: 0,
            september: 0,
            october: 0,
            november: 0,
            december: 0
        };
    };
    TurnOverComponent.prototype.setVar = function () {
        if (this.mymodel != null) {
            this.mymodel = {
                january: this.mymodel.january,
                february: this.mymodel.february,
                march: this.mymodel.march,
                april: this.mymodel.april,
                may: this.mymodel.may,
                june: this.mymodel.june,
                july: this.mymodel.july,
                august: this.mymodel.august,
                september: this.mymodel.september,
                october: this.mymodel.october,
                november: this.mymodel.november,
                december: this.mymodel.december
            };
            this.dtForm.controls['january'].setValue(this.mymodel.january);
            this.dtForm.controls['february'].setValue(this.mymodel.february);
            this.dtForm.controls['march'].setValue(this.mymodel.march);
            this.dtForm.controls['april'].setValue(this.mymodel.april);
            this.dtForm.controls['may'].setValue(this.mymodel.may);
            this.dtForm.controls['june'].setValue(this.mymodel.june);
            this.dtForm.controls['july'].setValue(this.mymodel.july);
            this.dtForm.controls['august'].setValue(this.mymodel.august);
            this.dtForm.controls['september'].setValue(this.mymodel.september);
            this.dtForm.controls['october'].setValue(this.mymodel.october);
            this.dtForm.controls['november'].setValue(this.mymodel.november);
            this.dtForm.controls['december'].setValue(this.mymodel.december);
            this.january = this.dtForm.controls['january'];
            this.february = this.dtForm.controls['february'];
            this.march = this.dtForm.controls['march'];
            this.april = this.dtForm.controls['april'];
            this.may = this.dtForm.controls['may'];
            this.june = this.dtForm.controls['june'];
            this.july = this.dtForm.controls['july'];
            this.august = this.dtForm.controls['august'];
            this.september = this.dtForm.controls['september'];
            this.october = this.dtForm.controls['october'];
            this.november = this.dtForm.controls['november'];
            this.december = this.dtForm.controls['december'];
        }
    };
    TurnOverComponent.prototype.buildForm = function () {
        var _this = this;
        this.dtForm = this.fb.group({
            'january': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'february': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'march': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'april': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'may': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'june': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'july': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'august': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'september': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'october': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'november': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]],
            'december': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11)]]
        });
        this.january = this.dtForm.controls['january'];
        this.february = this.dtForm.controls['february'];
        this.march = this.dtForm.controls['march'];
        this.april = this.dtForm.controls['april'];
        this.may = this.dtForm.controls['may'];
        this.june = this.dtForm.controls['june'];
        this.july = this.dtForm.controls['july'];
        this.august = this.dtForm.controls['august'];
        this.september = this.dtForm.controls['september'];
        this.october = this.dtForm.controls['october'];
        this.november = this.dtForm.controls['november'];
        this.december = this.dtForm.controls['december'];
        this.dtForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(false, data); });
        //this.onValueChanged(); // (re)set validation messages now
    };
    TurnOverComponent.prototype.onValueChanged = function (unDirty, data) {
        if (!this.dtForm) {
            return;
        }
        var form = this.dtForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            var control = form.get(field);
            if (control && (control.dirty || unDirty) && control.invalid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    };
    TurnOverComponent.prototype.doSaveTurnOverPlaning = function () {
        var _this = this;
        this.onValueChanged(true);
        for (var field in this.formErrors) {
            if (this.formErrors[field].length > 0) {
                return;
            }
        }
        this.mymodel.january = this.january.value;
        this.mymodel.february = this.february.value;
        this.mymodel.march = this.march.value;
        this.mymodel.april = this.april.value;
        this.mymodel.may = this.may.value;
        this.mymodel.june = this.june.value;
        this.mymodel.july = this.july.value;
        this.mymodel.august = this.august.value;
        this.mymodel.september = this.september.value;
        this.mymodel.october = this.october.value;
        this.mymodel.november = this.november.value;
        this.mymodel.december = this.december.value;
        var params = {
            'year_no': this.selectedYear,
            'rest_id': this.restId,
            'turnover_planing': JSON.stringify(this.mymodel),
            'user_id': this.userId
        };
        this.turnOverService.updateTurnoverPlaning(params).then(function (response) {
            var harError = false;
            for (var field in response.errors) {
                harError = true;
                _this.formErrors[field] = response.errors[field];
            }
            if (harError) {
                return;
            }
            _this.notifyService.success('Turn over has been updated');
            _this.fetchTurnOverPlaning(_this.restId, _this.selectedYear);
            _this.fetchTurnOver(_this.restId, _this.selectedYear);
        }, function (error) {
            console.log(error);
        });
    };
    ;
    TurnOverComponent.prototype.fetchTurnOverPlaning = function (restId, year) {
        var _this = this;
        var param = {
            'rest_id': restId,
            'year_no': year
        };
        var self = this;
        this.turnOverService.getTurnoverPlaning(param).then(function (response) {
            if (response.turn_over_planing.length == 0) {
                _this.mymodel = {
                    january: 0,
                    february: 0,
                    march: 0,
                    april: 0,
                    may: 0,
                    june: 0,
                    july: 0,
                    august: 0,
                    september: 0,
                    october: 0,
                    november: 0,
                    december: 0
                };
                _this.turnOverPlaning = _this.mymodel;
                console.log('response.turn_over_planing ', _this.mymodel);
            }
            else {
                _this.turnOverPlaning = JSON.parse(response.turn_over_planing[0].turn_over_planing);
                console.log('getTurnoverPlaning ', _this.turnOverPlaning);
                _this.mymodel = _this.turnOverPlaning;
            }
            _this.setVar();
            _this.updatePlaningChart();
        }, function (error) {
            alert('ok fd');
            console.log(error);
        });
    };
    TurnOverComponent.prototype.updatePlaningChart = function () {
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [+this.mymodel.january,
            +this.mymodel.march,
            +this.mymodel.april,
            +this.mymodel.april,
            +this.mymodel.may,
            +this.mymodel.june,
            +this.mymodel.july,
            +this.mymodel.august,
            +this.mymodel.september,
            +this.mymodel.october,
            +this.mymodel.november,
            +this.mymodel.december];
        this.barChartData = clone;
        var clone1 = JSON.parse(JSON.stringify(this.lineChartData));
        clone1[0].data = [+this.mymodel.january,
            +this.mymodel.march,
            +this.mymodel.april,
            +this.mymodel.april,
            +this.mymodel.may,
            +this.mymodel.june,
            +this.mymodel.july,
            +this.mymodel.august,
            +this.mymodel.september,
            +this.mymodel.october,
            +this.mymodel.november,
            +this.mymodel.december];
        this.lineChartData = clone1;
    };
    TurnOverComponent.prototype.updateActualChart = function () {
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[1].data = this.turnOverByMonth;
        this.barChartData = clone;
        var clone1 = JSON.parse(JSON.stringify(this.lineChartData));
        clone1[1].data = this.turnOverByMonth;
        this.lineChartData = clone1;
    };
    TurnOverComponent.prototype.fetchTurnOver = function (restId, year) {
        var _this = this;
        var param = {
            'rest_id': restId,
            'year_no': year
        };
        this.turnOverService.getTurnover(param).then(function (response) {
            // 	console.log('getTurnover ',response);
            _this.turnOvers = response.turnover;
            //  console.log('this.turnOvers ',this.turnOvers);
            _this.calcTurnOverByMonth();
        }, function (error) {
            console.log(error);
        });
    };
    TurnOverComponent.prototype.fetchAllYears = function () {
        this.items = [];
        for (var i = 1; i < 15; i++) {
            this.items.push({
                id: i,
                text: 2016 + i
            });
        }
        this.select.items = this.items;
        this.value = this.items[0];
    };
    TurnOverComponent.prototype.selected = function (value) {
        this.fetchTurnOverPlaning(this.restId, Number(value.text));
        this.fetchTurnOver(this.restId, Number(value.text));
        // console.log(value.text);
        this.selectedYear = value.text;
        this.value = value;
    };
    TurnOverComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    TurnOverComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    TurnOverComponent.prototype.calcTurnOverByMonth = function () {
        this.turnOverByMonth = [];
        for (var i = 0; i < this.turnOvers.length; i++) {
            var turnOver = this.turnOvers[i];
            var total = 0;
            for (var j = 0; j < turnOver.length; j++) {
                total += Number(turnOver[j].price);
            }
            this.turnOverByMonth.push(total);
        }
        this.updateActualChart();
    };
    // events
    TurnOverComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    TurnOverComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], TurnOverComponent.prototype, "select", void 0);
    TurnOverComponent = __decorate([
        core_1.Component({
            selector: 'turn-over',
            templateUrl: utils_1.default.getView('app/components/turn-over/turn-over.component.html'),
            styleUrls: [utils_1.default.getView('app/components/turn-over/turn-over.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [turnover_service_1.TurnOverService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            forms_1.FormBuilder,
            storage_service_1.StorageService])
    ], TurnOverComponent);
    return TurnOverComponent;
}());
exports.TurnOverComponent = TurnOverComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL3R1cm4tb3Zlci90dXJuLW92ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTZEO0FBQzdELHdDQUE2RjtBQUM3Rix5Q0FBb0M7QUFFcEMsMEVBQXdFO0FBRXhFLDZEQUEyRDtBQUMzRCwwQ0FBeUM7QUFDekMsMERBQXlEO0FBQ3pELGdFQUE4RDtBQUM5RCxrRUFBOEQ7QUFDN0QseUNBQXdEO0FBUXpEO0lBd0RFLDJCQUNRLGVBQWdDLEVBQ2hDLFFBQXFCLEVBQ3JCLGFBQWtDLEVBQ2xDLE1BQWMsRUFDZCxFQUFlLEVBQ2YsY0FBOEI7UUFMOUIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsa0JBQWEsR0FBYixhQUFhLENBQXFCO1FBQ2xDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBbkQ5QixvQkFBZSxHQUFVLEVBQUUsQ0FBQztRQUk1QixhQUFRLEdBQW9CLElBQUksQ0FBQztRQUNqQyxVQUFLLEdBQW9CLElBQUksQ0FBQztRQUM5QixVQUFLLEdBQW9CLElBQUksQ0FBQztRQUM5QixRQUFHLEdBQW9CLElBQUksQ0FBQztRQUM1QixTQUFJLEdBQW9CLElBQUksQ0FBQztRQUM3QixTQUFJLEdBQW9CLElBQUksQ0FBQztRQUM3QixXQUFNLEdBQW9CLElBQUksQ0FBQztRQUMvQixjQUFTLEdBQW1CLElBQUksQ0FBQztRQUNqQyxZQUFPLEdBQW1CLElBQUksQ0FBQztRQUMvQixhQUFRLEdBQW1CLElBQUksQ0FBQztRQUNoQyxhQUFRLEdBQW1CLElBQUksQ0FBQztRQUVoQyxVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2hCLG9CQUFlLEdBQU87WUFDM0Isc0JBQXNCLEVBQUUsS0FBSztZQUM3QixVQUFVLEVBQUUsSUFBSTtTQUNqQixDQUFDO1FBRUssbUJBQWMsR0FBWSxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDckosaUJBQVksR0FBVSxLQUFLLENBQUM7UUFDNUIsbUJBQWMsR0FBVyxJQUFJLENBQUM7UUFFOUIsaUJBQVksR0FBUztZQUMxQixFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBQztZQUM1QixFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBQztTQUM1QixDQUFDO1FBR0ksa0JBQWEsR0FBYztZQUMvQixFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBQztZQUM1QixFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBQztTQUU1QixDQUFDO1FBQ0ssb0JBQWUsR0FBYyxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDeEoscUJBQWdCLEdBQU87WUFDNUIsVUFBVSxFQUFFLElBQUk7U0FDakIsQ0FBQztRQUVLLG9CQUFlLEdBQVcsSUFBSSxDQUFDO1FBQy9CLGtCQUFhLEdBQVUsTUFBTSxDQUFDO1FBMElyQyxlQUFVLEdBQUc7WUFDWCxTQUFTLEVBQUUsRUFBRTtZQUNiLFVBQVUsRUFBRSxFQUFFO1lBQ2QsT0FBTyxFQUFFLEVBQUU7WUFDWCxPQUFPLEVBQUUsRUFBRTtZQUNYLEtBQUssRUFBRSxFQUFFO1lBQ1QsTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxFQUFFO1lBQ1osV0FBVyxFQUFFLEVBQUU7WUFDZixTQUFTLEVBQUUsRUFBRTtZQUNiLFVBQVUsRUFBRSxFQUFFO1lBQ2QsVUFBVSxFQUFFLEVBQUU7U0FFZixDQUFDO1FBRUYsdUJBQWtCLEdBQUc7WUFDbkIsU0FBUyxFQUFFO2dCQUNULFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3JELFdBQVcsRUFBRSw2REFBNkQ7Z0JBQzFFLFNBQVMsRUFBRSx3Q0FBd0M7YUFDcEQ7WUFDRCxVQUFVLEVBQUU7Z0JBQ1YsVUFBVSxFQUFFLHlDQUF5QztnQkFDckQsV0FBVyxFQUFFLDZEQUE2RDtnQkFDMUUsU0FBUyxFQUFFLHdDQUF3QzthQUNwRDtZQUNELE9BQU8sRUFBRTtnQkFDUixVQUFVLEVBQUUseUNBQXlDO2dCQUNwRCxXQUFXLEVBQUUsNkRBQTZEO2dCQUMxRSxTQUFTLEVBQUUsd0NBQXdDO2FBQ3BEO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3JELFdBQVcsRUFBRSw2REFBNkQ7Z0JBQzFFLFNBQVMsRUFBRSx3Q0FBd0M7YUFDcEQ7WUFDRCxLQUFLLEVBQUU7Z0JBQ0wsVUFBVSxFQUFFLHlDQUF5QztnQkFDckQsV0FBVyxFQUFFLDZEQUE2RDtnQkFDMUUsU0FBUyxFQUFFLHdDQUF3QzthQUNwRDtZQUNELE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQUUseUNBQXlDO2dCQUNyRCxXQUFXLEVBQUUsNkRBQTZEO2dCQUMxRSxTQUFTLEVBQUUsd0NBQXdDO2FBQ3BEO1lBQ0QsTUFBTSxFQUFFO2dCQUNMLFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3RELFdBQVcsRUFBRSw2REFBNkQ7Z0JBQzFFLFNBQVMsRUFBRSx3Q0FBd0M7YUFDcEQ7WUFDRCxRQUFRLEVBQUU7Z0JBQ1QsVUFBVSxFQUFFLHlDQUF5QztnQkFDcEQsV0FBVyxFQUFFLDZEQUE2RDtnQkFDMUUsU0FBUyxFQUFFLHdDQUF3QzthQUNwRDtZQUNELFdBQVcsRUFBRTtnQkFDVixVQUFVLEVBQUUseUNBQXlDO2dCQUN0RCxXQUFXLEVBQUUsNkRBQTZEO2dCQUMxRSxTQUFTLEVBQUUsd0NBQXdDO2FBQ3BEO1lBQ0EsU0FBUyxFQUFFO2dCQUNWLFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3JELFdBQVcsRUFBRSw2REFBNkQ7Z0JBQzFFLFNBQVMsRUFBRSx3Q0FBd0M7YUFDcEQ7WUFDQSxVQUFVLEVBQUU7Z0JBQ1gsVUFBVSxFQUFFLHlDQUF5QztnQkFDckQsV0FBVyxFQUFFLDZEQUE2RDtnQkFDMUUsU0FBUyxFQUFFLHdDQUF3QzthQUNwRDtZQUNBLFVBQVUsRUFBRTtnQkFDVixVQUFVLEVBQUUseUNBQXlDO2dCQUN0RCxXQUFXLEVBQUUsNkRBQTZEO2dCQUMxRSxTQUFTLEVBQUUsd0NBQXdDO2FBQ3BEO1NBRUYsQ0FBQztJQWhOd0MsQ0FBQztJQUUzQyxvQ0FBUSxHQUFSO1FBRUUsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNwQyxJQUFJLFdBQVcsR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztRQUNoQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsQ0FBQztZQUNWLFFBQVEsRUFBQyxDQUFDO1lBQ1YsS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLENBQUM7WUFDUCxJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxDQUFDO1lBQ1QsU0FBUyxFQUFFLENBQUM7WUFDWixPQUFPLEVBQUUsQ0FBQztZQUNWLFFBQVEsRUFBRSxDQUFDO1lBQ1gsUUFBUSxFQUFFLENBQUM7U0FDWixDQUFDO0lBRUosQ0FBQztJQUVRLGtDQUFNLEdBQWQ7UUFFQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRztnQkFDYixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPO2dCQUM3QixRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRO2dCQUMvQixLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUN6QixLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUN6QixHQUFHLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUN2QixNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO2dCQUMzQixTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTO2dCQUNqQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPO2dCQUM3QixRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRO2dCQUMvQixRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRO2FBQ2hDLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV6RCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUVqRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVuRCxDQUFDO0lBQ0gsQ0FBQztJQUVPLHFDQUFTLEdBQWpCO1FBQUEsaUJBaUNDO1FBL0JDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDMUIsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNoRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDOUQsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5RCxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzVELE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0QsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM3RCxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQy9ELFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEUsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNoRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbEUsQ0FBQyxDQUFDO1FBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFakQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZO2FBQ3ZCLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUM7UUFDdkQsMkRBQTJEO0lBQzdELENBQUM7SUFFTywwQ0FBYyxHQUF0QixVQUF1QixPQUFPLEVBQUUsSUFBVTtRQUV4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUM3QixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQzdELElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEQsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBa0ZPLGlEQUFxQixHQUE3QjtRQUFBLGlCQTJDQztRQTFDQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLE1BQU0sQ0FBQztZQUNULENBQUM7UUFDSCxDQUFDO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7UUFDbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7UUFDOUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFFNUMsSUFBSSxNQUFNLEdBQUc7WUFDWCxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVk7WUFDNUIsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLGtCQUFrQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUNoRCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07U0FDdkIsQ0FBQztRQUVKLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUM1RCxJQUFJLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDckIsR0FBRyxDQUFDLENBQUMsSUFBTSxLQUFLLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsRCxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDYixNQUFNLENBQUM7WUFDVCxDQUFDO1lBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUN4RCxLQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFBQSxDQUFDO0lBRU0sZ0RBQW9CLEdBQTVCLFVBQTZCLE1BQU0sRUFBRSxJQUFJO1FBQXpDLGlCQXdDRTtRQXZDRSxJQUFJLEtBQUssR0FBRztZQUNWLFNBQVMsRUFBRSxNQUFNO1lBQ2pCLFNBQVMsRUFBRSxJQUFJO1NBQ2hCLENBQUM7UUFDRixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQ2xDLFVBQUEsUUFBUTtZQUVOLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFMUMsS0FBSSxDQUFDLE9BQU8sR0FBSTtvQkFDRyxPQUFPLEVBQUUsQ0FBQztvQkFDVixRQUFRLEVBQUMsQ0FBQztvQkFDVixLQUFLLEVBQUUsQ0FBQztvQkFDUixLQUFLLEVBQUUsQ0FBQztvQkFDUixHQUFHLEVBQUUsQ0FBQztvQkFDTixJQUFJLEVBQUUsQ0FBQztvQkFDUCxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsQ0FBQztvQkFDVCxTQUFTLEVBQUUsQ0FBQztvQkFDWixPQUFPLEVBQUUsQ0FBQztvQkFDVixRQUFRLEVBQUUsQ0FBQztvQkFDWCxRQUFRLEVBQUUsQ0FBQztpQkFDWixDQUFDO2dCQUNoQixLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVELENBQUM7WUFDRCxJQUFJLENBQUMsQ0FBQztnQkFDRixLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUUsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3JGLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN2RCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUM7WUFDeEMsQ0FBQztZQUVBLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNkLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBRTdCLENBQUMsRUFDZCxVQUFBLEtBQUs7WUFBTyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7WUFBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQy9DLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVRLDhDQUFrQixHQUExQjtRQUNHLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUN6RCxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU87WUFDdkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUc7WUFDakIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDbEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDbEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07WUFDcEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVM7WUFDdkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU87WUFDckIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7WUFDdEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXhDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRTNCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUMzRCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU87WUFDeEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUc7WUFDakIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDbEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDbEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07WUFDcEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVM7WUFDdkIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU87WUFDckIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7WUFDdEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXhDLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO0lBRWhDLENBQUM7SUFFTyw2Q0FBaUIsR0FBekI7UUFFSSxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDMUQsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXpCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUM3RCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDdEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7SUFFaEMsQ0FBQztJQUVPLHlDQUFhLEdBQXJCLFVBQXNCLE1BQU0sRUFBRSxJQUFJO1FBQWxDLGlCQWNBO1FBYkUsSUFBSSxLQUFLLEdBQUc7WUFDVixTQUFTLEVBQUUsTUFBTTtZQUNqQixTQUFTLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBQ0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUMzQixVQUFBLFFBQVE7WUFDVCx5Q0FBeUM7WUFDdkMsS0FBSSxDQUFDLFNBQVMsR0FBSSxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3RDLGtEQUFrRDtZQUNoRCxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUM1QixDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyx5Q0FBYSxHQUFyQjtRQUNLLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBRWhCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUksRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxFQUFFLElBQUksR0FBRyxDQUFDO2FBQ2YsQ0FBQyxDQUFDO1FBQ1QsQ0FBQztRQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFHSyxvQ0FBUSxHQUFmLFVBQWdCLEtBQVM7UUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDdEQsMkJBQTJCO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsS0FBSyxHQUFFLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRU0sbUNBQU8sR0FBZCxVQUFlLEtBQVM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0saUNBQUssR0FBWixVQUFhLEtBQVM7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU8sK0NBQW1CLEdBQTNCO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7UUFDMUIsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzVDLElBQUksUUFBUSxHQUFVLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2QsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUN2QyxDQUFDO2dCQUNHLEtBQUssSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7WUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBRUQsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFFM0IsQ0FBQztJQU9ILFNBQVM7SUFDRix3Q0FBWSxHQUFuQixVQUFvQixDQUFLO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDakIsQ0FBQztJQUVNLHdDQUFZLEdBQW5CLFVBQW9CLENBQUs7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqQixDQUFDO0lBL2RzQjtRQUF0QixnQkFBUyxDQUFDLFVBQVUsQ0FBQztrQ0FBZ0IsNEJBQWU7cURBQUM7SUFGM0MsaUJBQWlCO1FBUDVCLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsV0FBVztZQUNyQixXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxtREFBbUQsQ0FBQztZQUMvRSxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7WUFDOUUsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0EwRHlCLGtDQUFlO1lBQ3RCLDBCQUFXO1lBQ04sMENBQW1CO1lBQzFCLGVBQU07WUFDVixtQkFBVztZQUNDLGdDQUFjO09BOUQzQixpQkFBaUIsQ0F5ZTdCO0lBQUQsd0JBQUM7Q0F6ZUQsQUF5ZUMsSUFBQTtBQXplWSw4Q0FBaUIiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvdHVybi1vdmVyL3R1cm4tb3Zlci5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBOZ0Zvcm0sIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgUmVzdGF1cmFudFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvcmVzdGF1cmFudC5zZXJ2aWNlJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5pbXBvcnQge1R1cm5PdmVyU2VydmljZX0gZnJvbSAnYXBwL3NlcnZpY2VzL3R1cm5vdmVyLnNlcnZpY2UnO1xuIGltcG9ydCB7U2VsZWN0TW9kdWxlLFNlbGVjdENvbXBvbmVudH0gZnJvbSAnbmcyLXNlbGVjdCc7XG4gQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndHVybi1vdmVyJyxcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL3R1cm4tb3Zlci90dXJuLW92ZXIuY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvdHVybi1vdmVyL3R1cm4tb3Zlci5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJyd9XG59KVxuZXhwb3J0IGNsYXNzIFR1cm5PdmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBAVmlld0NoaWxkKCdTZWxlY3RJZCcpIHB1YmxpYyBzZWxlY3Q6IFNlbGVjdENvbXBvbmVudDtcblxuICBwcml2YXRlIHRva2VuIDogYW55O1xuICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgc2VsZWN0ZWRZZWFyOiBhbnk7XG4gIHByaXZhdGUgdHVybk92ZXJQbGFuaW5nOiBhbnlbXTtcbiAgcHJpdmF0ZSB0dXJuT3ZlcnMgOiBhbnlbXTtcbiAgcHJpdmF0ZSBteW1vZGVsO1xuICBwcml2YXRlIHR1cm5PdmVyQnlNb250aDogYW55W10gPSBbXTtcblxuICBwcml2YXRlIGR0Rm9ybTogRm9ybUdyb3VwO1xuICBwcml2YXRlIGphbnVhcnk6IEFic3RyYWN0Q29udHJvbDtcbiAgcHJpdmF0ZSBmZWJydWFyeTogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBtYXJjaDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBhcHJpbDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBtYXk6IEFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUganVuZTogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBqdWx5OiBBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIGF1Z3VzdDogQWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBzZXB0ZW1iZXI6QWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBvY3RvYmVyOkFic3RyYWN0Q29udHJvbCA9IG51bGw7XG4gIHByaXZhdGUgbm92ZW1iZXI6QWJzdHJhY3RDb250cm9sID0gbnVsbDtcbiAgcHJpdmF0ZSBkZWNlbWJlcjpBYnN0cmFjdENvbnRyb2wgPSBudWxsO1xuICBwcml2YXRlIGl0ZW1zOiBhbnlbXTtcbiAgcHJpdmF0ZSB2YWx1ZTphbnkgPSB7fTtcbiAgcHVibGljIGJhckNoYXJ0T3B0aW9uczphbnkgPSB7XG4gICAgc2NhbGVTaG93VmVydGljYWxMaW5lczogZmFsc2UsXG4gICAgcmVzcG9uc2l2ZTogdHJ1ZVxuICB9O1xuXG4gIHB1YmxpYyBiYXJDaGFydExhYmVsczpzdHJpbmdbXSA9IFsnSmFudWFyeScsICdGZWJydWFyeScsICdNYXJjaCcsICdBcHJpbCcsICdNYXknLCAnSnVuZScsICdKdWx5JywgJ0F1Z3VzdCcsICdTZXB0ZW1iZXInLCAnT2N0b2JlcicsICdOb3ZlbWJlcicsICdEZWNlbWJlciddO1xuICBwdWJsaWMgYmFyQ2hhcnRUeXBlOnN0cmluZyA9ICdiYXInO1xuICBwdWJsaWMgYmFyQ2hhcnRMZWdlbmQ6Ym9vbGVhbiA9IHRydWU7XG4gXG4gIHB1YmxpYyBiYXJDaGFydERhdGE6YW55W10gPSBbXG4gICAge2RhdGE6IFtdLCBsYWJlbDogJ1BsYW5pbmcnfSxcbiAgICB7ZGF0YTogW10sIGxhYmVsOiAnQWN0dWFsJ31cbiAgXTtcbiBcbiBcbiBwdWJsaWMgbGluZUNoYXJ0RGF0YTpBcnJheTxhbnk+ID0gW1xuICAgIHtkYXRhOiBbXSwgbGFiZWw6ICdQbGFuaW5nJ30sXG4gICAge2RhdGE6IFtdLCBsYWJlbDogJ0FjdHVhbCd9XG4gICBcbiAgXTtcbiAgcHVibGljIGxpbmVDaGFydExhYmVsczpBcnJheTxhbnk+ID0gWydKYW51YXJ5JywgJ0ZlYnJ1YXJ5JywgJ01hcmNoJywgJ0FwcmlsJywgJ01heScsICdKdW5lJywgJ0p1bHknLCAnQXVndXN0JywgJ1NlcHRlbWJlcicsICdPY3RvYmVyJywgJ05vdmVtYmVyJywgJ0RlY2VtYmVyJ107XG4gIHB1YmxpYyBsaW5lQ2hhcnRPcHRpb25zOmFueSA9IHtcbiAgICByZXNwb25zaXZlOiB0cnVlXG4gIH07XG4gXG4gIHB1YmxpYyBsaW5lQ2hhcnRMZWdlbmQ6Ym9vbGVhbiA9IHRydWU7XG4gIHB1YmxpYyBsaW5lQ2hhcnRUeXBlOnN0cmluZyA9ICdsaW5lJztcblxuICBjb25zdHJ1Y3RvcihcbiAgcHJpdmF0ZSB0dXJuT3ZlclNlcnZpY2U6IFR1cm5PdmVyU2VydmljZSxcbiAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UsXG4gIHByaXZhdGUgbm90aWZ5U2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gIHByaXZhdGUgU3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlKSB7IH0gIFxuXG4gIG5nT25Jbml0KCkgeyBcbiAgIFxuICAgIGxldCB1c2VySW5mbyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgICB0aGlzLmZldGNoQWxsWWVhcnMoKTtcbiAgICB0aGlzLnJlc3RJZCA9IHVzZXJJbmZvLnJlc3RhdXJhbnRzWzBdLmlkO1xuICAgIHRoaXMudXNlcklkID0gdXNlckluZm8udXNlcl9pbmZvLmlkO1xuICAgIGxldCBjdXJyZW50WWVhciA9IChuZXcgRGF0ZSgpKS5nZXRGdWxsWWVhcigpO1xuICAgIHRoaXMuc2VsZWN0ZWRZZWFyID0gY3VycmVudFllYXI7XG4gICAgdGhpcy5mZXRjaFR1cm5PdmVyUGxhbmluZyh0aGlzLnJlc3RJZCwgY3VycmVudFllYXIpO1xuICAgIHRoaXMuZmV0Y2hUdXJuT3Zlcih0aGlzLnJlc3RJZCwgY3VycmVudFllYXIpO1xuICAgXG4gICAgIHRoaXMuYnVpbGRGb3JtKCk7XG4gICAgIHRoaXMubXltb2RlbCA9IHtcbiAgICAgIGphbnVhcnk6IDAsXG4gICAgICBmZWJydWFyeTowLFxuICAgICAgbWFyY2g6IDAsXG4gICAgICBhcHJpbDogMCxcbiAgICAgIG1heTogMCxcbiAgICAgIGp1bmU6IDAsXG4gICAgICBqdWx5OiAwLFxuICAgICAgYXVndXN0OiAwLFxuICAgICAgc2VwdGVtYmVyOiAwLFxuICAgICAgb2N0b2JlcjogMCxcbiAgICAgIG5vdmVtYmVyOiAwLFxuICAgICAgZGVjZW1iZXI6IDBcbiAgICB9O1xuICAgXG4gIH0gXG5cbiAgIHByaXZhdGUgc2V0VmFyKCk6IHZvaWQge1xuXG4gICAgaWYgKHRoaXMubXltb2RlbCAhPSBudWxsKSB7XG4gICAgICB0aGlzLm15bW9kZWwgPSB7XG4gICAgICAgIGphbnVhcnk6IHRoaXMubXltb2RlbC5qYW51YXJ5LFxuICAgICAgICBmZWJydWFyeTogdGhpcy5teW1vZGVsLmZlYnJ1YXJ5LFxuICAgICAgICBtYXJjaDogdGhpcy5teW1vZGVsLm1hcmNoLFxuICAgICAgICBhcHJpbDogdGhpcy5teW1vZGVsLmFwcmlsLFxuICAgICAgICBtYXk6IHRoaXMubXltb2RlbC5tYXksXG4gICAgICAgIGp1bmU6IHRoaXMubXltb2RlbC5qdW5lLFxuICAgICAgICBqdWx5OiB0aGlzLm15bW9kZWwuanVseSxcbiAgICAgICAgYXVndXN0OiB0aGlzLm15bW9kZWwuYXVndXN0LFxuICAgICAgICBzZXB0ZW1iZXI6IHRoaXMubXltb2RlbC5zZXB0ZW1iZXIsXG4gICAgICAgIG9jdG9iZXI6IHRoaXMubXltb2RlbC5vY3RvYmVyLFxuICAgICAgICBub3ZlbWJlcjogdGhpcy5teW1vZGVsLm5vdmVtYmVyLFxuICAgICAgICBkZWNlbWJlcjogdGhpcy5teW1vZGVsLmRlY2VtYmVyXG4gICAgICB9O1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2phbnVhcnknXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuamFudWFyeSk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snZmVicnVhcnknXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuZmVicnVhcnkpO1xuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ21hcmNoJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLm1hcmNoKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydhcHJpbCddLnNldFZhbHVlKHRoaXMubXltb2RlbC5hcHJpbCk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snbWF5J10uc2V0VmFsdWUodGhpcy5teW1vZGVsLm1heSk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snanVuZSddLnNldFZhbHVlKHRoaXMubXltb2RlbC5qdW5lKTtcblxuICAgICAgdGhpcy5kdEZvcm0uY29udHJvbHNbJ2p1bHknXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuanVseSk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snYXVndXN0J10uc2V0VmFsdWUodGhpcy5teW1vZGVsLmF1Z3VzdCk7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snc2VwdGVtYmVyJ10uc2V0VmFsdWUodGhpcy5teW1vZGVsLnNlcHRlbWJlcik7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snb2N0b2JlciddLnNldFZhbHVlKHRoaXMubXltb2RlbC5vY3RvYmVyKTtcbiAgICAgIHRoaXMuZHRGb3JtLmNvbnRyb2xzWydub3ZlbWJlciddLnNldFZhbHVlKHRoaXMubXltb2RlbC5ub3ZlbWJlcik7XG4gICAgICB0aGlzLmR0Rm9ybS5jb250cm9sc1snZGVjZW1iZXInXS5zZXRWYWx1ZSh0aGlzLm15bW9kZWwuZGVjZW1iZXIpO1xuICAgICAgXG4gICAgICB0aGlzLmphbnVhcnkgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snamFudWFyeSddO1xuICAgICAgdGhpcy5mZWJydWFyeSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydmZWJydWFyeSddO1xuICAgICAgdGhpcy5tYXJjaCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydtYXJjaCddO1xuICAgICAgdGhpcy5hcHJpbCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydhcHJpbCddO1xuICAgICAgdGhpcy5tYXkgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snbWF5J107XG4gICAgICB0aGlzLmp1bmUgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snanVuZSddO1xuICAgICAgdGhpcy5qdWx5ID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2p1bHknXTtcbiAgICAgIHRoaXMuYXVndXN0ID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2F1Z3VzdCddO1xuICAgICAgdGhpcy5zZXB0ZW1iZXIgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snc2VwdGVtYmVyJ107XG4gICAgICB0aGlzLm9jdG9iZXIgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snb2N0b2JlciddO1xuICAgICAgdGhpcy5ub3ZlbWJlciA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydub3ZlbWJlciddO1xuICAgICAgdGhpcy5kZWNlbWJlciA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydkZWNlbWJlciddO1xuICAgICAgXG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBidWlsZEZvcm0oKTogdm9pZCB7XG5cbiAgICB0aGlzLmR0Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgICAgJ2phbnVhcnknOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMSldXSxcbiAgICAgICdmZWJydWFyeSc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDExKV1dLFxuICAgICAgJ21hcmNoJzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTEpXV0sXG4gICAgICAnYXByaWwnOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMSldXSxcbiAgICAgICdtYXknOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMSldXSxcbiAgICAgICdqdW5lJzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTEpXV0sXG4gICAgICAnanVseSc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDExKV1dLFxuICAgICAgJ2F1Z3VzdCc6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDExKV1dLFxuICAgICAgJ3NlcHRlbWJlcic6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDExKV1dLFxuICAgICAgJ29jdG9iZXInOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMSldXSxcbiAgICAgICdub3ZlbWJlcic6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDExKV1dLFxuICAgICAgJ2RlY2VtYmVyJzogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTEpXV1cbiAgICB9KTtcblxuICAgICAgdGhpcy5qYW51YXJ5ID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2phbnVhcnknXTtcbiAgICAgIHRoaXMuZmVicnVhcnkgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZmVicnVhcnknXTtcbiAgICAgIHRoaXMubWFyY2ggPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snbWFyY2gnXTtcbiAgICAgIHRoaXMuYXByaWwgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snYXByaWwnXTtcbiAgICAgIHRoaXMubWF5ID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ21heSddO1xuICAgICAgdGhpcy5qdW5lID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ2p1bmUnXTtcbiAgICAgIHRoaXMuanVseSA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydqdWx5J107XG4gICAgICB0aGlzLmF1Z3VzdCA9IHRoaXMuZHRGb3JtLmNvbnRyb2xzWydhdWd1c3QnXTtcbiAgICAgIHRoaXMuc2VwdGVtYmVyID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ3NlcHRlbWJlciddO1xuICAgICAgdGhpcy5vY3RvYmVyID0gdGhpcy5kdEZvcm0uY29udHJvbHNbJ29jdG9iZXInXTtcbiAgICAgIHRoaXMubm92ZW1iZXIgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snbm92ZW1iZXInXTtcbiAgICAgIHRoaXMuZGVjZW1iZXIgPSB0aGlzLmR0Rm9ybS5jb250cm9sc1snZGVjZW1iZXInXTtcblxuICAgICAgdGhpcy5kdEZvcm0udmFsdWVDaGFuZ2VzXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5vblZhbHVlQ2hhbmdlZChmYWxzZSwgZGF0YSkpO1xuICAgIC8vdGhpcy5vblZhbHVlQ2hhbmdlZCgpOyAvLyAocmUpc2V0IHZhbGlkYXRpb24gbWVzc2FnZXMgbm93XG4gIH1cblxuICBwcml2YXRlIG9uVmFsdWVDaGFuZ2VkKHVuRGlydHksIGRhdGE/OiBhbnkpIHtcblxuICAgIGlmICghdGhpcy5kdEZvcm0pIHsgcmV0dXJuOyB9XG4gICAgY29uc3QgZm9ybSA9IHRoaXMuZHRGb3JtO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICAvLyBjbGVhciBwcmV2aW91cyBlcnJvciBtZXNzYWdlIChpZiBhbnkpXG4gICAgICB0aGlzLmZvcm1FcnJvcnNbZmllbGRdID0gW107XG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybS5nZXQoZmllbGQpO1xuICAgICAgaWYgKGNvbnRyb2wgJiYgKGNvbnRyb2wuZGlydHkgfHwgdW5EaXJ0eSkgJiYgY29udHJvbC5pbnZhbGlkKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2VzID0gdGhpcy52YWxpZGF0aW9uTWVzc2FnZXNbZmllbGRdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBjb250cm9sLmVycm9ycykge1xuICAgICAgICAgIHRoaXMuZm9ybUVycm9yc1tmaWVsZF0ucHVzaChtZXNzYWdlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZvcm1FcnJvcnMgPSB7XG4gICAgJ2phbnVhcnknOiBbXSxcbiAgICAnZmVicnVhcnknOiBbXSxcbiAgICAnbWFyY2gnOiBbXSxcbiAgICAnYXByaWwnOiBbXSxcbiAgICAnbWF5JzogW10sXG4gICAgJ2p1bmUnOiBbXSxcbiAgICAnanVseSc6IFtdLFxuICAgICdhdWd1c3QnOiBbXSxcbiAgICAnc2VwdGVtYmVyJzogW10sXG4gICAgJ29jdG9iZXInOiBbXSxcbiAgICAnbm92ZW1iZXInOiBbXSxcbiAgICAnZGVjZW1iZXInOiBbXVxuXG4gIH07XG5cbiAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICdqYW51YXJ5Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIGZpZWxkIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDExIGNoYXJhY3RlcnMuJyxcbiAgICAgICdwYXR0ZXJuJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIG11c3QgYmUgYSBudW1iZXIuJ1xuICAgIH0sXG4gICAgJ2ZlYnJ1YXJ5Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIGZpZWxkIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDExIGNoYXJhY3RlcnMuJyxcbiAgICAgICdwYXR0ZXJuJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIG11c3QgYmUgYSBudW1iZXIuJ1xuICAgIH0sXG4gICAgJ21hcmNoJzoge1xuICAgICAncmVxdWlyZWQnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nLFxuICAgICAgJ3BhdHRlcm4nOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbXVzdCBiZSBhIG51bWJlci4nXG4gICAgfSxcbiAgICAnYXByaWwnOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nLFxuICAgICAgJ3BhdHRlcm4nOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbXVzdCBiZSBhIG51bWJlci4nXG4gICAgfSxcbiAgICAnbWF5Jzoge1xuICAgICAgJ3JlcXVpcmVkJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIGZpZWxkIGlzIHJlcXVpcmVkLicsXG4gICAgICAnbWF4bGVuZ3RoJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIG1heSBub3QgYmUgZ3JlYXRlciB0aGFuIDExIGNoYXJhY3RlcnMuJyxcbiAgICAgICdwYXR0ZXJuJzogJ1RoZSB0dXJub3ZlciBwbGFuaW5nIG11c3QgYmUgYSBudW1iZXIuJ1xuICAgIH0sXG4gICAgJ2p1bmUnOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nLFxuICAgICAgJ3BhdHRlcm4nOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbXVzdCBiZSBhIG51bWJlci4nXG4gICAgfSxcbiAgICAnanVseSc6IHtcbiAgICAgICAncmVxdWlyZWQnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nLFxuICAgICAgJ3BhdHRlcm4nOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbXVzdCBiZSBhIG51bWJlci4nXG4gICAgfSxcbiAgICAnYXVndXN0Jzoge1xuICAgICAncmVxdWlyZWQnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nLFxuICAgICAgJ3BhdHRlcm4nOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbXVzdCBiZSBhIG51bWJlci4nXG4gICAgfSxcbiAgICAnc2VwdGVtYmVyJzoge1xuICAgICAgICdyZXF1aXJlZCc6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxMSBjaGFyYWN0ZXJzLicsXG4gICAgICAncGF0dGVybic6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBtdXN0IGJlIGEgbnVtYmVyLidcbiAgICB9LFxuICAgICAnb2N0b2Jlcic6IHtcbiAgICAgICdyZXF1aXJlZCc6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxMSBjaGFyYWN0ZXJzLicsXG4gICAgICAncGF0dGVybic6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBtdXN0IGJlIGEgbnVtYmVyLidcbiAgICB9LFxuICAgICAnbm92ZW1iZXInOiB7XG4gICAgICAncmVxdWlyZWQnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgZmllbGQgaXMgcmVxdWlyZWQuJyxcbiAgICAgICdtYXhsZW5ndGgnOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbWF5IG5vdCBiZSBncmVhdGVyIHRoYW4gMTEgY2hhcmFjdGVycy4nLFxuICAgICAgJ3BhdHRlcm4nOiAnVGhlIHR1cm5vdmVyIHBsYW5pbmcgbXVzdCBiZSBhIG51bWJlci4nXG4gICAgfSxcbiAgICAgJ2RlY2VtYmVyJzoge1xuICAgICAgICdyZXF1aXJlZCc6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgJ21heGxlbmd0aCc6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBtYXkgbm90IGJlIGdyZWF0ZXIgdGhhbiAxMSBjaGFyYWN0ZXJzLicsXG4gICAgICAncGF0dGVybic6ICdUaGUgdHVybm92ZXIgcGxhbmluZyBtdXN0IGJlIGEgbnVtYmVyLidcbiAgICB9XG5cbiAgfTtcblxuICBwcml2YXRlIGRvU2F2ZVR1cm5PdmVyUGxhbmluZygpIHtcbiAgICB0aGlzLm9uVmFsdWVDaGFuZ2VkKHRydWUpO1xuICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5mb3JtRXJyb3JzKSB7XG4gICAgICBpZiAodGhpcy5mb3JtRXJyb3JzW2ZpZWxkXS5sZW5ndGggPiAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5teW1vZGVsLmphbnVhcnkgPSB0aGlzLmphbnVhcnkudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLmZlYnJ1YXJ5ID0gdGhpcy5mZWJydWFyeS52YWx1ZTtcbiAgICB0aGlzLm15bW9kZWwubWFyY2ggPSB0aGlzLm1hcmNoLnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5hcHJpbCA9IHRoaXMuYXByaWwudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLm1heSA9IHRoaXMubWF5LnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5qdW5lID0gdGhpcy5qdW5lLnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5qdWx5ID0gdGhpcy5qdWx5LnZhbHVlO1xuICAgIHRoaXMubXltb2RlbC5hdWd1c3QgPSB0aGlzLmF1Z3VzdC52YWx1ZTtcbiAgICB0aGlzLm15bW9kZWwuc2VwdGVtYmVyID0gdGhpcy5zZXB0ZW1iZXIudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLm9jdG9iZXIgPSB0aGlzLm9jdG9iZXIudmFsdWU7XG4gICAgdGhpcy5teW1vZGVsLm5vdmVtYmVyID0gdGhpcy5ub3ZlbWJlci52YWx1ZTtcbiAgICB0aGlzLm15bW9kZWwuZGVjZW1iZXIgPSB0aGlzLmRlY2VtYmVyLnZhbHVlO1xuXG4gICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICd5ZWFyX25vJzogdGhpcy5zZWxlY3RlZFllYXIsXG4gICAgICAncmVzdF9pZCc6IHRoaXMucmVzdElkLFxuICAgICAgJ3R1cm5vdmVyX3BsYW5pbmcnOiBKU09OLnN0cmluZ2lmeSh0aGlzLm15bW9kZWwpLFxuICAgICAgJ3VzZXJfaWQnOiB0aGlzLnVzZXJJZFxuICAgIH07XG5cbiAgdGhpcy50dXJuT3ZlclNlcnZpY2UudXBkYXRlVHVybm92ZXJQbGFuaW5nKHBhcmFtcykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBsZXQgaGFyRXJyb3IgPSBmYWxzZTtcbiAgICAgIGZvciAoY29uc3QgZmllbGQgaW4gcmVzcG9uc2UuZXJyb3JzKSB7XG4gICAgICAgIGhhckVycm9yID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JzW2ZpZWxkXSA9IHJlc3BvbnNlLmVycm9yc1tmaWVsZF07XG4gICAgICB9XG4gICAgICBpZiAoaGFyRXJyb3IpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5ub3RpZnlTZXJ2aWNlLnN1Y2Nlc3MoJ1R1cm4gb3ZlciBoYXMgYmVlbiB1cGRhdGVkJyk7XG4gICAgICAgdGhpcy5mZXRjaFR1cm5PdmVyUGxhbmluZyh0aGlzLnJlc3RJZCwgdGhpcy5zZWxlY3RlZFllYXIpO1xuICAgICAgdGhpcy5mZXRjaFR1cm5PdmVyKHRoaXMucmVzdElkLCB0aGlzLnNlbGVjdGVkWWVhcik7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuXG4gIH07XG5cbiAgcHJpdmF0ZSBmZXRjaFR1cm5PdmVyUGxhbmluZyhyZXN0SWQsIHllYXIpIHtcbiAgICAgIGxldCBwYXJhbSA9IHtcbiAgICAgICAgJ3Jlc3RfaWQnOiByZXN0SWQsXG4gICAgICAgICd5ZWFyX25vJzogeWVhclxuICAgICAgfTtcbiAgICAgIGxldCBzZWxmID0gdGhpczsgIFxuICBcdFx0dGhpcy50dXJuT3ZlclNlcnZpY2UuZ2V0VHVybm92ZXJQbGFuaW5nKHBhcmFtKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHtcbiAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICBpZihyZXNwb25zZS50dXJuX292ZXJfcGxhbmluZy5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5teW1vZGVsID0gIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgamFudWFyeTogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmVicnVhcnk6MCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyY2g6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcmlsOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXk6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1bmU6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1bHk6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1Z3VzdDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VwdGVtYmVyOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvY3RvYmVyOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBub3ZlbWJlcjogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVjZW1iZXI6IDBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50dXJuT3ZlclBsYW5pbmcgPSB0aGlzLm15bW9kZWw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncmVzcG9uc2UudHVybl9vdmVyX3BsYW5pbmcgJyx0aGlzLm15bW9kZWwpO1xuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50dXJuT3ZlclBsYW5pbmcgPSBKU09OLnBhcnNlKCByZXNwb25zZS50dXJuX292ZXJfcGxhbmluZ1swXS50dXJuX292ZXJfcGxhbmluZyk7XG4gICAgICAgICAgICAgICAgICAgICAgXHQgICBjb25zb2xlLmxvZygnZ2V0VHVybm92ZXJQbGFuaW5nICcsdGhpcy50dXJuT3ZlclBsYW5pbmcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5teW1vZGVsID0gdGhpcy50dXJuT3ZlclBsYW5pbmc7XG4gICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUGxhbmluZ0NoYXJ0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHsgIGFsZXJ0KCdvayBmZCcpOyBjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cblxuICAgIHByaXZhdGUgdXBkYXRlUGxhbmluZ0NoYXJ0KCkge1xuICAgICAgIGxldCBjbG9uZSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5iYXJDaGFydERhdGEpKTtcbiAgICAgICAgY2xvbmVbMF0uZGF0YSA9IFsrIHRoaXMubXltb2RlbC5qYW51YXJ5LFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5tYXJjaCxcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwuYXByaWwsXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLmFwcmlsLFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5tYXksXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLmp1bmUsXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLmp1bHksXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLmF1Z3VzdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwuc2VwdGVtYmVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5vY3RvYmVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5ub3ZlbWJlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwuZGVjZW1iZXJdO1xuICAgICAgICAgICAgICAgICAgXG4gICAgICAgIHRoaXMuYmFyQ2hhcnREYXRhID0gY2xvbmU7XG4gICAgICBcbiAgICAgICBsZXQgY2xvbmUxID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh0aGlzLmxpbmVDaGFydERhdGEpKTtcbiAgICAgICAgY2xvbmUxWzBdLmRhdGEgPSBbKyB0aGlzLm15bW9kZWwuamFudWFyeSxcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwubWFyY2gsXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLmFwcmlsLFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5hcHJpbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwubWF5LFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5qdW5lLFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5qdWx5LFxuICAgICAgICAgICAgICAgICAgICAgICAgK3RoaXMubXltb2RlbC5hdWd1c3QsXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLnNlcHRlbWJlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwub2N0b2JlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICt0aGlzLm15bW9kZWwubm92ZW1iZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICArdGhpcy5teW1vZGVsLmRlY2VtYmVyXTtcbiAgICAgICAgICAgICAgICAgIFxuICAgICAgICB0aGlzLmxpbmVDaGFydERhdGEgPSBjbG9uZTE7XG5cbiAgICB9XG5cbiAgICBwcml2YXRlIHVwZGF0ZUFjdHVhbENoYXJ0KCkge1xuXG4gICAgICAgIGxldCBjbG9uZSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5iYXJDaGFydERhdGEpKTtcbiAgICAgICAgY2xvbmVbMV0uZGF0YSA9IHRoaXMudHVybk92ZXJCeU1vbnRoO1xuICAgICAgICB0aGlzLmJhckNoYXJ0RGF0YSA9IGNsb25lO1xuXG4gICAgICAgICBsZXQgY2xvbmUxID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh0aGlzLmxpbmVDaGFydERhdGEpKTtcbiAgICAgICAgY2xvbmUxWzFdLmRhdGEgPSB0aGlzLnR1cm5PdmVyQnlNb250aDtcbiAgICAgICAgdGhpcy5saW5lQ2hhcnREYXRhID0gY2xvbmUxO1xuXG4gICAgfVxuXG4gICAgcHJpdmF0ZSBmZXRjaFR1cm5PdmVyKHJlc3RJZCwgeWVhcikge1xuICAgICAgbGV0IHBhcmFtID0ge1xuICAgICAgICAncmVzdF9pZCc6IHJlc3RJZCxcbiAgICAgICAgJ3llYXJfbm8nOiB5ZWFyXG4gICAgICB9O1xuICBcdFx0dGhpcy50dXJuT3ZlclNlcnZpY2UuZ2V0VHVybm92ZXIocGFyYW0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4ge1xuICAgICAgICAgICAgICAgICAgICAvLyBcdGNvbnNvbGUubG9nKCdnZXRUdXJub3ZlciAnLHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnR1cm5PdmVycyAgPSByZXNwb25zZS50dXJub3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgLy8gIGNvbnNvbGUubG9nKCd0aGlzLnR1cm5PdmVycyAnLHRoaXMudHVybk92ZXJzKTtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbGNUdXJuT3ZlckJ5TW9udGgoKTtcbiAgICAgICAgICAgICAgICAgICAgIH0sXG4gIFx0XHRcdFx0XHQgZXJyb3IgPT4ge2NvbnNvbGUubG9nKGVycm9yKVxuICBcdFx0XHRcdFx0fSk7XG4gIFx0fVxuXG4gICBwcml2YXRlIGZldGNoQWxsWWVhcnMoKSB7XG4gICAgICAgIHRoaXMuaXRlbXMgPSBbXTtcbiAgICAgICBcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCAgMTU7IGkrKykge1xuICAgICAgICAgICAgICB0aGlzLml0ZW1zLnB1c2goe1xuICAgICAgICAgICAgICAgIGlkOiBpLFxuICAgICAgICAgICAgICAgIHRleHQ6IDIwMTYgKyBpXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgdGhpcy5zZWxlY3QuaXRlbXMgPSB0aGlzLml0ZW1zOyBcbiAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5pdGVtc1swXTsgIFxuICBcdH1cblxuXG4gIHB1YmxpYyBzZWxlY3RlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIHRoaXMuZmV0Y2hUdXJuT3ZlclBsYW5pbmcodGhpcy5yZXN0SWQsIE51bWJlcih2YWx1ZS50ZXh0KSk7XG4gICAgdGhpcy5mZXRjaFR1cm5PdmVyKHRoaXMucmVzdElkLCAgTnVtYmVyKHZhbHVlLnRleHQpKTtcbiAgIC8vIGNvbnNvbGUubG9nKHZhbHVlLnRleHQpO1xuICAgIHRoaXMuc2VsZWN0ZWRZZWFyID0gdmFsdWUudGV4dDtcbiAgICB0aGlzLnZhbHVlID12YWx1ZTtcbiAgfVxuIFxuICBwdWJsaWMgcmVtb3ZlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdSZW1vdmVkIHZhbHVlIGlzOiAnLCB2YWx1ZSk7XG4gIH1cbiBcbiAgcHVibGljIHR5cGVkKHZhbHVlOmFueSk6dm9pZCB7XG4gICAgY29uc29sZS5sb2coJ05ldyBzZWFyY2ggaW5wdXQ6ICcsIHZhbHVlKTtcbiAgfVxuXG4gIHByaXZhdGUgY2FsY1R1cm5PdmVyQnlNb250aCgpIHtcbiAgICAgIHRoaXMudHVybk92ZXJCeU1vbnRoID0gW107XG4gICAgICBmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy50dXJuT3ZlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBsZXQgdHVybk92ZXI6IGFueVtdID0gdGhpcy50dXJuT3ZlcnNbaV07XG4gICAgICAgICAgbGV0IHRvdGFsID0gMDtcbiAgICAgICAgICBmb3IodmFyIGogPSAwOyBqIDwgdHVybk92ZXIubGVuZ3RoOyBqKyspXG4gICAgICAgICAge1xuICAgICAgICAgICAgICB0b3RhbCArPSBOdW1iZXIodHVybk92ZXJbal0ucHJpY2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBcbiAgICAgICAgICB0aGlzLnR1cm5PdmVyQnlNb250aC5wdXNoKHRvdGFsKTtcbiAgICAgIH1cbiAgICAgXG4gICAgICB0aGlzLnVwZGF0ZUFjdHVhbENoYXJ0KCk7XG4gICAgICAgXG4gICAgfVxuXG4gIFxuXG4gIFxuIFxuIFxuICAvLyBldmVudHNcbiAgcHVibGljIGNoYXJ0Q2xpY2tlZChlOmFueSk6dm9pZCB7XG4gICAgY29uc29sZS5sb2coZSk7XG4gIH1cbiBcbiAgcHVibGljIGNoYXJ0SG92ZXJlZChlOmFueSk6dm9pZCB7XG4gICAgY29uc29sZS5sb2coZSk7XG4gIH1cbiBcbiBcbiBcbiBcbiBcbiBcbiBcbn1cbiJdfQ==
