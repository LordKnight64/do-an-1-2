"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var optionValueFoodItem_service_1 = require("app/services/optionValueFoodItem.service");
var optionFoodItem_service_1 = require("app/services/optionFoodItem.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var OptionValueFoodItemsComponent = (function () {
    function OptionValueFoodItemsComponent(OptionFoodItemService, OptionValueFoodItemService, authServ, notif, router, StorageService) {
        this.OptionFoodItemService = OptionFoodItemService;
        this.OptionValueFoodItemService = OptionValueFoodItemService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.StorageService = StorageService;
    }
    OptionValueFoodItemsComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllOptionFoodItems(this.restId);
        this.fetchAllOptionValueFoodItems(this.restId);
    };
    OptionValueFoodItemsComponent.prototype.fetchAllOptionFoodItems = function (restId) {
        var _this = this;
        this.OptionFoodItemService.getOptionFoodItems(restId, null).then(function (response) {
            _this.optionFoodItems = response.options;
            console.log(response.options);
        }, function (error) {
            console.log(error);
        });
    };
    OptionValueFoodItemsComponent.prototype.fetchAllOptionValueFoodItems = function (restId) {
        var _this = this;
        this.OptionValueFoodItemService.getOptionValueFoodItems(restId, null).then(function (response) {
            _this.optionValueFoodItems = response.options;
            console.log(response.options);
        }, function (error) {
            console.log(error);
        });
    };
    OptionValueFoodItemsComponent.prototype.createOptionValueFoodItemForOption = function (item) {
        this.StorageService.setScope(item.id);
        this.router.navigateByUrl('/option-value-food-item/create');
    };
    OptionValueFoodItemsComponent.prototype.editOptionValueFoodItemForOption = function (itemvalue) {
        this.StorageService.setScope(itemvalue);
        this.router.navigateByUrl('/option-value-food-item/create/' + itemvalue.id);
    };
    OptionValueFoodItemsComponent.prototype.createOptionValueFoodItem = function () {
        this.StorageService.setScope(null);
        this.router.navigateByUrl('/option-value-food-item/create');
    };
    OptionValueFoodItemsComponent.prototype.deleteOptionFoodItem = function (item) {
        var _this = this;
        this.optionFoodItem = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'option': {
                'id': item.id,
                'name': item.name,
                'option_id': item.option_id,
                'is_delete': '1'
            }
        };
        this.OptionValueFoodItemService.createOptionValueFoodItems(this.optionFoodItem).then(function (response) { return _this.processResult(response); }, function (error) { return _this.failedCreate.bind(error); });
    };
    OptionValueFoodItemsComponent.prototype.processResult = function (response) {
        if (response.message == undefined || response.message == 'OK') {
            this.notif.success('New Option item has been added');
            this.fetchAllOptionFoodItems(this.restId);
            this.fetchAllOptionValueFoodItems(this.restId);
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    OptionValueFoodItemsComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    OptionValueFoodItemsComponent = __decorate([
        core_1.Component({
            selector: 'option-value-food-items',
            templateUrl: utils_1.default.getView('app/components/option-value-food-items/option-value-food-items.component.html'),
            styleUrls: [utils_1.default.getView('app/components/option-value-food-items/option-value-food-items.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [optionFoodItem_service_1.OptionFoodItemService,
            optionValueFoodItem_service_1.OptionValueFoodItemService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            storage_service_1.StorageService])
    ], OptionValueFoodItemsComponent);
    return OptionValueFoodItemsComponent;
}());
exports.OptionValueFoodItemsComponent = OptionValueFoodItemsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx5Q0FBb0M7QUFHcEMsNkRBQTJEO0FBQzNELHdGQUFzRjtBQUN0Riw4RUFBNEU7QUFDNUUsMERBQXlEO0FBQ3pELDJDQUE4QztBQUM5QywwRUFBd0U7QUFDeEUsMENBQXlDO0FBRXpDLGdFQUE4RDtBQVE5RDtJQVNFLHVDQUNRLHFCQUE0QyxFQUM1QywwQkFBc0QsRUFDdEQsUUFBcUIsRUFDckIsS0FBMEIsRUFDMUIsTUFBYyxFQUNkLGNBQThCO1FBTDlCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFDNUMsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUN0RCxhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLFVBQUssR0FBTCxLQUFLLENBQXFCO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7SUFBSSxDQUFDO0lBRTNDLGdEQUFRLEdBQVI7UUFDSyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRXBELENBQUM7SUFDTywrREFBdUIsR0FBL0IsVUFBZ0MsTUFBTTtRQUF0QyxpQkFRRTtRQVBBLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUM5QyxVQUFBLFFBQVE7WUFDUCxLQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNkLFVBQUEsS0FBSztZQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ00sb0VBQTRCLEdBQXBDLFVBQXFDLE1BQU07UUFBM0MsaUJBUUU7UUFQQSxJQUFJLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDeEQsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLG9CQUFvQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNkLFVBQUEsS0FBSztZQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0YsMEVBQWtDLEdBQWxDLFVBQW1DLElBQUk7UUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGdDQUFnQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUNELHdFQUFnQyxHQUFoQyxVQUFpQyxTQUFTO1FBQ3hDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGlDQUFpQyxHQUFHLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBQ0QsaUVBQXlCLEdBQXpCO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBQ0QsNERBQW9CLEdBQXBCLFVBQXFCLElBQUk7UUFBekIsaUJBYUM7UUFaRyxJQUFJLENBQUMsY0FBYyxHQUFHO1lBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBQyxJQUFJLENBQUMsTUFBTTtZQUNyQixRQUFRLEVBQUM7Z0JBQ1AsSUFBSSxFQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNaLE1BQU0sRUFBQyxJQUFJLENBQUMsSUFBSTtnQkFDaEIsV0FBVyxFQUFDLElBQUksQ0FBQyxTQUFTO2dCQUMxQixXQUFXLEVBQUMsR0FBRzthQUNoQjtTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsMEJBQTBCLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FDdkUsVUFBQSxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixFQUNwQyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUNPLHFEQUFhLEdBQXJCLFVBQXNCLFFBQVE7UUFFNUIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLHFCQUFJLENBQ0EsY0FBYyxFQUNkLE9BQU8sQ0FDUixDQUFDLEtBQUssQ0FBQyxxQkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7SUFDSCxDQUFDO0lBQ08sb0RBQVksR0FBcEIsVUFBc0IsR0FBRztRQUN2QixxQkFBSSxDQUNFLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBdkZVLDZCQUE2QjtRQVB6QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQywrRUFBK0UsQ0FBQztZQUMzRyxTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLDhFQUE4RSxDQUFDLENBQUM7WUFDMUcsVUFBVSxFQUFFLENBQUMsb0NBQWdCLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsRUFBQyxxQkFBcUIsRUFBRSxFQUFFLEVBQUM7U0FDbEMsQ0FBQzt5Q0FXK0IsOENBQXFCO1lBQ2hCLHdEQUEwQjtZQUM1QywwQkFBVztZQUNkLDBDQUFtQjtZQUNsQixlQUFNO1lBQ0UsZ0NBQWM7T0FmM0IsNkJBQTZCLENBd0Z6QztJQUFELG9DQUFDO0NBeEZELEFBd0ZDLElBQUE7QUF4Rlksc0VBQTZCIiwiZmlsZSI6ImFwcC9jb21wb25lbnRzL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyByb3V0ZXJGYWRlIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL29wdGlvblZhbHVlRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgeyBPcHRpb25Gb29kSXRlbVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvb3B0aW9uRm9vZEl0ZW0uc2VydmljZSc7XG5pbXBvcnQgIHsgQXV0aFNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IGRlZmF1bHQgYXMgc3dhbCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3N0b3JhZ2Uuc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdvcHRpb24tdmFsdWUtZm9vZC1pdGVtcycsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcHRpb24tdmFsdWUtZm9vZC1pdGVtcy9vcHRpb24tdmFsdWUtZm9vZC1pdGVtcy5jb21wb25lbnQuaHRtbCcpLFxuICBzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcHRpb24tdmFsdWUtZm9vZC1pdGVtcy9vcHRpb24tdmFsdWUtZm9vZC1pdGVtcy5jb21wb25lbnQuY3NzJyldLFxuICBhbmltYXRpb25zOiBbcm91dGVyVHJhbnNpdGlvbigpXSxcbiAgaG9zdDogeydbQHJvdXRlclRyYW5zaXRpb25dJzogJyd9XG59KVxuZXhwb3J0IGNsYXNzIE9wdGlvblZhbHVlRm9vZEl0ZW1zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcml2YXRlIHVzZXJJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSByZXN0SWQgOiBTdHJpbmc7XG4gIHByaXZhdGUgb3B0aW9uVmFsdWVGb29kSXRlbXMgOiBhbnk7XG4gIHByaXZhdGUgb3B0aW9uVmFsdWVGb29kSXRlbSA6IGFueTtcbiAgcHJpdmF0ZSBvcHRpb25Gb29kSXRlbXMgOiBhbnk7XG4gIHByaXZhdGUgb3B0aW9uRm9vZEl0ZW0gOiBhbnk7XG5cbiAgY29uc3RydWN0b3IoXG4gIHByaXZhdGUgT3B0aW9uRm9vZEl0ZW1TZXJ2aWNlOiBPcHRpb25Gb29kSXRlbVNlcnZpY2UsXG4gIHByaXZhdGUgT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2U6IE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlLFxuICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcbiAgcHJpdmF0ZSBub3RpZjogTm90aWZpY2F0aW9uU2VydmljZSxcbiAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgcHJpdmF0ZSBTdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgICAgIGxldCB1c2VySW5mbyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgICAgIHRoaXMucmVzdElkID0gdXNlckluZm8ucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgICAgdGhpcy51c2VySWQgPSB1c2VySW5mby51c2VyX2luZm8uaWQ7XG4gICAgXHQgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvblZhbHVlRm9vZEl0ZW1zKHRoaXMucmVzdElkKTtcblxuICB9XG4gIHByaXZhdGUgZmV0Y2hBbGxPcHRpb25Gb29kSXRlbXMocmVzdElkKSB7XG4gIFx0XHR0aGlzLk9wdGlvbkZvb2RJdGVtU2VydmljZS5nZXRPcHRpb25Gb29kSXRlbXMocmVzdElkLG51bGwpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5vcHRpb25Gb29kSXRlbXMgPSByZXNwb25zZS5vcHRpb25zO1xuICAgICAgICAgICAgICAgICAgICAgXHRjb25zb2xlLmxvZyhyZXNwb25zZS5vcHRpb25zKTsgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cbiAgcHJpdmF0ZSBmZXRjaEFsbE9wdGlvblZhbHVlRm9vZEl0ZW1zKHJlc3RJZCkge1xuICBcdFx0dGhpcy5PcHRpb25WYWx1ZUZvb2RJdGVtU2VydmljZS5nZXRPcHRpb25WYWx1ZUZvb2RJdGVtcyhyZXN0SWQsbnVsbCkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB7IFxuICAgICAgICAgICAgICAgICAgICAgXHR0aGlzLm9wdGlvblZhbHVlRm9vZEl0ZW1zID0gcmVzcG9uc2Uub3B0aW9ucztcbiAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2Uub3B0aW9ucyk7IFxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG4gIGNyZWF0ZU9wdGlvblZhbHVlRm9vZEl0ZW1Gb3JPcHRpb24oaXRlbSl7XG4gICAgdGhpcy5TdG9yYWdlU2VydmljZS5zZXRTY29wZShpdGVtLmlkKTtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS9jcmVhdGUnKTtcbiAgfVxuICBlZGl0T3B0aW9uVmFsdWVGb29kSXRlbUZvck9wdGlvbihpdGVtdmFsdWUpe1xuICAgIHRoaXMuU3RvcmFnZVNlcnZpY2Uuc2V0U2NvcGUoaXRlbXZhbHVlKTtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS9jcmVhdGUvJyArIGl0ZW12YWx1ZS5pZCk7XG4gIH1cbiAgY3JlYXRlT3B0aW9uVmFsdWVGb29kSXRlbSgpe1xuICAgIHRoaXMuU3RvcmFnZVNlcnZpY2Uuc2V0U2NvcGUobnVsbCk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL29wdGlvbi12YWx1ZS1mb29kLWl0ZW0vY3JlYXRlJyk7XG4gIH1cbiAgZGVsZXRlT3B0aW9uRm9vZEl0ZW0oaXRlbSl7XG4gICAgICB0aGlzLm9wdGlvbkZvb2RJdGVtID0ge1xuICAgICAgICAgICAgICAgICdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG4gICAgICAgICAgICAgICAgJ3VzZXJfaWQnOnRoaXMudXNlcklkLFxuICAgICAgICAgICAgICAgICdvcHRpb24nOntcbiAgICAgICAgICAgICAgICAgICdpZCc6aXRlbS5pZCxcbiAgICAgICAgICAgICAgICAgICduYW1lJzppdGVtLm5hbWUsXG4gICAgICAgICAgICAgICAgICAnb3B0aW9uX2lkJzppdGVtLm9wdGlvbl9pZCxcbiAgICAgICAgICAgICAgICAgICdpc19kZWxldGUnOicxJ1xuICAgICAgICAgICAgICAgIH19O1xuICAgICAgXHR0aGlzLk9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlLmNyZWF0ZU9wdGlvblZhbHVlRm9vZEl0ZW1zKHRoaXMub3B0aW9uRm9vZEl0ZW0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UpLFxuICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yID0+ICB0aGlzLmZhaWxlZENyZWF0ZS5iaW5kKGVycm9yKSk7XG4gIH1cbiAgcHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgICB0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ldyBPcHRpb24gaXRlbSBoYXMgYmVlbiBhZGRlZCcpO1xuICAgICAgdGhpcy5mZXRjaEFsbE9wdGlvbkZvb2RJdGVtcyh0aGlzLnJlc3RJZCk7XG4gICAgICB0aGlzLmZldGNoQWxsT3B0aW9uVmFsdWVGb29kSXRlbXModGhpcy5yZXN0SWQpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzd2FsKFxuICAgICAgICAgICdVcGRhdGUgRmFpbCEnLFxuICAgICAgICAgICdlcnJvcidcbiAgICAgICAgKS5jYXRjaChzd2FsLm5vb3ApO1xuICAgIH1cbiAgfVxuICBwcml2YXRlIGZhaWxlZENyZWF0ZSAocmVzKSB7XG4gICAgc3dhbChcbiAgICAgICAgICAnVXBkYXRlIEZhaWwhJyxcbiAgICAgICAgICAnZXJyb3InXG4gICAgICAgICkuY2F0Y2goc3dhbC5ub29wKTtcbiAgfVxufVxuIl19
