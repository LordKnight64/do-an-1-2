"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_animations_1 = require("../../router.animations");
var foodItem_service_1 = require("app/services/foodItem.service");
var auth_service_1 = require("app/services/auth.service");
var sweetalert2_1 = require("sweetalert2");
var notification_service_1 = require("app/services/notification.service");
var router_1 = require("@angular/router");
var storage_service_1 = require("app/services/storage.service");
var category_service_1 = require("app/services/category.service");
var ng2_select_1 = require("ng2-select");
var OrderFoodComponent = (function () {
    function OrderFoodComponent(FoodItemService, categoryService, authServ, notif, router, StorageService) {
        this.FoodItemService = FoodItemService;
        this.categoryService = categoryService;
        this.authServ = authServ;
        this.notif = notif;
        this.router = router;
        this.StorageService = StorageService;
        this.items = [];
        /*select start*/
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    OrderFoodComponent.prototype.ngOnInit = function () {
        var userInfo = JSON.parse(localStorage.getItem('user_key'));
        this.restId = userInfo.restaurants[0].id;
        this.userId = userInfo.user_info.id;
        this.fetchAllCategories(this.restId);
        var categoryId = null;
        if (this.value.id != undefined && this.value.id != null && this.value.id != 0) {
            categoryId = this.value.id;
        }
        this.fetchAllItem(this.restId, categoryId);
    };
    OrderFoodComponent.prototype.fetchAllItem = function (restId, categoryId) {
        var _this = this;
        this.FoodItemService.getFoodItems(restId, categoryId, null).then(function (response) {
            _this.itemLst = response.food_items;
            _this.itemLst = _this.itemLst.filter(function (item) { return item.active_flg === "1"; });
            console.log(" response.categories ", _this.itemLst);
        }, function (error) {
            console.log(error);
        });
    };
    OrderFoodComponent.prototype.fetchAllCategories = function (restId) {
        var _this = this;
        this.categoryService.getCategories(restId, null).then(function (response) {
            _this.categories = response.categories;
            _this.items.push({
                id: 0,
                text: "ALL"
            });
            for (var i = 0; i < _this.categories.length; i++) {
                _this.items.push({
                    id: _this.categories[i].id,
                    text: "" + _this.categories[i].name
                });
            }
            _this.select.items = _this.items;
            _this.value = _this.items[0];
            console.log(response.categories);
        }, function (error) {
            console.log(error);
        });
    };
    OrderFoodComponent.prototype.createItem = function () {
        this.StorageService.setScope(null);
        this.router.navigateByUrl('/food-item/create');
    };
    OrderFoodComponent.prototype.editItem = function (item) {
        this.StorageService.setScope(item);
        this.router.navigateByUrl('/food-item/edit/' + item.id);
    };
    OrderFoodComponent.prototype.deleteItem = function (item) {
        var _this = this;
        var params = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'item': {
                'id': item.id,
                'category_id': item.category_id,
                'name': item.name,
                'price': item.price,
                'thumb': '',
                'sku': item.sku,
                'is_active': item.active_flg,
                'is_alcoho': item.is_alcoho,
                'food_type': item.food_type,
                'description': item.description,
                'is_delete': 1,
                'option_items': item.option_items,
            }
        };
        this.FoodItemService.createFoodItems(params).then(function (response) { return _this.processResult(response, item.thumb); }, function (error) { return _this.failedCreate.bind(error); });
    };
    OrderFoodComponent.prototype.updateItem = function (item, active) {
        var _this = this;
        var params = {
            'rest_id': this.restId,
            'user_id': this.userId,
            'item': {
                'id': item.id,
                'category_id': item.category_id,
                'name': item.name,
                'price': item.price,
                'thumb': '',
                'point': item.point,
                'sku': item.sku,
                'is_active': active,
                'is_alcoho': item.is_alcoho,
                'food_type': item.food_type,
                'description': item.description,
                'is_delete': 0,
                'update_in_lst_flg': 1,
                'option_items': item.option_items,
            }
        };
        this.FoodItemService.createFoodItems(params).then(function (response) { return _this.processResult(response, null); }, function (error) { return _this.failedCreate.bind(error); });
    };
    OrderFoodComponent.prototype.processResult = function (response, thumb) {
        var _this = this;
        if (response.message == undefined || response.message == 'OK') {
            if (thumb != null) {
                this.FoodItemService.deleteFile({ 'thumb': thumb }).then(function (response) {
                    _this.notif.success('News has been deleted');
                }, function (error) {
                    console.log(error);
                });
            }
            this.notif.success('Item has been deleted');
            this.fetchAllItem(this.restId, this.value.id);
        }
        else {
            sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
        }
    };
    OrderFoodComponent.prototype.failedCreate = function (res) {
        sweetalert2_1.default('Update Fail!', 'error').catch(sweetalert2_1.default.noop);
    };
    Object.defineProperty(OrderFoodComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    OrderFoodComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
        var categoryId = null;
        if (this.value.id != undefined && this.value.id != null && this.value.id != 0) {
            categoryId = this.value.id;
        }
        this.fetchAllItem(this.restId, categoryId);
    };
    OrderFoodComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    OrderFoodComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    OrderFoodComponent.prototype.refreshValue = function (value) {
        if (value != undefined && value != null && value.length != 0) {
            this.value = value;
            var categoryId = null;
            if (this.value.id != undefined && this.value.id != null && this.value.id != 0) {
                categoryId = this.value.id;
            }
            this.fetchAllItem(this.restId, categoryId);
        }
        else {
            this.fetchAllItem(this.restId, null);
        }
    };
    __decorate([
        core_1.ViewChild('SelectId'),
        __metadata("design:type", ng2_select_1.SelectComponent)
    ], OrderFoodComponent.prototype, "select", void 0);
    OrderFoodComponent = __decorate([
        core_1.Component({
            selector: 'food-items-list',
            templateUrl: utils_1.default.getView('app/components/order-food/order-food.component.html'),
            styleUrls: [utils_1.default.getView('app/components/order-food/order-food.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [foodItem_service_1.FoodItemService,
            category_service_1.CategoryService,
            auth_service_1.AuthService,
            notification_service_1.NotificationService,
            router_1.Router,
            storage_service_1.StorageService])
    ], OrderFoodComponent);
    return OrderFoodComponent;
}());
exports.OrderFoodComponent = OrderFoodComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL29yZGVyLWZvb2Qvb3JkZXItZm9vZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBK0U7QUFDL0UseUNBQW9DO0FBR3BDLDZEQUEyRDtBQUMzRCxrRUFBZ0U7QUFDaEUsMERBQXlEO0FBQ3pELDJDQUE4QztBQUM5QywwRUFBd0U7QUFDeEUsMENBQXlDO0FBRXpDLGdFQUE4RDtBQUM5RCxrRUFBZ0U7QUFDaEUseUNBQXdEO0FBUXhEO0lBUUUsNEJBQW9CLGVBQStCLEVBQzNDLGVBQWdDLEVBQ2hDLFFBQXFCLEVBQ3JCLEtBQTBCLEVBQzFCLE1BQWMsRUFDZCxjQUE4QjtRQUxsQixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFDM0Msb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsVUFBSyxHQUFMLEtBQUssQ0FBcUI7UUFDMUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVg5QixVQUFLLEdBQWMsRUFBRSxDQUFDO1FBMEkvQixnQkFBZ0I7UUFFVCxVQUFLLEdBQU8sRUFBRSxDQUFDO1FBQ2IsZUFBVSxHQUFVLEdBQUcsQ0FBQztRQUN4QixhQUFRLEdBQVcsS0FBSyxDQUFDO0lBbklVLENBQUM7SUFFNUMscUNBQVEsR0FBUjtRQUNFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBRTVELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQztRQUN0QixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBRSxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDdEUsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1FBQzdCLENBQUM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNNLHlDQUFZLEdBQXBCLFVBQXFCLE1BQU0sRUFBQyxVQUFVO1FBQXRDLGlCQVNHO1FBUkEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQzdDLFVBQUEsUUFBUTtZQUNQLEtBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQztZQUNuQyxLQUFJLENBQUMsT0FBTyxHQUFJLEtBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFVBQVUsS0FBSyxHQUFHLEVBQXZCLENBQXVCLENBQUMsQ0FBQTtZQUNwRSxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixFQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSSwrQ0FBa0IsR0FBMUIsVUFBMkIsTUFBTTtRQUFqQyxpQkFvQkk7UUFuQkEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDbkMsVUFBQSxRQUFRO1lBQ1AsS0FBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNQLEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxLQUFLO2FBQ1osQ0FBQyxDQUFDO1lBQ1YsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUM1QyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztvQkFDZCxFQUFFLEVBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN6QixJQUFJLEVBQUUsS0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQU07aUJBQ25DLENBQUMsQ0FBQztZQUNSLENBQUM7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDO1lBQzlCLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQyxDQUFDLEVBQ2QsVUFBQSxLQUFLO1lBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFQSx1Q0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBQ0QscUNBQVEsR0FBUixVQUFTLElBQUk7UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsR0FBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUNMLHVDQUFVLEdBQVYsVUFBVyxJQUFJO1FBQWYsaUJBcUJDO1FBcEJDLElBQUksTUFBTSxHQUFHO1lBQ0gsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3RCLFNBQVMsRUFBQyxJQUFJLENBQUMsTUFBTTtZQUNwQixNQUFNLEVBQUM7Z0JBQ04sSUFBSSxFQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNaLGFBQWEsRUFBQyxJQUFJLENBQUMsV0FBVztnQkFDOUIsTUFBTSxFQUFDLElBQUksQ0FBQyxJQUFJO2dCQUNoQixPQUFPLEVBQUMsSUFBSSxDQUFDLEtBQUs7Z0JBQ2xCLE9BQU8sRUFBQyxFQUFFO2dCQUNWLEtBQUssRUFBQyxJQUFJLENBQUMsR0FBRztnQkFDZCxXQUFXLEVBQUMsSUFBSSxDQUFDLFVBQVU7Z0JBQzNCLFdBQVcsRUFBQyxJQUFJLENBQUMsU0FBUztnQkFDMUIsV0FBVyxFQUFDLElBQUksQ0FBQyxTQUFTO2dCQUMxQixhQUFhLEVBQUMsSUFBSSxDQUFDLFdBQVc7Z0JBQzlCLFdBQVcsRUFBQyxDQUFDO2dCQUNiLGNBQWMsRUFBQyxJQUFJLENBQUMsWUFBWTthQUNqQztTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3BDLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUF2QyxDQUF1QyxFQUMvQyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUNELHVDQUFVLEdBQVYsVUFBVyxJQUFJLEVBQUMsTUFBTTtRQUF0QixpQkF3QkM7UUF2QkUsSUFBSSxNQUFNLEdBQUc7WUFDSixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsU0FBUyxFQUFDLElBQUksQ0FBQyxNQUFNO1lBQ3BCLE1BQU0sRUFBQztnQkFDTixJQUFJLEVBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ1osYUFBYSxFQUFDLElBQUksQ0FBQyxXQUFXO2dCQUM5QixNQUFNLEVBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ2hCLE9BQU8sRUFBQyxJQUFJLENBQUMsS0FBSztnQkFDbEIsT0FBTyxFQUFDLEVBQUU7Z0JBQ1YsT0FBTyxFQUFDLElBQUksQ0FBQyxLQUFLO2dCQUNsQixLQUFLLEVBQUMsSUFBSSxDQUFDLEdBQUc7Z0JBQ2QsV0FBVyxFQUFDLE1BQU07Z0JBQ2xCLFdBQVcsRUFBQyxJQUFJLENBQUMsU0FBUztnQkFDMUIsV0FBVyxFQUFDLElBQUksQ0FBQyxTQUFTO2dCQUMxQixhQUFhLEVBQUMsSUFBSSxDQUFDLFdBQVc7Z0JBQzlCLFdBQVcsRUFBQyxDQUFDO2dCQUNiLG1CQUFtQixFQUFDLENBQUM7Z0JBQ3JCLGNBQWMsRUFBQyxJQUFJLENBQUMsWUFBWTthQUNqQztTQUFDLENBQUM7UUFDWixJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3BDLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLEVBQWpDLENBQWlDLEVBQ3pDLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLENBQUMsQ0FBQztJQUU5RCxDQUFDO0lBQ08sMENBQWEsR0FBckIsVUFBc0IsUUFBUSxFQUFDLEtBQUs7UUFBcEMsaUJBa0JHO1FBaEJDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksU0FBUyxJQUFJLFFBQVEsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUM5RCxFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUUsSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7b0JBQy9ELEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQzlDLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMvQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixxQkFBSSxDQUNBLGNBQWMsRUFDZCxPQUFPLENBQ1IsQ0FBQyxLQUFLLENBQUMscUJBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixDQUFDO0lBQ0gsQ0FBQztJQUNPLHlDQUFZLEdBQXBCLFVBQXNCLEdBQUc7UUFDdkIscUJBQUksQ0FDRSxjQUFjLEVBQ2QsT0FBTyxDQUNSLENBQUMsS0FBSyxDQUFDLHFCQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQU9ELHNCQUFZLHlDQUFTO2FBQXJCO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQzthQUVELFVBQXNCLEtBQVk7WUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxLQUFLLEdBQUcsQ0FBQztRQUMxQyxDQUFDOzs7T0FMQTtJQU9NLHFDQUFRLEdBQWYsVUFBZ0IsS0FBUztRQUV2QixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQztRQUN0QixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBRSxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDdEUsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1FBQzdCLENBQUM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVNLG9DQUFPLEdBQWQsVUFBZSxLQUFTO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLGtDQUFLLEdBQVosVUFBYSxLQUFTO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLHlDQUFZLEdBQW5CLFVBQW9CLEtBQVM7UUFDM0IsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFFLFNBQVMsSUFBSSxLQUFLLElBQUUsSUFBSSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNuQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDcEIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUUsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFFLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUN0RSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDN0IsQ0FBQztZQUNELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxVQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkMsQ0FBQztJQUNKLENBQUM7SUF2THNCO1FBQXRCLGdCQUFTLENBQUMsVUFBVSxDQUFDO2tDQUFnQiw0QkFBZTtzREFBQztJQUQzQyxrQkFBa0I7UUFQOUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMscURBQXFELENBQUM7WUFDakYsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1lBQ2hGLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUMscUJBQXFCLEVBQUUsRUFBRSxFQUFDO1NBQ2xDLENBQUM7eUNBU29DLGtDQUFlO1lBQzFCLGtDQUFlO1lBQ3RCLDBCQUFXO1lBQ2QsMENBQW1CO1lBQ2xCLGVBQU07WUFDRSxnQ0FBYztPQWIzQixrQkFBa0IsQ0F5TDlCO0lBQUQseUJBQUM7Q0F6TEQsQUF5TEMsSUFBQTtBQXpMWSxnREFBa0IiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvb3JkZXItZm9vZC9vcmRlci1mb29kLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LFZpZXdDaGlsZCxWaWV3RW5jYXBzdWxhdGlvbiAgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IHJvdXRlckZhZGUgfSBmcm9tICcuLi8uLi9yb3V0ZXIuYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyByb3V0ZXJUcmFuc2l0aW9uIH0gZnJvbSAnLi4vLi4vcm91dGVyLmFuaW1hdGlvbnMnO1xuaW1wb3J0IHsgRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2Zvb2RJdGVtLnNlcnZpY2UnO1xuaW1wb3J0ICB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBkZWZhdWx0IGFzIHN3YWwgfSBmcm9tICdzd2VldGFsZXJ0Mic7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2NhdGVnb3J5LnNlcnZpY2UnO1xuaW1wb3J0IHtTZWxlY3RNb2R1bGUsU2VsZWN0Q29tcG9uZW50fSBmcm9tICduZzItc2VsZWN0JztcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Zvb2QtaXRlbXMtbGlzdCcsXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcmRlci1mb29kL29yZGVyLWZvb2QuY29tcG9uZW50Lmh0bWwnKSxcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL2NvbXBvbmVudHMvb3JkZXItZm9vZC9vcmRlci1mb29kLmNvbXBvbmVudC5jc3MnKV0sXG4gIGFuaW1hdGlvbnM6IFtyb3V0ZXJUcmFuc2l0aW9uKCldLFxuICBob3N0OiB7J1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJ31cbn0pXG5leHBvcnQgY2xhc3MgT3JkZXJGb29kQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQFZpZXdDaGlsZCgnU2VsZWN0SWQnKSBwdWJsaWMgc2VsZWN0OiBTZWxlY3RDb21wb25lbnQ7XG4gIHByaXZhdGUgaXRlbXM6QXJyYXk8YW55PiA9IFtdO1xuICBwcml2YXRlIGl0ZW1Mc3QgOiBhbnk7XG4gIHByaXZhdGUgdXNlcklkIDogU3RyaW5nO1xuICBwcml2YXRlIHJlc3RJZCA6IFN0cmluZztcbiAgcHJpdmF0ZSBjYXRlZ29yeU9iamVjdCA6IGFueTtcbiAgcHJpdmF0ZSBjYXRlZ29yaWVzIDogYW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIEZvb2RJdGVtU2VydmljZTpGb29kSXRlbVNlcnZpY2UsXG4gIHByaXZhdGUgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsXG4gIHByaXZhdGUgYXV0aFNlcnY6IEF1dGhTZXJ2aWNlLFxuICBwcml2YXRlIG5vdGlmOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxuICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICBwcml2YXRlIFN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSwpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGxldCB1c2VySW5mbyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuICAgIFxuICAgIHRoaXMucmVzdElkID0gdXNlckluZm8ucmVzdGF1cmFudHNbMF0uaWQ7XG4gICAgdGhpcy51c2VySWQgPSB1c2VySW5mby51c2VyX2luZm8uaWQ7XG4gIFx0dGhpcy5mZXRjaEFsbENhdGVnb3JpZXModGhpcy5yZXN0SWQpO1xuICAgIGxldCBjYXRlZ29yeUlkID0gbnVsbDtcbiAgICBpZih0aGlzLnZhbHVlLmlkIT11bmRlZmluZWQgJiYgdGhpcy52YWx1ZS5pZCE9bnVsbCAmJiB0aGlzLnZhbHVlLmlkIT0wKXtcbiAgICAgIGNhdGVnb3J5SWQgPSB0aGlzLnZhbHVlLmlkO1xuICAgIH1cbiAgICB0aGlzLmZldGNoQWxsSXRlbSh0aGlzLnJlc3RJZCxjYXRlZ29yeUlkKTtcbiAgfVxuXHRwcml2YXRlIGZldGNoQWxsSXRlbShyZXN0SWQsY2F0ZWdvcnlJZCkge1xuICBcdFx0dGhpcy5Gb29kSXRlbVNlcnZpY2UuZ2V0Rm9vZEl0ZW1zKHJlc3RJZCxjYXRlZ29yeUlkLG51bGwpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgIFx0dGhpcy5pdGVtTHN0ID0gcmVzcG9uc2UuZm9vZF9pdGVtcztcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1Mc3QgPSAgdGhpcy5pdGVtTHN0LmZpbHRlcihpdGVtID0+IGl0ZW0uYWN0aXZlX2ZsZyA9PT0gXCIxXCIpXG4gICAgICAgICAgICAgICAgICAgICBcdGNvbnNvbGUubG9nKFwiIHJlc3BvbnNlLmNhdGVnb3JpZXMgXCIsIFx0dGhpcy5pdGVtTHN0KTsgXG4gICAgICAgICAgICAgICAgICAgICB9LFxuICBcdFx0XHRcdFx0IGVycm9yID0+IHtjb25zb2xlLmxvZyhlcnJvcilcbiAgXHRcdFx0XHRcdH0pO1xuICBcdH1cblxucHJpdmF0ZSBmZXRjaEFsbENhdGVnb3JpZXMocmVzdElkKSB7XG4gIFx0XHR0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yaWVzKHJlc3RJZCxudWxsKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgID0+IHsgXG4gICAgICAgICAgICAgICAgICAgICBcdHRoaXMuY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmNhdGVnb3JpZXM7XG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXRlbXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBgQUxMYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgIHRoaXMuY2F0ZWdvcmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHRoaXMuY2F0ZWdvcmllc1tpXS5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogYCR7dGhpcy5jYXRlZ29yaWVzW2ldLm5hbWV9YFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdC5pdGVtcyA9IHRoaXMuaXRlbXM7IFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuaXRlbXNbMF07ICBcbiAgICAgICAgICAgICAgICAgICAgIFx0Y29uc29sZS5sb2cocmVzcG9uc2UuY2F0ZWdvcmllcyk7IFxuICAgICAgICAgICAgICAgICAgICAgfSxcbiAgXHRcdFx0XHRcdCBlcnJvciA9PiB7Y29uc29sZS5sb2coZXJyb3IpXG4gIFx0XHRcdFx0XHR9KTtcbiAgXHR9XG5cbiAgICBjcmVhdGVJdGVtKCl7XG4gICAgICB0aGlzLlN0b3JhZ2VTZXJ2aWNlLnNldFNjb3BlKG51bGwpO1xuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL2Zvb2QtaXRlbS9jcmVhdGUnKTtcbiAgICB9XG4gICAgZWRpdEl0ZW0oaXRlbSl7XG4gICAgICAgdGhpcy5TdG9yYWdlU2VydmljZS5zZXRTY29wZShpdGVtKTtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9mb29kLWl0ZW0vZWRpdC8nKyBpdGVtLmlkKTtcbiAgICB9XG5kZWxldGVJdGVtKGl0ZW0pe1xuICBsZXQgcGFyYW1zID0ge1xuICAgICAgICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICAnaXRlbSc6e1xuICAgICAgICAgICAgICAnaWQnOml0ZW0uaWQsXG4gICAgICAgICAgICAgICdjYXRlZ29yeV9pZCc6aXRlbS5jYXRlZ29yeV9pZCxcbiAgICAgICAgICAgICAgJ25hbWUnOml0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgJ3ByaWNlJzppdGVtLnByaWNlLFxuICAgICAgICAgICAgICAndGh1bWInOicnLFxuICAgICAgICAgICAgICAnc2t1JzppdGVtLnNrdSxcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6aXRlbS5hY3RpdmVfZmxnLFxuICAgICAgICAgICAgICAnaXNfYWxjb2hvJzppdGVtLmlzX2FsY29obyxcbiAgICAgICAgICAgICAgJ2Zvb2RfdHlwZSc6aXRlbS5mb29kX3R5cGUsXG4gICAgICAgICAgICAgICdkZXNjcmlwdGlvbic6aXRlbS5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgJ2lzX2RlbGV0ZSc6MSxcbiAgICAgICAgICAgICAgJ29wdGlvbl9pdGVtcyc6aXRlbS5vcHRpb25faXRlbXMsXG4gICAgICAgICAgICB9fTtcbiAgXHR0aGlzLkZvb2RJdGVtU2VydmljZS5jcmVhdGVGb29kSXRlbXMocGFyYW1zKS50aGVuKFxuICAgICAgICAgICAgICAgIHJlc3BvbnNlICA9PiB0aGlzLnByb2Nlc3NSZXN1bHQocmVzcG9uc2UsaXRlbS50aHVtYiksXG4gICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiAgdGhpcy5mYWlsZWRDcmVhdGUuYmluZChlcnJvcikpO1xufVxudXBkYXRlSXRlbShpdGVtLGFjdGl2ZSl7XG4gICBsZXQgcGFyYW1zID0ge1xuICAgICAgICAgICAgJ3Jlc3RfaWQnOiB0aGlzLnJlc3RJZCxcbiAgICAgICAgICAgICd1c2VyX2lkJzp0aGlzLnVzZXJJZCxcbiAgICAgICAgICAgICAnaXRlbSc6e1xuICAgICAgICAgICAgICAnaWQnOml0ZW0uaWQsXG4gICAgICAgICAgICAgICdjYXRlZ29yeV9pZCc6aXRlbS5jYXRlZ29yeV9pZCxcbiAgICAgICAgICAgICAgJ25hbWUnOml0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgJ3ByaWNlJzppdGVtLnByaWNlLFxuICAgICAgICAgICAgICAndGh1bWInOicnLFxuICAgICAgICAgICAgICAncG9pbnQnOml0ZW0ucG9pbnQsXG4gICAgICAgICAgICAgICdza3UnOml0ZW0uc2t1LFxuICAgICAgICAgICAgICAnaXNfYWN0aXZlJzphY3RpdmUsXG4gICAgICAgICAgICAgICdpc19hbGNvaG8nOml0ZW0uaXNfYWxjb2hvLFxuICAgICAgICAgICAgICAnZm9vZF90eXBlJzppdGVtLmZvb2RfdHlwZSxcbiAgICAgICAgICAgICAgJ2Rlc2NyaXB0aW9uJzppdGVtLmRlc2NyaXB0aW9uLFxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzowLFxuICAgICAgICAgICAgICAndXBkYXRlX2luX2xzdF9mbGcnOjEsXG4gICAgICAgICAgICAgICdvcHRpb25faXRlbXMnOml0ZW0ub3B0aW9uX2l0ZW1zLFxuICAgICAgICAgICAgfX07XG4gIFx0dGhpcy5Gb29kSXRlbVNlcnZpY2UuY3JlYXRlRm9vZEl0ZW1zKHBhcmFtcykudGhlbihcbiAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlLG51bGwpLFxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuZmFpbGVkQ3JlYXRlLmJpbmQoZXJyb3IpKTtcblxufVxucHJpdmF0ZSBwcm9jZXNzUmVzdWx0KHJlc3BvbnNlLHRodW1iKSB7XG5cbiAgICBpZiAocmVzcG9uc2UubWVzc2FnZSA9PSB1bmRlZmluZWQgfHwgcmVzcG9uc2UubWVzc2FnZSA9PSAnT0snKSB7XG4gICAgICBpZih0aHVtYiE9bnVsbCl7XG4gICAgICAgIHRoaXMuRm9vZEl0ZW1TZXJ2aWNlLmRlbGV0ZUZpbGUoeyAndGh1bWInOiB0aHVtYiB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICB0aGlzLm5vdGlmLnN1Y2Nlc3MoJ05ld3MgaGFzIGJlZW4gZGVsZXRlZCcpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHRoaXMubm90aWYuc3VjY2VzcygnSXRlbSBoYXMgYmVlbiBkZWxldGVkJyk7XG4gICAgICB0aGlzLmZldGNoQWxsSXRlbSh0aGlzLnJlc3RJZCx0aGlzLnZhbHVlLmlkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc3dhbChcbiAgICAgICAgICAnVXBkYXRlIEZhaWwhJyxcbiAgICAgICAgICAnZXJyb3InXG4gICAgICAgICkuY2F0Y2goc3dhbC5ub29wKTtcbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBmYWlsZWRDcmVhdGUgKHJlcykge1xuICAgIHN3YWwoXG4gICAgICAgICAgJ1VwZGF0ZSBGYWlsIScsXG4gICAgICAgICAgJ2Vycm9yJ1xuICAgICAgICApLmNhdGNoKHN3YWwubm9vcCk7XG4gIH1cbiAvKnNlbGVjdCBzdGFydCovXG5cbnByaXZhdGUgdmFsdWU6YW55ID0ge307XG4gIHByaXZhdGUgX2Rpc2FibGVkVjpzdHJpbmcgPSAnMCc7XG4gIHByaXZhdGUgZGlzYWJsZWQ6Ym9vbGVhbiA9IGZhbHNlO1xuIFxuICBwcml2YXRlIGdldCBkaXNhYmxlZFYoKTpzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZFY7XG4gIH1cbiBcbiAgcHJpdmF0ZSBzZXQgZGlzYWJsZWRWKHZhbHVlOnN0cmluZykge1xuICAgIHRoaXMuX2Rpc2FibGVkViA9IHZhbHVlO1xuICAgIHRoaXMuZGlzYWJsZWQgPSB0aGlzLl9kaXNhYmxlZFYgPT09ICcxJztcbiAgfVxuIFxuICBwdWJsaWMgc2VsZWN0ZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICBcbiAgICBjb25zb2xlLmxvZygnU2VsZWN0ZWQgdmFsdWUgaXM6ICcsIHZhbHVlKTtcbiAgICBsZXQgY2F0ZWdvcnlJZCA9IG51bGw7XG4gICAgaWYodGhpcy52YWx1ZS5pZCE9dW5kZWZpbmVkICYmIHRoaXMudmFsdWUuaWQhPW51bGwgJiYgdGhpcy52YWx1ZS5pZCE9MCl7XG4gICAgICBjYXRlZ29yeUlkID0gdGhpcy52YWx1ZS5pZDtcbiAgICB9XG4gICAgdGhpcy5mZXRjaEFsbEl0ZW0odGhpcy5yZXN0SWQsY2F0ZWdvcnlJZCk7XG4gIH1cbiBcbiAgcHVibGljIHJlbW92ZWQodmFsdWU6YW55KTp2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnUmVtb3ZlZCB2YWx1ZSBpczogJywgdmFsdWUpO1xuICB9XG4gXG4gIHB1YmxpYyB0eXBlZCh2YWx1ZTphbnkpOnZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdOZXcgc2VhcmNoIGlucHV0OiAnLCB2YWx1ZSk7XG4gIH1cbiBcbiAgcHVibGljIHJlZnJlc2hWYWx1ZSh2YWx1ZTphbnkpOnZvaWQge1xuICAgIGlmKHZhbHVlIT11bmRlZmluZWQgJiYgdmFsdWUhPW51bGwgJiYgdmFsdWUubGVuZ3RoIT0wKXtcbiAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICAgIGxldCBjYXRlZ29yeUlkID0gbnVsbDtcbiAgICAgICAgaWYodGhpcy52YWx1ZS5pZCE9dW5kZWZpbmVkICYmIHRoaXMudmFsdWUuaWQhPW51bGwgJiYgdGhpcy52YWx1ZS5pZCE9MCl7XG4gICAgICAgICAgY2F0ZWdvcnlJZCA9IHRoaXMudmFsdWUuaWQ7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5mZXRjaEFsbEl0ZW0odGhpcy5yZXN0SWQsY2F0ZWdvcnlJZCk7XG4gICAgIH1lbHNle1xuICAgICAgICB0aGlzLmZldGNoQWxsSXRlbSh0aGlzLnJlc3RJZCxudWxsKTtcbiAgICAgfVxuICB9ICAgXG59XG4iXX0=
