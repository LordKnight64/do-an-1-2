"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var router_1 = require("@angular/router");
var router_animations_1 = require("../../router.animations");
var user_service_1 = require("app/services/user.service");
var storage_service_1 = require("app/services/storage.service");
var notification_service_1 = require("app/services/notification.service");
var geoLocation_service_1 = require("app/services/geoLocation.service");
var order_service_1 = require("app/services/order.service");
var common_1 = require("@angular/common");
var comCode_service_1 = require("app/services/comCode.service");
var email_service_1 = require("app/services/email.service");
var OrderingRequestComponent = (function () {
    function OrderingRequestComponent(userServ, orderServ, router, activatedRoute, storageServ, geoLocationServ, notifyServ, comCodeServ, location, sendMailSer) {
        this.userServ = userServ;
        this.orderServ = orderServ;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.storageServ = storageServ;
        this.geoLocationServ = geoLocationServ;
        this.notifyServ = notifyServ;
        this.comCodeServ = comCodeServ;
        this.location = location;
        this.sendMailSer = sendMailSer;
        this.deliveryMethod = "";
    }
    OrderingRequestComponent.prototype.ngOnInit = function () {
        var _this = this;
        alert("ok");
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
            this.userServ.logout();
        }
        else {
            this.userId = userKey.user_info.id;
            this.restId = userKey.restaurants[0].id;
        }
        this.id = this.activatedRoute.snapshot.params['id'];
        if (this.storageServ.scope) {
            this.order = this.storageServ.scope;
            this.comCodeServ.getCode({ 'cd_group': '0' }).then(function (response) {
                console.log('response ', response);
                var codes = response.code;
                for (var i = 0; i < codes.length; i++) {
                    if (_this.order.delivery_type == +codes[i].cd) {
                        _this.deliveryMethod = codes[i].cd_name;
                        console.log(_this.deliveryMethod);
                    }
                }
            }, function (error) {
                console.log(error);
            });
            this.setVar();
        }
        else {
            var params = {
                'user_id': this.userId,
                'rest_id': this.restId,
                'id': this.id
            };
            this.orderServ.getOrderList(params).then(function (response) {
                _this.order = response.orders[0];
                _this.setVar();
                _this.comCodeServ.getCode({ 'cd_group': '0' }).then(function (response) {
                    console.log('response ', response);
                    var codes = response.code;
                    for (var i = 0; i < codes.length; i++) {
                        if (_this.order.delivery_type == +codes[i].cd) {
                            _this.deliveryMethod = codes[i].cd_name;
                            console.log(_this.deliveryMethod);
                        }
                    }
                }, function (error) {
                    console.log(error);
                });
            }, function (error) {
                console.log(error);
            });
        }
        // this.sub = this.activatedRoute.params.subscribe(params => {
        // 	this.id = params['id'];
        // 	// In a real app: dispatch action to load the details here.
        // 	this.initMap();
        // });
    };
    OrderingRequestComponent.prototype.setVar = function () {
        var _this = this;
        if (this.order != null) {
            var delivery_fee = +this.order.delivery_fee;
            this.totalAmount = (this.order.total_price * (1 - this.order.promo_discount)) + delivery_fee;
            var params = {
                'source': {
                    'lat_val': this.order.rest_latitue,
                    'long_val': this.order.rest_longtitue
                },
                'destination': {
                    'lat_val': this.order.latitue,
                    'long_val': this.order.longtitue
                }
            };
            this.geoLocationServ.getDistance(params).then(function (response) {
                _this.distance = response[0].distance;
            }, function (error) {
                console.log(error);
            });
            this.initMap();
        }
    };
    OrderingRequestComponent.prototype.doUpdateOrder = function (orderStatus) {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'order_id': this.order.id,
            'status': orderStatus
        };
        this.orderServ.updateOrder(params).then(function (response) {
            _this.sendMail();
            _this.notifyServ.success('Order has been updated');
            _this.router.navigateByUrl('order/list');
            //this.goBack();
        }, function (error) {
            console.log(error);
        });
    };
    OrderingRequestComponent.prototype.sendMail = function () {
        var _this = this;
        var params = {
            'user_id': this.userId,
            'order_id': this.order.id,
        };
        this.sendMailSer.sendEmailAcceptOrder(params).then(function (response) {
            _this.notifyServ.success('Mail has been sent');
            var harError = false;
            for (var field in response.errors) {
                harError = true;
            }
            if (harError) {
                return;
            }
            console.log("SENT");
        }, function (error) {
            console.log(error);
        });
    };
    OrderingRequestComponent.prototype.goBack = function () {
        this.location.back();
    };
    OrderingRequestComponent.prototype.ngOnDestroy = function () {
        // this.sub.unsubscribe();
        this.storageServ.setScope(null);
    };
    OrderingRequestComponent.prototype.initMap = function () {
        var _this = this;
        this.mapObj = new google.maps.Map(document.getElementById('order-map'), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: 6
        });
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        //getCurrentPositionBrower(); // not run un localhost 
        // set markers current Location and store location for test only start
        //var positionCurrent = new google.maps.LatLng(10.8020047, 106.6413804);//(user used web on DB)
        var positionCurrent = new google.maps.LatLng(this.order.latitue, this.order.longtitue); //(user used web on DB)
        this.markerCurrentLocation = new google.maps.Marker({
            position: positionCurrent
        });
        //var positionStore = new google.maps.LatLng(10.8000144, 106.6590802);//lat long of restaurant
        var positionStore = new google.maps.LatLng(this.order.rest_latitue, this.order.rest_longtitue); //lat long of restaurant
        this.markerStoreLocation = new google.maps.Marker({
            position: positionStore
        });
        // var distance = new google.maps.geometry.spherical.computeDistanceBetween(positionCurrent, positionStore);
        directionsService.route({
            origin: this.markerCurrentLocation.getPosition(),
            destination: this.markerStoreLocation.getPosition(),
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(_this.mapObj);
                var startLocation = new Object();
                var endLocation = new Object();
                // Display start and end markers for the route.
                var legs = response.routes[0].legs;
                // for (i = 0; i < legs.length; i++) {
                //  if (i == 0) {
                //    startLocation.latlng = legs[i].start_location;
                //    startLocation.address = legs[i].start_address;
                //    // createMarker(legs[i].start_location, "start", legs[i].start_address, "green");
                //  }
                //  if (i == legs.length - 1) {
                //    endLocation.latlng = legs[i].end_location;
                //    endLocation.address = legs[i].end_address;
                //  }
                // }
                //    createInfoWindowGoogleChooseContent();
                //    var contentResult = $('#googleRightClickPopup').html();
                //    infowindowRouteResult = new google.maps.InfoWindow({
                //   content: contentResult,
                //   position:markerRouterB.getPosition()
                // });
                //    infowindowRouteResult.open(mapObj,markerRouterB);
            }
            else {
                //window.alert('Directions request failed due to ' + status);
            }
        });
        // set markers current Location and store location for test only end
    };
    OrderingRequestComponent.prototype.handleLocationError = function (browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    };
    OrderingRequestComponent.prototype.getCurrentPositionBrower = function () {
        var infoWindow = new google.maps.InfoWindow({ map: this.mapObj });
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                this.mapObj.setCenter(pos);
            }, function () {
                this.handleLocationError(true, infoWindow, this.mapObj.getCenter());
            });
        }
        else {
            // Browser doesn't support Geolocation
            this.handleLocationError(false, infoWindow, this.mapObj.getCenter());
        }
    };
    OrderingRequestComponent = __decorate([
        core_1.Component({
            selector: 'ordering-request',
            templateUrl: utils_1.default.getView('app/components/ordering-request/ordering-request.component.html'),
            styleUrls: [utils_1.default.getView('app/components/ordering-request/ordering-request.component.css')],
            animations: [router_animations_1.routerTransition()],
            host: { '[@routerTransition]': '' }
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            order_service_1.OrderService,
            router_1.Router,
            router_1.ActivatedRoute,
            storage_service_1.StorageService,
            geoLocation_service_1.GeoLocationService,
            notification_service_1.NotificationService,
            comCode_service_1.ComCodeService,
            common_1.Location,
            email_service_1.EmailService])
    ], OrderingRequestComponent);
    return OrderingRequestComponent;
}());
exports.OrderingRequestComponent = OrderingRequestComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb21wb25lbnRzL29yZGVyaW5nLXJlcXVlc3Qvb3JkZXJpbmctcmVxdWVzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQseUNBQW9DO0FBQ3BDLDBDQUF5RDtBQUN6RCw2REFBMkQ7QUFDM0QsMERBQXdEO0FBQ3hELGdFQUE4RDtBQUM5RCwwRUFBd0U7QUFDeEUsd0VBQXNFO0FBQ3RFLDREQUEwRDtBQUMxRCwwQ0FBMkM7QUFDM0MsZ0VBQThEO0FBQzlELDREQUEwRDtBQVUxRDtJQWNDLGtDQUNTLFFBQXFCLEVBQ3JCLFNBQXVCLEVBQ3ZCLE1BQWMsRUFDZCxjQUE4QixFQUM5QixXQUEyQixFQUMzQixlQUFtQyxFQUNuQyxVQUErQixFQUMvQixXQUEyQixFQUMzQixRQUFrQixFQUNsQixXQUF5QjtRQVR6QixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBQ3JCLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDM0Isb0JBQWUsR0FBZixlQUFlLENBQW9CO1FBQ25DLGVBQVUsR0FBVixVQUFVLENBQXFCO1FBQy9CLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUMzQixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLGdCQUFXLEdBQVgsV0FBVyxDQUFjO1FBWjFCLG1CQUFjLEdBQVEsRUFBRSxDQUFDO0lBYTdCLENBQUM7SUFFTCwyQ0FBUSxHQUFSO1FBQUEsaUJBOERDO1FBN0RBLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNaLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDeEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ1AsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3pDLENBQUM7UUFFRCxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7Z0JBQzVELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNuQyxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUMxQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztvQkFDdEMsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRyxDQUFDLENBQUMsQ0FBQzt3QkFDOUMsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO3dCQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUUsQ0FBQztvQkFDbkMsQ0FBQztnQkFDRixDQUFDO1lBRUYsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2YsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ1AsSUFBSSxNQUFNLEdBQUc7Z0JBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNO2dCQUN0QixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07Z0JBQ3RCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTthQUNiLENBQUM7WUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO2dCQUNoRCxLQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRWhDLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFFWixLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7b0JBQzNELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUNuQyxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUMxQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQzt3QkFDdEMsRUFBRSxDQUFBLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRyxDQUFDLENBQUMsQ0FBQzs0QkFDOUMsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDOzRCQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUUsQ0FBQzt3QkFDbkMsQ0FBQztvQkFDRixDQUFDO2dCQUVGLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEIsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEIsQ0FBQyxDQUFDLENBQUM7UUFDSixDQUFDO1FBRUQsOERBQThEO1FBQzlELDJCQUEyQjtRQUUzQiwrREFBK0Q7UUFDL0QsbUJBQW1CO1FBQ25CLE1BQU07SUFDUCxDQUFDO0lBRU8seUNBQU0sR0FBZDtRQUFBLGlCQXNCQztRQXBCQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztZQUM1QyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQztZQUM3RixJQUFJLE1BQU0sR0FBRztnQkFDWixRQUFRLEVBQUU7b0JBQ1QsU0FBUyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWTtvQkFDbEMsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYztpQkFDckM7Z0JBQ0QsYUFBYSxFQUFFO29CQUNkLFNBQVMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87b0JBQzdCLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVM7aUJBQ2hDO2FBQ0QsQ0FBQztZQUNGLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVE7Z0JBQ3JELEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUN0QyxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDaEIsQ0FBQztJQUNGLENBQUM7SUFFTyxnREFBYSxHQUFyQixVQUFzQixXQUFXO1FBQWpDLGlCQWVDO1FBYkEsSUFBSSxNQUFNLEdBQUc7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QixRQUFRLEVBQUUsV0FBVztTQUNyQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUMvQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUNsRCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN4QyxnQkFBZ0I7UUFDakIsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRU8sMkNBQVEsR0FBaEI7UUFBQSxpQkFtQkM7UUFqQkUsSUFBSSxNQUFNLEdBQUc7WUFDZCxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdEIsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtTQUN6QixDQUFDO1FBQ0EsSUFBSSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ3hELEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDOUMsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQU0sS0FBSyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNiLE1BQU0sQ0FBQztZQUNULENBQUM7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNGLHlDQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFDRiw4Q0FBVyxHQUFYO1FBQ0MsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCwwQ0FBTyxHQUFQO1FBQUEsaUJBdUVDO1FBdEVBLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3ZFLE1BQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFO1lBQ3RDLElBQUksRUFBRSxDQUFDO1NBQ1AsQ0FBQyxDQUFDO1FBRUgsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDMUQsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFM0Qsc0RBQXNEO1FBRXRELHNFQUFzRTtRQUN0RSwrRkFBK0Y7UUFDL0YsSUFBSSxlQUFlLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUEsdUJBQXVCO1FBQzlHLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ25ELFFBQVEsRUFBRSxlQUFlO1NBR3pCLENBQUMsQ0FBQztRQUNILDhGQUE4RjtRQUM5RixJQUFJLGFBQWEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQSx3QkFBd0I7UUFDdkgsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDakQsUUFBUSxFQUFFLGFBQWE7U0FHdkIsQ0FBQyxDQUFDO1FBRUgsNEdBQTRHO1FBRTVHLGlCQUFpQixDQUFDLEtBQUssQ0FBQztZQUN2QixNQUFNLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRTtZQUNoRCxXQUFXLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRTtZQUNuRCxVQUFVLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTztTQUMxQyxFQUFFLFVBQUMsUUFBUSxFQUFFLE1BQU07WUFDbkIsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDaEQsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMxQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLGFBQWEsR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO2dCQUNqQyxJQUFJLFdBQVcsR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO2dCQUMvQiwrQ0FBK0M7Z0JBQy9DLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNuQyxzQ0FBc0M7Z0JBQ3RDLGlCQUFpQjtnQkFDakIsb0RBQW9EO2dCQUNwRCxvREFBb0Q7Z0JBQ3BELHVGQUF1RjtnQkFDdkYsS0FBSztnQkFDTCwrQkFBK0I7Z0JBQy9CLGdEQUFnRDtnQkFDaEQsZ0RBQWdEO2dCQUNoRCxLQUFLO2dCQUNMLElBQUk7Z0JBR0osNENBQTRDO2dCQUM1Qyw2REFBNkQ7Z0JBRzdELDBEQUEwRDtnQkFDMUQsNEJBQTRCO2dCQUM1Qix5Q0FBeUM7Z0JBQ3pDLE1BQU07Z0JBRU4sdURBQXVEO1lBRXhELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDUCw2REFBNkQ7WUFDOUQsQ0FBQztRQUVGLENBQUMsQ0FBQyxDQUFDO1FBQ0gsb0VBQW9FO0lBQ3JFLENBQUM7SUFFRCxzREFBbUIsR0FBbkIsVUFBb0IscUJBQXFCLEVBQUUsVUFBVSxFQUFFLEdBQUc7UUFDekQsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixVQUFVLENBQUMsVUFBVSxDQUFDLHFCQUFxQjtZQUMxQyx3Q0FBd0M7WUFDeEMsbURBQW1ELENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsMkRBQXdCLEdBQXhCO1FBQ0MsSUFBSSxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUVsRSx5QkFBeUI7UUFDekIsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDM0IsU0FBUyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLFFBQVE7Z0JBQzFELElBQUksR0FBRyxHQUFHO29CQUNULEdBQUcsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVE7b0JBQzdCLEdBQUcsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVM7aUJBQzlCLENBQUM7Z0JBRUYsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUIsVUFBVSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUN6QyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QixDQUFDLEVBQUU7Z0JBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLENBQUMsQ0FBQyxDQUFDO1FBQ0osQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ1Asc0NBQXNDO1lBQ3RDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztRQUN0RSxDQUFDO0lBQ0YsQ0FBQztJQXJRVyx3QkFBd0I7UUFQcEMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsaUVBQWlFLENBQUM7WUFDN0YsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO1lBQzVGLFVBQVUsRUFBRSxDQUFDLG9DQUFnQixFQUFFLENBQUM7WUFDaEMsSUFBSSxFQUFFLEVBQUUscUJBQXFCLEVBQUUsRUFBRSxFQUFFO1NBQ25DLENBQUM7eUNBZ0JrQiwwQkFBVztZQUNWLDRCQUFZO1lBQ2YsZUFBTTtZQUNFLHVCQUFjO1lBQ2pCLGdDQUFjO1lBQ1Ysd0NBQWtCO1lBQ3ZCLDBDQUFtQjtZQUNsQixnQ0FBYztZQUNqQixpQkFBUTtZQUNMLDRCQUFZO09BeEJ0Qix3QkFBd0IsQ0FzUXBDO0lBQUQsK0JBQUM7Q0F0UUQsQUFzUUMsSUFBQTtBQXRRWSw0REFBd0IiLCJmaWxlIjoiYXBwL2NvbXBvbmVudHMvb3JkZXJpbmctcmVxdWVzdC9vcmRlcmluZy1yZXF1ZXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgcm91dGVyVHJhbnNpdGlvbiB9IGZyb20gJy4uLy4uL3JvdXRlci5hbmltYXRpb25zJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgR2VvTG9jYXRpb25TZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2dlb0xvY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgT3JkZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgQ29tQ29kZVNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvY29tQ29kZS5zZXJ2aWNlJztcbmltcG9ydCB7IEVtYWlsU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9lbWFpbC5zZXJ2aWNlJztcbmRlY2xhcmUgdmFyIGdvb2dsZTogYW55O1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdvcmRlcmluZy1yZXF1ZXN0Jyxcblx0dGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC9jb21wb25lbnRzL29yZGVyaW5nLXJlcXVlc3Qvb3JkZXJpbmctcmVxdWVzdC5jb21wb25lbnQuaHRtbCcpLFxuXHRzdHlsZVVybHM6IFtVdGlscy5nZXRWaWV3KCdhcHAvY29tcG9uZW50cy9vcmRlcmluZy1yZXF1ZXN0L29yZGVyaW5nLXJlcXVlc3QuY29tcG9uZW50LmNzcycpXSxcblx0YW5pbWF0aW9uczogW3JvdXRlclRyYW5zaXRpb24oKV0sXG5cdGhvc3Q6IHsgJ1tAcm91dGVyVHJhbnNpdGlvbl0nOiAnJyB9XG59KVxuZXhwb3J0IGNsYXNzIE9yZGVyaW5nUmVxdWVzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0cHJpdmF0ZSByZXN0SWQ7XG5cdHByaXZhdGUgdXNlcklkO1xuXHRwcml2YXRlIGlkOiBzdHJpbmc7XG5cdHByaXZhdGUgb3JkZXI6IGFueTtcblx0cHJpdmF0ZSBkaXN0YW5jZTogYW55O1xuXHRwcml2YXRlIHRvdGFsQW1vdW50OiBhbnk7XG5cdHByaXZhdGUgc3ViOiBhbnk7XG5cdHByaXZhdGUgbWFwT2JqOiBhbnk7XG5cdHByaXZhdGUgbWFya2VyQ3VycmVudExvY2F0aW9uOiBhbnk7XG5cdHByaXZhdGUgbWFya2VyU3RvcmVMb2NhdGlvbjogYW55O1xuXHRwcml2YXRlIGRlbGl2ZXJ5TWV0aG9kOiBhbnkgPSBcIlwiO1xuXHRwcml2YXRlIGdvb2dsZTogYW55O1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcblx0XHRwcml2YXRlIG9yZGVyU2VydjogT3JkZXJTZXJ2aWNlLFxuXHRcdHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG5cdFx0cHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG5cdFx0cHJpdmF0ZSBzdG9yYWdlU2VydjogU3RvcmFnZVNlcnZpY2UsXG5cdFx0cHJpdmF0ZSBnZW9Mb2NhdGlvblNlcnY6IEdlb0xvY2F0aW9uU2VydmljZSxcblx0XHRwcml2YXRlIG5vdGlmeVNlcnY6IE5vdGlmaWNhdGlvblNlcnZpY2UsXG5cdFx0cHJpdmF0ZSBjb21Db2RlU2VydjogQ29tQ29kZVNlcnZpY2UsXG5cdFx0cHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sXG5cdFx0cHJpdmF0ZSBzZW5kTWFpbFNlcjogRW1haWxTZXJ2aWNlXG5cdCkgeyB9XG5cblx0bmdPbkluaXQoKSB7XG5cdFx0YWxlcnQoXCJva1wiKTtcblx0XHRsZXQgdXNlcktleSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xuXHRcdGlmICghdXNlcktleSkge1xuXHRcdFx0dGhpcy51c2VyU2Vydi5sb2dvdXQoKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy51c2VySWQgPSB1c2VyS2V5LnVzZXJfaW5mby5pZDtcblx0XHRcdHRoaXMucmVzdElkID0gdXNlcktleS5yZXN0YXVyYW50c1swXS5pZDtcblx0XHR9XG5cblx0XHR0aGlzLmlkID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5wYXJhbXNbJ2lkJ107XG5cdFx0aWYgKHRoaXMuc3RvcmFnZVNlcnYuc2NvcGUpIHtcblx0XHRcdHRoaXMub3JkZXIgPSB0aGlzLnN0b3JhZ2VTZXJ2LnNjb3BlO1xuXHRcdFx0ICB0aGlzLmNvbUNvZGVTZXJ2LmdldENvZGUoeyAnY2RfZ3JvdXAnOiAnMCcgfSkudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKCdyZXNwb25zZSAnLCByZXNwb25zZSk7IFxuXHRcdFx0XHRsZXQgY29kZXMgPSByZXNwb25zZS5jb2RlO1xuXHRcdFx0XHRmb3IobGV0IGkgPSAwOyBpIDwgY29kZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRpZih0aGlzLm9yZGVyLmRlbGl2ZXJ5X3R5cGUgPT0gK2NvZGVzW2ldLmNkICkge1xuXHRcdFx0XHRcdFx0dGhpcy5kZWxpdmVyeU1ldGhvZCA9IGNvZGVzW2ldLmNkX25hbWU7XG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyh0aGlzLmRlbGl2ZXJ5TWV0aG9kICk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFxuXHRcdFx0fSwgZXJyb3IgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XG5cdFx0XHR9KTtcblx0XHRcdHRoaXMuc2V0VmFyKCk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHZhciBwYXJhbXMgPSB7XG5cdFx0XHRcdCd1c2VyX2lkJzogdGhpcy51c2VySWQsXG5cdFx0XHRcdCdyZXN0X2lkJzogdGhpcy5yZXN0SWQsXG5cdFx0XHRcdCdpZCc6IHRoaXMuaWRcblx0XHRcdH07XG5cdFx0XHR0aGlzLm9yZGVyU2Vydi5nZXRPcmRlckxpc3QocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0dGhpcy5vcmRlciA9IHJlc3BvbnNlLm9yZGVyc1swXTtcblx0XHRcdCAgICBcblx0XHRcdFx0dGhpcy5zZXRWYXIoKTtcbiAgICAgICAgICAgICAgICAgXG5cdFx0XHRcdCAgdGhpcy5jb21Db2RlU2Vydi5nZXRDb2RlKHsgJ2NkX2dyb3VwJzogJzAnIH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ3Jlc3BvbnNlICcsIHJlc3BvbnNlKTsgXG5cdFx0XHRcdFx0XHRsZXQgY29kZXMgPSByZXNwb25zZS5jb2RlO1xuXHRcdFx0XHRcdFx0Zm9yKGxldCBpID0gMDsgaSA8IGNvZGVzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRcdFx0XHRcdGlmKHRoaXMub3JkZXIuZGVsaXZlcnlfdHlwZSA9PSArY29kZXNbaV0uY2QgKSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5kZWxpdmVyeU1ldGhvZCA9IGNvZGVzW2ldLmNkX25hbWU7XG5cdFx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2codGhpcy5kZWxpdmVyeU1ldGhvZCApO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcblx0XHRcdFx0XHR9LCBlcnJvciA9PiB7XG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHR9LCBlcnJvciA9PiB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKGVycm9yKTtcblx0XHRcdH0pO1xuXHRcdH1cblxuXHRcdC8vIHRoaXMuc3ViID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5wYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG5cdFx0Ly8gXHR0aGlzLmlkID0gcGFyYW1zWydpZCddO1xuXG5cdFx0Ly8gXHQvLyBJbiBhIHJlYWwgYXBwOiBkaXNwYXRjaCBhY3Rpb24gdG8gbG9hZCB0aGUgZGV0YWlscyBoZXJlLlxuXHRcdC8vIFx0dGhpcy5pbml0TWFwKCk7XG5cdFx0Ly8gfSk7XG5cdH1cblxuXHRwcml2YXRlIHNldFZhcigpOiB2b2lkIHtcblxuXHRcdGlmICh0aGlzLm9yZGVyICE9IG51bGwpIHtcblx0XHRcdGxldCBkZWxpdmVyeV9mZWUgPSArdGhpcy5vcmRlci5kZWxpdmVyeV9mZWU7XG5cdFx0XHR0aGlzLnRvdGFsQW1vdW50ID0gKHRoaXMub3JkZXIudG90YWxfcHJpY2UgKiAoMSAtIHRoaXMub3JkZXIucHJvbW9fZGlzY291bnQpKSArIGRlbGl2ZXJ5X2ZlZTtcblx0XHRcdHZhciBwYXJhbXMgPSB7XG5cdFx0XHRcdCdzb3VyY2UnOiB7XG5cdFx0XHRcdFx0J2xhdF92YWwnOiB0aGlzLm9yZGVyLnJlc3RfbGF0aXR1ZSxcblx0XHRcdFx0XHQnbG9uZ192YWwnOiB0aGlzLm9yZGVyLnJlc3RfbG9uZ3RpdHVlXG5cdFx0XHRcdH0sXG5cdFx0XHRcdCdkZXN0aW5hdGlvbic6IHtcblx0XHRcdFx0XHQnbGF0X3ZhbCc6IHRoaXMub3JkZXIubGF0aXR1ZSxcblx0XHRcdFx0XHQnbG9uZ192YWwnOiB0aGlzLm9yZGVyLmxvbmd0aXR1ZVxuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdFx0dGhpcy5nZW9Mb2NhdGlvblNlcnYuZ2V0RGlzdGFuY2UocGFyYW1zKS50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0dGhpcy5kaXN0YW5jZSA9IHJlc3BvbnNlWzBdLmRpc3RhbmNlO1xuXHRcdFx0fSwgZXJyb3IgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XG5cdFx0XHR9KTtcblx0XHRcdHRoaXMuaW5pdE1hcCgpO1xuXHRcdH1cblx0fVxuXG5cdHByaXZhdGUgZG9VcGRhdGVPcmRlcihvcmRlclN0YXR1cykge1xuXG5cdFx0dmFyIHBhcmFtcyA9IHtcblx0XHRcdCd1c2VyX2lkJzogdGhpcy51c2VySWQsXG5cdFx0XHQnb3JkZXJfaWQnOiB0aGlzLm9yZGVyLmlkLFxuXHRcdFx0J3N0YXR1cyc6IG9yZGVyU3RhdHVzXG5cdFx0fTtcblx0XHR0aGlzLm9yZGVyU2Vydi51cGRhdGVPcmRlcihwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0dGhpcy5zZW5kTWFpbCgpO1xuXHRcdFx0dGhpcy5ub3RpZnlTZXJ2LnN1Y2Nlc3MoJ09yZGVyIGhhcyBiZWVuIHVwZGF0ZWQnKTtcblx0XHRcdHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ29yZGVyL2xpc3QnKTtcblx0XHRcdC8vdGhpcy5nb0JhY2soKTtcblx0XHR9LCBlcnJvciA9PiB7XG5cdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XG5cdFx0fSk7XG5cdH1cblxuXHRwcml2YXRlIHNlbmRNYWlsKClcblx0e1x0XHRcblx0XHQgIHZhciBwYXJhbXMgPSB7XG5cdFx0XHQndXNlcl9pZCc6IHRoaXMudXNlcklkLFxuXHRcdFx0J29yZGVyX2lkJzogdGhpcy5vcmRlci5pZCxcblx0XHR9O1xuXHRcdCAgdGhpcy5zZW5kTWFpbFNlci5zZW5kRW1haWxBY2NlcHRPcmRlcihwYXJhbXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHQgICAgICB0aGlzLm5vdGlmeVNlcnYuc3VjY2VzcygnTWFpbCBoYXMgYmVlbiBzZW50Jyk7XG5cdCAgICAgIGxldCBoYXJFcnJvciA9IGZhbHNlO1xuXHQgICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHJlc3BvbnNlLmVycm9ycykge1xuXHQgICAgICAgIGhhckVycm9yID0gdHJ1ZTtcblx0ICAgICAgfVxuXHQgICAgICBpZiAoaGFyRXJyb3IpIHtcblx0ICAgICAgICByZXR1cm47XG5cdCAgICAgIH1cblx0ICAgICAgY29uc29sZS5sb2coXCJTRU5UXCIpO1xuXHQgICAgfSwgZXJyb3IgPT4ge1xuXHQgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG5cdCAgICB9KTtcblx0fVxuZ29CYWNrKCk6IHZvaWQge1xuICAgIHRoaXMubG9jYXRpb24uYmFjaygpO1xuICB9XG5cdG5nT25EZXN0cm95KCkge1xuXHRcdC8vIHRoaXMuc3ViLnVuc3Vic2NyaWJlKCk7XG5cdFx0dGhpcy5zdG9yYWdlU2Vydi5zZXRTY29wZShudWxsKTtcblx0fVxuXG5cdGluaXRNYXAoKSB7XG5cdFx0dGhpcy5tYXBPYmogPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdvcmRlci1tYXAnKSwge1xuXHRcdFx0Y2VudGVyOiB7IGxhdDogLTM0LjM5NywgbG5nOiAxNTAuNjQ0IH0sXG5cdFx0XHR6b29tOiA2XG5cdFx0fSk7XG5cblx0XHR2YXIgZGlyZWN0aW9uc1NlcnZpY2UgPSBuZXcgZ29vZ2xlLm1hcHMuRGlyZWN0aW9uc1NlcnZpY2U7XG5cdFx0dmFyIGRpcmVjdGlvbnNEaXNwbGF5ID0gbmV3IGdvb2dsZS5tYXBzLkRpcmVjdGlvbnNSZW5kZXJlcjtcblxuXHRcdC8vZ2V0Q3VycmVudFBvc2l0aW9uQnJvd2VyKCk7IC8vIG5vdCBydW4gdW4gbG9jYWxob3N0IFxuXG5cdFx0Ly8gc2V0IG1hcmtlcnMgY3VycmVudCBMb2NhdGlvbiBhbmQgc3RvcmUgbG9jYXRpb24gZm9yIHRlc3Qgb25seSBzdGFydFxuXHRcdC8vdmFyIHBvc2l0aW9uQ3VycmVudCA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoMTAuODAyMDA0NywgMTA2LjY0MTM4MDQpOy8vKHVzZXIgdXNlZCB3ZWIgb24gREIpXG5cdFx0dmFyIHBvc2l0aW9uQ3VycmVudCA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcodGhpcy5vcmRlci5sYXRpdHVlLCB0aGlzLm9yZGVyLmxvbmd0aXR1ZSk7Ly8odXNlciB1c2VkIHdlYiBvbiBEQilcblx0XHR0aGlzLm1hcmtlckN1cnJlbnRMb2NhdGlvbiA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuXHRcdFx0cG9zaXRpb246IHBvc2l0aW9uQ3VycmVudFxuXG5cblx0XHR9KTtcblx0XHQvL3ZhciBwb3NpdGlvblN0b3JlID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZygxMC44MDAwMTQ0LCAxMDYuNjU5MDgwMik7Ly9sYXQgbG9uZyBvZiByZXN0YXVyYW50XG5cdFx0dmFyIHBvc2l0aW9uU3RvcmUgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHRoaXMub3JkZXIucmVzdF9sYXRpdHVlLCB0aGlzLm9yZGVyLnJlc3RfbG9uZ3RpdHVlKTsvL2xhdCBsb25nIG9mIHJlc3RhdXJhbnRcblx0XHR0aGlzLm1hcmtlclN0b3JlTG9jYXRpb24gPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcblx0XHRcdHBvc2l0aW9uOiBwb3NpdGlvblN0b3JlXG5cblxuXHRcdH0pO1xuXG5cdFx0Ly8gdmFyIGRpc3RhbmNlID0gbmV3IGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKHBvc2l0aW9uQ3VycmVudCwgcG9zaXRpb25TdG9yZSk7XG5cblx0XHRkaXJlY3Rpb25zU2VydmljZS5yb3V0ZSh7XG5cdFx0XHRvcmlnaW46IHRoaXMubWFya2VyQ3VycmVudExvY2F0aW9uLmdldFBvc2l0aW9uKCksXG5cdFx0XHRkZXN0aW5hdGlvbjogdGhpcy5tYXJrZXJTdG9yZUxvY2F0aW9uLmdldFBvc2l0aW9uKCksXG5cdFx0XHR0cmF2ZWxNb2RlOiBnb29nbGUubWFwcy5UcmF2ZWxNb2RlLkRSSVZJTkdcblx0XHR9LCAocmVzcG9uc2UsIHN0YXR1cykgPT4ge1xuXHRcdFx0aWYgKHN0YXR1cyA9PT0gZ29vZ2xlLm1hcHMuRGlyZWN0aW9uc1N0YXR1cy5PSykge1xuXHRcdFx0XHRkaXJlY3Rpb25zRGlzcGxheS5zZXREaXJlY3Rpb25zKHJlc3BvbnNlKTtcblx0XHRcdFx0ZGlyZWN0aW9uc0Rpc3BsYXkuc2V0TWFwKHRoaXMubWFwT2JqKTtcblx0XHRcdFx0bGV0IHN0YXJ0TG9jYXRpb24gPSBuZXcgT2JqZWN0KCk7XG5cdFx0XHRcdGxldCBlbmRMb2NhdGlvbiA9IG5ldyBPYmplY3QoKTtcblx0XHRcdFx0Ly8gRGlzcGxheSBzdGFydCBhbmQgZW5kIG1hcmtlcnMgZm9yIHRoZSByb3V0ZS5cblx0XHRcdFx0bGV0IGxlZ3MgPSByZXNwb25zZS5yb3V0ZXNbMF0ubGVncztcblx0XHRcdFx0Ly8gZm9yIChpID0gMDsgaSA8IGxlZ3MubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0Ly8gIGlmIChpID09IDApIHtcblx0XHRcdFx0Ly8gICAgc3RhcnRMb2NhdGlvbi5sYXRsbmcgPSBsZWdzW2ldLnN0YXJ0X2xvY2F0aW9uO1xuXHRcdFx0XHQvLyAgICBzdGFydExvY2F0aW9uLmFkZHJlc3MgPSBsZWdzW2ldLnN0YXJ0X2FkZHJlc3M7XG5cdFx0XHRcdC8vICAgIC8vIGNyZWF0ZU1hcmtlcihsZWdzW2ldLnN0YXJ0X2xvY2F0aW9uLCBcInN0YXJ0XCIsIGxlZ3NbaV0uc3RhcnRfYWRkcmVzcywgXCJncmVlblwiKTtcblx0XHRcdFx0Ly8gIH1cblx0XHRcdFx0Ly8gIGlmIChpID09IGxlZ3MubGVuZ3RoIC0gMSkge1xuXHRcdFx0XHQvLyAgICBlbmRMb2NhdGlvbi5sYXRsbmcgPSBsZWdzW2ldLmVuZF9sb2NhdGlvbjtcblx0XHRcdFx0Ly8gICAgZW5kTG9jYXRpb24uYWRkcmVzcyA9IGxlZ3NbaV0uZW5kX2FkZHJlc3M7XG5cdFx0XHRcdC8vICB9XG5cdFx0XHRcdC8vIH1cblxuXG5cdFx0XHRcdC8vICAgIGNyZWF0ZUluZm9XaW5kb3dHb29nbGVDaG9vc2VDb250ZW50KCk7XG5cdFx0XHRcdC8vICAgIHZhciBjb250ZW50UmVzdWx0ID0gJCgnI2dvb2dsZVJpZ2h0Q2xpY2tQb3B1cCcpLmh0bWwoKTtcblxuXG5cdFx0XHRcdC8vICAgIGluZm93aW5kb3dSb3V0ZVJlc3VsdCA9IG5ldyBnb29nbGUubWFwcy5JbmZvV2luZG93KHtcblx0XHRcdFx0Ly8gICBjb250ZW50OiBjb250ZW50UmVzdWx0LFxuXHRcdFx0XHQvLyAgIHBvc2l0aW9uOm1hcmtlclJvdXRlckIuZ2V0UG9zaXRpb24oKVxuXHRcdFx0XHQvLyB9KTtcblxuXHRcdFx0XHQvLyAgICBpbmZvd2luZG93Um91dGVSZXN1bHQub3BlbihtYXBPYmosbWFya2VyUm91dGVyQik7XG5cblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdC8vd2luZG93LmFsZXJ0KCdEaXJlY3Rpb25zIHJlcXVlc3QgZmFpbGVkIGR1ZSB0byAnICsgc3RhdHVzKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdH0pO1xuXHRcdC8vIHNldCBtYXJrZXJzIGN1cnJlbnQgTG9jYXRpb24gYW5kIHN0b3JlIGxvY2F0aW9uIGZvciB0ZXN0IG9ubHkgZW5kXG5cdH1cblxuXHRoYW5kbGVMb2NhdGlvbkVycm9yKGJyb3dzZXJIYXNHZW9sb2NhdGlvbiwgaW5mb1dpbmRvdywgcG9zKSB7XG5cdFx0aW5mb1dpbmRvdy5zZXRQb3NpdGlvbihwb3MpO1xuXHRcdGluZm9XaW5kb3cuc2V0Q29udGVudChicm93c2VySGFzR2VvbG9jYXRpb24gP1xuXHRcdFx0J0Vycm9yOiBUaGUgR2VvbG9jYXRpb24gc2VydmljZSBmYWlsZWQuJyA6XG5cdFx0XHQnRXJyb3I6IFlvdXIgYnJvd3NlciBkb2VzblxcJ3Qgc3VwcG9ydCBnZW9sb2NhdGlvbi4nKTtcblx0fVxuXG5cdGdldEN1cnJlbnRQb3NpdGlvbkJyb3dlcigpIHtcblx0XHR2YXIgaW5mb1dpbmRvdyA9IG5ldyBnb29nbGUubWFwcy5JbmZvV2luZG93KHsgbWFwOiB0aGlzLm1hcE9iaiB9KTtcblxuXHRcdC8vIFRyeSBIVE1MNSBnZW9sb2NhdGlvbi5cblx0XHRpZiAobmF2aWdhdG9yLmdlb2xvY2F0aW9uKSB7XG5cdFx0XHRuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKGZ1bmN0aW9uIChwb3NpdGlvbikge1xuXHRcdFx0XHR2YXIgcG9zID0ge1xuXHRcdFx0XHRcdGxhdDogcG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLFxuXHRcdFx0XHRcdGxuZzogcG9zaXRpb24uY29vcmRzLmxvbmdpdHVkZVxuXHRcdFx0XHR9O1xuXG5cdFx0XHRcdGluZm9XaW5kb3cuc2V0UG9zaXRpb24ocG9zKTtcblx0XHRcdFx0aW5mb1dpbmRvdy5zZXRDb250ZW50KCdMb2NhdGlvbiBmb3VuZC4nKTtcblx0XHRcdFx0dGhpcy5tYXBPYmouc2V0Q2VudGVyKHBvcyk7XG5cdFx0XHR9LCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHRoaXMuaGFuZGxlTG9jYXRpb25FcnJvcih0cnVlLCBpbmZvV2luZG93LCB0aGlzLm1hcE9iai5nZXRDZW50ZXIoKSk7XG5cdFx0XHR9KTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0Ly8gQnJvd3NlciBkb2Vzbid0IHN1cHBvcnQgR2VvbG9jYXRpb25cblx0XHRcdHRoaXMuaGFuZGxlTG9jYXRpb25FcnJvcihmYWxzZSwgaW5mb1dpbmRvdywgdGhpcy5tYXBPYmouZ2V0Q2VudGVyKCkpO1xuXHRcdH1cblx0fVxufVxuIl19
