"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Tour of Heroes';
        //  constructor(private authServ: AuthService) {
        //   this.authServ.handleAuthentication();
        // }
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            //templateUrl: './app/app.html'
            //templateUrl: '/tpl/admin/index'
            templateUrl: utils_1.default.getView('app/app.html')
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsc0NBQXdDO0FBQ3hDLHlDQUFvQztBQVVwQztJQVBBO1FBUUksVUFBSyxHQUFHLGdCQUFnQixDQUFDO1FBRTFCLGdEQUFnRDtRQUNoRCwwQ0FBMEM7UUFDMUMsSUFBSTtJQUNQLENBQUM7SUFOWSxZQUFZO1FBUHhCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUMsUUFBUTtZQUNqQiwrQkFBK0I7WUFDL0IsaUNBQWlDO1lBQ2pDLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQztTQUM3QyxDQUFDO09BRVcsWUFBWSxDQU14QjtJQUFELG1CQUFDO0NBTkQsQUFNQyxJQUFBO0FBTlksb0NBQVkiLCJmaWxlIjoiYXBwL2FwcC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnYXBwL3V0aWxzL3V0aWxzJztcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSBcImFwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2VcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6J215LWFwcCcsXG4gICAgLy90ZW1wbGF0ZVVybDogJy4vYXBwL2FwcC5odG1sJ1xuICAgIC8vdGVtcGxhdGVVcmw6ICcvdHBsL2FkbWluL2luZGV4J1xuICAgIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvYXBwLmh0bWwnKVxufSlcblxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XG4gICAgdGl0bGUgPSAnVG91ciBvZiBIZXJvZXMnO1xuXG4gICAvLyAgY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2UpIHtcblx0ICAvLyAgIHRoaXMuYXV0aFNlcnYuaGFuZGxlQXV0aGVudGljYXRpb24oKTtcblx0ICAvLyB9XG59Il19
