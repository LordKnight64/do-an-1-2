"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var guard_service_1 = require("./services/guard.service");
// Components
var home_component_1 = require("./components/home/home.component");
// import { PageNumComponent } from './components/page-num/page-num.component';
var client_component_1 = require("./components/client/client.component");
var auth_1 = require("./components/layouts/auth/auth");
var login_component_1 = require("./components/login/login.component");
var register_component_1 = require("./components/register/register.component");
var page_not_found_component_1 = require("./components/page-not-found/page-not-found.component");
var system_error_component_1 = require("./components/system-error/system-error.component");
var dashboard_component_1 = require("./components/dashboard/dashboard.component");
var ordering_request_component_1 = require("./components/ordering-request/ordering-request.component");
var profile_component_1 = require("./components/profile/profile.component");
var category_list_component_1 = require("./components/category-list/category-list.component");
var create_category_component_1 = require("./components/create-category/create-category.component");
var option_food_items_list_component_1 = require("./components/option-food-items-list/option-food-items-list.component");
var create_option_food_item_component_1 = require("./components/create-option-food-item/create-option-food-item.component");
var option_value_food_items_component_1 = require("./components/option-value-food-items/option-value-food-items.component");
var create_option_value_food_item_component_1 = require("./components/create-option-value-food-item/create-option-value-food-item.component");
var contents_component_1 = require("./components/contents/contents.component");
var news_list_component_1 = require("./components/news-list/news-list.component");
var news_item_component_1 = require("./components/news-item/news-item.component");
var orders_list_component_1 = require("./components/orders-list/orders-list.component");
var contact_list_component_1 = require("./components/contact-list/contact-list.component");
var contact_item_component_1 = require("./components/contact-item/contact-item.component");
var order_setting_component_1 = require("./components/order-setting/order-setting.component");
var turn_over_component_1 = require("./components/turn-over/turn-over.component");
var food_items_list_component_1 = require("./components/food-items-list/food-items-list.component");
var create_food_item_component_1 = require("./components/create-food-item/create-food-item.component");
var feedback_component_1 = require("./components/feedback/feedback.component");
var table_list_component_1 = require("./components/table-list/table-list.component");
var table_booking_component_1 = require("./components/table-booking/table-booking.component");
var subscribe_list_component_1 = require("./components/subscribe-list/subscribe-list.component");
var send_email_marketing_component_1 = require("./components/send-email-marketing/send-email-marketing.component");
var search_items_component_1 = require("./components/search-items/search-items.component");
var email_marketing_list_component_1 = require("./components/email-marketing-list/email-marketing-list.component");
var review_item_component_1 = require("./components/review-item/review-item.component");
var view_email_marketing_component_1 = require("./components/view-email-marketing/view-email-marketing.component");
var area_list_component_1 = require("./components/area-list/area-list.component");
var create_area_component_1 = require("./components/create-area/create-area.component");
var reserve_table_component_1 = require("./components/reserve-table/reserve-table.component");
var create_table_component_1 = require("./components/create-table/create-table.component");
var table_booking_schedule_component_1 = require("./components/table-booking-schedule/table-booking-schedule.component");
var order_food_component_1 = require("./components/order-food/order-food.component");
var routes = [
    // logged routes
    {
        canActivate: [guard_service_1.CanActivateGuard],
        children: [
            {
                redirectTo: 'dashboard',
                path: '',
                pathMatch: 'full'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: home_component_1.HomeComponent,
                path: 'home'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: dashboard_component_1.DashboardComponent,
                path: 'dashboard'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: client_component_1.ClientComponent,
                path: 'client'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: ordering_request_component_1.OrderingRequestComponent,
                path: 'ordering-detail/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: profile_component_1.ProfileComponent,
                path: 'profile'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: category_list_component_1.CategoryListComponent,
                path: 'category/list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_category_component_1.CreateCategoryComponent,
                path: 'category/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_category_component_1.CreateCategoryComponent,
                path: 'category/create/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: option_food_items_list_component_1.OptionFoodItemsListComponent,
                path: 'option-food-items/list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_option_food_item_component_1.CreateOptionFoodItemComponent,
                path: 'option-food-item/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_option_food_item_component_1.CreateOptionFoodItemComponent,
                path: 'option-food-item/create/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: option_value_food_items_component_1.OptionValueFoodItemsComponent,
                path: 'option-value-food-items/list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_option_value_food_item_component_1.CreateOptionValueFoodItemComponent,
                path: 'option-value-food-item/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_option_value_food_item_component_1.CreateOptionValueFoodItemComponent,
                path: 'option-value-food-item/create/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: contents_component_1.ContentsComponent,
                path: 'contents'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: news_list_component_1.NewsListComponent,
                path: 'news'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: news_item_component_1.NewsItemComponent,
                path: 'news/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: news_item_component_1.NewsItemComponent,
                path: 'news/edit/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: orders_list_component_1.OrdersListComponent,
                path: 'order/list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: orders_list_component_1.OrdersListComponent,
                path: 'my-orders'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: order_food_component_1.OrderFoodComponent,
                path: 'order-food'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: contact_list_component_1.ContactListComponent,
                path: 'contact'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: contact_item_component_1.ContactItemComponent,
                path: 'contact/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: contact_item_component_1.ContactItemComponent,
                path: 'contact/edit/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: order_setting_component_1.OrderSettingComponent,
                path: 'order-setting'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: turn_over_component_1.TurnOverComponent,
                path: 'turn-over'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: food_items_list_component_1.FoodItemsListComponent,
                path: 'food-items/list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_food_item_component_1.CreateFoodItemComponent,
                path: 'food-item/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_food_item_component_1.CreateFoodItemComponent,
                path: 'food-item/edit/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: feedback_component_1.FeedbackComponent,
                path: 'feedback'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: table_list_component_1.TableListComponent,
                path: 'table-list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_table_component_1.CreateTableComponent,
                path: 'table/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_table_component_1.CreateTableComponent,
                path: 'table/create/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: table_booking_schedule_component_1.TableBookingScheduleComponent,
                path: 'table-booking-schedule/view/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: table_booking_component_1.TableBookingComponent,
                path: 'table-booking'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: subscribe_list_component_1.SubscribeListComponent,
                path: 'subscribe-list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: send_email_marketing_component_1.SendEmailMarketingComponent,
                path: 'send-email-marketing'
            }, {
                canActivate: [guard_service_1.CanActivateGuard],
                component: search_items_component_1.SearchItemsComponent,
                path: 'search-items'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: email_marketing_list_component_1.EmailMarketingListComponent,
                path: 'email-marketing-list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: review_item_component_1.ReviewItemComponent,
                path: 'review-item'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: view_email_marketing_component_1.ViewEmailMarketingComponent,
                path: 'view-email-marketing-detail/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: area_list_component_1.AreaListComponent,
                path: 'area-list'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_area_component_1.CreateAreaComponent,
                path: 'area/create'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: create_area_component_1.CreateAreaComponent,
                path: 'area/create/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: reserve_table_component_1.ReserveTableComponent,
                path: 'reserve-table/create/:id'
            },
            {
                canActivate: [guard_service_1.CanActivateGuard],
                component: reserve_table_component_1.ReserveTableComponent,
                path: 'reserve-table/create'
            },
        ],
        component: auth_1.LayoutsAuthComponent,
        path: ''
    },
    // not logged routes
    //
    {
        component: login_component_1.LoginComponent,
        path: 'login'
    },
    {
        component: register_component_1.RegisterComponent,
        path: 'register'
    },
    {
        component: system_error_component_1.SystemErrorComponent,
        path: 'error'
    },
    {
        component: page_not_found_component_1.PageNotFoundComponent,
        path: 'error/404'
    },
    { path: '**', redirectTo: 'error/404' }
];
exports.routing = router_1.RouterModule.forRoot(routes);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAucm91dGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDBDQUF1RDtBQUN2RCwwREFBNEQ7QUFFNUQsYUFBYTtBQUNiLG1FQUFpRTtBQUNqRSwrRUFBK0U7QUFDL0UseUVBQXVFO0FBQ3ZFLHVEQUFzRTtBQUN0RSxzRUFBb0U7QUFDcEUsK0VBQTZFO0FBRTdFLGlHQUE2RjtBQUM3RiwyRkFBd0Y7QUFDeEYsa0ZBQWdGO0FBQ2hGLHVHQUFvRztBQUNwRyw0RUFBMEU7QUFDMUUsOEZBQTJGO0FBQzNGLG9HQUFpRztBQUNqRyx5SEFBb0g7QUFDcEgsNEhBQXVIO0FBQ3ZILDRIQUF1SDtBQUN2SCw4SUFBd0k7QUFDeEksK0VBQTZFO0FBQzdFLGtGQUErRTtBQUMvRSxrRkFBK0U7QUFDL0Usd0ZBQXFGO0FBQ3JGLDJGQUF3RjtBQUN4RiwyRkFBd0Y7QUFDeEYsOEZBQTJGO0FBQzNGLGtGQUErRTtBQUMvRSxvR0FBZ0c7QUFDaEcsdUdBQW1HO0FBQ25HLCtFQUE2RTtBQUM3RSxxRkFBa0Y7QUFDbEYsOEZBQTJGO0FBRTNGLGlHQUE4RjtBQUM5RixtSEFBK0c7QUFDL0csMkZBQXdGO0FBQ3hGLG1IQUErRztBQUMvRyx3RkFBcUY7QUFDckYsbUhBQStHO0FBQy9HLGtGQUErRTtBQUMvRSx3RkFBcUY7QUFDckYsOEZBQTBGO0FBQzFGLDJGQUF3RjtBQUN4Rix5SEFBbUg7QUFDbkgscUZBQWdGO0FBQ2hGLElBQU0sTUFBTSxHQUFXO0lBQ3JCLGdCQUFnQjtJQUNoQjtRQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO1FBQy9CLFFBQVEsRUFBRTtZQUNSO2dCQUNFLFVBQVUsRUFBRSxXQUFXO2dCQUN2QixJQUFJLEVBQUUsRUFBRTtnQkFDUixTQUFTLEVBQUUsTUFBTTthQUNsQjtZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsOEJBQWE7Z0JBQ3hCLElBQUksRUFBRSxNQUFNO2FBQ2I7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLHdDQUFrQjtnQkFDN0IsSUFBSSxFQUFFLFdBQVc7YUFDbEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLGtDQUFlO2dCQUMxQixJQUFJLEVBQUUsUUFBUTthQUNmO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSxxREFBd0I7Z0JBQ25DLElBQUksRUFBRSxxQkFBcUI7YUFDNUI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLG9DQUFnQjtnQkFDM0IsSUFBSSxFQUFFLFNBQVM7YUFDaEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLCtDQUFxQjtnQkFDaEMsSUFBSSxFQUFFLGVBQWU7YUFDdEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLG1EQUF1QjtnQkFDbEMsSUFBSSxFQUFFLGlCQUFpQjthQUN4QjtZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsbURBQXVCO2dCQUNsQyxJQUFJLEVBQUUscUJBQXFCO2FBQzVCO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSwrREFBNEI7Z0JBQ3ZDLElBQUksRUFBRSx3QkFBd0I7YUFDL0I7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLGlFQUE2QjtnQkFDeEMsSUFBSSxFQUFFLHlCQUF5QjthQUNoQztZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsaUVBQTZCO2dCQUN4QyxJQUFJLEVBQUUsNkJBQTZCO2FBQ3BDO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSxpRUFBNkI7Z0JBQ3hDLElBQUksRUFBRSw4QkFBOEI7YUFDckM7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDRFQUFrQztnQkFDN0MsSUFBSSxFQUFFLCtCQUErQjthQUN0QztZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsNEVBQWtDO2dCQUM3QyxJQUFJLEVBQUUsbUNBQW1DO2FBQzFDO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSxzQ0FBaUI7Z0JBQzVCLElBQUksRUFBRSxVQUFVO2FBQ2pCO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSx1Q0FBaUI7Z0JBQzVCLElBQUksRUFBRSxNQUFNO2FBQ2I7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLHVDQUFpQjtnQkFDNUIsSUFBSSxFQUFFLGFBQWE7YUFDcEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLHVDQUFpQjtnQkFDNUIsSUFBSSxFQUFFLGVBQWU7YUFDdEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDJDQUFtQjtnQkFDOUIsSUFBSSxFQUFFLFlBQVk7YUFDbkI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDJDQUFtQjtnQkFDOUIsSUFBSSxFQUFFLFdBQVc7YUFDbEI7WUFFQTtnQkFDQyxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLHlDQUFrQjtnQkFDN0IsSUFBSSxFQUFFLFlBQVk7YUFDbkI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDZDQUFvQjtnQkFDL0IsSUFBSSxFQUFFLFNBQVM7YUFDaEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDZDQUFvQjtnQkFDL0IsSUFBSSxFQUFFLGdCQUFnQjthQUN2QjtZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsNkNBQW9CO2dCQUMvQixJQUFJLEVBQUUsa0JBQWtCO2FBQ3pCO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSwrQ0FBcUI7Z0JBQ2hDLElBQUksRUFBRSxlQUFlO2FBQ3RCO1lBQ0E7Z0JBQ0MsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSx1Q0FBaUI7Z0JBQzVCLElBQUksRUFBRSxXQUFXO2FBQ2xCO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSxrREFBc0I7Z0JBQ2pDLElBQUksRUFBRSxpQkFBaUI7YUFDeEI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLG9EQUF1QjtnQkFDbEMsSUFBSSxFQUFFLGtCQUFrQjthQUN6QjtZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsb0RBQXVCO2dCQUNsQyxJQUFJLEVBQUUsb0JBQW9CO2FBQzNCO1lBQ0E7Z0JBQ0MsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSxzQ0FBaUI7Z0JBQzVCLElBQUksRUFBRSxVQUFVO2FBQ2pCO1lBQ0M7Z0JBQ0EsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSx5Q0FBa0I7Z0JBQzdCLElBQUksRUFBRSxZQUFZO2FBQ25CO1lBQ0M7Z0JBQ0EsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSw2Q0FBb0I7Z0JBQy9CLElBQUksRUFBRSxjQUFjO2FBQ3JCO1lBQ0M7Z0JBQ0EsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSw2Q0FBb0I7Z0JBQy9CLElBQUksRUFBRSxrQkFBa0I7YUFDekI7WUFDRDtnQkFDRSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLGdFQUE2QjtnQkFDeEMsSUFBSSxFQUFFLGlDQUFpQzthQUN4QztZQUVDO2dCQUNBLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsK0NBQXFCO2dCQUNoQyxJQUFJLEVBQUUsZUFBZTthQUN0QjtZQUNDO2dCQUNBLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsaURBQXNCO2dCQUNqQyxJQUFJLEVBQUUsZ0JBQWdCO2FBQ3ZCO1lBQ0M7Z0JBQ0EsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSw0REFBMkI7Z0JBQ3RDLElBQUksRUFBRSxzQkFBc0I7YUFDN0IsRUFBRztnQkFDRixXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDZDQUFvQjtnQkFDL0IsSUFBSSxFQUFFLGNBQWM7YUFDckI7WUFDQztnQkFDQSxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLDREQUEyQjtnQkFDdEMsSUFBSSxFQUFFLHNCQUFzQjthQUM3QjtZQUNBO2dCQUNDLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsMkNBQW1CO2dCQUM5QixJQUFJLEVBQUUsYUFBYTthQUNwQjtZQUNEO2dCQUNFLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsNERBQTJCO2dCQUN0QyxJQUFJLEVBQUUsaUNBQWlDO2FBQ3hDO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSx1Q0FBaUI7Z0JBQzVCLElBQUksRUFBRSxXQUFXO2FBRWxCO1lBRUQ7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSwyQ0FBbUI7Z0JBQzlCLElBQUksRUFBRSxhQUFhO2FBQ3BCO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLENBQUMsZ0NBQWdCLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSwyQ0FBbUI7Z0JBQzlCLElBQUksRUFBRSxpQkFBaUI7YUFDeEI7WUFDQTtnQkFDQyxXQUFXLEVBQUUsQ0FBQyxnQ0FBZ0IsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLCtDQUFxQjtnQkFDaEMsSUFBSSxFQUFFLDBCQUEwQjthQUNqQztZQUNEO2dCQUNDLFdBQVcsRUFBRSxDQUFDLGdDQUFnQixDQUFDO2dCQUMvQixTQUFTLEVBQUUsK0NBQXFCO2dCQUNoQyxJQUFJLEVBQUUsc0JBQXNCO2FBQzdCO1NBR0Q7UUFDRCxTQUFTLEVBQUUsMkJBQW9CO1FBQy9CLElBQUksRUFBRSxFQUFFO0tBQ1Q7SUFDRCxvQkFBb0I7SUFDcEIsRUFBRTtJQUNGO1FBQ0UsU0FBUyxFQUFFLGdDQUFjO1FBQ3pCLElBQUksRUFBRSxPQUFPO0tBQ2Q7SUFDRDtRQUNFLFNBQVMsRUFBRSxzQ0FBaUI7UUFDNUIsSUFBSSxFQUFFLFVBQVU7S0FDakI7SUFDRDtRQUNFLFNBQVMsRUFBRSw2Q0FBb0I7UUFDL0IsSUFBSSxFQUFFLE9BQU87S0FDZDtJQUNEO1FBQ0UsU0FBUyxFQUFFLGdEQUFxQjtRQUNoQyxJQUFJLEVBQUUsV0FBVztLQUNsQjtJQUNELEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFO0NBQ3hDLENBQUM7QUFFVyxRQUFBLE9BQU8sR0FBd0IscUJBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMiLCJmaWxlIjoiYXBwL2FwcC5yb3V0aW5nLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUm91dGVzLCBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQ2FuQWN0aXZhdGVHdWFyZCB9IGZyb20gJy4vc2VydmljZXMvZ3VhcmQuc2VydmljZSc7XG5cbi8vIENvbXBvbmVudHNcbmltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudCc7XG4vLyBpbXBvcnQgeyBQYWdlTnVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3BhZ2UtbnVtL3BhZ2UtbnVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDbGllbnRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2xpZW50L2NsaWVudC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTGF5b3V0c0F1dGhDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbGF5b3V0cy9hdXRoL2F1dGgnO1xuaW1wb3J0IHsgTG9naW5Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50JztcbmltcG9ydCB7IFJlZ2lzdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IFBhZ2VOb3RGb3VuZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wYWdlLW5vdC1mb3VuZC9wYWdlLW5vdC1mb3VuZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3lzdGVtRXJyb3JDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc3lzdGVtLWVycm9yL3N5c3RlbS1lcnJvci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGFzaGJvYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50JztcbmltcG9ydCB7IE9yZGVyaW5nUmVxdWVzdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcmRlcmluZy1yZXF1ZXN0L29yZGVyaW5nLXJlcXVlc3QuY29tcG9uZW50JztcbmltcG9ydCB7IFByb2ZpbGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDYXRlZ29yeUxpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2F0ZWdvcnktbGlzdC9jYXRlZ29yeS1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDcmVhdGVDYXRlZ29yeUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jcmVhdGUtY2F0ZWdvcnkvY3JlYXRlLWNhdGVnb3J5LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25Gb29kSXRlbXNMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL29wdGlvbi1mb29kLWl0ZW1zLWxpc3Qvb3B0aW9uLWZvb2QtaXRlbXMtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3JlYXRlT3B0aW9uRm9vZEl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY3JlYXRlLW9wdGlvbi1mb29kLWl0ZW0vY3JlYXRlLW9wdGlvbi1mb29kLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IE9wdGlvblZhbHVlRm9vZEl0ZW1zQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zL29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDcmVhdGVPcHRpb25WYWx1ZUZvb2RJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NyZWF0ZS1vcHRpb24tdmFsdWUtZm9vZC1pdGVtL2NyZWF0ZS1vcHRpb24tdmFsdWUtZm9vZC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb250ZW50c0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250ZW50cy9jb250ZW50cy5jb21wb25lbnQnO1xuaW1wb3J0IHsgTmV3c0xpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbmV3cy1saXN0L25ld3MtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTmV3c0l0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbmV3cy1pdGVtL25ld3MtaXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3JkZXJzTGlzdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcmRlcnMtbGlzdC9vcmRlcnMtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29udGFjdExpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGFjdC1saXN0L2NvbnRhY3QtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29udGFjdEl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGFjdC1pdGVtL2NvbnRhY3QtaXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3JkZXJTZXR0aW5nQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL29yZGVyLXNldHRpbmcvb3JkZXItc2V0dGluZy5jb21wb25lbnQnO1xuaW1wb3J0IHsgVHVybk92ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdHVybi1vdmVyL3R1cm4tb3Zlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9vZEl0ZW1zTGlzdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb29kLWl0ZW1zLWxpc3QvZm9vZC1pdGVtcy1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDcmVhdGVGb29kSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jcmVhdGUtZm9vZC1pdGVtL2NyZWF0ZS1mb29kLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEZlZWRiYWNrQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZlZWRiYWNrL2ZlZWRiYWNrLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUYWJsZUxpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGFibGUtbGlzdC90YWJsZS1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUYWJsZUJvb2tpbmdDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGFibGUtYm9va2luZy90YWJsZS1ib29raW5nLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IFN1YnNjcmliZUxpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3Qvc3Vic2NyaWJlLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IFNlbmRFbWFpbE1hcmtldGluZ0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zZW5kLWVtYWlsLW1hcmtldGluZy9zZW5kLWVtYWlsLW1hcmtldGluZy5jb21wb25lbnQnO1xuaW1wb3J0IHsgU2VhcmNoSXRlbXNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2VhcmNoLWl0ZW1zL3NlYXJjaC1pdGVtcy5jb21wb25lbnQnO1xuaW1wb3J0IHsgRW1haWxNYXJrZXRpbmdMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2VtYWlsLW1hcmtldGluZy1saXN0L2VtYWlsLW1hcmtldGluZy1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSZXZpZXdJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3Jldmlldy1pdGVtL3Jldmlldy1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBWaWV3RW1haWxNYXJrZXRpbmdDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdmlldy1lbWFpbC1tYXJrZXRpbmcvdmlldy1lbWFpbC1tYXJrZXRpbmcuY29tcG9uZW50JztcbmltcG9ydCB7IEFyZWFMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2FyZWEtbGlzdC9hcmVhLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IENyZWF0ZUFyZWFDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY3JlYXRlLWFyZWEvY3JlYXRlLWFyZWEuY29tcG9uZW50JztcbmltcG9ydCB7IFJlc2VydmVUYWJsZUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL3Jlc2VydmUtdGFibGUvcmVzZXJ2ZS10YWJsZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3JlYXRlVGFibGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY3JlYXRlLXRhYmxlL2NyZWF0ZS10YWJsZS5jb21wb25lbnQnO1xuaW1wb3J0IHtUYWJsZUJvb2tpbmdTY2hlZHVsZUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL3RhYmxlLWJvb2tpbmctc2NoZWR1bGUvdGFibGUtYm9va2luZy1zY2hlZHVsZS5jb21wb25lbnQnO1xuaW1wb3J0IHtPcmRlckZvb2RDb21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9vcmRlci1mb29kL29yZGVyLWZvb2QuY29tcG9uZW50JztcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuICAvLyBsb2dnZWQgcm91dGVzXG4gIHtcbiAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgIGNoaWxkcmVuOiBbXG4gICAgICB7XG4gICAgICAgIHJlZGlyZWN0VG86ICdkYXNoYm9hcmQnLFxuICAgICAgICBwYXRoOiAnJyxcbiAgICAgICAgcGF0aE1hdGNoOiAnZnVsbCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogSG9tZUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2hvbWUnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IERhc2hib2FyZENvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2Rhc2hib2FyZCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogQ2xpZW50Q29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnY2xpZW50J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBPcmRlcmluZ1JlcXVlc3RDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdvcmRlcmluZy1kZXRhaWwvOmlkJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBQcm9maWxlQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAncHJvZmlsZSdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogQ2F0ZWdvcnlMaXN0Q29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnY2F0ZWdvcnkvbGlzdCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogQ3JlYXRlQ2F0ZWdvcnlDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdjYXRlZ29yeS9jcmVhdGUnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZUNhdGVnb3J5Q29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnY2F0ZWdvcnkvY3JlYXRlLzppZCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogT3B0aW9uRm9vZEl0ZW1zTGlzdENvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ29wdGlvbi1mb29kLWl0ZW1zL2xpc3QnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZU9wdGlvbkZvb2RJdGVtQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnb3B0aW9uLWZvb2QtaXRlbS9jcmVhdGUnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZU9wdGlvbkZvb2RJdGVtQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnb3B0aW9uLWZvb2QtaXRlbS9jcmVhdGUvOmlkJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBPcHRpb25WYWx1ZUZvb2RJdGVtc0NvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ29wdGlvbi12YWx1ZS1mb29kLWl0ZW1zL2xpc3QnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZU9wdGlvblZhbHVlRm9vZEl0ZW1Db21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdvcHRpb24tdmFsdWUtZm9vZC1pdGVtL2NyZWF0ZSdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogQ3JlYXRlT3B0aW9uVmFsdWVGb29kSXRlbUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ29wdGlvbi12YWx1ZS1mb29kLWl0ZW0vY3JlYXRlLzppZCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogQ29udGVudHNDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdjb250ZW50cydcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogTmV3c0xpc3RDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICduZXdzJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBOZXdzSXRlbUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ25ld3MvY3JlYXRlJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBOZXdzSXRlbUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ25ld3MvZWRpdC86aWQnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IE9yZGVyc0xpc3RDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdvcmRlci9saXN0J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBPcmRlcnNMaXN0Q29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnbXktb3JkZXJzJ1xuICAgICAgfSxcbiAgICAgIFxuICAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBPcmRlckZvb2RDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdvcmRlci1mb29kJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBDb250YWN0TGlzdENvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2NvbnRhY3QnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENvbnRhY3RJdGVtQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnY29udGFjdC9jcmVhdGUnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENvbnRhY3RJdGVtQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnY29udGFjdC9lZGl0LzppZCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogT3JkZXJTZXR0aW5nQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnb3JkZXItc2V0dGluZydcbiAgICAgIH0sXG4gICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IFR1cm5PdmVyQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAndHVybi1vdmVyJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBGb29kSXRlbXNMaXN0Q29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnZm9vZC1pdGVtcy9saXN0J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBDcmVhdGVGb29kSXRlbUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2Zvb2QtaXRlbS9jcmVhdGUnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZUZvb2RJdGVtQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnZm9vZC1pdGVtL2VkaXQvOmlkJ1xuICAgICAgfSxcbiAgICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogRmVlZGJhY2tDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdmZWVkYmFjaydcbiAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBUYWJsZUxpc3RDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICd0YWJsZS1saXN0J1xuICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZVRhYmxlQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAndGFibGUvY3JlYXRlJ1xuICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZVRhYmxlQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAndGFibGUvY3JlYXRlLzppZCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogVGFibGVCb29raW5nU2NoZWR1bGVDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICd0YWJsZS1ib29raW5nLXNjaGVkdWxlL3ZpZXcvOmlkJ1xuICAgICAgfSxcblxuICAgICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogVGFibGVCb29raW5nQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAndGFibGUtYm9va2luZydcbiAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBTdWJzY3JpYmVMaXN0Q29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnc3Vic2NyaWJlLWxpc3QnXG4gICAgICB9LFxuICAgICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogU2VuZEVtYWlsTWFya2V0aW5nQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnc2VuZC1lbWFpbC1tYXJrZXRpbmcnXG4gICAgICB9LCAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IFNlYXJjaEl0ZW1zQ29tcG9uZW50LFxuICAgICAgICBwYXRoOiAnc2VhcmNoLWl0ZW1zJ1xuICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IEVtYWlsTWFya2V0aW5nTGlzdENvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2VtYWlsLW1hcmtldGluZy1saXN0J1xuICAgICAgfSxcbiAgICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogUmV2aWV3SXRlbUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ3Jldmlldy1pdGVtJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBWaWV3RW1haWxNYXJrZXRpbmdDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICd2aWV3LWVtYWlsLW1hcmtldGluZy1kZXRhaWwvOmlkJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICAgY29tcG9uZW50OiBBcmVhTGlzdENvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2FyZWEtbGlzdCdcblxuICAgICAgfSxcblxuICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IENyZWF0ZUFyZWFDb21wb25lbnQsXG4gICAgICAgIHBhdGg6ICdhcmVhL2NyZWF0ZSdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQ2FuQWN0aXZhdGVHdWFyZF0sXG4gICAgICAgIGNvbXBvbmVudDogQ3JlYXRlQXJlYUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ2FyZWEvY3JlYXRlLzppZCdcbiAgICAgIH0sXG4gICAgICAge1xuICAgICAgICBjYW5BY3RpdmF0ZTogW0NhbkFjdGl2YXRlR3VhcmRdLFxuICAgICAgICBjb21wb25lbnQ6IFJlc2VydmVUYWJsZUNvbXBvbmVudCxcbiAgICAgICAgcGF0aDogJ3Jlc2VydmUtdGFibGUvY3JlYXRlLzppZCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgY2FuQWN0aXZhdGU6IFtDYW5BY3RpdmF0ZUd1YXJkXSxcbiAgICAgICBjb21wb25lbnQ6IFJlc2VydmVUYWJsZUNvbXBvbmVudCxcbiAgICAgICBwYXRoOiAncmVzZXJ2ZS10YWJsZS9jcmVhdGUnXG4gICAgIH0sXG5cblxuICAgIF0sXG4gICAgY29tcG9uZW50OiBMYXlvdXRzQXV0aENvbXBvbmVudCxcbiAgICBwYXRoOiAnJ1xuICB9LFxuICAvLyBub3QgbG9nZ2VkIHJvdXRlc1xuICAvL1xuICB7XG4gICAgY29tcG9uZW50OiBMb2dpbkNvbXBvbmVudCxcbiAgICBwYXRoOiAnbG9naW4nXG4gIH0sXG4gIHtcbiAgICBjb21wb25lbnQ6IFJlZ2lzdGVyQ29tcG9uZW50LFxuICAgIHBhdGg6ICdyZWdpc3RlcidcbiAgfSxcbiAge1xuICAgIGNvbXBvbmVudDogU3lzdGVtRXJyb3JDb21wb25lbnQsXG4gICAgcGF0aDogJ2Vycm9yJ1xuICB9LFxuICB7XG4gICAgY29tcG9uZW50OiBQYWdlTm90Rm91bmRDb21wb25lbnQsXG4gICAgcGF0aDogJ2Vycm9yLzQwNCdcbiAgfSxcbiAgeyBwYXRoOiAnKionLCByZWRpcmVjdFRvOiAnZXJyb3IvNDA0JyB9XG5dO1xuXG5leHBvcnQgY29uc3Qgcm91dGluZzogTW9kdWxlV2l0aFByb3ZpZGVycyA9IFJvdXRlck1vZHVsZS5mb3JSb290KHJvdXRlcyk7XG4iXX0=
