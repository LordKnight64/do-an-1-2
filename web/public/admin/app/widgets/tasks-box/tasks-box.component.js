"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var TasksBoxComponent = (function () {
    function TasksBoxComponent() {
        this.tasksLength = { 0: '9' };
        // TODO 
    }
    TasksBoxComponent.prototype.ngOnInit = function () {
        // TODO
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], TasksBoxComponent.prototype, "user", void 0);
    TasksBoxComponent = __decorate([
        core_1.Component({
            /* tslint:disable */
            selector: '.tasksBox',
            /* tslint:enable */
            styleUrls: [utils_1.default.getView('app/widgets/tasks-box/tasks-box.component.css')],
            templateUrl: utils_1.default.getView('app/widgets/tasks-box/tasks-box.component.html')
        }),
        __metadata("design:paramtypes", [])
    ], TasksBoxComponent);
    return TasksBoxComponent;
}());
exports.TasksBoxComponent = TasksBoxComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL3Rhc2tzLWJveC90YXNrcy1ib3guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQXlEO0FBRXpELHlDQUFvQztBQVNwQztJQU1JO1FBSFEsZ0JBQVcsR0FBTyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUlqQyxRQUFRO0lBQ1osQ0FBQztJQUVNLG9DQUFRLEdBQWY7UUFDSSxPQUFPO0lBQ1gsQ0FBQztJQVJRO1FBQVIsWUFBSyxFQUFFOzttREFBYTtJQUpaLGlCQUFpQjtRQVA3QixnQkFBUyxDQUFFO1lBQ1Isb0JBQW9CO1lBQ3BCLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLG1CQUFtQjtZQUNuQixTQUFTLEVBQUUsQ0FBQyxlQUFLLENBQUMsT0FBTyxDQUFDLCtDQUErQyxDQUFDLENBQUM7WUFDM0UsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsZ0RBQWdELENBQUM7U0FDL0UsQ0FBQzs7T0FDVyxpQkFBaUIsQ0FjN0I7SUFBRCx3QkFBQztDQWRELEFBY0MsSUFBQTtBQWRZLDhDQUFpQiIsImZpbGUiOiJhcHAvd2lkZ2V0cy90YXNrcy1ib3gvdGFza3MtYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNZXNzYWdlIH0gZnJvbSAnYXBwL21vZGVscy9tZXNzYWdlJztcclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KCB7XHJcbiAgICAvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xyXG4gICAgc2VsZWN0b3I6ICcudGFza3NCb3gnLFxyXG4gICAgLyogdHNsaW50OmVuYWJsZSAqL1xyXG4gICAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvdGFza3MtYm94L3Rhc2tzLWJveC5jb21wb25lbnQuY3NzJyldLFxyXG4gICAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC93aWRnZXRzL3Rhc2tzLWJveC90YXNrcy1ib3guY29tcG9uZW50Lmh0bWwnKVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFza3NCb3hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgbWVzc2FnZXM6IE1lc3NhZ2VbXTtcclxuICAgIHByaXZhdGUgdGFza3NMZW5ndGg6IHt9ID0geyAwOiAnOScgfTtcclxuICAgIEBJbnB1dCgpIHB1YmxpYyB1c2VyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIC8vIFRPRE8gXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG5nT25Jbml0KCkge1xyXG4gICAgICAgIC8vIFRPRE9cclxuICAgIH1cclxuXHJcbn1cclxuIl19
