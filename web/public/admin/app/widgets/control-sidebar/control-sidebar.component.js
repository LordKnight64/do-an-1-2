"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var ControlSidebarComponent = (function () {
    function ControlSidebarComponent() {
        // TODO
    }
    ControlSidebarComponent = __decorate([
        core_1.Component({
            selector: 'app-aside',
            styleUrls: [utils_1.default.getView('app/widgets/control-sidebar/control-sidebar.component.css')],
            // templateUrl: './control-sidebar.component.html'
            templateUrl: utils_1.default.getView('app/widgets/control-sidebar/control-sidebar.component.html')
        }),
        __metadata("design:paramtypes", [])
    ], ControlSidebarComponent);
    return ControlSidebarComponent;
}());
exports.ControlSidebarComponent = ControlSidebarComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL2NvbnRyb2wtc2lkZWJhci9jb250cm9sLXNpZGViYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWlEO0FBQ2pELHlDQUFvQztBQVFwQztJQUVFO1FBRUUsT0FBTztJQUNULENBQUM7SUFMVSx1QkFBdUI7UUFObkMsZ0JBQVMsQ0FBRTtZQUNSLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFNBQVMsRUFBRSxDQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsMkRBQTJELENBQUMsQ0FBQztZQUN4RixrREFBa0Q7WUFDbEQsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsNERBQTRELENBQUM7U0FDM0YsQ0FBQzs7T0FDVyx1QkFBdUIsQ0FNbkM7SUFBRCw4QkFBQztDQU5ELEFBTUMsSUFBQTtBQU5ZLDBEQUF1QiIsImZpbGUiOiJhcHAvd2lkZ2V0cy9jb250cm9sLXNpZGViYXIvY29udHJvbC1zaWRlYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KCB7XHJcbiAgICBzZWxlY3RvcjogJ2FwcC1hc2lkZScsXHJcbiAgICBzdHlsZVVybHM6IFsgVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvY29udHJvbC1zaWRlYmFyL2NvbnRyb2wtc2lkZWJhci5jb21wb25lbnQuY3NzJyldLFxyXG4gICAgLy8gdGVtcGxhdGVVcmw6ICcuL2NvbnRyb2wtc2lkZWJhci5jb21wb25lbnQuaHRtbCdcclxuICAgIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvd2lkZ2V0cy9jb250cm9sLXNpZGViYXIvY29udHJvbC1zaWRlYmFyLmNvbXBvbmVudC5odG1sJylcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRyb2xTaWRlYmFyQ29tcG9uZW50IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBcclxuICAgIC8vIFRPRE9cclxuICB9XHJcbn1cclxuIl19
