"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_1 = require("app/models/user");
// import { Router } from '@angular/router';
var user_service_1 = require("app/services/user.service");
var utils_1 = require("app/utils/utils");
var MenuAsideComponent = (function () {
    function MenuAsideComponent(userServ) {
        this.userServ = userServ;
        this.currentUser = new user_1.User();
        this.restaurant = {};
        this.links = [];
        // getting the current url
        // this.router.events.subscribe((evt) => this.currentUrl = evt.url);
        // this.userServ.currentUser.subscribe((user) => this.currentUser = user);
    }
    MenuAsideComponent.prototype.ngOnInit = function () {
        // TODO
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        console.log('userKey.restaurants ', userKey.restaurants[0]);
        this.restaurant = userKey.restaurants[0];
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], MenuAsideComponent.prototype, "links", void 0);
    MenuAsideComponent = __decorate([
        core_1.Component({
            selector: 'app-menu-aside',
            styleUrls: [utils_1.default.getView('app/widgets/menu-aside/menu-aside.component.css')],
            // templateUrl: './menu-aside.component.html'
            templateUrl: utils_1.default.getView('app/widgets/menu-aside/menu-aside.component.html')
        }),
        __metadata("design:paramtypes", [user_service_1.UserService])
    ], MenuAsideComponent);
    return MenuAsideComponent;
}());
exports.MenuAsideComponent = MenuAsideComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL21lbnUtYXNpZGUvbWVudS1hc2lkZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBeUQ7QUFDekQsd0NBQXVDO0FBQ3ZDLDRDQUE0QztBQUM1QywwREFBd0Q7QUFDeEQseUNBQW9DO0FBUXBDO0lBTUUsNEJBQW9CLFFBQXFCO1FBQXJCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFKakMsZ0JBQVcsR0FBUyxJQUFJLFdBQUksRUFBRSxDQUFDO1FBQy9CLGVBQVUsR0FBUSxFQUFFLENBQUM7UUFDWixVQUFLLEdBQWUsRUFBRSxDQUFDO1FBR3RDLDBCQUEwQjtRQUMxQixvRUFBb0U7UUFDcEUsMEVBQTBFO0lBQzVFLENBQUM7SUFFTSxxQ0FBUSxHQUFmO1FBQ0UsT0FBTztRQUNOLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzNELE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEVBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBYlE7UUFBUixZQUFLLEVBQUU7a0NBQWdCLEtBQUs7cURBQVc7SUFKN0Isa0JBQWtCO1FBTjlCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLFNBQVMsRUFBRSxDQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsaURBQWlELENBQUMsQ0FBQztZQUM5RSw2Q0FBNkM7WUFDN0MsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsa0RBQWtELENBQUM7U0FDL0UsQ0FBQzt5Q0FPOEIsMEJBQVc7T0FOOUIsa0JBQWtCLENBbUI5QjtJQUFELHlCQUFDO0NBbkJELEFBbUJDLElBQUE7QUFuQlksZ0RBQWtCIiwiZmlsZSI6ImFwcC93aWRnZXRzL21lbnUtYXNpZGUvbWVudS1hc2lkZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVXNlciB9IGZyb20gJ2FwcC9tb2RlbHMvdXNlcic7XHJcbi8vIGltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZSc7XHJcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbWVudS1hc2lkZScsXHJcbiAgc3R5bGVVcmxzOiBbIFV0aWxzLmdldFZpZXcoJ2FwcC93aWRnZXRzL21lbnUtYXNpZGUvbWVudS1hc2lkZS5jb21wb25lbnQuY3NzJyldLFxyXG4gIC8vIHRlbXBsYXRlVXJsOiAnLi9tZW51LWFzaWRlLmNvbXBvbmVudC5odG1sJ1xyXG4gIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvd2lkZ2V0cy9tZW51LWFzaWRlL21lbnUtYXNpZGUuY29tcG9uZW50Lmh0bWwnKVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWVudUFzaWRlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBwcml2YXRlIGN1cnJlbnRVcmw6IHN0cmluZztcclxuICBwcml2YXRlIGN1cnJlbnRVc2VyOiBVc2VyID0gbmV3IFVzZXIoKTtcclxuICBwcml2YXRlIHJlc3RhdXJhbnQ6IGFueSA9IHt9O1xyXG4gIEBJbnB1dCgpIHByaXZhdGUgbGlua3M6IEFycmF5PGFueT4gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UpIHtcclxuICAgIC8vIGdldHRpbmcgdGhlIGN1cnJlbnQgdXJsXHJcbiAgICAvLyB0aGlzLnJvdXRlci5ldmVudHMuc3Vic2NyaWJlKChldnQpID0+IHRoaXMuY3VycmVudFVybCA9IGV2dC51cmwpO1xyXG4gICAgLy8gdGhpcy51c2VyU2Vydi5jdXJyZW50VXNlci5zdWJzY3JpYmUoKHVzZXIpID0+IHRoaXMuY3VycmVudFVzZXIgPSB1c2VyKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBuZ09uSW5pdCgpIHtcclxuICAgIC8vIFRPRE9cclxuICAgICBsZXQgdXNlcktleSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJfa2V5JykpO1xyXG4gICAgIGNvbnNvbGUubG9nKCd1c2VyS2V5LnJlc3RhdXJhbnRzICcsdXNlcktleS5yZXN0YXVyYW50c1swXSk7XHJcbiAgICAgdGhpcy5yZXN0YXVyYW50ID0gdXNlcktleS5yZXN0YXVyYW50c1swXTtcclxuICB9XHJcblxyXG59XHJcblxyXG4iXX0=
