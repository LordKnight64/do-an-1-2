"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var NotificationBoxComponent = (function () {
    function NotificationBoxComponent() {
        this.notifLength = { 0: '10' };
        // TODO 
    }
    NotificationBoxComponent.prototype.ngOnInit = function () {
        // TODO
    };
    NotificationBoxComponent = __decorate([
        core_1.Component({
            /* tslint:disable */
            selector: '.notificationsBox',
            /* tslint:enable */
            styleUrls: [utils_1.default.getView('app/widgets/notification-box/notification-box.component.css')],
            templateUrl: utils_1.default.getView('app/widgets/notification-box/notification-box.component.html')
        }),
        __metadata("design:paramtypes", [])
    ], NotificationBoxComponent);
    return NotificationBoxComponent;
}());
exports.NotificationBoxComponent = NotificationBoxComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL25vdGlmaWNhdGlvbi1ib3gvbm90aWZpY2F0aW9uLWJveC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFHbEQseUNBQW9DO0FBU3BDO0lBS0k7UUFGUSxnQkFBVyxHQUFPLEVBQUMsQ0FBQyxFQUFFLElBQUksRUFBQyxDQUFDO1FBR2hDLFFBQVE7SUFDWixDQUFDO0lBRU0sMkNBQVEsR0FBZjtRQUNJLE9BQU87SUFDWCxDQUFDO0lBWFEsd0JBQXdCO1FBUHBDLGdCQUFTLENBQUU7WUFDUixvQkFBb0I7WUFDcEIsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixtQkFBbUI7WUFDbkIsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO1lBQ3pGLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLDhEQUE4RCxDQUFDO1NBQzdGLENBQUM7O09BQ1csd0JBQXdCLENBYXBDO0lBQUQsK0JBQUM7Q0FiRCxBQWFDLElBQUE7QUFiWSw0REFBd0IiLCJmaWxlIjoiYXBwL3dpZGdldHMvbm90aWZpY2F0aW9uLWJveC9ub3RpZmljYXRpb24tYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lc3NhZ2UgfSBmcm9tICdhcHAvbW9kZWxzL21lc3NhZ2UnO1xyXG5pbXBvcnQgeyBNZXNzYWdlc1NlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbWVzc2FnZXMuc2VydmljZSc7XHJcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xyXG5cclxuQENvbXBvbmVudCgge1xyXG4gICAgLyogdHNsaW50OmRpc2FibGUgKi9cclxuICAgIHNlbGVjdG9yOiAnLm5vdGlmaWNhdGlvbnNCb3gnLFxyXG4gICAgLyogdHNsaW50OmVuYWJsZSAqL1xyXG4gICAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvbm90aWZpY2F0aW9uLWJveC9ub3RpZmljYXRpb24tYm94LmNvbXBvbmVudC5jc3MnKV0sXHJcbiAgICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvbm90aWZpY2F0aW9uLWJveC9ub3RpZmljYXRpb24tYm94LmNvbXBvbmVudC5odG1sJylcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbkJveENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlczogTWVzc2FnZVtdO1xyXG4gICAgcHJpdmF0ZSBub3RpZkxlbmd0aDoge30gPSB7MDogJzEwJ307XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgLy8gVE9ETyBcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy8gVE9ET1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=
