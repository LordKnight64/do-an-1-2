"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var AppFooterComponent = (function () {
    function AppFooterComponent() {
        // TODO
    }
    AppFooterComponent = __decorate([
        core_1.Component({
            selector: 'app-footer',
            styleUrls: ['admin/app/widgets/app-footer/app-footer.component.css'],
            // templateUrl: './app-footer.component.html'
            templateUrl: utils_1.default.getView('app/widgets/app-footer/app-footer.component.html')
        }),
        __metadata("design:paramtypes", [])
    ], AppFooterComponent);
    return AppFooterComponent;
}());
exports.AppFooterComponent = AppFooterComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL2FwcC1mb290ZXIvYXBwLWZvb3Rlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBaUQ7QUFDakQseUNBQW9DO0FBUXBDO0lBRUk7UUFDRSxPQUFPO0lBQ1QsQ0FBQztJQUpRLGtCQUFrQjtRQU45QixnQkFBUyxDQUFFO1lBQ1IsUUFBUSxFQUFFLFlBQVk7WUFDdEIsU0FBUyxFQUFFLENBQUMsdURBQXVELENBQUM7WUFDcEUsNkNBQTZDO1lBQzdDLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLGtEQUFrRCxDQUFDO1NBQ2pGLENBQUM7O09BQ1csa0JBQWtCLENBSzlCO0lBQUQseUJBQUM7Q0FMRCxBQUtDLElBQUE7QUFMWSxnREFBa0IiLCJmaWxlIjoiYXBwL3dpZGdldHMvYXBwLWZvb3Rlci9hcHAtZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KCB7XHJcbiAgICBzZWxlY3RvcjogJ2FwcC1mb290ZXInLFxyXG4gICAgc3R5bGVVcmxzOiBbJ2FkbWluL2FwcC93aWRnZXRzL2FwcC1mb290ZXIvYXBwLWZvb3Rlci5jb21wb25lbnQuY3NzJ10sXHJcbiAgICAvLyB0ZW1wbGF0ZVVybDogJy4vYXBwLWZvb3Rlci5jb21wb25lbnQuaHRtbCdcclxuICAgIHRlbXBsYXRlVXJsOiBVdGlscy5nZXRWaWV3KCdhcHAvd2lkZ2V0cy9hcHAtZm9vdGVyL2FwcC1mb290ZXIuY29tcG9uZW50Lmh0bWwnKVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwRm9vdGVyQ29tcG9uZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgLy8gVE9ET1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
