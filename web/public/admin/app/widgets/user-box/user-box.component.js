"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_1 = require("app/models/user");
var user_service_1 = require("app/services/user.service");
var router_1 = require("@angular/router");
var utils_1 = require("app/utils/utils");
var UserBoxComponent = (function () {
    function UserBoxComponent(userServ, router) {
        var _this = this;
        this.userServ = userServ;
        this.router = router;
        this.currentUser = new user_1.User();
        this.restaurant = {};
        this.logout = function () {
            _this.userServ.logout();
        };
        // se connecter au modif du user courant
        this.userServ.currentUser.subscribe(function (user) { return _this.currentUser = user; });
    }
    UserBoxComponent.prototype.ngOnInit = function () {
        // TODO
        var userKey = JSON.parse(localStorage.getItem('user_key'));
        console.log('userKey.restaurants ', userKey.restaurants[0]);
        this.restaurant = userKey.restaurants[0];
    };
    UserBoxComponent = __decorate([
        core_1.Component({
            /* tslint:disable */
            selector: '.userBox',
            /* tslint:enable */
            styleUrls: [utils_1.default.getView('app/widgets/user-box/user-box.component.css')],
            templateUrl: utils_1.default.getView('app/widgets/user-box/user-box.component.html')
        }),
        __metadata("design:paramtypes", [user_service_1.UserService, router_1.Router])
    ], UserBoxComponent);
    return UserBoxComponent;
}());
exports.UserBoxComponent = UserBoxComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL3VzZXItYm94L3VzZXItYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx3Q0FBcUM7QUFDckMsMERBQXNEO0FBQ3RELDBDQUF5QztBQUN6Qyx5Q0FBb0M7QUFTcEM7SUFHRSwwQkFBb0IsUUFBcUIsRUFBVSxNQUFjO1FBQWpFLGlCQUdDO1FBSG1CLGFBQVEsR0FBUixRQUFRLENBQWE7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBRnpELGdCQUFXLEdBQVMsSUFBSSxXQUFJLEVBQUUsQ0FBQztRQUMvQixlQUFVLEdBQVEsRUFBRSxDQUFDO1FBY3JCLFdBQU0sR0FBRztZQUNmLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQyxDQUFBO1FBZEMsd0NBQXdDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQVUsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUF2QixDQUF1QixDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVNLG1DQUFRLEdBQWY7UUFDRSxPQUFPO1FBQ04sSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRTVDLENBQUM7SUFkVSxnQkFBZ0I7UUFQNUIsZ0JBQVMsQ0FBQztZQUNULG9CQUFvQjtZQUNwQixRQUFRLEVBQUUsVUFBVTtZQUNwQixtQkFBbUI7WUFDbkIsU0FBUyxFQUFFLENBQUMsZUFBSyxDQUFDLE9BQU8sQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFDO1lBQ3pFLFdBQVcsRUFBRSxlQUFLLENBQUMsT0FBTyxDQUFDLDhDQUE4QyxDQUFDO1NBQzNFLENBQUM7eUNBSThCLDBCQUFXLEVBQWtCLGVBQU07T0FIdEQsZ0JBQWdCLENBbUI1QjtJQUFELHVCQUFDO0NBbkJELEFBbUJDLElBQUE7QUFuQlksNENBQWdCIiwiZmlsZSI6ImFwcC93aWRnZXRzL3VzZXItYm94L3VzZXItYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7VXNlcn0gZnJvbSAnYXBwL21vZGVscy91c2VyJztcclxuaW1wb3J0IHtVc2VyU2VydmljZX0gZnJvbSAnYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCBVdGlscyBmcm9tICdhcHAvdXRpbHMvdXRpbHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgLyogdHNsaW50OmRpc2FibGUgKi9cclxuICBzZWxlY3RvcjogJy51c2VyQm94JyxcclxuICAvKiB0c2xpbnQ6ZW5hYmxlICovXHJcbiAgc3R5bGVVcmxzOiBbVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvdXNlci1ib3gvdXNlci1ib3guY29tcG9uZW50LmNzcycpXSxcclxuICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvdXNlci1ib3gvdXNlci1ib3guY29tcG9uZW50Lmh0bWwnKVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlckJveENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcHJpdmF0ZSBjdXJyZW50VXNlcjogVXNlciA9IG5ldyBVc2VyKCk7XHJcbiAgcHJpdmF0ZSByZXN0YXVyYW50OiBhbnkgPSB7fTtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG4gICAgLy8gc2UgY29ubmVjdGVyIGF1IG1vZGlmIGR1IHVzZXIgY291cmFudFxyXG4gICAgICB0aGlzLnVzZXJTZXJ2LmN1cnJlbnRVc2VyLnN1YnNjcmliZSgodXNlcjogVXNlcikgPT4gdGhpcy5jdXJyZW50VXNlciA9IHVzZXIpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG5nT25Jbml0KCkge1xyXG4gICAgLy8gVE9ET1xyXG4gICAgIGxldCB1c2VyS2V5ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKSk7XHJcbiAgICAgY29uc29sZS5sb2coJ3VzZXJLZXkucmVzdGF1cmFudHMgJyx1c2VyS2V5LnJlc3RhdXJhbnRzWzBdKTtcclxuICAgICB0aGlzLnJlc3RhdXJhbnQgPSB1c2VyS2V5LnJlc3RhdXJhbnRzWzBdO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGxvZ291dCA9ICgpOiB2b2lkID0+IHtcclxuICAgIHRoaXMudXNlclNlcnYubG9nb3V0KCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
