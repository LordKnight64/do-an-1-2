"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var messages_service_1 = require("app/services/messages.service");
var logger_service_1 = require("app/services/logger.service");
var utils_1 = require("app/utils/utils");
var MessagesBoxComponent = (function () {
    function MessagesBoxComponent(msgServ, logger) {
        this.msgServ = msgServ;
        this.logger = logger;
        this.messages = [];
    }
    MessagesBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Every incoming message changes entire local message Array.
        this.msgServ.messages.subscribe(function (msg) {
            _this.logger.log('MsgBox', null, 'RECIEVED.MESSAGE', null);
            _this.messages = msg;
            console.log('---------messages-------', _this.messages);
            _this.msgLength = { 0: _this.messages.length };
        });
    };
    MessagesBoxComponent = __decorate([
        core_1.Component({
            /* tslint:disable */
            selector: '.messagesBox',
            /* tslint:enable */
            styleUrls: [utils_1.default.getView('app/widgets/messages-box/messages-box.component.css')],
            // templateUrl: './messages-box.component.html'
            templateUrl: utils_1.default.getView('app/widgets/messages-box/messages-box.component.html')
        }),
        __metadata("design:paramtypes", [messages_service_1.MessagesService, logger_service_1.LoggerService])
    ], MessagesBoxComponent);
    return MessagesBoxComponent;
}());
exports.MessagesBoxComponent = MessagesBoxComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL21lc3NhZ2VzLWJveC9tZXNzYWdlcy1ib3guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQXlEO0FBR3pELGtFQUFnRTtBQUNoRSw4REFBNEQ7QUFFNUQseUNBQW9DO0FBVXBDO0lBS0ksOEJBQXFCLE9BQXdCLEVBQVUsTUFBcUI7UUFBdkQsWUFBTyxHQUFQLE9BQU8sQ0FBaUI7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3hFLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFTSx1Q0FBUSxHQUFmO1FBQUEsaUJBUUM7UUFQRyw2REFBNkQ7UUFDN0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUUsR0FBYztZQUM1QyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixFQUFFLElBQUksQ0FBRSxDQUFDO1lBQzVELEtBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO1lBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZELEtBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNqRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFqQlEsb0JBQW9CO1FBUmhDLGdCQUFTLENBQUU7WUFDUixvQkFBb0I7WUFDcEIsUUFBUSxFQUFFLGNBQWM7WUFDeEIsbUJBQW1CO1lBQ25CLFNBQVMsRUFBRSxDQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMscURBQXFELENBQUMsQ0FBQztZQUNsRiwrQ0FBK0M7WUFDL0MsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsc0RBQXNELENBQUM7U0FDckYsQ0FBQzt5Q0FNZ0Msa0NBQWUsRUFBa0IsOEJBQWE7T0FMbkUsb0JBQW9CLENBa0JoQztJQUFELDJCQUFDO0NBbEJELEFBa0JDLElBQUE7QUFsQlksb0RBQW9CIiwiZmlsZSI6ImFwcC93aWRnZXRzL21lc3NhZ2VzLWJveC9tZXNzYWdlcy1ib3guY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHAgfSBmcm9tICdAYW5ndWxhci9odHRwJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ25nMi10cmFuc2xhdGUnO1xyXG5pbXBvcnQgeyBNZXNzYWdlc1NlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbWVzc2FnZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ2dlclNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvbG9nZ2VyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNZXNzYWdlIH0gZnJvbSAnYXBwL21vZGVscy9tZXNzYWdlJztcclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KCB7XHJcbiAgICAvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xyXG4gICAgc2VsZWN0b3I6ICcubWVzc2FnZXNCb3gnLFxyXG4gICAgLyogdHNsaW50OmVuYWJsZSAqL1xyXG4gICAgc3R5bGVVcmxzOiBbIFV0aWxzLmdldFZpZXcoJ2FwcC93aWRnZXRzL21lc3NhZ2VzLWJveC9tZXNzYWdlcy1ib3guY29tcG9uZW50LmNzcycpXSxcclxuICAgIC8vIHRlbXBsYXRlVXJsOiAnLi9tZXNzYWdlcy1ib3guY29tcG9uZW50Lmh0bWwnXHJcbiAgICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvbWVzc2FnZXMtYm94L21lc3NhZ2VzLWJveC5jb21wb25lbnQuaHRtbCcpXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNZXNzYWdlc0JveENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICAvLyBEZWNsYXJpbmcgdGhlIHZhcmlhYmxlIGZvciBiaW5kaW5nIHdpdGggaW5pdGlhbCB2YWx1ZVxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlczogTWVzc2FnZVtdO1xyXG4gICAgcHJpdmF0ZSBtc2dMZW5ndGg6IHt9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIG1zZ1NlcnY6IE1lc3NhZ2VzU2VydmljZSwgcHJpdmF0ZSBsb2dnZXI6IExvZ2dlclNlcnZpY2UgKSB7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlcyA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcclxuICAgICAgICAvLyBFdmVyeSBpbmNvbWluZyBtZXNzYWdlIGNoYW5nZXMgZW50aXJlIGxvY2FsIG1lc3NhZ2UgQXJyYXkuXHJcbiAgICAgICAgdGhpcy5tc2dTZXJ2Lm1lc3NhZ2VzLnN1YnNjcmliZSgoIG1zZzogTWVzc2FnZVtdICkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvZ2dlci5sb2coICdNc2dCb3gnLCBudWxsLCAnUkVDSUVWRUQuTUVTU0FHRScsIG51bGwgKTtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IG1zZztcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJy0tLS0tLS0tLW1lc3NhZ2VzLS0tLS0tLScsIHRoaXMubWVzc2FnZXMpO1xyXG4gICAgICAgICAgICB0aGlzLm1zZ0xlbmd0aCA9IHsgMDogdGhpcy5tZXNzYWdlcy5sZW5ndGggfTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
