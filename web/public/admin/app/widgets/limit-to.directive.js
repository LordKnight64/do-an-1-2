"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var LimitToDirective = (function () {
    function LimitToDirective() {
    }
    LimitToDirective.prototype._onKeypress = function (e) {
        var limit = +this.limitTo;
        if (e.target.value.length === limit)
            e.preventDefault();
    };
    __decorate([
        core_1.Input('limit-to'),
        __metadata("design:type", Object)
    ], LimitToDirective.prototype, "limitTo", void 0);
    LimitToDirective = __decorate([
        core_1.Directive({
            selector: '[limit-to]',
            host: {
                '(keypress)': '_onKeypress($event)',
            }
        })
    ], LimitToDirective);
    return LimitToDirective;
}());
exports.LimitToDirective = LimitToDirective;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL2xpbWl0LXRvLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUE4QztBQVM5QztJQUFBO0lBTUEsQ0FBQztJQUpDLHNDQUFXLEdBQVgsVUFBWSxDQUFDO1FBQ1YsSUFBTSxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxLQUFLLENBQUM7WUFBQyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDM0QsQ0FBQztJQUprQjtRQUFsQixZQUFLLENBQUMsVUFBVSxDQUFDOztxREFBUztJQURoQixnQkFBZ0I7UUFONUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxZQUFZO1lBQ3RCLElBQUksRUFBRTtnQkFDSixZQUFZLEVBQUUscUJBQXFCO2FBQ3BDO1NBQ0YsQ0FBQztPQUNXLGdCQUFnQixDQU01QjtJQUFELHVCQUFDO0NBTkQsQUFNQyxJQUFBO0FBTlksNENBQWdCIiwiZmlsZSI6ImFwcC93aWRnZXRzL2xpbWl0LXRvLmRpcmVjdGl2ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RGlyZWN0aXZlLCBJbnB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcblxuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbbGltaXQtdG9dJyxcbiAgaG9zdDoge1xuICAgICcoa2V5cHJlc3MpJzogJ19vbktleXByZXNzKCRldmVudCknLFxuICB9XG59KVxuZXhwb3J0IGNsYXNzIExpbWl0VG9EaXJlY3RpdmUge1xuICBASW5wdXQoJ2xpbWl0LXRvJykgbGltaXRUbzsgXG4gIF9vbktleXByZXNzKGUpIHtcbiAgICAgY29uc3QgbGltaXQgPSArdGhpcy5saW1pdFRvO1xuICAgICBpZiAoZS50YXJnZXQudmFsdWUubGVuZ3RoID09PSBsaW1pdCkgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB9XG59Il19
