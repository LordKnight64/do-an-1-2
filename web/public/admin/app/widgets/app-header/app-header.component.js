"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("app/utils/utils");
var AppHeaderComponent = (function () {
    function AppHeaderComponent() {
        // TODO
    }
    AppHeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            styleUrls: [utils_1.default.getView('app/widgets/app-header/app-header.component.css')],
            // templateUrl: './app-header.component.html'
            templateUrl: utils_1.default.getView('app/widgets/app-header/app-header.component.html')
        }),
        __metadata("design:paramtypes", [])
    ], AppHeaderComponent);
    return AppHeaderComponent;
}());
exports.AppHeaderComponent = AppHeaderComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL2FwcC1oZWFkZXIvYXBwLWhlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBaUQ7QUFFakQseUNBQW9DO0FBUXBDO0lBRUU7UUFFRSxPQUFPO0lBQ1QsQ0FBQztJQUxVLGtCQUFrQjtRQU45QixnQkFBUyxDQUFFO1lBQ1IsUUFBUSxFQUFFLFlBQVk7WUFDdEIsU0FBUyxFQUFFLENBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO1lBQzlFLDZDQUE2QztZQUM3QyxXQUFXLEVBQUUsZUFBSyxDQUFDLE9BQU8sQ0FBQyxrREFBa0QsQ0FBQztTQUNqRixDQUFDOztPQUNXLGtCQUFrQixDQU05QjtJQUFELHlCQUFDO0NBTkQsQUFNQyxJQUFBO0FBTlksZ0RBQWtCIiwiZmlsZSI6ImFwcC93aWRnZXRzL2FwcC1oZWFkZXIvYXBwLWhlYWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICduZzItdHJhbnNsYXRlJztcclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KCB7XHJcbiAgICBzZWxlY3RvcjogJ2FwcC1oZWFkZXInLFxyXG4gICAgc3R5bGVVcmxzOiBbIFV0aWxzLmdldFZpZXcoJ2FwcC93aWRnZXRzL2FwcC1oZWFkZXIvYXBwLWhlYWRlci5jb21wb25lbnQuY3NzJyldLFxyXG4gICAgLy8gdGVtcGxhdGVVcmw6ICcuL2FwcC1oZWFkZXIuY29tcG9uZW50Lmh0bWwnXHJcbiAgICB0ZW1wbGF0ZVVybDogVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvYXBwLWhlYWRlci9hcHAtaGVhZGVyLmNvbXBvbmVudC5odG1sJylcclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcEhlYWRlckNvbXBvbmVudCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICkge1xyXG4gICAgLy8gVE9ET1xyXG4gIH1cclxufVxyXG4iXX0=
