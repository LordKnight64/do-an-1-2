"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// import { Router }             from '@angular/router';
var breadcrumb_service_1 = require("app/services/breadcrumb.service");
var utils_1 = require("app/utils/utils");
var BreadcrumbComponent = (function () {
    function BreadcrumbComponent(breadServ) {
        var _this = this;
        this.breadServ = breadServ;
        this.display = false;
        this.header = '';
        this.description = '';
        this.levels = [];
        // getting the data from the services
        this.breadServ.current.subscribe(function (data) {
            _this.display = data.display;
            _this.header = data.header;
            _this.description = data.description;
            _this.levels = data.levels;
            console.log('breadcrumb', _this);
        });
    }
    BreadcrumbComponent = __decorate([
        core_1.Component({
            selector: 'app-breadcrumb',
            styleUrls: [utils_1.default.getView('app/widgets/breadcrumb/breadcrumb.component.css')],
            templateUrl: utils_1.default.getView('app/widgets/breadcrumb/breadcrumb.component.html')
        }),
        __metadata("design:paramtypes", [breadcrumb_service_1.BreadcrumbService])
    ], BreadcrumbComponent);
    return BreadcrumbComponent;
}());
exports.BreadcrumbComponent = BreadcrumbComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC93aWRnZXRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0Msd0RBQXdEO0FBQ3hELHNFQUEyRTtBQUMzRSx5Q0FBb0M7QUFPcEM7SUFNRSw2QkFBb0IsU0FBNEI7UUFBaEQsaUJBU0M7UUFUbUIsY0FBUyxHQUFULFNBQVMsQ0FBbUI7UUFMeEMsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN6QixXQUFNLEdBQVcsRUFBRSxDQUFDO1FBQ3BCLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBQ3pCLFdBQU0sR0FBZSxFQUFFLENBQUM7UUFHOUIscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDcEMsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQzVCLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUMxQixLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDcEMsS0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFDLEtBQUksQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQWZVLG1CQUFtQjtRQUwvQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixTQUFTLEVBQUUsQ0FBRSxlQUFLLENBQUMsT0FBTyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7WUFDOUUsV0FBVyxFQUFFLGVBQUssQ0FBQyxPQUFPLENBQUMsa0RBQWtELENBQUM7U0FDL0UsQ0FBQzt5Q0FPK0Isc0NBQWlCO09BTnJDLG1CQUFtQixDQWlCL0I7SUFBRCwwQkFBQztDQWpCRCxBQWlCQyxJQUFBO0FBakJZLGtEQUFtQiIsImZpbGUiOiJhcHAvd2lkZ2V0cy9icmVhZGNydW1iL2JyZWFkY3J1bWIuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gIGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG4vLyBpbXBvcnQgeyBSb3V0ZXIgfSAgICAgICAgICAgICBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9ICAgICAgICBmcm9tICdhcHAvc2VydmljZXMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcclxuaW1wb3J0IFV0aWxzIGZyb20gJ2FwcC91dGlscy91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1icmVhZGNydW1iJyxcclxuICBzdHlsZVVybHM6IFsgVXRpbHMuZ2V0VmlldygnYXBwL3dpZGdldHMvYnJlYWRjcnVtYi9icmVhZGNydW1iLmNvbXBvbmVudC5jc3MnKV0sXHJcbiAgdGVtcGxhdGVVcmw6IFV0aWxzLmdldFZpZXcoJ2FwcC93aWRnZXRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQuaHRtbCcpXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iQ29tcG9uZW50IHtcclxuICBwcml2YXRlIGRpc3BsYXk6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIGhlYWRlcjogc3RyaW5nID0gJyc7XHJcbiAgcHJpdmF0ZSBkZXNjcmlwdGlvbjogc3RyaW5nID0gJyc7XHJcbiAgcHJpdmF0ZSBsZXZlbHM6IEFycmF5PGFueT4gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBicmVhZFNlcnY6IEJyZWFkY3J1bWJTZXJ2aWNlKSB7XHJcbiAgICAvLyBnZXR0aW5nIHRoZSBkYXRhIGZyb20gdGhlIHNlcnZpY2VzXHJcbiAgICB0aGlzLmJyZWFkU2Vydi5jdXJyZW50LnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICB0aGlzLmRpc3BsYXkgPSBkYXRhLmRpc3BsYXk7XHJcbiAgICAgIHRoaXMuaGVhZGVyID0gZGF0YS5oZWFkZXI7XHJcbiAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBkYXRhLmRlc2NyaXB0aW9uO1xyXG4gICAgICB0aGlzLmxldmVscyA9IGRhdGEubGV2ZWxzO1xyXG4gICAgICBjb25zb2xlLmxvZygnYnJlYWRjcnVtYicsdGhpcyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==
