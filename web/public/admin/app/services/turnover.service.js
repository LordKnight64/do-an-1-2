"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var TurnOverService = (function () {
    function TurnOverService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    TurnOverService.prototype.getTurnoverPlaning = function (params) {
        var action = 'get_turnover_planing';
        return this.serverServ.doPost(action, params);
    };
    TurnOverService.prototype.getTurnover = function (params) {
        var action = 'get_turnover';
        return this.serverServ.doPost(action, params);
    };
    TurnOverService.prototype.updateTurnoverPlaning = function (params) {
        var action = 'update_turnover_planing';
        return this.serverServ.doPost(action, params);
    };
    TurnOverService.prototype.handleError = function (res) {
        //TODO
    };
    TurnOverService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], TurnOverService);
    return TurnOverService;
}());
exports.TurnOverService = TurnOverService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy90dXJub3Zlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBQzNDLG1EQUFpRDtBQUdqRDtJQUVFLHlCQUNVLFVBQXlCO1FBQXpCLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFakMsT0FBTztJQUNULENBQUM7SUFFTSw0Q0FBa0IsR0FBekIsVUFBMEIsTUFBTTtRQUM5QixJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztRQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSxxQ0FBVyxHQUFsQixVQUFtQixNQUFNO1FBQ3ZCLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQztRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSwrQ0FBcUIsR0FBNUIsVUFBNkIsTUFBTTtRQUNqQyxJQUFJLE1BQU0sR0FBRyx5QkFBeUIsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTyxxQ0FBVyxHQUFuQixVQUFvQixHQUFHO1FBQ3JCLE1BQU07SUFDUixDQUFDO0lBekJVLGVBQWU7UUFEM0IsaUJBQVUsRUFBRTt5Q0FJVyw4QkFBYTtPQUh4QixlQUFlLENBMEIzQjtJQUFELHNCQUFDO0NBMUJELEFBMEJDLElBQUE7QUExQlksMENBQWUiLCJmaWxlIjoiYXBwL3NlcnZpY2VzL3R1cm5vdmVyLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFR1cm5PdmVyU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBzZXJ2ZXJTZXJ2OiBTZXJ2ZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICAvLyBUT0RPXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VHVybm92ZXJQbGFuaW5nKHBhcmFtcykge1xyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfdHVybm92ZXJfcGxhbmluZyc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VHVybm92ZXIocGFyYW1zKSB7XHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF90dXJub3Zlcic7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdXBkYXRlVHVybm92ZXJQbGFuaW5nKHBhcmFtcykge1xyXG4gICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfdHVybm92ZXJfcGxhbmluZyc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUVycm9yKHJlcykge1xyXG4gICAgLy9UT0RPXHJcbiAgfVxyXG59Il19
