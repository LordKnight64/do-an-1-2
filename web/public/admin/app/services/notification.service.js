"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_toaster_1 = require("angular2-toaster/angular2-toaster");
var NotificationService = (function () {
    function NotificationService(toastr) {
        var _this = this;
        this.toastr = toastr;
        this.success = function (body, title) {
            if (title === void 0) { title = 'Operation successful'; }
            _this.toastr.pop({ body: body, title: title, type: 'success' });
        };
        this.error = function (body, title) {
            if (title === void 0) { title = 'An error occured'; }
            _this.toastr.pop({ body: body, title: title, type: 'error' });
        };
        this.warning = function (body, title) {
            if (title === void 0) { title = 'Something went wrong'; }
            _this.toastr.pop({ body: body, title: title, type: 'warning' });
        };
    }
    NotificationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [angular2_toaster_1.ToasterService])
    ], NotificationService);
    return NotificationService;
}());
exports.NotificationService = NotificationService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUEyQztBQUMzQyxzRUFBMEU7QUFHMUU7SUFDRSw2QkFBb0IsTUFBc0I7UUFBMUMsaUJBQStDO1FBQTNCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBRW5DLFlBQU8sR0FBRyxVQUFDLElBQVksRUFBRSxLQUE4QjtZQUE5QixzQkFBQSxFQUFBLDhCQUE4QjtZQUM1RCxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztRQUNqRSxDQUFDLENBQUE7UUFFTSxVQUFLLEdBQUcsVUFBQyxJQUFZLEVBQUUsS0FBMEI7WUFBMUIsc0JBQUEsRUFBQSwwQkFBMEI7WUFDdEQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUFBO1FBRU0sWUFBTyxHQUFHLFVBQUMsSUFBWSxFQUFFLEtBQThCO1lBQTlCLHNCQUFBLEVBQUEsOEJBQThCO1lBQzVELEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ2pFLENBQUMsQ0FBQTtJQVo2QyxDQUFDO0lBRHBDLG1CQUFtQjtRQUQvQixpQkFBVSxFQUFFO3lDQUVpQixpQ0FBYztPQUQvQixtQkFBbUIsQ0FjL0I7SUFBRCwwQkFBQztDQWRELEFBY0MsSUFBQTtBQWRZLGtEQUFtQiIsImZpbGUiOiJhcHAvc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlLCBUb2FzdCB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXIvYW5ndWxhcjItdG9hc3Rlcic7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25TZXJ2aWNlIHtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRvYXN0cjogVG9hc3RlclNlcnZpY2UpIHsgfVxyXG5cclxuICBwdWJsaWMgc3VjY2VzcyA9IChib2R5OiBzdHJpbmcsIHRpdGxlID0gJ09wZXJhdGlvbiBzdWNjZXNzZnVsJyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy50b2FzdHIucG9wKHsgYm9keTogYm9keSwgdGl0bGU6IHRpdGxlLCB0eXBlOiAnc3VjY2VzcycgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZXJyb3IgPSAoYm9keTogc3RyaW5nLCB0aXRsZSA9ICdBbiBlcnJvciBvY2N1cmVkJyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy50b2FzdHIucG9wKHsgYm9keTogYm9keSwgdGl0bGU6IHRpdGxlLCB0eXBlOiAnZXJyb3InIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHdhcm5pbmcgPSAoYm9keTogc3RyaW5nLCB0aXRsZSA9ICdTb21ldGhpbmcgd2VudCB3cm9uZycpOiB2b2lkID0+IHtcclxuICAgIHRoaXMudG9hc3RyLnBvcCh7IGJvZHk6IGJvZHksIHRpdGxlOiB0aXRsZSwgdHlwZTogJ3dhcm5pbmcnIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
