"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var AreaService = (function () {
    function AreaService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    AreaService.prototype.getAreas = function (restId, id) {
        console.log('start get areas service ');
        var action = 'get_area_by_rest_id';
        var params = {
            'rest_id': restId,
            'id': id,
        };
        console.log('get areas service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    AreaService.prototype.createArea = function (areaObject) {
        console.log('start area ');
        var action = 'update_area_by_id';
        var params = {
            'rest_id': areaObject.rest_id,
            'user_id': areaObject.user_id,
            'area': {
                'id': areaObject.area.id,
                'name': areaObject.area.name,
                'thumb': areaObject.area.thumb,
                'is_active': areaObject.area.is_active,
                'is_delete': areaObject.area.is_delete
            },
        };
        console.log('get areas service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    AreaService.prototype.areaUploadFile = function (areaObject) {
        console.log('start createArea upload file');
        var action = 'upload_file';
        var params = {
            'id': areaObject.id,
            'thumb': areaObject.thumb,
            'typeScreen': 'area'
        };
        console.log('get areas service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    AreaService.prototype.deleteFile = function (params) {
        var action = 'delete_file';
        console.log('get delete file service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    AreaService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], AreaService);
    return AreaService;
}());
exports.AreaService = AreaService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9hcmVhLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFFM0MsMENBQXdDO0FBQ3hDLG1EQUFpRDtBQUdqRDtJQUVJO1FBQ0csbUNBQW1DO1FBQzNCLE1BQWMsRUFDZCxVQUF5QjtRQUR6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVsQyxPQUFPO0lBQ1QsQ0FBQztJQUlNLDhCQUFRLEdBQWYsVUFBZ0IsTUFBTSxFQUFDLEVBQUU7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBRXZDLElBQUksTUFBTSxHQUFHLHFCQUFxQixDQUFDO1FBQ2xDLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLE1BQU07WUFDakIsSUFBSSxFQUFFLEVBQUU7U0FDVixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUMsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0sZ0NBQVUsR0FBakIsVUFBa0IsVUFBVTtRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRTFCLElBQUksTUFBTSxHQUFHLG1CQUFtQixDQUFDO1FBQ2hDLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLFVBQVUsQ0FBQyxPQUFPO1lBQzdCLFNBQVMsRUFBQyxVQUFVLENBQUMsT0FBTztZQUM1QixNQUFNLEVBQUM7Z0JBQ0wsSUFBSSxFQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdkIsTUFBTSxFQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSTtnQkFDM0IsT0FBTyxFQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSztnQkFDN0IsV0FBVyxFQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUztnQkFDckMsV0FBVyxFQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUzthQUN0QztTQUVILENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFTSxvQ0FBYyxHQUFyQixVQUFzQixVQUFVO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUUzQyxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUM7UUFDMUIsSUFBSSxNQUFNLEdBQUc7WUFDUixJQUFJLEVBQUMsVUFBVSxDQUFDLEVBQUU7WUFDbEIsT0FBTyxFQUFDLFVBQVUsQ0FBQyxLQUFLO1lBQ3hCLFlBQVksRUFBQyxNQUFNO1NBQ3ZCLENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDSSxnQ0FBVSxHQUFqQixVQUFrQixNQUFNO1FBRXhCLElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN4RCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUE3RFUsV0FBVztRQUR2QixpQkFBVSxFQUFFO3lDQUtVLGVBQU07WUFDRiw4QkFBYTtPQUwzQixXQUFXLENBOER2QjtJQUFELGtCQUFDO0NBOURELEFBOERDLElBQUE7QUE5RFksa0NBQVciLCJmaWxlIjoiYXBwL3NlcnZpY2VzL2FyZWEuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgUmVwbGF5U3ViamVjdCB9IGZyb20gJ3J4anMvUngnO1xyXG5pbXBvcnQgeyBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFyZWFTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgIC8vIHByaXZhdGUgc3RhdGVTZXJ2OiBTdGF0ZVNlcnZpY2UsXHJcbiAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgcHJpdmF0ZSBzZXJ2ZXJTZXJ2OiBTZXJ2ZXJTZXJ2aWNlXHJcbiAgICApIHtcclxuICAgICAgLy8gVE9ET1xyXG4gICAgfVxyXG5cclxuXHJcblxyXG4gICAgcHVibGljIGdldEFyZWFzKHJlc3RJZCxpZCl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgZ2V0IGFyZWFzIHNlcnZpY2UgJyk7XHJcblxyXG4gICAgICAgIGxldCBhY3Rpb24gPSAnZ2V0X2FyZWFfYnlfcmVzdF9pZCc7XHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogcmVzdElkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ2lkJzogaWQsLy9nZXQgZnJvbSBsb2dpblxyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBhcmVhcyBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9XG4gICAgICBcclxuICAgICAgcHVibGljIGNyZWF0ZUFyZWEoYXJlYU9iamVjdCl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgYXJlYSAnKTtcclxuXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfYXJlYV9ieV9pZCc7XHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogYXJlYU9iamVjdC5yZXN0X2lkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ3VzZXJfaWQnOmFyZWFPYmplY3QudXNlcl9pZCxcclxuICAgICAgICAgICAgJ2FyZWEnOntcclxuICAgICAgICAgICAgICAnaWQnOmFyZWFPYmplY3QuYXJlYS5pZCxcclxuICAgICAgICAgICAgICAnbmFtZSc6YXJlYU9iamVjdC5hcmVhLm5hbWUsXHJcbiAgICAgICAgICAgICAgJ3RodW1iJzphcmVhT2JqZWN0LmFyZWEudGh1bWIsXHJcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6YXJlYU9iamVjdC5hcmVhLmlzX2FjdGl2ZSxcclxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzphcmVhT2JqZWN0LmFyZWEuaXNfZGVsZXRlXHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBhcmVhcyBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9XG5cclxuICAgICAgcHVibGljIGFyZWFVcGxvYWRGaWxlKGFyZWFPYmplY3Qpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IGNyZWF0ZUFyZWEgdXBsb2FkIGZpbGUnKTtcclxuXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGxvYWRfZmlsZSc7XHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgJ2lkJzphcmVhT2JqZWN0LmlkLFxyXG4gICAgICAgICAgICAgICd0aHVtYic6YXJlYU9iamVjdC50aHVtYixcclxuICAgICAgICAgICAgICAndHlwZVNjcmVlbic6J2FyZWEnXHJcbiAgICAgICAgIH07XHJcbiAgICAgICBjb25zb2xlLmxvZygnZ2V0IGFyZWFzIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH1cclxuICAgIHB1YmxpYyBkZWxldGVGaWxlKHBhcmFtcykge1xyXG5cclxuICAgIGxldCBhY3Rpb24gPSAnZGVsZXRlX2ZpbGUnO1xyXG4gICAgY29uc29sZS5sb2coJ2dldCBkZWxldGUgZmlsZSBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcbn1cclxuIl19
