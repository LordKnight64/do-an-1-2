"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var StorageService = (function () {
    function StorageService() {
        this.scope = false;
    }
    StorageService.prototype.getScope = function () {
        return this.scope;
    };
    StorageService.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    StorageService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], StorageService);
    return StorageService;
}());
exports.StorageService = StorageService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFHM0M7SUFJSTtRQUZPLFVBQUssR0FBa0IsS0FBSyxDQUFDO0lBR3BDLENBQUM7SUFFTSxpQ0FBUSxHQUFmO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVNLGlDQUFRLEdBQWYsVUFBZ0IsS0FBVTtRQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBYlEsY0FBYztRQUQxQixpQkFBVSxFQUFFOztPQUNBLGNBQWMsQ0FjMUI7SUFBRCxxQkFBQztDQWRELEFBY0MsSUFBQTtBQWRZLHdDQUFjIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTdG9yYWdlU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHNjb3BlOiBhbnkgfCBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFNjb3BlKCk6IGFueSB8IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNjb3BlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRTY29wZShzY29wZTogYW55KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zY29wZSA9IHNjb3BlO1xyXG4gICAgfVxyXG59Il19
