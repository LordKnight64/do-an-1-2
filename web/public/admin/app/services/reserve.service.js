"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var ReserveService = (function () {
    function ReserveService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    ReserveService.prototype.createReserve = function (params) {
        console.log('start area ');
        console.log(params);
        var action = 'update_reversation_by_id';
        console.log('get areas service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    ReserveService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], ReserveService);
    return ReserveService;
}());
exports.ReserveService = ReserveService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9yZXNlcnZlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFFM0MsMENBQXdDO0FBQ3hDLG1EQUFpRDtBQUdqRDtJQUVJO1FBQ0csbUNBQW1DO1FBQzNCLE1BQWMsRUFDZCxVQUF5QjtRQUR6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVsQyxPQUFPO0lBQ1QsQ0FBQztJQUVRLHNDQUFhLEdBQXBCLFVBQXFCLE1BQU07UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25CLElBQUksTUFBTSxHQUFHLDBCQUEwQixDQUFDO1FBRXpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLE1BQU0sQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQWpCTSxjQUFjO1FBRDFCLGlCQUFVLEVBQUU7eUNBS1UsZUFBTTtZQUNGLDhCQUFhO09BTDNCLGNBQWMsQ0FrQjFCO0lBQUQscUJBQUM7Q0FsQkQsQUFrQkMsSUFBQTtBQWxCWSx3Q0FBYyIsImZpbGUiOiJhcHAvc2VydmljZXMvcmVzZXJ2ZS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBSZXBsYXlTdWJqZWN0IH0gZnJvbSAncnhqcy9SeCc7XHJcbmltcG9ydCB7IFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJy4vc2VydmVyLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgUmVzZXJ2ZVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgLy8gcHJpdmF0ZSBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSxcclxuICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAvLyBUT0RPXHJcbiAgICB9XHJcblxyXG4gICAgICBwdWJsaWMgY3JlYXRlUmVzZXJ2ZShwYXJhbXMpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IGFyZWEgJyk7XHJcbiAgICAgICBjb25zb2xlLmxvZyhwYXJhbXMpO1xyXG4gICAgICAgIGxldCBhY3Rpb24gPSAndXBkYXRlX3JldmVyc2F0aW9uX2J5X2lkJztcclxuXHJcbiAgICAgICBjb25zb2xlLmxvZygnZ2V0IGFyZWFzIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH1cbn1cclxuIl19
