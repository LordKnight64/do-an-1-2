"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = require("app/models/user");
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
// import { StateService } from "ui-router-ng2";
var router_1 = require("@angular/router");
var auth_service_1 = require("./auth.service");
var server_service_1 = require("./server.service");
var UserService = (function () {
    function UserService(
        // private stateServ: StateService,
        router, authServ, serverServ) {
        this.router = router;
        this.authServ = authServ;
        this.serverServ = serverServ;
        this.currentUser = new Rx_1.ReplaySubject(1);
        // TODO
    }
    UserService.prototype.setCurrentUser = function (user) {
        this.currentUser.next(user);
    };
    UserService.prototype.logout = function () {
        //remove token
        this.authServ.setToken('');
        //remove user
        this.removeCurrentUser();
        this.router.navigate(['login']);
        // this.stateServ.go('login');
    };
    UserService.prototype.removeCurrentUser = function () {
        var user = new user_1.User();
        // user.connected = false;
        user.isInit = false;
        this.setCurrentUser(user);
    };
    UserService.prototype.getAuthenticatedUser = function () {
        var _this = this;
        var action = 'authenticate/user';
        this.serverServ.doPost(action, {}).then(function (response) { return _this.processResult(response); }, function (error) { return _this.handleError.bind(error); });
    };
    UserService.prototype.processResult = function (response) {
        var user = response.user;
        var token = response.token;
        var user1 = new user_1.User({
            // avatarUrl: 'public/assets/img/user2-160x160.jpg',
            email: user.email,
            firstname: '',
            lastname: user.name
        });
        // set token
        this.authServ.setToken(token);
        this.setCurrentUser(user1);
    };
    UserService.prototype.handleError = function (res) {
        //TODO
    };
    UserService.prototype.setUserInfo = function (userInfo) {
        if (userInfo != null) {
            var user = userInfo;
            user.token = "";
            localStorage.setItem('user_key', JSON.stringify(user));
        }
    };
    UserService.prototype.getUserInfo = function () {
        var userInfo = localStorage.getItem('user_key');
        if (userInfo != "" && userInfo != null && userInfo != undefined)
            return JSON.parse(userInfo);
        return null;
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            auth_service_1.AuthService,
            server_service_1.ServerService])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy91c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSx3Q0FBdUM7QUFDdkMsc0NBQTJDO0FBQzNDLDhCQUFvRDtBQUNwRCxnREFBZ0Q7QUFDaEQsMENBQXdDO0FBQ3hDLCtDQUE2QztBQUM3QyxtREFBaUQ7QUFHakQ7SUFHSTtRQUNHLG1DQUFtQztRQUMzQixNQUFjLEVBQ2QsUUFBcUIsRUFDckIsVUFBeUI7UUFGekIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQU43QixnQkFBVyxHQUF3QixJQUFJLGtCQUFhLENBQVEsQ0FBQyxDQUFFLENBQUM7UUFRckUsT0FBTztJQUNULENBQUM7SUFFTSxvQ0FBYyxHQUFyQixVQUF1QixJQUFVO1FBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBRSxDQUFDO0lBQ2hDLENBQUM7SUFFTSw0QkFBTSxHQUFiO1FBRUUsY0FBYztRQUNkLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzNCLGFBQWE7UUFDYixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV6QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBRSxDQUFDLE9BQU8sQ0FBQyxDQUFFLENBQUM7UUFFbEMsOEJBQThCO0lBQ2hDLENBQUM7SUFFTSx1Q0FBaUIsR0FBeEI7UUFDRSxJQUFJLElBQUksR0FBRyxJQUFJLFdBQUksRUFBRSxDQUFDO1FBQ3RCLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFFLElBQUksQ0FBRSxDQUFDO0lBQzlCLENBQUM7SUFFTSwwQ0FBb0IsR0FBM0I7UUFBQSxpQkFNQztRQUxDLElBQUksTUFBTSxHQUFHLG1CQUFtQixDQUFDO1FBRWpDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQ3hCLFVBQUEsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBNUIsQ0FBNEIsRUFDekMsVUFBQSxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFTyxtQ0FBYSxHQUFyQixVQUFzQixRQUFRO1FBRTVCLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDekIsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLEtBQUssR0FBRyxJQUFJLFdBQUksQ0FBRTtZQUNoQixvREFBb0Q7WUFDcEQsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLFNBQVMsRUFBRSxFQUFFO1lBQ2IsUUFBUSxFQUFFLElBQUksQ0FBQyxJQUFJO1NBQ3RCLENBQUUsQ0FBQztRQUNOLFlBQVk7UUFDWixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU5QixJQUFJLENBQUMsY0FBYyxDQUFFLEtBQUssQ0FBRSxDQUFDO0lBQy9CLENBQUM7SUFFTyxpQ0FBVyxHQUFuQixVQUFxQixHQUFHO1FBQ3RCLE1BQU07SUFDUixDQUFDO0lBRU0saUNBQVcsR0FBbEIsVUFBbUIsUUFBUTtRQUN6QixFQUFFLENBQUEsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNsQixJQUFJLElBQUksR0FBRyxRQUFRLENBQUM7WUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQzFELENBQUM7SUFDSCxDQUFDO0lBRU0saUNBQVcsR0FBbEI7UUFDRSxJQUFJLFFBQVEsR0FBSSxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pELEVBQUUsQ0FBQSxDQUFDLFFBQVEsSUFBSSxFQUFFLElBQUksUUFBUSxJQUFJLElBQUksSUFBSSxRQUFRLElBQUksU0FBUyxDQUFDO1lBQzdELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBNUVRLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTt5Q0FNVSxlQUFNO1lBQ0osMEJBQVc7WUFDVCw4QkFBYTtPQVAzQixXQUFXLENBNkV2QjtJQUFELGtCQUFDO0NBN0VELEFBNkVDLElBQUE7QUE3RVksa0NBQVciLCJmaWxlIjoiYXBwL3NlcnZpY2VzL3VzZXIuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFVzZXIgfSBmcm9tICdhcHAvbW9kZWxzL3VzZXInO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFJlcGxheVN1YmplY3QgfSBmcm9tICdyeGpzL1J4JztcclxuLy8gaW1wb3J0IHsgU3RhdGVTZXJ2aWNlIH0gZnJvbSBcInVpLXJvdXRlci1uZzJcIjtcclxuaW1wb3J0IHsgUm91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gXCIuL2F1dGguc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2ZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBVc2VyU2VydmljZSB7XHJcbiAgICBwdWJsaWMgY3VycmVudFVzZXI6IFJlcGxheVN1YmplY3Q8VXNlcj4gPSBuZXcgUmVwbGF5U3ViamVjdDxVc2VyPiggMSApO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgLy8gcHJpdmF0ZSBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSxcclxuICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICBwcml2YXRlIGF1dGhTZXJ2OiBBdXRoU2VydmljZSxcclxuICAgICAgIHByaXZhdGUgc2VydmVyU2VydjogU2VydmVyU2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgIC8vIFRPRE9cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0Q3VycmVudFVzZXIoIHVzZXI6IFVzZXIgKSB7XHJcbiAgICAgIHRoaXMuY3VycmVudFVzZXIubmV4dCggdXNlciApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBsb2dvdXQoKSB7XHJcbiAgICAgIFxyXG4gICAgICAvL3JlbW92ZSB0b2tlblxyXG4gICAgICB0aGlzLmF1dGhTZXJ2LnNldFRva2VuKCcnKTtcclxuICAgICAgLy9yZW1vdmUgdXNlclxyXG4gICAgICB0aGlzLnJlbW92ZUN1cnJlbnRVc2VyKCk7XHJcblxyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZSggWydsb2dpbiddICk7XHJcblxyXG4gICAgICAvLyB0aGlzLnN0YXRlU2Vydi5nbygnbG9naW4nKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVtb3ZlQ3VycmVudFVzZXIoKXtcclxuICAgICAgbGV0IHVzZXIgPSBuZXcgVXNlcigpO1xyXG4gICAgICAvLyB1c2VyLmNvbm5lY3RlZCA9IGZhbHNlO1xyXG4gICAgICB1c2VyLmlzSW5pdCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLnNldEN1cnJlbnRVc2VyKCB1c2VyICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEF1dGhlbnRpY2F0ZWRVc2VyKCl7XHJcbiAgICAgIHZhciBhY3Rpb24gPSAnYXV0aGVudGljYXRlL3VzZXInO1xyXG5cclxuICAgICAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHt9KS50aGVuKFxyXG4gICAgICAgICAgICAgICAgICAgICByZXNwb25zZSAgPT4gdGhpcy5wcm9jZXNzUmVzdWx0KHJlc3BvbnNlKSxcclxuICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4gIHRoaXMuaGFuZGxlRXJyb3IuYmluZChlcnJvcikpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXNwb25zZSkge1xyXG5cclxuICAgICAgdmFyIHVzZXIgPSByZXNwb25zZS51c2VyO1xyXG4gICAgICB2YXIgdG9rZW4gPSByZXNwb25zZS50b2tlbjtcclxuICAgICAgbGV0IHVzZXIxID0gbmV3IFVzZXIoIHtcclxuICAgICAgICAgICAgLy8gYXZhdGFyVXJsOiAncHVibGljL2Fzc2V0cy9pbWcvdXNlcjItMTYweDE2MC5qcGcnLFxyXG4gICAgICAgICAgICBlbWFpbDogdXNlci5lbWFpbCxcclxuICAgICAgICAgICAgZmlyc3RuYW1lOiAnJyxcclxuICAgICAgICAgICAgbGFzdG5hbWU6IHVzZXIubmFtZVxyXG4gICAgICAgIH0gKTtcclxuICAgICAgLy8gc2V0IHRva2VuXHJcbiAgICAgIHRoaXMuYXV0aFNlcnYuc2V0VG9rZW4odG9rZW4pO1xyXG5cclxuICAgICAgdGhpcy5zZXRDdXJyZW50VXNlciggdXNlcjEgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhbmRsZUVycm9yIChyZXMpIHtcclxuICAgICAgLy9UT0RPXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldFVzZXJJbmZvKHVzZXJJbmZvKSB7XHJcbiAgICAgIGlmKHVzZXJJbmZvICE9IG51bGwpe1xyXG4gICAgICAgICB2YXIgdXNlciA9IHVzZXJJbmZvO1xyXG4gICAgICAgICB1c2VyLnRva2VuID0gXCJcIjtcclxuICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3VzZXJfa2V5JywgSlNPTi5zdHJpbmdpZnkodXNlcikpO1xyXG4gICAgICB9ICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VXNlckluZm8oKSB7XHJcbiAgICAgIHZhciB1c2VySW5mbyA9ICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9rZXknKTtcclxuICAgICAgaWYodXNlckluZm8gIT0gXCJcIiAmJiB1c2VySW5mbyAhPSBudWxsICYmIHVzZXJJbmZvICE9IHVuZGVmaW5lZClcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZSh1c2VySW5mbyk7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
