"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var GeoLocationService = (function () {
    function GeoLocationService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    GeoLocationService.prototype.getDistance = function (params) {
        var action = 'get_distance';
        return this.serverServ.doPost(action, params);
    };
    GeoLocationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], GeoLocationService);
    return GeoLocationService;
}());
exports.GeoLocationService = GeoLocationService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9nZW9Mb2NhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBQzNDLG1EQUFpRDtBQUdqRDtJQUVFLDRCQUNVLFVBQXlCO1FBQXpCLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFakMsT0FBTztJQUNULENBQUM7SUFFTSx3Q0FBVyxHQUFsQixVQUFtQixNQUFNO1FBRXZCLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQztRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFaVSxrQkFBa0I7UUFEOUIsaUJBQVUsRUFBRTt5Q0FJVyw4QkFBYTtPQUh4QixrQkFBa0IsQ0FhOUI7SUFBRCx5QkFBQztDQWJELEFBYUMsSUFBQTtBQWJZLGdEQUFrQiIsImZpbGUiOiJhcHAvc2VydmljZXMvZ2VvTG9jYXRpb24uc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJy4vc2VydmVyLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgR2VvTG9jYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIFRPRE9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXREaXN0YW5jZShwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9kaXN0YW5jZSc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG59Il19
