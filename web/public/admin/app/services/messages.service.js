"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var initialMessages = [];
var MessagesService = (function () {
    // public markThreadAsRead: Subject<any> = new Subject<any>();
    function MessagesService() {
        var _this = this;
        this.messagesList = [];
        // a stream that publishes new messages only once
        this.newMessages = new Rx_1.Subject();
        // `messages` is a stream that emits an array of the most up to date messages
        this.messages = new Rx_1.ReplaySubject(1);
        // `updates` receives _operations_ to be applied to our `messages`
        // it's a way we can perform changes on *all* messages (that are currently
        // stored in `messages`)
        this.updates = new Rx_1.Subject();
        // action streams
        this.create = new Rx_1.Subject();
        // recois des operation, et les fais sur la liste interne, puis diffuse le resultat sur messages
        this.updates.subscribe(function (ope) {
            _this.messagesList = ope(_this.messagesList);
            console.log(_this.messagesList);
            _this.messages.next(_this.messagesList);
        });
        this.newMessages
            .map(function (message) {
            return function (messages) {
                return messages.concat(message);
            };
        })
            .subscribe(this.updates);
    }
    // an imperative function call to this action stream
    MessagesService.prototype.addMessage = function (message) {
        this.newMessages.next(message);
    };
    MessagesService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], MessagesService);
    return MessagesService;
}());
exports.MessagesService = MessagesService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9tZXNzYWdlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBR0Esc0NBQTJDO0FBQzNDLDhCQUE2RDtBQUU3RCxJQUFJLGVBQWUsR0FBYyxFQUFFLENBQUM7QUFPcEM7SUFlRSw4REFBOEQ7SUFFOUQ7UUFBQSxpQkFnQkM7UUFoQ08saUJBQVksR0FBYyxFQUFFLENBQUM7UUFDckMsaURBQWlEO1FBQzFDLGdCQUFXLEdBQXFCLElBQUksWUFBTyxFQUFXLENBQUM7UUFFOUQsNkVBQTZFO1FBQ3RFLGFBQVEsR0FBNkIsSUFBSSxrQkFBYSxDQUFZLENBQUMsQ0FBQyxDQUFDO1FBRTVFLGtFQUFrRTtRQUNsRSwwRUFBMEU7UUFDMUUsd0JBQXdCO1FBQ2pCLFlBQU8sR0FBaUIsSUFBSSxZQUFPLEVBQU8sQ0FBQztRQUVsRCxpQkFBaUI7UUFDVixXQUFNLEdBQXFCLElBQUksWUFBTyxFQUFXLENBQUM7UUFJdkQsZ0dBQWdHO1FBQ2hHLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFVBQUMsR0FBRztZQUN6QixLQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDL0IsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFdBQVc7YUFDYixHQUFHLENBQUMsVUFBUyxPQUFnQjtZQUM1QixNQUFNLENBQUMsVUFBQyxRQUFtQjtnQkFDekIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbEMsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDO2FBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUU3QixDQUFDO0lBRUQsb0RBQW9EO0lBQzdDLG9DQUFVLEdBQWpCLFVBQWtCLE9BQWdCO1FBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUF0Q1UsZUFBZTtRQUQzQixpQkFBVSxFQUFFOztPQUNBLGVBQWUsQ0F3QzNCO0lBQUQsc0JBQUM7Q0F4Q0QsQUF3Q0MsSUFBQTtBQXhDWSwwQ0FBZSIsImZpbGUiOiJhcHAvc2VydmljZXMvbWVzc2FnZXMuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGJhc2VkIG9uIGh0dHBzOi8vZ2l0aHViLmNvbS9uZy1ib29rL2FuZ3VsYXIyLXJ4anMtY2hhdC9ibG9iL21hc3Rlci9hcHAvdHMvc2VydmljZXMvTWVzc2FnZXNTZXJ2aWNlLnRzXHJcbmltcG9ydCB7IFVzZXIgfSBmcm9tICdhcHAvbW9kZWxzL3VzZXInO1xyXG5pbXBvcnQgeyBNZXNzYWdlIH0gZnJvbSAnLi4vbW9kZWxzL21lc3NhZ2UnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QsIFJlcGxheVN1YmplY3QgfSBmcm9tICdyeGpzL1J4JztcclxuXHJcbmxldCBpbml0aWFsTWVzc2FnZXM6IE1lc3NhZ2VbXSA9IFtdO1xyXG5cclxuaW50ZXJmYWNlIElNZXNzYWdlc09wZXJhdGlvbiBleHRlbmRzIEZ1bmN0aW9uIHtcclxuICAobWVzc2FnZXM6IE1lc3NhZ2VbXSk6IE1lc3NhZ2VbXTtcclxufVxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTWVzc2FnZXNTZXJ2aWNlIHtcclxuICBwcml2YXRlIG1lc3NhZ2VzTGlzdDogTWVzc2FnZVtdID0gW107XHJcbiAgLy8gYSBzdHJlYW0gdGhhdCBwdWJsaXNoZXMgbmV3IG1lc3NhZ2VzIG9ubHkgb25jZVxyXG4gIHB1YmxpYyBuZXdNZXNzYWdlczogU3ViamVjdDxNZXNzYWdlPiA9IG5ldyBTdWJqZWN0PE1lc3NhZ2U+KCk7XHJcblxyXG4gIC8vIGBtZXNzYWdlc2AgaXMgYSBzdHJlYW0gdGhhdCBlbWl0cyBhbiBhcnJheSBvZiB0aGUgbW9zdCB1cCB0byBkYXRlIG1lc3NhZ2VzXHJcbiAgcHVibGljIG1lc3NhZ2VzOiBSZXBsYXlTdWJqZWN0PE1lc3NhZ2VbXT4gPSBuZXcgUmVwbGF5U3ViamVjdDxNZXNzYWdlW10+KDEpO1xyXG5cclxuICAvLyBgdXBkYXRlc2AgcmVjZWl2ZXMgX29wZXJhdGlvbnNfIHRvIGJlIGFwcGxpZWQgdG8gb3VyIGBtZXNzYWdlc2BcclxuICAvLyBpdCdzIGEgd2F5IHdlIGNhbiBwZXJmb3JtIGNoYW5nZXMgb24gKmFsbCogbWVzc2FnZXMgKHRoYXQgYXJlIGN1cnJlbnRseVxyXG4gIC8vIHN0b3JlZCBpbiBgbWVzc2FnZXNgKVxyXG4gIHB1YmxpYyB1cGRhdGVzOiBTdWJqZWN0PGFueT4gPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcblxyXG4gIC8vIGFjdGlvbiBzdHJlYW1zXHJcbiAgcHVibGljIGNyZWF0ZTogU3ViamVjdDxNZXNzYWdlPiA9IG5ldyBTdWJqZWN0PE1lc3NhZ2U+KCk7XHJcbiAgLy8gcHVibGljIG1hcmtUaHJlYWRBc1JlYWQ6IFN1YmplY3Q8YW55PiA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAvLyByZWNvaXMgZGVzIG9wZXJhdGlvbiwgZXQgbGVzIGZhaXMgc3VyIGxhIGxpc3RlIGludGVybmUsIHB1aXMgZGlmZnVzZSBsZSByZXN1bHRhdCBzdXIgbWVzc2FnZXNcclxuICAgIHRoaXMudXBkYXRlcy5zdWJzY3JpYmUoKG9wZSkgPT4ge1xyXG4gICAgICB0aGlzLm1lc3NhZ2VzTGlzdCA9IG9wZSh0aGlzLm1lc3NhZ2VzTGlzdCk7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMubWVzc2FnZXNMaXN0KTtcclxuICAgICAgdGhpcy5tZXNzYWdlcy5uZXh0KHRoaXMubWVzc2FnZXNMaXN0KTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMubmV3TWVzc2FnZXNcclxuICAgICAgLm1hcChmdW5jdGlvbihtZXNzYWdlOiBNZXNzYWdlKTogSU1lc3NhZ2VzT3BlcmF0aW9uIHtcclxuICAgICAgICByZXR1cm4gKG1lc3NhZ2VzOiBNZXNzYWdlW10pID0+IHtcclxuICAgICAgICAgIHJldHVybiBtZXNzYWdlcy5jb25jYXQobWVzc2FnZSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfSlcclxuICAgICAgLnN1YnNjcmliZSh0aGlzLnVwZGF0ZXMpO1xyXG5cclxuICB9XHJcblxyXG4gIC8vIGFuIGltcGVyYXRpdmUgZnVuY3Rpb24gY2FsbCB0byB0aGlzIGFjdGlvbiBzdHJlYW1cclxuICBwdWJsaWMgYWRkTWVzc2FnZShtZXNzYWdlOiBNZXNzYWdlKTogdm9pZCB7XHJcbiAgICB0aGlzLm5ld01lc3NhZ2VzLm5leHQobWVzc2FnZSk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=
