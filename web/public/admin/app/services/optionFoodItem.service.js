"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var OptionFoodItemService = (function () {
    function OptionFoodItemService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    OptionFoodItemService.prototype.getOptionFoodItems = function (restId, id) {
        console.log('start OptionFoodItems service ');
        var action = 'get_options_by_rest_id';
        var params = {
            'rest_id': restId,
            'id': id,
        };
        console.log('get categories service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    OptionFoodItemService.prototype.createOptionFoodItems = function (optionItemObject) {
        console.log('start createOptionFoodItems ');
        var action = 'update_option_by_id';
        var params = {
            'rest_id': optionItemObject.rest_id,
            'user_id': optionItemObject.user_id,
            'option': {
                'id': optionItemObject.option.id,
                'name': optionItemObject.option.name,
                'is_delete': optionItemObject.option.is_delete
            },
        };
        console.log('get createOptionFoodItems service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    OptionFoodItemService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], OptionFoodItemService);
    return OptionFoodItemService;
}());
exports.OptionFoodItemService = OptionFoodItemService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9vcHRpb25Gb29kSXRlbS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBRTNDLDBDQUF3QztBQUN4QyxtREFBaUQ7QUFHakQ7SUFFSTtRQUNHLG1DQUFtQztRQUMzQixNQUFjLEVBQ2QsVUFBeUI7UUFEekIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFbEMsT0FBTztJQUNULENBQUM7SUFDTSxrREFBa0IsR0FBekIsVUFBMEIsTUFBTSxFQUFDLEVBQUU7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBRTdDLElBQUksTUFBTSxHQUFHLHdCQUF3QixDQUFDO1FBQ3JDLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLE1BQU07WUFDakIsSUFBSSxFQUFFLEVBQUU7U0FDVixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbkQsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ00scURBQXFCLEdBQTVCLFVBQTZCLGdCQUFnQjtRQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFFM0MsSUFBSSxNQUFNLEdBQUcscUJBQXFCLENBQUM7UUFDbEMsSUFBSSxNQUFNLEdBQUc7WUFDVixTQUFTLEVBQUUsZ0JBQWdCLENBQUMsT0FBTztZQUNuQyxTQUFTLEVBQUMsZ0JBQWdCLENBQUMsT0FBTztZQUNsQyxRQUFRLEVBQUM7Z0JBQ1AsSUFBSSxFQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUMvQixNQUFNLEVBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLElBQUk7Z0JBQ25DLFdBQVcsRUFBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsU0FBUzthQUM5QztTQUVILENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5RCxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFwQ00scUJBQXFCO1FBRGpDLGlCQUFVLEVBQUU7eUNBS1UsZUFBTTtZQUNGLDhCQUFhO09BTDNCLHFCQUFxQixDQXNDakM7SUFBRCw0QkFBQztDQXRDRCxBQXNDQyxJQUFBO0FBdENZLHNEQUFxQiIsImZpbGUiOiJhcHAvc2VydmljZXMvb3B0aW9uRm9vZEl0ZW0uc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgUmVwbGF5U3ViamVjdCB9IGZyb20gJ3J4anMvUngnO1xyXG5pbXBvcnQgeyBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE9wdGlvbkZvb2RJdGVtU2VydmljZSB7ICAgIFxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgLy8gcHJpdmF0ZSBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSxcclxuICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAvLyBUT0RPXHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0T3B0aW9uRm9vZEl0ZW1zKHJlc3RJZCxpZCl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgT3B0aW9uRm9vZEl0ZW1zIHNlcnZpY2UgJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICdnZXRfb3B0aW9uc19ieV9yZXN0X2lkJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogcmVzdElkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ2lkJzogaWQsLy9nZXQgZnJvbSBsb2dpblxyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBjYXRlZ29yaWVzIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH0gICAgICAgICBcclxuICAgICAgcHVibGljIGNyZWF0ZU9wdGlvbkZvb2RJdGVtcyhvcHRpb25JdGVtT2JqZWN0KXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBjcmVhdGVPcHRpb25Gb29kSXRlbXMgJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfb3B0aW9uX2J5X2lkJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogb3B0aW9uSXRlbU9iamVjdC5yZXN0X2lkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ3VzZXJfaWQnOm9wdGlvbkl0ZW1PYmplY3QudXNlcl9pZCxcclxuICAgICAgICAgICAgJ29wdGlvbic6e1xyXG4gICAgICAgICAgICAgICdpZCc6b3B0aW9uSXRlbU9iamVjdC5vcHRpb24uaWQsXHJcbiAgICAgICAgICAgICAgJ25hbWUnOm9wdGlvbkl0ZW1PYmplY3Qub3B0aW9uLm5hbWUsXHJcbiAgICAgICAgICAgICAgJ2lzX2RlbGV0ZSc6b3B0aW9uSXRlbU9iamVjdC5vcHRpb24uaXNfZGVsZXRlXHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBjcmVhdGVPcHRpb25Gb29kSXRlbXMgc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgICAgICAgcmV0dXJuICB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgfSAgICAgIFxyXG4gICAgICAgXHJcbn1cclxuIl19
