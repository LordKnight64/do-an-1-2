"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_translate_1 = require("ng2-translate");
var user_service_1 = require("./user.service");
var environment_1 = require("environments/environment");
var langs = ['en', 'fr', 'ru', 'he', 'zh', 'ja'];
var langmatch = /en|fr|ru|he|zh|ja/;
var AdminLTETranslateService = (function () {
    function AdminLTETranslateService(userServ, translate) {
        var _this = this;
        this.userServ = userServ;
        this.translate = translate;
        this.lang = 'us';
        translate.addLangs(langs);
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');
        // lang mode
        // 1: get language from browser
        // 2: get language from user setting
        if (environment_1.ENVIRONMENT.LANG_MODE === 1) {
            var browserLang = this.translate.getBrowserLang();
            var browserCultureLang = this.translate.getBrowserCultureLang();
            console.log('Detected browser language: "' + browserCultureLang + '"');
            var useLang = 'en';
            useLang = browserLang.match(langmatch) ? browserLang : 'en';
            this.translate.use(useLang);
            console.log('Translation language has been set to: "' + useLang + '"');
        }
        else if (environment_1.ENVIRONMENT.LANG_MODE === 2) {
            this.userServ.currentUser.subscribe(function (user) {
                _this.currentUser = user;
                // the lang to use, if the lang isn't available, it will use the current loader to get them
                var browserLang = _this.translate.getBrowserLang();
                var browserCultureLang = _this.translate.getBrowserCultureLang();
                console.log('Detected browser language: "' + browserCultureLang + '"');
                // check if current User has a Preferred Language set, and it differs from his browser lang
                var useLang = 'en';
                var prefLang = (_this.currentUser) ? _this.currentUser.preferredLang : null;
                if (!prefLang) {
                    useLang = browserLang.match(langmatch) ? browserLang : 'en';
                }
                else {
                    console.log('Detected User preferred language: "' + prefLang + '"');
                    useLang = prefLang.match(langmatch) ? prefLang : 'en';
                }
                _this.translate.use(useLang);
                console.log('Translation language has been set to: "' + useLang + '"');
                // translate.use( 'ru' );
            });
        }
    }
    AdminLTETranslateService.prototype.ngOnInit = function () {
        // TODO
    };
    AdminLTETranslateService.prototype.getTranslate = function () {
        return this.translate;
    };
    AdminLTETranslateService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [user_service_1.UserService, ng2_translate_1.TranslateService])
    ], AdminLTETranslateService);
    return AdminLTETranslateService;
}());
exports.AdminLTETranslateService = AdminLTETranslateService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy90cmFuc2xhdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFtRDtBQUNuRCwrQ0FBaUQ7QUFDakQsK0NBQTZDO0FBRTdDLHdEQUF1RDtBQUV2RCxJQUFNLEtBQUssR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFDbkQsSUFBTSxTQUFTLEdBQUcsbUJBQW1CLENBQUM7QUFHdEM7SUFJSSxrQ0FBcUIsUUFBcUIsRUFBVSxTQUEyQjtRQUEvRSxpQkEwQ0M7UUExQ29CLGFBQVEsR0FBUixRQUFRLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUh2RSxTQUFJLEdBQVcsSUFBSSxDQUFDO1FBSXhCLFNBQVMsQ0FBQyxRQUFRLENBQUUsS0FBSyxDQUFFLENBQUM7UUFDNUIsa0dBQWtHO1FBQ2xHLFNBQVMsQ0FBQyxjQUFjLENBQUUsSUFBSSxDQUFFLENBQUM7UUFFakMsWUFBWTtRQUNaLCtCQUErQjtRQUMvQixvQ0FBb0M7UUFDcEMsRUFBRSxDQUFBLENBQUMseUJBQVcsQ0FBQyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUM1QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ2xELElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUUsOEJBQThCLEdBQUcsa0JBQWtCLEdBQUcsR0FBRyxDQUFFLENBQUM7WUFFekUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ25CLE9BQU8sR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFFLFNBQVMsQ0FBRSxHQUFHLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDOUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUUsT0FBTyxDQUFFLENBQUM7WUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBRSx5Q0FBeUMsR0FBRyxPQUFPLEdBQUcsR0FBRyxDQUFFLENBQUM7UUFFN0UsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUEsQ0FBQyx5QkFBVyxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFFLElBQVU7Z0JBQzVDLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUV4QiwyRkFBMkY7Z0JBQzNGLElBQUksV0FBVyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ2xELElBQUksa0JBQWtCLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2dCQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFFLDhCQUE4QixHQUFHLGtCQUFrQixHQUFHLEdBQUcsQ0FBRSxDQUFDO2dCQUV6RSwyRkFBMkY7Z0JBQzNGLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDbkIsSUFBSSxRQUFRLEdBQUcsQ0FBRSxLQUFJLENBQUMsV0FBVyxDQUFFLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2dCQUM1RSxFQUFFLENBQUMsQ0FBRSxDQUFDLFFBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ2QsT0FBTyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUUsU0FBUyxDQUFFLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDbEUsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFFLHFDQUFxQyxHQUFHLFFBQVEsR0FBRyxHQUFHLENBQUUsQ0FBQztvQkFDdEUsT0FBTyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUUsU0FBUyxDQUFFLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDNUQsQ0FBQztnQkFDRCxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBRSxPQUFPLENBQUUsQ0FBQztnQkFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBRSx5Q0FBeUMsR0FBRyxPQUFPLEdBQUcsR0FBRyxDQUFFLENBQUM7Z0JBQ3pFLHlCQUF5QjtZQUM3QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7SUFFTCxDQUFDO0lBRU0sMkNBQVEsR0FBZjtRQUNJLE9BQU87SUFDWCxDQUFDO0lBRU0sK0NBQVksR0FBbkI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBdERRLHdCQUF3QjtRQURwQyxpQkFBVSxFQUFFO3lDQUtzQiwwQkFBVyxFQUFxQixnQ0FBZ0I7T0FKdEUsd0JBQXdCLENBd0RwQztJQUFELCtCQUFDO0NBeERELEFBd0RDLElBQUE7QUF4RFksNERBQXdCIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy90cmFuc2xhdGUuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnbmcyLXRyYW5zbGF0ZSc7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi91c2VyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi4vbW9kZWxzL3VzZXInO1xyXG5pbXBvcnQgeyBFTlZJUk9OTUVOVCB9IGZyb20gJ2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XHJcblxyXG5jb25zdCBsYW5ncyA9IFsnZW4nLCAnZnInLCAncnUnLCAnaGUnLCAnemgnLCAnamEnXTtcclxuY29uc3QgbGFuZ21hdGNoID0gL2VufGZyfHJ1fGhlfHpofGphLztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFkbWluTFRFVHJhbnNsYXRlU2VydmljZSBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBwcml2YXRlIGxhbmc6IHN0cmluZyA9ICd1cyc7XHJcbiAgICBwcml2YXRlIGN1cnJlbnRVc2VyOiBVc2VyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSwgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UgKSB7XHJcbiAgICAgICAgdHJhbnNsYXRlLmFkZExhbmdzKCBsYW5ncyApO1xyXG4gICAgICAgIC8vIHRoaXMgbGFuZ3VhZ2Ugd2lsbCBiZSB1c2VkIGFzIGEgZmFsbGJhY2sgd2hlbiBhIHRyYW5zbGF0aW9uIGlzbid0IGZvdW5kIGluIHRoZSBjdXJyZW50IGxhbmd1YWdlXHJcbiAgICAgICAgdHJhbnNsYXRlLnNldERlZmF1bHRMYW5nKCAnZW4nICk7XHJcblxyXG4gICAgICAgIC8vIGxhbmcgbW9kZVxyXG4gICAgICAgIC8vIDE6IGdldCBsYW5ndWFnZSBmcm9tIGJyb3dzZXJcclxuICAgICAgICAvLyAyOiBnZXQgbGFuZ3VhZ2UgZnJvbSB1c2VyIHNldHRpbmdcclxuICAgICAgICBpZihFTlZJUk9OTUVOVC5MQU5HX01PREUgPT09IDEpe1xyXG4gICAgICAgICAgICBsZXQgYnJvd3NlckxhbmcgPSB0aGlzLnRyYW5zbGF0ZS5nZXRCcm93c2VyTGFuZygpO1xyXG4gICAgICAgICAgICBsZXQgYnJvd3NlckN1bHR1cmVMYW5nID0gdGhpcy50cmFuc2xhdGUuZ2V0QnJvd3NlckN1bHR1cmVMYW5nKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCAnRGV0ZWN0ZWQgYnJvd3NlciBsYW5ndWFnZTogXCInICsgYnJvd3NlckN1bHR1cmVMYW5nICsgJ1wiJyApO1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZUxhbmcgPSAnZW4nO1xyXG4gICAgICAgICAgICB1c2VMYW5nID0gYnJvd3NlckxhbmcubWF0Y2goIGxhbmdtYXRjaCApID8gYnJvd3NlckxhbmcgOiAnZW4nO1xyXG4gICAgICAgICAgICB0aGlzLnRyYW5zbGF0ZS51c2UoIHVzZUxhbmcgKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coICdUcmFuc2xhdGlvbiBsYW5ndWFnZSBoYXMgYmVlbiBzZXQgdG86IFwiJyArIHVzZUxhbmcgKyAnXCInICk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH0gZWxzZSBpZihFTlZJUk9OTUVOVC5MQU5HX01PREUgPT09IDIpe1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJTZXJ2LmN1cnJlbnRVc2VyLnN1YnNjcmliZSgoIHVzZXI6IFVzZXIgKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gdXNlcjtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyB0aGUgbGFuZyB0byB1c2UsIGlmIHRoZSBsYW5nIGlzbid0IGF2YWlsYWJsZSwgaXQgd2lsbCB1c2UgdGhlIGN1cnJlbnQgbG9hZGVyIHRvIGdldCB0aGVtXHJcbiAgICAgICAgICAgICAgICBsZXQgYnJvd3NlckxhbmcgPSB0aGlzLnRyYW5zbGF0ZS5nZXRCcm93c2VyTGFuZygpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGJyb3dzZXJDdWx0dXJlTGFuZyA9IHRoaXMudHJhbnNsYXRlLmdldEJyb3dzZXJDdWx0dXJlTGFuZygpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coICdEZXRlY3RlZCBicm93c2VyIGxhbmd1YWdlOiBcIicgKyBicm93c2VyQ3VsdHVyZUxhbmcgKyAnXCInICk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gY2hlY2sgaWYgY3VycmVudCBVc2VyIGhhcyBhIFByZWZlcnJlZCBMYW5ndWFnZSBzZXQsIGFuZCBpdCBkaWZmZXJzIGZyb20gaGlzIGJyb3dzZXIgbGFuZ1xyXG4gICAgICAgICAgICAgICAgbGV0IHVzZUxhbmcgPSAnZW4nO1xyXG4gICAgICAgICAgICAgICAgbGV0IHByZWZMYW5nID0gKCB0aGlzLmN1cnJlbnRVc2VyICkgPyB0aGlzLmN1cnJlbnRVc2VyLnByZWZlcnJlZExhbmcgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgaWYgKCAhcHJlZkxhbmcgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlTGFuZyA9IGJyb3dzZXJMYW5nLm1hdGNoKCBsYW5nbWF0Y2ggKSA/IGJyb3dzZXJMYW5nIDogJ2VuJztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coICdEZXRlY3RlZCBVc2VyIHByZWZlcnJlZCBsYW5ndWFnZTogXCInICsgcHJlZkxhbmcgKyAnXCInICk7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlTGFuZyA9IHByZWZMYW5nLm1hdGNoKCBsYW5nbWF0Y2ggKSA/IHByZWZMYW5nIDogJ2VuJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMudHJhbnNsYXRlLnVzZSggdXNlTGFuZyApO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coICdUcmFuc2xhdGlvbiBsYW5ndWFnZSBoYXMgYmVlbiBzZXQgdG86IFwiJyArIHVzZUxhbmcgKyAnXCInICk7XHJcbiAgICAgICAgICAgICAgICAvLyB0cmFuc2xhdGUudXNlKCAncnUnICk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy8gVE9ET1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUcmFuc2xhdGUoKTogVHJhbnNsYXRlU2VydmljZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRlO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=
