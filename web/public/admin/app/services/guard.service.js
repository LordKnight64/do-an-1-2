"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//import { StateService } from "ui-router-ng2";
var user_service_1 = require("./user.service");
//import { ServerService } from './server.service';
var auth_service_1 = require("./auth.service");
var router_1 = require("@angular/router");
// import { AuthHttp } from 'angular2-jwt';
var CanActivateGuard = (function () {
    // private error: any;
    function CanActivateGuard(router, userServ, 
        //private stateServ: StateService,
        //private serverServ: ServerService,
        authServ
        // private authHttp: AuthHttp
    ) {
        this.router = router;
        this.userServ = userServ;
        this.authServ = authServ;
        // private connected: boolean = false;
        this.isGetUser = false;
        // this.userServ.currentUser.subscribe((user) => {
        //   if(!user.email){
        //     console.log('user', user);
        //     this.isGetUser = true;
        //   }
        // });
    }
    // public isLoggedIn() {
    //   // // var token = JSON.parse(window.sessionStorage.getItem('token'));
    //   // var token = window.localStorage.getItem('token');
    //   // var tokenState = this.authServ.checkToken(token);
    //   // //is token && is expires
    //   // if(tokenState === 1){
    //   //   //set expires
    //   //   // this.authServ.setToken(token.key);
    //   //   this.authServ.setToken(token);
    //   //   // is refesh page
    //   //   // if(!this.connected){
    //   //     //get authenticated user
    //   //     this.userServ.getAuthenticatedUser();
    //   //   // }
    //   //   return true;
    //   // } else if(tokenState === 2){ // is token && not expires 
    //   //     //remove token
    //   //    this.authServ.setToken('');
    //   //    //remove user
    //   //    this.userServ.removeCurrentUser();
    //   //    return false;
    //   // }
    //   // return false;
    //   var isAuthenticated = this.authServ.authenticated();
    //   if(isAuthenticated){
    //     //get authenticated user
    //     if(!this.isInit){
    //       this.userServ.getAuthenticatedUser();
    //     } else {
    //       this.isInit = false;
    //     }
    //   }
    //   return isAuthenticated;
    // }
    CanActivateGuard.prototype.canActivate = function () {
        console.log('--------------canActivate--------------------');
        // test here if you user is logged
        var isAuthenticated = this.authServ.authenticated();
        if (!isAuthenticated) {
            this.authServ.setToken('');
            this.userServ.removeCurrentUser();
            this.router.navigate(['login']);
            // if(!this.isInit){
            //   this.userServ.getAuthenticatedUser();
            // } else {
            //   this.isInit = false;
            // }
        }
        // else {
        //   // this.authHttp.get
        //   // console.log(this.authHttp);
        //   this.userServ.currentUser.subscribe((user) => {
        //     if(!user.email){
        //       console.log('user', user);
        //        this.userServ.getAuthenticatedUser();
        //     }
        //   },
        //   error => console.log(error));
        //   console.log( this.userServ.currentUser.subscribe());
        // }
        // console.log('------canActivate------', this.authServ.authenticated());
        return isAuthenticated;
    };
    CanActivateGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            user_service_1.UserService,
            auth_service_1.AuthService
            // private authHttp: AuthHttp
        ])
    ], CanActivateGuard);
    return CanActivateGuard;
}());
exports.CanActivateGuard = CanActivateGuard;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQW1EO0FBQ25ELCtDQUErQztBQUMvQywrQ0FBNkM7QUFDN0MsbURBQW1EO0FBQ25ELCtDQUE2QztBQUU3QywwQ0FBd0M7QUFDeEMsMkNBQTJDO0FBRzNDO0lBR0Usc0JBQXNCO0lBQ3RCLDBCQUNVLE1BQWMsRUFDZCxRQUFxQjtRQUM3QixrQ0FBa0M7UUFDbEMsb0NBQW9DO1FBQzVCLFFBQXFCO1FBQzdCLDZCQUE2Qjs7UUFMckIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQWE7UUFHckIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQVIvQixzQ0FBc0M7UUFDOUIsY0FBUyxHQUFZLEtBQUssQ0FBQztRQVVqQyxrREFBa0Q7UUFDbEQscUJBQXFCO1FBQ3JCLGlDQUFpQztRQUNqQyw2QkFBNkI7UUFDN0IsTUFBTTtRQUNOLE1BQU07SUFDUixDQUFDO0lBRUQsd0JBQXdCO0lBRXhCLDBFQUEwRTtJQUMxRSx5REFBeUQ7SUFDekQseURBQXlEO0lBR3pELGdDQUFnQztJQUNoQyw2QkFBNkI7SUFDN0IsdUJBQXVCO0lBQ3ZCLCtDQUErQztJQUMvQyx3Q0FBd0M7SUFFeEMsMkJBQTJCO0lBQzNCLGlDQUFpQztJQUNqQyxvQ0FBb0M7SUFDcEMsaURBQWlEO0lBQ2pELGNBQWM7SUFFZCxzQkFBc0I7SUFDdEIsZ0VBQWdFO0lBQ2hFLDBCQUEwQjtJQUMxQixzQ0FBc0M7SUFDdEMsd0JBQXdCO0lBQ3hCLDZDQUE2QztJQUU3Qyx3QkFBd0I7SUFDeEIsU0FBUztJQUVULHFCQUFxQjtJQUNyQix5REFBeUQ7SUFDekQseUJBQXlCO0lBQ3pCLCtCQUErQjtJQUMvQix3QkFBd0I7SUFDeEIsOENBQThDO0lBQzlDLGVBQWU7SUFDZiw2QkFBNkI7SUFDN0IsUUFBUTtJQUVSLE1BQU07SUFDTiw0QkFBNEI7SUFDNUIsSUFBSTtJQUVHLHNDQUFXLEdBQWxCO1FBRUUsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1FBQzdELGtDQUFrQztRQUNsQyxJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3BELEVBQUUsQ0FBQyxDQUFFLENBQUMsZUFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBRWxDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFFLENBQUUsT0FBTyxDQUFFLENBQUUsQ0FBQztZQUVwQyxvQkFBb0I7WUFDcEIsMENBQTBDO1lBQzFDLFdBQVc7WUFDWCx5QkFBeUI7WUFDekIsSUFBSTtRQUNOLENBQUM7UUFDRCxTQUFTO1FBQ1QseUJBQXlCO1FBQ3pCLG1DQUFtQztRQUNuQyxvREFBb0Q7UUFDcEQsdUJBQXVCO1FBQ3ZCLG1DQUFtQztRQUNuQywrQ0FBK0M7UUFDL0MsUUFBUTtRQUNSLE9BQU87UUFDUCxrQ0FBa0M7UUFDbEMseURBQXlEO1FBQ3pELElBQUk7UUFDSix5RUFBeUU7UUFDekUsTUFBTSxDQUFDLGVBQWUsQ0FBQztJQUN6QixDQUFDO0lBOUZVLGdCQUFnQjtRQUQ1QixpQkFBVSxFQUFFO3lDQU1PLGVBQU07WUFDSiwwQkFBVztZQUdYLDBCQUFXO1lBQzdCLDZCQUE2Qjs7T0FWcEIsZ0JBQWdCLENBK0Y1QjtJQUFELHVCQUFDO0NBL0ZELEFBK0ZDLElBQUE7QUEvRlksNENBQWdCIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbi8vaW1wb3J0IHsgU3RhdGVTZXJ2aWNlIH0gZnJvbSBcInVpLXJvdXRlci1uZzJcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuL3VzZXIuc2VydmljZSc7XHJcbi8vaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJy4vc2VydmVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlciB9IGZyb20gJ2FwcC9tb2RlbHMvdXNlcic7XHJcbmltcG9ydCB7IFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuLy8gaW1wb3J0IHsgQXV0aEh0dHAgfSBmcm9tICdhbmd1bGFyMi1qd3QnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQ2FuQWN0aXZhdGVHdWFyZCB7XHJcbiAgLy8gcHJpdmF0ZSBjb25uZWN0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIGlzR2V0VXNlcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIC8vIHByaXZhdGUgZXJyb3I6IGFueTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2OiBVc2VyU2VydmljZSxcclxuICAgIC8vcHJpdmF0ZSBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSxcclxuICAgIC8vcHJpdmF0ZSBzZXJ2ZXJTZXJ2OiBTZXJ2ZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhdXRoU2VydjogQXV0aFNlcnZpY2VcclxuICAgIC8vIHByaXZhdGUgYXV0aEh0dHA6IEF1dGhIdHRwXHJcbiAgKSB7XHJcbiAgICAvLyB0aGlzLnVzZXJTZXJ2LmN1cnJlbnRVc2VyLnN1YnNjcmliZSgodXNlcikgPT4ge1xyXG4gICAgLy8gICBpZighdXNlci5lbWFpbCl7XHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coJ3VzZXInLCB1c2VyKTtcclxuICAgIC8vICAgICB0aGlzLmlzR2V0VXNlciA9IHRydWU7XHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gcHVibGljIGlzTG9nZ2VkSW4oKSB7XHJcblxyXG4gIC8vICAgLy8gLy8gdmFyIHRva2VuID0gSlNPTi5wYXJzZSh3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgndG9rZW4nKSk7XHJcbiAgLy8gICAvLyB2YXIgdG9rZW4gPSB3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJyk7XHJcbiAgLy8gICAvLyB2YXIgdG9rZW5TdGF0ZSA9IHRoaXMuYXV0aFNlcnYuY2hlY2tUb2tlbih0b2tlbik7XHJcblxyXG5cclxuICAvLyAgIC8vIC8vaXMgdG9rZW4gJiYgaXMgZXhwaXJlc1xyXG4gIC8vICAgLy8gaWYodG9rZW5TdGF0ZSA9PT0gMSl7XHJcbiAgLy8gICAvLyAgIC8vc2V0IGV4cGlyZXNcclxuICAvLyAgIC8vICAgLy8gdGhpcy5hdXRoU2Vydi5zZXRUb2tlbih0b2tlbi5rZXkpO1xyXG4gIC8vICAgLy8gICB0aGlzLmF1dGhTZXJ2LnNldFRva2VuKHRva2VuKTtcclxuXHJcbiAgLy8gICAvLyAgIC8vIGlzIHJlZmVzaCBwYWdlXHJcbiAgLy8gICAvLyAgIC8vIGlmKCF0aGlzLmNvbm5lY3RlZCl7XHJcbiAgLy8gICAvLyAgICAgLy9nZXQgYXV0aGVudGljYXRlZCB1c2VyXHJcbiAgLy8gICAvLyAgICAgdGhpcy51c2VyU2Vydi5nZXRBdXRoZW50aWNhdGVkVXNlcigpO1xyXG4gIC8vICAgLy8gICAvLyB9XHJcbiAgICAgIFxyXG4gIC8vICAgLy8gICByZXR1cm4gdHJ1ZTtcclxuICAvLyAgIC8vIH0gZWxzZSBpZih0b2tlblN0YXRlID09PSAyKXsgLy8gaXMgdG9rZW4gJiYgbm90IGV4cGlyZXMgXHJcbiAgLy8gICAvLyAgICAgLy9yZW1vdmUgdG9rZW5cclxuICAvLyAgIC8vICAgIHRoaXMuYXV0aFNlcnYuc2V0VG9rZW4oJycpO1xyXG4gIC8vICAgLy8gICAgLy9yZW1vdmUgdXNlclxyXG4gIC8vICAgLy8gICAgdGhpcy51c2VyU2Vydi5yZW1vdmVDdXJyZW50VXNlcigpO1xyXG5cclxuICAvLyAgIC8vICAgIHJldHVybiBmYWxzZTtcclxuICAvLyAgIC8vIH1cclxuICAgICBcclxuICAvLyAgIC8vIHJldHVybiBmYWxzZTtcclxuICAvLyAgIHZhciBpc0F1dGhlbnRpY2F0ZWQgPSB0aGlzLmF1dGhTZXJ2LmF1dGhlbnRpY2F0ZWQoKTtcclxuICAvLyAgIGlmKGlzQXV0aGVudGljYXRlZCl7XHJcbiAgLy8gICAgIC8vZ2V0IGF1dGhlbnRpY2F0ZWQgdXNlclxyXG4gIC8vICAgICBpZighdGhpcy5pc0luaXQpe1xyXG4gIC8vICAgICAgIHRoaXMudXNlclNlcnYuZ2V0QXV0aGVudGljYXRlZFVzZXIoKTtcclxuICAvLyAgICAgfSBlbHNlIHtcclxuICAvLyAgICAgICB0aGlzLmlzSW5pdCA9IGZhbHNlO1xyXG4gIC8vICAgICB9XHJcbiAgICAgIFxyXG4gIC8vICAgfVxyXG4gIC8vICAgcmV0dXJuIGlzQXV0aGVudGljYXRlZDtcclxuICAvLyB9XHJcblxyXG4gIHB1YmxpYyBjYW5BY3RpdmF0ZSgpIHtcclxuXHJcbiAgICBjb25zb2xlLmxvZygnLS0tLS0tLS0tLS0tLS1jYW5BY3RpdmF0ZS0tLS0tLS0tLS0tLS0tLS0tLS0tJyk7XHJcbiAgICAvLyB0ZXN0IGhlcmUgaWYgeW91IHVzZXIgaXMgbG9nZ2VkXHJcbiAgICB2YXIgaXNBdXRoZW50aWNhdGVkID0gdGhpcy5hdXRoU2Vydi5hdXRoZW50aWNhdGVkKCk7XHJcbiAgICBpZiAoICFpc0F1dGhlbnRpY2F0ZWQgKSB7XHJcbiAgICAgIHRoaXMuYXV0aFNlcnYuc2V0VG9rZW4oJycpO1xyXG4gICAgICB0aGlzLnVzZXJTZXJ2LnJlbW92ZUN1cnJlbnRVc2VyKCk7XHJcbiAgICAgIFxyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZSggWyAnbG9naW4nIF0gKTtcclxuXHJcbiAgICAgIC8vIGlmKCF0aGlzLmlzSW5pdCl7XHJcbiAgICAgIC8vICAgdGhpcy51c2VyU2Vydi5nZXRBdXRoZW50aWNhdGVkVXNlcigpO1xyXG4gICAgICAvLyB9IGVsc2Uge1xyXG4gICAgICAvLyAgIHRoaXMuaXNJbml0ID0gZmFsc2U7XHJcbiAgICAgIC8vIH1cclxuICAgIH0gXHJcbiAgICAvLyBlbHNlIHtcclxuICAgIC8vICAgLy8gdGhpcy5hdXRoSHR0cC5nZXRcclxuICAgIC8vICAgLy8gY29uc29sZS5sb2codGhpcy5hdXRoSHR0cCk7XHJcbiAgICAvLyAgIHRoaXMudXNlclNlcnYuY3VycmVudFVzZXIuc3Vic2NyaWJlKCh1c2VyKSA9PiB7XHJcbiAgICAvLyAgICAgaWYoIXVzZXIuZW1haWwpe1xyXG4gICAgLy8gICAgICAgY29uc29sZS5sb2coJ3VzZXInLCB1c2VyKTtcclxuICAgIC8vICAgICAgICB0aGlzLnVzZXJTZXJ2LmdldEF1dGhlbnRpY2F0ZWRVc2VyKCk7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICB9LFxyXG4gICAgLy8gICBlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyggdGhpcy51c2VyU2Vydi5jdXJyZW50VXNlci5zdWJzY3JpYmUoKSk7XHJcbiAgICAvLyB9XHJcbiAgICAvLyBjb25zb2xlLmxvZygnLS0tLS0tY2FuQWN0aXZhdGUtLS0tLS0nLCB0aGlzLmF1dGhTZXJ2LmF1dGhlbnRpY2F0ZWQoKSk7XHJcbiAgICByZXR1cm4gaXNBdXRoZW50aWNhdGVkO1xyXG4gIH1cclxufVxyXG4iXX0=
