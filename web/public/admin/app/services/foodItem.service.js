"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var FoodItemService = (function () {
    function FoodItemService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    FoodItemService.prototype.getFoodItems = function (restId, categoryId, id) {
        console.log('start OptionFoodItems service ');
        var action = 'get_items_by_rest_id';
        var params = {
            'rest_id': restId,
            'category_id': categoryId,
            'id': id,
        };
        console.log('get Item service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    FoodItemService.prototype.createFoodItems = function (itemObject) {
        console.log('start createOptionFoodItems ');
        console.log('itemObject ', itemObject);
        var option_items = itemObject.item.option_items;
        var action = 'update_item_by_id';
        var params = {
            'rest_id': itemObject.rest_id,
            'user_id': itemObject.user_id,
            'item': {
                'category_id': itemObject.item.category_id,
                'id': itemObject.item.id,
                'name': itemObject.item.name,
                'price': itemObject.item.price,
                'thumb': itemObject.item.thumb,
                'sku': itemObject.item.sku,
                'point': itemObject.item.point,
                'is_active': itemObject.item.is_active,
                'is_alcoho': itemObject.item.is_alcoho,
                'food_type': itemObject.item.food_type,
                'description': itemObject.item.description,
                'is_delete': itemObject.item.is_delete,
                'update_in_lst_flg': itemObject.item.update_in_lst_flg,
                'option_items': option_items,
            }
        };
        console.log('get createFoodItems service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    FoodItemService.prototype.itemUploadFile = function (itemObject) {
        console.log('start item upload file');
        var action = 'upload_file';
        var params = {
            'id': itemObject.id,
            'thumb': itemObject.thumb,
            'typeScreen': 'items'
        };
        console.log('get item service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    FoodItemService.prototype.deleteFile = function (params) {
        var action = 'delete_file';
        console.log('get delete file service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    FoodItemService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], FoodItemService);
    return FoodItemService;
}());
exports.FoodItemService = FoodItemService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9mb29kSXRlbS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBRTNDLDBDQUF3QztBQUN4QyxtREFBaUQ7QUFHakQ7SUFFSTtRQUNHLG1DQUFtQztRQUMzQixNQUFjLEVBQ2QsVUFBeUI7UUFEekIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFbEMsT0FBTztJQUNULENBQUM7SUFDTSxzQ0FBWSxHQUFuQixVQUFvQixNQUFNLEVBQUMsVUFBVSxFQUFDLEVBQUU7UUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBRTdDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ25DLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLE1BQU07WUFDakIsYUFBYSxFQUFFLFVBQVU7WUFDekIsSUFBSSxFQUFFLEVBQUU7U0FDVixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDN0MsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0seUNBQWUsR0FBdEIsVUFBdUIsVUFBVTtRQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDdkMsSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDL0MsSUFBSSxNQUFNLEdBQUcsbUJBQW1CLENBQUM7UUFDaEMsSUFBSSxNQUFNLEdBQUc7WUFDVixTQUFTLEVBQUUsVUFBVSxDQUFDLE9BQU87WUFDN0IsU0FBUyxFQUFDLFVBQVUsQ0FBQyxPQUFPO1lBQzVCLE1BQU0sRUFBQztnQkFDTCxhQUFhLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXO2dCQUN6QyxJQUFJLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN2QixNQUFNLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJO2dCQUMzQixPQUFPLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUM3QixPQUFPLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUM3QixLQUFLLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHO2dCQUN6QixPQUFPLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUM3QixXQUFXLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUNyQyxXQUFXLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUNyQyxXQUFXLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUNyQyxhQUFhLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXO2dCQUN6QyxXQUFXLEVBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUNyQyxtQkFBbUIsRUFBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGlCQUFpQjtnQkFDckQsY0FBYyxFQUFDLFlBQVk7YUFDNUI7U0FDSCxDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDeEQsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ00sd0NBQWMsR0FBckIsVUFBc0IsVUFBVTtRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFckMsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzFCLElBQUksTUFBTSxHQUFHO1lBQ1IsSUFBSSxFQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ2xCLE9BQU8sRUFBQyxVQUFVLENBQUMsS0FBSztZQUN4QixZQUFZLEVBQUMsT0FBTztTQUN4QixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDN0MsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ0ksb0NBQVUsR0FBakIsVUFBa0IsTUFBTTtRQUV4QixJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUM7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBbkVVLGVBQWU7UUFEM0IsaUJBQVUsRUFBRTt5Q0FLVSxlQUFNO1lBQ0YsOEJBQWE7T0FMM0IsZUFBZSxDQW9FM0I7SUFBRCxzQkFBQztDQXBFRCxBQW9FQyxJQUFBO0FBcEVZLDBDQUFlIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9mb29kSXRlbS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBSZXBsYXlTdWJqZWN0IH0gZnJvbSAncnhqcy9SeCc7XHJcbmltcG9ydCB7IFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJy4vc2VydmVyLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRm9vZEl0ZW1TZXJ2aWNlIHsgICAgXHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAvLyBwcml2YXRlIHN0YXRlU2VydjogU3RhdGVTZXJ2aWNlLFxyXG4gICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgIHByaXZhdGUgc2VydmVyU2VydjogU2VydmVyU2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgIC8vIFRPRE9cclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRGb29kSXRlbXMocmVzdElkLGNhdGVnb3J5SWQsaWQpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IE9wdGlvbkZvb2RJdGVtcyBzZXJ2aWNlICcpO1xyXG4gICAgICBcclxuICAgICAgICBsZXQgYWN0aW9uID0gJ2dldF9pdGVtc19ieV9yZXN0X2lkJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogcmVzdElkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ2NhdGVnb3J5X2lkJzogY2F0ZWdvcnlJZCxcclxuICAgICAgICAgICAgJ2lkJzogaWQsLy9nZXQgZnJvbSBsb2dpblxyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBJdGVtIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH1cclxuICAgICAgICAgICAgICAgXHJcbiAgICAgIHB1YmxpYyBjcmVhdGVGb29kSXRlbXMoaXRlbU9iamVjdCl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgY3JlYXRlT3B0aW9uRm9vZEl0ZW1zICcpO1xyXG4gICAgICAgY29uc29sZS5sb2coJ2l0ZW1PYmplY3QgJywgaXRlbU9iamVjdCk7XHJcbiAgICAgICBsZXQgb3B0aW9uX2l0ZW1zID0gaXRlbU9iamVjdC5pdGVtLm9wdGlvbl9pdGVtcztcclxuICAgICAgICBsZXQgYWN0aW9uID0gJ3VwZGF0ZV9pdGVtX2J5X2lkJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogaXRlbU9iamVjdC5yZXN0X2lkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ3VzZXJfaWQnOml0ZW1PYmplY3QudXNlcl9pZCxcclxuICAgICAgICAgICAgJ2l0ZW0nOntcclxuICAgICAgICAgICAgICAnY2F0ZWdvcnlfaWQnOml0ZW1PYmplY3QuaXRlbS5jYXRlZ29yeV9pZCxcclxuICAgICAgICAgICAgICAnaWQnOml0ZW1PYmplY3QuaXRlbS5pZCxcclxuICAgICAgICAgICAgICAnbmFtZSc6aXRlbU9iamVjdC5pdGVtLm5hbWUsXHJcbiAgICAgICAgICAgICAgJ3ByaWNlJzppdGVtT2JqZWN0Lml0ZW0ucHJpY2UsXHJcbiAgICAgICAgICAgICAgJ3RodW1iJzppdGVtT2JqZWN0Lml0ZW0udGh1bWIsXHJcbiAgICAgICAgICAgICAgJ3NrdSc6aXRlbU9iamVjdC5pdGVtLnNrdSxcclxuICAgICAgICAgICAgICAncG9pbnQnOml0ZW1PYmplY3QuaXRlbS5wb2ludCxcclxuICAgICAgICAgICAgICAnaXNfYWN0aXZlJzppdGVtT2JqZWN0Lml0ZW0uaXNfYWN0aXZlLFxyXG4gICAgICAgICAgICAgICdpc19hbGNvaG8nOml0ZW1PYmplY3QuaXRlbS5pc19hbGNvaG8sXHJcbiAgICAgICAgICAgICAgJ2Zvb2RfdHlwZSc6aXRlbU9iamVjdC5pdGVtLmZvb2RfdHlwZSxcclxuICAgICAgICAgICAgICAnZGVzY3JpcHRpb24nOml0ZW1PYmplY3QuaXRlbS5kZXNjcmlwdGlvbixcclxuICAgICAgICAgICAgICAnaXNfZGVsZXRlJzppdGVtT2JqZWN0Lml0ZW0uaXNfZGVsZXRlLFxyXG4gICAgICAgICAgICAgICd1cGRhdGVfaW5fbHN0X2ZsZyc6aXRlbU9iamVjdC5pdGVtLnVwZGF0ZV9pbl9sc3RfZmxnLFxyXG4gICAgICAgICAgICAgICdvcHRpb25faXRlbXMnOm9wdGlvbl9pdGVtcyxcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBjcmVhdGVGb29kSXRlbXMgc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgICAgICAgcmV0dXJuICB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgfSAgICAgIFxyXG4gICAgICBwdWJsaWMgaXRlbVVwbG9hZEZpbGUoaXRlbU9iamVjdCl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgaXRlbSB1cGxvYWQgZmlsZScpO1xyXG4gICAgIFxyXG4gICAgICAgIGxldCBhY3Rpb24gPSAndXBsb2FkX2ZpbGUnOyBcclxuICAgICAgICAgbGV0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAnaWQnOml0ZW1PYmplY3QuaWQsXHJcbiAgICAgICAgICAgICAgJ3RodW1iJzppdGVtT2JqZWN0LnRodW1iLFxyXG4gICAgICAgICAgICAgICd0eXBlU2NyZWVuJzonaXRlbXMnXHJcbiAgICAgICAgIH07XHJcbiAgICAgICBjb25zb2xlLmxvZygnZ2V0IGl0ZW0gc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgICAgICAgcmV0dXJuICB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgfSBcclxuICAgIHB1YmxpYyBkZWxldGVGaWxlKHBhcmFtcykge1xyXG4gICAgXHJcbiAgICBsZXQgYWN0aW9uID0gJ2RlbGV0ZV9maWxlJztcclxuICAgIGNvbnNvbGUubG9nKCdnZXQgZGVsZXRlIGZpbGUgc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfSAgICAgIFxyXG59XHJcbiJdfQ==
