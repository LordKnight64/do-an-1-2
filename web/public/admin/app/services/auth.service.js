"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("app/services/server.service");
var angular2_jwt_1 = require("angular2-jwt");
var router_1 = require("@angular/router");
// jwtHelper: JwtHelper = new JwtHelper();
var CryptoJS = require("crypto-js");
var AuthService = (function () {
    function AuthService(serverServ, 
        // private userServ: UserService, 
        router) {
        this.serverServ = serverServ;
        this.router = router;
        //TODO
        this.KEY = CryptoJS.enc.Base64.parse("#base64Key#");
        this.IV = CryptoJS.enc.Base64.parse("#base64IV#");
    }
    AuthService.prototype.checkToken = function (token) {
        if (token) {
            // var current = moment();
            // var tokenExpiresAt = moment(token.expiresAt);
            // //check expires
            // if(current.diff(tokenExpiresAt) > environment.JWTTtl){
            //   // not expires 
            //   return 2;
            // }
            //is expires
            return 1;
        }
        return 3;
    };
    AuthService.prototype.authenticated = function () {
        // Check if there's an unexpired JWT
        // This searches for an item in localStorage with key == 'id_token'
        var token = localStorage.getItem('id_token');
        var idToken = "";
        console.log('encryptedToken ', token);
        if (token != undefined && token != null) {
            var decrypted = CryptoJS.AES.decrypt(token.toString(), this.KEY, { iv: this.IV });
            idToken = decrypted.toString(CryptoJS.enc.Utf8);
            console.log('descrypted ', idToken);
            return angular2_jwt_1.tokenNotExpired('id_token', idToken);
        }
        return false;
    };
    AuthService.prototype.setToken = function (tokenKey) {
        if (tokenKey) {
            var key = CryptoJS.enc.Base64.parse("#base64Key#");
            var iv = CryptoJS.enc.Base64.parse("#base64IV#");
            //Impementing the Key and IV and encrypt the password
            var encryptedToken = CryptoJS.AES.encrypt(tokenKey, this.KEY, { iv: this.IV });
            console.log('encrypted ', encryptedToken.toString());
            localStorage.setItem('id_token', encryptedToken.toString());
        }
        else {
            // window.sessionStorage.removeItem('token');
            localStorage.removeItem('id_token');
        }
    };
    AuthService.prototype.getToken = function () {
        var _this = this;
        var action = 'token';
        return this.serverServ.doPost(action, {}).then(function (response) { return _this.processResult(response); }, function (error) { return _this.handleError(error); });
    };
    ;
    AuthService.prototype.processResult = function (res) {
        var token = res.token;
        this.setToken(token);
    };
    AuthService.prototype.handleError = function (error) {
        console.log(error);
        // localStorage.removeItem('id_token');
        // this.userServ.removeCurrentUser();
        // this.router.navigate( ['login'] );
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService,
            router_1.Router])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBbUQ7QUFHbkQsOERBQTREO0FBSTVELDZDQUEwRDtBQUMxRCwwQ0FBd0M7QUFDeEMsMENBQTBDO0FBQzFDLG9DQUFzQztBQUd0QztJQUdFLHFCQUNVLFVBQXlCO1FBQ2pDLGtDQUFrQztRQUMxQixNQUFjO1FBRmQsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUV6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBRXRCLE1BQU07UUFDTixJQUFJLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0sZ0NBQVUsR0FBakIsVUFBa0IsS0FBSztRQUVyQixFQUFFLENBQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDO1lBQ1IsMEJBQTBCO1lBQzFCLGdEQUFnRDtZQUNoRCxrQkFBa0I7WUFDbEIseURBQXlEO1lBQ3pELG9CQUFvQjtZQUNwQixjQUFjO1lBQ2QsSUFBSTtZQUNKLFlBQVk7WUFDWixNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQztRQUNELE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0sbUNBQWEsR0FBcEI7UUFFRSxvQ0FBb0M7UUFDcEMsbUVBQW1FO1FBQ25FLElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFN0MsSUFBSyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUMsS0FBSyxDQUFDLENBQUM7UUFDckMsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFJLFNBQVMsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQztZQUNoRixPQUFPLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25DLE1BQU0sQ0FBQyw4QkFBZSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFTSw4QkFBUSxHQUFmLFVBQWdCLFFBQVE7UUFDdEIsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQztZQUNYLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNuRCxJQUFJLEVBQUUsR0FBSSxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEQscURBQXFEO1lBQ3JELElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDO1lBQzdFLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3BELFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLDZDQUE2QztZQUM3QyxZQUFZLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7SUFDSCxDQUFDO0lBRU0sOEJBQVEsR0FBZjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUM1QyxVQUFBLFFBQVEsSUFBSSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEVBQTVCLENBQTRCLEVBQ3hDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsQ0FDakMsQ0FBQztJQUNKLENBQUM7SUFBQSxDQUFDO0lBRU0sbUNBQWEsR0FBckIsVUFBc0IsR0FBRztRQUN2QixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVPLGlDQUFXLEdBQW5CLFVBQW9CLEtBQUs7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQix1Q0FBdUM7UUFDdkMscUNBQXFDO1FBQ3JDLHFDQUFxQztJQUN2QyxDQUFDO0lBOUVVLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTt5Q0FLVyw4QkFBYTtZQUVqQixlQUFNO09BTmIsV0FBVyxDQStFdkI7SUFBRCxrQkFBQztDQS9FRCxBQStFQyxJQUFBO0FBL0VZLGtDQUFXIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuLy8gaW1wb3J0IHsgU3RhdGVTZXJ2aWNlIH0gZnJvbSBcInVpLXJvdXRlci1uZzJcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICdhcHAvc2VydmljZXMvdXNlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJ2FwcC9zZXJ2aWNlcy9zZXJ2ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXIgfSBmcm9tICdhcHAvbW9kZWxzL3VzZXInO1xyXG5pbXBvcnQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XHJcbmltcG9ydCB7IEVOVklST05NRU5UIH0gZnJvbSAnZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcclxuaW1wb3J0IHsgdG9rZW5Ob3RFeHBpcmVkLCBKd3RIZWxwZXIgfSBmcm9tICdhbmd1bGFyMi1qd3QnO1xyXG5pbXBvcnQgeyBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbi8vIGp3dEhlbHBlcjogSnd0SGVscGVyID0gbmV3IEp3dEhlbHBlcigpO1xyXG5pbXBvcnQgKiBhcyBDcnlwdG9KUyBmcm9tICdjcnlwdG8tanMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2Uge1xyXG4gIHByaXZhdGUgS0VZOnN0cmluZztcclxuICBwcml2YXRlIElWOiBzdHJpbmc7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2UsIFxyXG4gICAgLy8gcHJpdmF0ZSB1c2VyU2VydjogVXNlclNlcnZpY2UsIFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlclxyXG4gICApIHtcclxuICAgIC8vVE9ET1xyXG4gICAgdGhpcy5LRVkgPSBDcnlwdG9KUy5lbmMuQmFzZTY0LnBhcnNlKFwiI2Jhc2U2NEtleSNcIik7XHJcbiAgICB0aGlzLklWID0gQ3J5cHRvSlMuZW5jLkJhc2U2NC5wYXJzZShcIiNiYXNlNjRJViNcIik7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2hlY2tUb2tlbih0b2tlbikge1xyXG4gICBcclxuICAgIGlmKHRva2VuKXtcclxuICAgICAgLy8gdmFyIGN1cnJlbnQgPSBtb21lbnQoKTtcclxuICAgICAgLy8gdmFyIHRva2VuRXhwaXJlc0F0ID0gbW9tZW50KHRva2VuLmV4cGlyZXNBdCk7XHJcbiAgICAgIC8vIC8vY2hlY2sgZXhwaXJlc1xyXG4gICAgICAvLyBpZihjdXJyZW50LmRpZmYodG9rZW5FeHBpcmVzQXQpID4gZW52aXJvbm1lbnQuSldUVHRsKXtcclxuICAgICAgLy8gICAvLyBub3QgZXhwaXJlcyBcclxuICAgICAgLy8gICByZXR1cm4gMjtcclxuICAgICAgLy8gfVxyXG4gICAgICAvL2lzIGV4cGlyZXNcclxuICAgICAgcmV0dXJuIDE7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gMztcclxuICB9XHJcblxyXG4gIHB1YmxpYyBhdXRoZW50aWNhdGVkKCkge1xyXG4gICBcclxuICAgIC8vIENoZWNrIGlmIHRoZXJlJ3MgYW4gdW5leHBpcmVkIEpXVFxyXG4gICAgLy8gVGhpcyBzZWFyY2hlcyBmb3IgYW4gaXRlbSBpbiBsb2NhbFN0b3JhZ2Ugd2l0aCBrZXkgPT0gJ2lkX3Rva2VuJ1xyXG4gICAgdmFyIHRva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2lkX3Rva2VuJyk7IFxyXG4gICBcclxuICAgIHZhciAgaWRUb2tlbiA9IFwiXCI7XHJcbiAgICBjb25zb2xlLmxvZygnZW5jcnlwdGVkVG9rZW4gJyx0b2tlbik7XHJcbiAgICBpZih0b2tlbiAhPSB1bmRlZmluZWQgJiYgdG9rZW4gIT0gbnVsbCkge1xyXG4gICAgICAgIHZhciBkZWNyeXB0ZWQgPSBDcnlwdG9KUy5BRVMuZGVjcnlwdCh0b2tlbi50b1N0cmluZygpLCB0aGlzLktFWSwge2l2OiB0aGlzLklWfSk7ICAgICAgIFxyXG4gICAgICAgIGlkVG9rZW4gPSBkZWNyeXB0ZWQudG9TdHJpbmcoQ3J5cHRvSlMuZW5jLlV0ZjgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdkZXNjcnlwdGVkICcsaWRUb2tlbik7XHJcbiAgICAgICAgcmV0dXJuIHRva2VuTm90RXhwaXJlZCgnaWRfdG9rZW4nLCBpZFRva2VuKTtcclxuICAgIH0gICAgICAgXHJcbiAgICByZXR1cm4gZmFsc2U7ICAgXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2V0VG9rZW4odG9rZW5LZXkpeyAgIFxyXG4gICAgaWYodG9rZW5LZXkpeyAgICAgXHJcbiAgICAgIHZhciBrZXkgPSBDcnlwdG9KUy5lbmMuQmFzZTY0LnBhcnNlKFwiI2Jhc2U2NEtleSNcIik7XHJcbiAgICAgIHZhciBpdiAgPSBDcnlwdG9KUy5lbmMuQmFzZTY0LnBhcnNlKFwiI2Jhc2U2NElWI1wiKTsgICAgICBcclxuICAgICAgLy9JbXBlbWVudGluZyB0aGUgS2V5IGFuZCBJViBhbmQgZW5jcnlwdCB0aGUgcGFzc3dvcmRcclxuICAgICAgdmFyIGVuY3J5cHRlZFRva2VuID0gQ3J5cHRvSlMuQUVTLmVuY3J5cHQodG9rZW5LZXksIHRoaXMuS0VZLCB7aXY6IHRoaXMuSVZ9KTsgIFxyXG4gICAgICBjb25zb2xlLmxvZygnZW5jcnlwdGVkICcsIGVuY3J5cHRlZFRva2VuLnRvU3RyaW5nKCkpOyAgICAgXHJcbiAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnaWRfdG9rZW4nLCBlbmNyeXB0ZWRUb2tlbi50b1N0cmluZygpKTsgXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyB3aW5kb3cuc2Vzc2lvblN0b3JhZ2UucmVtb3ZlSXRlbSgndG9rZW4nKTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2lkX3Rva2VuJyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VG9rZW4oKXtcclxuICAgIHZhciBhY3Rpb24gPSAndG9rZW4nO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCB7fSkudGhlbiggXHJcbiAgICAgIHJlc3BvbnNlID0+IHRoaXMucHJvY2Vzc1Jlc3VsdChyZXNwb25zZSksXHJcbiAgICAgIGVycm9yID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpXHJcbiAgICApO1xyXG4gIH07XHJcblxyXG4gIHByaXZhdGUgcHJvY2Vzc1Jlc3VsdChyZXMpe1xyXG4gICAgdmFyIHRva2VuID0gcmVzLnRva2VuO1xyXG4gICAgdGhpcy5zZXRUb2tlbih0b2tlbik7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yKXtcclxuICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgIC8vIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdpZF90b2tlbicpO1xyXG4gICAgLy8gdGhpcy51c2VyU2Vydi5yZW1vdmVDdXJyZW50VXNlcigpO1xyXG4gICAgLy8gdGhpcy5yb3V0ZXIubmF2aWdhdGUoIFsnbG9naW4nXSApO1xyXG4gIH1cclxufVxyXG4iXX0=
