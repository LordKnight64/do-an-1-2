"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var RestaurantService = (function () {
    function RestaurantService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    RestaurantService.prototype.getContents = function (params) {
        var action = 'get_contents_by_rest_id';
        return this.serverServ.doPost(action, params);
    };
    RestaurantService.prototype.updateContents = function (params) {
        var action = 'update_contents_by_rest_id';
        return this.serverServ.doPost(action, params);
    };
    RestaurantService.prototype.getOrderSetting = function (params) {
        var action = 'get_order_setting_by_rest_id';
        return this.serverServ.doPost(action, params);
    };
    RestaurantService.prototype.updateOrderSetting = function (params) {
        var action = 'update_order_setting_by_rest_id';
        return this.serverServ.doPost(action, params);
    };
    RestaurantService.prototype.sendFeeback = function (params) {
        var action = 'send_feedback';
        return this.serverServ.doPost(action, params);
    };
    RestaurantService.prototype.handleError = function (res) {
        //TODO
    };
    RestaurantService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], RestaurantService);
    return RestaurantService;
}());
exports.RestaurantService = RestaurantService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9yZXN0YXVyYW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsbURBQWlEO0FBR2pEO0lBRUUsMkJBQ1UsVUFBeUI7UUFBekIsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVqQyxPQUFPO0lBQ1QsQ0FBQztJQUVNLHVDQUFXLEdBQWxCLFVBQW1CLE1BQU07UUFFdkIsSUFBSSxNQUFNLEdBQUcseUJBQXlCLENBQUM7UUFDdkMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU0sMENBQWMsR0FBckIsVUFBc0IsTUFBTTtRQUUxQixJQUFJLE1BQU0sR0FBRyw0QkFBNEIsQ0FBQztRQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSwyQ0FBZSxHQUF0QixVQUF1QixNQUFNO1FBRTNCLElBQUksTUFBTSxHQUFHLDhCQUE4QixDQUFDO1FBQzVDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVNLDhDQUFrQixHQUF6QixVQUEwQixNQUFNO1FBRTlCLElBQUksTUFBTSxHQUFHLGlDQUFpQyxDQUFDO1FBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVPLHVDQUFXLEdBQWxCLFVBQW1CLE1BQU07UUFDeEIsSUFBSSxNQUFNLEdBQUcsZUFBZSxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUNPLHVDQUFXLEdBQW5CLFVBQW9CLEdBQUc7UUFDckIsTUFBTTtJQUNSLENBQUM7SUF0Q1UsaUJBQWlCO1FBRDdCLGlCQUFVLEVBQUU7eUNBSVcsOEJBQWE7T0FIeEIsaUJBQWlCLENBdUM3QjtJQUFELHdCQUFDO0NBdkNELEFBdUNDLElBQUE7QUF2Q1ksOENBQWlCIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9yZXN0YXVyYW50LnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFJlc3RhdXJhbnRTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIFRPRE9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb250ZW50cyhwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9jb250ZW50c19ieV9yZXN0X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyB1cGRhdGVDb250ZW50cyhwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ3VwZGF0ZV9jb250ZW50c19ieV9yZXN0X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRPcmRlclNldHRpbmcocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfb3JkZXJfc2V0dGluZ19ieV9yZXN0X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyB1cGRhdGVPcmRlclNldHRpbmcocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfb3JkZXJfc2V0dGluZ19ieV9yZXN0X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcbiAgXHJcbiAgIHB1YmxpYyBzZW5kRmVlYmFjayhwYXJhbXMpIHtcclxuICAgIGxldCBhY3Rpb24gPSAnc2VuZF9mZWVkYmFjayc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG4gIHByaXZhdGUgaGFuZGxlRXJyb3IocmVzKSB7XHJcbiAgICAvL1RPRE9cclxuICB9XHJcbn0iXX0=
