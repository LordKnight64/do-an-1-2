"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var OptionValueFoodItemService = (function () {
    function OptionValueFoodItemService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    OptionValueFoodItemService.prototype.getOptionValueFoodItems = function (restId, id) {
        console.log('start OptionFoodItems service ');
        var action = 'get_options_value_by_rest_id';
        var params = {
            'rest_id': restId,
            'id': id,
        };
        console.log('get categories service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    OptionValueFoodItemService.prototype.createOptionValueFoodItems = function (optionItemObject) {
        console.log('start createOptionFoodItems ');
        var action = 'update_option_value_by_id';
        var params = {
            'rest_id': optionItemObject.rest_id,
            'user_id': optionItemObject.user_id,
            'option': {
                'id': optionItemObject.option.id,
                'name': optionItemObject.option.name,
                'option_id': optionItemObject.option.option_id,
                'is_delete': optionItemObject.option.is_delete
            },
        };
        console.log('get createOptionFoodItems service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    OptionValueFoodItemService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], OptionValueFoodItemService);
    return OptionValueFoodItemService;
}());
exports.OptionValueFoodItemService = OptionValueFoodItemService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9vcHRpb25WYWx1ZUZvb2RJdGVtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFFM0MsMENBQXdDO0FBQ3hDLG1EQUFpRDtBQUdqRDtJQUVJO1FBQ0csbUNBQW1DO1FBQzNCLE1BQWMsRUFDZCxVQUF5QjtRQUR6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVsQyxPQUFPO0lBQ1QsQ0FBQztJQUNNLDREQUF1QixHQUE5QixVQUErQixNQUFNLEVBQUMsRUFBRTtRQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFFN0MsSUFBSSxNQUFNLEdBQUcsOEJBQThCLENBQUM7UUFDM0MsSUFBSSxNQUFNLEdBQUc7WUFDVixTQUFTLEVBQUUsTUFBTTtZQUNqQixJQUFJLEVBQUUsRUFBRTtTQUNWLENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNuRCxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDTSwrREFBMEIsR0FBakMsVUFBa0MsZ0JBQWdCO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUUzQyxJQUFJLE1BQU0sR0FBRywyQkFBMkIsQ0FBQztRQUN4QyxJQUFJLE1BQU0sR0FBRztZQUNWLFNBQVMsRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO1lBQ25DLFNBQVMsRUFBQyxnQkFBZ0IsQ0FBQyxPQUFPO1lBQ2xDLFFBQVEsRUFBQztnQkFDUCxJQUFJLEVBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sRUFBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSTtnQkFDbkMsV0FBVyxFQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxTQUFTO2dCQUM3QyxXQUFXLEVBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFNBQVM7YUFDOUM7U0FFSCxDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQ0FBb0MsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUQsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBckNNLDBCQUEwQjtRQUR0QyxpQkFBVSxFQUFFO3lDQUtVLGVBQU07WUFDRiw4QkFBYTtPQUwzQiwwQkFBMEIsQ0F1Q3RDO0lBQUQsaUNBQUM7Q0F2Q0QsQUF1Q0MsSUFBQTtBQXZDWSxnRUFBMEIiLCJmaWxlIjoiYXBwL3NlcnZpY2VzL29wdGlvblZhbHVlRm9vZEl0ZW0uc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgUmVwbGF5U3ViamVjdCB9IGZyb20gJ3J4anMvUngnO1xyXG5pbXBvcnQgeyBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlIHsgICAgXHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAvLyBwcml2YXRlIHN0YXRlU2VydjogU3RhdGVTZXJ2aWNlLFxyXG4gICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgIHByaXZhdGUgc2VydmVyU2VydjogU2VydmVyU2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgIC8vIFRPRE9cclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRPcHRpb25WYWx1ZUZvb2RJdGVtcyhyZXN0SWQsaWQpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IE9wdGlvbkZvb2RJdGVtcyBzZXJ2aWNlICcpO1xyXG4gICAgIFxyXG4gICAgICAgIGxldCBhY3Rpb24gPSAnZ2V0X29wdGlvbnNfdmFsdWVfYnlfcmVzdF9pZCc7IFxyXG4gICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAncmVzdF9pZCc6IHJlc3RJZCwvL2dldCBmcm9tIGxvZ2luXHJcbiAgICAgICAgICAgICdpZCc6IGlkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgY2F0ZWdvcmllcyBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9ICAgICAgICAgXHJcbiAgICAgIHB1YmxpYyBjcmVhdGVPcHRpb25WYWx1ZUZvb2RJdGVtcyhvcHRpb25JdGVtT2JqZWN0KXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBjcmVhdGVPcHRpb25Gb29kSXRlbXMgJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfb3B0aW9uX3ZhbHVlX2J5X2lkJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogb3B0aW9uSXRlbU9iamVjdC5yZXN0X2lkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ3VzZXJfaWQnOm9wdGlvbkl0ZW1PYmplY3QudXNlcl9pZCxcclxuICAgICAgICAgICAgJ29wdGlvbic6e1xyXG4gICAgICAgICAgICAgICdpZCc6b3B0aW9uSXRlbU9iamVjdC5vcHRpb24uaWQsXHJcbiAgICAgICAgICAgICAgJ25hbWUnOm9wdGlvbkl0ZW1PYmplY3Qub3B0aW9uLm5hbWUsXHJcbiAgICAgICAgICAgICAgJ29wdGlvbl9pZCc6b3B0aW9uSXRlbU9iamVjdC5vcHRpb24ub3B0aW9uX2lkLFxyXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOm9wdGlvbkl0ZW1PYmplY3Qub3B0aW9uLmlzX2RlbGV0ZVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgY3JlYXRlT3B0aW9uRm9vZEl0ZW1zIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH0gICAgICBcclxuICAgICAgIFxyXG59XHJcbiJdfQ==
