"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var TableService = (function () {
    function TableService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    TableService.prototype.getTables = function (restId, id, area_id) {
        console.log('start get areas service ');
        var action = 'get_table_by_rest_id';
        var params = {
            'rest_id': restId,
            'id': id,
            'area_id': area_id
        };
        console.log('get tables service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    TableService.prototype.createTable = function (tableObject) {
        console.log('start table ');
        var action = 'update_table_by_id';
        var params = {
            'rest_id': tableObject.rest_id,
            'user_id': tableObject.user_id,
            'table': {
                'id': tableObject.table.id,
                'name': tableObject.table.name,
                'thumb': tableObject.table.thumb,
                'slots': tableObject.table.slots,
                'content': tableObject.table.content,
                'is_active': tableObject.table.is_active,
                'is_delete': tableObject.table.is_delete
            },
        };
        console.log('get table service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    TableService.prototype.tableUploadFile = function (tableObject) {
        console.log('start createTable upload file');
        var action = 'upload_file';
        var params = {
            'id': tableObject.id,
            'thumb': tableObject.thumb,
            'typeScreen': 'table'
        };
        console.log('get areas service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    TableService.prototype.deleteFile = function (params) {
        var action = 'delete_file';
        console.log('get delete file service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    TableService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], TableService);
    return TableService;
}());
exports.TableService = TableService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy90YWJsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBRTNDLDBDQUF3QztBQUN4QyxtREFBaUQ7QUFHakQ7SUFFSTtRQUNHLG1DQUFtQztRQUMzQixNQUFjLEVBQ2QsVUFBeUI7UUFEekIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFbEMsT0FBTztJQUNULENBQUM7SUFJTSxnQ0FBUyxHQUFoQixVQUFpQixNQUFNLEVBQUMsRUFBRSxFQUFDLE9BQU87UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBRXZDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ25DLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLE1BQU07WUFDakIsSUFBSSxFQUFFLEVBQUU7WUFDUixTQUFTLEVBQUUsT0FBTztTQUNwQixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0MsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0sa0NBQVcsR0FBbEIsVUFBbUIsV0FBVztRQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBRTNCLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO1FBQ2pDLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLFdBQVcsQ0FBQyxPQUFPO1lBQzlCLFNBQVMsRUFBQyxXQUFXLENBQUMsT0FBTztZQUM3QixPQUFPLEVBQUM7Z0JBQ04sSUFBSSxFQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDekIsTUFBTSxFQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSTtnQkFDN0IsT0FBTyxFQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSztnQkFDL0IsT0FBTyxFQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSztnQkFDL0IsU0FBUyxFQUFFLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTztnQkFDcEMsV0FBVyxFQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsU0FBUztnQkFDdkMsV0FBVyxFQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsU0FBUzthQUN4QztTQUVILENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFTSxzQ0FBZSxHQUF0QixVQUF1QixXQUFXO1FBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUU1QyxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUM7UUFDMUIsSUFBSSxNQUFNLEdBQUc7WUFDUixJQUFJLEVBQUMsV0FBVyxDQUFDLEVBQUU7WUFDbkIsT0FBTyxFQUFDLFdBQVcsQ0FBQyxLQUFLO1lBQ3pCLFlBQVksRUFBQyxPQUFPO1NBQ3hCLENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDSSxpQ0FBVSxHQUFqQixVQUFrQixNQUFNO1FBRXhCLElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN4RCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFoRVUsWUFBWTtRQUR4QixpQkFBVSxFQUFFO3lDQUtVLGVBQU07WUFDRiw4QkFBYTtPQUwzQixZQUFZLENBaUV4QjtJQUFELG1CQUFDO0NBakVELEFBaUVDLElBQUE7QUFqRVksb0NBQVkiLCJmaWxlIjoiYXBwL3NlcnZpY2VzL3RhYmxlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFJlcGxheVN1YmplY3QgfSBmcm9tICdyeGpzL1J4JztcclxuaW1wb3J0IHsgUm91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2ZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBUYWJsZVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgLy8gcHJpdmF0ZSBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSxcclxuICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAvLyBUT0RPXHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICBwdWJsaWMgZ2V0VGFibGVzKHJlc3RJZCxpZCxhcmVhX2lkKXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBnZXQgYXJlYXMgc2VydmljZSAnKTtcclxuXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICdnZXRfdGFibGVfYnlfcmVzdF9pZCc7XHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogcmVzdElkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ2lkJzogaWQsLy9nZXQgZnJvbSBsb2dpblxyXG4gICAgICAgICAgICAnYXJlYV9pZCc6IGFyZWFfaWRcclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgdGFibGVzIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH1cblxyXG4gICAgICBwdWJsaWMgY3JlYXRlVGFibGUodGFibGVPYmplY3Qpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IHRhYmxlICcpO1xyXG5cclxuICAgICAgICBsZXQgYWN0aW9uID0gJ3VwZGF0ZV90YWJsZV9ieV9pZCc7XHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogdGFibGVPYmplY3QucmVzdF9pZCwvL2dldCBmcm9tIGxvZ2luXHJcbiAgICAgICAgICAgICd1c2VyX2lkJzp0YWJsZU9iamVjdC51c2VyX2lkLFxyXG4gICAgICAgICAgICAndGFibGUnOntcclxuICAgICAgICAgICAgICAnaWQnOnRhYmxlT2JqZWN0LnRhYmxlLmlkLFxyXG4gICAgICAgICAgICAgICduYW1lJzp0YWJsZU9iamVjdC50YWJsZS5uYW1lLFxyXG4gICAgICAgICAgICAgICd0aHVtYic6dGFibGVPYmplY3QudGFibGUudGh1bWIsXHJcbiAgICAgICAgICAgICAgJ3Nsb3RzJzp0YWJsZU9iamVjdC50YWJsZS5zbG90cyxcclxuICAgICAgICAgICAgICAnY29udGVudCc6IHRhYmxlT2JqZWN0LnRhYmxlLmNvbnRlbnQsXHJcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6dGFibGVPYmplY3QudGFibGUuaXNfYWN0aXZlLFxyXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOnRhYmxlT2JqZWN0LnRhYmxlLmlzX2RlbGV0ZVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgdGFibGUgc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgICAgICAgcmV0dXJuICB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgfVxuXHJcbiAgICAgIHB1YmxpYyB0YWJsZVVwbG9hZEZpbGUodGFibGVPYmplY3Qpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IGNyZWF0ZVRhYmxlIHVwbG9hZCBmaWxlJyk7XHJcblxyXG4gICAgICAgIGxldCBhY3Rpb24gPSAndXBsb2FkX2ZpbGUnO1xyXG4gICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICdpZCc6dGFibGVPYmplY3QuaWQsXHJcbiAgICAgICAgICAgICAgJ3RodW1iJzp0YWJsZU9iamVjdC50aHVtYixcclxuICAgICAgICAgICAgICAndHlwZVNjcmVlbic6J3RhYmxlJ1xyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBhcmVhcyBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9XHJcbiAgICBwdWJsaWMgZGVsZXRlRmlsZShwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2RlbGV0ZV9maWxlJztcclxuICAgIGNvbnNvbGUubG9nKCdnZXQgZGVsZXRlIGZpbGUgc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
