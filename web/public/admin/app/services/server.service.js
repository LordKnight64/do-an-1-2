"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Moiz.Kachwala on 02-06-2016.
 */
var core_1 = require("@angular/core");
// import { Http, Response } from '@angular/http';
var http_1 = require("@angular/http");
var angular2_jwt_1 = require("angular2-jwt");
var logger_service_1 = require("app/services/logger.service");
require("rxjs/add/operator/toPromise");
var environment_1 = require("environments/environment");
var router_1 = require("@angular/router");
var moment = require("moment");
var ServerService = (function () {
    function ServerService(
        // private http: Http, 
        logger, window, authHttp, router) {
        this.logger = logger;
        this.authHttp = authHttp;
        this.router = router;
        this.url = environment_1.ENVIRONMENT.DOMAIN_API + ':' + environment_1.ENVIRONMENT.SERVER_PORT + '/';
    }
    ServerService.prototype.doPost = function (action, params) {
        // let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':  'Bearer'});
        // // let headers = new Headers({ 'Content-Type': 'application/json'});
        var _this = this;
        // // var token = JSON.parse(window.sessionStorage.getItem('token'));
        // var token = localStorage.getItem('token');
        //   if (token) {
        //     // headers.set('Authorization', 'Bearer ' + token.key);
        //     headers.set('Authorization', 'Bearer ' + token);
        //   } 
        //  let options = new RequestOptions({ headers: headers });
        // console.log('headers', headers);
        if (environment_1.ENVIRONMENT.PRODUCTION === false) {
            var actionStartDate = moment();
            console.log(params);
            this.logger.log(actionStartDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   START]', action);
            this.logger.log('PARAM', JSON.stringify(params));
        }
        // console.log('this.authHttp.get', this.authHttp.get());
        return this.authHttp.post(this.url + action, params)
            .map(function (response) { return _this.extractData(response, actionStartDate); })
            .toPromise()
            .catch(function (error) { return _this.handleError(error, _this.router); });
    };
    ServerService.prototype.extractData = function (res, actionStartDate) {
        if (environment_1.ENVIRONMENT.PRODUCTION === false) {
            var actionEndDate = moment();
            this.logger.log(actionEndDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   END  ]', res.url + ' [' + (actionEndDate.diff(actionStartDate)) + 'ms]');
            this.logger.log('RESULT', JSON.stringify(res.json()));
        }
        var data = res.json();
        return data || {};
    };
    ServerService.prototype.handleError = function (error, router) {
        // In a real world app, we might use a remote logging infrastructure
        if (error) {
            switch (error.status) {
                case 403:
                    // this.router.navigate(['log']);
                    break;
                case 401:
                    localStorage.removeItem('id_token');
                    this.router.navigate(['login']);
                    break;
                default:
                    this.router.navigate(['error']);
                    break;
            }
        }
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        // this.logger.log('Error', errMsg);
        console.log('Error: ' + errMsg);
        return Promise.reject(error);
    };
    ServerService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject("windowObject")),
        __metadata("design:paramtypes", [logger_service_1.LoggerService,
            Window,
            angular2_jwt_1.AuthHttp,
            router_1.Router])
    ], ServerService);
    return ServerService;
}());
exports.ServerService = ServerService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9zZXJ2ZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBOztHQUVHO0FBQ0gsc0NBQW1EO0FBQ25ELGtEQUFrRDtBQUNsRCxzQ0FBeUM7QUFFekMsNkNBQXdDO0FBRXhDLDhEQUE0RDtBQUM1RCx1Q0FBcUM7QUFDckMsd0RBQXVEO0FBQ3ZELDBDQUF5QztBQUV6QywrQkFBaUM7QUFHakM7SUFHSTtRQUNJLHVCQUF1QjtRQUNmLE1BQXFCLEVBQ0wsTUFBYyxFQUMvQixRQUFrQixFQUNsQixNQUFjO1FBSGIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUV0QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFOakIsUUFBRyxHQUFHLHlCQUFXLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyx5QkFBVyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7SUFPeEUsQ0FBQztJQUVKLDhCQUFNLEdBQU4sVUFBTyxNQUFjLEVBQUUsTUFBVztRQUU5QixnR0FBZ0c7UUFDaEcsdUVBQXVFO1FBSDNFLGlCQTJCQztRQXRCRyxxRUFBcUU7UUFDckUsNkNBQTZDO1FBQzdDLGlCQUFpQjtRQUNqQiw4REFBOEQ7UUFDOUQsdURBQXVEO1FBQ3ZELE9BQU87UUFDUCwyREFBMkQ7UUFDMUQsbUNBQW1DO1FBQ3BDLEVBQUUsQ0FBQyxDQUFDLHlCQUFXLENBQUMsVUFBVSxLQUFNLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEMsSUFBSSxlQUFlLEdBQUcsTUFBTSxFQUFFLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsbUJBQW1CLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDakcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNyRCxDQUFDO1FBQ0QseURBQXlEO1FBRXpELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sRUFBRSxNQUFNLENBQUM7YUFDL0MsR0FBRyxDQUNBLFVBQUEsUUFBUSxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLEVBQTNDLENBQTJDLENBQzFEO2FBQ0EsU0FBUyxFQUFFO2FBQ1gsS0FBSyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFwQyxDQUFvQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVPLG1DQUFXLEdBQW5CLFVBQW9CLEdBQWEsRUFBRSxlQUFvQjtRQUNuRCxFQUFFLENBQUMsQ0FBQyx5QkFBVyxDQUFDLFVBQVUsS0FBTSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksYUFBYSxHQUFHLE1BQU0sRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxtQkFBbUIsRUFBRSxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUN2SixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNELENBQUM7UUFDRCxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsTUFBTSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVPLG1DQUFXLEdBQW5CLFVBQW9CLEtBQXFCLEVBQUUsTUFBYztRQUNyRCxvRUFBb0U7UUFFbkUsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQztZQUNQLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixLQUFLLEdBQUc7b0JBQ0osaUNBQWlDO29CQUNqQyxLQUFLLENBQUM7Z0JBQ1YsS0FBSyxHQUFHO29CQUNKLFlBQVksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFFLENBQUMsT0FBTyxDQUFDLENBQUUsQ0FBQztvQkFDbEMsS0FBSyxDQUFDO2dCQUNWO29CQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDaEMsS0FBSyxDQUFDO1lBQ2QsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLE1BQWMsQ0FBQztRQUNuQixFQUFFLENBQUMsQ0FBQyxLQUFLLFlBQVksZUFBUSxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFNLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDO1lBQ2hDLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQyxNQUFNLEdBQU0sS0FBSyxDQUFDLE1BQU0sWUFBTSxLQUFLLENBQUMsVUFBVSxJQUFJLEVBQUUsVUFBSSxHQUFLLENBQUM7UUFDbEUsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUQsQ0FBQztRQUNELG9DQUFvQztRQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsQ0FBQztRQUNoQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBL0VRLGFBQWE7UUFEekIsaUJBQVUsRUFBRTtRQU9KLFdBQUEsYUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFBO3lDQURQLDhCQUFhO1lBQ0csTUFBTTtZQUNyQix1QkFBUTtZQUNWLGVBQU07T0FSaEIsYUFBYSxDQWdGekI7SUFBRCxvQkFBQztDQWhGRCxBQWdGQyxJQUFBO0FBaEZZLHNDQUFhIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9zZXJ2ZXIuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSBNb2l6LkthY2h3YWxhIG9uIDAyLTA2LTIwMTYuXG4gKi9cbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuLy8gaW1wb3J0IHsgSHR0cCwgUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBIZWFkZXJzLCBSZXF1ZXN0T3B0aW9ucyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgQXV0aEh0dHAgfSBmcm9tICdhbmd1bGFyMi1qd3QnO1xuXG5pbXBvcnQgeyBMb2dnZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL3NlcnZpY2VzL2xvZ2dlci5zZXJ2aWNlJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvdG9Qcm9taXNlJztcbmltcG9ydCB7IEVOVklST05NRU5UIH0gZnJvbSAnZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmltcG9ydCBtb21lbnQgPSByZXF1aXJlKCdtb21lbnQnKVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU2VydmVyU2VydmljZSB7XG5cbiAgICBwcml2YXRlIHVybCA9IEVOVklST05NRU5ULkRPTUFJTl9BUEkgKyAnOicgKyBFTlZJUk9OTUVOVC5TRVJWRVJfUE9SVCArICcvJztcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgLy8gcHJpdmF0ZSBodHRwOiBIdHRwLCBcbiAgICAgICAgcHJpdmF0ZSBsb2dnZXI6IExvZ2dlclNlcnZpY2UsIFxuICAgICAgICBASW5qZWN0KFwid2luZG93T2JqZWN0XCIpIHdpbmRvdzogV2luZG93LCBcbiAgICAgICAgcHVibGljIGF1dGhIdHRwOiBBdXRoSHR0cCxcbiAgICAgICAgcHVibGljIHJvdXRlcjogUm91dGVyXG4gICAgKSB7fVxuXG4gICAgZG9Qb3N0KGFjdGlvbjogc3RyaW5nLCBwYXJhbXM6IGFueSk6IFByb21pc2U8YW55PiB7XG5cbiAgICAgICAgLy8gbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsICdBdXRob3JpemF0aW9uJzogICdCZWFyZXInfSk7XG4gICAgICAgIC8vIC8vIGxldCBoZWFkZXJzID0gbmV3IEhlYWRlcnMoeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nfSk7XG4gICAgICAgXG4gICAgICAgIC8vIC8vIHZhciB0b2tlbiA9IEpTT04ucGFyc2Uod2luZG93LnNlc3Npb25TdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJykpO1xuICAgICAgICAvLyB2YXIgdG9rZW4gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndG9rZW4nKTtcbiAgICAgICAgLy8gICBpZiAodG9rZW4pIHtcbiAgICAgICAgLy8gICAgIC8vIGhlYWRlcnMuc2V0KCdBdXRob3JpemF0aW9uJywgJ0JlYXJlciAnICsgdG9rZW4ua2V5KTtcbiAgICAgICAgLy8gICAgIGhlYWRlcnMuc2V0KCdBdXRob3JpemF0aW9uJywgJ0JlYXJlciAnICsgdG9rZW4pO1xuICAgICAgICAvLyAgIH0gXG4gICAgICAgIC8vICBsZXQgb3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XG4gICAgICAgICAvLyBjb25zb2xlLmxvZygnaGVhZGVycycsIGhlYWRlcnMpO1xuICAgICAgICBpZiAoRU5WSVJPTk1FTlQuUFJPRFVDVElPTiA9PT0gIGZhbHNlKSB7XG4gICAgICAgICAgICB2YXIgYWN0aW9uU3RhcnREYXRlID0gbW9tZW50KCk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhwYXJhbXMpO1xuICAgICAgICAgICAgdGhpcy5sb2dnZXIubG9nKGFjdGlvblN0YXJ0RGF0ZS5mb3JtYXQoXCJZWVlZL01NL0REIEhIOm1tOnNzLlNTU1wiKSArICcgW0FDVElPTiAgIFNUQVJUXScsIGFjdGlvbik7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ1BBUkFNJywgSlNPTi5zdHJpbmdpZnkocGFyYW1zKSk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5sb2coJ3RoaXMuYXV0aEh0dHAuZ2V0JywgdGhpcy5hdXRoSHR0cC5nZXQoKSk7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5hdXRoSHR0cC5wb3N0KHRoaXMudXJsICsgYWN0aW9uLCBwYXJhbXMpXG4gICAgICAgICAgICAubWFwKFxuICAgICAgICAgICAgICAgIHJlc3BvbnNlID0+IHRoaXMuZXh0cmFjdERhdGEocmVzcG9uc2UsIGFjdGlvblN0YXJ0RGF0ZSlcbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIC50b1Byb21pc2UoKVxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IsIHRoaXMucm91dGVyKSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBleHRyYWN0RGF0YShyZXM6IFJlc3BvbnNlLCBhY3Rpb25TdGFydERhdGU6IGFueSkge1xuICAgICAgICBpZiAoRU5WSVJPTk1FTlQuUFJPRFVDVElPTiA9PT0gIGZhbHNlKSB7XG4gICAgICAgICAgICB2YXIgYWN0aW9uRW5kRGF0ZSA9IG1vbWVudCgpO1xuICAgICAgICAgICAgdGhpcy5sb2dnZXIubG9nKGFjdGlvbkVuZERhdGUuZm9ybWF0KFwiWVlZWS9NTS9ERCBISDptbTpzcy5TU1NcIikgKyAnIFtBQ1RJT04gICBFTkQgIF0nLCByZXMudXJsICsgJyBbJyArIChhY3Rpb25FbmREYXRlLmRpZmYoYWN0aW9uU3RhcnREYXRlKSkgKyAnbXNdJyk7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ1JFU1VMVCcgLCBKU09OLnN0cmluZ2lmeShyZXMuanNvbigpKSk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGRhdGEgPSByZXMuanNvbigpO1xuICAgICAgICByZXR1cm4gZGF0YSB8fCB7fTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yOiBSZXNwb25zZSB8IGFueSwgcm91dGVyOiBSb3V0ZXIpOiBQcm9taXNlPGFueT4ge1xuICAgICAgICAvLyBJbiBhIHJlYWwgd29ybGQgYXBwLCB3ZSBtaWdodCB1c2UgYSByZW1vdGUgbG9nZ2luZyBpbmZyYXN0cnVjdHVyZVxuICAgICAgXG4gICAgICAgICBpZihlcnJvcil7XG4gICAgICAgICAgICBzd2l0Y2ggKGVycm9yLnN0YXR1cykge1xuICAgICAgICAgICAgICAgIGNhc2UgNDAzOlxuICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2xvZyddKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0MDE6XG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdpZF90b2tlbicpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZSggWydsb2dpbiddICk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnZXJyb3InXSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGVyck1zZzogc3RyaW5nO1xuICAgICAgICBpZiAoZXJyb3IgaW5zdGFuY2VvZiBSZXNwb25zZSkge1xuICAgICAgICAgICAgY29uc3QgYm9keSA9IGVycm9yLmpzb24oKSB8fCAnJztcbiAgICAgICAgICAgIGNvbnN0IGVyciA9IGJvZHkuZXJyb3IgfHwgSlNPTi5zdHJpbmdpZnkoYm9keSk7XG4gICAgICAgICAgICBlcnJNc2cgPSBgJHtlcnJvci5zdGF0dXN9IC0gJHtlcnJvci5zdGF0dXNUZXh0IHx8ICcnfSAke2Vycn1gO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZXJyTXNnID0gZXJyb3IubWVzc2FnZSA/IGVycm9yLm1lc3NhZ2UgOiBlcnJvci50b1N0cmluZygpO1xuICAgICAgICB9XG4gICAgICAgIC8vIHRoaXMubG9nZ2VyLmxvZygnRXJyb3InLCBlcnJNc2cpO1xuICAgICAgICBjb25zb2xlLmxvZygnRXJyb3I6ICcgKyBlcnJNc2cpO1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgIH1cbn0iXX0=
