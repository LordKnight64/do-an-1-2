"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var ReviewService = (function () {
    function ReviewService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    ReviewService.prototype.getRestaurantReviews = function (params) {
        var action = 'get_restaurant_reviews';
        return this.serverServ.doPost(action, params);
    };
    ReviewService.prototype.updateRestaurantReview = function (params) {
        var action = 'update_restaurant_review';
        return this.serverServ.doPost(action, params);
    };
    ReviewService.prototype.deleteRestaurantReview = function (params) {
        var action = 'delete_restaurant_review';
        return this.serverServ.doPost(action, params);
    };
    ReviewService.prototype.getItemsReviews = function (params) {
        var action = 'get_items_reviews';
        return this.serverServ.doPost(action, params);
    };
    ReviewService.prototype.handleError = function (res) {
        //TODO
    };
    ReviewService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], ReviewService);
    return ReviewService;
}());
exports.ReviewService = ReviewService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9yZXZpZXcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUEyQztBQUMzQyxtREFBaUQ7QUFHakQ7SUFFRSx1QkFDVSxVQUF5QjtRQUF6QixlQUFVLEdBQVYsVUFBVSxDQUFlO1FBRWpDLE9BQU87SUFDVCxDQUFDO0lBRU0sNENBQW9CLEdBQTNCLFVBQTRCLE1BQU07UUFFaEMsSUFBSSxNQUFNLEdBQUcsd0JBQXdCLENBQUM7UUFDdEMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU0sOENBQXNCLEdBQTdCLFVBQThCLE1BQU07UUFFbEMsSUFBSSxNQUFNLEdBQUcsMEJBQTBCLENBQUM7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU0sOENBQXNCLEdBQTdCLFVBQThCLE1BQU07UUFFbEMsSUFBSSxNQUFNLEdBQUcsMEJBQTBCLENBQUM7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU8sdUNBQWUsR0FBdEIsVUFBdUIsTUFBTTtRQUU1QixJQUFJLE1BQU0sR0FBRyxtQkFBbUIsQ0FBQztRQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFLTyxtQ0FBVyxHQUFuQixVQUFvQixHQUFHO1FBQ3JCLE1BQU07SUFDUixDQUFDO0lBckNVLGFBQWE7UUFEekIsaUJBQVUsRUFBRTt5Q0FJVyw4QkFBYTtPQUh4QixhQUFhLENBc0N6QjtJQUFELG9CQUFDO0NBdENELEFBc0NDLElBQUE7QUF0Q1ksc0NBQWEiLCJmaWxlIjoiYXBwL3NlcnZpY2VzL3Jldmlldy5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2ZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBSZXZpZXdTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIFRPRE9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRSZXN0YXVyYW50UmV2aWV3cyhwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9yZXN0YXVyYW50X3Jldmlld3MnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHVwZGF0ZVJlc3RhdXJhbnRSZXZpZXcocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfcmVzdGF1cmFudF9yZXZpZXcnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGRlbGV0ZVJlc3RhdXJhbnRSZXZpZXcocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdkZWxldGVfcmVzdGF1cmFudF9yZXZpZXcnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgIHB1YmxpYyBnZXRJdGVtc1Jldmlld3MocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfaXRlbXNfcmV2aWV3cyc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBcclxuXHJcbiBcclxuICBwcml2YXRlIGhhbmRsZUVycm9yKHJlcykge1xyXG4gICAgLy9UT0RPXHJcbiAgfVxyXG59Il19
