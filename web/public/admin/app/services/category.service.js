"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var CategoryService = (function () {
    function CategoryService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    CategoryService.prototype.getCategories = function (restId, id) {
        console.log('start get categories service ');
        var action = 'get_categories_by_rest_id';
        var params = {
            'rest_id': restId,
            'id': id,
        };
        console.log('get categories service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    CategoryService.prototype.createCategory = function (categoryObject) {
        console.log('start createCategory ');
        var action = 'update_categories_by_id';
        var params = {
            'rest_id': categoryObject.rest_id,
            'user_id': categoryObject.user_id,
            'category': {
                'id': categoryObject.category.id,
                'name': categoryObject.category.name,
                'thumb': categoryObject.category.thumb,
                'description': categoryObject.category.description,
                'is_active': categoryObject.category.is_active,
                'is_delete': categoryObject.category.is_delete
            },
        };
        console.log('get categories service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    CategoryService.prototype.categoryUploadFile = function (categoryObject) {
        console.log('start createCategory upload file');
        var action = 'upload_file';
        var params = {
            'id': categoryObject.id,
            'thumb': categoryObject.thumb,
            'typeScreen': 'category'
        };
        console.log('get categories service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    CategoryService.prototype.deleteFile = function (params) {
        var action = 'delete_file';
        console.log('get delete file service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    CategoryService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], CategoryService);
    return CategoryService;
}());
exports.CategoryService = CategoryService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9jYXRlZ29yeS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBRTNDLDBDQUF3QztBQUN4QyxtREFBaUQ7QUFHakQ7SUFFSTtRQUNHLG1DQUFtQztRQUMzQixNQUFjLEVBQ2QsVUFBeUI7UUFEekIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFbEMsT0FBTztJQUNULENBQUM7SUFFTSx1Q0FBYSxHQUFwQixVQUFxQixNQUFNLEVBQUMsRUFBRTtRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUM7UUFFNUMsSUFBSSxNQUFNLEdBQUcsMkJBQTJCLENBQUM7UUFDeEMsSUFBSSxNQUFNLEdBQUc7WUFDVixTQUFTLEVBQUUsTUFBTTtZQUNqQixJQUFJLEVBQUUsRUFBRTtTQUNWLENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNuRCxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDTSx3Q0FBYyxHQUFyQixVQUFzQixjQUFjO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUVwQyxJQUFJLE1BQU0sR0FBRyx5QkFBeUIsQ0FBQztRQUN0QyxJQUFJLE1BQU0sR0FBRztZQUNWLFNBQVMsRUFBRSxjQUFjLENBQUMsT0FBTztZQUNqQyxTQUFTLEVBQUMsY0FBYyxDQUFDLE9BQU87WUFDaEMsVUFBVSxFQUFDO2dCQUNULElBQUksRUFBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sRUFBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUk7Z0JBQ25DLE9BQU8sRUFBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUs7Z0JBQ3JDLGFBQWEsRUFBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVc7Z0JBQ2pELFdBQVcsRUFBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFNBQVM7Z0JBQzdDLFdBQVcsRUFBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFNBQVM7YUFDOUM7U0FFSCxDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbkQsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ00sNENBQWtCLEdBQXpCLFVBQTBCLGNBQWM7UUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1FBRS9DLElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQztRQUMxQixJQUFJLE1BQU0sR0FBRztZQUNSLElBQUksRUFBQyxjQUFjLENBQUMsRUFBRTtZQUN0QixPQUFPLEVBQUMsY0FBYyxDQUFDLEtBQUs7WUFDNUIsWUFBWSxFQUFDLFVBQVU7U0FDM0IsQ0FBQztRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELE1BQU0sQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUNJLG9DQUFVLEdBQWpCLFVBQWtCLE1BQU07UUFFeEIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQTFEVSxlQUFlO1FBRDNCLGlCQUFVLEVBQUU7eUNBS1UsZUFBTTtZQUNGLDhCQUFhO09BTDNCLGVBQWUsQ0EyRDNCO0lBQUQsc0JBQUM7Q0EzREQsQUEyREMsSUFBQTtBQTNEWSwwQ0FBZSIsImZpbGUiOiJhcHAvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgUmVwbGF5U3ViamVjdCB9IGZyb20gJ3J4anMvUngnO1xyXG5pbXBvcnQgeyBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENhdGVnb3J5U2VydmljZSB7ICAgIFxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgLy8gcHJpdmF0ZSBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSxcclxuICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAvLyBUT0RPXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldENhdGVnb3JpZXMocmVzdElkLGlkKXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBnZXQgY2F0ZWdvcmllcyBzZXJ2aWNlICcpO1xyXG4gICAgIFxyXG4gICAgICAgIGxldCBhY3Rpb24gPSAnZ2V0X2NhdGVnb3JpZXNfYnlfcmVzdF9pZCc7IFxyXG4gICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAncmVzdF9pZCc6IHJlc3RJZCwvL2dldCBmcm9tIGxvZ2luXHJcbiAgICAgICAgICAgICdpZCc6IGlkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgY2F0ZWdvcmllcyBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9ICAgICAgICAgXHJcbiAgICAgIHB1YmxpYyBjcmVhdGVDYXRlZ29yeShjYXRlZ29yeU9iamVjdCl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgY3JlYXRlQ2F0ZWdvcnkgJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfY2F0ZWdvcmllc19ieV9pZCc7IFxyXG4gICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAncmVzdF9pZCc6IGNhdGVnb3J5T2JqZWN0LnJlc3RfaWQsLy9nZXQgZnJvbSBsb2dpblxyXG4gICAgICAgICAgICAndXNlcl9pZCc6Y2F0ZWdvcnlPYmplY3QudXNlcl9pZCxcclxuICAgICAgICAgICAgJ2NhdGVnb3J5Jzp7XHJcbiAgICAgICAgICAgICAgJ2lkJzpjYXRlZ29yeU9iamVjdC5jYXRlZ29yeS5pZCxcclxuICAgICAgICAgICAgICAnbmFtZSc6Y2F0ZWdvcnlPYmplY3QuY2F0ZWdvcnkubmFtZSxcclxuICAgICAgICAgICAgICAndGh1bWInOmNhdGVnb3J5T2JqZWN0LmNhdGVnb3J5LnRodW1iLFxyXG4gICAgICAgICAgICAgICdkZXNjcmlwdGlvbic6Y2F0ZWdvcnlPYmplY3QuY2F0ZWdvcnkuZGVzY3JpcHRpb24sXHJcbiAgICAgICAgICAgICAgJ2lzX2FjdGl2ZSc6Y2F0ZWdvcnlPYmplY3QuY2F0ZWdvcnkuaXNfYWN0aXZlLFxyXG4gICAgICAgICAgICAgICdpc19kZWxldGUnOmNhdGVnb3J5T2JqZWN0LmNhdGVnb3J5LmlzX2RlbGV0ZVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgY2F0ZWdvcmllcyBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9ICAgICAgXHJcbiAgICAgIHB1YmxpYyBjYXRlZ29yeVVwbG9hZEZpbGUoY2F0ZWdvcnlPYmplY3Qpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IGNyZWF0ZUNhdGVnb3J5IHVwbG9hZCBmaWxlJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGxvYWRfZmlsZSc7IFxyXG4gICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICdpZCc6Y2F0ZWdvcnlPYmplY3QuaWQsXHJcbiAgICAgICAgICAgICAgJ3RodW1iJzpjYXRlZ29yeU9iamVjdC50aHVtYixcclxuICAgICAgICAgICAgICAndHlwZVNjcmVlbic6J2NhdGVnb3J5J1xyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBjYXRlZ29yaWVzIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH0gICAgXHJcbiAgICBwdWJsaWMgZGVsZXRlRmlsZShwYXJhbXMpIHtcclxuICAgIFxyXG4gICAgbGV0IGFjdGlvbiA9ICdkZWxldGVfZmlsZSc7XHJcbiAgICBjb25zb2xlLmxvZygnZ2V0IGRlbGV0ZSBmaWxlIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH0gICAgICAgIFxyXG59XHJcbiJdfQ==
