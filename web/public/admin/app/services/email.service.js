"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var EmailService = (function () {
    function EmailService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    EmailService.prototype.getEmailMarketingList = function (params) {
        var action = 'get_email_marketing_list';
        return this.serverServ.doPost(action, params);
    };
    EmailService.prototype.deleteEmailMarketingList = function (params) {
        var action = 'delete_email_marketing_list';
        return this.serverServ.doPost(action, params);
    };
    EmailService.prototype.getEmailMarketingDetail = function (params) {
        var action = 'get_email_marketing_detail';
        return this.serverServ.doPost(action, params);
    };
    EmailService.prototype.sendEmailMarketingList = function (params) {
        var action = 'send_email_marketing_list';
        return this.serverServ.doPost(action, params);
    };
    EmailService.prototype.sendEmailAcceptOrder = function (params) {
        var action = 'send_email_accept_order';
        return this.serverServ.doPost(action, params);
    };
    EmailService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], EmailService);
    return EmailService;
}());
exports.EmailService = EmailService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9lbWFpbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBQzNDLG1EQUFpRDtBQUdqRDtJQUVFLHNCQUNVLFVBQXlCO1FBQXpCLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFakMsT0FBTztJQUNULENBQUM7SUFFTSw0Q0FBcUIsR0FBNUIsVUFBNkIsTUFBTTtRQUVqQyxJQUFJLE1BQU0sR0FBRywwQkFBMEIsQ0FBQztRQUN4QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFHTSwrQ0FBd0IsR0FBL0IsVUFBZ0MsTUFBTTtRQUVwQyxJQUFJLE1BQU0sR0FBRyw2QkFBNkIsQ0FBQztRQUMzQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSw4Q0FBdUIsR0FBOUIsVUFBK0IsTUFBTTtRQUVuQyxJQUFJLE1BQU0sR0FBRyw0QkFBNEIsQ0FBQztRQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTyw2Q0FBc0IsR0FBN0IsVUFBOEIsTUFBTTtRQUVuQyxJQUFJLE1BQU0sR0FBRywyQkFBMkIsQ0FBQztRQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSwyQ0FBb0IsR0FBM0IsVUFBNEIsTUFBTTtRQUNoQyxJQUFJLE1BQU0sR0FBRyx5QkFBeUIsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFwQ1UsWUFBWTtRQUR4QixpQkFBVSxFQUFFO3lDQUlXLDhCQUFhO09BSHhCLFlBQVksQ0F5Q3hCO0lBQUQsbUJBQUM7Q0F6Q0QsQUF5Q0MsSUFBQTtBQXpDWSxvQ0FBWSIsImZpbGUiOiJhcHAvc2VydmljZXMvZW1haWwuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJy4vc2VydmVyLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRW1haWxTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIFRPRE9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRFbWFpbE1hcmtldGluZ0xpc3QocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfZW1haWxfbWFya2V0aW5nX2xpc3QnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcblxyXG4gIHB1YmxpYyBkZWxldGVFbWFpbE1hcmtldGluZ0xpc3QocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdkZWxldGVfZW1haWxfbWFya2V0aW5nX2xpc3QnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldEVtYWlsTWFya2V0aW5nRGV0YWlsKHBhcmFtcykge1xyXG5cclxuICAgIGxldCBhY3Rpb24gPSAnZ2V0X2VtYWlsX21hcmtldGluZ19kZXRhaWwnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgIHB1YmxpYyBzZW5kRW1haWxNYXJrZXRpbmdMaXN0KHBhcmFtcykge1xyXG5cclxuICAgIGxldCBhY3Rpb24gPSAnc2VuZF9lbWFpbF9tYXJrZXRpbmdfbGlzdCc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2VuZEVtYWlsQWNjZXB0T3JkZXIocGFyYW1zKXtcclxuICAgIGxldCBhY3Rpb24gPSAnc2VuZF9lbWFpbF9hY2NlcHRfb3JkZXInO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiBcclxuXHJcblxyXG59XHJcbiJdfQ==
