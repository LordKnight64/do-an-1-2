"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var OrderService = (function () {
    function OrderService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    OrderService.prototype.getOrderList = function (params) {
        var pr = {
            'user_id': params.user_id,
            'rest_id': params.rest_id,
            'order_sts': params.order_sts != null ? params.order_sts : null,
            'delivery_ts_to': params.delivery_ts_to != null ? params.delivery_ts_to : null,
            'delivery_ts_from': params.delivery_ts_from != null ? params.delivery_ts_from : null,
            'user_id_search': params.user_id_search != null ? params.user_id_search : null,
            'id': params.id != null ? params.id : null,
        };
        var action = 'get_orders_by_rest_id';
        return this.serverServ.doPost(action, pr);
    };
    OrderService.prototype.updateOrder = function (params) {
        var action = 'update_order_by_id';
        return this.serverServ.doPost(action, params);
    };
    OrderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], OrderService);
    return OrderService;
}());
exports.OrderService = OrderService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9vcmRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBQzNDLG1EQUFpRDtBQUdqRDtJQUVFLHNCQUNVLFVBQXlCO1FBQXpCLGVBQVUsR0FBVixVQUFVLENBQWU7UUFFakMsT0FBTztJQUNULENBQUM7SUFFTSxtQ0FBWSxHQUFuQixVQUFvQixNQUFNO1FBRXhCLElBQUksRUFBRSxHQUFHO1lBQ1AsU0FBUyxFQUFFLE1BQU0sQ0FBQyxPQUFPO1lBQ3pCLFNBQVMsRUFBRSxNQUFNLENBQUMsT0FBTztZQUN6QixXQUFXLEVBQUUsTUFBTSxDQUFDLFNBQVMsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJO1lBQy9ELGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxjQUFjLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLEdBQUcsSUFBSTtZQUM5RSxrQkFBa0IsRUFBRSxNQUFNLENBQUMsZ0JBQWdCLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJO1lBQ3BGLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxjQUFjLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLEdBQUcsSUFBSTtZQUM5RSxJQUFJLEVBQUUsTUFBTSxDQUFDLEVBQUUsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxJQUFJO1NBQzNDLENBQUM7UUFDRixJQUFJLE1BQU0sR0FBRyx1QkFBdUIsQ0FBQztRQUNyQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFTSxrQ0FBVyxHQUFsQixVQUFtQixNQUFNO1FBRXZCLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO1FBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQTNCVSxZQUFZO1FBRHhCLGlCQUFVLEVBQUU7eUNBSVcsOEJBQWE7T0FIeEIsWUFBWSxDQTRCeEI7SUFBRCxtQkFBQztDQTVCRCxBQTRCQyxJQUFBO0FBNUJZLG9DQUFZIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9vcmRlci5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2ZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBPcmRlclNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgc2VydmVyU2VydjogU2VydmVyU2VydmljZVxyXG4gICkge1xyXG4gICAgLy8gVE9ET1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldE9yZGVyTGlzdChwYXJhbXMpIHtcclxuXHJcbiAgICB2YXIgcHIgPSB7XHJcbiAgICAgICd1c2VyX2lkJzogcGFyYW1zLnVzZXJfaWQsXHJcbiAgICAgICdyZXN0X2lkJzogcGFyYW1zLnJlc3RfaWQsXHJcbiAgICAgICdvcmRlcl9zdHMnOiBwYXJhbXMub3JkZXJfc3RzICE9IG51bGwgPyBwYXJhbXMub3JkZXJfc3RzIDogbnVsbCxcclxuICAgICAgJ2RlbGl2ZXJ5X3RzX3RvJzogcGFyYW1zLmRlbGl2ZXJ5X3RzX3RvICE9IG51bGwgPyBwYXJhbXMuZGVsaXZlcnlfdHNfdG8gOiBudWxsLFxyXG4gICAgICAnZGVsaXZlcnlfdHNfZnJvbSc6IHBhcmFtcy5kZWxpdmVyeV90c19mcm9tICE9IG51bGwgPyBwYXJhbXMuZGVsaXZlcnlfdHNfZnJvbSA6IG51bGwsXHJcbiAgICAgICd1c2VyX2lkX3NlYXJjaCc6IHBhcmFtcy51c2VyX2lkX3NlYXJjaCAhPSBudWxsID8gcGFyYW1zLnVzZXJfaWRfc2VhcmNoIDogbnVsbCxcclxuICAgICAgJ2lkJzogcGFyYW1zLmlkICE9IG51bGwgPyBwYXJhbXMuaWQgOiBudWxsLFxyXG4gICAgfTtcclxuICAgIGxldCBhY3Rpb24gPSAnZ2V0X29yZGVyc19ieV9yZXN0X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcHIpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHVwZGF0ZU9yZGVyKHBhcmFtcykge1xyXG5cclxuICAgIGxldCBhY3Rpb24gPSAndXBkYXRlX29yZGVyX2J5X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcbn0iXX0=
