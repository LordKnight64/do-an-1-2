"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var BreadcrumbService = (function () {
    function BreadcrumbService() {
        this.initialData = {
            description: '',
            display: false,
            header: '',
            levels: [
                {
                    icon: 'clock-o',
                    link: ['/'],
                    title: 'Default'
                }
            ]
        };
        this.current = new rxjs_1.ReplaySubject(1);
        this.clear();
    }
    BreadcrumbService.prototype.set = function (data) {
        this.current.next(data);
    };
    BreadcrumbService.prototype.clear = function () {
        this.set(this.initialData);
    };
    BreadcrumbService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], BreadcrumbService);
    return BreadcrumbService;
}());
exports.BreadcrumbService = BreadcrumbService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsNkJBQXFDO0FBR3JDO0lBZ0JFO1FBZFEsZ0JBQVcsR0FBUTtZQUN6QixXQUFXLEVBQUUsRUFBRTtZQUNmLE9BQU8sRUFBRSxLQUFLO1lBQ2QsTUFBTSxFQUFHLEVBQUU7WUFFWCxNQUFNLEVBQUU7Z0JBQ047b0JBQ0UsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDO29CQUNYLEtBQUssRUFBRSxTQUFTO2lCQUNqQjthQUNGO1NBQ0YsQ0FBQztRQUdBLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxvQkFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFTSwrQkFBRyxHQUFWLFVBQVcsSUFBUztRQUNsQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU0saUNBQUssR0FBWjtRQUNFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUEzQlUsaUJBQWlCO1FBRDdCLGlCQUFVLEVBQUU7O09BQ0EsaUJBQWlCLENBNkI3QjtJQUFELHdCQUFDO0NBN0JELEFBNkJDLElBQUE7QUE3QlksOENBQWlCIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9icmVhZGNydW1iLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlcGxheVN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEJyZWFkY3J1bWJTZXJ2aWNlIHtcclxuICBwdWJsaWMgY3VycmVudDogUmVwbGF5U3ViamVjdDxhbnk+O1xyXG4gIHByaXZhdGUgaW5pdGlhbERhdGE6IGFueSA9IHtcclxuICAgIGRlc2NyaXB0aW9uOiAnJyxcclxuICAgIGRpc3BsYXk6IGZhbHNlLFxyXG4gICAgaGVhZGVyIDogJycsXHJcblxyXG4gICAgbGV2ZWxzOiBbXHJcbiAgICAgIHtcclxuICAgICAgICBpY29uOiAnY2xvY2stbycsXHJcbiAgICAgICAgbGluazogWycvJ10sXHJcbiAgICAgICAgdGl0bGU6ICdEZWZhdWx0J1xyXG4gICAgICB9XHJcbiAgICBdXHJcbiAgfTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLmN1cnJlbnQgPSBuZXcgUmVwbGF5U3ViamVjdCgxKTtcclxuICAgIHRoaXMuY2xlYXIoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXQoZGF0YTogYW55KSB7XHJcbiAgICB0aGlzLmN1cnJlbnQubmV4dChkYXRhKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbGVhcigpIHtcclxuICAgIHRoaXMuc2V0KHRoaXMuaW5pdGlhbERhdGEpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19
