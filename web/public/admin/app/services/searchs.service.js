"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var SearchsService = (function () {
    function SearchsService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    SearchsService.prototype.getSearchList = function (params) {
        var action = 'get_search_items';
        return this.serverServ.doPost(action, params);
    };
    SearchsService.prototype.deleteSearch = function (params) {
        var action = 'delete_search_items';
        return this.serverServ.doPost(action, params);
    };
    SearchsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], SearchsService);
    return SearchsService;
}());
exports.SearchsService = SearchsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9zZWFyY2hzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsbURBQWlEO0FBR2pEO0lBRUUsd0JBQ1UsVUFBeUI7UUFBekIsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVqQyxPQUFPO0lBQ1QsQ0FBQztJQUVNLHNDQUFhLEdBQXBCLFVBQXFCLE1BQU07UUFFekIsSUFBSSxNQUFNLEdBQUcsa0JBQWtCLENBQUM7UUFDaEMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBR00scUNBQVksR0FBbkIsVUFBb0IsTUFBTTtRQUV4QixJQUFJLE1BQU0sR0FBRyxxQkFBcUIsQ0FBQztRQUNuQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFuQlUsY0FBYztRQUQxQixpQkFBVSxFQUFFO3lDQUlXLDhCQUFhO09BSHhCLGNBQWMsQ0F3QjFCO0lBQUQscUJBQUM7Q0F4QkQsQUF3QkMsSUFBQTtBQXhCWSx3Q0FBYyIsImZpbGUiOiJhcHAvc2VydmljZXMvc2VhcmNocy5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2ZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hzU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBzZXJ2ZXJTZXJ2OiBTZXJ2ZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICAvLyBUT0RPXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0U2VhcmNoTGlzdChwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9zZWFyY2hfaXRlbXMnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcblxyXG4gIHB1YmxpYyBkZWxldGVTZWFyY2gocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdkZWxldGVfc2VhcmNoX2l0ZW1zJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gXHJcblxyXG5cclxufVxyXG4iXX0=
