"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var ComCodeService = (function () {
    function ComCodeService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    ComCodeService.prototype.getCode = function (params) {
        var action = 'get_code_by_cd_group';
        return this.serverServ.doPost(action, params);
    };
    ComCodeService.prototype.handleError = function (res) {
        //TODO
    };
    ComCodeService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], ComCodeService);
    return ComCodeService;
}());
exports.ComCodeService = ComCodeService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9jb21Db2RlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsbURBQWlEO0FBR2pEO0lBRUUsd0JBQ1UsVUFBeUI7UUFBekIsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVqQyxPQUFPO0lBQ1QsQ0FBQztJQUVNLGdDQUFPLEdBQWQsVUFBZSxNQUFNO1FBRW5CLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVPLG9DQUFXLEdBQW5CLFVBQW9CLEdBQUc7UUFDckIsTUFBTTtJQUNSLENBQUM7SUFoQlUsY0FBYztRQUQxQixpQkFBVSxFQUFFO3lDQUlXLDhCQUFhO09BSHhCLGNBQWMsQ0FpQjFCO0lBQUQscUJBQUM7Q0FqQkQsQUFpQkMsSUFBQTtBQWpCWSx3Q0FBYyIsImZpbGUiOiJhcHAvc2VydmljZXMvY29tQ29kZS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2ZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDb21Db2RlU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBzZXJ2ZXJTZXJ2OiBTZXJ2ZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICAvLyBUT0RPXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0Q29kZShwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9jb2RlX2J5X2NkX2dyb3VwJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlRXJyb3IocmVzKSB7XHJcbiAgICAvL1RPRE9cclxuICB9XHJcbn0iXX0=
