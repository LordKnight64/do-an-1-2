"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var environment_1 = require("environments/environment");
var LoggerService = (function () {
    function LoggerService() {
        // TODO
    }
    LoggerService.prototype.log = function (component, msg, i18nRef, data) {
        if (environment_1.ENVIRONMENT.SILENT === false) {
            if (i18nRef) {
                var params = {};
                if (data) {
                    params = (data[0]) ? { 0: data[0] } : params;
                    params = (data[1]) ? { 0: data[0], 1: data[1] } : params;
                    params = (data[2]) ? { 0: data[0], 1: data[1], 2: data[2] } : params;
                }
                // this.translate.getTranslate().get( i18nRef, params ).subscribe(( res: string ) => {
                //     console.log( component + ': ' + res );
                // });
            }
            else {
                console.log(component + ': ' + msg);
            }
        }
    };
    LoggerService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], LoggerService);
    return LoggerService;
}());
exports.LoggerService = LoggerService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9sb2dnZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUdsRCx3REFBdUQ7QUFHdkQ7SUFFSTtRQUdJLE9BQU87SUFDWCxDQUFDO0lBRU0sMkJBQUcsR0FBVixVQUFZLFNBQWlCLEVBQUUsR0FBWSxFQUFFLE9BQWdCLEVBQUUsSUFBZTtRQUMxRSxFQUFFLENBQUMsQ0FBRSx5QkFBVyxDQUFDLE1BQU0sS0FBSyxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFFLE9BQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ1osSUFBSSxNQUFNLEdBQU8sRUFBRSxDQUFDO2dCQUNwQixFQUFFLENBQUMsQ0FBRSxJQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNULE1BQU0sR0FBRyxDQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztvQkFDL0MsTUFBTSxHQUFHLENBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7b0JBQzNELE1BQU0sR0FBRyxDQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQzNFLENBQUM7Z0JBQ0Qsc0ZBQXNGO2dCQUN0Riw2Q0FBNkM7Z0JBQzdDLE1BQU07WUFDVixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBRSxTQUFTLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBRSxDQUFDO1lBQzFDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQXhCUSxhQUFhO1FBRHpCLGlCQUFVLEVBQUU7O09BQ0EsYUFBYSxDQXlCekI7SUFBRCxvQkFBQztDQXpCRCxBQXlCQyxJQUFBO0FBekJZLHNDQUFhIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9sb2dnZXIuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi91c2VyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBZG1pbkxURVRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICcuL3RyYW5zbGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRU5WSVJPTk1FTlQgfSBmcm9tICdlbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTG9nZ2VyU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIFxyXG4gICAgICAgIC8vIHByaXZhdGUgdHJhbnNsYXRlOiBBZG1pbkxURVRyYW5zbGF0ZVNlcnZpY2UgXHJcbiAgICApIHtcclxuICAgICAgICAvLyBUT0RPXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGxvZyggY29tcG9uZW50OiBzdHJpbmcsIG1zZz86IHN0cmluZywgaTE4blJlZj86IHN0cmluZywgZGF0YT86IHN0cmluZ1tdICkge1xyXG4gICAgICAgIGlmICggRU5WSVJPTk1FTlQuU0lMRU5UID09PSBmYWxzZSApIHtcclxuICAgICAgICAgICAgaWYgKCBpMThuUmVmICkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHBhcmFtczoge30gPSB7fTtcclxuICAgICAgICAgICAgICAgIGlmICggZGF0YSApIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMgPSAoIGRhdGFbMF0gKSA/IHsgMDogZGF0YVswXSB9IDogcGFyYW1zO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcyA9ICggZGF0YVsxXSApID8geyAwOiBkYXRhWzBdLCAxOiBkYXRhWzFdIH0gOiBwYXJhbXM7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gKCBkYXRhWzJdICkgPyB7IDA6IGRhdGFbMF0sIDE6IGRhdGFbMV0sIDI6IGRhdGFbMl0gfSA6IHBhcmFtcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIHRoaXMudHJhbnNsYXRlLmdldFRyYW5zbGF0ZSgpLmdldCggaTE4blJlZiwgcGFyYW1zICkuc3Vic2NyaWJlKCggcmVzOiBzdHJpbmcgKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgY29uc29sZS5sb2coIGNvbXBvbmVudCArICc6ICcgKyByZXMgKTtcclxuICAgICAgICAgICAgICAgIC8vIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coIGNvbXBvbmVudCArICc6ICcgKyBtc2cgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=
