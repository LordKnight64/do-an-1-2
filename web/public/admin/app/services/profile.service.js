"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var server_service_1 = require("./server.service");
var ProfileService = (function () {
    function ProfileService(
        // private stateServ: StateService,
        router, serverServ) {
        this.router = router;
        this.serverServ = serverServ;
        // TODO
    }
    ProfileService.prototype.getProfileRetaurant = function (restId, userId) {
        console.log('start OptionFoodItems service ');
        var action = 'get_restaurant_detail_by_id';
        var params = {
            'rest_id': restId,
            'user_id': userId
        };
        console.log('ProfileService', action, params);
        return this.serverServ.doPost(action, params);
    };
    ProfileService.prototype.getOrderSeetingProfile = function (restId) {
        console.log('start OptionFoodItems service ');
        var action = 'get_order_setting_by_rest_id';
        var params = {
            'rest_id': restId
        };
        console.log('ProfileService', action, params);
        return this.serverServ.doPost(action, params);
    };
    ProfileService.prototype.createProfile = function (params) {
        console.log('start createOptionFoodItems ');
        //  let option_items = itemObject.item.option_items;
        var action = 'update_profile';
        //  let params = {
        //     'rest_id': itemObject.rest_id,//get from login
        //     'user_id':itemObject.user_id,
        //     'item':{
        //       'category_id':itemObject.item.category_id,
        //       'id':itemObject.item.id,
        //       'name':itemObject.item.name,
        //       'price':itemObject.item.price,
        //       'thumb':itemObject.item.thumb,
        //       'sku':itemObject.item.sku,
        //       'is_active':itemObject.item.is_active,
        //       'is_alcoho':itemObject.item.is_alcoho,
        //       'food_type':itemObject.item.food_type,
        //       'description':itemObject.item.description,
        //       'is_delete':itemObject.item.is_delete,
        //       'update_in_lst_flg':itemObject.item.update_in_lst_flg,
        //       'option_items':option_items,
        //     }
        //  };
        console.log('get createProfile service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    ProfileService.prototype.profileUploadFile = function (itemObject) {
        console.log('start item upload file');
        var action = 'upload_file';
        var params = {
            'id': itemObject.id,
            'thumb': itemObject.thumb,
            'typeScreen': 'profile'
        };
        console.log('get item service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    ProfileService.prototype.deleteFile = function (params) {
        var action = 'delete_file';
        console.log('get delete file service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    ProfileService.prototype.getAddress = function (object) {
        console.log('start getAddress');
        var action = 'get_addrress_by_latlon';
        var params = {
            'lat': object.lat,
            'lon': object.lon
        };
        console.log('getAddress ', action, params);
        return this.serverServ.doPost(action, params);
    };
    ProfileService.prototype.getLatlon = function (object) {
        console.log('start getLatlon');
        var action = 'get_data_coordinates';
        var params = {
            'address': object.address
        };
        return this.serverServ.doPost(action, params);
    };
    ProfileService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            server_service_1.ServerService])
    ], ProfileService);
    return ProfileService;
}());
exports.ProfileService = ProfileService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9wcm9maWxlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFFM0MsMENBQXdDO0FBQ3hDLG1EQUFpRDtBQUdqRDtJQUVJO1FBQ0csbUNBQW1DO1FBQzNCLE1BQWMsRUFDZCxVQUF5QjtRQUR6QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVsQyxPQUFPO0lBQ1QsQ0FBQztJQUNNLDRDQUFtQixHQUExQixVQUEyQixNQUFNLEVBQUMsTUFBTTtRQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFFN0MsSUFBSSxNQUFNLEdBQUcsNkJBQTZCLENBQUM7UUFDMUMsSUFBSSxNQUFNLEdBQUc7WUFDVixTQUFTLEVBQUUsTUFBTTtZQUNqQixTQUFTLEVBQUUsTUFBTTtTQUNuQixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDMUMsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ00sK0NBQXNCLEdBQTdCLFVBQThCLE1BQU07UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBRTdDLElBQUksTUFBTSxHQUFHLDhCQUE4QixDQUFDO1FBQzNDLElBQUksTUFBTSxHQUFHO1lBQ1YsU0FBUyxFQUFFLE1BQU07U0FDbkIsQ0FBQztRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzFDLE1BQU0sQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUNNLHNDQUFhLEdBQXBCLFVBQXFCLE1BQU07UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1FBQzdDLG9EQUFvRDtRQUNsRCxJQUFJLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQztRQUM5QixrQkFBa0I7UUFDbEIscURBQXFEO1FBQ3JELG9DQUFvQztRQUNwQyxlQUFlO1FBQ2YsbURBQW1EO1FBQ25ELGlDQUFpQztRQUNqQyxxQ0FBcUM7UUFDckMsdUNBQXVDO1FBQ3ZDLHVDQUF1QztRQUN2QyxtQ0FBbUM7UUFDbkMsK0NBQStDO1FBQy9DLCtDQUErQztRQUMvQywrQ0FBK0M7UUFDL0MsbURBQW1EO1FBQ25ELCtDQUErQztRQUMvQywrREFBK0Q7UUFDL0QscUNBQXFDO1FBQ3JDLFFBQVE7UUFDUixNQUFNO1FBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDdEQsTUFBTSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ00sMENBQWlCLEdBQXhCLFVBQXlCLFVBQVU7UUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBRXJDLElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQztRQUMxQixJQUFJLE1BQU0sR0FBRztZQUNSLElBQUksRUFBQyxVQUFVLENBQUMsRUFBRTtZQUNsQixPQUFPLEVBQUMsVUFBVSxDQUFDLEtBQUs7WUFDeEIsWUFBWSxFQUFDLFNBQVM7U0FDMUIsQ0FBQztRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzdDLE1BQU0sQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUNJLG1DQUFVLEdBQWpCLFVBQWtCLE1BQU07UUFFeEIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUdNLG1DQUFVLEdBQWpCLFVBQWtCLE1BQU07UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBRS9CLElBQUksTUFBTSxHQUFHLHdCQUF3QixDQUFDO1FBQ3JDLElBQUksTUFBTSxHQUFHO1lBQ1IsS0FBSyxFQUFDLE1BQU0sQ0FBQyxHQUFHO1lBQ2hCLEtBQUssRUFBQyxNQUFNLENBQUMsR0FBRztTQUNwQixDQUFDO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZDLE1BQU0sQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVFLGtDQUFTLEdBQWhCLFVBQWlCLE1BQU07UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBRS9CLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ2xDLElBQUksTUFBTSxHQUFHO1lBQ1QsU0FBUyxFQUFDLE1BQU0sQ0FBQyxPQUFPO1NBQzNCLENBQUM7UUFDRixNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFoR1UsY0FBYztRQUQxQixpQkFBVSxFQUFFO3lDQUtVLGVBQU07WUFDRiw4QkFBYTtPQUwzQixjQUFjLENBaUcxQjtJQUFELHFCQUFDO0NBakdELEFBaUdDLElBQUE7QUFqR1ksd0NBQWMiLCJmaWxlIjoiYXBwL3NlcnZpY2VzL3Byb2ZpbGUuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgUmVwbGF5U3ViamVjdCB9IGZyb20gJ3J4anMvUngnO1xyXG5pbXBvcnQgeyBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFByb2ZpbGVTZXJ2aWNlIHsgICAgXHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAvLyBwcml2YXRlIHN0YXRlU2VydjogU3RhdGVTZXJ2aWNlLFxyXG4gICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgIHByaXZhdGUgc2VydmVyU2VydjogU2VydmVyU2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgIC8vIFRPRE9cclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRQcm9maWxlUmV0YXVyYW50KHJlc3RJZCx1c2VySWQpe1xyXG4gICAgICAgY29uc29sZS5sb2coJ3N0YXJ0IE9wdGlvbkZvb2RJdGVtcyBzZXJ2aWNlICcpO1xyXG4gICAgIFxyXG4gICAgICAgIGxldCBhY3Rpb24gPSAnZ2V0X3Jlc3RhdXJhbnRfZGV0YWlsX2J5X2lkJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICdyZXN0X2lkJzogcmVzdElkLC8vZ2V0IGZyb20gbG9naW5cclxuICAgICAgICAgICAgJ3VzZXJfaWQnOiB1c2VySWRcclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdQcm9maWxlU2VydmljZScsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9ICBcclxuICAgICAgcHVibGljIGdldE9yZGVyU2VldGluZ1Byb2ZpbGUocmVzdElkKXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBPcHRpb25Gb29kSXRlbXMgc2VydmljZSAnKTtcclxuICAgICBcclxuICAgICAgICBsZXQgYWN0aW9uID0gJ2dldF9vcmRlcl9zZXR0aW5nX2J5X3Jlc3RfaWQnOyBcclxuICAgICAgICAgbGV0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgJ3Jlc3RfaWQnOiByZXN0SWRcclxuICAgICAgICAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdQcm9maWxlU2VydmljZScsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9ICAgICAgICAgXHJcbiAgICAgIHB1YmxpYyBjcmVhdGVQcm9maWxlKHBhcmFtcyl7XHJcbiAgICAgICBjb25zb2xlLmxvZygnc3RhcnQgY3JlYXRlT3B0aW9uRm9vZEl0ZW1zICcpO1xyXG4gICAgICAvLyAgbGV0IG9wdGlvbl9pdGVtcyA9IGl0ZW1PYmplY3QuaXRlbS5vcHRpb25faXRlbXM7XHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfcHJvZmlsZSc7IFxyXG4gICAgICAgIC8vICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgIC8vICAgICAncmVzdF9pZCc6IGl0ZW1PYmplY3QucmVzdF9pZCwvL2dldCBmcm9tIGxvZ2luXHJcbiAgICAgICAgLy8gICAgICd1c2VyX2lkJzppdGVtT2JqZWN0LnVzZXJfaWQsXHJcbiAgICAgICAgLy8gICAgICdpdGVtJzp7XHJcbiAgICAgICAgLy8gICAgICAgJ2NhdGVnb3J5X2lkJzppdGVtT2JqZWN0Lml0ZW0uY2F0ZWdvcnlfaWQsXHJcbiAgICAgICAgLy8gICAgICAgJ2lkJzppdGVtT2JqZWN0Lml0ZW0uaWQsXHJcbiAgICAgICAgLy8gICAgICAgJ25hbWUnOml0ZW1PYmplY3QuaXRlbS5uYW1lLFxyXG4gICAgICAgIC8vICAgICAgICdwcmljZSc6aXRlbU9iamVjdC5pdGVtLnByaWNlLFxyXG4gICAgICAgIC8vICAgICAgICd0aHVtYic6aXRlbU9iamVjdC5pdGVtLnRodW1iLFxyXG4gICAgICAgIC8vICAgICAgICdza3UnOml0ZW1PYmplY3QuaXRlbS5za3UsXHJcbiAgICAgICAgLy8gICAgICAgJ2lzX2FjdGl2ZSc6aXRlbU9iamVjdC5pdGVtLmlzX2FjdGl2ZSxcclxuICAgICAgICAvLyAgICAgICAnaXNfYWxjb2hvJzppdGVtT2JqZWN0Lml0ZW0uaXNfYWxjb2hvLFxyXG4gICAgICAgIC8vICAgICAgICdmb29kX3R5cGUnOml0ZW1PYmplY3QuaXRlbS5mb29kX3R5cGUsXHJcbiAgICAgICAgLy8gICAgICAgJ2Rlc2NyaXB0aW9uJzppdGVtT2JqZWN0Lml0ZW0uZGVzY3JpcHRpb24sXHJcbiAgICAgICAgLy8gICAgICAgJ2lzX2RlbGV0ZSc6aXRlbU9iamVjdC5pdGVtLmlzX2RlbGV0ZSxcclxuICAgICAgICAvLyAgICAgICAndXBkYXRlX2luX2xzdF9mbGcnOml0ZW1PYmplY3QuaXRlbS51cGRhdGVfaW5fbHN0X2ZsZyxcclxuICAgICAgICAvLyAgICAgICAnb3B0aW9uX2l0ZW1zJzpvcHRpb25faXRlbXMsXHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgfTtcclxuICAgICAgIGNvbnNvbGUubG9nKCdnZXQgY3JlYXRlUHJvZmlsZSBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgICAgICByZXR1cm4gIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICB9ICAgICAgXHJcbiAgICAgIHB1YmxpYyBwcm9maWxlVXBsb2FkRmlsZShpdGVtT2JqZWN0KXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBpdGVtIHVwbG9hZCBmaWxlJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICd1cGxvYWRfZmlsZSc7IFxyXG4gICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICdpZCc6aXRlbU9iamVjdC5pZCxcclxuICAgICAgICAgICAgICAndGh1bWInOml0ZW1PYmplY3QudGh1bWIsXHJcbiAgICAgICAgICAgICAgJ3R5cGVTY3JlZW4nOidwcm9maWxlJ1xyXG4gICAgICAgICB9O1xyXG4gICAgICAgY29uc29sZS5sb2coJ2dldCBpdGVtIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgICAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgIH0gXHJcbiAgICBwdWJsaWMgZGVsZXRlRmlsZShwYXJhbXMpIHtcclxuICAgIFxyXG4gICAgbGV0IGFjdGlvbiA9ICdkZWxldGVfZmlsZSc7XHJcbiAgICBjb25zb2xlLmxvZygnZ2V0IGRlbGV0ZSBmaWxlIHNlcnZpY2UgJywgYWN0aW9uLCBwYXJhbXMpO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH0gICAgICBcclxuXHJcbiAgXHJcbiAgcHVibGljIGdldEFkZHJlc3Mob2JqZWN0KXtcclxuICAgICAgIGNvbnNvbGUubG9nKCdzdGFydCBnZXRBZGRyZXNzJyk7XHJcbiAgICAgXHJcbiAgICAgICAgbGV0IGFjdGlvbiA9ICdnZXRfYWRkcnJlc3NfYnlfbGF0bG9uJzsgXHJcbiAgICAgICAgIGxldCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgJ2xhdCc6b2JqZWN0LmxhdCxcclxuICAgICAgICAgICAgICAnbG9uJzpvYmplY3QubG9uXHJcbiAgICAgICAgIH07XHJcbiAgICAgICBjb25zb2xlLmxvZygnZ2V0QWRkcmVzcyAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICAgICAgICAgcmV0dXJuICB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICAgICAgfSBcclxuICAgICAgXHJcbiAgcHVibGljIGdldExhdGxvbihvYmplY3Qpe1xyXG4gICAgY29uc29sZS5sb2coJ3N0YXJ0IGdldExhdGxvbicpO1xyXG4gIFxyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfZGF0YV9jb29yZGluYXRlcyc7IFxyXG4gICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgJ2FkZHJlc3MnOm9iamVjdC5hZGRyZXNzXHJcbiAgICAgIH07XHJcbiAgICAgIHJldHVybiAgdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfSBcclxufVxyXG4iXX0=
