"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var SubscribeService = (function () {
    function SubscribeService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    SubscribeService.prototype.getSubscribeList = function (params) {
        var action = 'get_subscribe';
        return this.serverServ.doPost(action, params);
    };
    SubscribeService.prototype.deleteSubscribe = function (params) {
        var action = 'delete_subscribe';
        return this.serverServ.doPost(action, params);
    };
    SubscribeService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], SubscribeService);
    return SubscribeService;
}());
exports.SubscribeService = SubscribeService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9zdWJzY3JpYmUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUEyQztBQUMzQyxtREFBaUQ7QUFHakQ7SUFFRSwwQkFDVSxVQUF5QjtRQUF6QixlQUFVLEdBQVYsVUFBVSxDQUFlO1FBRWpDLE9BQU87SUFDVCxDQUFDO0lBRU0sMkNBQWdCLEdBQXZCLFVBQXdCLE1BQU07UUFFNUIsSUFBSSxNQUFNLEdBQUcsZUFBZSxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUdNLDBDQUFlLEdBQXRCLFVBQXVCLE1BQU07UUFFM0IsSUFBSSxNQUFNLEdBQUcsa0JBQWtCLENBQUM7UUFDaEMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBbkJVLGdCQUFnQjtRQUQ1QixpQkFBVSxFQUFFO3lDQUlXLDhCQUFhO09BSHhCLGdCQUFnQixDQXdCNUI7SUFBRCx1QkFBQztDQXhCRCxBQXdCQyxJQUFBO0FBeEJZLDRDQUFnQiIsImZpbGUiOiJhcHAvc2VydmljZXMvc3Vic2NyaWJlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN1YnNjcmliZVNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgc2VydmVyU2VydjogU2VydmVyU2VydmljZVxyXG4gICkge1xyXG4gICAgLy8gVE9ET1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFN1YnNjcmliZUxpc3QocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfc3Vic2NyaWJlJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG5cclxuICBwdWJsaWMgZGVsZXRlU3Vic2NyaWJlKHBhcmFtcykge1xyXG5cclxuICAgIGxldCBhY3Rpb24gPSAnZGVsZXRlX3N1YnNjcmliZSc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuIFxyXG5cclxuXHJcbn1cclxuIl19
