"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var ContactService = (function () {
    function ContactService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    ContactService.prototype.getContactList = function (params) {
        var action = 'get_contacts_by_rest_id';
        return this.serverServ.doPost(action, params);
    };
    ContactService.prototype.getContact = function (params) {
        var action = 'get_contact_by_id';
        return this.serverServ.doPost(action, params);
    };
    ContactService.prototype.updateContact = function (params) {
        var action = 'update_contact_by_id';
        return this.serverServ.doPost(action, params);
    };
    ContactService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], ContactService);
    return ContactService;
}());
exports.ContactService = ContactService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9jb250YWN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsbURBQWlEO0FBR2pEO0lBRUUsd0JBQ1UsVUFBeUI7UUFBekIsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVqQyxPQUFPO0lBQ1QsQ0FBQztJQUVNLHVDQUFjLEdBQXJCLFVBQXNCLE1BQU07UUFFMUIsSUFBSSxNQUFNLEdBQUcseUJBQXlCLENBQUM7UUFDdkMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU0sbUNBQVUsR0FBakIsVUFBa0IsTUFBTTtRQUV0QixJQUFJLE1BQU0sR0FBRyxtQkFBbUIsQ0FBQztRQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSxzQ0FBYSxHQUFwQixVQUFxQixNQUFNO1FBRXpCLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQXhCVSxjQUFjO1FBRDFCLGlCQUFVLEVBQUU7eUNBSVcsOEJBQWE7T0FIeEIsY0FBYyxDQXlCMUI7SUFBRCxxQkFBQztDQXpCRCxBQXlCQyxJQUFBO0FBekJZLHdDQUFjIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9jb250YWN0LnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENvbnRhY3RTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIFRPRE9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb250YWN0TGlzdChwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9jb250YWN0c19ieV9yZXN0X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb250YWN0KHBhcmFtcykge1xyXG5cclxuICAgIGxldCBhY3Rpb24gPSAnZ2V0X2NvbnRhY3RfYnlfaWQnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHVwZGF0ZUNvbnRhY3QocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICd1cGRhdGVfY29udGFjdF9ieV9pZCc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
