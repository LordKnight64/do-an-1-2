"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var server_service_1 = require("./server.service");
var NewsService = (function () {
    function NewsService(serverServ) {
        this.serverServ = serverServ;
        // TODO
    }
    NewsService.prototype.getNewsList = function (params) {
        var action = 'get_news_by_rest_id';
        return this.serverServ.doPost(action, params);
    };
    NewsService.prototype.getNews = function (params) {
        var action = 'get_news_by_id';
        return this.serverServ.doPost(action, params);
    };
    NewsService.prototype.updateNews = function (params) {
        var action = 'update_news_by_id';
        return this.serverServ.doPost(action, params);
    };
    NewsService.prototype.uploadFile = function (obj) {
        console.log('start upload file');
        var action = 'upload_file';
        var params = {
            'id': obj.id,
            'thumb': obj.thumb,
            'typeScreen': 'news'
        };
        console.log('get upload service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    NewsService.prototype.deleteFile = function (params) {
        var action = 'delete_file';
        console.log('get delete file service ', action, params);
        return this.serverServ.doPost(action, params);
    };
    NewsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [server_service_1.ServerService])
    ], NewsService);
    return NewsService;
}());
exports.NewsService = NewsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9uZXdzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsbURBQWlEO0FBR2pEO0lBRUUscUJBQ1UsVUFBeUI7UUFBekIsZUFBVSxHQUFWLFVBQVUsQ0FBZTtRQUVqQyxPQUFPO0lBQ1QsQ0FBQztJQUVNLGlDQUFXLEdBQWxCLFVBQW1CLE1BQU07UUFFdkIsSUFBSSxNQUFNLEdBQUcscUJBQXFCLENBQUM7UUFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU0sNkJBQU8sR0FBZCxVQUFlLE1BQU07UUFFbkIsSUFBSSxNQUFNLEdBQUcsZ0JBQWdCLENBQUM7UUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU0sZ0NBQVUsR0FBakIsVUFBa0IsTUFBTTtRQUV0QixJQUFJLE1BQU0sR0FBRyxtQkFBbUIsQ0FBQztRQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSxnQ0FBVSxHQUFqQixVQUFrQixHQUFHO1FBRW5CLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNqQyxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUM7UUFDM0IsSUFBSSxNQUFNLEdBQUc7WUFDWCxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUU7WUFDWixPQUFPLEVBQUUsR0FBRyxDQUFDLEtBQUs7WUFDbEIsWUFBWSxFQUFFLE1BQU07U0FDckIsQ0FBQztRQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVNLGdDQUFVLEdBQWpCLFVBQWtCLE1BQU07UUFFdEIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQTVDVSxXQUFXO1FBRHZCLGlCQUFVLEVBQUU7eUNBSVcsOEJBQWE7T0FIeEIsV0FBVyxDQTZDdkI7SUFBRCxrQkFBQztDQTdDRCxBQTZDQyxJQUFBO0FBN0NZLGtDQUFXIiwiZmlsZSI6ImFwcC9zZXJ2aWNlcy9uZXdzLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlcnZlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE5ld3NTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNlcnZlclNlcnY6IFNlcnZlclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIFRPRE9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXROZXdzTGlzdChwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ2dldF9uZXdzX2J5X3Jlc3RfaWQnO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VydmVyU2Vydi5kb1Bvc3QoYWN0aW9uLCBwYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldE5ld3MocGFyYW1zKSB7XHJcblxyXG4gICAgbGV0IGFjdGlvbiA9ICdnZXRfbmV3c19ieV9pZCc7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdXBkYXRlTmV3cyhwYXJhbXMpIHtcclxuXHJcbiAgICBsZXQgYWN0aW9uID0gJ3VwZGF0ZV9uZXdzX2J5X2lkJztcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyB1cGxvYWRGaWxlKG9iaikge1xyXG5cclxuICAgIGNvbnNvbGUubG9nKCdzdGFydCB1cGxvYWQgZmlsZScpO1xyXG4gICAgbGV0IGFjdGlvbiA9ICd1cGxvYWRfZmlsZSc7XHJcbiAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAnaWQnOiBvYmouaWQsXHJcbiAgICAgICd0aHVtYic6IG9iai50aHVtYixcclxuICAgICAgJ3R5cGVTY3JlZW4nOiAnbmV3cydcclxuICAgIH07XHJcbiAgICBjb25zb2xlLmxvZygnZ2V0IHVwbG9hZCBzZXJ2aWNlICcsIGFjdGlvbiwgcGFyYW1zKTtcclxuICAgIHJldHVybiB0aGlzLnNlcnZlclNlcnYuZG9Qb3N0KGFjdGlvbiwgcGFyYW1zKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBkZWxldGVGaWxlKHBhcmFtcykge1xyXG4gICAgXHJcbiAgICBsZXQgYWN0aW9uID0gJ2RlbGV0ZV9maWxlJztcclxuICAgIGNvbnNvbGUubG9nKCdnZXQgZGVsZXRlIGZpbGUgc2VydmljZSAnLCBhY3Rpb24sIHBhcmFtcyk7XHJcbiAgICByZXR1cm4gdGhpcy5zZXJ2ZXJTZXJ2LmRvUG9zdChhY3Rpb24sIHBhcmFtcyk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
