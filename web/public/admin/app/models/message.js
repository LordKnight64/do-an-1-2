"use strict";
// import { User } from './user';
Object.defineProperty(exports, "__esModule", { value: true });
var Message = (function () {
    function Message(data) {
        if (data === void 0) { data = {}; }
        this.content = data.content || '';
        this.title = data.title || '';
        // this.author = data.author || null;
        // this.destination = data.destination || null;
        this.date = data.date || Date.now();
    }
    return Message;
}());
exports.Message = Message;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tb2RlbHMvbWVzc2FnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsaUNBQWlDOztBQUVqQztJQU9FLGlCQUFtQixJQUFjO1FBQWQscUJBQUEsRUFBQSxTQUFjO1FBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUM5QixxQ0FBcUM7UUFDckMsK0NBQStDO1FBQy9DLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUNILGNBQUM7QUFBRCxDQWRBLEFBY0MsSUFBQTtBQWRZLDBCQUFPIiwiZmlsZSI6ImFwcC9tb2RlbHMvbWVzc2FnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGltcG9ydCB7IFVzZXIgfSBmcm9tICcuL3VzZXInO1xyXG5cclxuZXhwb3J0IGNsYXNzIE1lc3NhZ2Uge1xyXG4gIHB1YmxpYyBjb250ZW50OiBzdHJpbmc7XHJcbiAgcHVibGljIHRpdGxlOiBzdHJpbmc7XHJcbiAgLy8gcHVibGljIGF1dGhvcjogVXNlcjtcclxuICAvLyBwdWJsaWMgZGVzdGluYXRpb246IFVzZXI7XHJcbiAgcHVibGljIGRhdGU6IHN0cmluZztcclxuXHJcbiAgcHVibGljIGNvbnN0cnVjdG9yKGRhdGE6IGFueSA9IHt9KSB7XHJcbiAgICB0aGlzLmNvbnRlbnQgPSBkYXRhLmNvbnRlbnQgfHwgJyc7XHJcbiAgICB0aGlzLnRpdGxlID0gZGF0YS50aXRsZSB8fCAnJztcclxuICAgIC8vIHRoaXMuYXV0aG9yID0gZGF0YS5hdXRob3IgfHwgbnVsbDtcclxuICAgIC8vIHRoaXMuZGVzdGluYXRpb24gPSBkYXRhLmRlc3RpbmF0aW9uIHx8IG51bGw7XHJcbiAgICB0aGlzLmRhdGUgPSBkYXRhLmRhdGUgfHwgRGF0ZS5ub3coKTtcclxuICB9XHJcbn1cclxuIl19
