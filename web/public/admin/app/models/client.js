"use strict";
// import { GuidHelper } from 'app/helpers/guid.helper';
Object.defineProperty(exports, "__esModule", { value: true });
var Client = (function () {
    function Client(name, clientId, adress) {
        this.name = name || '';
        // this.clientId = clientId || GuidHelper.generateGUID();
        this.address = adress || '';
    }
    return Client;
}());
exports.Client = Client;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tb2RlbHMvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx3REFBd0Q7O0FBRXhEO0lBS0UsZ0JBQVksSUFBYSxFQUFFLFFBQWlCLEVBQUUsTUFBZTtRQUMzRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDdkIseURBQXlEO1FBQ3pELElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxJQUFJLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBQ0gsYUFBQztBQUFELENBVkEsQUFVQyxJQUFBO0FBVlksd0JBQU0iLCJmaWxlIjoiYXBwL21vZGVscy9jbGllbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQgeyBHdWlkSGVscGVyIH0gZnJvbSAnYXBwL2hlbHBlcnMvZ3VpZC5oZWxwZXInO1xyXG5cclxuZXhwb3J0IGNsYXNzIENsaWVudCB7XHJcbiAgcHVibGljIG5hbWU6IHN0cmluZztcclxuICBwdWJsaWMgY2xpZW50SWQ6IHN0cmluZztcclxuICBwdWJsaWMgYWRkcmVzczogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcihuYW1lPzogc3RyaW5nLCBjbGllbnRJZD86IHN0cmluZywgYWRyZXNzPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLm5hbWUgPSBuYW1lIHx8ICcnO1xyXG4gICAgLy8gdGhpcy5jbGllbnRJZCA9IGNsaWVudElkIHx8IEd1aWRIZWxwZXIuZ2VuZXJhdGVHVUlEKCk7XHJcbiAgICB0aGlzLmFkZHJlc3MgPSBhZHJlc3MgfHwgJyc7XHJcbiAgfVxyXG59XHJcbiJdfQ==
