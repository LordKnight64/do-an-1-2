"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = (function () {
    function User(data, restaurant) {
        if (data === void 0) { data = {}; }
        if (restaurant === void 0) { restaurant = {}; }
        this.isInit = false;
        this.firstname = data.firstname || '';
        this.lastname = data.lastname || '';
        this.email = data.email || '';
        this.avatarUrl = data.avatarUrl || '';
        this.phone = data.phone || "";
        this.creationDate = data.creation_date || Date.now();
        this.preferredLang = data.preferredLang || null;
        this.roles = data.roles || [];
        this.tell = data.tell || '';
        // this.connected = data.connected || false;
        this.isInit = data.isInit || false;
        this.restaurants = restaurant;
    }
    User.prototype.getName = function () {
        return this.firstname + ' ' + this.lastname;
    };
    return User;
}());
exports.User = User;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tb2RlbHMvdXNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBY0ksY0FBb0IsSUFBYyxFQUFFLFVBQW9CO1FBQXBDLHFCQUFBLEVBQUEsU0FBYztRQUFFLDJCQUFBLEVBQUEsZUFBb0I7UUFIakQsV0FBTSxHQUFZLEtBQUssQ0FBQztRQUkzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNyRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDO1FBQ2hELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUM1Qiw0Q0FBNEM7UUFDNUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztJQUNsQyxDQUFDO0lBRU0sc0JBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ2hELENBQUM7SUFDTCxXQUFDO0FBQUQsQ0FoQ0EsQUFnQ0MsSUFBQTtBQWhDWSxvQkFBSSIsImZpbGUiOiJhcHAvbW9kZWxzL3VzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVXNlciB7XHJcbiAgICBwdWJsaWMgZmlyc3RuYW1lOiBzdHJpbmc7XHJcbiAgICBwdWJsaWMgbGFzdG5hbWU6IHN0cmluZztcclxuICAgIHB1YmxpYyBlbWFpbDogc3RyaW5nO1xyXG4gICAgcHVibGljIGF2YXRhclVybDogc3RyaW5nO1xyXG4gICAgcHVibGljIGNyZWF0aW9uRGF0ZTogc3RyaW5nO1xyXG4gICAgcHVibGljIHByZWZlcnJlZExhbmc6IHN0cmluZztcclxuICAgIHB1YmxpYyBwaG9uZTogc3RyaW5nO1xyXG4gICAgcHVibGljIHJvbGVzOmFueTtcclxuICAgIHB1YmxpYyB0ZWxsOnN0cmluZztcclxuICAgIHB1YmxpYyByZXN0YXVyYW50czogYW55O1xyXG4gICAgcHVibGljIGlzSW5pdDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoIGRhdGE6IGFueSA9IHt9LCByZXN0YXVyYW50OiBhbnkgPSB7fSkge1xyXG4gICAgICAgIHRoaXMuZmlyc3RuYW1lID0gZGF0YS5maXJzdG5hbWUgfHwgJyc7XHJcbiAgICAgICAgdGhpcy5sYXN0bmFtZSA9IGRhdGEubGFzdG5hbWUgfHwgJyc7XHJcbiAgICAgICAgdGhpcy5lbWFpbCA9IGRhdGEuZW1haWwgfHwgJyc7XHJcbiAgICAgICAgdGhpcy5hdmF0YXJVcmwgPSBkYXRhLmF2YXRhclVybCB8fCAnJztcclxuICAgICAgICB0aGlzLnBob25lID0gZGF0YS5waG9uZSB8fCBcIlwiO1xyXG4gICAgICAgIHRoaXMuY3JlYXRpb25EYXRlID0gZGF0YS5jcmVhdGlvbl9kYXRlIHx8IERhdGUubm93KCk7XHJcbiAgICAgICAgdGhpcy5wcmVmZXJyZWRMYW5nID0gZGF0YS5wcmVmZXJyZWRMYW5nIHx8IG51bGw7XHJcbiAgICAgICAgdGhpcy5yb2xlcyA9IGRhdGEucm9sZXMgfHwgW107XHJcbiAgICAgICAgdGhpcy50ZWxsID0gZGF0YS50ZWxsIHx8ICcnO1xyXG4gICAgICAgIC8vIHRoaXMuY29ubmVjdGVkID0gZGF0YS5jb25uZWN0ZWQgfHwgZmFsc2U7XHJcbiAgICAgICAgdGhpcy5pc0luaXQgPSBkYXRhLmlzSW5pdCB8fCBmYWxzZTtcclxuICAgICAgICB0aGlzLnJlc3RhdXJhbnRzID0gcmVzdGF1cmFudDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maXJzdG5hbWUgKyAnICcgKyB0aGlzLmxhc3RuYW1lO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
