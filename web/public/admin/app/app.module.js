"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
// import { FormsModule }    from '@angular/forms';
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var ngx_bootstrap_1 = require("ngx-bootstrap");
// import { ModalModule  } from 'ng2-bootstrap/modal/modal.module';
var angular2_toaster_1 = require("angular2-toaster/angular2-toaster");
var ng2_translate_1 = require("ng2-translate");
// import {trace, Category, UIRouterModule, UIView} from "ui-router-ng2";
// import { MyRootUIRouterConfig } from 'app/router.config';
// import {MAIN_STATES} from "app/app.states";
var angular2_jwt_1 = require("angular2-jwt");
// import { provideAuth, JwtHelper } from 'angular2-jwt';
var common_1 = require("@angular/common");
var environment_1 = require("environments/environment");
var ng2_select_1 = require("ng2-select");
var CryptoJS = require("crypto-js");
var ng2_charts_1 = require("ng2-charts");
var primeng_1 = require("primeng/primeng");
function createTranslateLoader(http) {
    // return new TranslateStaticLoader( http, 'admin/app/i18n/');
    return new ng2_translate_1.TranslateStaticLoader(http, 'locale');
}
exports.createTranslateLoader = createTranslateLoader;
// save a jwt token
// localStorage.setItem('token','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ');
function authHttpServiceFactory(http, options, router) {
    // let jwtHelper: JwtHelper = new JwtHelper();
    return new angular2_jwt_1.AuthHttp(new angular2_jwt_1.AuthConfig({
        tokenName: 'id_token',
        tokenGetter: function () {
            console.log('tokenGetter');
            var key = CryptoJS.enc.Base64.parse("#base64Key#");
            var iv = CryptoJS.enc.Base64.parse("#base64IV#");
            var idToken = "";
            var encryptedToken = localStorage.getItem('id_token');
            console.log('encryptedToken ', encryptedToken);
            if (encryptedToken != undefined && encryptedToken != null) {
                var decrypted = CryptoJS.AES.decrypt(encryptedToken.toString(), key, { iv: iv });
                idToken = decrypted.toString(CryptoJS.enc.Utf8);
                console.log('descrypted ', idToken);
            }
            if (idToken) {
                var jwtHelper = new angular2_jwt_1.JwtHelper();
                if (jwtHelper.isTokenExpired(idToken)) {
                    console.log('------AuthHttp get token server------');
                    var headers = new http_1.Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + idToken, 'skipAuthorization': true });
                    var options_1 = new http_1.RequestOptions({ headers: headers });
                    return http.post(environment_1.ENVIRONMENT.DOMAIN_API + ':' + environment_1.ENVIRONMENT.SERVER_PORT + '/token', options_1).toPromise().then(function (response) {
                        var data = response.json();
                        //Impementing the Key and IV and encrypt the password
                        var encryptedToken = CryptoJS.AES.encrypt(data.token, key, { iv: iv });
                        var token = encryptedToken.toString();
                        console.log('encrypted 123 ', encryptedToken.toString());
                        localStorage.setItem('id_token', token);
                        return token;
                    }, function (response) {
                        if (response) {
                            switch (response.status) {
                                case 403:
                                    // this.router.navigate(['log']);
                                    break;
                                case 401:
                                    localStorage.removeItem('id_token');
                                    this.router.navigate(['login']);
                                    break;
                                default:
                                    this.router.navigate(['error']);
                                    break;
                            }
                        }
                    });
                }
                else {
                    console.log('------AuthHttp get token localStorage------');
                    return idToken;
                }
            }
        },
        globalHeaders: [{ 'Content-Type': 'application/json' }],
        noJwtError: true
    }), http, options);
}
exports.authHttpServiceFactory = authHttpServiceFactory;
var modules = [
    platform_browser_1.BrowserModule,
    forms_1.FormsModule,
    forms_1.ReactiveFormsModule,
    http_1.HttpModule,
    ng2_charts_1.ChartsModule,
    forms_1.ReactiveFormsModule,
    router_1.RouterModule,
    angular2_toaster_1.ToasterModule,
    ngx_bootstrap_1.AlertModule.forRoot(),
    ngx_bootstrap_1.ButtonsModule.forRoot(),
    ngx_bootstrap_1.ModalModule.forRoot(),
    ngx_bootstrap_1.AccordionModule.forRoot(),
    ngx_bootstrap_1.DatepickerModule.forRoot(),
    ngx_bootstrap_1.TimepickerModule.forRoot(),
    ngx_bootstrap_1.CarouselModule.forRoot(),
    ngx_bootstrap_1.CollapseModule.forRoot(),
    ng2_translate_1.TranslateModule.forRoot({
        deps: [http_1.Http],
        provide: ng2_translate_1.TranslateLoader,
        useFactory: (createTranslateLoader)
    }),
    ngx_bootstrap_1.TabsModule.forRoot(),
    ngx_bootstrap_1.PaginationModule.forRoot(),
    // UIRouterModule.forRoot({
    //   states: MAIN_STATES,
    //   otherwise: { state: 'app', params: {} },
    //   useHash: true,
    //   configClass: MyRootUIRouterConfig
    // })
    ng2_select_1.SelectModule,
    primeng_1.CalendarModule
];
var app_component_1 = require("./app.component");
var app_header_component_1 = require("./widgets/app-header/app-header.component");
var menu_aside_component_1 = require("./widgets/menu-aside/menu-aside.component");
var breadcrumb_component_1 = require("./widgets/breadcrumb/breadcrumb.component");
var app_footer_component_1 = require("./widgets/app-footer/app-footer.component");
var control_sidebar_component_1 = require("./widgets/control-sidebar/control-sidebar.component");
var messages_box_component_1 = require("./widgets/messages-box/messages-box.component");
var notification_box_component_1 = require("./widgets/notification-box/notification-box.component");
var tasks_box_component_1 = require("./widgets/tasks-box/tasks-box.component");
var user_box_component_1 = require("./widgets/user-box/user-box.component");
var limit_to_directive_1 = require("./widgets/limit-to.directive");
var widgets = [
    app_component_1.AppComponent,
    breadcrumb_component_1.BreadcrumbComponent,
    app_header_component_1.AppHeaderComponent,
    app_footer_component_1.AppFooterComponent,
    menu_aside_component_1.MenuAsideComponent,
    control_sidebar_component_1.ControlSidebarComponent,
    messages_box_component_1.MessagesBoxComponent,
    notification_box_component_1.NotificationBoxComponent,
    tasks_box_component_1.TasksBoxComponent,
    user_box_component_1.UserBoxComponent,
    limit_to_directive_1.LimitToDirective
];
var storage_service_1 = require("./services/storage.service");
var user_service_1 = require("./services/user.service");
var messages_service_1 = require("./services/messages.service");
var guard_service_1 = require("./services/guard.service");
var notification_service_1 = require("./services/notification.service");
var breadcrumb_service_1 = require("./services/breadcrumb.service");
var translate_service_1 = require("./services/translate.service");
var logger_service_1 = require("./services/logger.service");
var hero_service_1 = require("./services/hero.service");
var server_service_1 = require("./services/server.service");
var auth_service_1 = require("./services/auth.service");
var category_service_1 = require("./services/category.service");
var area_service_1 = require("./services/area.service");
var table_service_1 = require("./services/table.service");
var news_service_1 = require("./services/news.service");
var restaurant_service_1 = require("./services/restaurant.service");
var contact_service_1 = require("./services/contact.service");
var optionFoodItem_service_1 = require("./services/optionFoodItem.service");
var optionValueFoodItem_service_1 = require("./services/optionValueFoodItem.service");
var comCode_service_1 = require("./services/comCode.service");
var order_service_1 = require("./services/order.service");
var foodItem_service_1 = require("./services/foodItem.service");
var profile_service_1 = require("./services/profile.service");
var geoLocation_service_1 = require("./services/geoLocation.service");
var subscribe_service_1 = require("./services/subscribe.service");
var searchs_service_1 = require("./services/searchs.service");
var email_service_1 = require("./services/email.service");
var review_service_1 = require("./services/review.service");
var reserve_service_1 = require("./services/reserve.service");
var turnover_service_1 = require("./services/turnover.service");
var services = [
    storage_service_1.StorageService,
    translate_service_1.AdminLTETranslateService,
    server_service_1.ServerService,
    auth_service_1.AuthService,
    user_service_1.UserService,
    breadcrumb_service_1.BreadcrumbService,
    messages_service_1.MessagesService,
    guard_service_1.CanActivateGuard,
    notification_service_1.NotificationService,
    logger_service_1.LoggerService,
    category_service_1.CategoryService,
    area_service_1.AreaService,
    table_service_1.TableService,
    hero_service_1.HeroService,
    news_service_1.NewsService,
    restaurant_service_1.RestaurantService,
    reserve_service_1.ReserveService,
    contact_service_1.ContactService,
    optionFoodItem_service_1.OptionFoodItemService,
    optionValueFoodItem_service_1.OptionValueFoodItemService,
    comCode_service_1.ComCodeService,
    order_service_1.OrderService,
    foodItem_service_1.FoodItemService,
    profile_service_1.ProfileService,
    subscribe_service_1.SubscribeService,
    searchs_service_1.SearchsService,
    email_service_1.EmailService,
    review_service_1.ReviewService,
    turnover_service_1.TurnOverService,
    geoLocation_service_1.GeoLocationService
];
var app_routing_1 = require("./app.routing");
// les pages
var home_component_1 = require("./components/home/home.component");
var client_component_1 = require("./components/client/client.component");
var auth_1 = require("./components/layouts/auth/auth");
var login_component_1 = require("./components/login/login.component");
var register_component_1 = require("./components/register/register.component");
var dashboard_component_1 = require("./components/dashboard/dashboard.component");
var system_error_component_1 = require("./components/system-error/system-error.component");
var page_not_found_component_1 = require("./components/page-not-found/page-not-found.component");
var ordering_request_component_1 = require("./components/ordering-request/ordering-request.component");
var profile_component_1 = require("./components/profile/profile.component");
var category_list_component_1 = require("./components/category-list/category-list.component");
var create_category_component_1 = require("./components/create-category/create-category.component");
var option_food_items_list_component_1 = require("./components/option-food-items-list/option-food-items-list.component");
var create_option_food_item_component_1 = require("./components/create-option-food-item/create-option-food-item.component");
var option_value_food_items_component_1 = require("./components/option-value-food-items/option-value-food-items.component");
var create_option_value_food_item_component_1 = require("./components/create-option-value-food-item/create-option-value-food-item.component");
var contents_component_1 = require("./components/contents/contents.component");
var news_list_component_1 = require("./components/news-list/news-list.component");
var news_item_component_1 = require("./components/news-item/news-item.component");
var orders_list_component_1 = require("./components/orders-list/orders-list.component");
var contact_list_component_1 = require("./components/contact-list/contact-list.component");
var contact_item_component_1 = require("./components/contact-item/contact-item.component");
var order_setting_component_1 = require("./components/order-setting/order-setting.component");
var turn_over_component_1 = require("./components/turn-over/turn-over.component");
var food_items_list_component_1 = require("./components/food-items-list/food-items-list.component");
var create_food_item_component_1 = require("./components/create-food-item/create-food-item.component");
var feedback_component_1 = require("./components/feedback/feedback.component");
var table_list_component_1 = require("./components/table-list/table-list.component");
var table_booking_component_1 = require("./components/table-booking/table-booking.component");
var subscribe_list_component_1 = require("./components/subscribe-list/subscribe-list.component");
var send_email_marketing_component_1 = require("./components/send-email-marketing/send-email-marketing.component");
var search_items_component_1 = require("./components/search-items/search-items.component");
var email_marketing_list_component_1 = require("./components/email-marketing-list/email-marketing-list.component");
var review_item_component_1 = require("./components/review-item/review-item.component");
var view_email_marketing_component_1 = require("./components/view-email-marketing/view-email-marketing.component");
var area_list_component_1 = require("./components/area-list/area-list.component");
var create_area_component_1 = require("./components/create-area/create-area.component");
var create_table_component_1 = require("./components/create-table/create-table.component");
var reserve_table_component_1 = require("./components/reserve-table/reserve-table.component");
var table_booking_schedule_component_1 = require("./components/table-booking-schedule/table-booking-schedule.component");
var order_food_component_1 = require("./components/order-food/order-food.component");
var login_modal_component_1 = require("./components/login-modal/login-modal.component");
var subscribe_list_modal_component_1 = require("./components/subscribe-list-modal/subscribe-list-modal.component");
var pages = [
    home_component_1.HomeComponent,
    // // PageNumComponent,
    create_area_component_1.CreateAreaComponent,
    client_component_1.ClientComponent,
    auth_1.LayoutsAuthComponent,
    login_component_1.LoginComponent,
    dashboard_component_1.DashboardComponent,
    register_component_1.RegisterComponent,
    page_not_found_component_1.PageNotFoundComponent,
    system_error_component_1.SystemErrorComponent,
    ordering_request_component_1.OrderingRequestComponent,
    profile_component_1.ProfileComponent,
    category_list_component_1.CategoryListComponent,
    create_category_component_1.CreateCategoryComponent,
    option_food_items_list_component_1.OptionFoodItemsListComponent,
    create_option_food_item_component_1.CreateOptionFoodItemComponent,
    option_value_food_items_component_1.OptionValueFoodItemsComponent,
    create_option_value_food_item_component_1.CreateOptionValueFoodItemComponent,
    contents_component_1.ContentsComponent,
    news_list_component_1.NewsListComponent,
    news_item_component_1.NewsItemComponent,
    orders_list_component_1.OrdersListComponent,
    contact_list_component_1.ContactListComponent,
    contact_item_component_1.ContactItemComponent,
    order_setting_component_1.OrderSettingComponent,
    turn_over_component_1.TurnOverComponent,
    food_items_list_component_1.FoodItemsListComponent,
    create_food_item_component_1.CreateFoodItemComponent,
    feedback_component_1.FeedbackComponent,
    table_list_component_1.TableListComponent,
    table_booking_component_1.TableBookingComponent,
    subscribe_list_component_1.SubscribeListComponent,
    send_email_marketing_component_1.SendEmailMarketingComponent,
    search_items_component_1.SearchItemsComponent,
    review_item_component_1.ReviewItemComponent,
    view_email_marketing_component_1.ViewEmailMarketingComponent,
    area_list_component_1.AreaListComponent,
    create_table_component_1.CreateTableComponent,
    reserve_table_component_1.ReserveTableComponent,
    login_modal_component_1.LoginModalComponent,
    order_food_component_1.OrderFoodComponent,
    table_booking_schedule_component_1.TableBookingScheduleComponent,
    email_marketing_list_component_1.EmailMarketingListComponent,
    subscribe_list_modal_component_1.SubscribeListModalComponent
];
// trace.enable(Category.TRANSITION, Category.VIEWCONFIG);
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: modules.concat([
                app_routing_1.routing
            ]),
            declarations: widgets.concat(pages),
            providers: services.concat([
                { provide: "windowObject", useValue: window },
                {
                    provide: angular2_jwt_1.AuthHttp,
                    useFactory: authHttpServiceFactory,
                    deps: [http_1.Http, http_1.RequestOptions, router_1.Router]
                },
                {
                    provide: common_1.LocationStrategy,
                    useClass: common_1.HashLocationStrategy
                }
            ]),
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsc0NBQXdGO0FBQ3hGLDhEQUEwRDtBQUMxRCxtREFBbUQ7QUFDbkQsd0NBQWtFO0FBQ2xFLHNDQUEwRTtBQUMxRSwwQ0FBdUQ7QUFDdkQsK0NBQTRMO0FBQzVMLG1FQUFtRTtBQUNuRSxzRUFBa0U7QUFDbEUsK0NBQXdGO0FBQ3hGLHlFQUF5RTtBQUN6RSw0REFBNEQ7QUFDNUQsOENBQThDO0FBQzlDLDZDQUErRDtBQUMvRCx5REFBeUQ7QUFDekQsMENBQXlFO0FBQ3pFLHdEQUF1RDtBQUN2RCx5Q0FBMEM7QUFDMUMsb0NBQXNDO0FBQ3RDLHlDQUEwQztBQUMxQywyQ0FBaUQ7QUFJakQsK0JBQXNDLElBQVU7SUFDNUMsOERBQThEO0lBQzlELE1BQU0sQ0FBQyxJQUFJLHFDQUFxQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztBQUNyRCxDQUFDO0FBSEQsc0RBR0M7QUFFRCxtQkFBbUI7QUFDbkIseUxBQXlMO0FBR3pMLGdDQUF1QyxJQUFVLEVBQUUsT0FBdUIsRUFBRSxNQUFjO0lBQ3RGLDhDQUE4QztJQUM5QyxNQUFNLENBQUMsSUFBSSx1QkFBUSxDQUFDLElBQUkseUJBQVUsQ0FBQztRQUMvQixTQUFTLEVBQUUsVUFBVTtRQUNyQixXQUFXLEVBQUU7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzNCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNuRCxJQUFJLEVBQUUsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDakQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLElBQUksY0FBYyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxjQUFjLENBQUMsQ0FBQztZQUMvQyxFQUFFLENBQUMsQ0FBQyxjQUFjLElBQUksU0FBUyxJQUFJLGNBQWMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ2pGLE9BQU8sR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3hDLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNWLElBQUksU0FBUyxHQUFjLElBQUksd0JBQVMsRUFBRSxDQUFDO2dCQUMzQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO29CQUNyRCxJQUFJLE9BQU8sR0FBRyxJQUFJLGNBQU8sQ0FBQyxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsU0FBUyxHQUFHLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO29CQUNuSSxJQUFJLFNBQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztvQkFDdkQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQVcsQ0FBQyxVQUFVLEdBQUcsR0FBRyxHQUFHLHlCQUFXLENBQUMsV0FBVyxHQUFHLFFBQVEsRUFBRSxTQUFPLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxRQUFRO3dCQUM1SCxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBRTNCLHFEQUFxRDt3QkFDckQsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQzt3QkFDdkUsSUFBSSxLQUFLLEdBQUcsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3dCQUN6RCxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFFeEMsTUFBTSxDQUFDLEtBQUssQ0FBQztvQkFDakIsQ0FBQyxFQUFFLFVBQVUsUUFBUTt3QkFDakIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDWCxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQ0FDdEIsS0FBSyxHQUFHO29DQUNKLGlDQUFpQztvQ0FDakMsS0FBSyxDQUFDO2dDQUNWLEtBQUssR0FBRztvQ0FDSixZQUFZLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29DQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0NBQ2hDLEtBQUssQ0FBQztnQ0FDVjtvQ0FDSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0NBQ2hDLEtBQUssQ0FBQzs0QkFDZCxDQUFDO3dCQUNMLENBQUM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7b0JBQzNELE1BQU0sQ0FBQyxPQUFPLENBQUM7Z0JBQ25CLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUNELGFBQWEsRUFBRSxDQUFDLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLENBQUM7UUFDdkQsVUFBVSxFQUFFLElBQUk7S0FDbkIsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztBQUN2QixDQUFDO0FBMURELHdEQTBEQztBQUVELElBQUksT0FBTyxHQUFHO0lBQ1YsZ0NBQWE7SUFDYixtQkFBVztJQUNYLDJCQUFtQjtJQUNuQixpQkFBVTtJQUNWLHlCQUFZO0lBQ1osMkJBQW1CO0lBQ25CLHFCQUFZO0lBQ1osZ0NBQWE7SUFDYiwyQkFBVyxDQUFDLE9BQU8sRUFBRTtJQUNyQiw2QkFBYSxDQUFDLE9BQU8sRUFBRTtJQUN2QiwyQkFBVyxDQUFDLE9BQU8sRUFBRTtJQUNyQiwrQkFBZSxDQUFDLE9BQU8sRUFBRTtJQUV6QixnQ0FBZ0IsQ0FBQyxPQUFPLEVBQUU7SUFDMUIsZ0NBQWdCLENBQUMsT0FBTyxFQUFFO0lBQzFCLDhCQUFjLENBQUMsT0FBTyxFQUFFO0lBQ3hCLDhCQUFjLENBQUMsT0FBTyxFQUFFO0lBQ3hCLCtCQUFlLENBQUMsT0FBTyxDQUFDO1FBQ3BCLElBQUksRUFBRSxDQUFDLFdBQUksQ0FBQztRQUNaLE9BQU8sRUFBRSwrQkFBZTtRQUN4QixVQUFVLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztLQUN0QyxDQUFDO0lBQ0YsMEJBQVUsQ0FBQyxPQUFPLEVBQUU7SUFDcEIsZ0NBQWdCLENBQUMsT0FBTyxFQUFFO0lBQzFCLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekIsNkNBQTZDO0lBQzdDLG1CQUFtQjtJQUNuQixzQ0FBc0M7SUFDdEMsS0FBSztJQUNMLHlCQUFZO0lBQ1osd0JBQWM7Q0FDakIsQ0FBQztBQUVGLGlEQUErQztBQUMvQyxrRkFBK0U7QUFDL0Usa0ZBQStFO0FBQy9FLGtGQUFnRjtBQUNoRixrRkFBK0U7QUFDL0UsaUdBQThGO0FBQzlGLHdGQUFxRjtBQUNyRixvR0FBaUc7QUFDakcsK0VBQTRFO0FBQzVFLDRFQUF5RTtBQUN6RSxtRUFBZ0U7QUFFaEUsSUFBSSxPQUFPLEdBQUc7SUFDViw0QkFBWTtJQUNaLDBDQUFtQjtJQUNuQix5Q0FBa0I7SUFDbEIseUNBQWtCO0lBQ2xCLHlDQUFrQjtJQUNsQixtREFBdUI7SUFDdkIsNkNBQW9CO0lBQ3BCLHFEQUF3QjtJQUN4Qix1Q0FBaUI7SUFDakIscUNBQWdCO0lBQ2hCLHFDQUFnQjtDQUNuQixDQUFDO0FBRUYsOERBQTREO0FBQzVELHdEQUFzRDtBQUN0RCxnRUFBOEQ7QUFDOUQsMERBQTREO0FBQzVELHdFQUFzRTtBQUN0RSxvRUFBa0U7QUFDbEUsa0VBQXdFO0FBQ3hFLDREQUEwRDtBQUMxRCx3REFBc0Q7QUFDdEQsNERBQTBEO0FBQzFELHdEQUFzRDtBQUN0RCxnRUFBOEQ7QUFDOUQsd0RBQXNEO0FBQ3RELDBEQUF3RDtBQUN4RCx3REFBc0Q7QUFDdEQsb0VBQWtFO0FBQ2xFLDhEQUE0RDtBQUM1RCw0RUFBMEU7QUFDMUUsc0ZBQW9GO0FBQ3BGLDhEQUE0RDtBQUM1RCwwREFBd0Q7QUFDeEQsZ0VBQThEO0FBQzlELDhEQUE0RDtBQUM1RCxzRUFBb0U7QUFDcEUsa0VBQWdFO0FBQ2hFLDhEQUE0RDtBQUM1RCwwREFBd0Q7QUFDeEQsNERBQTBEO0FBQzFELDhEQUE0RDtBQUM1RCxnRUFBNEQ7QUFFNUQsSUFBSSxRQUFRLEdBQUc7SUFDWCxnQ0FBYztJQUNkLDRDQUF3QjtJQUN4Qiw4QkFBYTtJQUNiLDBCQUFXO0lBQ1gsMEJBQVc7SUFDWCxzQ0FBaUI7SUFDakIsa0NBQWU7SUFDZixnQ0FBZ0I7SUFDaEIsMENBQW1CO0lBQ25CLDhCQUFhO0lBQ2Isa0NBQWU7SUFDZiwwQkFBVztJQUNYLDRCQUFZO0lBQ1osMEJBQVc7SUFDWCwwQkFBVztJQUNYLHNDQUFpQjtJQUNqQixnQ0FBYztJQUNkLGdDQUFjO0lBQ2QsOENBQXFCO0lBQ3JCLHdEQUEwQjtJQUMxQixnQ0FBYztJQUNkLDRCQUFZO0lBQ1osa0NBQWU7SUFDZixnQ0FBYztJQUNkLG9DQUFnQjtJQUNoQixnQ0FBYztJQUNkLDRCQUFZO0lBQ1osOEJBQWE7SUFDYixrQ0FBZTtJQUNmLHdDQUFrQjtDQUNyQixDQUFDO0FBRUYsNkNBQXdDO0FBRXhDLFlBQVk7QUFDWixtRUFBaUU7QUFDakUseUVBQXVFO0FBQ3ZFLHVEQUFzRTtBQUN0RSxzRUFBb0U7QUFDcEUsK0VBQTZFO0FBQzdFLGtGQUFnRjtBQUNoRiwyRkFBd0Y7QUFDeEYsaUdBQTZGO0FBQzdGLHVHQUFvRztBQUNwRyw0RUFBMEU7QUFDMUUsOEZBQTJGO0FBQzNGLG9HQUFpRztBQUVqRyx5SEFBb0g7QUFDcEgsNEhBQXVIO0FBQ3ZILDRIQUF1SDtBQUN2SCw4SUFBd0k7QUFDeEksK0VBQTZFO0FBQzdFLGtGQUErRTtBQUMvRSxrRkFBK0U7QUFDL0Usd0ZBQXFGO0FBQ3JGLDJGQUF3RjtBQUN4RiwyRkFBd0Y7QUFDeEYsOEZBQTJGO0FBQzNGLGtGQUErRTtBQUMvRSxvR0FBZ0c7QUFDaEcsdUdBQW1HO0FBQ25HLCtFQUE2RTtBQUM3RSxxRkFBa0Y7QUFDbEYsOEZBQTJGO0FBRzNGLGlHQUE4RjtBQUM5RixtSEFBK0c7QUFDL0csMkZBQXdGO0FBQ3hGLG1IQUErRztBQUMvRyx3RkFBcUY7QUFDckYsbUhBQStHO0FBQy9HLGtGQUErRTtBQUMvRSx3RkFBcUY7QUFDckYsMkZBQXdGO0FBQ3hGLDhGQUF5RjtBQUN6Rix5SEFBbUg7QUFDbkgscUZBQWdGO0FBRWhGLHdGQUFvRjtBQUNwRixtSEFBK0c7QUFFL0csSUFBSSxLQUFLLEdBQUc7SUFDUiw4QkFBYTtJQUNiLHVCQUF1QjtJQUN2QiwyQ0FBbUI7SUFDbkIsa0NBQWU7SUFDZiwyQkFBb0I7SUFDcEIsZ0NBQWM7SUFDZCx3Q0FBa0I7SUFDbEIsc0NBQWlCO0lBQ2pCLGdEQUFxQjtJQUNyQiw2Q0FBb0I7SUFDcEIscURBQXdCO0lBQ3hCLG9DQUFnQjtJQUNoQiwrQ0FBcUI7SUFDckIsbURBQXVCO0lBQ3ZCLCtEQUE0QjtJQUM1QixpRUFBNkI7SUFDN0IsaUVBQTZCO0lBQzdCLDRFQUFrQztJQUNsQyxzQ0FBaUI7SUFDakIsdUNBQWlCO0lBQ2pCLHVDQUFpQjtJQUNqQiwyQ0FBbUI7SUFDbkIsNkNBQW9CO0lBQ3BCLDZDQUFvQjtJQUNwQiwrQ0FBcUI7SUFDckIsdUNBQWlCO0lBQ2pCLGtEQUFzQjtJQUN0QixvREFBdUI7SUFDdkIsc0NBQWlCO0lBQ2pCLHlDQUFrQjtJQUNsQiwrQ0FBcUI7SUFDckIsaURBQXNCO0lBQ3RCLDREQUEyQjtJQUMzQiw2Q0FBb0I7SUFDcEIsMkNBQW1CO0lBQ25CLDREQUEyQjtJQUMzQix1Q0FBaUI7SUFDakIsNkNBQW9CO0lBQ3BCLCtDQUFxQjtJQUNyQiwyQ0FBbUI7SUFDbkIseUNBQWtCO0lBQ2xCLGdFQUE2QjtJQUM3Qiw0REFBMkI7SUFDM0IsNERBQTJCO0NBQzlCLENBQUM7QUFFRiwwREFBMEQ7QUE0QjFEO0lBQUE7SUFBeUIsQ0FBQztJQUFiLFNBQVM7UUExQnJCLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFDQSxPQUFPO2dCQUNWLHFCQUFPO2NBQ1Y7WUFDRCxZQUFZLEVBQ0wsT0FBTyxRQUNQLEtBQUssQ0FDWDtZQUNELFNBQVMsRUFFRixRQUFRO2dCQUNYLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO2dCQUM3QztvQkFDSSxPQUFPLEVBQUUsdUJBQVE7b0JBQ2pCLFVBQVUsRUFBRSxzQkFBc0I7b0JBQ2xDLElBQUksRUFBRSxDQUFDLFdBQUksRUFBRSxxQkFBYyxFQUFFLGVBQU0sQ0FBQztpQkFDdkM7Z0JBQ0Q7b0JBQ0ksT0FBTyxFQUFFLHlCQUFnQjtvQkFDekIsUUFBUSxFQUFFLDZCQUFvQjtpQkFDakM7Y0FDSjtZQUNELFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7U0FDNUIsQ0FBQztPQUVXLFNBQVMsQ0FBSTtJQUFELGdCQUFDO0NBQTFCLEFBQTBCLElBQUE7QUFBYiw4QkFBUyIsImZpbGUiOiJhcHAvYXBwLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOZ01vZHVsZUZhY3RvcnlMb2FkZXIsIFN5c3RlbUpzTmdNb2R1bGVMb2FkZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQnJvd3Nlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuLy8gaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSAgICBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEh0dHBNb2R1bGUsIEh0dHAsIFJlcXVlc3RPcHRpb25zLCBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBBbGVydE1vZHVsZSwgRGF0ZXBpY2tlck1vZHVsZSwgVGltZXBpY2tlck1vZHVsZSwgIEJ1dHRvbnNNb2R1bGUsIE1vZGFsTW9kdWxlLCBBY2NvcmRpb25Nb2R1bGUsIENhcm91c2VsTW9kdWxlLCBDb2xsYXBzZU1vZHVsZSwgVGFic01vZHVsZSwgUGFnaW5hdGlvbk1vZHVsZSB9IGZyb20gJ25neC1ib290c3RyYXAnO1xuLy8gaW1wb3J0IHsgTW9kYWxNb2R1bGUgIH0gZnJvbSAnbmcyLWJvb3RzdHJhcC9tb2RhbC9tb2RhbC5tb2R1bGUnO1xuaW1wb3J0IHsgVG9hc3Rlck1vZHVsZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXIvYW5ndWxhcjItdG9hc3Rlcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUsIFRyYW5zbGF0ZUxvYWRlciwgVHJhbnNsYXRlU3RhdGljTG9hZGVyIH0gZnJvbSAnbmcyLXRyYW5zbGF0ZSc7XG4vLyBpbXBvcnQge3RyYWNlLCBDYXRlZ29yeSwgVUlSb3V0ZXJNb2R1bGUsIFVJVmlld30gZnJvbSBcInVpLXJvdXRlci1uZzJcIjtcbi8vIGltcG9ydCB7IE15Um9vdFVJUm91dGVyQ29uZmlnIH0gZnJvbSAnYXBwL3JvdXRlci5jb25maWcnO1xuLy8gaW1wb3J0IHtNQUlOX1NUQVRFU30gZnJvbSBcImFwcC9hcHAuc3RhdGVzXCI7XG5pbXBvcnQgeyBBdXRoSHR0cCwgQXV0aENvbmZpZywgSnd0SGVscGVyIH0gZnJvbSAnYW5ndWxhcjItand0Jztcbi8vIGltcG9ydCB7IHByb3ZpZGVBdXRoLCBKd3RIZWxwZXIgfSBmcm9tICdhbmd1bGFyMi1qd3QnO1xuaW1wb3J0IHsgTG9jYXRpb25TdHJhdGVneSwgSGFzaExvY2F0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRU5WSVJPTk1FTlQgfSBmcm9tICdlbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xuaW1wb3J0IHsgU2VsZWN0TW9kdWxlIH0gZnJvbSAnbmcyLXNlbGVjdCc7XG5pbXBvcnQgKiBhcyBDcnlwdG9KUyBmcm9tICdjcnlwdG8tanMnO1xuaW1wb3J0IHsgQ2hhcnRzTW9kdWxlIH0gZnJvbSAnbmcyLWNoYXJ0cyc7XG5pbXBvcnQgeyBDYWxlbmRhck1vZHVsZSB9IGZyb20gJ3ByaW1lbmcvcHJpbWVuZyc7XG5cblxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlVHJhbnNsYXRlTG9hZGVyKGh0dHA6IEh0dHApIHtcbiAgICAvLyByZXR1cm4gbmV3IFRyYW5zbGF0ZVN0YXRpY0xvYWRlciggaHR0cCwgJ2FkbWluL2FwcC9pMThuLycpO1xuICAgIHJldHVybiBuZXcgVHJhbnNsYXRlU3RhdGljTG9hZGVyKGh0dHAsICdsb2NhbGUnKTtcbn1cblxuLy8gc2F2ZSBhIGp3dCB0b2tlblxuLy8gbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Rva2VuJywnZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lJeE1qTTBOVFkzT0Rrd0lpd2libUZ0WlNJNklrcHZhRzRnUkc5bElpd2lZV1J0YVc0aU9uUnlkV1Y5LlRKVkE5NU9yTTdFMmNCYWIzMFJNSHJIRGNFZnhqb1laZ2VGT05GaDdIZ1EnKTtcblxuXG5leHBvcnQgZnVuY3Rpb24gYXV0aEh0dHBTZXJ2aWNlRmFjdG9yeShodHRwOiBIdHRwLCBvcHRpb25zOiBSZXF1ZXN0T3B0aW9ucywgcm91dGVyOiBSb3V0ZXIpIHtcbiAgICAvLyBsZXQgand0SGVscGVyOiBKd3RIZWxwZXIgPSBuZXcgSnd0SGVscGVyKCk7XG4gICAgcmV0dXJuIG5ldyBBdXRoSHR0cChuZXcgQXV0aENvbmZpZyh7XG4gICAgICAgIHRva2VuTmFtZTogJ2lkX3Rva2VuJyxcbiAgICAgICAgdG9rZW5HZXR0ZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCd0b2tlbkdldHRlcicpO1xuICAgICAgICAgICAgdmFyIGtleSA9IENyeXB0b0pTLmVuYy5CYXNlNjQucGFyc2UoXCIjYmFzZTY0S2V5I1wiKTtcbiAgICAgICAgICAgIHZhciBpdiA9IENyeXB0b0pTLmVuYy5CYXNlNjQucGFyc2UoXCIjYmFzZTY0SVYjXCIpO1xuICAgICAgICAgICAgdmFyIGlkVG9rZW4gPSBcIlwiO1xuICAgICAgICAgICAgdmFyIGVuY3J5cHRlZFRva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2lkX3Rva2VuJyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZW5jcnlwdGVkVG9rZW4gJywgZW5jcnlwdGVkVG9rZW4pO1xuICAgICAgICAgICAgaWYgKGVuY3J5cHRlZFRva2VuICE9IHVuZGVmaW5lZCAmJiBlbmNyeXB0ZWRUb2tlbiAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgdmFyIGRlY3J5cHRlZCA9IENyeXB0b0pTLkFFUy5kZWNyeXB0KGVuY3J5cHRlZFRva2VuLnRvU3RyaW5nKCksIGtleSwgeyBpdjogaXYgfSk7XG4gICAgICAgICAgICAgICAgaWRUb2tlbiA9IGRlY3J5cHRlZC50b1N0cmluZyhDcnlwdG9KUy5lbmMuVXRmOCk7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2Rlc2NyeXB0ZWQgJywgaWRUb2tlbik7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChpZFRva2VuKSB7XG4gICAgICAgICAgICAgICAgbGV0IGp3dEhlbHBlcjogSnd0SGVscGVyID0gbmV3IEp3dEhlbHBlcigpO1xuICAgICAgICAgICAgICAgIGlmIChqd3RIZWxwZXIuaXNUb2tlbkV4cGlyZWQoaWRUb2tlbikpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJy0tLS0tLUF1dGhIdHRwIGdldCB0b2tlbiBzZXJ2ZXItLS0tLS0nKTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsICdBdXRob3JpemF0aW9uJzogJ0JlYXJlciAnICsgaWRUb2tlbiwgJ3NraXBBdXRob3JpemF0aW9uJzogdHJ1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IG9wdGlvbnMgPSBuZXcgUmVxdWVzdE9wdGlvbnMoeyBoZWFkZXJzOiBoZWFkZXJzIH0pO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gaHR0cC5wb3N0KEVOVklST05NRU5ULkRPTUFJTl9BUEkgKyAnOicgKyBFTlZJUk9OTUVOVC5TRVJWRVJfUE9SVCArICcvdG9rZW4nLCBvcHRpb25zKS50b1Byb21pc2UoKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGEgPSByZXNwb25zZS5qc29uKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vSW1wZW1lbnRpbmcgdGhlIEtleSBhbmQgSVYgYW5kIGVuY3J5cHQgdGhlIHBhc3N3b3JkXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZW5jcnlwdGVkVG9rZW4gPSBDcnlwdG9KUy5BRVMuZW5jcnlwdChkYXRhLnRva2VuLCBrZXksIHsgaXY6IGl2IH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRva2VuID0gZW5jcnlwdGVkVG9rZW4udG9TdHJpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlbmNyeXB0ZWQgMTIzICcsIGVuY3J5cHRlZFRva2VuLnRvU3RyaW5nKCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2lkX3Rva2VuJywgdG9rZW4pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChyZXNwb25zZS5zdGF0dXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSA0MDM6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2xvZyddKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDQwMTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdpZF90b2tlbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydsb2dpbiddKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydlcnJvciddKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJy0tLS0tLUF1dGhIdHRwIGdldCB0b2tlbiBsb2NhbFN0b3JhZ2UtLS0tLS0nKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGlkVG9rZW47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBnbG9iYWxIZWFkZXJzOiBbeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH1dLFxuICAgICAgICBub0p3dEVycm9yOiB0cnVlXG4gICAgfSksIGh0dHAsIG9wdGlvbnMpO1xufVxuXG5sZXQgbW9kdWxlcyA9IFtcbiAgICBCcm93c2VyTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgSHR0cE1vZHVsZSxcbiAgICBDaGFydHNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBSb3V0ZXJNb2R1bGUsXG4gICAgVG9hc3Rlck1vZHVsZSxcbiAgICBBbGVydE1vZHVsZS5mb3JSb290KCksXG4gICAgQnV0dG9uc01vZHVsZS5mb3JSb290KCksXG4gICAgTW9kYWxNb2R1bGUuZm9yUm9vdCgpLFxuICAgIEFjY29yZGlvbk1vZHVsZS5mb3JSb290KCksXG5cbiAgICBEYXRlcGlja2VyTW9kdWxlLmZvclJvb3QoKSxcbiAgICBUaW1lcGlja2VyTW9kdWxlLmZvclJvb3QoKSxcbiAgICBDYXJvdXNlbE1vZHVsZS5mb3JSb290KCksXG4gICAgQ29sbGFwc2VNb2R1bGUuZm9yUm9vdCgpLFxuICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KHtcbiAgICAgICAgZGVwczogW0h0dHBdLFxuICAgICAgICBwcm92aWRlOiBUcmFuc2xhdGVMb2FkZXIsXG4gICAgICAgIHVzZUZhY3Rvcnk6IChjcmVhdGVUcmFuc2xhdGVMb2FkZXIpXG4gICAgfSksXG4gICAgVGFic01vZHVsZS5mb3JSb290KCksXG4gICAgUGFnaW5hdGlvbk1vZHVsZS5mb3JSb290KCksXG4gICAgLy8gVUlSb3V0ZXJNb2R1bGUuZm9yUm9vdCh7XG4gICAgLy8gICBzdGF0ZXM6IE1BSU5fU1RBVEVTLFxuICAgIC8vICAgb3RoZXJ3aXNlOiB7IHN0YXRlOiAnYXBwJywgcGFyYW1zOiB7fSB9LFxuICAgIC8vICAgdXNlSGFzaDogdHJ1ZSxcbiAgICAvLyAgIGNvbmZpZ0NsYXNzOiBNeVJvb3RVSVJvdXRlckNvbmZpZ1xuICAgIC8vIH0pXG4gICAgU2VsZWN0TW9kdWxlLFxuICAgIENhbGVuZGFyTW9kdWxlXG5dO1xuXG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tICcuL2FwcC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQXBwSGVhZGVyQ29tcG9uZW50IH0gZnJvbSAnLi93aWRnZXRzL2FwcC1oZWFkZXIvYXBwLWhlYWRlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgTWVudUFzaWRlQ29tcG9uZW50IH0gZnJvbSAnLi93aWRnZXRzL21lbnUtYXNpZGUvbWVudS1hc2lkZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYkNvbXBvbmVudCB9IGZyb20gJy4vd2lkZ2V0cy9icmVhZGNydW1iL2JyZWFkY3J1bWIuY29tcG9uZW50JztcbmltcG9ydCB7IEFwcEZvb3RlckNvbXBvbmVudCB9IGZyb20gJy4vd2lkZ2V0cy9hcHAtZm9vdGVyL2FwcC1mb290ZXIuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRyb2xTaWRlYmFyQ29tcG9uZW50IH0gZnJvbSAnLi93aWRnZXRzL2NvbnRyb2wtc2lkZWJhci9jb250cm9sLXNpZGViYXIuY29tcG9uZW50JztcbmltcG9ydCB7IE1lc3NhZ2VzQm94Q29tcG9uZW50IH0gZnJvbSAnLi93aWRnZXRzL21lc3NhZ2VzLWJveC9tZXNzYWdlcy1ib3guY29tcG9uZW50JztcbmltcG9ydCB7IE5vdGlmaWNhdGlvbkJveENvbXBvbmVudCB9IGZyb20gJy4vd2lkZ2V0cy9ub3RpZmljYXRpb24tYm94L25vdGlmaWNhdGlvbi1ib3guY29tcG9uZW50JztcbmltcG9ydCB7IFRhc2tzQm94Q29tcG9uZW50IH0gZnJvbSAnLi93aWRnZXRzL3Rhc2tzLWJveC90YXNrcy1ib3guY29tcG9uZW50JztcbmltcG9ydCB7IFVzZXJCb3hDb21wb25lbnQgfSBmcm9tICcuL3dpZGdldHMvdXNlci1ib3gvdXNlci1ib3guY29tcG9uZW50JztcbmltcG9ydCB7IExpbWl0VG9EaXJlY3RpdmUgfSBmcm9tICcuL3dpZGdldHMvbGltaXQtdG8uZGlyZWN0aXZlJztcblxubGV0IHdpZGdldHMgPSBbXG4gICAgQXBwQ29tcG9uZW50LFxuICAgIEJyZWFkY3J1bWJDb21wb25lbnQsXG4gICAgQXBwSGVhZGVyQ29tcG9uZW50LFxuICAgIEFwcEZvb3RlckNvbXBvbmVudCxcbiAgICBNZW51QXNpZGVDb21wb25lbnQsXG4gICAgQ29udHJvbFNpZGViYXJDb21wb25lbnQsXG4gICAgTWVzc2FnZXNCb3hDb21wb25lbnQsXG4gICAgTm90aWZpY2F0aW9uQm94Q29tcG9uZW50LFxuICAgIFRhc2tzQm94Q29tcG9uZW50LFxuICAgIFVzZXJCb3hDb21wb25lbnQsXG4gICAgTGltaXRUb0RpcmVjdGl2ZVxuXTtcblxuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgTWVzc2FnZXNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9tZXNzYWdlcy5zZXJ2aWNlJztcbmltcG9ydCB7IENhbkFjdGl2YXRlR3VhcmQgfSBmcm9tICcuL3NlcnZpY2VzL2d1YXJkLnNlcnZpY2UnO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgQnJlYWRjcnVtYlNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQgeyBBZG1pbkxURVRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3RyYW5zbGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IExvZ2dlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2xvZ2dlci5zZXJ2aWNlJztcbmltcG9ydCB7IEhlcm9TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9oZXJvLnNlcnZpY2UnO1xuaW1wb3J0IHsgU2VydmVyU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvc2VydmVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2NhdGVnb3J5LnNlcnZpY2UnO1xuaW1wb3J0IHsgQXJlYVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2FyZWEuc2VydmljZSc7XG5pbXBvcnQgeyBUYWJsZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3RhYmxlLnNlcnZpY2UnO1xuaW1wb3J0IHsgTmV3c1NlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL25ld3Muc2VydmljZSc7XG5pbXBvcnQgeyBSZXN0YXVyYW50U2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvcmVzdGF1cmFudC5zZXJ2aWNlJztcbmltcG9ydCB7IENvbnRhY3RTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb250YWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgT3B0aW9uRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9vcHRpb25Gb29kSXRlbS5zZXJ2aWNlJztcbmltcG9ydCB7IE9wdGlvblZhbHVlRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9vcHRpb25WYWx1ZUZvb2RJdGVtLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tQ29kZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2NvbUNvZGUuc2VydmljZSc7XG5pbXBvcnQgeyBPcmRlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgRm9vZEl0ZW1TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9mb29kSXRlbS5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2ZpbGVTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9wcm9maWxlLnNlcnZpY2UnO1xuaW1wb3J0IHsgR2VvTG9jYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9nZW9Mb2NhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFN1YnNjcmliZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3N1YnNjcmliZS5zZXJ2aWNlJztcbmltcG9ydCB7IFNlYXJjaHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9zZWFyY2hzLnNlcnZpY2UnO1xuaW1wb3J0IHsgRW1haWxTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9lbWFpbC5zZXJ2aWNlJztcbmltcG9ydCB7IFJldmlld1NlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3Jldmlldy5zZXJ2aWNlJztcbmltcG9ydCB7IFJlc2VydmVTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9yZXNlcnZlLnNlcnZpY2UnO1xuaW1wb3J0IHtUdXJuT3ZlclNlcnZpY2V9IGZyb20gJy4vc2VydmljZXMvdHVybm92ZXIuc2VydmljZSc7XG5cbmxldCBzZXJ2aWNlcyA9IFtcbiAgICBTdG9yYWdlU2VydmljZSxcbiAgICBBZG1pbkxURVRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgU2VydmVyU2VydmljZSxcbiAgICBBdXRoU2VydmljZSxcbiAgICBVc2VyU2VydmljZSxcbiAgICBCcmVhZGNydW1iU2VydmljZSxcbiAgICBNZXNzYWdlc1NlcnZpY2UsXG4gICAgQ2FuQWN0aXZhdGVHdWFyZCxcbiAgICBOb3RpZmljYXRpb25TZXJ2aWNlLFxuICAgIExvZ2dlclNlcnZpY2UsXG4gICAgQ2F0ZWdvcnlTZXJ2aWNlLFxuICAgIEFyZWFTZXJ2aWNlLFxuICAgIFRhYmxlU2VydmljZSxcbiAgICBIZXJvU2VydmljZSxcbiAgICBOZXdzU2VydmljZSxcbiAgICBSZXN0YXVyYW50U2VydmljZSxcbiAgICBSZXNlcnZlU2VydmljZSxcbiAgICBDb250YWN0U2VydmljZSxcbiAgICBPcHRpb25Gb29kSXRlbVNlcnZpY2UsXG4gICAgT3B0aW9uVmFsdWVGb29kSXRlbVNlcnZpY2UsXG4gICAgQ29tQ29kZVNlcnZpY2UsXG4gICAgT3JkZXJTZXJ2aWNlLFxuICAgIEZvb2RJdGVtU2VydmljZSxcbiAgICBQcm9maWxlU2VydmljZSxcbiAgICBTdWJzY3JpYmVTZXJ2aWNlLFxuICAgIFNlYXJjaHNTZXJ2aWNlLFxuICAgIEVtYWlsU2VydmljZSxcbiAgICBSZXZpZXdTZXJ2aWNlLFxuICAgIFR1cm5PdmVyU2VydmljZSxcbiAgICBHZW9Mb2NhdGlvblNlcnZpY2Vcbl07XG5cbmltcG9ydCB7IHJvdXRpbmcgfSBmcm9tICcuL2FwcC5yb3V0aW5nJztcblxuLy8gbGVzIHBhZ2VzXG5pbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2xpZW50Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NsaWVudC9jbGllbnQuY29tcG9uZW50JztcbmltcG9ydCB7IExheW91dHNBdXRoQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2xheW91dHMvYXV0aC9hdXRoJztcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSZWdpc3RlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGFzaGJvYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50JztcbmltcG9ydCB7IFN5c3RlbUVycm9yQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3N5c3RlbS1lcnJvci9zeXN0ZW0tZXJyb3IuY29tcG9uZW50JztcbmltcG9ydCB7IFBhZ2VOb3RGb3VuZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wYWdlLW5vdC1mb3VuZC9wYWdlLW5vdC1mb3VuZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3JkZXJpbmdSZXF1ZXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL29yZGVyaW5nLXJlcXVlc3Qvb3JkZXJpbmctcmVxdWVzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUHJvZmlsZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50JztcbmltcG9ydCB7IENhdGVnb3J5TGlzdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXRlZ29yeS1saXN0L2NhdGVnb3J5LWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IENyZWF0ZUNhdGVnb3J5Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NyZWF0ZS1jYXRlZ29yeS9jcmVhdGUtY2F0ZWdvcnkuY29tcG9uZW50JztcblxuaW1wb3J0IHsgT3B0aW9uRm9vZEl0ZW1zTGlzdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcHRpb24tZm9vZC1pdGVtcy1saXN0L29wdGlvbi1mb29kLWl0ZW1zLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IENyZWF0ZU9wdGlvbkZvb2RJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtL2NyZWF0ZS1vcHRpb24tZm9vZC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25WYWx1ZUZvb2RJdGVtc0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcHRpb24tdmFsdWUtZm9vZC1pdGVtcy9vcHRpb24tdmFsdWUtZm9vZC1pdGVtcy5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3JlYXRlT3B0aW9uVmFsdWVGb29kSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS9jcmVhdGUtb3B0aW9uLXZhbHVlLWZvb2QtaXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29udGVudHNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGVudHMvY29udGVudHMuY29tcG9uZW50JztcbmltcG9ydCB7IE5ld3NMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25ld3MtbGlzdC9uZXdzLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IE5ld3NJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25ld3MtaXRlbS9uZXdzLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IE9yZGVyc0xpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvb3JkZXJzLWxpc3Qvb3JkZXJzLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRhY3RMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbnRhY3QtbGlzdC9jb250YWN0LWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRhY3RJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbnRhY3QtaXRlbS9jb250YWN0LWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IE9yZGVyU2V0dGluZ0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcmRlci1zZXR0aW5nL29yZGVyLXNldHRpbmcuY29tcG9uZW50JztcbmltcG9ydCB7IFR1cm5PdmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3R1cm4tb3Zlci90dXJuLW92ZXIuY29tcG9uZW50JztcbmltcG9ydCB7IEZvb2RJdGVtc0xpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9vZC1pdGVtcy1saXN0L2Zvb2QtaXRlbXMtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3JlYXRlRm9vZEl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY3JlYXRlLWZvb2QtaXRlbS9jcmVhdGUtZm9vZC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGZWVkYmFja0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mZWVkYmFjay9mZWVkYmFjay5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGFibGVMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3RhYmxlLWxpc3QvdGFibGUtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGFibGVCb29raW5nQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3RhYmxlLWJvb2tpbmcvdGFibGUtYm9va2luZy5jb21wb25lbnQnO1xuXG5cbmltcG9ydCB7IFN1YnNjcmliZUxpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc3Vic2NyaWJlLWxpc3Qvc3Vic2NyaWJlLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IFNlbmRFbWFpbE1hcmtldGluZ0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zZW5kLWVtYWlsLW1hcmtldGluZy9zZW5kLWVtYWlsLW1hcmtldGluZy5jb21wb25lbnQnO1xuaW1wb3J0IHsgU2VhcmNoSXRlbXNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2VhcmNoLWl0ZW1zL3NlYXJjaC1pdGVtcy5jb21wb25lbnQnO1xuaW1wb3J0IHsgRW1haWxNYXJrZXRpbmdMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2VtYWlsLW1hcmtldGluZy1saXN0L2VtYWlsLW1hcmtldGluZy1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSZXZpZXdJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3Jldmlldy1pdGVtL3Jldmlldy1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBWaWV3RW1haWxNYXJrZXRpbmdDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdmlldy1lbWFpbC1tYXJrZXRpbmcvdmlldy1lbWFpbC1tYXJrZXRpbmcuY29tcG9uZW50JztcbmltcG9ydCB7IEFyZWFMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2FyZWEtbGlzdC9hcmVhLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IENyZWF0ZUFyZWFDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY3JlYXRlLWFyZWEvY3JlYXRlLWFyZWEuY29tcG9uZW50JztcbmltcG9ydCB7IENyZWF0ZVRhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NyZWF0ZS10YWJsZS9jcmVhdGUtdGFibGUuY29tcG9uZW50JztcbmltcG9ydCB7UmVzZXJ2ZVRhYmxlQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvcmVzZXJ2ZS10YWJsZS9yZXNlcnZlLXRhYmxlLmNvbXBvbmVudCc7XG5pbXBvcnQge1RhYmxlQm9va2luZ1NjaGVkdWxlQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvdGFibGUtYm9va2luZy1zY2hlZHVsZS90YWJsZS1ib29raW5nLXNjaGVkdWxlLmNvbXBvbmVudCc7XG5pbXBvcnQge09yZGVyRm9vZENvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL29yZGVyLWZvb2Qvb3JkZXItZm9vZC5jb21wb25lbnQnO1xuXG5pbXBvcnQgeyBMb2dpbk1vZGFsQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvbG9naW4tbW9kYWwvbG9naW4tbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IFN1YnNjcmliZUxpc3RNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zdWJzY3JpYmUtbGlzdC1tb2RhbC9zdWJzY3JpYmUtbGlzdC1tb2RhbC5jb21wb25lbnQnO1xuXG5sZXQgcGFnZXMgPSBbXG4gICAgSG9tZUNvbXBvbmVudCxcbiAgICAvLyAvLyBQYWdlTnVtQ29tcG9uZW50LFxuICAgIENyZWF0ZUFyZWFDb21wb25lbnQsXG4gICAgQ2xpZW50Q29tcG9uZW50LFxuICAgIExheW91dHNBdXRoQ29tcG9uZW50LFxuICAgIExvZ2luQ29tcG9uZW50LFxuICAgIERhc2hib2FyZENvbXBvbmVudCxcbiAgICBSZWdpc3RlckNvbXBvbmVudCxcbiAgICBQYWdlTm90Rm91bmRDb21wb25lbnQsXG4gICAgU3lzdGVtRXJyb3JDb21wb25lbnQsXG4gICAgT3JkZXJpbmdSZXF1ZXN0Q29tcG9uZW50LFxuICAgIFByb2ZpbGVDb21wb25lbnQsXG4gICAgQ2F0ZWdvcnlMaXN0Q29tcG9uZW50LFxuICAgIENyZWF0ZUNhdGVnb3J5Q29tcG9uZW50LFxuICAgIE9wdGlvbkZvb2RJdGVtc0xpc3RDb21wb25lbnQsXG4gICAgQ3JlYXRlT3B0aW9uRm9vZEl0ZW1Db21wb25lbnQsXG4gICAgT3B0aW9uVmFsdWVGb29kSXRlbXNDb21wb25lbnQsXG4gICAgQ3JlYXRlT3B0aW9uVmFsdWVGb29kSXRlbUNvbXBvbmVudCxcbiAgICBDb250ZW50c0NvbXBvbmVudCxcbiAgICBOZXdzTGlzdENvbXBvbmVudCxcbiAgICBOZXdzSXRlbUNvbXBvbmVudCxcbiAgICBPcmRlcnNMaXN0Q29tcG9uZW50LFxuICAgIENvbnRhY3RMaXN0Q29tcG9uZW50LFxuICAgIENvbnRhY3RJdGVtQ29tcG9uZW50LFxuICAgIE9yZGVyU2V0dGluZ0NvbXBvbmVudCxcbiAgICBUdXJuT3ZlckNvbXBvbmVudCxcbiAgICBGb29kSXRlbXNMaXN0Q29tcG9uZW50LFxuICAgIENyZWF0ZUZvb2RJdGVtQ29tcG9uZW50LFxuICAgIEZlZWRiYWNrQ29tcG9uZW50LFxuICAgIFRhYmxlTGlzdENvbXBvbmVudCxcbiAgICBUYWJsZUJvb2tpbmdDb21wb25lbnQsXG4gICAgU3Vic2NyaWJlTGlzdENvbXBvbmVudCxcbiAgICBTZW5kRW1haWxNYXJrZXRpbmdDb21wb25lbnQsXG4gICAgU2VhcmNoSXRlbXNDb21wb25lbnQsXG4gICAgUmV2aWV3SXRlbUNvbXBvbmVudCxcbiAgICBWaWV3RW1haWxNYXJrZXRpbmdDb21wb25lbnQsXG4gICAgQXJlYUxpc3RDb21wb25lbnQsXG4gICAgQ3JlYXRlVGFibGVDb21wb25lbnQsXG4gICAgUmVzZXJ2ZVRhYmxlQ29tcG9uZW50LFxuICAgIExvZ2luTW9kYWxDb21wb25lbnQsXG4gICAgT3JkZXJGb29kQ29tcG9uZW50LFxuICAgIFRhYmxlQm9va2luZ1NjaGVkdWxlQ29tcG9uZW50LFxuICAgIEVtYWlsTWFya2V0aW5nTGlzdENvbXBvbmVudCxcbiAgICBTdWJzY3JpYmVMaXN0TW9kYWxDb21wb25lbnRcbl07XG5cbi8vIHRyYWNlLmVuYWJsZShDYXRlZ29yeS5UUkFOU0lUSU9OLCBDYXRlZ29yeS5WSUVXQ09ORklHKTtcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIC4uLm1vZHVsZXMsXG4gICAgICAgIHJvdXRpbmdcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICAuLi53aWRnZXRzLFxuICAgICAgICAuLi5wYWdlcyxcbiAgICBdLFxuICAgIHByb3ZpZGVyczogW1xuICAgICAgICAvLyB7IHByb3ZpZGU6IE5nTW9kdWxlRmFjdG9yeUxvYWRlciwgdXNlQ2xhc3M6IFN5c3RlbUpzTmdNb2R1bGVMb2FkZXIgfSxcbiAgICAgICAgLi4uc2VydmljZXMsXG4gICAgICAgIHsgcHJvdmlkZTogXCJ3aW5kb3dPYmplY3RcIiwgdXNlVmFsdWU6IHdpbmRvdyB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBwcm92aWRlOiBBdXRoSHR0cCxcbiAgICAgICAgICAgIHVzZUZhY3Rvcnk6IGF1dGhIdHRwU2VydmljZUZhY3RvcnksXG4gICAgICAgICAgICBkZXBzOiBbSHR0cCwgUmVxdWVzdE9wdGlvbnMsIFJvdXRlcl1cbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgcHJvdmlkZTogTG9jYXRpb25TdHJhdGVneSxcbiAgICAgICAgICAgIHVzZUNsYXNzOiBIYXNoTG9jYXRpb25TdHJhdGVneVxuICAgICAgICB9XG4gICAgXSxcbiAgICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdXG59KVxuXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuIl19
