"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
/** A hero's name can't match the given regular expression */
function invalidEmailValidator() {
    return function (control) {
        var nameRe = new RegExp(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i);
        var name = control.value;
        if (name == '') {
            return null;
        }
        var no = !nameRe.test(name);
        return no ? { 'invalidEmail': { name: name } } : null;
    };
}
exports.invalidEmailValidator = invalidEmailValidator;
var InvalidEmailValidatorDirective = (function () {
    function InvalidEmailValidatorDirective() {
        this.valFn = forms_1.Validators.nullValidator;
    }
    InvalidEmailValidatorDirective_1 = InvalidEmailValidatorDirective;
    InvalidEmailValidatorDirective.prototype.ngOnChanges = function (changes) {
        var change = changes['invalidEmail'];
        if (change) {
            var val = change.currentValue;
            var re = val instanceof RegExp ? val : new RegExp(val, 'i');
            this.valFn = invalidEmailValidator();
        }
        else {
            this.valFn = forms_1.Validators.nullValidator;
        }
    };
    InvalidEmailValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], InvalidEmailValidatorDirective.prototype, "invalidEmail", void 0);
    InvalidEmailValidatorDirective = InvalidEmailValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[invalidEmail]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: InvalidEmailValidatorDirective_1, multi: true }]
        })
    ], InvalidEmailValidatorDirective);
    return InvalidEmailValidatorDirective;
    var InvalidEmailValidatorDirective_1;
}());
exports.InvalidEmailValidatorDirective = InvalidEmailValidatorDirective;
/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/ 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC91dGlscy9lbWFpbC12YWxpZGF0b3IuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJFO0FBQzNFLHdDQUFvRztBQUVwRyw2REFBNkQ7QUFDN0Q7SUFDRSxNQUFNLENBQUMsVUFBQyxPQUF3QjtRQUU5QixJQUFNLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxtR0FBbUcsQ0FBQyxDQUFDO1FBQy9ILElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDM0IsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDZixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2QsQ0FBQztRQUNELElBQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QixNQUFNLENBQUMsRUFBRSxHQUFHLEVBQUMsY0FBYyxFQUFFLEVBQUMsSUFBSSxNQUFBLEVBQUMsRUFBQyxHQUFHLElBQUksQ0FBQztJQUM5QyxDQUFDLENBQUM7QUFDSixDQUFDO0FBWEQsc0RBV0M7QUFNRDtJQUpBO1FBTVUsVUFBSyxHQUFHLGtCQUFVLENBQUMsYUFBYSxDQUFDO0lBZ0IzQyxDQUFDO3VDQWxCWSw4QkFBOEI7SUFJekMsb0RBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN2QyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ1gsSUFBTSxHQUFHLEdBQW9CLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDakQsSUFBTSxFQUFFLEdBQUcsR0FBRyxZQUFZLE1BQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxLQUFLLEdBQUcscUJBQXFCLEVBQUUsQ0FBQztRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLGtCQUFVLENBQUMsYUFBYSxDQUFDO1FBQ3hDLENBQUM7SUFDSCxDQUFDO0lBRUQsaURBQVEsR0FBUixVQUFTLE9BQXdCO1FBQy9CLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFoQlE7UUFBUixZQUFLLEVBQUU7O3dFQUFzQjtJQURuQiw4QkFBOEI7UUFKMUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsU0FBUyxFQUFFLENBQUMsRUFBQyxPQUFPLEVBQUUscUJBQWEsRUFBRSxXQUFXLEVBQUUsZ0NBQThCLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDO1NBQ2hHLENBQUM7T0FDVyw4QkFBOEIsQ0FrQjFDO0lBQUQscUNBQUM7O0NBbEJELEFBa0JDLElBQUE7QUFsQlksd0VBQThCO0FBc0IzQzs7OztFQUlFIiwiZmlsZSI6ImFwcC91dGlscy9lbWFpbC12YWxpZGF0b3IuZGlyZWN0aXZlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBYnN0cmFjdENvbnRyb2wsIE5HX1ZBTElEQVRPUlMsIFZhbGlkYXRvciwgVmFsaWRhdG9yRm4sIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbi8qKiBBIGhlcm8ncyBuYW1lIGNhbid0IG1hdGNoIHRoZSBnaXZlbiByZWd1bGFyIGV4cHJlc3Npb24gKi9cbmV4cG9ydCBmdW5jdGlvbiBpbnZhbGlkRW1haWxWYWxpZGF0b3IoKTogVmFsaWRhdG9yRm4ge1xuICByZXR1cm4gKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCk6IHtba2V5OiBzdHJpbmddOiBhbnl9ID0+IHtcblxuICAgIGNvbnN0IG5hbWVSZSA9IG5ldyBSZWdFeHAoL15bYS16MC05ISMkJSYnKitcXC89P15fYHt8fX4uLV0rQFthLXowLTldKFthLXowLTktXSpbYS16MC05XSk/KFxcLlthLXowLTldKFthLXowLTktXSpbYS16MC05XSk/KSokL2kpO1xuICAgIGNvbnN0IG5hbWUgPSBjb250cm9sLnZhbHVlO1xuICAgIGlmIChuYW1lID09ICcnKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgY29uc3Qgbm8gPSAhbmFtZVJlLnRlc3QobmFtZSk7XG4gICAgcmV0dXJuIG5vID8geydpbnZhbGlkRW1haWwnOiB7bmFtZX19IDogbnVsbDtcbiAgfTtcbn1cblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2ludmFsaWRFbWFpbF0nLFxuICBwcm92aWRlcnM6IFt7cHJvdmlkZTogTkdfVkFMSURBVE9SUywgdXNlRXhpc3Rpbmc6IEludmFsaWRFbWFpbFZhbGlkYXRvckRpcmVjdGl2ZSwgbXVsdGk6IHRydWV9XVxufSlcbmV4cG9ydCBjbGFzcyBJbnZhbGlkRW1haWxWYWxpZGF0b3JEaXJlY3RpdmUgaW1wbGVtZW50cyBWYWxpZGF0b3IsIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIGludmFsaWRFbWFpbDogc3RyaW5nO1xuICBwcml2YXRlIHZhbEZuID0gVmFsaWRhdG9ycy5udWxsVmFsaWRhdG9yO1xuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICBjb25zdCBjaGFuZ2UgPSBjaGFuZ2VzWydpbnZhbGlkRW1haWwnXTtcbiAgICBpZiAoY2hhbmdlKSB7XG4gICAgICBjb25zdCB2YWw6IHN0cmluZyB8IFJlZ0V4cCA9IGNoYW5nZS5jdXJyZW50VmFsdWU7XG4gICAgICBjb25zdCByZSA9IHZhbCBpbnN0YW5jZW9mIFJlZ0V4cCA/IHZhbCA6IG5ldyBSZWdFeHAodmFsLCAnaScpO1xuICAgICAgdGhpcy52YWxGbiA9IGludmFsaWRFbWFpbFZhbGlkYXRvcigpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnZhbEZuID0gVmFsaWRhdG9ycy5udWxsVmFsaWRhdG9yO1xuICAgIH1cbiAgfVxuXG4gIHZhbGlkYXRlKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCk6IHtba2V5OiBzdHJpbmddOiBhbnl9IHtcbiAgICByZXR1cm4gdGhpcy52YWxGbihjb250cm9sKTtcbiAgfVxufVxuXG5cblxuLypcbkNvcHlyaWdodCAyMDE3IEdvb2dsZSBJbmMuIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG5Vc2Ugb2YgdGhpcyBzb3VyY2UgY29kZSBpcyBnb3Zlcm5lZCBieSBhbiBNSVQtc3R5bGUgbGljZW5zZSB0aGF0XG5jYW4gYmUgZm91bmQgaW4gdGhlIExJQ0VOU0UgZmlsZSBhdCBodHRwOi8vYW5ndWxhci5pby9saWNlbnNlXG4qLyJdfQ==
