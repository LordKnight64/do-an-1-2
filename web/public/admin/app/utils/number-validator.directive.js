"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
/** A hero's name can't match the given regular expression */
function invalidNumberValidator() {
    return function (control) {
        var nameRe = new RegExp(/^[0-9]+$/i);
        var name = control.value;
        if (name == '') {
            return null;
        }
        var no = !nameRe.test(name);
        return no ? { 'invalidNumber': { name: name } } : null;
    };
}
exports.invalidNumberValidator = invalidNumberValidator;
var InvalidNumberValidatorDirective = (function () {
    function InvalidNumberValidatorDirective() {
        this.valFn = forms_1.Validators.nullValidator;
    }
    InvalidNumberValidatorDirective_1 = InvalidNumberValidatorDirective;
    InvalidNumberValidatorDirective.prototype.ngOnChanges = function (changes) {
        var change = changes['invalidNumber'];
        if (change) {
            var val = change.currentValue;
            var re = val instanceof RegExp ? val : new RegExp(val, 'i');
            this.valFn = invalidNumberValidator();
        }
        else {
            this.valFn = forms_1.Validators.nullValidator;
        }
    };
    InvalidNumberValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], InvalidNumberValidatorDirective.prototype, "invalidNumber", void 0);
    InvalidNumberValidatorDirective = InvalidNumberValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[invalidNumber]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: InvalidNumberValidatorDirective_1, multi: true }]
        })
    ], InvalidNumberValidatorDirective);
    return InvalidNumberValidatorDirective;
    var InvalidNumberValidatorDirective_1;
}());
exports.InvalidNumberValidatorDirective = InvalidNumberValidatorDirective;
/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/ 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC91dGlscy9udW1iZXItdmFsaWRhdG9yLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUEyRTtBQUMzRSx3Q0FBb0c7QUFFcEcsNkRBQTZEO0FBQzdEO0lBQ0UsTUFBTSxDQUFDLFVBQUMsT0FBd0I7UUFFOUIsSUFBTSxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDdkMsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUMzQixFQUFFLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNmLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBQ0QsSUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLE1BQU0sQ0FBQyxFQUFFLEdBQUcsRUFBQyxlQUFlLEVBQUUsRUFBQyxJQUFJLE1BQUEsRUFBQyxFQUFDLEdBQUcsSUFBSSxDQUFDO0lBQy9DLENBQUMsQ0FBQztBQUNKLENBQUM7QUFYRCx3REFXQztBQU1EO0lBSkE7UUFNVSxVQUFLLEdBQUcsa0JBQVUsQ0FBQyxhQUFhLENBQUM7SUFnQjNDLENBQUM7d0NBbEJZLCtCQUErQjtJQUkxQyxxREFBVyxHQUFYLFVBQVksT0FBc0I7UUFDaEMsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3hDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDWCxJQUFNLEdBQUcsR0FBb0IsTUFBTSxDQUFDLFlBQVksQ0FBQztZQUNqRCxJQUFNLEVBQUUsR0FBRyxHQUFHLFlBQVksTUFBTSxHQUFHLEdBQUcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLEtBQUssR0FBRyxzQkFBc0IsRUFBRSxDQUFDO1FBQ3hDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxLQUFLLEdBQUcsa0JBQVUsQ0FBQyxhQUFhLENBQUM7UUFDeEMsQ0FBQztJQUNILENBQUM7SUFFRCxrREFBUSxHQUFSLFVBQVMsT0FBd0I7UUFDL0IsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQWhCUTtRQUFSLFlBQUssRUFBRTs7MEVBQXVCO0lBRHBCLCtCQUErQjtRQUozQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSxxQkFBYSxFQUFFLFdBQVcsRUFBRSxpQ0FBK0IsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUM7U0FDakcsQ0FBQztPQUNXLCtCQUErQixDQWtCM0M7SUFBRCxzQ0FBQzs7Q0FsQkQsQUFrQkMsSUFBQTtBQWxCWSwwRUFBK0I7QUFzQjVDOzs7O0VBSUUiLCJmaWxlIjoiYXBwL3V0aWxzL251bWJlci12YWxpZGF0b3IuZGlyZWN0aXZlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBYnN0cmFjdENvbnRyb2wsIE5HX1ZBTElEQVRPUlMsIFZhbGlkYXRvciwgVmFsaWRhdG9yRm4sIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbi8qKiBBIGhlcm8ncyBuYW1lIGNhbid0IG1hdGNoIHRoZSBnaXZlbiByZWd1bGFyIGV4cHJlc3Npb24gKi9cbmV4cG9ydCBmdW5jdGlvbiBpbnZhbGlkTnVtYmVyVmFsaWRhdG9yKCk6IFZhbGlkYXRvckZuIHtcbiAgcmV0dXJuIChjb250cm9sOiBBYnN0cmFjdENvbnRyb2wpOiB7W2tleTogc3RyaW5nXTogYW55fSA9PiB7XG5cbiAgICBjb25zdCBuYW1lUmUgPSBuZXcgUmVnRXhwKC9eWzAtOV0rJC9pKTtcbiAgICBjb25zdCBuYW1lID0gY29udHJvbC52YWx1ZTtcbiAgICBpZiAobmFtZSA9PSAnJykge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIGNvbnN0IG5vID0gIW5hbWVSZS50ZXN0KG5hbWUpO1xuICAgIHJldHVybiBubyA/IHsnaW52YWxpZE51bWJlcic6IHtuYW1lfX0gOiBudWxsO1xuICB9O1xufVxuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbaW52YWxpZE51bWJlcl0nLFxuICBwcm92aWRlcnM6IFt7cHJvdmlkZTogTkdfVkFMSURBVE9SUywgdXNlRXhpc3Rpbmc6IEludmFsaWROdW1iZXJWYWxpZGF0b3JEaXJlY3RpdmUsIG11bHRpOiB0cnVlfV1cbn0pXG5leHBvcnQgY2xhc3MgSW52YWxpZE51bWJlclZhbGlkYXRvckRpcmVjdGl2ZSBpbXBsZW1lbnRzIFZhbGlkYXRvciwgT25DaGFuZ2VzIHtcbiAgQElucHV0KCkgaW52YWxpZE51bWJlcjogc3RyaW5nO1xuICBwcml2YXRlIHZhbEZuID0gVmFsaWRhdG9ycy5udWxsVmFsaWRhdG9yO1xuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICBjb25zdCBjaGFuZ2UgPSBjaGFuZ2VzWydpbnZhbGlkTnVtYmVyJ107XG4gICAgaWYgKGNoYW5nZSkge1xuICAgICAgY29uc3QgdmFsOiBzdHJpbmcgfCBSZWdFeHAgPSBjaGFuZ2UuY3VycmVudFZhbHVlO1xuICAgICAgY29uc3QgcmUgPSB2YWwgaW5zdGFuY2VvZiBSZWdFeHAgPyB2YWwgOiBuZXcgUmVnRXhwKHZhbCwgJ2knKTtcbiAgICAgIHRoaXMudmFsRm4gPSBpbnZhbGlkTnVtYmVyVmFsaWRhdG9yKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMudmFsRm4gPSBWYWxpZGF0b3JzLm51bGxWYWxpZGF0b3I7XG4gICAgfVxuICB9XG5cbiAgdmFsaWRhdGUoY29udHJvbDogQWJzdHJhY3RDb250cm9sKToge1trZXk6IHN0cmluZ106IGFueX0ge1xuICAgIHJldHVybiB0aGlzLnZhbEZuKGNvbnRyb2wpO1xuICB9XG59XG5cblxuXG4vKlxuQ29weXJpZ2h0IDIwMTcgR29vZ2xlIEluYy4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cblVzZSBvZiB0aGlzIHNvdXJjZSBjb2RlIGlzIGdvdmVybmVkIGJ5IGFuIE1JVC1zdHlsZSBsaWNlbnNlIHRoYXRcbmNhbiBiZSBmb3VuZCBpbiB0aGUgTElDRU5TRSBmaWxlIGF0IGh0dHA6Ly9hbmd1bGFyLmlvL2xpY2Vuc2VcbiovIl19
