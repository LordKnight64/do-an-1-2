"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
/** A hero's name can't match the given regular expression */
function invalidDecimalValidator() {
    return function (control) {
        var nameRe = new RegExp(/^[0-9]+(\.[0-9]{1,4})?$/);
        var name = control.value;
        if (name == '') {
            return null;
        }
        var no = !nameRe.test(name);
        return no ? { 'invalidDecimal': { name: name } } : null;
    };
}
exports.invalidDecimalValidator = invalidDecimalValidator;
var InvalidNumberValidatorDirective = (function () {
    function InvalidNumberValidatorDirective() {
        this.valFn = forms_1.Validators.nullValidator;
    }
    InvalidNumberValidatorDirective_1 = InvalidNumberValidatorDirective;
    InvalidNumberValidatorDirective.prototype.ngOnChanges = function (changes) {
        var change = changes['invalidDecimal'];
        if (change) {
            var val = change.currentValue;
            var re = val instanceof RegExp ? val : new RegExp(val, 'i');
            this.valFn = invalidDecimalValidator();
        }
        else {
            this.valFn = forms_1.Validators.nullValidator;
        }
    };
    InvalidNumberValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], InvalidNumberValidatorDirective.prototype, "invalidNumber", void 0);
    InvalidNumberValidatorDirective = InvalidNumberValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[invalidDecimal]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: InvalidNumberValidatorDirective_1, multi: true }]
        })
    ], InvalidNumberValidatorDirective);
    return InvalidNumberValidatorDirective;
    var InvalidNumberValidatorDirective_1;
}());
exports.InvalidNumberValidatorDirective = InvalidNumberValidatorDirective;
/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/ 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC91dGlscy9kZWNpbWFsLXZhbGlkYXRvci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkU7QUFDM0Usd0NBQW9HO0FBRXBHLDZEQUE2RDtBQUM3RDtJQUNFLE1BQU0sQ0FBQyxVQUFDLE9BQXdCO1FBRTlCLElBQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDckQsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUMzQixFQUFFLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNmLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBQ0QsSUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLE1BQU0sQ0FBQyxFQUFFLEdBQUcsRUFBQyxnQkFBZ0IsRUFBRSxFQUFDLElBQUksTUFBQSxFQUFDLEVBQUMsR0FBRyxJQUFJLENBQUM7SUFDaEQsQ0FBQyxDQUFDO0FBQ0osQ0FBQztBQVhELDBEQVdDO0FBTUQ7SUFKQTtRQU1VLFVBQUssR0FBRyxrQkFBVSxDQUFDLGFBQWEsQ0FBQztJQWdCM0MsQ0FBQzt3Q0FsQlksK0JBQStCO0lBSTFDLHFEQUFXLEdBQVgsVUFBWSxPQUFzQjtRQUNoQyxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6QyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ1gsSUFBTSxHQUFHLEdBQW9CLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDakQsSUFBTSxFQUFFLEdBQUcsR0FBRyxZQUFZLE1BQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxLQUFLLEdBQUcsdUJBQXVCLEVBQUUsQ0FBQztRQUN6QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLGtCQUFVLENBQUMsYUFBYSxDQUFDO1FBQ3hDLENBQUM7SUFDSCxDQUFDO0lBRUQsa0RBQVEsR0FBUixVQUFTLE9BQXdCO1FBQy9CLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFoQlE7UUFBUixZQUFLLEVBQUU7OzBFQUF1QjtJQURwQiwrQkFBK0I7UUFKM0MsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsU0FBUyxFQUFFLENBQUMsRUFBQyxPQUFPLEVBQUUscUJBQWEsRUFBRSxXQUFXLEVBQUUsaUNBQStCLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDO1NBQ2pHLENBQUM7T0FDVywrQkFBK0IsQ0FrQjNDO0lBQUQsc0NBQUM7O0NBbEJELEFBa0JDLElBQUE7QUFsQlksMEVBQStCO0FBc0I1Qzs7OztFQUlFIiwiZmlsZSI6ImFwcC91dGlscy9kZWNpbWFsLXZhbGlkYXRvci5kaXJlY3RpdmUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCwgTkdfVkFMSURBVE9SUywgVmFsaWRhdG9yLCBWYWxpZGF0b3JGbiwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuLyoqIEEgaGVybydzIG5hbWUgY2FuJ3QgbWF0Y2ggdGhlIGdpdmVuIHJlZ3VsYXIgZXhwcmVzc2lvbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGludmFsaWREZWNpbWFsVmFsaWRhdG9yKCk6IFZhbGlkYXRvckZuIHtcbiAgcmV0dXJuIChjb250cm9sOiBBYnN0cmFjdENvbnRyb2wpOiB7W2tleTogc3RyaW5nXTogYW55fSA9PiB7XG5cbiAgICBjb25zdCBuYW1lUmUgPSBuZXcgUmVnRXhwKC9eWzAtOV0rKFxcLlswLTldezEsNH0pPyQvKTtcbiAgICBjb25zdCBuYW1lID0gY29udHJvbC52YWx1ZTtcbiAgICBpZiAobmFtZSA9PSAnJykge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIGNvbnN0IG5vID0gIW5hbWVSZS50ZXN0KG5hbWUpO1xuICAgIHJldHVybiBubyA/IHsnaW52YWxpZERlY2ltYWwnOiB7bmFtZX19IDogbnVsbDtcbiAgfTtcbn1cblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2ludmFsaWREZWNpbWFsXScsXG4gIHByb3ZpZGVyczogW3twcm92aWRlOiBOR19WQUxJREFUT1JTLCB1c2VFeGlzdGluZzogSW52YWxpZE51bWJlclZhbGlkYXRvckRpcmVjdGl2ZSwgbXVsdGk6IHRydWV9XVxufSlcbmV4cG9ydCBjbGFzcyBJbnZhbGlkTnVtYmVyVmFsaWRhdG9yRGlyZWN0aXZlIGltcGxlbWVudHMgVmFsaWRhdG9yLCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBpbnZhbGlkTnVtYmVyOiBzdHJpbmc7XG4gIHByaXZhdGUgdmFsRm4gPSBWYWxpZGF0b3JzLm51bGxWYWxpZGF0b3I7XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGNvbnN0IGNoYW5nZSA9IGNoYW5nZXNbJ2ludmFsaWREZWNpbWFsJ107XG4gICAgaWYgKGNoYW5nZSkge1xuICAgICAgY29uc3QgdmFsOiBzdHJpbmcgfCBSZWdFeHAgPSBjaGFuZ2UuY3VycmVudFZhbHVlO1xuICAgICAgY29uc3QgcmUgPSB2YWwgaW5zdGFuY2VvZiBSZWdFeHAgPyB2YWwgOiBuZXcgUmVnRXhwKHZhbCwgJ2knKTtcbiAgICAgIHRoaXMudmFsRm4gPSBpbnZhbGlkRGVjaW1hbFZhbGlkYXRvcigpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnZhbEZuID0gVmFsaWRhdG9ycy5udWxsVmFsaWRhdG9yO1xuICAgIH1cbiAgfVxuXG4gIHZhbGlkYXRlKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCk6IHtba2V5OiBzdHJpbmddOiBhbnl9IHtcbiAgICByZXR1cm4gdGhpcy52YWxGbihjb250cm9sKTtcbiAgfVxufVxuXG5cblxuLypcbkNvcHlyaWdodCAyMDE3IEdvb2dsZSBJbmMuIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG5Vc2Ugb2YgdGhpcyBzb3VyY2UgY29kZSBpcyBnb3Zlcm5lZCBieSBhbiBNSVQtc3R5bGUgbGljZW5zZSB0aGF0XG5jYW4gYmUgZm91bmQgaW4gdGhlIExJQ0VOU0UgZmlsZSBhdCBodHRwOi8vYW5ndWxhci5pby9saWNlbnNlXG4qLyJdfQ==
