// import {AppComponent} from 'app/app.component';
// import {Ng2StateDeclaration, Transition} from 'ui-router-ng2';
// import { LayoutsAuthComponent } from './components/layouts/auth/auth';
// import { HomeComponent } from './components/home/home.component';
// import { ClientComponent } from './components/client/client.component';
// import { HeroesComponent }      from './components/heroes/heroes.component';
// import { LoginComponent } from './components/login/login.component';
// /** The top level state(s) */
// export let MAIN_STATES: Ng2StateDeclaration[] = [
//     // The top-level app state.
//     // This state fills the root <ui-view></ui-view> (defined in index.html) with the AppComponent
//     {
// 		name: 'app',
// 		url: '/',
// 		component: LayoutsAuthComponent
// 	},
//     {
// 	 	name: 'app.home',
// 	 	url: '/home',
// 	 	data: {
// 	        auth: true
// 	      },
// 	 	component: HomeComponent
// 	 },
//     {
// 	 	name: 'app.client',
// 	 	url: '/client',
// 	 	data: {
// 	        auth: true
// 	      },
// 	 	component: ClientComponent
// 	 },
//     {
// 	 	name: 'app.heroes',
// 	 	data: {
// 	        auth: true
// 	      },
// 	 	url: '/heroes',
// 	 	component: HeroesComponent
// 	 },
// 	 {
// 	 	name: 'login',
// 	 	url: '/login',
// 	 	component: LoginComponent
// 	 }
// ]; 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAuc3RhdGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGtEQUFrRDtBQUNsRCxpRUFBaUU7QUFDakUseUVBQXlFO0FBQ3pFLG9FQUFvRTtBQUNwRSwwRUFBMEU7QUFDMUUsK0VBQStFO0FBQy9FLHVFQUF1RTtBQUV2RSxnQ0FBZ0M7QUFDaEMsb0RBQW9EO0FBQ3BELGtDQUFrQztBQUNsQyxxR0FBcUc7QUFDckcsUUFBUTtBQUNSLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2Qsb0NBQW9DO0FBQ3BDLE1BQU07QUFDTixRQUFRO0FBQ1IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLFlBQVk7QUFDWiw4QkFBOEI7QUFDOUIsT0FBTztBQUNQLFFBQVE7QUFDUix5QkFBeUI7QUFDekIscUJBQXFCO0FBQ3JCLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsWUFBWTtBQUNaLGdDQUFnQztBQUNoQyxPQUFPO0FBQ1AsUUFBUTtBQUNSLHlCQUF5QjtBQUN6QixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLFlBQVk7QUFDWixxQkFBcUI7QUFDckIsZ0NBQWdDO0FBQ2hDLE9BQU87QUFDUCxNQUFNO0FBQ04sb0JBQW9CO0FBQ3BCLG9CQUFvQjtBQUNwQiwrQkFBK0I7QUFDL0IsTUFBTTtBQUNOLEtBQUsiLCJmaWxlIjoiYXBwL2FwcC5zdGF0ZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQge0FwcENvbXBvbmVudH0gZnJvbSAnYXBwL2FwcC5jb21wb25lbnQnO1xyXG4vLyBpbXBvcnQge05nMlN0YXRlRGVjbGFyYXRpb24sIFRyYW5zaXRpb259IGZyb20gJ3VpLXJvdXRlci1uZzInO1xyXG4vLyBpbXBvcnQgeyBMYXlvdXRzQXV0aENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9sYXlvdXRzL2F1dGgvYXV0aCc7XHJcbi8vIGltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudCc7XHJcbi8vIGltcG9ydCB7IENsaWVudENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jbGllbnQvY2xpZW50LmNvbXBvbmVudCc7XHJcbi8vIGltcG9ydCB7IEhlcm9lc0NvbXBvbmVudCB9ICAgICAgZnJvbSAnLi9jb21wb25lbnRzL2hlcm9lcy9oZXJvZXMuY29tcG9uZW50JztcclxuLy8gaW1wb3J0IHsgTG9naW5Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50JztcclxuXHJcbi8vIC8qKiBUaGUgdG9wIGxldmVsIHN0YXRlKHMpICovXHJcbi8vIGV4cG9ydCBsZXQgTUFJTl9TVEFURVM6IE5nMlN0YXRlRGVjbGFyYXRpb25bXSA9IFtcclxuLy8gICAgIC8vIFRoZSB0b3AtbGV2ZWwgYXBwIHN0YXRlLlxyXG4vLyAgICAgLy8gVGhpcyBzdGF0ZSBmaWxscyB0aGUgcm9vdCA8dWktdmlldz48L3VpLXZpZXc+IChkZWZpbmVkIGluIGluZGV4Lmh0bWwpIHdpdGggdGhlIEFwcENvbXBvbmVudFxyXG4vLyAgICAge1xyXG4vLyBcdFx0bmFtZTogJ2FwcCcsXHJcbi8vIFx0XHR1cmw6ICcvJyxcclxuLy8gXHRcdGNvbXBvbmVudDogTGF5b3V0c0F1dGhDb21wb25lbnRcclxuLy8gXHR9LFxyXG4vLyAgICAge1xyXG4vLyBcdCBcdG5hbWU6ICdhcHAuaG9tZScsXHJcbi8vIFx0IFx0dXJsOiAnL2hvbWUnLFxyXG4vLyBcdCBcdGRhdGE6IHtcclxuLy8gXHQgICAgICAgIGF1dGg6IHRydWVcclxuLy8gXHQgICAgICB9LFxyXG4vLyBcdCBcdGNvbXBvbmVudDogSG9tZUNvbXBvbmVudFxyXG4vLyBcdCB9LFxyXG4vLyAgICAge1xyXG4vLyBcdCBcdG5hbWU6ICdhcHAuY2xpZW50JyxcclxuLy8gXHQgXHR1cmw6ICcvY2xpZW50JyxcclxuLy8gXHQgXHRkYXRhOiB7XHJcbi8vIFx0ICAgICAgICBhdXRoOiB0cnVlXHJcbi8vIFx0ICAgICAgfSxcclxuLy8gXHQgXHRjb21wb25lbnQ6IENsaWVudENvbXBvbmVudFxyXG4vLyBcdCB9LFxyXG4vLyAgICAge1xyXG4vLyBcdCBcdG5hbWU6ICdhcHAuaGVyb2VzJyxcclxuLy8gXHQgXHRkYXRhOiB7XHJcbi8vIFx0ICAgICAgICBhdXRoOiB0cnVlXHJcbi8vIFx0ICAgICAgfSxcclxuLy8gXHQgXHR1cmw6ICcvaGVyb2VzJyxcclxuLy8gXHQgXHRjb21wb25lbnQ6IEhlcm9lc0NvbXBvbmVudFxyXG4vLyBcdCB9LFxyXG4vLyBcdCB7XHJcbi8vIFx0IFx0bmFtZTogJ2xvZ2luJyxcclxuLy8gXHQgXHR1cmw6ICcvbG9naW4nLFxyXG4vLyBcdCBcdGNvbXBvbmVudDogTG9naW5Db21wb25lbnRcclxuLy8gXHQgfVxyXG4vLyBdOyJdfQ==
