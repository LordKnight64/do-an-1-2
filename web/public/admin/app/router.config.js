// import {UIRouter, Transition, StateService} from "ui-router-ng2";
// import {Injectable} from "@angular/core";
// declare var SystemJS;
// import { GuardService } from './services/guard.service';
// /**
//  * Create your own configuration class (if necessary) for any root/feature/lazy module.
//  *
//  * Pass it to the UIRouterModule.forRoot/forChild factory methods as `configClass`.
//  *
//  * The class will be added to the Injector and instantiate when the module loads.
//  */
// @Injectable()
// export class MyRootUIRouterConfig {
//   /** You may inject dependencies into the constructor */
//   constructor(uiRouter: UIRouter, guardServ: GuardService, stateServ: StateService) {
//     // Show the ui-router visualizer
//     // let vis = window['ui-router-visualizer'];
//     // vis.visualizer(uiRouter);
//     // let criteria = { to: (state) => state.data && state.data.auth };
//     // uiRouter.transitionService.onBefore(criteria, this.requireAuthentication);
//     uiRouter.transitionService.onBefore({to: 'app.**'}, (transition: Transition) => {
//   	    // let auth = transition.injector().get(guardServ);
//         // console.log('------guardServ.isLoggedIn()-------', guardServ.isLoggedIn());
//   	    if (!guardServ.isLoggedIn()) {
//   	        return transition.router.stateService.target('login');
//             // return stateService
//   	    }
//   	    return true;
//   	});
//   }
// } 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9yb3V0ZXIuY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9FQUFvRTtBQUNwRSw0Q0FBNEM7QUFDNUMsd0JBQXdCO0FBQ3hCLDJEQUEyRDtBQUUzRCxNQUFNO0FBQ04sMEZBQTBGO0FBQzFGLEtBQUs7QUFDTCxzRkFBc0Y7QUFDdEYsS0FBSztBQUNMLG9GQUFvRjtBQUNwRixNQUFNO0FBQ04sZ0JBQWdCO0FBQ2hCLHNDQUFzQztBQUN0Qyw0REFBNEQ7QUFDNUQsd0ZBQXdGO0FBQ3hGLHVDQUF1QztBQUN2QyxtREFBbUQ7QUFDbkQsbUNBQW1DO0FBRW5DLDBFQUEwRTtBQUMxRSxvRkFBb0Y7QUFFcEYsd0ZBQXdGO0FBQ3hGLDZEQUE2RDtBQUM3RCx5RkFBeUY7QUFDekYsd0NBQXdDO0FBQ3hDLG9FQUFvRTtBQUNwRSxxQ0FBcUM7QUFDckMsV0FBVztBQUNYLHNCQUFzQjtBQUV0QixTQUFTO0FBQ1QsTUFBTTtBQUNOLElBQUkiLCJmaWxlIjoiYXBwL3JvdXRlci5jb25maWcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQge1VJUm91dGVyLCBUcmFuc2l0aW9uLCBTdGF0ZVNlcnZpY2V9IGZyb20gXCJ1aS1yb3V0ZXItbmcyXCI7XHJcbi8vIGltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuLy8gZGVjbGFyZSB2YXIgU3lzdGVtSlM7XHJcbi8vIGltcG9ydCB7IEd1YXJkU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZ3VhcmQuc2VydmljZSc7XHJcblxyXG4vLyAvKipcclxuLy8gICogQ3JlYXRlIHlvdXIgb3duIGNvbmZpZ3VyYXRpb24gY2xhc3MgKGlmIG5lY2Vzc2FyeSkgZm9yIGFueSByb290L2ZlYXR1cmUvbGF6eSBtb2R1bGUuXHJcbi8vICAqXHJcbi8vICAqIFBhc3MgaXQgdG8gdGhlIFVJUm91dGVyTW9kdWxlLmZvclJvb3QvZm9yQ2hpbGQgZmFjdG9yeSBtZXRob2RzIGFzIGBjb25maWdDbGFzc2AuXHJcbi8vICAqXHJcbi8vICAqIFRoZSBjbGFzcyB3aWxsIGJlIGFkZGVkIHRvIHRoZSBJbmplY3RvciBhbmQgaW5zdGFudGlhdGUgd2hlbiB0aGUgbW9kdWxlIGxvYWRzLlxyXG4vLyAgKi9cclxuLy8gQEluamVjdGFibGUoKVxyXG4vLyBleHBvcnQgY2xhc3MgTXlSb290VUlSb3V0ZXJDb25maWcge1xyXG4vLyAgIC8qKiBZb3UgbWF5IGluamVjdCBkZXBlbmRlbmNpZXMgaW50byB0aGUgY29uc3RydWN0b3IgKi9cclxuLy8gICBjb25zdHJ1Y3Rvcih1aVJvdXRlcjogVUlSb3V0ZXIsIGd1YXJkU2VydjogR3VhcmRTZXJ2aWNlLCBzdGF0ZVNlcnY6IFN0YXRlU2VydmljZSkge1xyXG4vLyAgICAgLy8gU2hvdyB0aGUgdWktcm91dGVyIHZpc3VhbGl6ZXJcclxuLy8gICAgIC8vIGxldCB2aXMgPSB3aW5kb3dbJ3VpLXJvdXRlci12aXN1YWxpemVyJ107XHJcbi8vICAgICAvLyB2aXMudmlzdWFsaXplcih1aVJvdXRlcik7XHJcblxyXG4vLyAgICAgLy8gbGV0IGNyaXRlcmlhID0geyB0bzogKHN0YXRlKSA9PiBzdGF0ZS5kYXRhICYmIHN0YXRlLmRhdGEuYXV0aCB9O1xyXG4vLyAgICAgLy8gdWlSb3V0ZXIudHJhbnNpdGlvblNlcnZpY2Uub25CZWZvcmUoY3JpdGVyaWEsIHRoaXMucmVxdWlyZUF1dGhlbnRpY2F0aW9uKTtcclxuXHRcclxuLy8gICAgIHVpUm91dGVyLnRyYW5zaXRpb25TZXJ2aWNlLm9uQmVmb3JlKHt0bzogJ2FwcC4qKid9LCAodHJhbnNpdGlvbjogVHJhbnNpdGlvbikgPT4ge1xyXG4vLyAgIFx0ICAgIC8vIGxldCBhdXRoID0gdHJhbnNpdGlvbi5pbmplY3RvcigpLmdldChndWFyZFNlcnYpO1xyXG4vLyAgICAgICAgIC8vIGNvbnNvbGUubG9nKCctLS0tLS1ndWFyZFNlcnYuaXNMb2dnZWRJbigpLS0tLS0tLScsIGd1YXJkU2Vydi5pc0xvZ2dlZEluKCkpO1xyXG4vLyAgIFx0ICAgIGlmICghZ3VhcmRTZXJ2LmlzTG9nZ2VkSW4oKSkge1xyXG4vLyAgIFx0ICAgICAgICByZXR1cm4gdHJhbnNpdGlvbi5yb3V0ZXIuc3RhdGVTZXJ2aWNlLnRhcmdldCgnbG9naW4nKTtcclxuLy8gICAgICAgICAgICAgLy8gcmV0dXJuIHN0YXRlU2VydmljZVxyXG4vLyAgIFx0ICAgIH1cclxuLy8gICBcdCAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBcclxuLy8gICBcdH0pO1xyXG4vLyAgIH1cclxuLy8gfSJdfQ==
