/*
 *
 *  Push Notifications codelab
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

/* eslint-env browser, es6 */

'use strict';

const applicationServerPublicKey = 'BI3iKGUTs-t5MO5YOm1iPCGXwLKSDo-5jIaMnj0W15tLlsjd6Ln2aayVFuCrraDy-A8n_3Mn9P6qm_J2WQXbz5M';

// const pushButton = document.querySelector('.js-push-btn');

let isSubscribed = false;
let swRegistration = null;
var userIdInsert = null;
var url = null;
var myExtObject = (function() {

    return {
        func1: function(userId, urlSend) {
            userIdInsert = userId;
            url = urlSend;
            PushManagerServiceWorker();
        },
        func2: function() {
            alert('function 2 called');
        }
    }

})(myExtObject || {})

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function PushManagerServiceWorker() {
    console.log('Service Worker and Push is supported');

    navigator.serviceWorker.register('../sw.js')
        .then(function(swReg) {
            console.log('Service Worker is registered', swReg);

            swRegistration = swReg;
            initialiseUI();
        })
        .catch(function(error) {
            console.error('Service Worker Error', error);
        });
}

function initialiseUI() {

    //da subscribed && user dang nhap co trong table mst_token_devices

    if (isSubscribed) {
        // unsubscribeUser();
    } else {
        subscribeUser();
    }

    var userId = userIdInsert;
    console.log('User id', userId);
    swRegistration.pushManager.getSubscription()
        .then(function(subscription) {
            isSubscribed = !(subscription === null);

            if (isSubscribed) {
                console.log('User IS subscribed.');
            } else {
                console.log('User is NOT subscribed.');
                if (isSubscribed) {
                    unsubscribeUser();
                } else {
                    subscribeUser();
                }
            }

            // updateBtn();
        });
}

function updateBtn() {
    if (Notification.permission === 'denied') {
        pushButton.textContent = 'Push Messaging Blocked.';
        pushButton.disabled = true;
        updateSubscriptionOnServer(null);
        return;
    }
    if (isSubscribed) {
        pushButton.textContent = 'Disable Push Messaging';
    } else {
        pushButton.textContent = 'Enable Push Messaging';
    }

    pushButton.disabled = false;
}

function subscribeUser() {
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey
        })
        .then(function(subscription) {
            console.log('User is subscribed.');

            updateSubscriptionOnServer(subscription);

            isSubscribed = true;
            // updateBtn();
        })
        .catch(function(err) {
            console.log('Failed to subscribe the user: ', err);
            // updateBtn();
        });
}

function updateSubscriptionOnServer(subscription) {
    // TODO: Send subscription to application server

    if (subscription) {
        // subscriptionJson.textContent = JSON.stringify(subscription);
        var objectParam = JSON.parse(JSON.stringify(subscription));
        //INSERT SERVER
        var param = {
                'endpoint': objectParam.endpoint,
                'keys': objectParam.keys,
                'payload': 'Hello!',
                'userId': userIdInsert,
                'optionUrl': 'admin'
            }
            //----------------------------------------
            // var url = 'http://api.mysite.local:8000/store_endpoint'
        url = url + 'store_endpoint';
        var httpParams = {
            method: 'POST',
            url: url,
            data: param
        };
        $.ajax(httpParams).success(function(data, status, headers) {
            alert('POST SUSCESS');
        }).error(function(data, status, headers) {
            alert('POST ERROR');
        });
    } else {

    }
}

function unsubscribeUser() {
    swRegistration.pushManager.getSubscription()
        .then(function(subscription) {
            if (subscription) {
                // TODO: Tell application server to delete subscription
                return subscription.unsubscribe();
            }
        })
        .catch(function(error) {
            console.log('Error unsubscribing', error);
        })
        .then(function() {
            updateSubscriptionOnServer(null);

            console.log('User is unsubscribed.');
            isSubscribed = false;
            // updateBtn();
        });
}