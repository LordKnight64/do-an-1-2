<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Thi Thuy An
 *
 */ 
class MstRestaurantReviews extends BaseModel {
	protected $table = "mst_restaurant_reviews";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	 protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** String  */
        "restaurant_id",
        /** Integer  */
        "item_id",
          /** String  */
        "active_flg",
          /** String  */
        "user_id",
          /** String  */
        "title",
          /** String  */
        "rating",
          /** Date  */
        "content"
      
        
       
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}