<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Thi Thuy An
 *
 */ 
class MstTurnoverPlaning extends BaseModel {
	protected $table = "mst_turnover_planing";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	 protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Integer  */
        "year_no",
         /** Integer  */
        "restaurant_id",
         /** String  */
        "turn_over_planing",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}