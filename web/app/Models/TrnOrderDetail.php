<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class TrnOrderDetail extends BaseModel {
	protected $table = "trn_order_detail";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** Long  */
        "order_id",
        /** Long  */
        "item_id",
        /** Integer  */
        "seq_no",
        /** Long  */
        "option_value_id",
        /** BigDecimal  */
        "price",
        /** String  */
        "notes",
        "point",
        "seq_no"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}