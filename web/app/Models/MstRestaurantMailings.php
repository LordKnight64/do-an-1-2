<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Thi Thuy An
 *
 */ 
class MstRestaurantMailings extends BaseModel {
	protected $table = "mst_restaurant_mailings";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	 protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** String  */
        "restaurant_id",
        /** Integer  */
        "subject",
          /** String  */
        "send_to",
          /** String  */
        "send_cc",
          /** String  */
        "send_bb",
          /** String  */
        "content",
          /** Date  */
        "start_date",
          /** Date  */
        "end_date",
          /** Time  */
        "start_time",
          /** Time  */
        "end_time"
        
       
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}