<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstItemOptionValue extends BaseModel {
	protected $table = "mst_item_option_value";

    //protected $primaryKey = 'option_id';
    //protected $primaryKey = 'item_id';
    //protected $primaryKey = 'option_value_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "option_id",
        /** Long  */
        "item_id",
        /** Long  */
        "option_value_id",
        /** String  */
        "active_flg",
        /** BigDecimal  */
        "add_price",
         /** Long  */
        "point",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}