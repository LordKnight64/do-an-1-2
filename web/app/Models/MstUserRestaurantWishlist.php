<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstUserRestaurantWishlist extends BaseModel {
	protected $table = "mst_user_restaurant_wishlist";

    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "restaurant_id",
        /** String  */
        "item_id",
        /** String  */
        "active_flg",
        /** BigDecimal  */
        "user_id",
        /** BigDecimal  */
        "del_flg",
        "cre_ts",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}