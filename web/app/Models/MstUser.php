<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use App\Notifications\UserResetPasswordNotification;
use Session;
use Log;
/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstUser extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'cre_ts';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'mod_ts';

    protected $table = "mst_user";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
        return redirect()->route('user.password.request');
    }
}
