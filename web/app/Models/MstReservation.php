<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Thi Thuy An
 *
 */ 
class MstReservation extends BaseModel {
	protected $table = "mst_reservation";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	 protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** String  */
        "name",
        /** Integer  */
        "adults",
          /** String  */
        "name",
          /** String  */
        "email",
          /** String  */
        "phone",
          /** String  */
        "comment",
          /** Date  */
        "reserved_date",
        
          /** Time  */
        "start_time",
          /** Time  */
        "end_time"
        
       
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}