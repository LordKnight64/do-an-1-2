<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Thi Thuy An
 *
 */ 
class MstArea extends BaseModel {
	protected $table = "mst_area";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	 protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** String  */
        "name",
         /** Integer  */
        "restaurant_id",
          /** String  */
        "thumb"
       
        
       
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}