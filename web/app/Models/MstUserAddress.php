<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstUserAddress extends BaseModel {
	protected $table = "mst_user_address";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** String  */
        "name",
        /** String  */
        "address",
        /** BigDecimal  */
        "lat_val",
        /** BigDecimal  */
        "long_val",
        /** String  */
        "tel",
        /** Long  */
        "user_id",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}