<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstRestaurant extends BaseModel {
	protected $table = "mst_restaurant";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** String  */
        "name",
        /** String  */
        "address",
        /** String  */
        "tel",
        /** String  */
        "mobile",
        /** String  */
        "email",
        /** BigDecimal  */
        "lat_val",
        /** BigDecimal  */
        "long_val",
        /** String  */
        "website",
        /** String  */
        "logo_path",
        /** Integer  */
        "rating",
        /** Long  */
        "unit_point",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}