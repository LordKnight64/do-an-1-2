<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

use Laratrust\Traits\LaratrustRoleTrait;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstRole extends BaseModel {
    use LaratrustRoleTrait;
    
	protected $table = "mst_role";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Integer  */
        "id",
        /** String  */
        "name",
        /** String  */
        "display_name",
        /** String  */
        "description",
        /** LocalDateTime  */
        "created_at",
        /** LocalDateTime  */
        "updated_at"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}