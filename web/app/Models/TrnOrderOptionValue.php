<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class TrnOrderOptionValue extends BaseModel {
	protected $table = "trn_order_option_value";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** Long  */
        "order_id",
        /** Integer  */
        "seq_no",
        /** Long  */
        "item_id",
        /** Long  */
        "option_value_id",
        "point",
        /** Long */
        "option_id",
        /** String */
        "name",
        /** Integer */
        "seq_no"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}