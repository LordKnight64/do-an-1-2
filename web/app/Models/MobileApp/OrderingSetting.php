<?php

namespace App\Models\MobileApp;

use Illuminate\Database\Eloquent\Model;

class OrderingSetting {

	public $payment_fee;
	public $delivery_charge;
	public $minimum_order;
	public $payment_method;
	public $delivery_methods;
	public $delivery_time;
	public $default_currency;
	public $food_ordering_system_type;
}
