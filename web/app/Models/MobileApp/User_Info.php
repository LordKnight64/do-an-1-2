<?php

namespace App\Models\MobileApp;

use Illuminate\Database\Eloquent\Model;

class User_Info {

    public $id;
    public $mobile;
		public $email;
		public $last_name;
		public $first_name;
		public $tel;
		public $roles;
}
