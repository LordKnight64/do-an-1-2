<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class TrnNews extends BaseModel {
	protected $table = "trn_news";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** Long  */
        "restaurant_id",
        /** String  */
        "title",
        /** String  */
        "slug",
        /** String  */
        "content",
        /** String */
        "thumb",
        /** int */
        "restaurant_type"
         
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}