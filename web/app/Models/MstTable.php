<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Thi Thuy An
 *
 */ 
class MstTable extends BaseModel {
	protected $table = "mst_table";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	 protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** String  */
        "name",
         /** Integer  */
        "restaurant_id",
          /** String  */
        "content",
          /** Integer  */
        "is_active",
          /** Integer  */
        "axis_x",
          /** Integer  */
        "axis_y",
          /** String  */
        "thumb",
          /** Integer  */
        "slots"
        
       
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}