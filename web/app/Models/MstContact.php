<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 *
 * @author Dang Nhat Anh
 *
 */
class MstContact extends BaseModel {
	protected $table = "mst_contact";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** Long  */
        "name",
				/** String  */
        "email",
        /** String  */
        "content",
				/** String  */
        "phone",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
