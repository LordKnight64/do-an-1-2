<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstCd extends BaseModel {
	protected $table = "mst_cd";

    //protected $primaryKey = 'cd_group';
    //protected $primaryKey = 'cd';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** String  */
        "cd_group",
        /** String  */
        "cd",
        /** String  */
        "cd_name",
        /** String  */
        "cd_val",
        /** Integer  */
        "display_order",
        /** String  */
        "stop_flg",
        /** Color */
        "color"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}