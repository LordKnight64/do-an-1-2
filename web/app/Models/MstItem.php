<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstItem extends BaseModel {
	protected $table = "mst_item";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** Long  */
        "restaurant_id",
        /** String  */
        "name",
        /** String  */
        "sku",
        /** BigDecimal  */
        "price",
        /** LocalDate  */
        "start_date",
        /** String  */
        "active_flg",
        /** String  */
        "description",
          /** Long  */
        "point",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}