<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {

	/**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'cre_ts';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'mod_ts';

}
