<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class MstRestaurantTags extends BaseModel {
	protected $table = "mst_restaurant_tags";

    //protected $primaryKey = 'restaurant_id';
    //protected $primaryKey = 'tags_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "restaurant_id",
        /** Long  */
        "tags_id",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}