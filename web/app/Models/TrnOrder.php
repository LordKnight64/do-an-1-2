<?php
/**
 * Copyright(c) SystemEXE Co., Ltd. All Rights Reserved.
 */

namespace App\Models;

/**
 * 
 * @author Nguyen Phu Cuong
 *
 */ 
class TrnOrder extends BaseModel {
	protected $table = "trn_order";

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /** Long  */
        "id",
        /** Long  */
        "user_id",
        /** Long  */
        "address_id",
        /** Long  */
        "restaurant_id",
        /** LocalDateTime  */
        "order_ts",
        /** String 0: moi tao
        1: dang giao
        2: hoan tat
        99: huy bo */
        "order_sts",
        /** BigDecimal  */
        "price",
        /** String  */
        "notes",
        /** LocalDateTime  */
        "delivery_ts",
        /** BigDecimal  */
        "delivery_fee",
        "is_favorite",
        "used_point",
        "recieved_point"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}