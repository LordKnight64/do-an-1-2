<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use App\Models\MstSearch;
use App\Models\MstUser;

class TableReversationController extends Controller
{
    public function get_reversations(Request $request)
    {
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        try {
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }


    public function update_reversation_by_id(Request $request)
    {
        $param = $request->all();
        $user = $param['user'];
        $table = $param['table'];


        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        DB::table('mst_reservation')->insertGetId(
          [
            'name' => $user['name'],
            'email' => $user['email'],
            'address' => $user['address'],
            'phone' => $user['phone'],
            'comment' => $user['comment'],
            'reserved_date' => date('Y-m-d', strtotime($user['date'])),
            'start_time' => $user['open_time'],
            'end_time' => $user['close_time']
          ]
        );
        try {
            DB::commit();
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}
