<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Services\PushNotificationService;


class PushNotificationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PushNotificationService $pushNotificationService)
    {
        $this->pushNotificationService = $pushNotificationService;
    }

    public function get_all(Request $request) {
        $response = $this->pushNotificationService->get_all();
        
        return response()->json($response);
    }
    public function store(Request $request)
    {
        $param = $request->all();
        
        $response = $this->pushNotificationService->store($param);
        
        return response()->json($response);
    }
    public function push_notification(Request $request) {
        $param = $request->all();
        $response = $this->pushNotificationService->push($param);
        return response()->json($response);
    }
    
}