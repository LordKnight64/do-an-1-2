<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstTable;
use App\Models\MstUser;

class TableController extends Controller
{
    public function get_table_list(Request $request)
    {
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        $param = $request->all();

        $area_id = $param['area_id'];
        $rest_id = $param['rest_id'];
        try {
            if ($area_id == null || $area_id =='') {
                $tables = MstTable::where('del_flg', '0')
                            ->where('restaurant_id', $rest_id)
                            ->select('id', 'name', 'content', 'is_active', 'axis_x', 'axis_y', 'thumb', 'area_id', 'slots')
                            ->get();
            } else {
                $tables = MstTable::where('del_flg', '0')
                            ->where('area_id', $area_id)
                            ->where('restaurant_id', $rest_id)
                            ->select('id', 'name', 'content', 'is_active', 'axis_x', 'axis_y', 'thumb', 'area_id', 'slots')
                            ->get();
            }
            $res['tables'] = $tables;
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

    public function update_table_by_id(Request $request)
    {
        Log::info('--------update_table_by_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $table_param = $param['table'];
        $validator = $this->validateObject($table_param, [
            'name'    => 'required|max:256',
        ]);

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }

        $res['return_cd'] = 0;
        $res['message'] = 'OK';


        DB::beginTransaction();
        try {
            $id = $table_param['id'];
            if ($id == -1) {
                $table =  new MstTable;
                $table->restaurant_id = $rest_id;
                $table->name = $table_param['name'];
                $table->thumb = $table_param['thumb'];
                $table->content = $table_param['content'];
                $table->is_active = $table_param['is_active'];
                $table->cre_user_id = $user_id;
                $table->mod_user_id = $user_id;
                $table->save();
                $res['area_id'] = $table->id ;
            } else {
                $table = MstTable::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($table)) {
                    if ($table_param['is_delete'] != 1) {
                        $table->name = $table_param['name'];
                        if ($table_param['thumb']!=null || !empty($table_param['thumb'])) {
                            $table->thumb = $table_param['thumb'];
                        }
                        $table->content = $table_param['content'];
                        $table->is_active = $table_param['is_active'];
                        $table->del_flg = $table_param['is_delete'];
                    } else {
                        $table->del_flg = '1';
                        //DELETE
                    }
                    $table->mod_user_id = $user_id;
                    $table->save();
                    $res['table_id'] = $table->id ;
                }
            }
            DB::commit();
            $res['user_id'] = $param['user_id'];
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }

    public function delete_table_by_id(Request $request)
    {
        $param = $request->all();
        $id = $param['id'];


        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {
            MstTable::where('id', $id)->delete();
            DB::commit();
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }


    // public function delete_area_by_id(Request $request)
    // {
    //     $param = $request->all();
    //     $id = $param['id'];
    //
    //
    //     $res['return_cd'] = 0;
    //     $res['message'] = 'OK';
    //     DB::beginTransaction();
    //     try {
    //         $tables = MstTable::where('area_id', $area_id)
    //                         ->where('del_flg', '0')
    //                         ->select('id', 'name', 'content', 'is_active', 'axis_x', 'axis_y', 'thumb', 'area_id', 'slots')
    //                         ->get();
    //
    //         if (count($tables) != 0) {
    //             MstArea::where('id', $id)->delete();
    //             DB::commit();
    //         } else {
    //             $res['return_cd'] = 1;
    //             $res['message'] = 'There are tables in this area';
    //         }
    //     } catch (QueryException $ex) {
    //         $res['return_cd'] = 1;
    //         $res['message'] = $ex->getMessage();
    //         DB::rollBack();
    //     } catch (Exception $ex) {
    //         $res['return_cd'] = 1;
    //         $res['message'] = $ex->getMessage();
    //         DB::rollBack();
    //     }
    //     return response()->json($res);
    // }
}
