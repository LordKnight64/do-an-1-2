<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstCd;


class ComCodeController extends Controller
{
    public function get_code_by_cd_group(Request $request) {

        Log::info('--------get_code_by_cd_group---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $cd_group = $param['cd_group'];
        try {
            $code = MstCd::where('cd_group', $cd_group)
                            ->where('del_flg', '0')
                            ->select('cd', 'cd_name')
                            ->get();
            $res['code'] = $code;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }
}