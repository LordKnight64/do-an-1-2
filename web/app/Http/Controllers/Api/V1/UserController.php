<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Validator;
use Illuminate\Database\QueryException;
use App\Models\MstUser;
use App\Models\MstRoleUser;
use App\Models\MstRestaurant;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function update_profile(Request $request) {

        Log::info('--------update_profile---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $user_id = $param['user_id'];
        $user_info = $param['user_info'];
        $roles = $user_info['roles'];
        $restaurant_info = $param['restaurant_info'];
        $ordering_setting = $restaurant_info['ordering_setting'];
         Validator::extend('max_length', function($attribute, $value, $parameters, $validator){
            Log::debug('test1-------------');
            Log::debug($attribute);
            return strlen($value) <= $parameters[0];
        });
         $validator = $this->validateObject($user_info, [
            'first_name'    => 'required|max:256',
            'last_name'    => 'required|max:256',
            'tel'    => 'required|max:32',
            'phone'    => 'required|numeric|max_length:32',
            'password'    => 'required|min:6',
            'email'    => 'required|email|min:6',
            'confirm_password'    => 'required|min:6',
        ]); 
        $validator1 = $this->validateObject($restaurant_info, [
            'name'    => 'required|max:256',
            'address'    => 'required|max:256',
            'rest_type'    => 'required',
            'open_time'    => 'required',
            'close_time'    => 'required',
            'website'    => 'required|max:256',
        ]); 
        $validator2 = $this->validateObject($ordering_setting, [
            'default_currency'    => 'required',
        ]); 

         if ($validator->fails()||$validator1->fails()||$validator2->fails()) {
           $errors = $this->formatValidationErrors($validator);
           $errors1 = $this->formatValidationErrors($validator1);
           $errors2 = $this->formatValidationErrors($validator2);
           $merge = array_merge($errors, $errors1); 
           $mergeALL = array_merge($merge, $errors2); 

            return $this->buildFailedValidationResponse(
                $request,$mergeALL);
            // return response()->json($this->formatValidationErrors($validator));
        }
        if (isset($user_info['password']) && isset($user_info['confirm_password']) && $user_info['password']!=$user_info['confirm_password']) {
            return $this->buildFailedLoginResponse($request, ['Confirm password different password']);
        }
        DB::beginTransaction();
        try {
            $user = MstUser::find($user_id);
            if (!empty($user)) {
            	//update user info
            	$user->first_name = $user_info['first_name'];
            	$user->last_name = $user_info['last_name'];
                $user->tel = $user_info['tel'];
            	$user->mobile = $user_info['phone'];
                $user->password = Hash::make($user_info['password']);
                $user->mod_user_id = $user_id;
                $user->version_no = $user->version_no + 1;
                $user->save();
                //delete old roles
                // MstRoleUser::where('user_id', $user_id)->delete();
                // //insert new roles
                // foreach ($roles as $role) {
                //     $role_tmp = new MstRoleUser;
                //     $role_tmp->user_id = $user_id;
                //     $role_tmp->role_id = $role;
                //     $role_tmp->save();
                // }

                //Update restaurent
                $restaurant = MstRestaurant::where('id',$restaurant_info["id"])->where('del_flg','0')->first();
                $queries = DB::getQueryLog();
                            $last_query = end($queries);
                             Log::info($last_query);
                // $restaurant_count = count($restaurants);
                // if ($restaurant_count > 1) {
                //     $res['return_cd'] = 1;
                //     $res['message'] = 'Một người dùng có '.$restaurant_count.' nhà hàng';
                //     DB::rollBack();
                // } else  if ($restaurant_count < 1) {
                //     $res['return_cd'] = 1;
                //     $res['message'] = 'Không tìm thấy nhà hàng';
                //     DB::rollBack();
                // } else {
                    // $restaurant = MstRestaurant::find($restaurants[0]->id);
                    if (!empty($restaurant)) {
                        Log::info($restaurant_info);
                        $restaurant->name = $restaurant_info['name'];
                        $restaurant->address = $restaurant_info['address'];
                        $restaurant->tel = $restaurant_info['tel'];
                        $restaurant->mobile = $restaurant_info['mobile'];
                        $restaurant->email = $restaurant_info['email'];
                        $restaurant->lat_val = $restaurant_info['latitue'];
                        $restaurant->long_val = $restaurant_info['longtitue'];
                        $restaurant->website = $restaurant_info['website'];
                        // $restaurant->logo_path = $restaurant_info['logo'];
                        $restaurant->open_time = $restaurant_info['open_time'];
                        $restaurant->close_time = $restaurant_info['close_time'];
                        $restaurant->promote_code = $restaurant_info['promotion'];
                        $restaurant->facebook_link = $restaurant_info['facebook_link'];
                        $restaurant->twitter_link = $restaurant_info['twitter_link'];
                        $restaurant->linkedIn_link = $restaurant_info['linkedIn_link'];
                        $restaurant->google_plus_link = $restaurant_info['google_plus_link'];
                        $restaurant->restaurant_type = $restaurant_info['rest_type'];
                        $restaurant->delivery_charge = $ordering_setting['delivery_charge'];
                        $restaurant->minimum_order = $ordering_setting['minimum_order'];
                        $restaurant->payment_method = $ordering_setting['payment_method'];
                        $restaurant->delivery_methods = $ordering_setting['delivery_methods'];
                        $restaurant->default_currency = $ordering_setting['default_currency'];
                        $restaurant->food_ordering_system_type = $ordering_setting['food_ordering_system_type'];
                        $restaurant->delivery_time = $ordering_setting['delivery_time'];
                        $restaurant->payment_fee = $ordering_setting['payment_fee'];
                        $restaurant->promo_discount = $restaurant_info['promo_discount'];
                        $restaurant->city = $restaurant_info['city'];
                        $restaurant->province = $restaurant_info['province'];
                        $restaurant->country = $restaurant_info['country'];
                        $restaurant->street = $restaurant_info['street'];
                        $restaurant->mod_user_id = $user_id;
                        $restaurant->version_no = $restaurant->version_no + 1;
                        $restaurant->save();
                        DB::commit();
                        $res['id'] = $restaurant_info["id"];
                    }
                // }
                
            } else {
                Log::info('User not found');
                $res['return_cd'] = 1;
                $res['message'] = 'User not found';
            }
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }

        return response()->json($res);
    }
}