<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use Validator;
use App\Models\MstRestaurant;
use App\Models\MstFeedback;


class RestaurantController extends Controller
{
    public function get_restaurant_detail_by_id(Request $request) {

        Log::info('--------get_restaurant_detail_by_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        try {
            $restaurant_info = MstRestaurant::where('id', $rest_id)
                            ->where('del_flg', '0')
                            ->select('id', 'name', 'address', 'tel', 'mobile',
                                    'email', 'lat_val as latitue', 'long_val as longtitue',
                                    'website', 'logo_path as logo', 'open_time', 'close_time',
                                    'promote_code as promotion', 'restaurant_type as rest_type',
                                    'facebook_link', 'twitter_link',
                                    'linkedIn_link', 'google_plus_link' ,'promo_discount'
                                    )
                            ->get();
                            $queries = DB::getQueryLog();
                            $last_query = end($queries);
                             Log::info($last_query);
            $res['user_id'] = $user_id;
            $res['restaurant_info'] = $restaurant_info;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function get_order_setting_by_rest_id(Request $request) {

        Log::info('--------get_order_setting_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        try {
            $ordering_setting = MstRestaurant::where('id', $rest_id)
                            ->where('del_flg', '0')
                            ->select('payment_fee', 'delivery_charge', 'minimum_order', 
                            		'payment_method', 'delivery_methods', 'delivery_time',
									'default_currency', 'food_ordering_system_type', 'unit_point')
                            ->first();
            $res['ordering_setting'] = $ordering_setting;
            $res['rest_id'] = $rest_id;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function update_order_setting_by_rest_id(Request $request) {
        Log::info('--------update_order_setting_by_rest_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $order_setting = $param['order_setting'];
        Validator::extend('max_length', function($attribute, $value, $parameters, $validator){
            Log::debug('test1-------------');
            Log::debug($attribute);
            return strlen($value) <= $parameters[0];
        });
        $validator = $this->validateObject($order_setting, [
            'payment_fee' => 'required|numeric|max_length:256',
            'delivery_charge' => 'required|numeric|max_length:256',
            'minimum_order' => 'required|regex:/^[0-9]+(\.[0-9]{1,4})?$/|max_length:20',
            'payment_method' => 'required|max:20',
            'delivery_methods' => 'required|max:20',
            'delivery_time' => 'required',
            'default_currency' => 'required|max:4',
            'food_ordering_system_type' => 'max:4',
            'unit_point' => 'max:11',
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {

            $restaurant = MstRestaurant::where('id',$rest_id)->where('del_flg','0')->first();
            if (!empty($restaurant)) {
                $restaurant->payment_fee = $order_setting['payment_fee'];
                $restaurant->delivery_charge = $order_setting['delivery_charge'];
                $restaurant->minimum_order = $order_setting['minimum_order'];
                $restaurant->payment_method = $order_setting['payment_method'];
                $restaurant->delivery_methods = $order_setting['delivery_methods'];
                $restaurant->delivery_time = $order_setting['delivery_time'];
                $restaurant->default_currency = $order_setting['default_currency'];
                $restaurant->food_ordering_system_type = $order_setting['food_ordering_system_type'];
                $restaurant->unit_point = $order_setting['unit_point'];
                $restaurant->version_no = $restaurant->version_no + 1;
                $restaurant->mod_user_id = $user_id;
                $restaurant->save();
                $res['restaurant_id'] = $restaurant->id ;
            }
            
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }

    public function get_contents_by_rest_id(Request $request) {

        Log::info('--------get_contents_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        try {
            $content = MstRestaurant::where('id', $rest_id)
                            ->where('del_flg', '0')
                            ->select('private_policy', 'term_of_use', 'about_us')
                            ->first();
            if(!empty($content)) {
                $res['private_policy'] = $content->private_policy;   
                $res['term_of_use'] = $content->term_of_use;
                $res['about_us'] = $content->about_us;
                $res['rest_id'] = $rest_id;
            } else {
                $res['return_cd'] = 1;
                $res['message'] = 'Not found';                
            }
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function update_contents_by_rest_id(Request $request) {
        Log::info('--------update_contents_by_rest_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $private_policy = $param['private_policy'];
        $term_of_use = $param['term_of_use'];
        $about_us = $param['about_us'];
        $content = array($private_policy, $term_of_use, $about_us);
        $validator = $this->validateObject($content, [
            'private_policy' => 'max:1024',
            'term_of_use' => 'max:1024',
            'about_us' => 'max:1024',
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {

            $restaurant = MstRestaurant::where('id',$rest_id)->where('del_flg','0')->first();
            if (!empty($restaurant)) {
                $restaurant->private_policy = $private_policy;
                $restaurant->term_of_use = $term_of_use;
                $restaurant->about_us = $about_us;
               
                $restaurant->version_no = $restaurant->version_no + 1;
                $restaurant->mod_user_id = $user_id;
                $restaurant->save();
                $res['restaurant_id'] = $restaurant->id ;
            }
            
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }


    public function send_feedback(Request $request) {
        Log::info('--------send_feedback---------');
        $param = $request->all();
        $user_id = $param['user_id'];       
        $content = $param['content'];
        $array_valid = array($content);
        
       $validator = $this->validateObject($array_valid, [
            'content' => 'max:1536',
        ]);

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));           
        }
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        DB::beginTransaction();
        try {          
           
            $feedback =  new MstFeedback;
            $feedback->user_id = $user_id;           
            $feedback->content = $content;                   
            $feedback->cre_user_id = $user_id;
            $feedback->mod_user_id = $user_id;
            $feedback->save();      
          
            DB::commit();
           
        } catch(QueryException $ex){           
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
    
}