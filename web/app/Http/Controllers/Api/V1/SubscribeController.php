<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstSubscribe;


class SubscribeController extends Controller
{
    public function get_subscribe(Request $request) {

        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        try {
            $subscribe = MstSubscribe::where('del_flg', '0')
                            ->select('email', 'name', 
                            		 'cre_ts as issue_date')
                            ->get();
                          
            $res['subscribe'] = $subscribe;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

    
    public function delete_subscribe(Request $request) {
        Log::info('--------update_news_by_id---------');
        $param = $request->all();
        $email = $param['email'];
       
      
         Log::info($email);
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {
            MstSubscribe::where('email', $email)->delete();
            DB::commit();
            
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}