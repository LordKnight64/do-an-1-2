<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Services\RestaurantService;
use App\Services\GeoLocationService;


class GeoLocationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RestaurantService $restaurantService
                                , GeoLocationService $geoLocationService)
    {
        $this->restaurantService = $restaurantService;
        $this->geoLocationService = $geoLocationService;
    }

    public function get_coordinates(Request $request)
    {
        $param = $request->all();
        $city = $param['city'];
        $street = $param['street'];
        $province = $param['province'];
        
        $address = urlencode($city.','.$street.','.$province);
        
        $response = $this->geoLocationService->get_coordinates($address);
        
        return response()->json($response);
    }
    public function get_coordinates_by_addrress(Request $request)
    {
        $param = $request->all();
      
        $address = urlencode($param['address']);
      
        $response = $this->geoLocationService->get_coordinates($address);
        
        return response()->json($response);
    }

    public function find_restaurants(Request $request)
    {
        
        $param = $request->all();
        $source = $param["source"];
        $radius = $param["radius"];
        $destinations = $this->restaurantService->get_res_location();

        $results = $this->geoLocationService->get_driving_distance($source, $destinations, $radius);
        return response()->json($results);
    }

    public function get_distance(Request $request)
    {
        
        $param = $request->all();
        $source = $param["source"];
        $destination = $param["destination"];       

        $results = $this->geoLocationService->get_distance($source, $destination);
     
        return response()->json($results);
    }
    public function get_addrress_by_latlon(Request $request)
    {
        
        $param = $request->all();
        $lat = $param["lat"];
        $lon = $param["lon"];
        $results = $this->geoLocationService->get_all_address_info($lat, $lon);
    
        return response()->json($results);
    }
     public function get_data_coordinates(Request $request)
    {
        
        $param = $request->all();
        $address = urlencode($param['address']);
        $results = $this->geoLocationService->get_data_coordinates($address);
        return response()->json($results);
    }
}