<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use App\Services\EmailService;
use App\Models\MstRestaurantMailings;



class EmailOrderController extends Controller
{

     public function send_email_accept_order(Request $request) {
        
        $param = $request->all();
        $user_id = $param['user_id'];
        $order_id = $param['order_id'];

        $objective['items'] = DB::table('trn_order_detail as od')
                        ->join('mst_item as i',function($join){
                                $join->on('od.item_id','=','i.id')
                                ->where('i.del_flg','0')
                                ->where('i.active_flg','1');
                        })
                        ->where('od.order_id',$order_id)
                        ->where('od.del_flg','0')
                        ->select('i.name','i.price as unit_price','od.price','od.quantity','i.point')
                        ->get();
        
        $objective['order'] = DB::table('trn_order')
                    ->where('id',$order_id)
                    ->first();
        
        $objective['user'] = DB::table('mst_user')
                            ->where('id',$user_id)
                            ->first();
        
        $objective['restaurant'] = DB::table('mst_restaurant')
                            ->where('id',$objective['order']->restaurant_id)
                            ->first();

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        try {
            //EmailService
            Log::info("TỚI ĐÂY");
            EmailService::order($objective);
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
     Log::info("GOOD");
     
        return response()->json($res);
    }

}