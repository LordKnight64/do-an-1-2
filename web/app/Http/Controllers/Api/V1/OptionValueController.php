<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstItemOptionValue;
use App\Models\MstOptionValue;


class OptionValueController extends Controller
{
    public function get_options_value_by_rest_id(Request $request) {

        Log::info('--------get_options_value_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        $id = $param['id'];
        try {
            // $options = DB::table('mst_item_option_value as a')
            //                 ->join('mst_option_value as b', function ($join) { 
            //                 		 $join->on('a.option_value_id', '=', 'b.id')
            //                 		 	  ->where('b.del_flg', '0');
            //                 })
            //                 ->join('mst_option as c', function ($join) { 
            //                 		 $join->on('b.option_id', '=', 'c.id')
            //                 		 	  ->where('c.del_flg', '0');
            //                 })
            //                 ->where('a.del_flg', '0')
            //                 ->whereIn('a.item_id', function ($query) use ($rest_id) {
            //                             $query->from('mst_item')
            //                                   ->where('restaurant_id', '=', $rest_id)
            //                                   ->where('active_flg', '=', '1')
            //                                   ->where('del_flg', '=', '0')
            //                                   ->select('id')
            //                                   ->get();
            //                         })
            //                 ->select('b.id', 'b.name', 'b.cre_user_id', 
            //                 		'b.option_id', 'c.name as option_name', 'b.cre_ts'
	        //                         )
            //                 ->distinct()
            //                 ->get();
            // $res['options'] = $options;

            $options = DB::table('mst_option_value as b')
                            ->join('mst_option as c', function ($join) { 
                            		 $join->on('b.option_id', '=', 'c.id')
                            		 	  ->where('c.del_flg', '0');
                            })
                            ->where('b.del_flg', '0')
                            ->where(function($query) use ($id)
                             {
                                if(!empty($id)){
                                    $query->where('b.id', $id);
                                }
                            })
                            ->select('b.id', 'b.name', 'b.cre_user_id', 
                            		'b.option_id', 'c.name as option_name', 'b.cre_ts'
	                                )
                            ->distinct()
                            ->get();
            $res['options'] = $options;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }
    public function update_option_value_by_id(Request $request) {
        Log::info('--------update_option_value_by_id---------');
        
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();

        $option_value_param = $param['option'];
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $validator = $this->validateObject($option_value_param, [
            'name'    => 'required|max:256',
            'option_id'=> 'required',
        ]); 

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }
        DB::beginTransaction();
        try {
            $id = $option_value_param['id'];
            if ($id == -1) {
	            $option = new MstOptionValue;
                $option->restaurant_id = $rest_id;
                $option->option_id = $option_value_param['option_id'];
                $option->name = $option_value_param['name'];
                $option->cre_user_id = $user_id;
                $option->mod_user_id = $user_id;
                $option->save();
                $res['id'] = $option->id ;
            } else {
                $option = MstOptionValue::where('id',$id)->where('del_flg','0')->first();
                if (!empty($option)) {
                    if ($option_value_param['is_delete'] != 1) {
                        $option->option_id = $option_value_param['option_id'];
                        $option->name = $option_value_param['name'];
                    } else {
                        $option->del_flg = '1';
                        // DELETE MST_OPTION_VALUE : 
                        MstItemOptionValue::where('option_id',$option_value_param['option_id'])->update(array('del_flg' => 1));

                    }
                    $option->restaurant_id = $rest_id;
					$option->mod_user_id = $user_id;
                    $option->version_no = $option->version_no + 1;
                    $option->save();
                    $res['id'] = $option->id ;
                }
            }
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}