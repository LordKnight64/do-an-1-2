<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstItemOption;
use App\Models\MstOption;
use App\Models\MstItemOptionValue;


class OptionController extends Controller
{
    public function get_options_by_rest_id(Request $request) {

        Log::info('--------get_options_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        $option_id = $param['id'];
        try {
            // $options = DB::table('mst_item_option as a')
            //                 ->join('mst_option as b', function ($join) { 
            //                 		 $join->on('a.option_id', '=', 'b.id')
            //                 		 	  ->where('b.del_flg', '0');
            //                 })   
            //                 ->where('a.del_flg', '0')
            //                 ->whereIn('a.item_id', function ($query) use ($rest_id) {
            //                             $query->from('mst_item')
            //                                   ->where('restaurant_id', '=', $rest_id)
            //                                   ->where('active_flg', '=', '1')
            //                                   ->where('del_flg', '=', '0')
            //                                   ->select('id')
            //                                   ->get();
            //                         })
            //                 ->where(function($query) use ($option_id)
            //                     {
            //                         if(!empty($option_id)){
            //                             $query->where('b.id', $option_id);
            //                         }
            //                     })  
            //                 ->select('b.id', 'b.name')
            //                 ->distinct()
            //                 ->get();
            // $res['options'] = $options;
            $options = MstOption::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->where(function($query) use ($option_id)
                            {
                                if(!empty($option_id)){
                                    $query->where('id', $option_id);
                                }
                            })
                            ->select('id', 'name')
                            ->get();
            $res['options'] = $options;
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }
    public function update_option_by_id(Request $request) {
        Log::info('--------update_option_by_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $option_param = $param['option'];
        $validator = $this->validateObject($option_param, [
            'name'    => 'required|max:256',
        ]); 

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }
        DB::beginTransaction();
        try {
        	$id = $option_param['id'];
        	if($id == -1) {
        		$option = new MstOption;
                $option->restaurant_id = $rest_id;
        		$option->name = $option_param['name'];
        		$option->cre_user_id = $user_id;
        		$option->mod_user_id = $user_id;
        		$option->save();
        		$res['id'] = $option->id;
        	} else {
	            $option = MstOption::where('id',$id)->where('del_flg','0')->first();
	            if (!empty($option)) {
	            	if ($option_param['is_delete'] != 1) {
	            		$option->name = $option_param['name'];
	            	} else {
	            		$option->del_flg = '1';
                        // DELETE MST_ITEM_OPTION && MST_ITEM_OPTION_VALUE
                        MstItemOption::where('option_id',$option_param['id'])->update(array('del_flg' => 1));
                        MstItemOptionValue::where('option_id',$option_param['id'])->update(array('del_flg' => 1));
	            	} 
                    $option->restaurant_id = $rest_id;
            		$option->mod_user_id = $user_id;
            		$option->version_no = $option->version_no+1;
            		$option->save();
	            	$res['id'] = $option->id;
	            }
        	}
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}