<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstSearch;
use App\Models\MstUser;


class SearchController extends Controller
{
    public function get_search_items(Request $request) {

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
  
        try {
            $searchWithoutLogin = MstSearch::where('del_flg', '0')
                            -> where('user_id', '0')
                            ->select('name', 'id',
                            		 'cre_ts as issue_date', 'user_id' )
                            ->get();

            $search = DB::table('mst_searchs as b')
                            ->join('mst_user as c', function ($join) { 
                            		 $join->on('b.user_id', '=', 'c.id')
                            		 	  ->where('c.del_flg', '0');
                            })
                            ->where('b.del_flg', '0')
                            ->select('b.id', 'b.name', 'b.cre_ts as issue_date', 
                            		'b.user_id', 'c.first_name', 'c.last_name'
	                                )
                            ->distinct()
                            ->get();
                          
            $res['searchWithoutLogin'] = $searchWithoutLogin;
            $res['search'] = $search;

        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

    
    public function delete_search_items(Request $request) {
        
        $param = $request->all();
        $id = $param['id'];

       
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {
            MstSearch::where('id', $id)->delete();
            DB::commit();
            
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}