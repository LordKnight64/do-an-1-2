<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstRestaurantReviews;
use App\Models\MstUser;
use App\Models\MstRestaurant;
use App\Models\MstItem;


class RestaurantReviewsController extends Controller
{


    public function get_restaurant_reviews(Request $request) {

        $param = $request->all();
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        $rest_id = $param['rest_id'];

        try {
         

            $reviews = DB::table('mst_restaurant_reviews as b')
                            ->join('mst_item as c', function ($join) { 
                            		 $join->on('b.item_id', '=', 'c.id')
                            		 	  ->where('c.del_flg', '0');
                            })
                            ->join('mst_user as u', function ($join) { 
                            		 $join->on('u.id', '=', 'b.user_id')
                            		 	  ->where('u.del_flg', '0');
                            })
                            ->where('b.del_flg', '0')
                            ->select('b.id', 'b.restaurant_id', 'b.item_id', 'b.cre_ts',
                            		'b.active_flg', 'b.title', 'b.rating' , 'b.content', 'c.name', 'u.email' , 'u.first_name', 'u.last_name'
	                                )
                            ->distinct()
                            ->get();
                          
            $res['reviews'] = $reviews;
          

        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

     public function get_items_reviews(Request $request) {

        $param = $request->all();
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        $rest_id = $param['rest_id'];

        try {
         
            $items = DB::table('mst_item as b')
                            ->join('mst_restaurant_reviews as c', function ($join) { 
                            		 $join->on('c.item_id', '=', 'b.id')
                            		 	  ->where('c.del_flg', '0');
                                          
                            })
                            ->where('b.restaurant_id',  $rest_id)
                            ->where('b.del_flg', '0')
                            ->select('b.id', 'b.name'
	                                )
                            ->distinct()
                            ->get();
                          
            $res['items'] = $items;
          

        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

 public function update_restaurant_review(Request $request) {
        $param = $request->all();
        $active_flg = $param['active_flg'];
        $id = $param['id'];
        $res['return_cd'] = 0;
        $res['message'] = 'Update review item is successfull';

         DB::beginTransaction();
        try {

            $restaurant_reviews = MstRestaurantReviews::where('id',$id)->where('del_flg','0')->first();
            if (!empty($restaurant_reviews)) {
                $restaurant_reviews->active_flg =  $active_flg;
                $restaurant_reviews->save();
            }
            
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
 }
    
    public function delete_restaurant_review(Request $request) {
        
        $param = $request->all();
        $id = $param['id'];

       
        $res['return_cd'] = 0;
        $res['message'] = 'Delete review item is successfull';
        DB::beginTransaction();
        try {
            MstRestaurantReviews::where('id', $id)->delete();
            DB::commit();
            
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}