<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\TrnNews;


class NewsController extends Controller
{
    public function get_news_by_rest_id(Request $request) {

        Log::info('--------get_news_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        try {
            $news = TrnNews::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->select('id', 'title', 'content as description', 
                            		'thumb', 'cre_ts as issue_date', 'is_active')
                            ->get();
            $res['news'] = $news;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function get_news_by_id(Request $request) {

        Log::info('--------get_news_by_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        Log::info('--------param---------');
        Log::info($param);
        $id = $param['id'];
        try {
            $news = TrnNews::where('id', $id)
                            ->where('del_flg', '0')
                            ->select('id', 'title', 'content as description', 
                            		'thumb', 'cre_ts as issue_date', 'is_active')
                            ->first();
            $res['news'] = $news;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }
    
    public function update_news_by_id(Request $request) {
        Log::info('--------update_news_by_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $news_param = $param['news'];
        $validator = $this->validateObject($news_param, [
            'title' => 'required|max:1536',
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {
            $id = $news_param['id'];
            if ($id == -1) {
	        	$news =  new TrnNews;
                $news->restaurant_id = $rest_id;
                $news->title = $news_param['title'];
                $news->content = $news_param['description'];
                $news->thumb = $news_param['thumb'];
                $news->slug = $news_param['slug'];
                $news->is_active = $news_param['is_active'];
                $news->cre_user_id = $user_id;
                $news->mod_user_id = $user_id;
                $news->save();
                $res['news'] = $news;
            } else {
                $news = TrnNews::where('id',$id)->where('del_flg','0')->first();
                if (!empty($news)) {
                    if ($news_param['is_delete'] != 1) {
                    	$news->title = $news_param['title'];
		                $news->content = $news_param['description'];
		                $news->thumb = $news_param['thumb'];
		                $news->slug = $news_param['slug'];
		                $news->is_active = $news_param['is_active'];
		                $news->mod_user_id = $user_id;
                    } else {
                        $news->del_flg = '1';
                    }
					$news->version_no = $news->version_no + 1;
	                $news->mod_user_id = $user_id;
	                $news->save();
	                $res['news'] = $news;
                }
            }
            DB::commit();
            $res['user_id'] = $param['user_id'];
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}