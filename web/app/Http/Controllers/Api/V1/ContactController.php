<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use Validator;
use App\Models\TrnContacts;


class ContactController extends Controller
{
    public function get_contacts_by_rest_id(Request $request) {

        Log::info('--------get_contacts_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        try {
            $contact = TrnContacts::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->select('id', 'name', 'email', 'phone', 'address')
                            ->get();
            $res['contacts'] = $contact;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function get_contact_by_id(Request $request) {

        Log::info('--------get_contact_by_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        Log::info('--------param---------');
        Log::info($param);
        $id = $param['id'];
        try {
            $contact = TrnContacts::where('id', $id)
                            ->where('del_flg', '0')
                            ->select('id', 'name', 'email', 'phone', 'address')
                            ->first();
            $res['contacts'] = $contact;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function update_contact_by_id(Request $request) {
        Log::info('--------update_contact_by_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $contact_param = $param['contact'];
        Validator::extend('max_length', function($attribute, $value, $parameters, $validator){
            Log::debug('test1-------------');
            Log::debug($attribute);
            return strlen($value) <= $parameters[0];
        });
        $validator = $this->validateObject($contact_param, [
            'name' => 'required|max:256',
            'email' => 'required|email|min:6|max:256',
            'phone' => 'numeric|max_length:32',
            'address' => 'max:1536',
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {
            $id = $contact_param['id'];
            if ($id == -1) {
	        	$contact =  new TrnContacts;
                $contact->restaurant_id = $rest_id;
                $contact->name = $contact_param['name'];
                $contact->email = $contact_param['email'];
                $contact->phone = $contact_param['phone'];
                $contact->address = $contact_param['address'];
                $contact->cre_user_id = $user_id;
                $contact->mod_user_id = $user_id;
                $contact->save();
                $res['contact_id'] = $contact->id ;
            } else {
                $contact = TrnContacts::where('id',$id)->where('del_flg','0')->first();
                if (!empty($contact)) {
                    if ($contact_param['is_delete'] != 1) {
                    	$contact->name = $contact_param['name'];
                    	$contact->email = $contact_param['email'];
                    	$contact->address = $contact_param['address'];
                    	$contact->phone = $contact_param['phone'];
                        $contact->del_flg = $contact_param['is_delete'];
                    } else {
                        $contact->del_flg = '1';
                    }
					$contact->version_no = $contact->version_no + 1;
	                $contact->mod_user_id = $user_id;
	                $contact->save();
	                $res['contact_id'] = $contact->id ;
                }
            }
            DB::commit();
            $res['user_id'] = $param['user_id'];
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}