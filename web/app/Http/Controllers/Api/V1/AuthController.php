<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Http\Controllers\Api\V1\Controller as BaseController;
use App\Models\MstUser;
use App\Models\MstRole;
use App\Models\MstPermission;

class AuthController extends BaseController
{
    /**
     * Authenticate user.
     *
     * @param Instance Request instance
     *
     * @return JSON user details and auth credentials
     */
    public function login(Request $request) {

        Log::debug('----------login api-------------');

        $validator = $this->validate($request, [
            'email'    => 'required|email|min:6',
            'password' => 'required|min:6'
          
        ]);

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }

       
        $credentials = $request->only('email', 'password');

        $user = MstUser::whereEmail($credentials['email'])->first();

        if (isset($user->email_verified) && $user->email_verified == 0) {
            return $this->buildFailedLoginResponse($request, ['Email Unverified']);
        }
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->buildFailedLoginResponse($request, ['Invalid credentials']);
                // return response()->json('Invalid credentials');
                // return response()->error('Invalid credentials', 401);
            }
        } catch (\JWTException $e) {
            return $this->buildFailedLoginResponse($request, ['Could not create token']);
            // return response()->json('Could not create token', 500);
            // return response()->error('Could not create token', 500);
        }

        $user = Auth::user();
        $restaurants= DB::table('mst_user_restaurant as a')
                        ->join('mst_restaurant as b', 'a.restaurant_id', '=', 'b.id')
                        ->where('a.user_id', $user->id)
                        ->where('b.is_active', 1)
                        ->select('b.id', 'b.name', 'b.logo_path', 'b.address')
                        ->get();

        // if user is restaurant owner
        if( count($restaurants) != 0)    { 
             $token = JWTAuth::fromUser($user);
         

            $roles = DB::table('mst_role_user as a')
                    ->join('mst_role as b', 'a.role_id', '=', 'b.id')
                    ->where('a.user_id', $user->id)
                    ->select('a.role_id', 'b.name')
                    ->get();

            $user_info['id'] = $user->id;
            $user_info['phone'] = $user->mobile;
            $user_info['email'] = $user->email;
            $user_info['first_name'] = $user->first_name;
            $user_info['last_name'] = $user->last_name;
            $user_info['roles'] = $roles;
            $user_info['tel'] = $user->tel;        

            $res['return_cd'] = '0';
            $res['token'] = $token;
            $res['user_info'] = $user_info;
            $res['restaurants'] = $restaurants;

            return response()->json($res);
        }             
        else
            return $this->buildFailedLoginResponse($request, ['Not found your restaurant']);
    }

    /**
     * Get all roles and their corresponding permissions.
     *
     * @return array
     */
    private function getRolesAbilities($user)
    {
        $abilities = [];
        //$roles = MstRole::all();
        $roles = $user->roles;
        Log::info($roles);

        foreach ($roles as $role) {
            if (!empty($role->name)) {
                $abilities[$role->name] = [];
                $rolePermission = $role->permissions()->get();

                foreach ($rolePermission as $permission) {
                    if (!empty($permission->name)) {
                        array_push($abilities[$role->name], $permission->name);
                    }
                }
            }
        }

        return $abilities;
    }

    public function getTest(Request $request) {

        return response()->success('OK'); 
    }

    /**
     * Get authenticated user details and auth credentials.
     *
     * @return JSON
     */
    public function getAuthenticatedUser()
    {
        Log::debug('----------getAuthenticatedUser api-------------');
        $user = JWTAuth::parseToken()->toUser();
        $token = JWTAuth::fromUser($user);
        Log::debug('**************getAuthenticatedUser token', (array)  $token);
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user', 'token'));
    }



    /**
     * 
     *
     * @return JSON
     */
    public function getToken(Request $request)
    {
        Log::debug('----------getToken api-------------');
        $user = JWTAuth::parseToken()->toUser();
        $token = JWTAuth::fromUser($user);
        Log::debug('**************getToken token', (array)  $token);
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('token'));
    }
}