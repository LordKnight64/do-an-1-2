<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use Folklore\Image\Facades\Image;
use App\Models\MstCategory;
use App\Models\TrnNews;
use App\Models\MstItem;
use App\Models\MstArea;
use App\Models\MstTable;
use App\Models\MstRestaurant;

class UploadController extends Controller
{
    public function uploadFileImage(Request $request)
    {
        //************insert athlete************
        Log::debug('************insert uploadFileImage************');

        $param = $request->all();
        DB::beginTransaction();
        //update anh
        $id = $param['id'];
        $data = $param["thumb"];
        // Log::debug($param["thumb"]);
        if ($data!=null || !empty($data)) {
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $f = finfo_open();
            $mime_type = finfo_buffer($f, $data, FILEINFO_MIME_TYPE);
            list($imageFirst, $type) = explode("/", $mime_type);
            Log::debug('type .' . $type);
            $format = '.'. $type;
            $filename = $param["id"].$format;
            $typeScreen = $param["typeScreen"];
            // $dirname = public_path("admin\images\\".$typeScreen);
            $modeServer = env('MODE_SEVER');
            $dirname = $_SERVER['DOCUMENT_ROOT']."/admin/images/".$typeScreen;
            if ($modeServer==1) {
                $dirUrlUpload = env('DIR_URL_UPLOAD');
                $dirname = $dirUrlUpload.'/public_html/restaurant/admin/images/'.$typeScreen;
            }
            Log::info($dirname);
            if (!file_exists($dirname)) {
                mkdir($dirname, 0777);
                Log::debug('************The directory '.$dirname.' was successfully created************');
            }
            $path = public_path("admin\images\\".$typeScreen."\upload". $filename);
            if ($modeServer==1) {
                $dirUrlUpload = env('DIR_URL_UPLOAD');
                $path = $dirUrlUpload.'/public_html/restaurant/admin/images/'.$typeScreen."/upload". $filename;
                Log::debug('************File '.$path.' modeServer************');
            }
            try {
                if ($path!=null || !empty($path)) {
                    if (file_exists($path)) {
                        unlink($path);
                        Log::debug('************File '.$path.' has been deleted************');
                    }
                }
            } catch (QueryException $ex) {
                $res['return_cd'] = 1;
                $res['message'] = $ex->getMessage();
            } catch (Exception $ex) {
                $res['return_cd'] = 1;
                $res['message'] = $ex->getMessage();
            }
            // $path = $_SERVER['DOCUMENT_ROOT']."/admin/images/".$typeScreen."/upload". $filename;
            Log::info($path);
            $DOMAIN_ADMIN = env('DOMAIN_ADMIN');
            Log::info($DOMAIN_ADMIN);
            $url = $request->url();
            $parsedUrl = parse_url($url);
            Log::info(json_encode($parsedUrl));
            // $root = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
            $DOMAIN_ADMIN = $parsedUrl['scheme'] . '://' . $DOMAIN_ADMIN;
            if (array_key_exists('port', $parsedUrl)) {
                $DOMAIN_ADMIN = $DOMAIN_ADMIN.':'.$parsedUrl['port'];
            }
            Log::info($DOMAIN_ADMIN);
            // if($parsedUrl['port']$parsedUrl['port']){
            // 	$root = $root.':'.$parsedUrl['port'];
            // }
            $pathSaveDB = $DOMAIN_ADMIN."\admin\images\\".$typeScreen."\upload". $filename;
            $file  = file_put_contents($path, $data);
            $image = imagecreatefromstring(file_get_contents($path));
            list($width_orig, $height_orig) = getimagesize($path);
            $aspectRatio = $height_orig / $width_orig;
            $width = 150;
            $height = 150;
            $image_p = imagecreatetruecolor($width, $height);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagejpeg($image_p, $path, 100);
            imagedestroy($image_p);
            imagedestroy($image);
            if ($typeScreen == 'category') {
                $category = MstCategory::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($param['thumb'])) {
                    $category->thumb = $pathSaveDB;
                    $category->save();
                    DB::commit();
                }
            }
            if ($typeScreen == 'news') {
                $row = TrnNews::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($param['thumb'])) {
                    $row->thumb = $pathSaveDB;
                    $row->save();
                    DB::commit();
                }
            }
            if ($typeScreen == 'items') {
                $item = MstItem::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($param['thumb'])) {
                    $item->thumb = $pathSaveDB;
                    $item->save();
                    DB::commit();
                }
            }
            if ($typeScreen == 'profile') {
                $item = MstRestaurant::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($param['thumb'])) {
                    $item->logo_path = $pathSaveDB;
                    $item->save();
                    DB::commit();
                }
            }

            if ($typeScreen == 'area') {
                $area = MstArea::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($param['thumb'])) {
                    $area->thumb = $pathSaveDB;
                    $area->save();
                    DB::commit();
                }
            }

            if ($typeScreen == 'table') {
                $table = MstTable::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($param['thumb'])) {
                    $table->thumb = $pathSaveDB;
                    $table->save();
                    DB::commit();
                }
            }
        }

        $res['id'] = $id;
        return response()->json($res);
        ;
    }

    public function delete_file(Request $request)
    {
        Log::info('--------delete_file_image---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        Log::info('--------param---------');
        Log::info($param);
        $thumb = $param['thumb'];
        $DOMAIN_ADMIN = env('DOMAIN_ADMIN');
        Log::info($DOMAIN_ADMIN);
        $url = $request->url();
        $parsedUrl = parse_url($url);
        Log::info(json_encode($parsedUrl));
        // $root = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
        $DOMAIN_ADMIN = $parsedUrl['scheme'] . '://' . $DOMAIN_ADMIN;
        if (array_key_exists('port', $parsedUrl)) {
            $DOMAIN_ADMIN = $DOMAIN_ADMIN.':'.$parsedUrl['port'];
        }
        Log::info($DOMAIN_ADMIN);
        $modeServer = env('MODE_SEVER');
        $dirname = $_SERVER['DOCUMENT_ROOT'];
        if ($modeServer==1) {
            $dirUrlUpload = env('DIR_URL_UPLOAD');
            $dirname = $dirUrlUpload.'/public_html/restaurant';
        }
        Log::info("_____dir______");
        Log::info($dirname);
        $thumb = str_replace($DOMAIN_ADMIN, $dirname, $thumb);
        Log::info("____________thumb____________");
        Log::info($thumb);
        if ($modeServer==1) {
            $thumb = str_replace("\\", "/", $thumb);
        }
        try {
            if ($thumb!=null || !empty($thumb)) {
                $filename = $thumb;
                if (file_exists($filename)) {
                    unlink($filename);
                    Log::debug('************File '.$filename.' has been deleted************');
                }
            }
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }
}
