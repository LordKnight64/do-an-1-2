<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use App\Models\TrnOrder;
use App\Services\PushNotificationService;
use App\Models\MstCd;

class OrderController extends Controller
{
     public function __construct(PushNotificationService $pushNotificationService)
    {
        $this->pushNotificationService = $pushNotificationService;
    }
    public function get_orders_by_rest_id(Request $request) {

        Log::info('--------get_orders_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        Log::info($param);
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $order_sts = $param['order_sts'];
        $delivery_ts_to = is_null($param['delivery_ts_to']) ? $param['delivery_ts_to'] : date('Y-m-d', strtotime($param['delivery_ts_to']));
        $delivery_ts_from = is_null($param['delivery_ts_from']) ? $param['delivery_ts_from'] : date('Y-m-d 23:59:59.999', strtotime($param['delivery_ts_from']));
        $userIdSearch = $param['user_id_search'];
        $id = $param['id'];
        $orders_result_list = [];
        try {
            //Get orders by user_id and rest_id
            $orders = DB::table('trn_order as a')
                            ->leftJoin('mst_user_address as b', function ($join) { 
                            		 $join->on('a.address_id', '=', 'b.id')
                            		 	  ->where('b.del_flg', '=', '0');
                            })
                            ->leftJoin('mst_cd as c', function ($join) { 
                            		 $join->on('a.order_sts', '=', 'c.cd')
                            		 	  ->where('c.cd_group', '=', '5')
                                          ->where('c.del_flg', '=', '0');
                            })
                            ->leftJoin('mst_restaurant as d', function ($join) { 
                            		 $join->on('a.restaurant_id', '=', 'd.id')
                            		 	  ->where('d.del_flg', '=', '0');
                            })
                            ->where('a.restaurant_id', '=', $rest_id)
                            ->where(function($query) use ($order_sts)
                            {
                                if(!is_null($order_sts)){
                                    $query->where('a.order_sts', $order_sts);
                                }
                            })
                            ->where(function($query) use ($delivery_ts_to)
                            {
                                if(!is_null($delivery_ts_to)){
                                    $query->where('a.delivery_ts', '>=', $delivery_ts_to);
                                }
                            })
                            ->where(function($query) use ($delivery_ts_from)
                            {
                                if(!is_null($delivery_ts_from)){
                                    $query->where('a.delivery_ts', '<=', $delivery_ts_from);
                                }
                            })
                            ->where(function($query) use ($userIdSearch,$user_id)
                            {
                                if($userIdSearch=="1"){
                                     $query->where('a.user_id', '=', $user_id);
                                }else if($userIdSearch=="2"){
                                     $query->where('a.user_id', '!=', $user_id);
                                }
                            })
                            ->where(function($query) use ($id)
                            {
                                if(!is_null($id)){
                                    $query->where('a.id', $id);
                                }
                            })
                            ->where('a.del_flg', '=', '0')
                            ->orderBy('a.id', 'desc')
                            ->select(   'a.id', 'a.user_id', 'a.phone', 'b.address', 'b.lat_val', 'b.long_val','a.order_ts', 'a.order_sts', 
                                        'a.price', 'a.delivery_fee', 'a.delivery_ts', 'a.notes', 'a.name', 'a.delivery_type', 'c.cd_name',
                                        'd.default_currency', 'd.promo_discount', 'd.promote_code', 'd.lat_val as rest_lat_val', 'd.long_val as rest_long_val', 'a.recieved_point' , 'a.used_point'
                                    )
                            ->get();
            //check null orders
            if (!empty($orders)) {
                foreach ($orders as $order) {
                    //Set basic info of order
                    $orders_tmp = $this->set_basic_info_order($order);
                    
                    //Get data for items
                    $order_details = DB::table('trn_order_detail as a')
                                        ->join('mst_item as b', function ($join) { 
                            		 		$join->on('a.item_id', '=', 'b.id')
                            		 			 ->where('b.del_flg', '=', '0');
                                        })
                                        ->where('a.order_id', '=', $order->id)
                                        ->where('a.del_flg', '=', '0')
                                        ->select(   'a.item_id', 'b.name', 'a.quantity',
                                                    'a.price', 'a.notes', 'a.seq_no', 'a.point'
                                                )
                                        ->get();
                    $items =  [];
                    //check null order_details
                    if (!empty($order_details)) {

                        foreach ($order_details as $order_detail) {
                            //set basic info of item
                            $item = $this->set_basic_info_item($order_detail);

                            // Get data for options
                            $option_items = DB::table('trn_order_option_value as a')
                                        ->join('mst_option_value as b', function ($join) {
                                            $join->on('a.option_value_id', '=', 'b.id')
                                                 ->where('b.del_flg', '=', '0');
                                        })
                                        ->join('mst_option as c', function ($join) {
                                            $join->on('a.option_id', '=', 'c.id')
                                                 ->where('c.del_flg', '=', '0');
                                        })
                                        ->join('mst_item_option_value as d', function ($join) {
                                            $join->on('a.item_id', '=', 'd.item_id')
                                                 ->on('a.option_value_id', '=', 'd.option_value_id')
                                                 ->on('a.option_id', '=', 'd.option_id')
                                                 ->where('d.del_flg', '=', '0');
                                        })
                                        ->where('a.order_id', '=', $order->id)
                                        ->where('a.item_id', '=', $order_detail->item_id)
                                        ->where('a.del_flg', '=', '0')
                                        ->select(   'c.id as option_id',
                                                    'b.id as option_value_id',
                                                    'c.name as option_name',
                                                    'b.name as option_value_name',
                                                    'd.add_price', 'd.active_flg', 'a.point'
                                                )
                                        ->get();
                            //Set option for item
                            $item['option_items'] = $option_items;
                             Log::info('--------item---------');
                             Log::info($item);
                            array_push($items, $item);

                        }
                    }
                    $orders_tmp['items'] = $items;
                    array_push($orders_result_list, $orders_tmp);
                }
            }
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        $res['orders'] = $orders_result_list;

        return response()->json($res);
    }
    
    public function update_order_by_id(Request $request) {
    	Log::info('--------update_order_by_id---------');
    	$res['return_cd'] = 0;
    	$res['message'] = 'OK';
    
    	$param = $request->all();
    	$user_id = $param['user_id'];
    	$order_id = $param['order_id'];
    	$status = $param['status'];
    	DB::beginTransaction();
    	try {
			$order = TrnOrder::find($order_id);
			if (!empty($order)) {
				$order->order_sts = $status;
				$order->version_no = $order->version_no + 1;
				$order->mod_user_id = $user_id;
				$order->save();
                //Push notification
                $code = MstCd::where('cd_group', "5")
                            ->where('cd', $status)
                            ->where('del_flg', '0')
                            ->select('cd', 'cd_name')
                            ->first();
                $lishPush = $this->pushNotificationService->get_for_user($order->user_id,"web");
                if (isset($lishPush)) {
                    foreach ($lishPush as $itemPush) {
                        $token = $itemPush["token"];
                        $tokenPush = json_decode($token);
                        Log::debug('-----------------tokenPush-------------'.json_encode($tokenPush));
                        $paramPush['endpoint'] = $tokenPush->{'endpoint'};
                        Log::debug('-----------------endpoint-------------'.$paramPush['endpoint']);
                        // $responsePush = $this->pushNotificationService->push($param);
                        $keysPush=$tokenPush->{'keys'};
                        $keys['p256dh']=$p256dh=$keysPush->{'p256dh'};
                        Log::debug('-----------------p256dh-------------'. $keys['p256dh']);
                        $keys['auth']=$keysPush->{'auth'};
                        $paramPush['keys'] =$keys;
                        Log::debug('-----------------auth-------------'.$paramPush['keys']['auth']);
                        $paramPush['payload']="";
                        if(isset($code)){
                            $paramPush['payload']="Your order had change status to ".$code->cd_name;
                        }
                        Log::debug('-----------------payload-------------'. $paramPush['payload']);
                        $this->pushNotificationService->push($paramPush);
                    }
                }
				$res['order_id'] = $order_id;
	    		DB::commit();
			}
    	} catch(QueryException $ex){
    		$res['return_cd'] = 1;
    		$res['message'] = $ex->getMessage();
    		DB::rollBack();
    	} catch(Exception $ex){
    		$res['return_cd'] = 1;
    		$res['message'] = $ex->getMessage();
    		DB::rollBack();
    	}
    	return response()->json($res);
    }
    
    private function set_basic_info_order($order) {
        
        $orders_tmp['id'] = $order->id;
        $orders_tmp['user_id'] = $order->user_id;
        $orders_tmp['phone'] = $order->phone;
        $orders_tmp['address'] = $order->address;
        $orders_tmp['latitue'] = $order->lat_val;
        $orders_tmp['longtitue'] = $order->long_val;
        $orders_tmp['order_time'] = $order->order_ts;
        $orders_tmp['order_sts'] = $order->order_sts;
        $orders_tmp['total_price'] = $order->price;
        $orders_tmp['delivery_fee'] = $order->delivery_fee;
        $orders_tmp['delivery_time'] = $order->delivery_ts;
        $orders_tmp['note'] = $order->notes;
        $orders_tmp['name'] = $order->name;
        $orders_tmp['delivery_type'] = $order->delivery_type;
        $orders_tmp['order_sts_display'] = $order->cd_name;
        $orders_tmp['default_currency'] = $order->default_currency;
        $orders_tmp['promo_discount'] = $order->promo_discount;
        $orders_tmp['promote_code'] = $order->promote_code;
        $orders_tmp['rest_latitue'] = $order->rest_lat_val;
        $orders_tmp['rest_longtitue'] = $order->rest_long_val;
        $orders_tmp['used_point'] = $order->used_point;
        $orders_tmp['recieved_point'] = $order->recieved_point;

        return $orders_tmp;
    }
    private function set_basic_info_item($order_detail) {
        
        $item['item_id'] = $order_detail->item_id;
        $item['name'] = $order_detail->name;
        $item['quantity'] = $order_detail->quantity;
        $item['price'] = $order_detail->price;
        $item['note'] = $order_detail->notes;
        $item['seq_no'] = $order_detail->seq_no;
        $item['point'] = $order_detail->point;

        return $item;
    }

}