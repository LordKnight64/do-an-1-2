<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstArea;

class AreaController extends Controller
{
    public function get_area_by_rest_id(Request $request)
    {
        Log::info('--------get_areas_by_rest_id---------');

        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        $area_id = $param['id'];
        Log::info('ID'.$area_id);
        try {
            $area = MstArea::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->where(function ($query) use ($area_id) {
                                if (!empty($area_id)) {
                                    $query->where('id', $area_id);
                                }
                            })
                            ->select('id', 'name', 'thumb', 'is_active')
                            ->get();
            $res['areas'] = $area;
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function update_area_by_id(Request $request)
    {
        Log::info('--------update_categories_by_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $area_param = $param['area'];
        Log::info('--------update_categories_by_id---------');
        $validator = $this->validateObject($area_param, [
            'name'    => 'required|max:256',
        ]);

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }

        $res['return_cd'] = 0;
        $res['message'] = 'OK';


        DB::beginTransaction();
        try {
            $id = $area_param['id'];
            if ($id == -1) {
                $area =  new MstArea;
                $area->restaurant_id = $rest_id;
                $area->name = $area_param['name'];
                $area->thumb = $area_param['thumb'];
                $area->is_active = $area_param['is_active'];
                ;
                $area->cre_user_id = $user_id;
                $area->mod_user_id = $user_id;
                $area->save();
                $res['area_id'] = $area->id ;
            } else {
                $area = MstArea::where('id', $id)->where('del_flg', '0')->first();
                if (!empty($area)) {
                    if ($area_param['is_delete'] != 1) {
                        $area->name = $area_param['name'];
                        if ($area_param['thumb']!=null || !empty($area_param['thumb'])) {
                            $area->thumb = $area_param['thumb'];
                        }
                        $area->is_active = $area_param['is_active'];
                        $area->del_flg = $area_param['is_delete'];
                    } else {
                        $area->del_flg = '1';
                        //DELETE
                    }
                    $area->mod_user_id = $user_id;
                    $area->save();
                    $res['area_id'] = $area->id ;
                }
            }
            DB::commit();
            $res['user_id'] = $param['user_id'];
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}
