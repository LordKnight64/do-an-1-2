<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use App\Services\EmailService;
use App\Models\MstRestaurantMailings;



class EmailMarketingController extends Controller
{
   

    public function get_email_marketing_list(Request $request) {

        
        $param = $request->all();
        $rest_id = $param['rest_id'];
         Log::info('--------get_subscribe---------');
        Log::info( $rest_id);

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
       
        try {
        
              $emails = MstRestaurantMailings::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->select('id', 'subject', 'send_to', 'send_cc', 'send_bb', 'cre_ts as issue_date')
                            ->get();

            $res['emails'] = $emails;
            

        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

     public function get_email_marketing_detail(Request $request) {

        Log::info('--------get_email_marketing_detail---------');
        $param = $request->all();
        $email_id = $param['id'];
         Log::info('--------get_email_marketing_detail 123---------');
        Log::info( $email_id);

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
       
        try {
        
              $email = MstRestaurantMailings::where('id', $email_id)
                            ->where('del_flg', '0')
                            ->select('id', 'subject', 'send_to', 'send_cc', 'send_bb', 'content', 'cre_ts as issue_date')
                            ->get();

            $res['email'] = $email;
            

        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

     public function send_email_marketing_list(Request $request) {
        
        $param = $request->all();
        $rest_id = $param['rest_id'];
        $subject = $param['subject'];
        $send_to = $param['send_to'];
        $send_cc = $param['send_cc'];
        $send_bb = $param['send_bb'];
        $content = $param['content'];
        $user_id = $param['user_id'];

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        Log::info("$subject === $send_to === $content");
        DB::beginTransaction();
        try {

            $Mail =  new MstRestaurantMailings;
            $Mail->restaurant_id = $rest_id;
            $Mail->subject = $subject;
            $Mail->send_to = $send_to;
            $Mail->send_cc = $send_cc;
            $Mail->send_bb = $send_bb;
            $Mail->content = $content;
            $Mail->cre_user_id = $user_id;
            $Mail->mod_user_id = $user_id;
            $Mail->save();
            $email = $Mail->send_to;
            DB::commit();
            //EmailService
            Log::info("TỚI ĐÂY");
            EmailService::send_mail_marketing($Mail,$email);

   
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
     Log::info("GOOD");
     
        return response()->json($res);
    }

   
    
    public function delete_email_marketing_list(Request $request) {
        
        $param = $request->all();
        $id = $param['id'];
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {
            MstRestaurantMailings::where('id', $id)->delete();
            DB::commit();
            
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}