<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstTurnoverPlaning;
use App\Models\TrnOrder;


class TurnOverPlaningController extends Controller
{
   

    public function get_turnover_planing(Request $request) {

        
        $param = $request->all();
        $rest_id = $param['rest_id'];
        $year_no = $param['year_no'];

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
       
        try {
        
            $turn_over_planing = MstTurnoverPlaning::where('restaurant_id', $rest_id)
                            ->where('year_no', $year_no)
                            ->select('id', 'year_no', 'turn_over_planing')
                            ->get();
                             Log::info('$turn_over_planing');
          
            if (!empty($turn_over_planing)) {
                 $res['turn_over_planing'] = $turn_over_planing;
            }
            else {
                 $res['turn_over_planing'] = null;
            }
            
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
       
        return response()->json($res);
    }

     public function get_turnover(Request $request) {

        $param = $request->all();
     
        $rest_id = $param['rest_id'];
        $year_no = $param['year_no'];

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
       
        try {
            $turnovers = [];
            for ($i = 1 ; $i <= 12; $i++ ) {
                 $turnover = TrnOrder::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->where('order_sts', '2')
                            ->whereYear('order_ts', '=', $year_no)
                            ->whereMonth('order_ts', '=', $i)
                            ->select('id', 'order_ts', 'price')
                            ->get();

                    array_push($turnovers,  $turnover);               
            }

             $res['turnover'] = $turnovers;

        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        Log::info('--------get_subscribe---------');
        return response()->json($res);
    }

    

     public function update_turnover_planing(Request $request) {

        $param = $request->all();
        $rest_id = $param['rest_id'];
        $year_no = $param['year_no'];
        $user_id = $param['user_id'];

        $turnover_planing = $param['turnover_planing'];

        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        DB::beginTransaction();
        try {

            $restaurant_planing = MstTurnoverPlaning::where('year_no',$year_no)->where('restaurant_id',$rest_id)->first();
            if (!empty($restaurant_planing)) {
                $restaurant_planing->turn_over_planing =  $turnover_planing;
                $restaurant_planing->save();
            }
            else {
                $restaurant_planing =  new MstTurnoverPlaning;
                $restaurant_planing->turn_over_planing = $turnover_planing;
                $restaurant_planing->year_no =$year_no;
                $restaurant_planing->restaurant_id = $rest_id;
                $restaurant_planing->cre_user_id = $user_id;
                 $restaurant_planing->mod_user_id = $user_id;
                $restaurant_planing->save();
            }
            
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }

     
     
        return response()->json($res);
    }

}