<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;

use App\Models\MstCategory;
use App\Models\MstItemCategory;

class CategoryController extends Controller
{
    public function get_categories_by_rest_id(Request $request) {

        Log::info('--------get_categories_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        $category_id = $param['id'];
        Log::info('ID'.$category_id);
        try {
            $category = MstCategory::where('restaurant_id', $rest_id)
                            ->where('del_flg', '0')
                            ->where(function($query) use ($category_id)
                            {
                                if(!empty($category_id)){
                                    $query->where('id', $category_id);
                                }
                            })
                            ->select('id', 'name', 'thumb', 'description', 'is_active')
                            ->get();
            $res['categories'] = $category;
           
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        return response()->json($res);
    }

    public function update_categories_by_id(Request $request) {
        Log::info('--------update_categories_by_id---------');
        $param = $request->all();
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $category_param = $param['category'];
        Log::info('--------update_categories_by_id---------');
         $validator = $this->validateObject($category_param, [
            'name'    => 'required|max:256',
            'description'    => 'max:512',
        ]); 

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }

        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        
        DB::beginTransaction();
        try {
            $id = $category_param['id'];
            if ($id == -1) {
            	$category =  new MstCategory;
                $category->restaurant_id = $rest_id;
                $category->name = $category_param['name'];
                $category->thumb = $category_param['thumb'];
                $category->description = $category_param['description'];
                $category->is_active = $category_param['is_active'];;
                $category->cre_user_id = $user_id;
                $category->mod_user_id = $user_id;
                $category->save();
                $res['category_id'] = $category->id ;
            } else {
                $category = MstCategory::where('id',$id)->where('del_flg','0')->first();
                if (!empty($category)) {
                    if ($category_param['is_delete'] != 1) {
                        $category->name = $category_param['name'];
                        if($category_param['thumb']!=null || !empty($category_param['thumb'])){
						    $category->thumb = $category_param['thumb'];
                        }
						$category->description = $category_param['description'];
                        $category->is_active = $category_param['is_active'];
                        $category->del_flg = $category_param['is_delete'];
                    } else {
                        $category->del_flg = '1';
                        //DELETE 
                        MstItemCategory::where('category_id',$id)->update(array('del_flg' => 1));
                    }
					$category->version_no = $category->version_no + 1;
					$category->mod_user_id = $user_id;
					$category->save();
					$res['category_id'] = $category->id ;
                }
            }
            DB::commit();
            $res['user_id'] = $param['user_id'];
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
             DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
}