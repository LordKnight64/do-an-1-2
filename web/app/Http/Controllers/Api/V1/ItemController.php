<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;
use Validator;
use App\Models\MstItem;
use App\Models\MstItemOptionValue;
use App\Models\MstOptionValue;
use App\Models\MstItemOption;
use App\Models\MstItemCategory;
use Carbon\Carbon;
class ItemController extends Controller
{
    public function get_items_by_rest_id(Request $request) {

        Log::info('--------get_items_by_rest_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $rest_id = $param['rest_id'];
        $category_id = $param['category_id'];
        $id = $param['id'];
        $food_items = [];
        try {
            $items = DB::table('mst_item as a')
                            ->join('mst_item_category as b', function ($join) { 
                            		 $join->on('a.id', '=', 'b.item_id')
                            		 		->where('b.del_flg', '0');
                            })
                            ->join('mst_category as c', function ($join) { 
                            		 $join->on('b.category_id', '=', 'c.id')
                            		 		->where('c.del_flg', '0');
                            })
                            ->where('a.restaurant_id', $rest_id)
                            ->where('a.del_flg', '0')
                            // ->where('a.active_flg', '1')
                            ->where(function($query) use ($category_id)
                            {
                                if(!empty($category_id)){
                                    $query->where('c.id', $category_id);
                                }
                            })
                            ->where(function($query) use ($id)
                            {
                                if(!empty($id)){
                                    $query->where('a.id', $id);
                                }
                            })
                            ->select('c.id as category_id', 'c.name as category_name',
                                'a.id', 'a.name', 'a.price', 'a.active_flg',
                                'a.is_alcoho', 'a.food_type','a.thumb','a.sku','a.description', 'a.point'
                                )
                            ->get();
                            // $queries = DB::getQueryLog();
                            // $last_query = end($queries);
                            //  Log::info($last_query);
            if (count($items)) {
                foreach ($items as $item) {
                    $food_item['category_id'] = $item->category_id;
                    $food_item['category_name'] = $item->category_name;
                    $food_item['id'] = $item->id;
                    $food_item['name'] = $item->name;
                    $food_item['price'] = $item->price;
                    $food_item['active_flg'] = $item->active_flg;
                    $food_item['is_alcoho'] = $item->is_alcoho;
                    $food_item['food_type'] = $item->food_type;
                    $food_item['thumb'] = $item->thumb;
                    $food_item['sku'] = $item->sku;
                    $food_item['point'] = $item->point;
                    $food_item['description'] = $item->description;
                    $options = DB::table('mst_item_option_value as a')
                                ->join('mst_option as b', function ($join) { 
                            		 $join->on( 'a.option_id', '=', 'b.id')
                            		 		->where('b.del_flg', '=', '0');
                                })
                                ->join('mst_option_value as c', function ($join) { 
                            		 $join->on('a.option_value_id', '=', 'c.id')
                                			->where('c.del_flg', '=', '0');
                                })
                                ->where('a.item_id', $item->id)
                                ->where('a.del_flg', '=', '0')
                                ->select('a.option_id', 'a.option_value_id', 'b.name as option_name' 
                                    , 'c.name as option_value_name', 'a.add_price', 'a.point', 'a.active_flg' )
                                ->get();
                             //    $queries = DB::getQueryLog();

                            // $last_query = end($queries);
                           //   Log::info("afsdfsfsdsfdafsd");
                           //   Log::info($last_query);
                    $food_item['option_items'] = $options;
                    array_push($food_items, $food_item);
                }
            }
            $res['food_items'] = $food_items;
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }
        return response()->json($res);
    }
    public function update_item_by_id(Request $request) {
        $mytime = Carbon::now();
        $mytime->toDateTimeString();
        Log::info($mytime);
        //$date = Carbon::createFromFormat('Y/m/d', $mytime->toDateTimeString());
        Log::info('--------update_item_by_id---------');
        $res['return_cd'] = 0;
        $res['message'] = 'OK';

        $param = $request->all();
        $item_param = $param['item'];
        $user_id = $param['user_id'];
        $rest_id = $param['rest_id'];
        $option_items = $item_param['option_items'];
        
        Validator::extend('max_length', function($attribute, $value, $parameters, $validator){
            Log::debug('test1-------------');
            Log::debug($attribute);
            return strlen($value) <= $parameters[0];
        });

        $validator = $this->validateObject($item_param, [
                    'name'    => 'required|max:256',
                    'price'    => 'required|numeric|max_length:10',
                    'category_id'    => 'required',
                    'sku'    => 'required|max:128',
                    'point'    => 'required|numeric|max_length:10'
                ]); 
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        DB::beginTransaction();
        try {
            //Update item
            $id = $item_param['id'];
            if($id == -1) {
            	$item = new MstItem;
                $item->restaurant_id = $rest_id;
                $item->name = $item_param['name'];
                $item->thumb = $item_param['thumb'];
                $item->sku = $item_param['sku'];
                $item->price = $item_param['price'];
                $item->point = $item_param['point'];
                $item->start_date = $mytime;
                $item->active_flg = '1';
                $item->description = $item_param['description'];
                $item->cre_user_id = $user_id;
                $item->mod_user_id = $user_id;
                $item->is_alcoho = $item_param['is_alcoho'];
                $item->food_type = $item_param['food_type'];
                $item->save();
                
                //insert new mst_item_category
                $this->insertMstItemCategory($item->id, $item_param['category_id'], $user_id);
                foreach ($option_items as $option_item) {
	                //insert new mst_item_option
                	$this->insertMstItemOption($item->id, $option_item['option_id'], $user_id);
	                //insert new mst_item_option_value
	                $this->insertMstItemOptionValue($item->id, $option_item, $user_id);
                }
                $res['item_id'] = $item->id;
            } else {
                $item = MstItem::where('id',$id)->where('del_flg','0')->first();
                if (!empty($item)) {
                    if ($item_param['is_delete'] != 1) {
                    	if($item_param['update_in_lst_flg']==1){
                            $item->active_flg = $item_param['is_active'];
                            $item->save();
                        }else{
                            $item->name = $item_param['name'];
                            if($item_param['thumb']!=null || !empty($item_param['thumb'])){
                                $item->thumb = $item_param['thumb'];
                            }
                            // $item->thumb = $item_param['thumb'];
                            $item->sku = $item_param['sku'];
                            $item->price = $item_param['price'];
                            $item->point = $item_param['point'];
                            $item->start_date = $mytime;
                            Log::info($item_param['is_active']);
                            $item->active_flg = $item_param['is_active'];
                            $item->description = $item_param['description'];
                            $item->mod_user_id = $user_id;
                            $item->version_no = $item->version_no + 1;
                            $item->is_alcoho = $item_param['is_alcoho'];
                            $item->food_type = $item_param['food_type'];
                            $item->save();
                            //Delete old mst_item_category
                            MstItemCategory::where('item_id',$id)->delete();
                            //insert new mst_item_category
                            $this->insertMstItemCategory($id, $item_param['category_id'], $user_id);
                            //Delete old mst_item_option
                            MstItemOption::where('item_id', $id)->delete();
                            //Delete old mst_item_option_value
                            MstItemOptionValue::where('item_id', $id)->delete();
                            foreach ($option_items as $option_item) {
                                //insert new mst_item_option
                                $this->insertMstItemOption($id, $option_item['option_id'], $user_id);
                                //insert into mst_item_option_value
                                $this->insertMstItemOptionValue($id, $option_item, $user_id);
                            }
                        }
                    } else {
                        
                        $item->del_flg = '1';
                        $item->mod_user_id = $user_id;
                        $item->version_no = $item->version_no + 1;
                        $item->save();
                        MstItemCategory::where('item_id',$id)->update(array('del_flg' => 1));
                        MstItemOption::where('item_id',$id)->update(array('del_flg' => 1));
                        MstItemOptionValue::where('item_id',$id)->update(array('del_flg' => 1));
                    }
                    $res['item_id'] = $item->id;
                }
            }
            DB::commit();
        } catch(QueryException $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
        }
        return response()->json($res);
    }
    
    private function insertMstItemCategory($id, $category_id, $user_id) {
    	 
    	$item_category = MstItemCategory::where('category_id', '=', $category_id)
    	->where('item_id', '=', $id)
    	->first();
    	if (empty($item_category)) {
    		$item_category = new MstItemCategory;
    		$item_category->category_id = $category_id;
    		$item_category->item_id = $id;
    		$item_category->cre_user_id = $user_id;
    		$item_category->mod_user_id = $user_id;
    		$item_category->is_active = '1';
    		$item_category->save();
    	}
    }
    private function insertMstItemOption($id, $option_id, $user_id) {
    	
    	$item_option = MstItemOption::where('option_id', '=', $option_id)
							    	->where('item_id', '=', $id)
							    	->first();
    	if (empty($item_option)) {
	    	$item_option = new MstItemOption;
	    	$item_option->option_id = $option_id;
	    	$item_option->item_id = $id;
	    	$item_option->cre_user_id = $user_id;
	    	$item_option->mod_user_id = $user_id;
	    	$item_option->save();
    	}
    }
    private function insertMstItemOptionValue($id, $option_item, $user_id) {
    	
    	$item_option_value = MstItemOptionValue::where('option_id', '=', $option_item['option_id'])
									    	->where('item_id', '=', $id)
									    	->where('option_value_id', '=', $option_item['option_value_id'])
									    	->first();
    	if (empty($item_option_value)) {
	    	$item_option_value = new MstItemOptionValue;
	    	$item_option_value->item_id = $id;
	    	$item_option_value->option_id = $option_item['option_id'];
	    	$item_option_value->option_value_id = $option_item['option_value_id'];
	    	$item_option_value->active_flg = $option_item['active_flg'];
	    	$item_option_value->add_price = $option_item['add_price'];
            $item_option_value->point = $option_item['point'];
	    	$item_option_value->cre_user_id = $user_id;
	    	$item_option_value->mod_user_id = $user_id;
	    	$item_option_value->save();
    	}
    }
}