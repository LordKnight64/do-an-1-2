<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use stdClass;

use App\Services\MobileApp\UserService;
use App\Services\EmailService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Signup Controller
 */
class SignupController extends BaseController
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * [signup description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function signup(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'email'    => 'required|email'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Insert user
      $user = $this->userService->get_user_by_email($request['email']);
      if(empty($user)){
        //2.1: Set data to user and generate password
        $user = new stdClass();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->mobile = $request['mobile'];
        $user->password =   $request['password'];

        //2.2: Insert user, sent mail and return user_id
        DB::beginTransaction();
        try {
          //2.1: Insert user
          $userid = $this->userService->insert_user_get_id($user);

          //2.2: Sent email to user
          EmailService::register($user, $user->password);

          //2.3: return user_id
          $result = $this->ok("OK");
          $result['user_id'] = $userid;
          DB::commit();
        } catch (\Exception $e) {
          DB::rollBack();
          $result =  $this->fail($e->getMessage());
        }
      }else{
        $result =  $this->fail($e->getMessage());
      }

      return response()->json($result);
    }
}
