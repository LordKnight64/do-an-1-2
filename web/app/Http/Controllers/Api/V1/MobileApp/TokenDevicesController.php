<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use stdClass;

use App\Services\MobileApp\TokenDevicesService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * TokenDevices Controller
 */
class TokenDevicesController extends BaseController
{
    protected $tokenDevicesService;

    public function __construct(TokenDevicesService $tokenDevicesService)
    {
        $this->tokenDevicesService = $tokenDevicesService;
    }

    /**
     * [subscribe description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function subscribe(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'uuid' => 'required',
          'token' => 'required',
          'os' => 'required'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Insert TokenDevices
      try{
        // 2.1: Create $tokenDevices from $request
        $tokenDevices = new stdClass;
        $tokenDevices->uuid = $request['uuid'];
        $tokenDevices->token = $request['token'];
        $tokenDevices->os = $request['os'];

        //2.2: Update or insert TokenDevices
        DB::beginTransaction();
        try {
          $updateFlg = $this->tokenDevicesService->update_subscribe($tokenDevices);
          if($updateFlg == 0){
            $this->tokenDevicesService->insert_subscribe_get_id($tokenDevices);
          }
          DB::commit();
        } catch (\Exception $e) {
          DB::rollBack();
        }
        
        $result = $this->ok("OK");
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }
      return response()->json($result);
    }
}
