<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * UpdateProfile Controller
 */
class UpdateProfileController extends BaseController
{
    /**
     * update_profile [Change password user.]
     *
     * @param $request [mobile, email, first_name, last_name, tel]
     *
     * @return JSON return_cd
     */
    public function update_profile(Request $request) {

        $user_info = $request['user_info'];
        $user = $this->getCurrentUser();

        //1: Validate request
        $validator = $this->validate($request, [
            'user_info.phone' => 'min:6',
            'user_info.email' => 'email|min:6',
            'user_info.tel' => 'min:6'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
            // return response()->json($this->formatValidationErrors($validator));
        }

        //2: Change profile user
        $user->mobile = $user_info['mobile'];
        $user->email = $user_info['email'];
        $user->first_name = $user_info['first_name'];
        $user->last_name = $user_info['last_name'];
        $user->tel = $user_info['tel'];

        //3: Update profile user in DB
        try{
          $user->save();
          $result = $this->ok("OK");
         //$token = JWTAuth::getToken();          
          //$result['token'] = $token."
        }
        catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }
}
