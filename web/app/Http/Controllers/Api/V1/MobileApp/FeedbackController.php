<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Services\MobileApp\FeedbackService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Feedback Controller
 */
class FeedbackController extends BaseController
{
    protected $feedbackService;

    public function __construct(FeedbackService $feedbackService)
    {
        $this->feedbackService = $feedbackService;
    }

    /**
     * [send_feedback description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function send_feedback(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'user_id' => 'required',
          'content' => 'required'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Insert Feedback
      try{
        $this->feedbackService->insert_feedback_get_id($request['user_id'], $request['content']);
        $result = $this->ok("OK");
         //$token = JWTAuth::getToken();          
          //$result['token'] = $token."";
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }
      return response()->json($result);
    }
}
