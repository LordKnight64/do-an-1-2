<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;

use App\Http\Controllers\Api\V1\Controller as BaseController;

class LogoutController extends BaseController
{
    /**
     * Authenticate user.
     *
     * @param request
     *
     * @return JSON return_cd
     */
    public function logout(Request $request) {
        // Auth::logout();
        JWTAuth::invalidate($request->token);
        $result = $this->ok("OK");
        return response()->json($result);
    }
}
