<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use stdClass;

use App\Http\Controllers\Api\V1\Controller as BaseController;
use App\Services\MobileApp\UserService;
use App\Services\MobileApp\UserDevicesService;
use App\Models\MstUser;
use App\Models\MstRole;
use App\Models\MstPermission;
use App\Models\MobileApp\User_Info;
use App\Models\MobileApp\Roles;
use App\Services\EmailService;

class LoginController extends BaseController
{
    protected $userService;
    protected $userDevicesService;
    public function __construct(UserService $userService, UserDevicesService $userDevicesService)
    {
        $this->userService = $userService;
        $this->userDevicesService = $userDevicesService;
    }

    /**
     * Authenticate user.
     *
     * @param request [username, password, uuid, os]
     *
     * @return JSON user details and auth credentials
     */
    public function login(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'username'    => 'required|email',
            'password' => 'required',
            'uuid' => 'required',
            'os' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Check error token
        $credentials = $request->only('password');
        $credentials['email'] = $request['username'];
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->buildFailedLoginResponse($request, ['Invalid credentials']);
            }
        } catch (\JWTException $e) {
            return $this->buildFailedLoginResponse($request, ['Could not create token']);
        }

        //3: Check email verify
        $user = MstUser::whereEmail($credentials['email'])->first();
        if (isset($user->email_verified) && $user->email_verified == 0) {
            return $this->buildFailedLoginResponse($request, ['Email Unverified']);
        }

        //4: Update or insert User Device
        DB::beginTransaction();
        try {
          $updateFlg = $this->userDevicesService->update($request['uuid'], $request['os']);
          if($updateFlg == 0){
            $this->userDevicesService->insert($request['uuid'], $request['os']);
          }
          DB::commit();
        } catch (\Exception $e) {
          DB::rollBack();
        }

        //5.1: Generate token
        $token = JWTAuth::fromUser($user);

        //5.2: Get detail user
        $tmp = $this->getCurrentUser($token);
        $user_info = new User_Info();
        $user_info->id = $tmp->id;
        $user_info->mobile = $tmp->mobile;
        $user_info->email = $tmp->email;
        $user_info->last_name = $tmp->last_name;
        $user_info->first_name = $tmp->first_name;
        $user_info->tel = $tmp->tel;
        $user_info->accumulative_point = $tmp->accumulative_point;

        //5.3: Add Roles to user        

        $user_info->roles = DB::table('mst_role_user as a')
                    ->join('mst_role as b', 'a.role_id', '=', 'b.id')
                    ->where('a.user_id', $tmp->id)
                    ->select('a.role_id', 'b.name')
                    ->get();      

       //  Log::info("An test login -----------");
       //  Log::info(json_encode($user_info));
         $user_info->restaurants = DB::table('mst_user_restaurant as a')
                    ->join('mst_restaurant as b', 'a.restaurant_id', '=', 'b.id')
                    ->where('a.user_id', $tmp->id)
                    ->where('a.is_active', 1)
                    ->select('a.restaurant_id', 'b.name')
                    ->get();

        $result = $this->ok("OK");
        $result['token'] = $token;
        $result['user_info'] = $user_info;

        return response()->json($result);
    }

    public function request_login(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'email'    => 'required|email',
            'access_token' => 'required',
            'provider_id' => 'required',
            'uuid' => 'required',
            'os' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Check email exist and create user if not exist
        $user = $this->userService->get_user_by_email($request['email']);
        if(empty($user)){
          //2.1: Set data to user and generate password
          $user = new stdClass();
          $user->first_name = $request['first_name'];
          $user->last_name = $request['last_name'];
          $user->email = $request['email'];
          $user->mobile = $request['mobile'];
          $user->oauth_provider = $request['access_token'];
          $user->oauth_provider_id = $request['provider_id'];
          $user->password = str_random(6);

          //2.2: Insert user and return user_id
          DB::beginTransaction();
          try {
            $this->userService->insert_user_get_id($user);
            DB::commit();
          } catch (\Exception $e) {
            DB::rollBack();
          }

          //2.3: Sent email to user
          EmailService::request_login($user, $user->password);
        }

        //3: Generate token
        $user = MstUser::whereEmail($request['email'])->first();
        $token = JWTAuth::fromUser($user);

        //4: Update or insert user device
        DB::beginTransaction();
        try {
          $updateFlg = $this->userDevicesService->update($request['uuid'], $request['os']);
          if($updateFlg == 0){
            $this->userDevicesService->insert($request['uuid'], $request['os']);
          }
          DB::commit();
        } catch (\Exception $e) {
          DB::rollBack();
        }

        //5.1: Get detail user
        $tmp = $this->getCurrentUser($token);
        $user_info = new User_Info();
        $user_info->id = $tmp->id;
        $user_info->mobile = $tmp->mobile;
        $user_info->email = $tmp->email;
        $user_info->last_name = $tmp->last_name;
        $user_info->first_name = $tmp->first_name;
        $user_info->tel = $tmp->tel;

        //5.2: Add userRoles to user
        $userRole = [];
        foreach ($user->roles as $role) {
            $iUserRole = new Roles;
            $iUserRole->id = $role->id;
            $iUserRole->name = $role->name;
            $userRole[] = $iUserRole; // array_push($userRole, $iUserRole);
        }
        $user_info->roles = $userRole;

        //6: Set result
        $result = $this->ok("OK");
        $result['token'] = $token;
        $result['user_info'] = $user_info;

        return response()->json($result);
    }
}
