<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Log;
use DB;
use stdClass;

use App\Services\EmailService;
use App\Services\MobileApp\ContactService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Contact Controller
 */
class ContactController extends BaseController
{
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function send_contact(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'content' => 'required',
            'phone' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Insert contact and send mail to user
        DB::beginTransaction();
        try{
          //2.1: Create contact from request
          $contact = new stdClass;
          $contact->name = $request['name'];
          $contact->email = $request['email'];
          $contact->content = $request['content'];
          $contact->phone = $request['phone'];

          //2.2: Insert from $contact and return contact_id
          $contact->id = $this->contactService->insert_contact_get_id($contact);

          //2.3: Send email to user
          EmailService::send_contact($contact);
          DB::commit();
          $result = $this->ok("OK");
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
          DB::rollBack();
        }
        return response()->json($result);
    }
}
