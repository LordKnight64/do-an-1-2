<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * [forgot password controler]
 * @return [type]
 */
class ForgotPasswordController extends BaseController
{
  use SendsPasswordResetEmails;
    /**
     * Forgot password user.
     *
     * @param request [email]
     *
     * @return JSON return_cd
     */
    public function forgot_password(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'email' => 'required'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Send email reset_link
      try{
          $this->sendResetLinkEmail($request);
          $result = $this->ok("OK");
      }
      catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }

      return response()->json($result);
    }
}
