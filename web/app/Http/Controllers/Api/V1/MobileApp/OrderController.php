<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use stdClass;

use App\Services\EmailService;
use App\Services\MobileApp\UserService;
use App\Services\MobileApp\TokenDevicesService;
use App\Services\MobileApp\RestaurantService;
use App\Services\MobileApp\OrderService;
use App\Services\MobileApp\OptionService;
use App\Services\MobileApp\PushNotificationService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Order Controller
 */
class OrderController extends BaseController
{
    protected $userService;
    protected $tokenDevicesService;
    protected $restaurantService;
    protected $orderService;
    protected $optionService;

    public function __construct(UserService $userService,
                                TokenDevicesService $tokenDevicesService,
                                RestaurantService $restaurantService,
                                OrderService $orderService,
                                OptionService $optionService)
    {
        $this->userService = $userService;
        $this->tokenDevicesService = $tokenDevicesService;
        $this->restaurantService = $restaurantService;
        $this->orderService = $orderService;
        $this->optionService = $optionService;
    }

    /**
  	 * get orders by userid
  	 *
  	 * @param request [user_id]
  	 *
  	 * @return [type]
  	 */
    public function get_orders_by_userid(Request $request) {

        //1: Validate request
        //todo: (validate temporary required for user_id)
        $validator = $this->validate($request, [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: get Orders by user_id
        try{
          $result = $this->ok("OK");        
        
          $result['orders'] = $this->orderService->get_orders_by_userid($request['user_id']);
					foreach ($result['orders'] as $order) {
						$order->items = $this->orderService->get_list_detail($order->id);
            foreach ($order->items as $item) {
      				$item->option_items = $this->optionService->get_option_value_by_orderId_itemId($order->id, $item->item_id);
      			}
					}
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }
         //$token = JWTAuth::getToken();          
          //$result['token'] = $token."";   
         Log::info(json_encode($result));  
        return response()->json($result);
    }

    /**
  	 * get orders by restid
  	 *
  	 * @param request [user_id, rest_id]
  	 *
  	 * @return [type]
  	 */
    public function get_orders_by_restid(Request $request) {

        //1: Validate request
        //todo: (validate temporary required for user_id, rest_id)
        $validator = $this->validate($request, [
            'user_id' => 'required',
            'rest_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Get Order by restaurant_id
        try{
          $result = $this->ok("OK");
          $result['orders'] = $this->orderService->get_orders_by_restid($request['user_id'], $request['rest_id']);
          foreach ($result['orders'] as $order) {
						$order->items = $this->orderService->get_list_detail($order->id);
            foreach ($element->items as $item) {
      				$item->option_items = $this->optionService->get_option_value_by_orderId_itemId($order->id, $item->item_id);
      			}
					}
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }

    /**
  	 * get orders by userid
  	 *
  	 * @param request [user_id]
  	 *
  	 * @return [type]
  	 */
    public function request_order_by_user_id(Request $request) {

        //1: Validate request
        //todo: (validate temporary required for user_id)
        $validator = $this->validate($request, [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Insert tables relationship with Order
        DB::beginTransaction();
        try{

          //2.1: Insert order
          $order = new stdClass();
          $order->user_id = $request['user_id'];
          $order->restaurant_id = $request['res_id'];
          $order->price = $request['price'];
          $order->notes = $request['notes'];
          $order->delivery_ts = $request['delivery_time'];
          $order->delivery_fee = $request['delivery_fee'];
          $order->email = $request['email'];
          $order->phone = $request['phone'];
          $order->address = $request['address'];
        

           $order->email = $request['email'];
           $order->phone = $request['mobile'];
           $order->name = $request['name'];
           $order->delivery_type = $request['delivery_type'];
           $order->promo_code = $request['promo_code'];
           $order->promo_discount = $request['promo_discount'];
           $order->delivery_distance = $request['delivery_distance'];
              

					$orderId = $this->orderService->insert_order_get_id($order);

          //2.2: Insert order detail
          foreach ($request['items'] as $item) {
            $orderDetail = new stdClass();
						$orderDetail->order_id = $orderId;
            $orderDetail->item_id = $item['item_id'];
            $orderDetail->seq_no = $item['seq_no'];
            $orderDetail->quantity = $item['quantity'];
            $orderDetail->price = $item['price'];
            $orderDetail->point = $item['point'];
            $orderDetail->notes = $item['notes'];
						$this->orderService->insert_order_detail_get_id($orderDetail);

            //2.3: Insert order option value
            foreach ($item['option_items'] as $option_item) {
             $orderOptionValue = new stdClass();
             $orderOptionValue->order_id = $orderDetail->order_id;
             $orderOptionValue->item_id = $orderDetail->item_id;
             $orderOptionValue->option_value_id = $option_item['option_value_id'];
             $orderOptionValue->option_id = $option_item['option_id'];
             $orderOptionValue->point = $option_item['point'];
   					 $this->orderService->insert_order_option_value($orderOptionValue);
   				 }
				 }

        DB::commit();
        $result = $this->ok("OK");
        $result['res_id'] = $request['res_id'];
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
          DB::rollBack();
        }

        return response()->json($result);
    }

    /**
     * [update_order_by_id update order by id]
     * @param  Request $request [order_id, status]
     * @return [type]           [description]
     */
    public function update_order_by_id(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'order_id' => 'required',
            'status'   => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: update Order's status by order_id
        DB::beginTransaction();
        try{
          //2.1: Update status order
          $this->orderService->update_status_by_id($request['order_id'], $request['status']);

          //2.2: Get infomation for sent email and push notification
          $order = $this->orderService->get_order_by_id($request['order_id']);
          $order->items = $this->orderService->get_list_detail($order->user_id);
          $restaurant = $this->restaurantService->get_restaurant_detail($order->restaurant_id);
          if(isset($order->user_id) && !empty($order->user_id)){
            $user = $this->userService->get_user_by_id($order->user_id);
            $deviceTokens = $this->tokenDevicesService->get_detail_by_user_id($order->user_id);
          }

          //2.3: Caculate end sprice
          $order->end_price = $order->total_price - $order->delivery_fee - $order->total_price * $restaurant->promo_discount;

          //2.4: Sent email and push notification
          switch ($request['status']) {
      			case 1:
              EmailService::order($user, $restaurant, $order);
              foreach ($deviceTokens as $item) {
                $title = "Tile: Order";
                $message = "Đơn hàng ". $order->id . " đã được " . $restaurant->name . " duyệt.";
                PushNotificationService::Message($item->os, $item->token, $title, $message);
              }
      				break;
      			case 2:
              foreach ($deviceTokens as $item) {
                $title = "Tile: Order";
                $message = "Đơn hàng " . $order->id . " đang được " . $restaurant->name . " giao.";
                PushNotificationService::Message($item->os, $item->token, $title, $message);
              }
      				break;
      			case 99:
              foreach ($deviceTokens as $item) {
                $title = "Tile: Order";
                $message = "Đơn hàng " . $order->id . " đã bị " . $restaurant->name . " hũy.";
                PushNotificationService::Message($item->os, $item->token, $title, $message);
              }
      				break;
      			default:
      				# code...
      				break;
      		}

          //3: Set result
          DB::commit();
					$result = $this->ok("OK");
					$result['order_id'] = $request['order_id'];
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
          DB::rollBack();
        }

        return response()->json($result);
    }

     /**
     * [update_order_by_id update order by id]
     * @param  Request $request [order_id, status]
     * @return [type]           [description]
     */
    public function set_favorite_by_id(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'order_id' => 'required',
            'is_favorite'   => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: update Order's status by order_id
        DB::beginTransaction();
        try{
          //2.1: Update status order
          $this->orderService->set_favorite_by_id($request['order_id'], $request['is_favorite']);

          //3: Set result
          DB::commit();
					$result = $this->ok("OK");
					$result['order_id'] = $request['order_id'];
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
          DB::rollBack();
        }

        return response()->json($result);
    }

      /**
     * [update_order_by_id update order by id]
     * @param  Request $request [order_id, status]
     * @return [type]           [description]
     */
    public function get_point(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'order_id' => 'required',
            'point'   => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: update Order's status by order_id
        DB::beginTransaction();
        try{
          //2.1: Update status order
          $this->orderService->get_point($request['order_id'], $request['point']);

          //3: Set result
          DB::commit();
					$result = $this->ok("OK");
					$result['order_id'] = $request['order_id'];
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
          DB::rollBack();
        }

        return response()->json($result);
    }
}
