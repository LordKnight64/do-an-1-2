<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Services\MobileApp\BestSaleService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * BestSale Controller
 */
class BestSaleController extends BaseController
{
    protected $bestSaleService;

    public function __construct(BestSaleService $bestSaleService)
    {
        $this->bestSaleService = $bestSaleService;
    }

    /**
  	 * get best sale by restid
  	 *
  	 * @param request [user_id, rest_id, count]
  	 *
  	 * @return [type]
  	 */
    public function get_best_sale_by_restid(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'user_id' => 'required',
            'rest_id' => 'required',
            'count' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Get best sale by restaurant id
        try{
          $result = $this->ok("OK");
          //$token = JWTAuth::getToken();          
          //$result['token'] = $token."";
		  $result['rest_id'] = $request['rest_id'];
          $result['items'] = $this->bestSaleService->get_best_sale_by_restid($request['user_id'], $request['rest_id'], $request['count']);
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }
}
