<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use stdClass;

use App\Services\MobileApp\AddressService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Address Controller
 */
class AddressController extends BaseController
{
    protected $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    /**
  	 * get address list
  	 *
  	 * @param request [user_id]
  	 *
  	 * @return [type]
  	 */
    public function get_address_list(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Get list address from service
        try{
          $result = $this->ok("OK");
          //$token = JWTAuth::getToken();          
          //$result['token'] = $token."";
          $result['address_list'] = $this->addressService->get_address_list($request['user_id']);
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }

    /**
  	 * update address
  	 *
  	 * @param request [user_id, address_info, id, name, user_id, lat_val, long_val, tel, address]
  	 *
  	 * @return [type]
  	 */
    public function update_address(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Create $address to update address from service
        DB::beginTransaction();
        try{
          //2.1: Create  $address
          $address = new stdClass();
          $address->id = $request['id'];
          $address->name = $request['name'];
          $address->user_id = $request['user_id'];
          $address->lat_val = $request['lat_val'];
          $address->long_val = $request['long_val'];
          $address->tel = $request['tel'];
          $address->address = $request['address'];
          $token = JWTAuth::getToken();          
          //2.2: Update address from service
            $result = $this->ok("OK");
            $result['token'] = $token;
            $result['address_id'] = $this->addressService->update_address($address);
          DB::commit();
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
          DB::rollBack();
        }

        return response()->json($result);
    }
}
