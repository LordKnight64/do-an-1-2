<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use Illuminate\Foundation\Auth\ResetsPasswords;

use App\Http\Controllers\Api\V1\Controller as BaseController;

class ResetPasswordController extends BaseController
{
  use ResetsPasswords;
  protected $redirectTo = '/home';

    /**
     * Post reset password user.
     *
     * @param Instance Request instance
     *
     * @return JSON return_cd
     */
    public function post_reset(Request $request) {
        $return_cd = 1;

        // $validator = $this->validate($request, [
        //     'email'    => 'required|email|min:6',
        //     'password' => 'required|min:6',
        //     'token' => 'required'
        // ]);
        //
        // if ($validator->fails()) {
        //     return $this->buildFailedValidationResponse(
        //         $request, $this->formatValidationErrors($validator));
        //     // return response()->json($this->formatValidationErrors($validator));
        // }

        try{
            $aaa = $this->reset($request);
            $return_cd = 0;
        }
        catch(\Exception $e){
        }

        return response()->json(compact('$return_cd'));
    }

    /**
     * Get reset password user.
     *
     * @param request [ new_password, confirm_passwrod]
     *
     * @return JSON return_cd
     */
    public function get_reset(Request $request) {
        return $this->showResetForm($request);
    }
}
