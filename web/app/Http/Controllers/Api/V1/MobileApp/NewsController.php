<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Services\MobileApp\NewsService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * News Controller
 */
class NewsController extends BaseController
{
    protected $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
  	 * get news
  	 *
  	 * @param request [ uuid]
  	 *
  	 * @return [type]
  	 */
    public function get_news(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'from' => 'required',
          'to' => 'required'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Get news by from, to
      try{
        $result = $this->ok("OK");
        $offset = $request['from'];
        $limit = $request['to'] - $request['from'];
        $result['news'] = $this->newsService->get_news($offset, $limit);
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }

      return response()->json($result);
    }

    /**
     * [get_total_news description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function get_total_news(Request $request) {

      try{
        $result = $this->ok("OK");
        $result['total'] = $this->newsService->get_total_news();
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }

      return response()->json($result);
    }
}
