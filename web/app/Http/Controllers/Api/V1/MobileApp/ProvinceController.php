<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Services\MobileApp\ProvinceService;
use App\Services\GeoLocationService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Province Controller
 */
class ProvinceController extends BaseController
{
    protected $provinceService;
    protected $geoLocationService;

    public function __construct(ProvinceService $provinceService, GeoLocationService $geoLocationService)
    {
        $this->provinceService = $provinceService;
        $this->geoLocationService = $geoLocationService;
    }

    public function get_provinces_by_city(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'city' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Get list province
        try{
          $result = $this->ok("OK");
          $result['provinces'] = $this->provinceService->get_provinces_by_city($request['city']);
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }

    public function get_provinces_by_axis(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'lat_val' => 'required',
          'long_val' => 'required'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }
      //2: Get list province
      try{
        $location = $this->geoLocationService->get_location_info_by_lat_long($request['lat_val'], $request['long_val']);
        $result = $this->ok("OK");
        $city = $location["city"];
        Log::debug('---------location-------', $location);
        if(!empty($city)){
          $result['provinces'] = $this->provinceService->get_provinces_by_city($city);
          $result['city'] = $city;
          $result['currentProvince'] = $location["province"];
        }
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }

      return response()->json($result);
    }
}
