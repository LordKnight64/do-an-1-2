<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Services\MobileApp\NotificationsService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Notifications Controller
 */
class NotificationsController extends BaseController
{
    protected $notificationsService;

    public function __construct(NotificationsService $notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }

    /**
  	 * get notifications
  	 *
  	 * @param request [user_id, uuid]
  	 *
  	 * @return [type]
  	 */
    public function get_notifications(Request $request) {

      //1: Validate request
      $validator = $this->validate($request, [
          'user_id' => 'required'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Get notifications
      try{
        $result = $this->ok("OK");
         //$token = JWTAuth::getToken();          
          //$result['token'] = $token."";
        $result['notifications'] = $this->notificationsService->get_notifications($request['user_id']);
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
      }

      return response()->json($result);
    }
}
