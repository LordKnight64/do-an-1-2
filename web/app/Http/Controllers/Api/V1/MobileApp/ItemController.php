<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

use App\Services\MobileApp\ItemService;
use App\Services\MobileApp\OptionService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

/**
 * Address Controller
 */
class ItemController extends BaseController
{
    protected $itemService;
    protected $optionService;

    public function __construct(ItemService $itemService, OptionService $optionService)
    {
        $this->itemService = $itemService;
        $this->optionService = $optionService;
    }

    /**
  	 * get item detail by id
  	 *
  	 * @param request [item_id]
  	 *
  	 * @return [type]
  	 */
    public function get_item_detail_by_id(Request $request) {

        //1: Validate request
        $validator = $this->validate($request, [
            'item_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2: Get item detail by id
        try{
          $result = $this->ok("OK");
          $result['item'] = $this->itemService->get_item_detail_by_id($request['item_id']);
          foreach($result['item'] as $element){
            $element->option_items = $this->optionService->get_option_value_by_itemId($element->id);
          }
        }catch(\Exception $e){
          $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }
}
