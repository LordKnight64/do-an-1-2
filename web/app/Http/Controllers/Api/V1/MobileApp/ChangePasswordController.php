<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use App\Services\EmailService;

use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Api\V1\Controller as BaseController;

class ChangePasswordController extends BaseController
{
    /**
     * Change password user.
     *
     * @param Request $request [old_password, new_password, confirm_passwrod	]
     *
     * @return JSON return_cd
     */
    public function change_password(Request $request) {

        $user = $this->getCurrentUser();

        //1: Validate request
        $validator = $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        //2.1: Check password
        if (Hash::check($request['old_password'], $user->password)) {
          //2.2: Check new password and confirm password does not match
          if($request['new_password'] == $request['confirm_password']){
            //2.3: Change password
            $user->fill([
                'password' => Hash::make($request['new_password'])
            ])->save();

            //2.4: Sent email to user
            EmailService::change_password($user, $request['new_password']);
            $result = $this->ok("OK");
            //$token = JWTAuth::getToken();          
          //$result['token'] = $token."";
          }else{
            $result =  $this->fail("New password and confirm password does not match.");
          }
        }else{
            $result =  $this->fail("Password incorrect.");
        }

        return response()->json($result);
    }
}
