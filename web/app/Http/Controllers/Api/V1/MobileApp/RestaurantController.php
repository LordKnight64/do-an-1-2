<?php
namespace App\Http\Controllers\Api\V1\MobileApp;

use App\Http\Controllers\Api\V1\Controller as BaseController;

use App\Services\GeoLocationService;
use App\Services\MobileApp\CategoryService;
use App\Services\MobileApp\ItemService;
use App\Services\MobileApp\OptionService;
use App\Services\MobileApp\RestaurantService;
use Dingo\Api\Http\Request;
use Log;

//Model
use App\Models\MstRestaurant;
use App\Models\MobileApp\OrderingSetting;

/**
 * Restaurant Controller
 */
class RestaurantController extends BaseController {
	protected $restaurantService;
	protected $categoryService;
	protected $itemService;
	protected $optionService;
	protected $geoLocationService;

	public function __construct(RestaurantService $restaurantService,
		CategoryService $categoryService,
		ItemService $itemService,
		OptionService $optionService,
		GeoLocationService $geoLocationService) {
		$this->restaurantService = $restaurantService;
		$this->categoryService = $categoryService;
		$this->itemService = $itemService;
		$this->optionService = $optionService;
		$this->geoLocationService = $geoLocationService;
	}

	/**
	 * get restaurant detail
	 *
	 * @param request [rest_id]
	 *
	 * @return [type]
	 */
	public function get_restaurant_detail(Request $request) {

		//1: Validate request
		$validator = $this->validate($request, [
			'res_id' => 'required',
		]);
		if ($validator->fails()) {
			return $this->buildFailedValidationResponse(
				$request, $this->formatValidationErrors($validator));
		}

		//2: Get restaurant detail by restaurant_id
		try {
			$restaurant = $this->get_restaurant($request['res_id']);
			$result = $this->ok("OK");
			$result['restaurant'] = $restaurant;
		} catch (\Exception $e) {
			$result = $this->fail($e->getMessage());
		}

		return response()->json($result);
	}

	/**
	 * [get_restaurants_by_province description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function get_restaurants_by_province(Request $request) {

		//1: Validate request
		$validator = $this->validate($request, [
			'province' => 'required',
		]);
		if ($validator->fails()) {
			return $this->buildFailedValidationResponse(
				$request, $this->formatValidationErrors($validator));
		}

		//2: Get Restaurants by provice
		try {
			//2.1: Get restaurant detail
			// Log::debug('**************get_restaurants_by_province');
			// $get_restaurant_by_province_params = array('province' => $request['province']);

			$restaurants = $this->restaurantService->find_restaurant($request);
			// Log::debugdata' . $restaurants);
			// $restaurants = $this->restaurantService->get_restaurant_by_province($request['province']);

			//2.2: Get ordering_setting and catogories
			foreach ($restaurants as $res) {
				$ordering_settings = $this->restaurantService->get_ordering_setting($res->id);
				$res->ordering_setting = new OrderingSetting;
				if (isset($ordering_settings) && count($ordering_settings) > 0) {
					$res->ordering_setting = $ordering_settings[0];
				}

				//  $res->categories = $this->categoryService->get_category_by_restaurantId($res->id);
				//  //2.2.1: Get items in category
				//  foreach($res->categories as $category){
				//   $category->items = $this->itemService->get_item_detail_by_categoryId($category->id);

				//   //2.2.1.1: Get Option_value in item
				//   foreach($category->items as $item){
				//     $item->option_items = $this->optionService->get_option_value_by_itemId($item->id);
				//   }
				// }
			}

			//2.3: Set result
			$result = $this->ok("OK");
			$result['restaurants'] = $restaurants;
		} catch (\Exception $e) {
			$result = $this->fail($e->getMessage());
		}

		return response()->json($result);
	}

	/**
	 * [get_restaurants_by_province description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function find_restaurants(Request $request) {

		//1: Validate request
		// $validator = $this->validate($request, [
		// 	'name' => 'required',
		// ]);
		// if ($validator->fails()) {
		// 	return $this->buildFailedValidationResponse(
		// 		$request, $this->formatValidationErrors($validator));
		// }

		//2: Get Restaurants by provice
		try {
			//2.1: Get restaurant detail
			// Log::debug('**************get_restaurants_by_province');
			// $get_restaurant_by_province_params = array('province' => $request['province']);

			$restaurants = $this->restaurantService->find_restaurant($request);
			// Log::debugdata' . $restaurants);
			// $restaurants = $this->restaurantService->get_restaurant_by_province($request['province']);

			//2.2: Get ordering_setting and catogories
			foreach ($restaurants as $res) {
				$ordering_settings = $this->restaurantService->get_ordering_setting($res->id);
				$res->ordering_setting = new OrderingSetting;
				if (isset($ordering_settings) && count($ordering_settings) > 0) {
					$res->ordering_setting = $ordering_settings[0];
				}

				//  $res->categories = $this->categoryService->get_category_by_restaurantId($res->id);
				//  //2.2.1: Get items in category
				//  foreach($res->categories as $category){
				//   $category->items = $this->itemService->get_item_detail_by_categoryId($category->id);

				//   //2.2.1.1: Get Option_value in item
				//   foreach($category->items as $item){
				//     $item->option_items = $this->optionService->get_option_value_by_itemId($item->id);
				//   }
				// }
			}

			//2.3: Set result
			$result = $this->ok("OK");
			$result['restaurants'] = $restaurants;
		} catch (\Exception $e) {
			$result = $this->fail($e->getMessage());
		}

		return response()->json($result);
	}

	public function get_restaurants_location(Request $request) {

		//1: Validate request
		$validator = $this->validate($request, [
			'lat_val' => 'required',
			'long_val' => 'required',
			'radius' => 'required',
		]);
		if ($validator->fails()) {
			return $this->buildFailedValidationResponse(
				$request, $this->formatValidationErrors($validator));
		}

		//2: get restaurants by location
		try {
			//2.1: Set input for function get_driving_distance
			$source['lat_val'] = $request["lat_val"];
			$source['long_val'] = $request["long_val"];
			$radius = $request["radius"];
			$destinations = $this->restaurantService->get_all_restaurant();

			//2.2: Get list restaurant by location with properties of address
			$res_locations = $this->geoLocationService->get_driving_distance($source, $destinations, $radius);

			//2.3: Get detail for restaurant in step 2.2
			$restaurants = [];
			foreach ($res_locations as $item) {
				$restaurant = $this->get_restaurant($item['id']);
				$restaurant->distance = $item['distance'];
				$restaurant->duration = $item['duration'];
				$restaurants[] = $restaurant;
			}

			//2.4 set result
			$result = $this->ok("OK");
			$result['restaurants'] = $restaurants;
		} catch (\Exception $e) {
			$result = $this->fail($e->getMessage());
		}

		return response()->json($result);
	}

	/**
	 * [get_restaurant description]
	 * @param  [type] $restaurantId [description]
	 * @return [type]               [description]
	 */
	private function get_restaurant($restaurantId) {
		//1: Get restaurant detail, ordering_setting and categories
		// $restaurant = $this->restaurantService->get_restaurant_detail($restaurantId);
    $get_restaurant_detail_param = array('id' => $restaurantId);
    $restaurants = $this->restaurantService->find_restaurant($get_restaurant_detail_param);
    
    $restaurant = new MstRestaurant;
    if(!empty($restaurants) && count($restaurants) > 0){
      $restaurant = $restaurants[0];
    }

    $ordering_settings = $this->restaurantService->get_ordering_setting($restaurantId);

    $ordering_setting = new OrderingSetting;
    if(!empty($ordering_settings) && count($ordering_settings) > 0){
      $ordering_setting = $ordering_settings[0];
    }
    
		$restaurant->ordering_setting = $ordering_setting;
		$restaurant->categories = $this->categoryService->get_category_by_restaurantId($restaurantId);

		//2: Get items in category
		foreach ($restaurant->categories as $category) {
			$category->items = $this->itemService->get_item_detail_by_categoryId($category->id);
			//2.1: Get option_value in item
			foreach ($category->items as $item) {
				$item->option_items = $this->optionService->get_option_value_by_itemId($item->id);
			}
		}

		return $restaurant;
	}

	/**
	 * [get_ordered_restaurants_by_user description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function get_ordered_restaurants_by_user(Request $request) {

		//1: Validate request
		$validator = $this->validate($request, [
			'user_id' => 'required',
		]);
		if ($validator->fails()) {
			return $this->buildFailedValidationResponse(
				$request, $this->formatValidationErrors($validator));
		}

		//2: Get Restaurants by provice
		try {
			//2.1: Get restaurant detail
			$restaurants = $this->restaurantService->get_ordered_restaurants_by_user($request['user_id']);

			//2.2: Get ordering_setting
			foreach ($restaurants as $res) {
				$res->ordering_setting = $this->restaurantService->get_ordering_setting($res->id);
			}

			//2.3: Set result
			$result = $this->ok("OK");
			$result['restaurants'] = $restaurants;
		} catch (\Exception $e) {
			$result = $this->fail($e->getMessage());
		}

		return response()->json($result);
	}
}
