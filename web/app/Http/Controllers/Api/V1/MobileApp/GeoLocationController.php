<?php

namespace App\Http\Controllers\Api\V1\MobileApp;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;
use Illuminate\Database\QueryException;


use App\Services\GeoLocationService;
use App\Http\Controllers\Api\V1\Controller as BaseController;

class GeoLocationController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeoLocationService $geoLocationService)
    {
      
        $this->geoLocationService = $geoLocationService;
    }

    public function get_coordinates(Request $request)
    {
        $param = $request->all();
        $city = $param['city'];
        $street = $param['street'];
        $province = $param['province'];
        
        $address = urlencode($city.','.$street.','.$province);
        
        $response = $this->geoLocationService->get_coordinates($address);
        $response['return_cd'] = 0;
        $response['message'] = 'OK';
        return response()->json($response);
    }
    public function get_coordinates_by_addrress(Request $request)
    {
        $param = $request->all();
      
        $address = urlencode($param['address']);
      
        $response = $this->geoLocationService->get_coordinates($address);
        $response['return_cd'] = 0;
        $response['message'] = 'OK';
        return response()->json($response);
    }

  

    public function get_distance(Request $request)
    {
        
        $param = $request->all();
        $source = $param["source"];
        $destination = $param["destination"];       

        $results['address'] = $this->geoLocationService->get_distance($source, $destination);
        $results['return_cd'] = 0;
        $results['message'] = 'OK';

        return response()->json($results);
    }
    public function get_addrress_by_latlon(Request $request)
    {
        
        $param = $request->all();
        $lat = $param["lat"];
        $lon = $param["lon"];
        $results['address']  = $this->geoLocationService->get_all_address_info($lat, $lon);
        $results['return_cd'] = 0;
        $results['message'] = 'OK';
        return response()->json($results);
    }
     public function get_data_coordinates(Request $request)
    {
        
        $param = $request->all();
        $address = urlencode($param['address']);
       $results['locations']  = $this->geoLocationService->get_data_coordinates($address);
        $results['return_cd'] = 0;
        $results['message'] = 'OK';
        return response()->json($results);
    }
}