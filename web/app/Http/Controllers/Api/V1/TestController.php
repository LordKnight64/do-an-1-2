<?php

namespace App\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Auth;
use JWTAuth;
use DB;
use Config;
use Log;

class TestController extends Controller
{
    public function test1(Request $request) {
        $param = $request->all();
        $param['cd'] = 'ok2';
        return response()->json($param);
    }

    public function test2(Request $request) {
        $user =JWTAuth::toUser();
        $param = $request->all();
        $param['cd'] = 'ok3';
        $param['user_id'] = $user->id;
        return response()->json($param);
    }
}