<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class RestaurantLoginController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','restaurantLogin');
        return view('frontend.RestaurantLogin');
    }
}
