<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class FeaturesController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','features');
        return view('frontend.features');
    }
}
