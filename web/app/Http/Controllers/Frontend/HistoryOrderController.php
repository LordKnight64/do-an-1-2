<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Carbon\Carbon;
use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Auth;
use Log;

class HistoryOrderController extends BaseController
{
    public function index() {
    if(Auth::check() == false)
    {
      return redirect('/');
    }
    $order['request'] = DB::table('trn_order')->where('user_id',Auth::user()->id)->get();
    foreach ($order['request'] as $req)
    {
      $req->delivery_t = new stdClass();
      $req->delivery_t = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('H:i:s');
      $req->delivery_ts = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('Y-m-d');
    }

    // $order['verified'] = DB::table('trn_order')->where('order_sts','1')->where('user_id','1')->get();
    // foreach ($order['request'] as $req)
    // {
    //   $req->delivery_t = new stdClass();
    //   $req->delivery_t = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('H:i:s');
    //   $req->delivery_ts = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('Y-m-d');
    // }
    //
    // $order['pending'] = DB::table('trn_order')->where('order_sts','99')->where('user_id','1')->get();
    // foreach ($order['request'] as $req)
    // {
    //   $req->delivery_t = new stdClass();
    //   $req->delivery_t = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('H:i:s');
    //   $req->delivery_ts = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('Y-m-d');
    // }
    //
    // $order['delivery'] = DB::table('trn_order')->where('order_sts','2')->where('user_id','1')->get();
    // foreach ($order['request'] as $req)
    // {
    //   $req->delivery_t = new stdClass();
    //   $req->delivery_t = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('H:i:s');
    //   $req->delivery_ts = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $req->delivery_ts)->format('Y-m-d');
    // }
        return view('frontend.historyOrder')->with($order);
    }
}
