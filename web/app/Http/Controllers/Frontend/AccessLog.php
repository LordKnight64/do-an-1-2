<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends BaseModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'access_log';

}