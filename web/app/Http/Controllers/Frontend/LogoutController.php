<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller as BaseController;

use Illuminate\Http\Request;
use Auth;
use App\Events\UserAccessEvent;
use Session;
use Log;
use App\Services\PushNotificationService;
use App\Models\MstTokenDevices;
use DB;
class LogoutController extends BaseController
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PushNotificationService $pushNotificationService)
    {
        $this->pushNotificationService = $pushNotificationService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Log::info("------------LOGOUT NE--------------------");
        if(Auth::check()){
            $user = Auth::user();
            if(isset($user)) {
                $userId =$user->id;
                Log::info('________USER ID___________'.$userId);
                //delete item of mst_token_devices
                $this->pushNotificationService->delete_for_key($userId,'web');
                event(new UserAccessEvent(Auth::user(), UserAccessEvent::LOGOUT));
            }
        }
        Session::forget('userInfo');
        Session::forget('restaurantLoginList');
        Session::forget('isSubscribed');
        Auth::logout();
        

        return redirect()->to('/');
    }
}
