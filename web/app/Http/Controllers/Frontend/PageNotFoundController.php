<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller as BaseController;

class PageNotFoundController extends BaseController
{
    public function index() {
        
        return view('frontend.404');

    }
}
