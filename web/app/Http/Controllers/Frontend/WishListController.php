<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Frontend\Controllers\BaseController;
use DB;
use Log;
use View;
use Route;
use Session;
use Auth;
use App\Http\Controllers\Frontend\StdClass;

class WishListController extends \Illuminate\Routing\Controller
{
    /**
     * Add item  wish list of user
     *
     * @return View::make('frontend.notificationList', $data)
     */
    public function index(Request $request)
    {
        if(Auth::user() != null)
        {
        $del_flag = DB::table('mst_user_restaurant_wishlist')
                        ->where('restaurant_id',$request->restaurant_id)
                        ->where('item_id',$request->item_id)
                        ->where('user_id',Auth::user()->id)
                        ->first();
        }
        else
        {
            $message_cd = "69";
            return response()->json($message_cd);
        }
        if(isset($del_flag))
        {
            Log::info($del_flag->del_flg);
            if($del_flag->del_flg == 0)
            {
                //Update Del flag = 1
                Log::info("Update Del flag = 1 ");

                DB::table('mst_user_restaurant_wishlist')
                ->where('restaurant_id',$request->restaurant_id)
                        ->where('item_id',$request->item_id)
                        ->where('user_id',Auth::user()->id)
                ->update(['del_flg' => '1']);

                $message_cd = "102";
                return response()->json($message_cd);
            }
            else
            {
                //Update Del flag = 0
                Log::info("Update Del flag = 0 ");
                DB::table('mst_user_restaurant_wishlist')
                ->where('restaurant_id',$request->restaurant_id)
                ->where('item_id',$request->item_id)
                ->where('user_id',Auth::user()->id)
                ->update(['del_flg' => '0']);
                $message_cd = "101";
                return response()->json($message_cd);
            }
        }
        else
        {
            Log::info("Không tồn tại");
            DB::table('mst_user_restaurant_wishlist')->insert(
                [
                    'restaurant_id' => $request->restaurant_id, 
                    'item_id' => $request->item_id,
                    'user_id' => Auth::user()->id,
                    'active_flg' => 1
                ]
            );
            $message_cd = "100";
            return response()->json($message_cd);
        }
        
    }


    public function deleteMyWishlist(Request $request)
    {
        DB::table('mst_user_restaurant_wishlist')
                ->where('restaurant_id',$request->restaurant_id)
                ->where('item_id',$request->item_id)
                ->where('user_id',Auth::user()->id)
                ->update(['del_flg' => '1']);

        return redirect('/myWishList');
    }


    public function loadMyWishlist()
    {
        if(Auth::user() == null)
        {
            return redirect('/');
        }
        else
        {
            Log::debug(Auth::user()->id);
            $data['wishlist'] = DB::table('mst_user_restaurant_wishlist as w')
                                ->join('mst_restaurant as r',function($join){
                                    $join->on('w.restaurant_id','=','r.id')
                                    ->where('r.del_flg','0')
                                    ->where('r.is_active','1');
                                })
                                ->join('mst_item as i',function($join){
                                    $join->on('w.item_id','=','i.id')
                                    ->where('i.del_flg','0')
                                    ->where('i.active_flg','1');
                                })
                                ->where('w.user_id',Auth::user()->id)
                                ->where('w.del_flg','0')
                                ->select('w.user_id',
                                         'w.restaurant_id',
                                         'w.item_id',
                                         'w.cre_ts',
                                         'r.name as restaurant_name',
                                         'r.food_ordering_system_type',
                                         'r.rating',
                                         'i.name as item_name',
                                         'i.description',
                                         'i.price',
                                         'i.point',
                                         'i.thumb',
                                         'r.promote_code',
                                         'r.promo_discount',
                                         'r.address',
                                         'r.delivery_charge',
                                         'r.default_currency')
                                ->get();
            
            foreach($data['wishlist'] as $w)
            {
                Log::info("$w->user_id $w->restaurant_id $w->item_id $w->cre_ts $w->restaurant_name $w->item_name $w->rating $w->price");
                
            }
        }
        return View::make('frontend.myWishList',$data);
    }

}