<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Session;
use App;
use Dingo\Api\Http\Request;
use Log;

class HomeController extends BaseController
{
    public function index()
    {
        $data['data'] = DB::table('mst_restaurant')->get();
        $data['item'] = DB::table('mst_item')->orderBy('cre_ts', 'asc')->limit(4)->get();
        Session::forget('hasSearch');
        Session::put('path', 'index');
        // App::setLocale('vi');
        Log::info('LANG DISPLAY  ... '.App::getLocale());

        $data['districts'] =  DB::table('mst_restaurant')
        ->select('province')
        ->distinct()
        ->orderBy('province', 'asc')
        ->get();
        return view('frontend.index')->with($data);
    }
    public function changeLanguage(Request $request)
    {
        Log::info('LANG CHOOSEN  ... '.$request->get("langChange"));
        App::setLocale($request->get("langChange"));
        Session::put('langChoosen', $request->get("langChange"));
        $result = $this->ok("OK");
        return response()->json($result);
    }
}
