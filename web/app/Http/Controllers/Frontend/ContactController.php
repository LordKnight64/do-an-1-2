<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Session;
use Illuminate\Http\Request;
use stdClass;
use DB;
use Log;

use App\Services\EmailService;
use App\Services\MobileApp\ContactService;
use App\Services\MobileApp\ContactRestaurantService;

class ContactController extends BaseController
{
    protected $contactService;
    protected $contactRestaurantService;

    public function __construct(ContactService $contactService, ContactRestaurantService $contactRestaurantService)
    {
        $this->contactService = $contactService;
        $this->contactRestaurantService = $contactRestaurantService;
        Log::info("CONSTRUCT");
    }

    public function index(Request $request) {
      Log::info("index");
      
      Log::debug(Session::get('path'));
      if(Session::get('path') == 'restaurantDetail' || Session::get('path')== 'about' || Session::get('path') =='contact'|| Session::get('path') =='order'|| Session::get('path') =='orderDetail')
      {
        Log::info("IF SESSION");
        if($request->id==null)
        {
          Log::info("NULL");
          $request->id = Session::get('idRes');
        }
        else Session::put('idRes',$request->id);
        
        $data['restaurant'] = DB::table('mst_restaurant')->where('id',$request->id)->first();
        Session::put('path','contact');
          return view('frontend.contact')->with($data);
        
      }
      else {
        Log::info("VĂNG");
        $data['restaurant'] = new stdClass();
        $data['restaurant']->name = '';
        $data['restaurant']->address = '#768, 5th floor, N S Building,
Csm Block, Park Road,
Bangalore - 234567';
        $data['restaurant']->mobile = '0123456789';
        $data['restaurant']->email = 'admin@abc.com';
        $data['restaurant']->open_time = ' ';
        $data['restaurant']->close_time = ' ';
        Session::put('path','contact1');

      }
        return view('frontend.contact')->with($data);
    }

    /**
     * [sent_contact description]
     * @param  Request $request [name, email,phone, content]
     * @return [type]           [description]
     */
    public function sent_contact(Request $request){
      //1: Validate request
      $validator = $this->validate($request, [
          'name' => 'required|max:256',
          'email' => 'required|max:256|email',
          'phone' => 'required|numeric',
          'content' => 'required|max:1536'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Insert contact and send mail to user
      DB::beginTransaction();
      try{
        //2.1: Create contact from request
        $contact = new stdClass;
        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->content = $request['content'];
        $contact->phone = $request['phone'];

        //2.2: Insert from $contact and return contact_id
        $contact->id = $this->contactService->insert_contact_get_id($contact);

        //2.3: Send email to user
        EmailService::send_contact($contact);
        DB::commit();
        $result = $this->ok("OK");
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
        DB::rollBack();
      }
      return response()->json($result);
    }

    public function sent_contact_restaurant(Request $request){
      //1: Validate request
      $validator = $this->validate($request, [
          'restaurant_id' => 'required',
          'restaurant_email' => 'required',
          'name' => 'required|max:256',
          'email' => 'required|max:256|email',
          'phone' => 'required|numeric',
          'content' => 'required|max:1536'
      ]);
      if ($validator->fails()) {
          return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
      }

      //2: Insert contact and send mail to user
      DB::beginTransaction();
      try{
        //2.1: Create contact from request
        $contact = new stdClass;
        $contact->restaurant_id = $request['restaurant_id'];
        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->phone = $request['phone'];
        $contact->content = $request['content'];

        //2.2: Insert from $contact and return contact_id
        $contact->id = $this->contactRestaurantService->insert_contact_restaurant_get_id($contact);

        //2.3: Send email
        EmailService::send_contact($contact);
        $contact->email = $request['restaurant_email'];
        EmailService::send_contact($contact);

        DB::commit();
        $result = $this->ok("OK");
      }catch(\Exception $e){
        $result =  $this->fail($e->getMessage());
        DB::rollBack();
      }
      return response()->json($result);
    }
}
