<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;
use stdClass;

          class OrderController extends Controller
          {
              public function index(Request $request) {
                Session::put('path','order');
                if(isset($request->restaurant_id)){
                $data['restaurant'] = DB::table('mst_restaurant')->where('id',$request->restaurant_id)->first();

                //Check param in get
                $checkId = DB::table('mst_category')->where('is_active','1')->where('restaurant_id','$request->restaurant_id')->get();
                if(empty($data['restaurant']) || empty($checkId) )
                {
                  return redirect('/search?inputSearch=');
                }

                //category = all
                if($request->category_id != '')
                {
                $data['item'] = DB::table('mst_item_category as ic')
                ->where('ic.category_id',$request->category_id)
                //->join('mst_item','mst_item.id','mst_item_category.item_id')
                ->join('mst_item as i',function($join){
                                $join->on('ic.item_id','=','i.id')
                                ->where('i.del_flg','0')
                                ->where('i.active_flg','1');
                      })
                ->where('i.restaurant_id', $data['restaurant']->id)
                ->paginate(9);
                }

                else
                {
                $data['item'] = DB::table('mst_item_category as ic')
                //->join('mst_item','mst_item.id','mst_item_category.item_id')
                ->join('mst_item as i',function($join){
                                $join->on('ic.item_id','=','i.id')
                                ->where('i.del_flg','0')
                                ->where('i.active_flg','1');
                      })
                ->where('i.restaurant_id', $data['restaurant']->id)
                ->paginate(9);
                }

              }
                else
                {
                  $data['restaurant'] = DB::table('mst_restaurant')->where('id',Session::get('restaurant_id'))->first();

                  if(empty($data['restaurant']))
                  {
                    return redirect('/search?inputSearch=');
                  }

                  $data['item'] = DB::table('mst_item_category as ic')
                  ->where('ic.category_id',$request->category_id)
                  //->join('mst_item','mst_item.id','mst_item_category.item_id')
                  ->join('mst_item as i',function($join){
                                $join->on('ic.item_id','=','i.id')
                                ->where('i.del_flg','0')
                                ->where('i.active_flg','1');
                      })
                  ->where('i.restaurant_id', $data['restaurant']->id)
                  ->paginate(8);
                }

                  $data['categories'] = DB::table('mst_category')->where('restaurant_id', $request->restaurant_id)->where('is_active',1)->get();

                  //active category
                  foreach($data['categories'] as $category)
                  {
                    $category->active = new stdClass();
                    if($request->category_id== $category->id)
                    {
                      $category->active = 'active';
                    }
                    else $category->active ='';
                  }

                  //active category all
                  if($request->category_id== '')
                  {
                    $data['active_category_all'] = 'active';

                  }
                  else $data['active_category_all']='';


                  $data['restaurant_id']= $request->restaurant_id;

                return View::make('frontend.order', $data);
              }

              public function indexBest()
              {
                Session::put('path','order');
                $item = DB::table('trn_order_detail')
                ->select('item_id',DB::raw('count(item_id) as counter'))
                ->groupBy('item_id')
                ->orderBy('counter','DESC')
                ->toSql();

                $data['item'] = DB::table('mst_item')
                ->join(DB::raw("({$item}) as item"), 'item.item_id','=','mst_item.id')
                ->where('active_flg',1)
                ->limit(12)
                ->get(['mst_item.*']);
                $data['tag'] = 'Best';
                $data['topTittle'] = 'Best Foods Sales ';

                return View::make('frontend.orderTop',$data);
              }

              public function indexDiscount()
              {
                Session::put('path','order');
                $data['item'] = DB::table('mst_item')
                ->where('discount','>',0)
                ->where('active_flg',1)
                ->orderBy('discount','DESC')
                ->limit(12)
                ->get(['mst_item.*']);

                $data['tag'] = 'Sale';
                $data['topTittle'] = 'Sale off';

                return View::make('frontend.orderTop',$data);
              }
}
