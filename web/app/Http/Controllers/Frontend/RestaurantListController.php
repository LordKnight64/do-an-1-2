<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Request;
use Log;
use App\Services\MobileApp\RestaurantService;
use App\Services\MobileApp\CategoryService;
use App\Services\MobileApp\ItemService;
use App\Services\MobileApp\OptionService;
use App\Services\GeoLocationService;


class RestaurantListController extends BaseController
{
    protected $restaurantService;
    protected $categoryService;
    protected $itemService;
    protected $optionService;
    protected $geoLocationService;

    public function __construct(RestaurantService $restaurantService,
                                CategoryService $categoryService,
                                ItemService $itemService,
                                OptionService $optionService,
                                GeoLocationService $geoLocationService)
    {
        $this->restaurantService = $restaurantService;
        $this->categoryService = $categoryService;
        $this->itemService = $itemService;
        $this->optionService = $optionService;
        $this->geoLocationService = $geoLocationService;
    }

    public function index(Request $request) {
      Session::put('path','restaurantList');

        return view('frontend.restaurantList');
    }

    

    public function get_restaurants_by_locations(Request $request){

      $restaurantsList = [];
      $param = $request::all();
      $locations = $param["locations"];


      foreach ($locations as $location) {
        $restaurants = [];

        $restaurants = $this->get_restaurant_by_location($location);
        $restaurantsList[] = $restaurants;
      }

      $result = $this->ok("OK");
      $result['restaurantList'] = $restaurantsList;
      return response()->json($result);
    }

    private function get_restaurant_by_location($location){
      //1: Validate request
      // $validator = $this->validate($location, [
      //     'lat_val' => 'required',
      //     'long_val' => 'required',
      //     'radius' => 'required'
      // ]);
      // if ($validator->fails()) {
      //     return $this->buildFailedValidationResponse(
      //         $location, $this->formatValidationErrors($validator));
      // }

      //2: get restaurants by location
      $restaurants = [];
      try{
        //2.1: Set input for function get_driving_distance
        $source['lat_val'] = $location["lat_val"];
        $source['long_val'] = $location["long_val"];
        $radius = $location["radius"];
        $destinations = $this->restaurantService->get_all_restaurant();

        //2.2: Get list restaurant by location with properties of address
        $res_locations = $this->geoLocationService->get_driving_distance($source, $destinations, $radius);
        //2.3: Get detail for restaurant in step 2.2
        foreach ($res_locations as $item) {
          $restaurant = $this->get_restaurant($item['id']);
          $restaurant->distance = $item['distance'];
          $restaurant->duration = $item['duration'];
          $restaurants[] = $restaurant;
        }
      }catch(\Exception $e){
      }
      return $restaurants;
    }

    /**
     * [get_restaurant description]
     * @param  [type] $restaurantId [description]
     * @return [type]               [description]
     */
    private function get_restaurant($restaurantId){
      try {
      //1: Get restaurant detail, ordering_setting and categories
      $restaurant = $this->restaurantService->get_restaurant_detail($restaurantId);
      $restaurant->ordering_setting = $this->restaurantService->get_ordering_setting($restaurantId);
      $restaurant->categories = $this->categoryService->get_category_by_restaurantId($restaurantId);


      //2: Get items in category
      foreach($restaurant->categories as $category){
       $category->items = $this->itemService->get_item_detail_by_categoryId($category->id);
       //2.1: Get option_value in item
       foreach($category->items as $item){
         $item->option_items = $this->optionService->get_option_value_by_itemId($item->id);
       }
     }
   }
   catch(\Exception $e){
     Log::debug($e);
   }
      return $restaurant;
    }
}
