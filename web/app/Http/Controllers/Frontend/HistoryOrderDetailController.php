<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Carbon\Carbon;
use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Auth;
use Log;

class HistoryOrderDetailController extends BaseController
{   
    /**
     * Get Order Detail Information
     */
    public function index(Request $request) {
      if(Auth::check() == false)
      {
         return redirect()->to('/');
      }
      $data['req']= DB::table('trn_order')->where('id',$request->id)->first();
      if($data['req'] == null)
      {
        return redirect('/');
      }
      $data['data'] = DB::table('trn_order_detail')
                          ->join('mst_item','mst_item.id','trn_order_detail.item_id')
                          ->where('trn_order_detail.order_id', $request->id)
                          ->paginate(3);

      $data['option'] = DB::table('trn_order_option_value as a')
                            ->join('mst_option_value as b', function ($join) { 
                            		 $join->on('a.option_value_id', '=', 'b.id')
                                      ->where('b.del_flg', '0');
                            })
                            ->where('a.order_id',$request->id )
                            ->get();
      
      $data['haveOption'] = DB::table('trn_order_option_value')
                            ->where('order_id',$request->id)
                            ->distinct()
                            ->get(['item_id']);
      foreach($data['haveOption'] as $option)
      {
          Log::info("$option->item_id");
      }
      foreach($data['option'] as $option)
      {
          Log::info("$option->option_id $option->item_id $option->option_value_id $option->name");
      }

      $data['optionGroup'] = DB::table('mst_option')
                                ->where('del_flg','0')
                                ->get();


      $data['optionPrice'] = DB:: table('mst_item_option_value as a')
                                  ->where('a.active_flg','1')
                                  ->select('a.item_id','a.option_id','a.option_value_id','a.add_price')
                                  ->get();
      

      

      
      $orders= DB::table('trn_order_detail')->where('order_id',$request->id)->get();
      
                              

      $data['estimate']= $data['req']->price - $data['req']->delivery_fee;
      $data['discount_fee'] = ($data['req']->price * $data['req']->promo_discount);

      $data['total']= $data['req']->price - $data['discount_fee'];

        return view('frontend.historyOrderDetail')->with($data);
    }
}
