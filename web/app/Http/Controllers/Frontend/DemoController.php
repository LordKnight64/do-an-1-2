<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Session;
use Illuminate\Http\Request;
use stdClass;
use DB;
use Log;

use App\Services\EmailService;
use App\Services\MobileApp\ContactService;

class DemoController extends BaseController
{
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * [request_demo description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function request_demo(Request $request)
    {
        //1: Validate request
      $validator = $this->validate($request, [
          'name' => 'required|max:256',
          'email' => 'required|max:256|email'
      ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
        }

      //2: Insert contact and send mail to user
      DB::beginTransaction();
        try {
            //2.1: Create contact from request
        $contact = new stdClass;
            $contact->name = $request['name'];
            $contact->email = $request['email'];

        //2.2: Insert from $contact and return contact_id
        $contact->id = $this->contactService->insert_contact_get_id($contact);

        //2.3: Send email to user
        EmailService::send_request_demo($contact, $contact->email);
            EmailService::send_request_demo($contact, "ifoodgo123@gmail.com");

            DB::commit();
            $result = $this->ok("OK");
        } catch (\Exception $e) {
            $result =  $this->fail($e->getMessage());
            DB::rollBack();
        }
        return response()->json($result);
    }
}
