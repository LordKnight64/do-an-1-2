<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Frontend\Controllers\BaseController;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;

class NotificationListController extends \Illuminate\Routing\Controller
{
    /**
     * Show list of news by restaurant type
     *
     * @return View::make('frontend.notificationList', $data)
     */
    public function index(Request $request)
    {
        Session::put('path','notificationList');
        $content = $request->inputContent;
        if($request->inputContent != null || $request->inputContent == " ")
        {
            Log::debug("===========CONTENT NOT NULL===========");
            if($request->restaurant_type !=null || $request->restaurant_type == " ")
            {
                Log::debug("===========RESTAURANT_TYPE NOT NULL===========");
                $data['news'] = DB::table('trn_news as t')
                        ->join('mst_restaurant as r',function($join){
                                $join->on('t.restaurant_id','=','r.id')
                                ->where('r.del_flg','0')
                                ->where('r.is_active','1');
                        })
                        ->where('r.restaurant_type',$request->restaurant_type)
                        ->where('t.is_active','1')
                        ->where(function($query) use ($content)
                        {
                            $query->where("t.title","LIKE","%{$content}%")
                                ->orwhere("t.content","LIKE","%{$content}%");
                        })
                        ->orderBy('t.id', 'desc')
                        ->paginate(4);
                    
                    
            }
            else
            {
                Log::debug("===========RESTAURANT TYPE NULL===========");
                $data['news'] = DB::table('trn_news as t')
                                ->where(function($query) use ($content)
                                {
                                    $query->where("t.title","LIKE","%{$content}%")
                                        ->orwhere("t.content","LIKE","%{$content}%");
                                })
                                ->where('t.is_active','1')
                                ->orderBy('t.id', 'desc')
                                ->paginate(4);
            }
        }
        else
        {
            Log::debug("===========CONTENT NULL===========");
            if($request->restaurant_type !=null || $request->restaurant_type == " ")
            {
                Log::debug("===========RESTAURANT_TYPE NOT NULL===========");
                $data['news'] = DB::table('trn_news as t')
                                ->join('mst_restaurant as r',function($join){
                                        $join->on('t.restaurant_id','=','r.id')
                                        ->where('r.del_flg','0')
                                        ->where('r.is_active','1');
                                })
                                ->where('r.restaurant_type',$request->restaurant_type)
                                ->where('t.is_active','1')
                                ->orderBy('t.id', 'desc')
                                ->paginate(4);
            }
            else
            {
                Log::debug("===========RESTAURANT_TYPE NULL===========");
                $data['news'] = DB::table('trn_news as t')
                                ->where('t.is_active','1')
                                ->orderBy('t.id', 'desc')
                                ->paginate(4);
            }
        }
        foreach($data['news'] as $n)
                    {
                        $timestamp = strtotime($n->cre_ts);
                        $date = date('d-m-Y', $timestamp);
                        $n->cre_ts = $date;
                    } 

        $data['tagType']= $this->getTag();

          foreach($data['tagType'] as $t)
          {
              if($t->cd == $request->restaurant_type)
              {
                  $t->color = "btn btn-tag-actived btn-sm";
              }
              else
              {
                    $t->color = $this->randomClass();
              }
          }

          $saveData = array(
              'searchContent' => $content,
              'restaurant_type_tag' => $request->restaurant_type,
          );
          
          $data['recentNews'] = $this->recentPost();
          return View::make('frontend.notificationList', compact('data','saveData'));

    }

    /**
     * Random tag button class
     *
     * @return $a_class[$random_keys[0]]
     */
    public function randomClass()
    {
        Log::debug("===========RANDOM===========");
        $a_class = array("btn btn-tag-unactiveorange btn-sm",
                         "btn btn-tag-unactivegreen btn-sm", 
                         "btn btn-info btn-sm");
        $random_keys = array_rand($a_class,2);
        return $a_class[$random_keys[0]];
    }

    /**
     * Show recent news (limit 4)
     *
     * @return $recentPost
     */
    public function recentPost()
    {
        Log::debug("===========RECENT===========");
        $recentPost = DB::table('trn_news')
                          ->orderBy('id', 'desc')
                          ->take(4)
                          ->get();
        return $recentPost;
    }
    /**
     * Show type tag.
     *
     * @return $tag
     */
    public function getTag()
    {
        Log::debug("===========TAG===========");
        $tag = DB::table('mst_cd')
                ->where('cd_group','3')
                ->get();
    
        return $tag;
    }
}
