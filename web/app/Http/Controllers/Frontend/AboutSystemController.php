<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class AboutSystemController extends \Illuminate\Routing\Controller
{
    public function index() {
        Session::put('path','aboutSystem');
        return view('frontend.aboutSystem');
    }
}
