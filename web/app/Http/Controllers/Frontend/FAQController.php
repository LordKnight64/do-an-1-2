<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class FAQController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','faq');
        return view('frontend.FAQ');
    }
}
