<?php


namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;
use stdClass;
use Illuminate\Support\Collection;
use App\Services\GeoLocationService;

class GeoController extends Controller
{

  public function __construct(GeoLocationService $geoLocationService)
  {
      $this->geoLocationService = $geoLocationService;
  }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function get_address_by_latlon(Request $request)
   {
       $param = $request->all();
       $lat = $param["lat"];
       $lon = $param["lon"];
       $results = $this->geoLocationService->get_all_address_info($lat, $lon);
       return response()->json($results);
   }

   public function get_latlon_by_address(Request $request)
 {
     $param = $request->all();
     $address = urlencode($param["address"]);
     $results = $this->geoLocationService->get_data_coordinates($address);
     return response()->json($results);
 }
 public function get_distance_by_address(Request $request)
    {
         Log::info('BBBBBBB');
        $param = $request->all();
        $address1 = urlencode($param["address1"]);
        $address2 = urlencode($param["address2"]);       

        $results = $this->geoLocationService->get_distance_by_address($address1, $address2);
        return response()->json($results);
    }
}
