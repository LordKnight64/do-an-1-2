<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Session;
use Illuminate\Http\Request;
use stdClass;
use DB;
use Log;
use Auth;

class SubscribeController extends BaseController
{

    public function doSubscribe(Request $request)
    {
      $id = Auth::id();
      $user = Auth::user();

          try{
            DB::table('mst_subscribe')->insertGetId(
              [
                'email' => $request['email'],
                'name' => empty($user) ? '' : $user->first_name .' '. $user->last_name ,
                //'cre_user_id' => empty($user) ? '' : $id,
                //'mod_user_id' => empty($user) ? '' : $id,
              ]);
              $result = $this->ok("OK");

          }
          catch(\Exception $e){
            $result =  $this->fail($e->getMessage());
          }
          return response()->json($result);

    }
}
