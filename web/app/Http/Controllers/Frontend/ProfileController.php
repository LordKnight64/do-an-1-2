<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Carbon\Carbon;
use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use stdClass;
use Auth;
use Log;

class ProfileController extends \Illuminate\Routing\Controller
{
  use ValidatesRequests;

    public function index(Request $request) {
       if(Auth::check() == false)
       {
         return redirect()->to('/');
       }
       $result['data'] =  Auth::user();
       $result['address'] = DB::table('mst_user_address')->where('id',  Auth::user()->def_address_id)->first();

       if(!isset($result['address']->address))
       {
         $result['address'] =new stdClass();
         $result['address']->address = '';
       }
       if(!isset($result['data']->first_name))
       {
         $result['data']->first_name = '';
       }
       if(!isset($result['data']->last_name))
       {
         $result['data']->last_name = '';
       }
       if(!isset($result['data']->phone))
       {
         $result['data']->phone = '';
       }

        return view('frontend.profile')->with($result);
    }

    public function saveProfile(Request $request)
    {
      $email =  Auth::user()->email;
      $user= DB::table('mst_user')->where('email',$email)->first();
      $validator = Validator::make($request->all(), [
        'first-name' => 'required|max:255',
        'last-name' => 'required|max:255',
        'phone' => 'required|numeric',
        'address' => 'required'
      ]);
      if ($validator->fails()) {
            return redirect('/profile');

        }

        $current = Carbon::now();
        DB::table('mst_user')->where('email', $email)
        ->update(
        ['first_name' =>  $request['first-name'] ,
         'last_name' =>  $request['last-name'],
         'mod_ts' => $current,
         'mobile' => $request['phone']]);
         if(DB::table('mst_user_address')->where('id', $user->def_address_id)->first() == '')
         {
           DB::table('mst_user_address')->insertGetId(
           [
             'address' => $request['address'],
             'user_id' => $user->id,
             'cre_ts' => $current,
             'mod_ts' => $current,
             'mod_user_id' => $user->id
           ]);
           $userA =DB::table('mst_user_address')->orderBy('id', 'desc')->first();

           DB::table('mst_user')->where('email', $email)
           ->update([
             'def_address_id' => $userA->id
           ]);

         }
         else{
         DB::table('mst_user_address')->where('id', $user->def_address_id)
         ->update(
         [
           'address' => $request['address'],
           'user_id' => $user->id,
           'cre_ts' => $current,
           'mod_ts' => $current,
           'mod_user_id' => $user->id
         ]);
       }
       Session::forget('name');
       Session::put('name',$request['first-name']);

         return redirect("/profile");

    }


    public function changePassword(Request $request)
    {
      $current = Carbon::now();
      $email =  Auth::user()->email;;
      $user= DB::table('mst_user')->where('email',$email)->first();
      $validator = Validator::make($request->all(), [
        'currentpass' => 'required|min:6',
        'newpass' => 'required|min:6',
        'newpass_confirm' => 'required|min:6',
      ]);
      if ($validator->fails()) {
            return redirect('/profile');
        }
        if(password_verify($request['currentpass'], $user->password))
        {
            DB::table('mst_user')->where('email',$email)
            ->update([
              'password' => password_hash($request['newpass'] , PASSWORD_BCRYPT),
              'mod_ts' => $current,
              'mod_user_id' => $user->id
            ]);
            Log::debug("OK");
            $mailSendObject['type'] = 'changeUserPass';
            $mailSendObject['email'] = $user->email;
            $mailSendObject['last_name'] = $user->last_name;
            $mailSendObject['first_name'] = $user->first_name;
            Session::put('mailSendObject',$mailSendObject);
            Log::info('---------userSend ADD FORM-----------'.json_encode($mailSendObject));
            return redirect("/sendMail");
        }
        else
        {
          Log::debug("Mật khẩu sai");
        }

    }
}
