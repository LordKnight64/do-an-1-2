<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Session;
use Illuminate\Http\Request;
use stdClass;
use DB;
use Log;
use Validator;
use App\Services\EmailService;
use App\Services\MobileApp\OrderService;
use Carbon\Carbon;
use App\Services\PushNotificationService;
use App\Models\MstUser;

class CheckoutController extends BaseController
{
    protected $orderService;

    public function __construct(OrderService $orderService, PushNotificationService $pushNotificationService)
    {
        $this->orderService = $orderService;
        $this->pushNotificationService = $pushNotificationService;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        Session::put('path', 'checkout');
        $data['deliveryLst'] = DB::table('mst_cd')->where('cd_group', 0)->get();
        return view('frontend.checkout')->with($data);
    }

/**
     * [index description]
     * @return [type] [description]
     */
    public function reviewCheckout()
    {
        Session::put('path', 'checkout');
        return view('frontend.reviewCheckout');
    }
    /**
     * [checkout description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCheckout(Request $request)
    {
        //  Log::debug('test1-------------');
        $items = $request['items'];
        Validator::extend('max_length', function ($attribute, $value, $parameters, $validator) {
            Log::debug('test1-------------');
            Log::debug($attribute);
            return strlen($value) <= $parameters[0];
        });
        $validator = $this->validate($request, [
          'email' => 'required|email|min:6',
          'phone' => 'required|numeric|max_length:120',
          'address' => 'required|max:1024',
          'promo_code'=>'max:256',
          'checkAccept'=>'required',
          'delivery_type'=>'required|numeric|max_length:11',
          'delivery_ts'=>'required',
      ]);
        $errors = $this->formatValidationErrors($validator);
        $merge = $errors;
        $flagErr = 0;
        if ($validator->fails()) {
            $flagErr = 1;
        }
        if (isset($items)) {
            Log::debug('items for-------------');
            foreach ($items as $key => $value) {
                $validator1 = $this->validateObject($items[$key], [
                'quantity' => 'required|numeric|max_length:11'
            ]);
                $errors1 = $this->formatValidationErrors($validator1);
                $merge = array_merge($merge, $errors1);

                if ($validator1->fails()) {
                    $flagErr = 1;
                }
            }
            if ($flagErr==1) {
                return $this->buildFailedValidationResponse(
                    $request, $merge);
            }
        } else {
            if ($flagErr==1) {
                return $this->buildFailedValidationResponse(
                    $request, $merge);
            }
        }
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
        }
        if ($request['promo_code']!=null && $request['promo_code']!="" && $request['promo_codeRes']!=null && $request['promo_codeRes']!="" && $request['promo_code']!=$request['promo_codeRes']) {
            return $this->buildFailedLoginResponse($request, ['Code do not code for discount cost']);
        }
        $result = $this->ok("OK");
        return response()->json($result);
    }
    /**
     * [checkout description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function payCheckout(Request $request)
    {
        $items = $request['items'];
        Validator::extend('max_length', function ($attribute, $value, $parameters, $validator) {
            Log::debug('test1-------------');
            Log::debug($attribute);
            return strlen($value) <= $parameters[0];
        });
        $validator = $this->validate($request, [
          'email' => 'required|email|min:6',
          'phone' => 'required|numeric|max_length:120',
          'address' => 'required|max:1024',
          'promo_code'=>'max:256',
          'delivery_type'=>'required|numeric|max_length:11',
          'delivery_ts'=>'required',
      ]);
        $errors = $this->formatValidationErrors($validator);
        $merge = $errors;
        $flagErr = 0;
        if ($validator->fails()) {
            $flagErr = 1;
        }
        if (isset($items)) {
            Log::debug('items for-------------');
            foreach ($items as $key => $value) {
                $validator1 = $this->validateObject($items[$key], [
                'quantity' => 'required|numeric|max_length:11'
            ]);
                $errors1 = $this->formatValidationErrors($validator1);
                $merge = array_merge($merge, $errors1);

                if ($validator1->fails()) {
                    $flagErr = 1;
                }
            }
            if ($flagErr==1) {
                return $this->buildFailedValidationResponse(
                    $request, $merge);
            }
        } else {
            if ($flagErr==1) {
                return $this->buildFailedValidationResponse(
                    $request, $merge);
            }
        }
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
              $request, $this->formatValidationErrors($validator));
        }

      //2: Insert tables relationship with Order
      DB::beginTransaction();
        try {
            Log::debug('-----------------INSERT-------------');
        //2.1: Insert order
        $order = new stdClass();
            $order->user_id = $request['user_id'];
            $order->restaurant_id = $request['res_id'];
            $order->name = $request['name'];
            Log::debug('-----------------price-------------'.$request['price']);
            Log::debug('-----------------TotalPoint-------------'.$request['point']);

            $order->price = $request['price'];
            $order->recieved_point = $request['point'];
            $order->used_point = $request['used_point'];
            $order->notes = $request['notes'];
        // $mytime = Carbon::now();
        // $mytime->toDateTimeString();
        $order->delivery_ts = $request['delivery_ts'];
            $order->delivery_type = $request['delivery_type'];
            $order->delivery_fee = $request['delivery_fee'];
            $order->email = $request['email'];
            $order->phone = $request['phone'];
            $order->address = $request['address'];
            $order->promo_code = $request['promo_code'];
            $order->promo_discount = $request['promo_discount'];
            $order->delivery_distance = $request['delivery_distance'];

            $orderId = $this->orderService->insert_order_get_id($order);

            //2.2: Insert order detail
            if (isset($items)) {
                foreach ($items as $item) {
                    $orderDetail = new stdClass();
                    $orderDetail->order_id = $orderId;
                    $orderDetail->item_id = $item['item_id'];
                    $orderDetail->seq_no = $item['number'];
                    $orderDetail->quantity = $item['quantity'];
                    $orderDetail->price = $item['price'];
                    Log::info('------------point------------'.json_encode($item));
                    $orderDetail->point = $item['point'];
                    Log::debug('-----------------price-------------'.$item['price']);
                    $orderDetail->notes = $item['notes'];
                    $this->orderService->insert_order_detail_get_id($orderDetail);


                //2.3: Insert order option value
                Log::info('------------item------------'.$item['point']);
                    $itemOption = $item['option_items'];
                    if (!empty($itemOption)) {
                        foreach ($itemOption as $option_item) {
                            $orderOptionValue = new stdClass();
                            $orderOptionValue->order_id = $orderDetail->order_id;
                            $orderOptionValue->item_id = $orderDetail->item_id;
                            $orderOptionValue->seq_no = $item['number'];
                            $orderOptionValue->option_value_id = $option_item['option_value_id'];
                            $orderOptionValue->option_id = $option_item['option_id'];
                            $this->orderService->insert_order_option_value($orderOptionValue);
                        }
                    }
                }
            }
             // save accumulative_point user 
            $user = MstUser::find($request['user_id']);
            if (!empty($user)) {
                //update user info
                $user->accumulative_point = $request['accumulative_point'];
                $user->save();
            }
            //Push notification
            $userIdPush= DB::table('mst_user_restaurant as a')
                            ->where('a.restaurant_id', $request['res_id'])
                            ->first();
            if ($userIdPush!=null) {
                $lishPush = $this->pushNotificationService->get_for_user($userIdPush->user_id, "admin");
                if (isset($lishPush)) {
                    foreach ($lishPush as $itemPush) {
                        $token = $itemPush["token"];
                        $tokenPush = json_decode($token);
                        Log::debug('-----------------tokenPush-------------'.json_encode($tokenPush));
                        $paramPush['endpoint'] = $tokenPush->{'endpoint'};
                        Log::debug('-----------------endpoint-------------'.$paramPush['endpoint']);
                    // $responsePush = $this->pushNotificationService->push($param);
                    $keysPush=$tokenPush->{'keys'};
                        $keys['p256dh']=$p256dh=$keysPush->{'p256dh'};
                        Log::debug('-----------------p256dh-------------'. $keys['p256dh']);
                        $keys['auth']=$keysPush->{'auth'};
                        $paramPush['keys'] =$keys;
                        Log::debug('-----------------auth-------------'.$paramPush['keys']['auth']);
                        $paramPush['payload']="your retaurant had new order";
                        Log::debug('-----------------payload-------------'. $paramPush['payload']);
                        $this->pushNotificationService->push($paramPush);
                    }
                }
            }
            DB::commit();
            $result = $this->ok("OK");
            $result['res_id'] = $request['res_id'];
        } catch (Exception $e) {
            $result =  $this->fail($e->getMessage());
            DB::rollBack();
        }

        return response()->json($result);
    }
}
