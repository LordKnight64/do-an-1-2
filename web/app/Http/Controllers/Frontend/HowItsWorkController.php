<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class HowItsWorkController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','howitswork');
        return view('frontend.howItsWork');
    }
}
