<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class PrivatePolicyController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','privatepolicy');
        return view('frontend.privatepolicy');
    }
}
