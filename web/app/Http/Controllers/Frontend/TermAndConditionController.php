<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class TermAndConditionController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','termandcondition');
        return view('frontend.termandcondition');
    }
}
