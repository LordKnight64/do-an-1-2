<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Session;
class SiteMapController extends \Illuminate\Routing\Controller
{
    public function index() {
      Session::put('path','sitemap');
        return view('frontend.sitemap');
    }
}
