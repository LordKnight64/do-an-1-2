<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;


class AboutController extends \Illuminate\Routing\Controller
{
    public function index(Request $request) {

      Session::put('path','about');
      if($request->id==null)
      {
        $request->id = Session::get('idRes');
      }
      else Session::put('idRes',$request->id);
      $data['restaurant'] = DB::table('mst_restaurant')->where('id',$request->id)->first();
        return view('frontend.about')->with($data);
    }
}
