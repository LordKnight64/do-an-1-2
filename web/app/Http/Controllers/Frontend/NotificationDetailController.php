<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Frontend\Controllers\BaseController;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;

class NotificationDetailController extends \Illuminate\Routing\Controller
{
    /**
     * Show list of news by restaurant type
     *
     * @return View::make('frontend.notificationList', $data)
     */
    public function index(Request $request)
    {
        
        $newsid = $request->news_id;
        $restype = 0;
        Log::info("$newsid");
        if($request->news_id != null || $request->news_id ==" ")
        {
                $data['news'] = DB::table('trn_news as n')
                                ->where('n.id',$request->news_id)
                                ->where('n.is_active','1')
                                ->get();
                
                foreach($data['news'] as $news)
                {
                    $news->restaurant_type = $this->setRestaurantType($news);
                    $news->restaurant_name = $this->getRestaurantName($news->restaurant_id);
                    $restype = $news->restaurant_type;
                }
        }
        else
        {
            Log::info("NEWS ID NULL");
            return redirect('/404');
        }
        $i = 0;
        foreach($data['news'] as $n)
        {
            Log::info($n->content);
           $timestamp = strtotime($n->cre_ts);
           $date = date('d-m-Y', $timestamp);
           $n->cre_ts = $date;
           $i++;
        }
        if($i == 0)
        {
            Log::info("NO DATA");
            return redirect('/404');
        } 

        $data['tagType']= $this->getTag();

          foreach($data['tagType'] as $t)
          {
              if($t->cd == $restype)
              {
                  $t->color = "btn btn-tag-actived btn-sm";
              }
              else
              {
                    $t->color = $this->randomClass();
              }
          }
          
          $data['recentNews'] = $this->recentPost();
          return View::make('frontend.notificationDetail',$data);

    }

    /**
     * Random tag button class
     *
     * @return $a_class[$random_keys[0]]
     */
    public function randomClass()
    {
        Log::debug("===========RANDOM===========");
        $a_class = array("btn btn-tag-unactiveorange btn-sm",
                         "btn btn-tag-unactivegreen btn-sm", 
                         "btn btn-info btn-sm");
        $random_keys = array_rand($a_class,2);
        return $a_class[$random_keys[0]];
    }

    /**
     * Show recent news (limit 4)
     *
     * @return $recentPost
     */
    public function recentPost()
    {
        Log::debug("===========RECENT===========");
        $recentPost = DB::table('trn_news')
                          ->orderBy('id', 'desc')
                          ->take(4)
                          ->get();
        return $recentPost;
    }

    /**
     * Get type of News
     * @return $type->restaurant_type
     */
    public function setRestaurantType($news)
    {
        $data['type'] = DB::table('mst_restaurant')
                                    ->where('id',$news->restaurant_id)
                                    ->get();
        foreach($data['type'] as $type)
        {
            return $type->restaurant_type;
        }
    }

    /**
     * Show type tag.
     *
     * @return $tag
     */
    public function getTag()
    {
        Log::debug("===========TAG===========");
        $tag = DB::table('mst_cd')
                ->where('cd_group','3')
                ->get();
    
        return $tag;
    }

    public function getRestaurantName($resID)
    {
        $restaurant = DB::table('mst_restaurant')
                        ->where('id',$resID)
                        ->get();
        foreach($restaurant as $res)
        {
            return $res->name;
        }
    }
}
