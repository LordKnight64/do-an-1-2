<?php


namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\Controller as BaseController;
use App\Item;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;
use stdClass;
use Illuminate\Support\Collection;

class RestaurantDetailController extends BaseController
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
     {
         Session::put('path', $request->path());
         Log::debug(Session::get('path'));
         $data['restaurant'] = DB::table('mst_restaurant')->where('id', $request->id)->first();
         if (empty($data['restaurant'])) {
             return redirect('/');
         }
         $data['item'] = DB::table('mst_item')
       ->where('restaurant_id', $request->id)
       ->where('active_flg', '1')
       ->orderBy('cre_ts', 'asc')
       ->limit(4)
       ->get();

         $data['item_best'] = DB::table('mst_item')
       ->where('restaurant_id', $request->id)
       ->where('active_flg', '1')
       ->orderBy('cre_ts', 'asc')
       ->limit(4)
       ->get();

         $item_best = DB::table('trn_order_detail')
       ->select('item_id', DB::raw('count(item_id) as counter'))
       ->groupBy('item_id')
       ->orderBy('counter', 'DESC')
       ->toSql();

         $data['item_best'] = DB::table('mst_item')
       ->join(DB::raw("({$item_best}) as item"), 'item.item_id', '=', 'mst_item.id')
       ->where('active_flg', 1)
       ->where('restaurant_id', $request->id)
       ->limit(4)
       ->get(['mst_item.*']);

         $data['menu'] = DB::table('mst_item_category as ic')
       ->join('mst_item as i', function ($join) {
           $join->on('ic.item_id', '=', 'i.id')
                                ->where('i.del_flg', '0')
                                ->where('i.active_flg', '1');
       })
       ->get();

         $data['menu_header'] = DB::table('mst_category')
       ->where('restaurant_id', $request->id)
       ->where('is_active', '1')
       ->get();

         foreach ($data['menu_header'] as $dat) {
             $coll = collect([]);
             foreach ($data['menu'] as $item) {
                 if ($item->category_id == $dat->id) {
                     $coll->push($item);
                 }
             }
             $dat->item = $coll;
         }



         $flag =0;
         foreach ($data['menu_header'] as $dat) {
             $dat->color = new stdClass();
             if ($flag==0) {
                 $dat->color = "br-red";
             }
             if ($flag==1) {
                 $dat->color = "br-lblue";
             }
             if ($flag==2) {
                 $dat->color = "br-green";
             }
             if ($flag==3) {
                 $dat->color = "br-purple";
             }
             if ($flag==4) {
                 $dat->color = "br-orange";
             }
             if ($flag==5) {
                 $dat->color = "br-blue";
             }
             $flag++;
             if ($flag==6) {
                 $flag=0;
             }
         }
         return View::make('frontend.restaurantDetail', $data);
     }

    public function getRestaurantById(Request $request)
    {

       //1: Validate request
       $validator = $this->validate($request, [
           'id' => 'required'
       ]);
        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
               $request, $this->formatValidationErrors($validator));
        }

       //2: Get restaurant
       try {
           $result = $this->ok("OK");
           $result['restaurant'] = DB::table('mst_restaurant')->where('id', $request->id)->first();
       } catch (\Exception $e) {
           $result =  $this->fail($e->getMessage());
       }

        return response()->json($result);
    }
}
