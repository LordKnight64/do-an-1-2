<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller as BaseController;
use Log;
use App\Exceptions\ApiValidationException;
use Illuminate\Http\Request;
use \Illuminate\Validation\ValidationException;
use \Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController {

    /**
     * Validate the given request with the given rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customAttributes
     * @return void
     */
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        return $validator;
    }
    /**
     * Validate the given request with the given rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customAttributes
     * @return void
     */
    public function validateObject(Array $object, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($object, $rules, $messages, $customAttributes);

        return $validator;
    }
    /**
     * Create the response for when a request fails validation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        $result = [
            'message' => 'Validation Error',
            'errors' => $errors,
            'code' => 422,
            'status_code' => 422,
            'return_cd' => -1
        ];

        return new JsonResponse($result);
    }
    protected function buildFailedLoginResponse(Request $request, array $errors)
    {
        $result = [
            'message' => 'Login Error',
            'errors' => $errors,
            'code' => 401,
            'status_code' => 401,
            'return_cd' => -1
        ];

        return new JsonResponse($result);
    }

    protected function ok($msg = null) {
  		return [
  			'return_cd' => 0,
  			'message' => $msg
  		];
  	}

  	protected function fail($msg = null) {
  		return [
  			'return_cd' => 1,
  			'message' => $msg
  		];
  	}

    // protected function error(String $message, $status_code = 200) {
    //     $result = [
    //         'message' => $message,
    //         'code' => -1,
    //         'status_code' => 200,
    //         'rtnCd' => -1
    //     ];

    //     return new JsonResponse($result, $status_code);
    // }
}
