<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use JWTAuth;
use Auth;
use Validator;
use Log;
use Input;
use Session;

class UserLoginController extends Controller
{


    public function index() {
        Session::forget('email');
        Session::put('path','userLogin');
        return view('frontend.userLogin');
    }

    public function doLogin(Request $request)
    {

      // $credentials = $request->only('email', 'password');

      // try {
      //     // verify the credentials and create a token for the user
      //     if (! $token = JWTAuth::attempt($credentials)) {
      //       Log::debug(response()->json(['error' => 'invalid_credentials'], 401));
      //       return redirect("/userLogin");

      //         //return response()->json(['error' => 'invalid_credentials'], 401);
      //     }
      // } catch (JWTException $e) {
      //     // something went wrong
      //     Log::debug(response()->json(['error' => 'could_not_create_token'], 500));
      //     return redirect("/userLogin");
      //     //return response()->json(['error' => 'could_not_create_token'], 500);
      // }

      // $user = Auth::user();

      // // if no errors are encountered we can return a JWT
      // Log::debug(response()->json(compact('user', 'token')));
      // Session::put('email', $user->email);
      // Session::put('name', $user->last_name);

      // return redirect("/restaurantDetail");
      //return response()->json(compact('user', 'token'));

      if (Auth::attempt(['email' => $request->get("email"), 'password' => $request->get("password")]))
      {
        Event::fire(new UserAccessEvent(Auth::user(), UserAccessEvent::LOGIN));
        Session::put('userInfo', Auth::user());
        $sidebar = $this->userService->getScreen(Auth::user()->id);
        foreach ($sidebar as &$item) {
            $item->url = Config::get('constants.'.$item->url);
        };
              Session::put('sidebarMenu', $sidebar);
        return redirect()->intended($this->redirectPath());

      }
      return redirect($this->loginPath())
            ->withInput($request->only('username'))
            ->withErrors([
              'username' => $this->getFailedLoginMessage(),
            ]);
    }
}
