<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Carbon\Carbon;
use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Auth;
use Log;
use Folklore\Image\Facades\Image;
use App\Models\MstRestaurant;

class AddRestaurantFormController extends BaseController
{

    public function index() {
      Session::put('path','addRestaurantForm');
        $data['type'] = DB::table('mst_cd')->where('cd_group',3)->get();
        $data['currency'] = DB::table('mst_cd')->where('cd_group',2)->get();
        return view('frontend.addRestaurantForm')->with($data);
    }

    public function tab1(Request $request)
    {
    //   $user= DB::table('mst_user')->where('email',$request->Email)->first();
    //   if($user!=null)
    //   {
    //   $check['email']=DB::table('mst_role_user')->where('user_id',$user->id)->get();
    //    Log::info("-------------count email-----------------".$check['email']->count());
    //    if($check['email']->count()>=2)
    //    {
    //      $test['Email'] = 'This is mail has been exist';
    //      return response()->json($test);
    //    }
    //  }
       $validator = Validator::make($request->all(), [
        'First_name' => 'required|max:12|min:1',
        'Last_name' => 'required|max:12|min:1',
        'Mobile_number' => 'required|numeric|max:999999999999',
        'Tel_number' => 'required|max:12',
        'Email' => 'required|email',
        'Password' => 'required|min:6|confirmed',
        'Password_confirmation' => 'required|min:6',
      ]);

      if ($validator->fails()) {
        return response()->json($validator->messages(), 200);
    }
    }

    public function tab2(Request $request)
    {

       $validator = Validator::make($request->all(), [
         'Restaurant_name' => 'required|max:256',
         'Restaurant_address' => 'required|max:1024',
         'Restaurant_mobile_number' => 'required|numeric|max:9999999999999999',
         'Restaurant_tel_number' => 'required|max:12',
         'Restaurant_email' => 'required|email|max:256',
         'Restaurant_open' => 'required|date_format:H:i',
         'Restaurant_close' => 'required|date_format:H:i',
         'Restaurant_Url' => 'required|max:256|alpha_num'
      ]);

      if ($validator->fails()) {
        return response()->json($validator->messages(), 200);
    }
    }


    public function addRestaurant(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'res_name' => 'required|max:255',
        'res_address' => 'required|max:255',
        // 'country' => 'required|max:255',
        // 'province' => 'required|max:255',
        // 'city' => 'required|max:255',
        // 'lat' => 'required|max:255',
        // 'long' => 'required|max:255',
        // 'street' => 'required|max:255',
        'Mobileno' => 'required|numeric',
        'Telno' => 'required|numeric',
        'res_telno' => 'required|numeric',
        'res_mobileno' => 'required|numeric',
        'email' => 'required|email',
        'res_email'=>'required|email',
        'Subdomain' => 'required|max:255',
        'open_time' => 'required',
        'close_time' => 'required'
      ]);


      if ($validator->fails() || $request->pass != $request->conf_pass) {
            Session::flash('alert-class', 'alert  alert-danger'); 
            $err = $validator->messages();
            Log::info("-------------ERRRRR-----------------".json_encode($err));
            Session::flash('messageErrorAddRes', json_encode($err)); 
            return redirect('/addRestaurantForm')->withErrors($validator->messages())->withInput();
        }

      //save image
      // $file = $request->logo;
      // $format = null;
      // if(strpos($file->getClientOriginalName(),'.jpg')!==false)
      // {
      //   $format = '.jpg';
      // }
      // if(strpos($file->getClientOriginalName(),'.png')!==false)
      // {
      //   $format = '.png';
      // }
      // if(strpos($file->getClientOriginalName(),'.gif')!==false)
      // {
      //   $format = '.gif';
      // }


  try{
          $check = DB::table('mst_user')->where('email', $request['email'])->first();
          if($check ==null)
          {
            $id = Auth::id();
            $current = Carbon::now();
            $isUserInsert = DB::table('mst_user')->insertGetId(
            ['last_name' => $request['last_name'],
             'first_name' => $request['last_name'],
             'email' => $request['email'],
             'password' =>   bcrypt($request['pass']),
             'tel' => $request['Telno'],
             'mobile'=>$request['Mobileno'],

             'cre_ts' => $current,
             'mod_ts' => $current]);
             Log::info('---------isUserInsert-----------'.$isUserInsert);  
            //  $lastRowUser = DB::table('mst_user')->orderBy('id', 'desc')->first();

            DB::table('mst_role_user')->insertGetId(
             ['user_id' => $isUserInsert,
              'role_id' => 2,
              'user_type' => 'Restaurant']);

              // if($format){
              //       $filename = $request->res_name.'_logo_'.$lastRowUser->id.$format;
              //       $path = public_path('frontend\img\logo_res\ '. $filename);
              //       $img = Image::make($file->getRealPath(),array(
              //           'max-width' => 150,
              //           'max-height' => 150,
              //           'grayscale' => false
              //               ))->save($path);

              //     }

            $idInsert = DB::table('mst_restaurant')->insertGetId(
              ['name' => $request['res_name'],
               'email' => $request['res_email'],
               'tel' => $request['res_telno'],
               'mobile' => $request['res_mobileno'],
              //  'logo_path' => 'frontend\img\logo_res\ '. $filename,
               'logo_path' => '',
               'promote_code' => $request['Promocode'],
               'promo_discount' => $request['Promodis'],
               'open_time' => $request['open_time'],
               'close_time' => $request['close_time'],
               'restaurant_type' => $request['res_type'],
               'lat_val' => $request['lat'],
               'long_val' => $request['long'],
               'city' => $request['city'],
               'province' => $request['province'],
               'country' => $request['country'],
               'street' => $request['street'],
               'address' => $request['res_address'],
               'website' => $request['Subdomain'],
               'facebook_link' => $request['Facebook'],
               'twitter_link' => $request['Twitter'],
               'linkedin_link' => $request['Linkedin'],
               'google_plus_link' => $request['Google'],
               'payment_method' => 0,
               'delivery_methods' =>0,
               'food_ordering_system_type' => 0,
               'cre_ts' => $current,
               'mod_ts' => $current]);
               Log::info('---------idInsert-----------'.$idInsert);  
              //  $lastRowRestaurant = DB::table('mst_restaurant')->orderBy('id', 'desc')->first();

               DB::table('mst_user_restaurant')->insertGetId(
                 ['user_id' => $lastRowUser->id,
                  'restaurant_id' => $idInsert,
                  'notes' => $request['message']]);
                  $mailSendObject['type'] = 'registerRes';
                  $mailSendObject['email'] = $request['email'];
                  $mailSendObject['last_name'] = $request['last_name'];
                  $mailSendObject['first_name'] = $request['first_name'];
                  $mailSendObject['id'] = $idInsert;
                  Session::put('mailSendObject',$mailSendObject);
                  Log::info('---------userSend ADD FORM-----------'.json_encode($mailSendObject));
               return redirect("/sendMail");
          }
          else{
            $current = Carbon::now();
            $checkRole=DB::table('mst_role_user')->where('user_id',$check->id)->where('role_id',2)->get();
            Log::info("-------------count email-----------------".json_encode($checkRole));
            if(empty($checkRole))
            {
              DB::table('mst_role_user')->insertGetId(
             ['user_id' =>  $check->id,
              'role_id' => 2,
              'user_type' => 'Restaurant']);
            }
              // if($format){
              //       $filename = $request->res_name.'_logo_'.$lastRowUser->id.$format;
              //       $path = public_path('frontend\img\logo_res\ '. $filename);
              //       $img = Image::make($file->getRealPath(),array(
              //           'max-width' => 150,
              //           'max-height' => 150,
              //           'grayscale' => false
              //               ))->save($path);

              //     }

                 $idInsert =  DB::table('mst_restaurant')->insertGetId(
                    ['name' => $request['res_name'],
                     'email' => $request['res_email'],
                     'tel' => $request['res_telno'],
                     'mobile' => $request['res_mobileno'],
                    //  'logo_path' => 'frontend\img\logo_res\ '. $filename,
                     'logo_path' => '',
                     'promote_code' => $request['Promocode'],
                     'promo_discount' => $request['Promodis'],
                     'open_time' => $request['open_time'],
                     'close_time' => $request['close_time'],
                     'restaurant_type' => $request['res_type'],
                     'lat_val' => $request['lat'],
                     'long_val' => $request['long'],
                     'city' => $request['city'],
                     'province' => $request['province'],
                     'country' => $request['country'],
                     'street' => $request['street'],
                     'address' => $request['res_address'],
                     'website' => $request['Subdomain'],
                     'facebook_link' => $request['Facebook'],
                     'twitter_link' => $request['Twitter'],
                     'linkedin_link' => $request['Linkedin'],
                     'google_plus_link' => $request['Google'],
                     'payment_method' => 0,
                     'delivery_methods' =>0,
                     'food_ordering_system_type' => 0,
                     'cre_ts' => $current,
                     'mod_ts' => $current]);
                     Log::info('---------idInsert-----------'.$idInsert);  
                    //  $lastRowRestaurant = DB::table('mst_restaurant')->orderBy('id', 'desc')->first();

                     DB::table('mst_user_restaurant')->insertGetId(
                       ['user_id' => $check->id,
                        'restaurant_id' => $idInsert,
                        'notes' => $request['message']]);
                    $mailSendObject['type'] = 'registerRes';
                    $mailSendObject['email'] = $request['email'];
                    $mailSendObject['last_name'] = $request['last_name'];
                    $mailSendObject['first_name'] = $request['first_name'];
                    $mailSendObject['id'] = $idInsert;
                    Log::info('---------userSend ADD FORM-----------'.json_encode($mailSendObject));
                    Session::put('mailSendObject',$mailSendObject);
                        return redirect("/sendMail");
                       }  
            }catch(Exception $e){
              $result =  $this->fail($e->getMessage());
              DB::rollBack();
            }
            return redirect("/");
    }

    public function restaurantActive(Request $request)
    {
      try{
          //MstRestaurant::where('id',$request['id'])->update(array('is_active' => 1));
          DB::table('mst_restaurant')
            ->where('id', $request->id)
            ->update(['is_active' => '1']);
          $result = $this->ok("OK");
          Session::flash('activeRestaurant', "1"); 
        }catch(Exception $e){
            Session::flash('activeRestaurant', "0"); 
            return view('frontend.restaurantActive');
        }
        $DOMAIN_ADMIN = env('DOMAIN_ADMIN');
        Log::info($DOMAIN_ADMIN);
        $url = $request->url();
        $parsedUrl = parse_url($url);
        Log::info(json_encode($parsedUrl));
        // $root = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
        $DOMAIN_ADMIN = $parsedUrl['scheme'] . '://' . $DOMAIN_ADMIN;
        if (array_key_exists('port', $parsedUrl)) {
          $DOMAIN_ADMIN = $DOMAIN_ADMIN.':'.$parsedUrl['port'];
        }
        Log::info($DOMAIN_ADMIN);
        $data['url'] = $DOMAIN_ADMIN;
       return view('frontend.restaurantActive')->with($data);
    }

}
