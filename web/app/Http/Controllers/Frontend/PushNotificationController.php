<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Session;
use Illuminate\Http\Request;
use stdClass;
use DB;
use Log;
use Validator;
use App\Services\EmailService;
use App\Services\MobileApp\OrderService;
use Carbon\Carbon;
use App\Services\PushNotificationService;


class PushNotificationController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PushNotificationService $pushNotificationService)
    {
        $this->pushNotificationService = $pushNotificationService;
    }

    public function get_all(Request $request) {
        $response = $this->pushNotificationService->get_all();
        
        return response()->json($response);
    }
    public function store(Request $request)
    {
       
        Log::debug("---------------AAAAAAAAAAA--------------");
        $param = $request->all();
        Log::debug(json_encode($param));
        Log::debug("---------------**********--------------");
        Log::debug($param['endpoint']);
        // Log::info("---------------AAAAAAAAAAA--------------");
        $response = $this->pushNotificationService->store($param);
        $returnCd = $response['return_cd'];
        if($returnCd==0){
            Session::put('isSubscribed', 'true');
        }
        return '';
    }
    public function push_notification(Request $request) {
        $param = $request->all();
        $response = $this->pushNotificationService->push($param);
        return response()->json($response);
    }
    
}