<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\Controller as BaseController;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use JWTAuth;
use Auth;
use Validator;
use Log;
use Input;
use Session;
use App\Services\EmailService;
class SendMailController extends BaseController
{

    /**
     * Show the application sendMail form.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('frontend.sendMail');
    }

    public function sendMail(Request $request) {
        $mailSendObject = session("mailSendObject");
        Log::info('---------userSend-----------'.json_encode($mailSendObject));
        if (!empty($mailSendObject)) {
            if($mailSendObject['type']=='registerRes'){
                try{
                    $userSend['email']=$mailSendObject['email'];
                    $userSend['first_name']=$mailSendObject['first_name'];
                    $userSend['last_name']=$mailSendObject['last_name'];
                    $userSend['id']=$mailSendObject['id'];
                    $DOMAIN_WWW = env('DOMAIN_WWW');
                    Log::info($DOMAIN_WWW);
                    $url = $request->url();
                    $parsedUrl = parse_url($url);
                    Log::info(json_encode($parsedUrl));
                    // $root = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
                    $DOMAIN_WWW = $parsedUrl['scheme'] . '://' . $DOMAIN_WWW;
                    if (array_key_exists('port', $parsedUrl)) {
                        $DOMAIN_WWW = $DOMAIN_WWW.':'.$parsedUrl['port'];
                    }
                    Log::info($DOMAIN_WWW);
                    $userSend['domain']=$DOMAIN_WWW;
                    Session::forget('mailSendObject');
                    Log::info('---------userSend-----------'.json_encode($userSend));
                    EmailService::send_mail_regisRes($userSend, $userSend['email']);
                    //EmailService::send_mail_regisRes($userSend, "kieu-trinh@system-exe.com.vn");
                     $result = $this->ok("OK");
                }catch(Exception $e){
                    $result =  $this->fail($e->getMessage());
                }
            }
            if($mailSendObject['type']=='registerUser')
            {
                try{
                    $userSend['email']=$mailSendObject['email'];
                    $userSend['first_name']=$mailSendObject['first_name'];
                    $userSend['last_name']=$mailSendObject['last_name'];
                    $userSend['id']=$mailSendObject['id'];
                    $userSend['password']=$mailSendObject['password'];
                    $DOMAIN_WWW = env('DOMAIN_WWW');
                    Log::info($DOMAIN_WWW);
                    $url = $request->url();
                    $parsedUrl = parse_url($url);
                    Log::info(json_encode($parsedUrl));
                    // $root = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
                    $DOMAIN_WWW = $parsedUrl['scheme'] . '://' . $DOMAIN_WWW;
                    if (array_key_exists('port', $parsedUrl)) {
                        $DOMAIN_WWW = $DOMAIN_WWW.':'.$parsedUrl['port'];
                    }
                    Log::info($DOMAIN_WWW);
                    $userSend['domain']=$DOMAIN_WWW;
                    Session::forget('mailSendObject');
                    Log::info('---------userSend-----------'.json_encode($userSend));
                    EmailService::register($userSend, $userSend['email']);
                    //EmailService::send_mail_regisRes($userSend, "kieu-trinh@system-exe.com.vn");
                     $result = $this->ok("OK");
                }catch(Exception $e){
                    $result =  $this->fail($e->getMessage());
                }
            }
            if($mailSendObject['type']=='changeUserPass')
            {
                try{
                    $userSend['email']=$mailSendObject['email'];
                    $userSend['first_name']=$mailSendObject['first_name'];
                    $userSend['last_name']=$mailSendObject['last_name'];
                    $DOMAIN_WWW = env('DOMAIN_WWW');
                    Log::info($DOMAIN_WWW);
                    $url = $request->url();
                    $parsedUrl = parse_url($url);
                    Log::info(json_encode($parsedUrl));
                    // $root = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
                    $DOMAIN_WWW = $parsedUrl['scheme'] . '://' . $DOMAIN_WWW;
                    if (array_key_exists('port', $parsedUrl)) {
                        $DOMAIN_WWW = $DOMAIN_WWW.':'.$parsedUrl['port'];
                    }
                    Log::info($DOMAIN_WWW);
                    $userSend['domain']=$DOMAIN_WWW;
                    Session::forget('mailSendObject');
                    Log::info('---------userSend-----------'.json_encode($userSend));
                    EmailService::change_password($userSend, $userSend['email']);
                     $result = $this->ok("OK");
                }catch(Exception $e){
                    $result =  $this->fail($e->getMessage());
                }
            }
        }
        return null;
    }
}
