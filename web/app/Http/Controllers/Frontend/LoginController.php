<?php

// namespace App\Http\Controllers\Auth;
namespace App\Http\Controllers\Frontend;

// use App\Http\Controllers\Controller;

use App\Http\Controllers\Frontend\Controller as BaseController;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use App\Http\Requests\Frontend\Auth\LoginRequest;
use Event;
use Auth;
use Session;
use App\Events\UserAccessEvent;
use Log;
use App\Models\MstUser;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected $redirectTo = '/';

    protected $loginPath = "/login";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.userLogin');
    }

        /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $validator = $this->validate($request, [
            'email'    => 'required|email|min:6',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return $this->buildFailedValidationResponse(
                $request, $this->formatValidationErrors($validator));
        }

        $user = MstUser::whereEmail($request->get("email"))->first();
        // if (isset($user->email_verified) && $user->email_verified == 0) {
        if (!isset($user)) {
            Log::debug('Email Unverified');
            return $this->buildFailedLoginResponse($request, ['Email Unverified']);
        }

        if (Auth::attempt(['email' => $request->get("email"), 'password' => $request->get("password")]))
        {
            event(new UserAccessEvent(Auth::user(), UserAccessEvent::LOGIN));
            Session::put('userInfo', Auth::user());
            // $sidebar = $this->userService->getScreen(Auth::user()->id);
            // foreach ($sidebar as &$item) {
            //     $item->url = Config::get('constants.'.$item->url);
            // };
            // Session::put('sidebarMenu', $sidebar);
            // Log::debug('-------UserAccessEvent::LOGIN------------');
            // return redirect()->intended($this->redirectTo);
            $user = Auth::user();
            return response()->json($user);
        }
         return $this->buildFailedLoginResponse($request, ['Invalid credentials']);
    }
        /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getRestaurantAfterLogin(Request $request)
    {
       try{
            $user = Auth::user();
            // $restaurantLst = DB::table('mst_user_restaurant')->where('user_id',$user->id)->where('del_flg','0')->orderBy('restaurant_id', 'asc')->get();
             $restaurantLst = DB::table('mst_user_restaurant as a')
                        ->join('mst_restaurant as b', 'a.restaurant_id', '=', 'b.id')
                        ->where('a.user_id', $user->id)
                        ->where('b.del_flg','0')
                         ->where('a.del_flg','0')
                        ->select('b.id', 'b.name')
                        ->get();
            Session::put('restaurantLoginList',$restaurantLst);
            $result = $this->ok("OK");
            $result['restaurantLoginList'] = $restaurantLst;
            Log::info(json_encode($result));
        }catch(Exception $e){
        $result =  $this->fail($e->getMessage());
        DB::rollBack();
      }
       return response()->json($result);
    }
}
