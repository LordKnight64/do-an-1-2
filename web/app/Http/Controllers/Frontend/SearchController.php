<?php


namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;
use Carbon\Carbon;
use Auth;

class SearchController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        Log::Debug($request);
        $data = DB::table('mst_restaurant')->select('name')->where("name", "LIKE", "%{$request->input('query')}%")->where("is_active", 1)->get();
        // $data = '';
         return response()->json($data);
    }

    public function search(Request $request)
    {
        if ($request->key!= null || $request->key!= '') { 
            $id = Auth::id();
            $current = Carbon::now();
            $data['searchInput']= $request->key;
            $saveSearch = DB::table('mst_searchs')->insertGetId(
          ['name' => $request->key,
           'user_id' => empty($id) ? '0' : $id,
           'cre_user_id' => empty($id) ? '0' : $id,
           'mod_user_id' => empty($id) ? '0' : $id,
           'cre_ts' => $current,
           'mod_ts' => $current]);
        } else { 
            $data['searchInput']='';
        }
        if ($request->province!= null || $request->province!= '') {
            $data['province']= $request->province;
        } else { 
            $data['province']='';
        }
        if ($request->restaurant_type != null || $request->restaurant_type != '')
        {
            $restaurant_type = $request->restaurant_type;
        }
        else
        {
            $restaurant_type = '';
        }
        $fiter = null; 
        if ($request->filter!= null || $request->filter!= '') { 
            $data['filter']= $request->filter;
            $fiter =  $data['filter'];
        }
        try {
            if (isset($request->sort)) { 
                if ($request->sort == 'az') {
                    $data['data'] =  DB::table('mst_restaurant')
                ->where("name", "LIKE", "%{$data['searchInput']}%")
                ->where("province", "LIKE", "%{$data['province']}%")
                ->where("restaurant_type","LIKE","%{$restaurant_type}%")
                ->where(function ($query) use ($fiter) {
                    if (isset($fiter)) { 
                        $query->where('delivery_methods', "LIKE", "%{$fiter}%");
                    }
                })
                ->where("is_active", 1)->orderBy('name', 'asc')->paginate(6);
                    Log::info('-------------sort az------------------');
                    $queries = DB::getQueryLog();
                    $last_query = end($queries);
                    Log::info($last_query);
                    $data['itemCount']=DB::table('mst_restaurant')
                ->where("name", "LIKE", "%{$data['searchInput']}%")
                ->where("province", "LIKE", "%{$data['province']}%")
                ->where("restaurant_type","LIKE","%{$restaurant_type}%")
                ->where(function ($query) use ($fiter) {
                    if (isset($fiter)) {
                        $query->where('delivery_methods', "LIKE", "%{$fiter}%");
                    }
                })
                ->where("is_active", 1)->count(); 
                } elseif ($request->sort == 'fee') {
                    $data['data'] =  DB::table('mst_restaurant')
                ->where("name", "LIKE", "%{$data['searchInput']}%")
                ->where("province", "LIKE", "%{$data['province']}%")
                ->where("restaurant_type","LIKE","%{$restaurant_type}%")
                ->where(function ($query) use ($fiter) {
                    if (isset($fiter)) {
                        $query->where('delivery_methods', "LIKE", "%{$fiter}%");
                    }
                })
                ->where("is_active", 1)->orderBy('delivery_charge', 'asc')->paginate(6);
                    Log::info('-------------sort fee------------------');
                    $queries = DB::getQueryLog();
                    $last_query = end($queries);
                    Log::info($last_query);
                    $data['itemCount']=DB::table('mst_restaurant')
                ->where("name", "LIKE", "%{$data['searchInput']}%")
                ->where("province", "LIKE", "%{$data['province']}%")
                ->where("restaurant_type","LIKE","%{$restaurant_type}%")
                ->where(function ($query) use ($fiter) {
                    if (isset($fiter)) {
                        $query->where('delivery_methods', "LIKE", "%{$fiter}%");
                    }
                })
                ->where("is_active", 1)->count();
                } else { 
                    return redirect('/search?key=&sort=');
                }
            } else { 
                Log::info('-------------sort null------------------');
                $data['data'] =  DB::table('mst_restaurant')
              ->where("name", "LIKE", "%{$data['searchInput']}%")
              ->where("province", "LIKE", "%{$data['province']}%")
              ->where("restaurant_type","LIKE","%{$restaurant_type}%")
              ->where("is_active", 1)
              ->where(function ($query) use ($fiter) {
                  if (isset($fiter)) {
                      $query->where('delivery_methods', "LIKE", "%{$fiter}%");
                  }
              })
              ->paginate(6);
                $queries = DB::getQueryLog();
                $last_query = end($queries);
                Log::info($last_query);
                $data['itemCount']= DB::table('mst_restaurant')
              ->where("name", "LIKE", "%{$data['searchInput']}%")
              ->where("province", "LIKE", "%{$data['province']}%")
              ->where("restaurant_type","LIKE","%{$restaurant_type}%")
              ->where("is_active", 1)
              ->where(function ($query) use ($fiter) {
                  if (isset($fiter)) {
                      $query->where('delivery_methods', "LIKE", "%{$fiter}%");
                  }
              })->count();
            }
        } catch (QueryException $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        } catch (Exception $ex) {
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
        }

        if ($request->rank) {
            $data['data'] =  DB::table('mst_restaurant')
            ->where('rating', '>', 0)
            ->orderBy('rating', 'DESC')
            ->paginate(6);
        }

        if ($request->type!=null) {
            $data['data'] =  DB::table('mst_restaurant')
            ->where('restaurant_type', $request->type)
            ->orderBy('rating', 'DESC')
            ->paginate(6);

            $data['res_type'] = DB::table('mst_cd')
            ->where('cd_group', 3)
            ->where('cd', $request->type)
            ->first();
        }

        $data['filter_value'] =  DB::table('mst_cd')
        ->where('cd_group', '0')
        ->get();

        $data['restaurant_type'] = DB::table('mst_cd')
        ->where('cd_group','3')
        ->get();

        return View::make('frontend.restaurantList', $data);
    }
}
