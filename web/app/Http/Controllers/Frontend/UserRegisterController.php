<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\MstUser;
use Log;
use Session;
use Event;
use App\Events\UserAccessEvent;
use App\Services\EmailService;

class UserRegisterController extends BaseController
{
    public function index()
    {
        return view('frontend.userRegister');
    }

    public function doRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'first-name' => 'required|max:255',
        'last-name' => 'required|max:255',
        'email' => 'required|email|unique:mst_user',
        'password' => 'required|min:6'
      ]);

        if ($validator->fails()) {
            return redirect('/userRegister?error=1');
            
        }
        else
        {
            $id = Auth::id();
            $current = Carbon::now();
            DB::table('mst_user')->insertGetId(
        ['first_name' =>  $request['first-name'] ,
        'last_name' =>  $request['last-name'],
        'email' => $request['email'],
        'password' =>   password_hash($request['password'], PASSWORD_BCRYPT),
        'cre_ts' => $current,
        'mod_ts' => $current]);
            $lastRow = DB::table('mst_user')->orderBy('id', 'desc')->first();
            DB::table('mst_role_user')->insertGetId(
        ['user_id' => $lastRow->id,
            'role_id' => 3,
            'user_type' => 'App\Models\MstUser']);
            $iduser = DB::table('mst_user')
                    ->orderBy('id', 'desc')
                    ->first();
             $mailSendObject['type'] = 'registerUser';
             $mailSendObject['email'] = $request['email'];
             $mailSendObject['last_name'] = $request['last_name'];
             $mailSendObject['first_name'] = $request['first_name'];
             $mailSendObject['password'] = $request['password'];
             $mailSendObject['id'] = $iduser->id;
             Session::put('mailSendObject',$mailSendObject);
             Log::info('---------userSend ADD FORM-----------'.json_encode($mailSendObject));
               return redirect("/sendMail");
        }
    }

    public function registerUrlOther(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'email' => 'required|email|unique:mst_user',
      ]);
        if ($validator->fails()) {
            $user = MstUser::whereEmail($request['email'])->first();
            MstUser::where('id', $user->id)->update(array('oauth_provider' => $request['oauth_provider'],'oauth_provider_id' => $request['oauth_provider_id']));
            Auth::login($user);
            Log::info("-----------------TON TAI USER------------------------".json_encode(Auth::user()));
            Session::put('userInfo', Auth::user());

            $result = $this->ok("OK");
            $result['userInfo'] = $user;
            return response()->json($result);
        }
        try {
            Log::info("-----------------try------------------------");
            $passBefor =  str_random(6);
            Log::info("-----------------PASSWORD ------------------------".$passBefor);
            $pass = password_hash($passBefor, PASSWORD_BCRYPT);
            $current = Carbon::now();
            DB::table('mst_user')->insertGetId(
          ['first_name' =>  $request['first_name'] ,
          'last_name' =>  $request['last_name'],
          'email' => $request['email'],
          'password' =>   $pass,
          'oauth_provider'=>$request['oauth_provider'],
          'oauth_provider_id'=>$request['oauth_provider_id'],
          'cre_ts' => $current,
          'mod_ts' => $current]);
            $lastRow = DB::table('mst_user')->orderBy('id', 'desc')->first();
            DB::table('mst_role_user')->insertGetId(
          ['user_id' => $lastRow->id,
            'role_id' => 3,
            'user_type' => 'App\Models\MstUser']);
            $user = MstUser::whereEmail($request['email'])->first();
        // if (Auth::attempt(['email' => $request->get("email"),'password' => $pass])){
        //       Log::info("-----------------CHUA TON TAI USER------------------------".json_encode(Auth::user()));
        //       event(new UserAccessEvent(Auth::user(), UserAccessEvent::LOGIN));
        //       Session::put('userInfo', Auth::user());
        // }
        Auth::login($user);
            Log::info("-----------------CHUA TON TAI USER------------------------".json_encode(Auth::user()));
            Session::put('userInfo', Auth::user());
            $result = $this->ok("OK");
            $result['userInfo'] = Auth::user();
            $result['password'] = $passBefor;
        // $userSend['email']=$request['email'];
        // $userSend['first_name']=$request['first_name'];
        // $userSend['last_name']=$request['last_name'];
        // $userSend['password']=$passBefor;

        // // Send email to user
        // EmailService::send_mail_LoginUrlOrther($userSend, $request['email']);
        // EmailService::send_mail_LoginUrlOrther($userSend, "ifoodgo123@gmail.com");
        } catch (Exception $e) {
            $result =  $this->fail($e->getMessage());
            DB::rollBack();
        }

        return response()->json($result);
    }
    public function sendMailUrlOther(Request $request)
    {
        try {
            $userSend['email']=$request['email'];
            $userSend['first_name']=$request['first_name'];
            $userSend['last_name']=$request['last_name'];
            $userSend['password']=$request['password'];

        // Send email to user
        EmailService::send_mail_LoginUrlOrther($userSend, $request['email']);
            //EmailService::send_mail_LoginUrlOrther($userSend, "kieu-trinh@system-exe.com.vn");
            $result = $this->ok("OK");
        } catch (Exception $e) {
            $result =  $this->fail($e->getMessage());
        }

        return response()->json($result);
    }
}
