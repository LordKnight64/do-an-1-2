<?php


namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use DB;
use Log;
use View;
use Paginator;
use Route;
use Session;
use Auth;

class OrderDetailController extends Controller
{
    public function index(Request $request)
    {
        Session::put('path', 'orderDetail');
        $data['restaurant'] = DB::table('mst_restaurant')
                            ->where('id', $request->restaurant_id)
                            ->first();

        $data['item'] = DB::table('mst_item')
                            ->where('id', $request->item_id)
                            ->first();

        $data['option'] = DB::table('mst_item_option')
                            ->join('mst_option', 'mst_item_option.option_id', 'mst_option.id')
                            ->where('item_id', $request->item_id)
                            ->where('mst_item_option.del_flg','0')
                            ->get();

        $data['option_value'] = DB::table('mst_item_option_value as a')
                            ->join('mst_option_value as b', 'a.option_value_id', 'b.id')
                            ->where('a.item_id', $request->item_id)
                            ->where('a.active_flg','1')
                            ->get();
        //Debug//
        foreach($data['option_value'] as $ov)
        {
            
            Log::info("Option ID: $ov->option_id , Item id: $ov->item_id, Option_value_id: $ov->option_value_id $ov->add_price");
        }
        ///////////////
        $data['order_option'] = DB::table('trn_order_option_value')
                                ->where('order_id',$request->order_id)
                                ->where('item_id',$request->item_id)
                                ->get();

        

        /*foreach($data['option_value'] as $ov)
        {
            foreach($data['order_option'] as $oo)
            {
                if($ov->option_id == $oo->option_id)
                {
                    if($ov->option_value_id == $oo->option_value_id)
                    {
                        $checker[]->seq_no = $oo->seq_no; 
                        $checker[]->id = $oo->option_value_id;
                    }
                }
            }
        }*/
        if(Auth::check() == true)
        {
            $checkWish=DB::table('mst_user_restaurant_wishlist')
                                ->where('user_id',Auth::user()->id)
                                ->where('restaurant_id',$request->restaurant_id)
                                ->where('item_id',$request->item_id)
                                ->where('del_flg',0)
                                ->first();
            if(isset($checkWish))
            {
                $data['Wish'] = 1;
            }
            else
            {
                $data['Wish'] = 0;
            }
        }
        $data['review'] = DB::table('mst_restaurant_reviews')
        ->where('restaurant_id', $request->restaurant_id)
        ->where('item_id', $request->item_id)
        ->where('active_flg', 1)
        ->join('mst_user', 'mst_user.id', 'user_id')
        ->select('mst_restaurant_reviews.title', 'mst_restaurant_reviews.rating', 'mst_restaurant_reviews.content', 'mst_restaurant_reviews.mod_ts as last_time', 'mst_user.email as user_email', 'mst_user.first_name as user_name')
        ->paginate(5);

        $data['count_review'] = DB::table('mst_restaurant_reviews')
        ->where('restaurant_id', $request->restaurant_id)
        ->where('item_id', $request->item_id)
        ->where('active_flg', 1)
        ->join('mst_user', 'mst_user.id', 'user_id')
        ->select('mst_restaurant_reviews.title', 'mst_restaurant_reviews.rating', 'mst_restaurant_reviews.content', 'mst_restaurant_reviews.mod_ts as last_time', 'mst_user.email as user_email', 'mst_user.first_name as user_name')
        ->count();

        $data['avg'] = DB::table('mst_restaurant_reviews')
        ->where('restaurant_id', $request->restaurant_id)
        ->where('item_id', $request->item_id)
        ->where('active_flg', 1)
        ->avg('rating');


        $number = 0;
        try {
            $number = intval($request->number);
        } catch (\Exception $e) {
        }
        $data['number'] = $number;

        return view('frontend.orderDetail')->with($data);
    }

    public function sendReview(Request $request)
    {
        $message_cd = "";
        if (Auth::check()) {
            DB::table('mst_restaurant_reviews')->insertGetId(
            [
              'restaurant_id'=> $request->restaurant_id,
              'item_id' => $request->item_id,
              'user_id' => Auth::user()->id,
              'title' => $request->title,
              'content' => $request->content,
              'rating' => $request->rating
            ]);

            $message_cd = "201";
            return response()->json($message_cd);
        } else {
            $message_cd = "403";
            return response()->json($message_cd);
        }
    }
}
