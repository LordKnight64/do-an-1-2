<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Frontend\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;

class AddRestaurantController extends \Illuminate\Routing\Controller
{
  use ValidatesRequests;

    public function index() {
        Session::put('path','addRestaurant');
        return view('frontend.addRestaurant');
    }

    
}
