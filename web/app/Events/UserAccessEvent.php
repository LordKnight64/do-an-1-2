<?php namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Log;

use App\Events\Event;
use App\Models\MstUser;

/**
 * User access event
 */
class UserAccessEvent extends Event {

	const LOGIN = "LOGIN";
	const LOGOUT = "LOGOUT";

	use SerializesModels;

	public $user;
	public $accessType;

	public function __construct(MstUser $user, $accessType)
    {
        $this->user = $user;
        $this->accessType = $accessType;
    }

}
