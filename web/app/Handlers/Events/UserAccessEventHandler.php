<?php namespace App\Handlers\Events;

use Illuminate\Queue\InteractsWithQueue;
// use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Events\UserAccessEvent;
use Log;
// use App\Models\AccessLog;

/**
 * User access event handler
 */
class UserAccessEventHandler implements ShouldQueue {
	
	use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserWasDeleted  $event
	 * @return void
	 */
	public function handle(UserAccessEvent $event)
	{
		// $accessLog = new AccessLog();
		// $accessLog -> log_type = $event->accessType;
		// $accessLog -> username = $event->user->username;
		// $accessLog -> user_id = $event->user->id;
		// $accessLog->save();
	}
}