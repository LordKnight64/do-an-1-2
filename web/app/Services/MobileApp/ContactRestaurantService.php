<?php
namespace App\Services\MobileApp;

use DB;
use Log;
use Auth;
use Carbon\Carbon;
use App\Services\Base\BaseService;
use App\Models\TrnContacts;

/**
 * ContactRestaurant Service
 */
class ContactRestaurantService extends BaseService {

	/**
	 * [insert_contact_restaurant_get_id description]
	 * @param  [type] $contact [description]
	 * @return [type]          [description]
	 */
	public function insert_contact_restaurant_get_id($contact) {
 	 $current = Carbon::now();
 	 $user = $this->getCurrentUser();
 	 $entityContactRestaurant = new TrnContacts();

 	 $entityContactRestaurant->cre_ts = $current;
 	 $entityContactRestaurant->mod_ts = $current;

	 if( isset($contact->restaurant_id)) {
 		 $entityContactRestaurant->restaurant_id = $contact->restaurant_id;
 	 }
 	 if( isset($contact->name)) {
 		 $entityContactRestaurant->name = $contact->name;
 	 }
	 if( isset($contact->email)) {
 		 $entityContactRestaurant->email = $contact->email;
 	 }
	 if( isset($contact->address)) {
 		 $entityContactRestaurant->address = $contact->address;
 	 }
	 if( isset($contact->phone)) {
 		 $entityContactRestaurant->phone = $contact->phone;
 	 }
 	 if( isset($user->id)) {
 		 $entityContactRestaurant->cre_user_id = $user->id;
 		 $entityContactRestaurant->mod_user_id = $user->id;
 	 }

 	 $entityContactRestaurant->save();
 	 return $entityContactRestaurant->id;
  }
}
