<?php
namespace App\Services\MobileApp;

use DB;
use Log;
use PushNotification;
use App\Services\Base\BaseService;

/**
 * PushNotification Service
 */
class PushNotificationService extends BaseService {

	public static function Message($os, $deviceToken, $title, $message) {

		$notification_message = PushNotification::Message($message,array(
		//  'badge' => 1,
		//  'sound' => 'example.aiff',
		//  'actionLocKey' => 'Action button title!',
		//  'locKey' => 'localized key',
		//  'locArgs' => array(
		// 		 'localized args',
		// 		 'localized args',
		//  ),
		//  'launchImage' => 'image.jpg',
		 'title' => $title,
		));
		
		$appName = "";
		if(strpos($os, "ios") !== false){
			$appName = "appNameIOS";
		}elseif(strpos($os, "android") !== false){
			$appName = "appNameAndroid";
		}else{
			return;
		}

		PushNotification::app($appName)
		->to($deviceToken)
		->send($notification_message);
	}
}
