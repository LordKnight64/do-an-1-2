<?php
namespace App\Services\MobileApp;

use DB;
use Log;

use App\Services\Base\BaseService;

/**
 * News Service
 */
class NewsService extends BaseService {

	/**
	 * [get_news description]
	 * @param  [type] $offset [description]
	 * @param  [type] $limit  [description]
	 * @return [type]         [description]
	 */
	public function get_news($offset, $limit) {

			 $sqlParam = array();
       $sql = "
              select
                id
                , title
                , content as description
                , thumb
                , cre_ts as issue_date
                , restaurant_id
                , slug
              from
                trn_news
              where
                1 = 1
                and del_flg = '0'
			         ";
				$sql .= $this->limitString($limit, $sqlParam );
				$sql .= $this->offsetString($offset, $sqlParam );
        $result = $this->select($sql, $sqlParam);
        return $result;
	}

	/**
	 * [get_total_news description]
	 * @return [type] [description]
	 */
	public function get_total_news() {

       $sql = "
			 select
			   count(*) as total
			 from
			   trn_news
			 where
			   del_flg = '0'
			         ";
        $result = $this->select($sql);
				reset($result);
        return current($result)->total;
	}
}
