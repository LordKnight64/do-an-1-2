<?php
namespace App\Services\MobileApp;

use App\Services\Base\BaseService;
use DB;
use Log;

/**
 * Restaurant Service
 */
class RestaurantService extends BaseService {

	public function find_restaurant($params) {

		$sqlParam = array();
		$sql = "
			 select
			   mr.id
			   , mr.name
			   , mr.address
			   , mr.tel
			   , mr.mobile
			   , mr.email
			   , mr.lat_val
			   , mr.long_val
				 , null as distance
				 , null as duration
			   , mr.website
			   , mr.logo_path as logo
			   , mr.rating
			   , mr.open_time
			   , mr.close_time
			   , mr.promote_code as promotion
			   , mr.promo_discount
			   , mr.restaurant_type as rest_type
			   , mr.facebook_link
			   , mr.twitter_link
			   , mr.linkedIn_link
			   , mr.google_plus_link
			   , mc.cd_name as rest_type_name
			 from
			   mst_restaurant as mr
			   left join mst_cd as mc
			   	on mc.cd_group = '3'
			   	and mc.del_flg = '0'
			   	and mc.cd = mr.restaurant_type
			 where
			   mr.del_flg = '0'
			         ";
		if (isset($params["id"])) {
			$sql .= $this->andWhereString($params["id"], 'mr.id', $sqlParam);
		}
		if (isset($params["province"])) {
			$sql .= $this->andWhereString($params["province"], 'mr.province', $sqlParam);
		}
		if (isset($params["name"])) {
			$sql .= " and mr.name like '%" . $params["name"] . "%'";
		}
		if (isset($params["filter"]) && !is_null($params["filter"])) {
			$sql .= " and locate(" . $params["filter"] . ", mr.delivery_methods) > 0";
		}

		if (isset($params["sort"]) && !empty($params["sort"])) {
			$sql .= " order by mr." . $params["sort"];
		}

		// Log::debug('**************data' . $sql);
		// Log::debug('**************data', $sqlParam);
		$result = $this->select($sql, $sqlParam);

		return $result;
	}

	public function get_all_restaurant() {
		$sqlParam = array();
		$sql = "
		select
			id
			, lat_val
			, long_val
		from
			mst_restaurant
		where
			del_flg = '0'
						";
		$result = $this->select($sql, $sqlParam);
		return $result;
	}

	/**
	 * [get_restaurant_detail get restaurant detail]
	 * @param  [type] $restaurantId [description]
	 * @return [type]               [description]
	 */
	public function get_restaurant_detail($restaurantId) {

		$sqlParam = array();
		$sql = "
			 select
			   id
			   , name
			   , address
			   , tel
			   , mobile
			   , email
			   , lat_val
			   , long_val
				 , null as distance
				 , null as duration
			   , website
			   , logo_path as logo
				 , rating
			   , open_time
			   , close_time
			   , promote_code as promotion
				 , promo_discount
			   , restaurant_type as rest_type
			   , facebook_link
			   , twitter_link
			   , linkedIn_link
			   , google_plus_link
				 , unit_point
			 from
			   mst_restaurant
			 where
			   del_flg = '0'
			         ";
		$sql .= $this->andWhereString($restaurantId, 'id', $sqlParam);
		$result = $this->select($sql, $sqlParam);

		reset($result);
		return current($result);
	}

	/**
	 * [get_restaurant_by_province description]
	 * @param  [type] $province [description]
	 * @return [type]           [description]
	 */
	public function get_restaurant_by_province($province) {

		$sqlParam = array();
		$sql = "
			 select
			   id
			   , name
			   , address
			   , tel
			   , mobile
			   , email
			   , lat_val
			   , long_val
				 , null as distance
				 , null as duration
			   , website
			   , logo_path as logo
			   , open_time
			   , close_time
			   , promote_code as promotion
			   , restaurant_type as rest_type
			   , facebook_link
			   , twitter_link
			   , linkedIn_link
			   , google_plus_link
				 , unit_point
			 from
			   mst_restaurant
			 where
			   del_flg = '0'
			         ";
		$sql .= $this->andWhereString($province, 'province', $sqlParam, false);
		$result = $this->select($sql, $sqlParam);

		return $result;
	}

	/**
	 * [get_ordered_restaurants_by_user description]
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public function get_ordered_restaurants_by_user($userId) {

		$sqlParam = array();
		$sql = "
			 select
				  a.id
				  , a.name
				  , a.address
				  , a.tel
				  , a.mobile
				  , a.email
				  , a.lat_val
				  , a.long_val
				  , null as distance
				  , null as duration
				  , a.website
				  , a.logo_path as logo
				  , a.open_time
				  , a.close_time
				  , a.promote_code as promotion
				  , a.restaurant_type as rest_type
				  , a.facebook_link
				  , a.twitter_link
				  , a.linkedIn_link
				  , a.google_plus_link
					, a.unit_point
				from
				  mst_restaurant a
				  left join trn_order b
				    on a.id = b.restaurant_id and b.del_flg = '0'
				where
				  a.del_flg = '0'
			         ";
		$sql .= $this->andWhereString($userId, 'b.user_id', $sqlParam);
		$result = $this->select($sql, $sqlParam);

		return $result;
	}

	/**
	 * [get_order_setting get order setting]
	 * @param  [type] $restaurantId [description]
	 * @return [type]               [description]
	 */
	public function get_ordering_setting($restaurantId) {
		$sqlParam = array();
		$sql = "
				select
				  payment_fee
				  , delivery_charge
				  , minimum_order
				  , payment_method
				  , delivery_methods
				  , delivery_time
				  , default_currency
				  , food_ordering_system_type
					, unit_point
				from
				  mst_restaurant
				where
				  del_flg = '0'
								";
		$sql .= $this->andWhereString($restaurantId, 'id', $sqlParam);
		$result = $this->select($sql, $sqlParam);

		return $result;
	}

	/**
	 * [get_all_restaurant_not_active_more_than_day description]
	 * @param  [type] $day [description]
	 * @return [type]      [description]
	 */
	public function get_all_restaurant_not_active_more_than_day($day) {

		$sql = "
		select
		  *
		from
		  mst_restaurant
		where
		  is_active = '0'
		  and datediff(CURDATE(), cre_ts) > $day
						";
		$result = $this->select($sql);
		return $result;
	}

	/**
	 * [delete_physical_restaurant_by_id description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete_physical_restaurant_by_id($id) {
		DB::table('mst_restaurant')->where('id', '=', $id)->delete();
	}
}
