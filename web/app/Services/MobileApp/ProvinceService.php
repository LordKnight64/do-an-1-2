<?php
namespace App\Services\MobileApp;

use Auth;
use DB;
use Log;

use App\Services\Base\BaseService;
use Carbon\Carbon;

/**
 * Province Service
 */
class ProvinceService extends BaseService {

	/**
	 * [get_provinces_by_city description]
	 * @param  [type] $city [description]
	 * @return [type]       [description]
	 */
	public function get_provinces_by_city($city) {

	    $sqlParam = array();
       	$sql = "
			 select
			  distinct(province) as name
			 from
			   mst_restaurant
			 where
			   del_flg = '0'
			         ";
				$sql .= $this->andWhereString($city, 'city', $sqlParam, false);
				$sql .= " order by province
				";
				$result = $this->select($sql, $sqlParam);
        return $result;
	}
}
