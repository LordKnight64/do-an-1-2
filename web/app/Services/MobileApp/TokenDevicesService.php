<?php
namespace App\Services\MobileApp;

use DB;
use Log;
use Auth;
use Carbon\Carbon;
use App\Services\Base\BaseService;
use App\Models\MstTokenDevices;

/**
 * TokenDevices Service
 */
class TokenDevicesService extends BaseService {

	/**
	 * [get_detail_by_device_id description]
	 * @param  [type] $deviceId [description]
	 * @return [type]           [description]
	 */
	public function get_detail_by_user_id($userId){
		$sqlParam = array();
		$sql = "
		select
		  a.uuid
		  , a.os
		  , a.token
		from
		  mst_token_devices a
		  left join mst_user_devices b
		    on a.uuid = b.uuid and b.del_flg = '0'
		where
		  a.del_flg = '0'
		 ";
		 $sql .= $this->andWhereString($userId, 'b.user_id', $sqlParam );
		 $result = $this->select($sql, $sqlParam);
		 return $result;
	}

	/**
	 * [insert_subscribe_get_id description]
	 * @param  [type] $tokenDevices [description]
	 * @return [type]               [description]
	 */
	public function insert_subscribe_get_id($tokenDevices) {
 	 $current = Carbon::now();
 	 $user = $this->getCurrentUser();
 	 $entityTokenDevices = new MstTokenDevices();

	 $entityTokenDevices->uuid = $tokenDevices->uuid;
 	 $entityTokenDevices->cre_ts = $current;
 	 $entityTokenDevices->mod_ts = $current;

	 if( isset($tokenDevices->token)) {
 		 $entityTokenDevices->token = $tokenDevices->token;
 	 }
	 if( isset($tokenDevices->os)) {
 		 $entityTokenDevices->os = $tokenDevices->os;
 	 }
 	 if( isset($user->id)) {
 		 $entityTokenDevices->cre_user_id = $user->id;
 		 $entityTokenDevices->mod_user_id = $user->id;
 	 }

 	 $entityTokenDevices->save();
 	 return $entityTokenDevices->uuid;
  }

	/**
	 * [update_subscribe description]
	 * @param  [type] $tokenDevices [description]
	 * @return [type]               [description]
	 */
	public function update_subscribe($tokenDevices) {

				$user = $this->getCurrentUser();
				$id = 0;
				if(isset($user->id)){
					$id = $user->id;
				}

				$current = Carbon::now();

				$result = DB::table('mst_token_devices')
				->where('uuid', $tokenDevices->uuid)
				->update([
					'token' => isset($tokenDevices->token)?$tokenDevices->token:"",
					'os' => isset($tokenDevices->os)?$tokenDevices->os:"",
					'mod_ts' => $current,
					'mod_user_id' => $id]);

        return $result;
	}
}
