<?php
namespace App\Services\MobileApp;

use Auth;
use DB;
use Log;

use App\Models\MstUserAddress;
use App\Services\Base\BaseService;
use Carbon\Carbon;

/**
 * Address Service
 */
class AddressService extends BaseService {

	/**
	 * [get_address_list get address list]
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public function get_address_list($userId) {

	     $sqlParam = array();
       $sql = "
					 select
					   id
					   , name
					   , user_id
					   , lat_val
					   , long_val
					   , tel
					   , address
					 from
					   mst_user_address
					 where
					   1 = 1
					   and del_flg = '0'
			         ";
				$sql .= $this->andWhereString($userId, 'user_id', $sqlParam );
				$result = $this->select($sql, $sqlParam);
        return $result;
	}

	/**
	 * [update_address update address]
	 * @param  [type] $address [user_id, id, name, address, lat_val, long_val, tel]
	 * @return [type]          [description]
	 */
	public function update_address($address) {

		$current = Carbon::now();
		$user = $this->getCurrentUser();

		$entityUserAddress = MstUserAddress::where('id',$address->id)->where('del_flg','0')->first();
		if (!empty($entityUserAddress)) {
				if( isset($address->name)) {
					$entityUserAddress->name = $address->name;
				}
				if( isset($address->user_id)) {
					$entityUserAddress->user_id = $address->user_id;
				}
				if( isset($address->address)) {
					$entityUserAddress->address = $address->address;
				}
				if( isset($address->lat_val)) {
					$entityUserAddress->lat_val = $address->lat_val;
				}
				if( isset($address->long_val)) {
					$entityUserAddress->long_val = $address->long_val;
				}
				if( isset($address->tel)) {
					$entityUserAddress->tel = $address->tel;
				}
				if( isset($user->id)) {
					$entityUserAddress->mod_user_id = $user->id;
				}

				$entityUserAddress->mod_ts = $current;
				$entityUserAddress->version_no = $entityUserAddress->version_no + 1;
				$entityUserAddress->save();
		}
    return $address->id;
	}
}
