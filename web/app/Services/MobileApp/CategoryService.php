<?php
namespace App\Services\MobileApp;

use DB;
use Log;

use App\Services\Base\BaseService;

/**
 * Category Service
 */
class CategoryService extends BaseService {
	/**
	 * [get_category_by_restaurantId get category by restaurantId]
	 * @param  [type] $restaurantId [description]
	 * @return [type]               [description]
	 */
	public function get_category_by_restaurantId($restaurantId) {
				$sqlParam = array();
				$sql = "
				select
				  id
				  ,name
				  ,thumb
				  ,description
				from
				  mst_category
				where
				  del_flg = '0'
								";
				 $sql .= $this->andWhereString($restaurantId, 'restaurant_id', $sqlParam );
				 $result = $this->select($sql, $sqlParam);
				 return $result;
	}
}
