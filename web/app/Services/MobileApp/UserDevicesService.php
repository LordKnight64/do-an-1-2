<?php
namespace App\Services\MobileApp;
use Auth;
use DB;
use Log;
use Carbon\Carbon;

use App\Services\Base\BaseService;

/**
 * UserDevices Service
 */
class UserDevicesService extends BaseService {

	/**
	 * [insert description]
	 * @param  [type] $uuid [description]
	 * @param  [type] $os   [description]
	 * @return [type]       [description]
	 */
	public function insert($uuid, $os) {

				$user = $this->getCurrentUser();
				$id = $user->id;
				$current = Carbon::now();

				$result = DB::table('mst_user_devices')->insertGetId(
				['uuid' => $uuid,
				 'os' => $os,
				 'user_id' => $id,
				 'cre_ts' => $current,
				 'cre_user_id' => $id,
				 'mod_ts' => $current,
				 'mod_user_id' => $id,
				 'version_no' => 0,
				 'del_flg' => '0']);

        return $result;
	}

	/**
	 * [update description]
	 * @param  [type] $uuid [description]
	 * @param  [type] $os   [description]
	 * @return [type]       [description]
	 */
	public function update($uuid, $os) {

				$user = $this->getCurrentUser();
				$id = $user->id;
				$current = Carbon::now();

				$result = DB::table('mst_user_devices')
				->where('user_id', $id)
				->where('uuid', $uuid)
				->update([
					'mod_ts' => $current,
					'mod_user_id' => $id]);

        return $result;
	}
}
