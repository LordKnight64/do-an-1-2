<?php
namespace App\Services\MobileApp;

use DB;
use Log;

use App\Services\Base\BaseService;

/**
 * Notifications Service
 */
class NotificationsService extends BaseService {

	/**
	 * [get_notifications get notifications]
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public function get_notifications($userId) {

	     $sqlParam = array();
       $sql = "
			 select
			   id
			   , order_id
			   , title
			   , content as description
			   , thumb
			   , cre_ts as issue_date
			   , is_active
			 from
			   mst_user_notification
			 where
			   1 = 1
			   and del_flg = '0'
			 ";
			 $sql .= $this->andWhereString($userId, 'user_id', $sqlParam );

      $result = $this->select($sql, $sqlParam);
      return $result;
	}
}
