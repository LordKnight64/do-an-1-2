<?php
namespace App\Services\MobileApp;

use Auth;
use DB;
use Log;

use App\Models\MstUserAddress;
use App\Services\Base\BaseService;
use Carbon\Carbon;

/**
 * HistoryRestaurant Service
 */
class HistoryRestaurantService extends BaseService {

	/**
	 * [insert description]
	 * @param  [type] $restaurant [description]
	 * @return [type]             [description]
	 */
	 public function insert($restaurant) {

 			// 	$user = $this->getCurrentUser();
 			// 	$id = $user->id;
 			// 	$current = Carbon::now();
 			
 				$result = DB::table('mst_history_restaurant')->insertGetId(
 				['id' => $restaurant->id,
				 'name' => $restaurant->name,
				 'address' => $restaurant->address,
				 'tel' => $restaurant->tel ,
				 'mobile' => $restaurant->mobile ,
				 'email' => $restaurant->email ,
				 'lat_val' => $restaurant->lat_val ,
				 'long_val' => $restaurant->long_val ,
				 'website' => $restaurant->website ,
				 'logo_path' => $restaurant->logo_path ,
				 'rating' => $restaurant->rating ,
				 'close_time' => $restaurant->close_time ,
				 'open_time' => $restaurant->open_time ,
				 'facebook_link' => $restaurant->facebook_link ,
				 'twitter_link' => $restaurant->twitter_link ,
				 'linkedin_link' => $restaurant->linkedin_link ,
				 'google_plus_link' => $restaurant->google_plus_link ,
				 'minimum_order' => $restaurant->minimum_order ,
				 'payment_method' => $restaurant->payment_method ,
				 'delivery_methods' => $restaurant->delivery_methods ,
				 'delivery_time' => $restaurant->delivery_time ,
				 'default_currency' => $restaurant->default_currency ,
				 'food_ordering_system_type' => $restaurant->food_ordering_system_type ,
				 'payment_fee' => $restaurant->payment_fee ,
				 'delivery_charge' => $restaurant->delivery_charge ,
				 'restaurant_type' => $restaurant->restaurant_type ,
				 'promo_discount' => $restaurant->promo_discount ,
				 'private_policy' => $restaurant->private_policy ,
				 'about_us' => $restaurant->about_us ,
				 'term_of_use' => $restaurant->term_of_use ,
				 'description' => $restaurant->description ,
				 'cre_ts' => $restaurant->cre_ts ,
				 'cre_user_id' => $restaurant->cre_user_id ,
				 'mod_ts' => $restaurant->mod_ts ,
				 'mod_user_id' => $restaurant->mod_user_id ,
				 'version_no' => $restaurant->version_no ,
				 'del_flg' => $restaurant->del_flg ,
				 'promote_code' => $restaurant->promote_code ,
				 'is_active' => $restaurant->is_active ,
				 'city' => $restaurant->city ,
				 'province' => $restaurant->province ,
				 'country' => $restaurant->country ,
				 'street' =>$restaurant->street ]);
         return $result;
 	}
}
