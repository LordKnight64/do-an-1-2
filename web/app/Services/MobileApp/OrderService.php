<?php
namespace App\Services\MobileApp;

use DB;
use Log;
use Auth;
use Carbon\Carbon;
use App\Services\Base\BaseService;
use App\Models\TrnOrder;
use App\Models\TrnOrderDetail;
use App\Models\TrnOrderOptionValue;

/**
 * Order Service
 */
class OrderService extends BaseService
{

    /**
     * [get_orders_by_id description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function get_order_by_id($id)
    {
        $sqlParam = array();
             //Hard code: a.address_id, b.lat_val, b.long_val
       $sql = "
			 select
				 a.id
				 , a.user_id
				 , a.restaurant_id
				 , a.address_id
				 , a.order_ts as order_time
				 , a.order_sts
				 , a.price as total_price
				 , a.delivery_fee
				 , a.delivery_ts as delivery_time
				 , a.notes
				 , a.is_favorite
				 , a.used_point
				from
				 trn_order a
				where
				 a.del_flg = '0'
			  ";
        $sql .= $this->andWhereString($id, 'a.id', $sqlParam);
        $result = $this->select($sql, $sqlParam);
        reset($result);
        return current($result);
    }

    /**
     * [get_orders_by_userid get orders by userid]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function get_orders_by_userid($userId)
    {
        $sqlParam = array();
             //Hard code: a.address_id, b.lat_val, b.long_val
       $sql = "
			 select
				a.id
				, a.user_id
				, a.restaurant_id
				, a.address_id
				, a.order_ts as order_time
				, a.order_sts
				, b.lat_val
				, b.long_val
				, a.price as total_price
				, a.delivery_fee
				, a.delivery_ts as delivery_time
				, a.notes
				, a.is_favorite
				, a.used_point
				, a.recieved_point
				from
				trn_order a
				left join mst_user_address b
					on a.address_id =  b.id
			where
				1 = 1
				and a.del_flg = '0'

			  ";
        $sql .= $this->andWhereString($userId, 'a.user_id', $sqlParam);
        $sql .= "order by a.order_ts desc";


        $result = $this->select($sql, $sqlParam);
        return $result;
    }

    /**
     * [get_orders_by_restid get orders by restid]
     * @param  [type] $userId       [description]
     * @param  [type] $restaurantId [description]
     * @return [type]               [description]
     */
    public function get_orders_by_restid($userId, $restaurantId)
    {
        $sqlParam = array();
             //Hard code: address_id
       $sql = "
			 select
			  a.id
			  , a.user_id
			  , a.restaurant_id as rest_id
			  , a.address_id
			  , a.order_ts as order_time
			  , a.order_sts
			  , b.lat_val
			  , b.long_val
			  , a.price as total_price
			  , a.delivery_fee
			  , a.delivery_ts as delivery_time
			  , a.notes
			  , a.is_favorite
			  , a.used_point
			  , a.recieved_point
			  , c.promote_code as promotion
			from
			  trn_order a
			  left join mst_user_address b
			    on a.address_id = b.id and b.del_flg = '0'
			  left join mst_restaurant c
			    on a.restaurant_id = c.id  and c.del_flg = '0'
			where
			  and a.del_flg = '0'
			  ";
        $sql .= $this->andWhereString($userId, 'a.user_id', $sqlParam);
        $sql .= $this->andWhereString($restaurantId, 'a.restaurant_id', $sqlParam);
        $result = $this->select($sql, $sqlParam);
        return $result;
    }

    /**
     * [get_list_detail get list detail]
     * @param  [type] $orderId [description]
     * @return [type]          [description]
     */
    public function get_list_detail($orderId)
    {
        $sqlParam = array();
        $sql = "
		select
		  a.item_id
		  , b.name
			, b.price as unit_price
		  , a.seq_no
		  , a.quantity
		  , a.price
		  , a.point
		  , a.notes
		from
		  trn_order_detail a
		  left join mst_item b on a.item_id = b.id
		where
		  1 = 1
		  and a.del_flg = '0'
		";

        $sql .= $this->andWhereString($orderId, 'a.order_id', $sqlParam);
        $result = $this->select($sql, $sqlParam);
        return $result;
    }

    /**
     * [insert_order_get_id insert order get id]
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
 public function insert_order_get_id($order)
 {
     $current = Carbon::now();
     $user = $this->getCurrentUser();
     $entityOrder = new TrnOrder();

     $entityOrder->address_id = 1; //todo: (hard code)
     $entityOrder->order_sts = '0';
     $entityOrder->order_ts = $current;
     $entityOrder->cre_ts = $current;
     $entityOrder->mod_ts = $current;
    //  $entityOrder->version_no = 0;
    //  $entityOrder->del_flg = '0';


     if (isset($order->user_id)) {
         $entityOrder->user_id = $order->user_id;
     }
     if (isset($order->restaurant_id)) {
         $entityOrder->restaurant_id = $order->restaurant_id;
     }

     if (isset($order->price)) {
         $entityOrder->price = $order->price;
     }
     if (isset($order->recieved_point)) {
         $entityOrder->recieved_point = $order->recieved_point;
     }
      if (isset($order->used_point)) {
         $entityOrder->used_point = $order->used_point;
     }
     if (isset($order->notes)) {
         $entityOrder->notes = $order->notes;
     }
     if (isset($order->delivery_ts)) {
         $entityOrder->delivery_ts = $order->delivery_ts;
     }
     if (isset($order->delivery_fee)) {
         $entityOrder->delivery_fee = $order->delivery_fee;
     }
     if (isset($order->delivery_type)) {
         $entityOrder->delivery_type = $order->delivery_type;
     }
     if (isset($order->email)) {
         $entityOrder->email = $order->email;
     }
     if (isset($order->phone)) {
         $entityOrder->phone = $order->phone;
     }
     if (isset($order->address)) {
         $entityOrder->address = $order->address;
     }
     if (isset($user->id)) {
         $entityOrder->cre_user_id = $user->id;
         $entityOrder->mod_user_id = $user->id;
     }
     if (isset($order->name)) {
         $entityOrder->name = $order->name;
     }
     if (isset($order->promo_code)) {
         $entityOrder->promo_code = $order->promo_code;
     }
     if (isset($order->promo_discount)) {
         $entityOrder->promo_discount = $order->promo_discount;
     }
     if (isset($order->delivery_distance)) {
         $entityOrder->delivery_distance = $order->delivery_distance;
     }

     if (isset($order->email)) {
         $entityOrder->email = $order->email;
     }

     if (isset($order->phone)) {
         $entityOrder->phone = $order->phone;
     }

     if (isset($order->name)) {
         $entityOrder->name = $order->name;
     }

     if (isset($order->delivery_type)) {
         $entityOrder->delivery_type = $order->delivery_type;
     }

     if (isset($order->promo_code)) {
         $entityOrder->promo_code = $order->promo_code;
     }

     if (isset($order->promo_discount)) {
         $entityOrder->promo_discount = $order->promo_discount;
     }

     if (isset($order->delivery_distance)) {
         $entityOrder->delivery_distance = $order->delivery_distance;
     }

     $entityOrder->save();
     return $entityOrder->id;
 }

    /**
     * [insert_order_detail_get_id insert order detail get id]
     * @param  [type] $orderDetail [description]
     * @return [type]              [description]
     */
    public function insert_order_detail_get_id($orderDetail)
    {
        $user = $this->getCurrentUser();
        $current = Carbon::now();
        $entityOrderDetail = new TrnOrderDetail();

        $entityOrderDetail->cre_ts = $current;
        $entityOrderDetail->mod_ts = $current;
             //  $entityOrderDetail->version_no = 0;
             //  $entityOrderDetail->del_flg = '0';

             if (isset($orderDetail->order_id)) {
                 $entityOrderDetail->order_id = $orderDetail->order_id;
             }
        if (isset($orderDetail->item_id)) {
            $entityOrderDetail->item_id = $orderDetail->item_id;
        }
        if (isset($orderDetail->seq_no)) {
            $entityOrderDetail->seq_no = $orderDetail->seq_no;
        }
        if (isset($orderDetail->quantity)) {
            $entityOrderDetail->quantity = $orderDetail->quantity;
        }
        if (isset($orderDetail->price)) {
            $entityOrderDetail->price = $orderDetail->price;
        }
        if (isset($orderDetail->point)) {
            $entityOrderDetail->point = $orderDetail->point;
        }
        if (isset($orderDetail->notes)) {
            $entityOrderDetail->notes = $orderDetail->notes;
        }
        if (isset($user->id)) {
            $entityOrderDetail->cre_user_id = $user->id;
            $entityOrderDetail->mod_user_id = $user->id;
        }

        $entityOrderDetail->save();
        return $entityOrderDetail->id;
    }

    /**
     * [insert_order_option_value insert order option value]
     * @param  [type] $orderOptionValue [description]
     * @return [type]                   [description]
     */
    public function insert_order_option_value($orderOptionValue)
    {
        $user = $this->getCurrentUser();
        $current = Carbon::now();
        $entityOrderOptionValue = new TrnOrderOptionValue();

        $entityOrderOptionValue->cre_ts = $current;
        $entityOrderOptionValue->mod_ts = $current;
                // $entityOrderOptionValue->version_no = 0;
                // $entityOrderOptionValue->del_flg = '0';

                if (isset($orderOptionValue->order_id)) {
                    $entityOrderOptionValue->order_id = $orderOptionValue->order_id;
                }
        if (isset($orderOptionValue->item_id)) {
            $entityOrderOptionValue->item_id = $orderOptionValue->item_id;
        }
        if (isset($orderOptionValue->seq_no)) {
            $entityOrderOptionValue->seq_no = $orderOptionValue->seq_no;
        }
        if (isset($orderOptionValue->option_value_id)) {
            $entityOrderOptionValue->option_value_id = $orderOptionValue->option_value_id;
        }
        if (isset($orderOptionValue->option_id)) {
            $entityOrderOptionValue->option_id = $orderOptionValue->option_id;
        }
        if (isset($orderOptionValue->point)) {
            $entityOrderOptionValue->point = $orderOptionValue->point;
        }
        if (isset($user->id)) {
            $entityOrderOptionValue->cre_user_id = $user->id;
            $entityOrderOptionValue->mod_user_id = $user->id;
        }

        $entityOrderOptionValue->save();
        return $entityOrderOptionValue->id;
    }

    /**
     * [update_status_by_id update status by id]
     * @param  [type] $orderId [description]
     * @param  [type] $status  [description]
     * @return [type]          [description]
     */
    public function update_status_by_id($orderId, $status)
    {
        $current = Carbon::now();
        $user = $this->getCurrentUser();

        $entityOrder = TrnOrder::where('id', $orderId)->where('del_flg', '0')->first();
        $entityOrder->order_sts = $status;
        $entityOrder->mod_ts = $current;
        $entityOrder->version_no = $entityOrder->version_no + 1;
        if (isset($user->id)) {
            $entityOrder->mod_user_id = $user->id;
        }
        $entityOrder->save();
    }

    /**
     * [update_status_by_id update status by id]
     * @param  [type] $orderId [description]
     * @param  [type] $status  [description]
     * @return [type]          [description]
     */
    public function set_favorite_by_id($orderId, $isFavorite)
    {
        $current = Carbon::now();
        $user = $this->getCurrentUser();

        $entityOrder = TrnOrder::where('id', $orderId)->where('del_flg', '0')->first();
        $entityOrder->is_favorite = $isFavorite;
        $entityOrder->mod_ts = $current;
        $entityOrder->version_no = $entityOrder->version_no + 1;
        if (isset($user->id)) {
            $entityOrder->mod_user_id = $user->id;
        }
        $entityOrder->save();
    }

    /**
     * [update_status_by_id update status by id]
     * @param  [type] $orderId [description]
     * @param  [type] $status  [description]
     * @return [type]          [description]
     */
    public function get_point($orderId, $point)
    {
        $current = Carbon::now();
        $user = $this->getCurrentUser();
        $entityOrder = TrnOrder::where('id', $orderId)->where('del_flg', '0')->where('recieved_point', '0')->first();
        $entityOrder->recieved_point = $point;
        $entityOrder->mod_ts = $current;
        $entityOrder->version_no = $entityOrder->version_no + 1;
        if (isset($user->id)) {
            $entityOrder->mod_user_id = $user->id;
        }
        $entityOrder->save();
    }
}
