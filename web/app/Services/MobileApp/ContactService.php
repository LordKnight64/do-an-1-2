<?php
namespace App\Services\MobileApp;

use DB;
use Log;
use Auth;
use Carbon\Carbon;
use App\Services\Base\BaseService;
use App\Models\MstContact;

/**
 * Contact Service
 */
class ContactService extends BaseService {

	/**
	 * [insert_contact_get_id description]
	 * @param  [type] $contact [description]
	 * @return [type]          [description]
	 */
	public function insert_contact_get_id($contact) {
 	 $current = Carbon::now();
 	 $user = $this->getCurrentUser();
 	 $entityContact = new MstContact();

 	 $entityContact->cre_ts = $current;
 	 $entityContact->mod_ts = $current;

 	 if( isset($contact->name)) {
 		 $entityContact->name = $contact->name;
 	 }
	 if( isset($contact->email)) {
 		 $entityContact->email = $contact->email;
 	 }
	 if( isset($contact->content)) {
 		 $entityContact->content = $contact->content;
 	 }
	 if( isset($contact->phone)) {
 		 $entityContact->phone = $contact->phone;
 	 }
 	 if( isset($user->id)) {
 		 $entityContact->cre_user_id = $user->id;
 		 $entityContact->mod_user_id = $user->id;
 	 }

 	 $entityContact->save();
 	 return $entityContact->id;
  }
}
