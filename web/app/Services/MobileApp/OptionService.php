<?php
namespace App\Services\MobileApp;

use DB;
use Log;

use App\Services\Base\BaseService;

/**
 * Option Service
 */
class OptionService extends BaseService {

	/**
	 * [get_option_value_by_itemId get option value by itemId]
	 * @param  [type] $itemId [description]
	 * @return [type]         [description]
	 */
	public function get_option_value_by_itemId($itemId) {
				$sqlParam = array();
				$sql = "
				select
				  a.option_id
				  , a.option_value_id
				  , b.name as option_name
				  , c.name as option_value_name
				  , a.add_price
					, a.point
				from
				  mst_item_option_value a
				  left join mst_option b
				    on a.option_id = b.id and b.del_flg = '0'
				  left join mst_option_value c
				    on a.option_value_id = c.id and c.del_flg = '0'
				where
				  a.del_flg = '0'
								";
				 $sql .= $this->andWhereString($itemId, 'a.item_id', $sqlParam );
				 $result = $this->select($sql, $sqlParam);
				 return $result;
	}

	/**
	 * [get_option_value_by_orderId_itemId get option value by orderId itemId]
	 * @param  [type] $orderId [description]
	 * @param  [type] $itemId  [description]
	 * @return [type]          [description]
	 */
	public function get_option_value_by_orderId_itemId($orderId, $itemId) {

	     $sqlParam = array();
       $sql = "
			 select
				  a.option_id
				  , a.option_value_id
				  , b.name as option_name
				  , c.name as option_value_name
				  , a.add_price
					, a.point
				from
				  trn_order_detail d
				  left join trn_order_option_value e
				    on d.order_id = e.order_id and d.item_id = e.item_id and e.del_flg = '0'
				  left join mst_item_option_value a
				    on  a.option_id = e.option_id and a.option_value_id = e.option_value_id and a.item_id = e.item_id and a.del_flg = '0'
				  left join mst_option b
				    on a.option_id = b.id and b.del_flg = '0'
				  left join mst_option_value c
				    on a.option_value_id = c.id and c.del_flg = '0'
				where
				  1 = 1
				  and d.del_flg = '0'
			         ";
				$sql .= $this->andWhereString($orderId, 'd.order_id', $sqlParam );
				$sql .= $this->andWhereString($itemId, 'd.item_id', $sqlParam );
        $result = $this->select($sql, $sqlParam);
        return $result;
	}
}
