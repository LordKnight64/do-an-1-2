<?php
namespace App\Services\MobileApp;

use DB;
use Log;

use App\Services\Base\BaseService;

/**
 * Item Service
 */
class ItemService extends BaseService {

	/**
	 * [get_item_detail_by_id get item detail by id]
	 * @param  [type] $itemId [description]
	 * @return [type]         [description]
	 */
	public function get_item_detail_by_id($itemId) {
				$sqlParam = array();
				$sql = "
				select
				  id
				  , name
				  , price
				  , description
				  , thumb
				  , is_alcoho
				  , food_type
					, point
				from
				  mst_item
				where
				  del_flg = '0'
								";
				 $sql .= $this->andWhereString($itemId, 'id', $sqlParam );
				 $result = $this->select($sql, $sqlParam);

				 return $result;
	}

	/**
	 * [get_item_detail_by_categoryId get item detail by categoryId]
	 * @param  [type] $categoryId [description]
	 * @return [type]             [description]
	 */
	public function get_item_detail_by_categoryId($categoryId) {
				$sqlParam = array();
				$sql = "
				select
				  c.id
				  , c.name
				  , c.price
				  , c.description
				  , c.thumb
				  , c.is_alcoho
				  , c.food_type
					, c.point
				from
				  mst_category a
				  left join mst_item_category b
				    on a.id = b.category_id and b.del_flg = '0'
				  left join mst_item c
				    on b.item_id = c.id and c.del_flg = '0'
				where
				  a.del_flg = '0'
								";
				 $sql .= $this->andWhereString($categoryId, 'a.id', $sqlParam );
				 $result = $this->select($sql, $sqlParam);
				 return $result;
	}
}
