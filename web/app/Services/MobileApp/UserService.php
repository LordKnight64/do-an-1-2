<?php
namespace App\Services\MobileApp;

use Auth;
use DB;
use Log;
use App\Models\MstUser;
use App\Services\Base\BaseService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

/**
 * User Service
 */
class UserService extends BaseService {

	/**
	 * [get_user_by_email Get detail user by email]
	 * @param  [type] $email [description]
	 * @return [type]        [return detail user]
	 */
	public function get_user_by_email($email) {

	     $sqlParam = array();
       $sql = "
			 select
			   id
			   , first_name
			   , last_name
			   , email
			   , password
			   , remember_token
			   , tel
			   , mobile
			   , user_sts
			   , def_address_id
			   , oauth_provider
			   , oauth_provider_id
			 from
			   mst_user
			 where
			   del_flg = '0'
			         ";
				$sql .= $this->andWhereString($email, 'email', $sqlParam );
        $result = $this->select($sql, $sqlParam);

				reset($result);
        return current($result);
	}

	/**
	 * [get_user_by_id description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function get_user_by_id($id) {

	     $sqlParam = array();
       $sql = "
			 select
			   id
			   , first_name
			   , last_name
			   , email
			   , password
			   , remember_token
			   , tel
			   , mobile
			   , user_sts
			   , def_address_id
			   , oauth_provider
			   , oauth_provider_id
			 from
			   mst_user
			 where
			   del_flg = '0'
			         ";
				$sql .= $this->andWhereString($id, 'id', $sqlParam );
        $result = $this->select($sql, $sqlParam);

				reset($result);
        return current($result);
	}

	/**
	 * [insert_user Insert user]
	 * @param  [type] $user [
	 * email, password, remember_token, remember_token,
	 * first_name, last_name, tel, mobile, user_sts,
	 * def_address_id, oauth_provider, oauth_provider_id]
	 * @return [type]       [return id user]
	 */
	public function insert_user_get_id($user){

		$current = Carbon::now();
		$userCur = $this->getCurrentUser();
		$entityUser = new MstUser();

		$entityUser->email = $user->email;
		$entityUser->cre_ts = $current;
		$entityUser->mod_ts = $current;

		if( isset($user->password)) {
			$entityUser->password = Hash::make($user->password);
		}
		if( isset($user->remember_token)) {
			$entityUser->remember_token = $user->remember_token;
		}
		if( isset($user->first_name)) {
			$entityUser->first_name = $user->first_name;
		}
		if( isset($user->last_name)) {
			$entityUser->last_name = $user->last_name;
		}
		if( isset($user->tel)) {
			$entityUser->tel = $user->tel;
		}
		if( isset($user->mobile)) {
			$entityUser->mobile = $user->mobile;
		}
		if( isset($user->user_sts)) {
			$entityUser->user_sts = $user->user_sts;
		}
		if( isset($user->def_address_id)) {
			$entityUser->def_address_id = $user->def_address_id;
		}
		if( isset($user->oauth_provider)) {
			$entityUser->oauth_provider = $user->oauth_provider;
		}
		if( isset($user->oauth_provider_id)) {
			$entityUser->oauth_provider_id = $user->oauth_provider_id;
		}
		if( isset($userCur->id)) {
			$entityUser->cre_user_id = $userCur->id;
			$entityUser->mod_user_id = $userCur->id;
		}

		$entityUser->save();

		return $entityUser->id;
	}
}
