<?php
namespace App\Services\MobileApp;

use DB;
use Log;
use Auth;
use Carbon\Carbon;
use App\Services\Base\BaseService;
use App\Models\MstFeedback;

/**
 * Feedback Service
 */
class FeedbackService extends BaseService {

	/**
	 * [insert_feedback description]
	 * @param  [type] $userId  [description]
	 * @param  [type] $content [description]
	 * @return [type]          [return feedback_id]
	 */
	public function insert_feedback_get_id($userId, $content) {
 	 $current = Carbon::now();
 	 $user = $this->getCurrentUser();
 	 $entityFeedback = new MstFeedback();

 	 $entityFeedback->cre_ts = $current;
 	 $entityFeedback->mod_ts = $current;

 	 if( isset($userId)) {
 		 $entityFeedback->user_id = $userId;
 	 }
	 if( isset($content)) {
 		 $entityFeedback->content = $content;
 	 }
 	 if( isset($user->id)) {
 		 $entityFeedback->cre_user_id = $user->id;
 		 $entityFeedback->mod_user_id = $user->id;
 	 }

 	 $entityFeedback->save();
 	 return $entityFeedback->id;
  }
}
