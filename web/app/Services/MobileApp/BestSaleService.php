<?php
namespace App\Services\MobileApp;

use DB;
use Log;

use App\Services\Base\BaseService;

/**
 * Best Sale Service
 */
class BestSaleService extends BaseService {

	/**
	 * [get_best_sale_by_restid get best sale by restid]
	 * @param  [type] $userId       [description]
	 * @param  [type] $restaurantId [description]
	 * @param  [type] $limit        [description]
	 * @return [type]               [description]
	 */
	public function get_best_sale_by_restid($userId, $restaurantId, $limit) {

	     $sqlParam = array();
       $sql = "
			 select
			    c.id
			   , c.name
			   , c.price
			   , c.thumb
			   , c.is_alcoho
			   , c.food_type
			   , e.name as category_name
			 from
			   trn_order_detail a
			   left join trn_order b
			     on a.order_id = b.id
			   left join mst_item c
			     on a.item_id = c.id
			   left join mst_item_category d
			     on c.id = d.item_id
			   left join mst_category e
			     on d.category_id = e.id
			 where
			   a.del_flg = '0'
			  ";
				$sql .= $this->andWhereString($userId, 'b.user_id', $sqlParam );
				$sql .= $this->andWhereString($restaurantId, 'b.restaurant_id', $sqlParam );
				$sql .= "
				group by
					a.item_id
				  , c.id
				  , c.name
				  , c.price
				  , c.thumb
				  , c.is_alcoho
				  , c.food_type
				  , e.name
				order by
					count(a.item_id) desc, c.id
				";
				$sql .= $this->limitString($limit,$sqlParam);

        $result = $this->select($sql, $sqlParam);
        return $result;
	}
}
