<?php
namespace App\Services\Base;

use DB;
use Log;
use Carbon\Carbon;

/**
 * DB Access Utils
 * @author SystemEXE Nguyen Phu Cuong
 */
trait DbServiceTrail {
	/**
     * select sql
     * @param  [type] $sql      [description]
     * @param  [type] $sqlParam [description]
     * @return [type]           [description]
     */
	public function select($sql, $sqlParam = []) {

        $data = DB::select(DB::raw($sql), $sqlParam);
        return $data;
    }

		/**
     * Add where string to sql
     *
     * @param  [type]
     * @param  [type]
     * @param  [type]
     * @param  [type]
     * @param  boolean
     * @return [type]
     */
    // public function andWhereString($param, $fieldName, $columnField, &$sqlParam, $isExact = false) {
    //     $sql = "";
    //     if( isset($param[$fieldName]) && strlen($param[$fieldName]) > 0) {
    //         if ($isExact) {
    //             $sql .= " and $columnField = ? ";
    //             $sqlParam[] = $param[$fieldName];
    //         } else {
    //             $sql .= " and lower($columnField) like ? ";
    //             $sqlParam[] = '%' . strtolower($param[$fieldName]) . '%';
    //         }
		//
    //     }
    //     return $sql;
    // }

		/**
     * Add where string to sql
     *
     * @param  [type]
     * @param  [type]
     * @param  [type]
     * @param  boolean
     * @return [type]
     */
    public function andWhereString($value, $columnField, &$sqlParam, $isExact = true) {
        $sql = "";
        if( isset($value) && strlen($value) > 0) {
            if ($isExact) {
                $sql .= " and $columnField = ? ";
                $sqlParam[] = $value;
            } else {
                $sql .= " and lower($columnField) like ? ";
                $sqlParam[] = '%' . strtolower($value) . '%';
            }

        }
        return $sql;
    }

		public function limitString($value, &$sqlParam) {
        $sql = "";
				$sql .= " limit ? ";
				$sqlParam[] = $value;
        return $sql;
    }

		public function offsetString($value, &$sqlParam) {
        $sql = "";
				$sql .= " offset ? ";
				$sqlParam[] = $value;
        return $sql;
    }

    /**
     * Get Date from string with format
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function dateFromString($str, $format = 'Y-m-d') {
        return Carbon::createFromFormat($format, $str);
    }

    /**
     * Get Time from string with format
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function timeFromString($str, $format = 'H:i') {
        return Carbon::createFromFormat($format, $str);
    }

    /**
     * Get DateTime from string with format
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function dateTimeFromString($str, $format = 'Y-m-d H:i') {
        return Carbon::createFromFormat($format, $str);
    }
}
