<?php
namespace App\Services\Base;

use DB;
use Log;
use Carbon\Carbon;
use App\Services\Base\DbServiceTrail;
use Auth;
use JWTAuth;

/**
 * BaseService
 */
class BaseService {
	use DbServiceTrail;

	/**
	 * [getCurrentUser description]
	 * @param  [type] $token [If token header not exist then input $token]
	 * @return [type]        [description]
	 */
	public function getCurrentUser($token = NULL) {
		$user = Auth::user();
		if(empty($user)){
			if(empty($token)){
				$token = JWTAuth::getToken();
			}
			try {
				$user = JWTAuth::toUser($token);
			} catch (\Exception $e) {
			}
		}
		return $user;
	}

	// protected function ok($msg = null) {
	// 	return [
	// 		'return_cd' => 0,
	// 		'msg' => $msg
	// 	];
	// }

	// protected function fail($msg = null) {
	// 	return [
	// 		'return_cd' => 1,
	// 		'msg' => $msg
	// 	];
	// }
}
