<?php namespace App\Services;

use Log;
use Cache;
use Auth;
use Config;
use DB;
use DateTime;
use App\Models\MstTokenDevices;

use Minishlink\WebPush\WebPush;


class PushNotificationServiceImpl implements PushNotificationService {

    public function delete_for_key($userId,$optionUrl) {
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        try {
            $optionList = MstTokenDevices::where('os', $optionUrl)
                                        ->where('uuid', $userId)
                                        ->where('del_flg', '0')
                                        ->select('uuid')
                                        ->get();
            if( isset($optionList)) {
                foreach ($optionList as $item) {
                    MstTokenDevices::where('uuid', $item->uuid)->where('os', $optionUrl)->delete();
                }
            }                      
         } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
            return $res;
        }
        return $res;
	}

	public function get_all($optionUrl) {
        $tokenList = MstTokenDevices::where('os', $optionUrl)
                                    ->where('del_flg', '0')
                                    ->select('token')
                                    ->get();
        return $tokenList;
	}

    public function get_for_user($userId,$optionUrl) {
        $tokenList = null;
        try {
            $tokenList = MstTokenDevices::where('os', $optionUrl)
                                        ->where('uuid', $userId)
                                        ->where('del_flg', '0')
                                        ->select('token')
                                        ->get();
            } catch(Exception $ex){ 
                DB::rollBack();
                return null;
            }
        return $tokenList;
	}

    public function store($param) {
        Log::info("--------------PUST SERVICE------------------");
        Log::debug($param['keys']['p256dh']);
        $res['return_cd'] = 0;
        $res['message'] = 'OK';
        try {
            $uuid = null;
            $creUserId = 0;
            if(isset($param['userId'])){
                $creUserId = $param['userId'];
                $uuid = $param['userId'];
            }
            $option = MstTokenDevices::where('os', $param['optionUrl'])
                                        ->where('uuid', $uuid)
                                        ->where('del_flg', '0')
                                        ->select('uuid')
                                        ->first();
            if($uuid!=null && isset($option)) {
                $json = json_encode($param);
                MstTokenDevices::where('uuid',$option->uuid)->update(array('token' => $json));
            }else{
                $tokenDevice = new MstTokenDevices;
                $tokenDevice->uuid = $uuid;
                $tokenDevice->os = $param['optionUrl'];
                $json = json_encode($param);
                $tokenDevice->token = $json;
                $tokenDevice->cre_user_id = $creUserId;
    		    $tokenDevice->mod_user_id = $creUserId;
                $tokenDevice->save();
            }
        } catch(Exception $ex){ 
            $res['return_cd'] = 1;
            $res['message'] = $ex->getMessage();
            DB::rollBack();
            return $res;
        }
        return $res;
    }

    public function push($param) {

        $auth = array(
            'VAPID' => array(
                'subject' => 'https://www.ifoodgo.vn',
                'publicKey' => 'BI3iKGUTs-t5MO5YOm1iPCGXwLKSDo-5jIaMnj0W15tLlsjd6Ln2aayVFuCrraDy-A8n_3Mn9P6qm_J2WQXbz5M',
                'privateKey' => 'n6d7kketu2RRuJ4cSiH_xzQvr6uFfQjmR2EO8gEJETs', // in the real world, this would be in a secret file
            ),
        );
        $webPush = new WebPush($auth);
        $endpoint = $param['endpoint'];
        $payload = $param['payload'];
        $token = $param['keys']['p256dh'];
        $auth = $param['keys']['auth'];

        $result = $webPush->sendNotification($endpoint, $payload, $token, $auth, true);
        if ($result) {
            $res['return_cd'] = '0';
            $res['message'] = 'OK';    
        } else {
            $res['return_cd'] = '1';
            $res['message'] = 'Push error';
        }
        
        return $res;
    }
	
}
