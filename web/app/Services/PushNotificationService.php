<?php namespace App\Services;

interface PushNotificationService {
	
	public function delete_for_key($userId,$optionUrl);
	public function get_all($optionUrl);
	public function get_for_user($userId,$optionUrl);
	public function store($param);
	public function push($param);
	
}
