<?php namespace App\Services;

use Log;
use Cache;
use Auth;
use Config;
use DB;
use DateTime;

use App\Models\MstRestaurant;

class RestaurantServiceImpl implements RestaurantService {

	public function get_res_location() {

		$restaurants = MstRestaurant::where('del_flg', '0')
									->where('is_active', '1')
									->select('id','name',
											'lat_val', 'long_val')
									->get();
		return $restaurants;
	}

}
