<?php namespace App\Services;

interface GeoLocationService {

	public function get_coordinates($address);
	public function get_address_components($address);
	public function get_all_coordinate_info($address);
	public function get_driving_distance($source, $destinations, $radius);
	public function get_location_info_by_lat_long($lat_val, $long_val);
	public function get_all_address_info($lat, $lon);
	public function get_data_coordinates($address);
	public function get_distance_by_address($address1, $address2);
}
