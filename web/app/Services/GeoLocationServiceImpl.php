<?php namespace App\Services;

use Log;
use Cache;
use Auth;
use Config;
use DB;
use DateTime;

class GeoLocationServiceImpl implements GeoLocationService {

	public function get_coordinates($address) {

        $response_a = $this->get_all_coordinate_info($address);
        $status = $response_a->status;
        $coordinate['lat_val'] = null;
        $coordinate['long_val'] = null;
        if ($status != 'ZERO_RESULTS') {
            $results = $response_a->results;

            Log::info('----------------------count($results)---------------');
            Log::info(count($results));
            foreach ($results as $result) {
                $coordinate['lat_val'] = $result->geometry->location->lat;
                $coordinate['long_val'] = $result->geometry->location->lng;
                return $coordinate;
            }
        }
        return $coordinate;
	}
	public function get_address_components($address) {
		$response_a = $this->get_all_coordinate_info($address);
        $status = $response_a->status;

        if ($status != 'ZERO_RESULTS') {
            $results = $response_a->results;

            Log::info('----------------------count($results)---------------');
            Log::info(count($results));
            foreach ($results as $result) {
            	$add_comps = $result->address_components;
            	foreach ($add_comps as $add_comp) {
            		$types = $add_comp->types;
            		foreach ($types as $type) {
            			if ($type == 'route') {
            				$ac['street_sn'] = $add_comp->short_name;
            				$ac['street_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'administrative_area_level_2') {
            				$ac['province_sn'] = $add_comp->short_name;
            				$ac['province_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'administrative_area_level_1') {
            				$ac['city_sn'] = $add_comp->short_name;
            				$ac['city_ln'] = $add_comp->long_name;
            				break;
            			}
            		}
            	}
                return $ac;
            }
        }
        return $coordinate;
	}
	public function get_all_coordinate_info($address) {

		$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=Vietnam&language=vi-VN";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $status = $response_a->status;
        return $response_a;
	}
	public function get_driving_distance($source, $destinations, $radius) {

		$results = [];
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCGY8k0Ref5TFxdJJNpOKHzwBldZL7Bw1c&units=metric";
        $url.="&origins=";
        $url.=$source['lat_val'];
        $url.=",";
        $url.=$source['long_val'];
        $url.="&destinations=";
        $is_first = true;
        foreach ($destinations as $destination) {
            if (!$is_first) {
                $url.= "|";
            } else {
                $is_first = false;
            }
            if (!empty($destination->lat_val) && !empty($destination->long_val)) {
                $url.= $destination->lat_val;
                $url.= ",";
                $url.= $destination->long_val;
            }
        }
        $url.= "&mode=";
        $url.= "driving";
        $url.= "&language=";
        $url.= "en-US";
        // $url.= "vi-VN";
        Log::info('----------------------url');
        Log::info($url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $status = $response_a['status'];

        if ($status == 'OK') {
            $destination_addresses = $response_a['destination_addresses'];
            $rows = $response_a['rows'];
            if (count($rows) > 0) {
                $elements = $rows[0]['elements'];
                $n = count($elements);
                if ($n > 0) {
                    for($i = 0; $i< $n; $i++) {
                        if ($elements[$i]['status'] === 'OK') {
                            $dist = str_replace(" km","",$elements[$i]['distance']['text']);
                            if ($dist <= $radius) {
																$result['id'] = $destinations[$i]->id;
                                $result['address'] = $destination_addresses[$i];
                                $result['lat_val'] = $destinations[$i]->lat_val;
                                $result['long_val'] = $destinations[$i]->long_val;
                                $result['distance'] = $dist;
                                $result['duration'] = str_replace(" mins","",$elements[$i]['duration']['text']);

                                array_push($results, $result);
                            }
                        }
                    }
                }
            }
        }
		return $results;
	}

    public function get_distance($source, $destination) {

		$results = [];
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric";
        $url.="&origins=";
        $url.=$source['lat_val'];
        $url.=",";
        $url.=$source['long_val'];
        $url.="&destinations=";
        $is_first = true;
        $url.= $destination['lat_val'];
        $url.= ",";
        $url.= $destination['long_val'];
        $url.= "&mode=";
        $url.= "driving";
        $url.= "&language=";
        $url.= "en-US";
        // $url.= "vi-VN";
        Log::info('----------------------url');
        Log::info($url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $status = $response_a['status'];

        if ($status == 'OK') {
            $destination_addresses = $response_a['destination_addresses'];
            $rows = $response_a['rows'];
            if (count($rows) > 0) {
                $elements = $rows[0]['elements'];
                $n = count($elements);
                if ($n > 0) {
                    for($i = 0; $i< $n; $i++) {
                        if ($elements[$i]['status'] === 'OK') {
                            $dist = str_replace(" km","",$elements[$i]['distance']['text']);
                            $result['address'] = $destination_addresses[$i];
                            $result['distance'] = $dist;
                           
                            $result['duration'] = str_replace(" mins","",$elements[$i]['duration']['text']);
                            array_push($results, $result);
                        }
                    }
                }
            }
        }
      
		return $results;
	}

	public function get_location_info_by_lat_long($lat_val, $long_val) {

		$url = "http://maps.google.com/maps/api/geocode/json?latlng=$lat_val,$long_val&sensor=false&region=Vietnam&language=vi-VN";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $tmp = current($response_a['results']);
		$address_components = $tmp['address_components'];

        $results = array(
            'country' => '',
            'city' => '',
            'province' => '',
            'street' => '',
        );
		foreach ($address_components as $item) {
			$types = $item['types'];
			foreach ($types as $type) {
                if($type === "country"){
                    $results["country"] = $item['long_name'];
                    break;
                } else if($type === "administrative_area_level_1"){
                    $results["city"] = $item['long_name'];
                    break;
                } else if($type === "administrative_area_level_2"){
                    $results["province"] = $item['long_name'];
                    break;
                } else if($type === "route"){
                    $results["street"] = $item['long_name'];
                    break;
                }
			}
		}
        
        return $results;
	}
    public function get_all_address_info($lat,$lon) {

		$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false&region=Vietnam&language=vi-VN";
        Log::info($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $status = $response_a->status;
        if ($status != 'ZERO_RESULTS') {
            $results = $response_a->results;

            Log::info('----------------------count($results)---------------');
            Log::info(count($results));
            foreach ($results as $result) {
                $ac['address'] = $result->formatted_address;
                $add_comps = $result->address_components;
            	foreach ($add_comps as $add_comp) {
            		$types = $add_comp->types;
            		foreach ($types as $type) {
            			if ($type == 'route') {
            				$ac['street_sn'] = $add_comp->short_name;
            				$ac['street_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'administrative_area_level_2') {
            				$ac['province_sn'] = $add_comp->short_name;
            				$ac['province_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'administrative_area_level_1') {
            				$ac['city_sn'] = $add_comp->short_name;
            				$ac['city_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'country') {
            				$ac['country_sn'] = $add_comp->short_name;
            				$ac['country_ln'] = $add_comp->long_name;
            				break;
            			}
            		}
            	}
                return $ac;
            }
        }
        return null;
	}
    public function get_data_coordinates($address) {

        $response_a = $this->get_all_coordinate_info($address);
        $status = $response_a->status;
        $ac['lat_val'] = null;
        $ac['long_val'] = null;
        if ($status != 'ZERO_RESULTS') {
            $results = $response_a->results;

            Log::info('----------------------count($results)---------------');
            Log::info(count($results));
            foreach ($results as $result) {
                $ac['lat_val'] = $result->geometry->location->lat;
                $ac['long_val'] = $result->geometry->location->lng;
                $add_comps = $result->address_components;
            	foreach ($add_comps as $add_comp) {
            		$types = $add_comp->types;
            		foreach ($types as $type) {
            			if ($type == 'route') {
            				$ac['street_sn'] = $add_comp->short_name;
            				$ac['street_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'administrative_area_level_2') {
            				$ac['province_sn'] = $add_comp->short_name;
            				$ac['province_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'administrative_area_level_1') {
            				$ac['city_sn'] = $add_comp->short_name;
            				$ac['city_ln'] = $add_comp->long_name;
            				break;
            			} else if ($type== 'country') {
            				$ac['country_sn'] = $add_comp->short_name;
            				$ac['country_ln'] = $add_comp->long_name;
            				break;
            			}
            		}
            	}
                return $ac;
            }
        }
        return $ac;
	}

    public function get_distance_by_address($address1, $address2) {
 Log::info('AAAAAAAAAAAAAAAAAAA');
		$results = [];
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric";
        $url.="&origins=";
        $url.=$address1;
        $url.="&destinations=";
        $is_first = true;
        $url.= $address2;
        $url.= "&mode=";
        $url.= "driving";
        $url.= "&language=";
        $url.= "en-US";
        // $url.= "vi-VN";
        Log::info('----------------------url');
        Log::info($url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $status = $response_a['status'];

        if ($status == 'OK') {
            $destination_addresses = $response_a['destination_addresses'];
            $rows = $response_a['rows'];
            if (count($rows) > 0) {
                $elements = $rows[0]['elements'];
                $n = count($elements);
                if ($n > 0) {
                    for($i = 0; $i< $n; $i++) {
                        if ($elements[$i]['status'] === 'OK') {
                            // $dist = str_replace(" km","",$elements[$i]['distance']['text']);
                            $result['address'] = $destination_addresses[$i];
                            $result['distance'] = $elements[$i]['distance']['value'];
                            // $result['duration'] = str_replace(" mins","",$elements[$i]['duration']['text']);
                            $result['duration'] = $elements[$i]['duration']['text'];
                            array_push($results, $result);
                        }
                    }
                }
            }
        }
		return $results;
	}
}
