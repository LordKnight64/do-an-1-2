<?php
namespace App\Services;

use Log;
use Mail;

use App\Services\Base\BaseService;

/**
 * Email Service
 */
class EmailService extends BaseService {

	/**
	 * [register description]
	 * @param  [type] $user     [description]
	 * @param  [type] $password [description]
	 * @return [type]           [description]
	 */
	public static function register($user, $email) {
		$data = [];
		$data['email'] = $user['email'];
		$data['name'] = $user['last_name']." ".$user['first_name'];
		$data['password'] = $user['password'];

		Mail::send("email.register", $data, function($message) use ($email){
			 $message->to($email)
			 ->subject('Subject: Welcome to IFOODGO');
		});
		Log::info("=========SENDING============");
		/*$data = [];
		$data['email'] = $user->email;
		$data['password'] = $password;

		Mail::send("email.register", $data, function($message) use ($user){
			 $message->to($user->email, $user->last_name +" " + $user->first_name)
			 ->subject('Subject: register');
		});*/
	}

	/**
	 * [request_login description]
	 * @param  [type] $user     [description]
	 * @param  [type] $password [description]
	 * @return [type]           [description]
	 */
	public static function request_login($user, $password) {

		$data = [];
		$data['email'] = $user->email;
		$data['password'] = $password;

		Mail::send("email.register", $data, function($message) use ($user){
			 $message->to($user->email, $user->last_name +" " + $user->first_name)
			 ->subject('Subject: request login');
		});
	}

	/**
	 * [change_password description]
	 * @param  [type] $user     [description]
	 * @param  [type] $password [description]
	 * @return [type]           [description]
	 */
	public static function change_password($user, $email) {

		$data = [];
		$data['email'] = $user['email'];
		$data['name'] = $user['last_name']." ".$user['first_name'];

		Mail::send("email.changePassword", $data, function($message) use ($email){
			 $message->to($email)
			 ->subject('Subject: Change Password');
		});
	}

	public static function order($object) {

		$object['restaurant']->logo_path = "data:image/png;base64," . base64_encode(file_get_contents($object['restaurant']->logo_path));
		$data = [];
		$data["user_name"] = $object['user']->last_name + " "+$object['user']->first_name;
		$data["restaurant"] = $object['restaurant'];
		$data["order"] = $object['order'];
		$data["items"] = $object['items'];

		Log::info("SEND MAIL HERE");
		Log::info($object['user']->email);

		Mail::send("email.notifyOrder", $data, function($message) use ($object){
			 $message->to($object['order']->email, $object['user']->last_name +" " + $object['user']->first_name)
			 ->subject('Subject: order');
			//  $message->from('xyz@gmail.com','Virat Gandhi');
		});
	}

	/**
	 * [send_contact description]
	 * @param  [type] $contact [description]
	 * @return [type]          [description]
	 */
	public static function send_contact($contact) {

		$data = [];
		$data['id'] = $contact->id;
		$data['name'] = $contact->name;
		$data['email'] = $contact->email;
		$data['content'] = $contact->content;

		Mail::send("email.contact", $data, function($message) use ($contact){
			 $message->to($contact->email, $contact->name)
			 ->subject('Subject: Send contact');
			//  $message->from('xyz@gmail.com','Virat Gandhi');
		});
	}

	/**
	 * [send_request_demo description]
	 * @param  [type] $contact [description]
	 * @param  [type] $email   [description]
	 * @return [type]          [description]
	 */
	public static function send_request_demo($contact, $email) {

		$data = [];
		$data['id'] = $contact->id;
		$data['name'] = $contact->name;

		Mail::send("email.requestDemo", $data, function($message) use ($email){
			 $message->to($email)
			 ->subject('Subject: send request Demo');
			//  $message->from('xyz@gmail.com','Virat Gandhi');
		});
	}

	/**
	 * [send_mail_LoginUrlOrther description]
	 * @param  [type] $contact [description]
	 * @param  [type] $email   [description]
	 * @return [type]          [description]
	 */
	public static function send_mail_LoginUrlOrther($object, $email) {

		$data = [];
		$data['email'] = $object['email'];
		$data['name'] = $object['last_name']." ".$object['first_name'];
		$data['password'] = $object['password'];

		Mail::send("email.loginUrlRepond", $data, function($message) use ($email){
			 $message->to($email)
			 ->subject('Subject: Send passWord for IFOODGO');
			//  $message->from('xyz@gmail.com','Virat Gandhi');
		});
	}

		/**
	 * [addRestaurant description]
	 * @param  [type] $contact [description]
	 * @param  [type] $email   [description]
	 * @return [type]          [description]
	 */
	public static function send_mail_regisRes($object, $email) {

		$data = [];
		$data['email'] = $object['email'];
		$data['name'] = $object['last_name']." ".$object['first_name'];
		$data['id'] = $object['id'];
		$data['domain'] = $object['domain'];
		Mail::send("email.addRestaurant", $data, function($message) use ($email){
			 $message->to($email)
			 ->subject('Subject: Register Restaurant in IfoodGo');
			//  $message->from('xyz@gmail.com','Virat Gandhi');
		});
	}


	public static function send_mail_marketing($sendObject, $email) {
		$data = [];
		$data['email'] = $sendObject['send_to'];
		$data['content'] = $sendObject['content'];
		$data['subject'] = $sendObject['subject'];
		$data['sendcc'] = $sendObject['send_cc'];
		$data['sendbcc']= $sendObject['send_bb'];
		$strmailto = $data['email'];
		$strmailcc = $data['sendcc'];
		$strmailbcc = $data['sendbcc'];
		preg_match_all('/[^;]*[^-\s;]/', $strmailto, $to);
		$sendto = $to[0];
		preg_match_all('/[^;]*[^-\s;]/', $strmailcc, $cc);
		$sendcc = $cc[0];
		preg_match_all('/[^;]*[^-\s;]/', $strmailbcc, $bcc);
		$sendbcc = $bcc[0];
		Log::info($sendto);
		if($data['sendcc'] != null)
		{
			if($data['sendbcc'] != null)
			{
				Mail::send("email.newsletters", $data, function($message) use ($data,$sendto,$sendcc,$sendbcc){
				$message->to($sendto)
				->subject('Subject: '.$data['subject'])
				->cc($sendcc)
				->bcc($sendbcc);
				});
			}
			else
			{
				Mail::send("email.newsletters", $data, function($message) use ($data,$sendto,$sendcc){
				$message->to($sendto)
				->subject('Subject: '.$data['subject'])
				->cc($sendcc);
				});
			}
			
		}
		else
		{
			if($data['sendbcc'] != null)
			{
				Mail::send("email.newsletters", $data, function($message) use ($data,$sendto,$sendbcc){
				$message->to($sendto)
				->subject('Subject: '.$data['subject'])
				->bcc($sendbcc);
				});
			}
			else
			{
				Mail::send("email.newsletters", $data, function($message) use ($data,$sendto){
				$message->to($sendto)
				->subject('Subject: '.$data['subject']);
				});
			}
		}

		
		Log::info("=========SENDING============");
	}


}
