<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Event;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\UserAccessEvent;
use App\Handlers\Events\UserAccessEventHandler;

use Event;
use Log;
use DB;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        "App\Events\UserAccessEvent" => [
            "App\Handlers\Events\UserAccessEventHandler",
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Log SQL
        if (env('APP_DEBUG') == true ){
            DB::enableQueryLog();
            Event::listen('illuminate.query', function ($query, $bindings, $times, $conn) {

                // Format binding data for sql insertion
                foreach ($bindings as $i => $binding)
                {   
                    if ($binding instanceof \DateTime)
                    {   
                        $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    }
                    else if (is_string($binding))
                    {   
                        $bindings[$i] = "'$binding'";
                    }   
                }       

                // Insert bindings into query
                $rawQuery = str_replace(array('%', '?'), array('%%', '%s'), $query);
                $rawQuery = vsprintf($rawQuery, $bindings); 

                Log::debug('illuminate.query ---------------------- ');
                Log::debug(
                    array (
                        'sql' => $query,
                        'bindings' => $bindings,
                        'times' => $times,
                        'conn' => $conn,
                        'raw' => $rawQuery
                    )
                );
                Log::debug(DB::getQueryLog());
                Log::debug('illuminate.query ---------------------- END');
            });
        }
    }
}
