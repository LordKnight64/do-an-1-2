<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $bindings = [
            'App\Services\RestaurantService'=>'App\Services\RestaurantServiceImpl',
            'App\Services\GeoLocationService'=>'App\Services\GeoLocationServiceImpl',
            'App\Services\PushNotificationService'=>'App\Services\PushNotificationServiceImpl',
        ];

        foreach ($bindings as $key => $value) {
            $this->app->bind($key, $value);
        }
    }
}
