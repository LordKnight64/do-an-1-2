<?php

namespace App\Console\Commands;

use Log;
use DB;
use Illuminate\Console\Command;
use App\Services\MobileApp\RestaurantService;
use App\Services\MobileApp\HistoryRestaurantService;

class CleanseRestaurant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restaurant:cleanse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cleanse restaurant';

    protected $restaurantService;
    protected $historyRestaurantService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RestaurantService $restaurantService,
                                HistoryRestaurantService $historyRestaurantService)
    {
        parent::__construct();

        $this->restaurantService = $restaurantService;
        $this->historyRestaurantService = $historyRestaurantService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day = 30;
        $restaurants = $this->restaurantService->get_all_restaurant_not_active_more_than_day($day);

        DB::beginTransaction();
        try {
          foreach ($restaurants as $restaurant) {
            $this->historyRestaurantService->insert($restaurant);
            $this->restaurantService->delete_physical_restaurant_by_id($restaurant->id);
          }
          DB::commit();
          $this->info('Cleanse restaurant finish!');
        } catch (\Exception $e) {
          DB::rollBack();
          $this->info('Cleanse restaurant error!');
          $this->info($e->getMessage());
        }
    }
}
