<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "SET SESSION sql_mode = 'ALLOW_INVALID_DATES'";
        DB::unprepared($sql);

        $filename = 'database/ERD/iFoodGo-DB.sql';
        $contents = File::get($filename);
        DB::unprepared($contents);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $filename = 'database/ERD/drop.sql';
        $contents = File::get($filename);
        DB::unprepared($contents);
    }
}
