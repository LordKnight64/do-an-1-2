<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\MstUser::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\MstRestaurant::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'tel' => $faker->phoneNumber,
        'mobile' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'lat_val' => $faker->latitude,
        'long_val' => $faker->longitude,
        'website' => $faker->url,
        'logo_path' => null,
        'rest_type' => '0',
        'payment_method' => 'cash',
        'delivery_methods' => 'delivery',
        'food_ordering_system_type' => '0',
        'restaurant_type' => '0'

    ];
});
$factory->define(App\Models\TrnOrder::class, function (Faker\Generator $faker) {

    return [

        'user_id' => '2',
        'address_id' => '1',
        'restaurant_id' => '2',
        'order_sts' => $faker->numberBetween($min = 1, $max = 3),
        'price' => $faker->numberBetween($min = 50000, $max = 1000000),
        // 'notes' => $faker->realText($maxNbChars = 50, $indexSize = 2),
        'delivery_fee' => $faker->numberBetween($min = 10000, $max = 20000),
        'order_ts'=> $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get()),
        'delivery_ts'=> $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get()),
    ];
});

$factory->define(App\Models\TrnOrderDetail::class, function (Faker\Generator $faker) {

    return [

        'order_id' => '1',
        'item_id' => '1',
        'price' => $faker->numberBetween($min = 50000, $max = 1000000),
        // 'notes' => $faker->realText($maxNbChars = 50, $indexSize = 2),
        'quantity' => $faker->numberBetween($min = 1, $max = 5)
    ];
});

$factory->define(App\Models\MstItem::class, function (Faker\Generator $faker) {

    return [

        'restaurant_id' => '1',
        'name' => 'name'.$faker->randomDigit,
        'sku' => $faker->swiftBicNumber,
        'price' => $faker->numberBetween($min = 50000, $max = 1000000),
        'is_alcoho' => '0',
        'food_type' => '0',
    ];
});

$factory->define(App\Models\MstUserAddress::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'lat_val' => $faker->latitude,
        'long_val' => $faker->longitude,
        'tel' => $faker->phoneNumber,
        'user_id' => $faker->unique()->numberBetween($min = 1, $max = 10)
    ];
});

$factory->define(App\Models\TrnOrderOptionValue::class, function (Faker\Generator $faker) {

    return [
        'order_id' => '1',
        'item_id' => '1',
        'option_value_id' => '1',
        'option_id' => '1'
    ];
});

$factory->define(App\Models\MstOptionValue::class, function (Faker\Generator $faker) {

    return [
        'option_id' => '1',
        'name' => 'option_value',
    ];
});

$factory->define(App\Models\MstOption::class, function (Faker\Generator $faker) {

    return [
        'name' => 'option',
    ];
});
$factory->define(App\Models\MstItemOptionValue::class, function (Faker\Generator $faker) {

    return [
        'option_id' => $faker->numberBetween($min = 1, $max = 10),
        'item_id' => $faker->numberBetween($min = 1, $max = 10),
        'option_value_id' => $faker->numberBetween($min = 1, $max = 10),
        'active_flg' => '1',
        'add_price' => $faker->numberBetween($min = 10000, $max = 20000)
    ];
});
$factory->define(App\Models\MstCategory::class, function (Faker\Generator $faker) {

    return [
        'restaurant_id' => '1',
        'name' => 'name',
    ];
});