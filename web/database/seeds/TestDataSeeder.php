<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $this->createRestaurant();
        // $this->createOrder();
        // $this->createOrderDetail();
        // $this->createItem();
        // $this->createUserAddress();
        // $this->createTrnOrderOptionValue();
        // $this->createOptionValue();
        // $this->createOption();
        // $this->createItemOptionValue();
        $this->createCategory();
    }

    private function createRestaurant() {
        App\Models\MstRestaurant::truncate();
        factory(App\Models\MstRestaurant::class, 100)->create()->each(function ($item) {
	        $item->save();
	    });
    }
    private function createOrder() {
        App\Models\TrnOrder::truncate();
        factory(App\Models\TrnOrder::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
    private function createOrderDetail() {
        App\Models\TrnOrderDetail::truncate();
        factory(App\Models\TrnOrderDetail::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }

    private function createItem() {
        App\Models\MstItem::truncate();
        factory(App\Models\MstItem::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }

    private function createUserAddress() {
        App\Models\MstUserAddress::truncate();
        factory(App\Models\MstUserAddress::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
    private function createTrnOrderOptionValue() {
        App\Models\TrnOrderOptionValue::truncate();
        factory(App\Models\TrnOrderOptionValue::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
    private function createOptionValue() {
        App\Models\MstOptionValue::truncate();
        factory(App\Models\MstOptionValue::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
    private function createOption() {
        App\Models\MstOption::truncate();
        factory(App\Models\MstOption::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
    private function createItemOptionValue() {
        App\Models\MstItemOptionValue::truncate();
        factory(App\Models\MstItemOptionValue::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
    private function createCategory() {
        App\Models\MstCategory::truncate();
        factory(App\Models\MstCategory::class, 10)->create()->each(function ($item) {
            $item->save();
        });
    }
}
