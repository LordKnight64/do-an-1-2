<?php

use Illuminate\Database\Seeder;

class RoleDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateLaratrustTables();
        $this->createRole();
        $this->createPermission();
        $this->createRolePermission();
    }

    private function createRole() {
        $roles = [
            [
                'id' => 1,
                'name' => 'administrator',
                'display_name' => 'Administrator',
                'description' => 'Quản trị hệ thống'
            ],
            [
                'id' => 2,
                'name' => 'restaurant',
                'display_name' => 'Restaurant',
                'description' => 'Nhà hàng'
            ],
            [
                'id' => 3,
                'name' => 'users',
                'display_name' => 'Users',
                'description' => 'Người dùng'
            ]
        ];

        foreach ($roles as $item) {
            $item["created_at"] = \Carbon\Carbon::now()->toDateTimeString();
            $item["updated_at"] = \Carbon\Carbon::now()->toDateTimeString();
            DB::table('mst_role')->insert($item);
        }
    }

    private function createPermission() {
        $permissions = [
            [ 'display_name' => 'COM TEST1', 'name' => 'com.test1', 'description' => '' ],
            [ 'display_name' => 'COM TEST2', 'name' => 'com.test2', 'description' => '' ]
        ];

        foreach ($permissions as $permission) {
            $permission["created_at"] = \Carbon\Carbon::now()->toDateTimeString();
            $permission["updated_at"] = \Carbon\Carbon::now()->toDateTimeString();
            DB::table('mst_permission')->insert($permission);
        }
    }

    private function createRolePermission() {
        $rolePermissions = [
            'com.test1'=>[1],
            'com.test2'=>[2,3],
        ];

        foreach ($rolePermissions as $permKey => $value) {
            foreach ($value as $roleId) {
                $newItem = [
                    'permission_id' => App\Models\MstPermission::where('name', $permKey)->first()->id,
                    'role_id' => $roleId,
                ];
                DB::table('mst_permission_role')->insert($newItem);
            }
        }
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    private function truncateLaratrustTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('mst_permission_role')->truncate();
        DB::table('mst_role_user')->truncate();
        \App\Models\MstUser::truncate();
        \App\Models\MstRole::truncate();
        \App\Models\MstPermission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
