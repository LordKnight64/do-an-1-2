<?php

use Illuminate\Database\Seeder;

class InitDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUser();
    }

    private function createUser() {
        DB::table('mst_user')->delete();
        $user = new \App\Models\MstUser();
        $user->name = "admin";
        $user->email = "admin@abc.com";
        $user->password = bcrypt('123456');
        $user->save();
        $user->attachRole(\App\Models\MstRole::findOrFail(1));
        $user->save();

        for( $i = 1; $i < 10 ; $i++ ) {
            $user = new \App\Models\MstUser();
            $user->name = "store" . $i;
            $user->email = "store" . $i . "@abc.com";
            $user->password = bcrypt('123456');
            $user->save();
            $user->attachRole(\App\Models\MstRole::findOrFail(2));
            $user->save();
        }

        for( $i = 1; $i < 10 ; $i++ ) {
            $user = new \App\Models\MstUser();
            $user->name = "user" . $i;
            $user->email = "user" . $i . "@abc.com";
            $user->password = bcrypt('123456');
            $user->save();
            $user->attachRole(\App\Models\MstRole::findOrFail(3));
            $user->save();
        }
    }
}
