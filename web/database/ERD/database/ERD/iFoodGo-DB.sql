SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS mst_item_category;
DROP TABLE IF EXISTS mst_category;
DROP TABLE IF EXISTS mst_cd;
DROP TABLE IF EXISTS mst_item_image;
DROP TABLE IF EXISTS trn_order_option_value;
DROP TABLE IF EXISTS mst_item_option_value;
DROP TABLE IF EXISTS mst_item_option;
DROP TABLE IF EXISTS trn_item_cmt;
DROP TABLE IF EXISTS trn_order_detail;
DROP TABLE IF EXISTS mst_item;
DROP TABLE IF EXISTS mst_option_value;
DROP TABLE IF EXISTS mst_option;
DROP TABLE IF EXISTS mst_restaurant_tags;
DROP TABLE IF EXISTS trn_contacts;
DROP TABLE IF EXISTS trn_news;
DROP TABLE IF EXISTS trn_order;
DROP TABLE IF EXISTS trn_restaurant_cmt;
DROP TABLE IF EXISTS mst_restaurant;
DROP TABLE IF EXISTS mst_tags;
DROP TABLE IF EXISTS mst_user_address;
DROP TABLE IF EXISTS mst_user_devices;
DROP TABLE IF EXISTS mst_user_restaurant;
DROP TABLE IF EXISTS mst_user;
DROP TABLE IF EXISTS trn_blacklist;
DROP TABLE IF EXISTS trn_user_notification;




/* Create Tables */

CREATE TABLE mst_category
(
	id bigint NOT NULL AUTO_INCREMENT,
	restaurant_id bigint NOT NULL,
	name varchar(256),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_cd
(
	cd_group varchar(64) NOT NULL,
	cd varchar(16) NOT NULL,
	cd_name varchar(512),
	cd_val varchar(512),
	display_order int DEFAULT 0,
	stop_flg char DEFAULT '0',
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (cd_group, cd)
);


CREATE TABLE mst_item
(
	id bigint NOT NULL AUTO_INCREMENT,
	restaurant_id bigint NOT NULL,
	name varchar(1024),
	sku varchar(128),
	price decimal,
	start_date date,
	active_flg char,
	description text,
	is_alcoho char DEFAULT '0',
	food_type varchar(4),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_item_category
(
	category_id bigint NOT NULL,
	item_id bigint NOT NULL,
	is_active char DEFAULT '1',
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (category_id, item_id)
);


CREATE TABLE mst_item_image
(
	id bigint NOT NULL AUTO_INCREMENT,
	item_id bigint NOT NULL,
	seq_no int NOT NULL,
	filename varchar(1024) NOT NULL,
	default_flg char DEFAULT '0',
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_item_option
(
	option_id bigint NOT NULL,
	item_id bigint NOT NULL,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (option_id, item_id)
);


CREATE TABLE mst_item_option_value
(
	option_id bigint NOT NULL,
	item_id bigint NOT NULL,
	option_value_id bigint NOT NULL,
	active_flg char,
	add_price decimal DEFAULT 0,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (option_id, item_id, option_value_id)
);


CREATE TABLE mst_option
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(256),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_option_value
(
	id bigint NOT NULL AUTO_INCREMENT,
	option_id bigint NOT NULL,
	name varchar(256),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_restaurant
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(256),
	address varchar(1024),
	tel varchar(32),
	mobile varchar(32),
	email varchar(256),
	lat_val decimal(10,6),
	long_val decimal(10,6),
	website varchar(256),
	logo_path varchar(1024),
	rating int,
	close_time timestamp,
	open_time timestamp,
	facebook_link varchar(512),
	twitter_link varchar(512),
	linkedin_link varchar(512),
	google_plus_link varchar(512),
	minimum_order decimal(19,4),
	payment_method varchar(4),
	delivery_methods varchar(4),
	delivery_time timestamp,
	default_currency varchar(4),
	food_ordering_system_type varchar(4),
	restaurant_type varchar(4),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_restaurant_tags
(
	restaurant_id bigint NOT NULL,
	tags_id bigint NOT NULL,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (restaurant_id, tags_id)
);


CREATE TABLE mst_tags
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(256),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_user
(
	id bigint NOT NULL AUTO_INCREMENT,
	first_name varchar(256),
	last_name varchar(256) NOT NULL,
	email varchar(256) NOT NULL,
	password varchar(256),
	remember_token varchar(100),
	tel varchar(32),
	mobile varchar(32),
	user_sts varchar(4),
	def_address_id bigint,
	oauth_provider varchar(256),
	oauth_provider_id varchar(256),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_user_address
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(256),
	address varchar(1024),
	lat_val decimal(10,6),
	long_val decimal(10,6),
	tel varchar(32),
	user_id bigint NOT NULL,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_user_devices
(
	id bigint NOT NULL AUTO_INCREMENT,
	uuid varchar(1024),
	user_id bigint NOT NULL,
	os varchar(128),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE mst_user_restaurant
(
	user_id bigint NOT NULL,
	restaurant_id bigint NOT NULL,
	purchase_flg char,
	notes text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (user_id, restaurant_id)
);


CREATE TABLE trn_blacklist
(
	id bigint NOT NULL AUTO_INCREMENT,
	user_id bigint,
	email varchar(256),
	phone varchar(256),
	notes text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_contacts
(
	id bigint NOT NULL AUTO_INCREMENT,
	restaurant_id bigint NOT NULL,
	name varchar(256),
	email varchar(256),
	address varchar(1536),
	phone varchar(256),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_item_cmt
(
	id bigint NOT NULL AUTO_INCREMENT,
	user_id bigint NOT NULL,
	item_id bigint NOT NULL,
	rating int DEFAULT 0,
	content text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_news
(
	id bigint NOT NULL AUTO_INCREMENT,
	restaurant_id bigint NOT NULL,
	title varchar(1536) NOT NULL,
	slug varchar(1536) NOT NULL,
	content text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_order
(
	id bigint NOT NULL AUTO_INCREMENT,
	address_id bigint,
	user_id bigint,
	restaurant_id bigint NOT NULL,
	order_ts timestamp,
	name varchar(128) NOT NULL,
	-- 0: moi tao
	-- 1: dang giao
	-- 2: hoan tat
	-- 99: huy bo
	order_sts varchar(4) DEFAULT '0' COMMENT '0: moi tao
1: dang giao
2: hoan tat
99: huy bo',
	price decimal,
	notes text,
	delivery_ts timestamp,
	delivery_fee decimal,
	email varchar(256),
	phone varchar(256),
	address varchar(1024),
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	delivery_type bigint(4) DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE trn_order_detail
(
	id bigint NOT NULL AUTO_INCREMENT,
	order_id bigint NOT NULL,
	item_id bigint NOT NULL,
	seq_no int,
	option_value_id bigint,
	price decimal,
	notes text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_order_option_value
(
	id bigint NOT NULL AUTO_INCREMENT,
	order_id bigint NOT NULL,
	option_id bigint NOT NULL,
	item_id bigint NOT NULL,
	option_value_id bigint NOT NULL,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_restaurant_cmt
(
	id bigint NOT NULL AUTO_INCREMENT,
	user_id bigint NOT NULL,
	restaurant_id bigint NOT NULL,
	rating int DEFAULT 0,
	content text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);


CREATE TABLE trn_user_notification
(
	id bigint NOT NULL AUTO_INCREMENT,
	uuid varchar(1024),
	os varchar(128),
	order_id bigint,
	content text,
	cre_ts timestamp DEFAULT NOW() NOT NULL,
	cre_user_id bigint DEFAULT 0 NOT NULL,
	mod_ts timestamp DEFAULT NOW() NOT NULL,
	mod_user_id bigint DEFAULT 0 NOT NULL,
	version_no int DEFAULT 0,
	del_flg char DEFAULT '0',
	PRIMARY KEY (id)
);



