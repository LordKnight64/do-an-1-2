SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS trn_password_resets;
DROP TABLE IF EXISTS mst_role_user;
DROP TABLE IF EXISTS mst_permission_role;
DROP TABLE IF EXISTS mst_permission_user;
DROP TABLE IF EXISTS mst_role;
DROP TABLE IF EXISTS mst_permission;

DROP TABLE IF EXISTS mst_item_category;
DROP TABLE IF EXISTS mst_category;
DROP TABLE IF EXISTS mst_cd;
DROP TABLE IF EXISTS mst_item_image;
DROP TABLE IF EXISTS trn_order_option_value;
DROP TABLE IF EXISTS mst_item_option_value;
DROP TABLE IF EXISTS mst_item_option;
DROP TABLE IF EXISTS trn_item_cmt;
DROP TABLE IF EXISTS trn_order_detail;
DROP TABLE IF EXISTS mst_item;
DROP TABLE IF EXISTS mst_option_value;
DROP TABLE IF EXISTS mst_option;
DROP TABLE IF EXISTS mst_restaurant_tags;
DROP TABLE IF EXISTS trn_contacts;
DROP TABLE IF EXISTS trn_news;
DROP TABLE IF EXISTS trn_order;
DROP TABLE IF EXISTS trn_restaurant_cmt;
DROP TABLE IF EXISTS mst_restaurant;
DROP TABLE IF EXISTS mst_tags;
DROP TABLE IF EXISTS mst_user_address;
DROP TABLE IF EXISTS mst_user_devices;
DROP TABLE IF EXISTS mst_user_restaurant;
DROP TABLE IF EXISTS mst_user;
DROP TABLE IF EXISTS trn_blacklist;
DROP TABLE IF EXISTS trn_user_notification;