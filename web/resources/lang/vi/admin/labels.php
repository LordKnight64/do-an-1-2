<?php

return [
	'copyright'=>'Copyright© 2016 NTLTT Phú Thọ',
	'reserved'=>'All rights reserved.',

	//screen
	'scr.role'=>'Phân quyền',

	//Com
	'com.colon' => ':',
	
	//home
	'home.home'=> 'Trang chủ',
	
	//login
	'login.description'=> 'Đăng nhập trực tuyến vào công việc kinh doanh của nhà hàng bạn',

	//user-box
	'user_box.my_profile' => 'Thông tin của tôi',
	'user_box.my_orders' => 'Đơn đặt hàng của tôi',
	'user_box.change_password' => 'Thay đổi mật khẩu',
	'user_box.logout' => 'Đăng xuất',

	//user login
	'user.email'=>'Email',
	'user.password'=>'Mật khẩu',
	//Order setting
	'order-setting.title'=>'Cài đặt đơn đặt hàng',

	//howItsWork
	'howItsWork.userlabel'=>'NGƯỜI DÙNG',
];
