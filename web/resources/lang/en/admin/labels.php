<?php

return [
	'copyright'=>'Copyright© 2016 NTLTT Phú Thọ',
	'reserved'=>'All rights reserved.',

	//screen
	'scr.role'=>'Phân quyền',

	//Com
	'com.colon' => ':',
	
	//home
	'home.home'=> 'Home',

	//login
	'login.description'=> 'Sign in to get your online Bussiness',
	
	//user-box
	'user_box.my_profile' => 'My Profile',
	'user_box.my_orders' => 'My Orders',
	'user_box.change_password' => 'Change Password',
	'user_box.logout' => 'Logout',

	//user login
	'user.email'=>'Email',
	'user.password'=>'Password',
	//Order setting
	'order-setting.title'=>'Order Setting',

	//howItsWork
	'howItsWork.userlabel'=>'USER',
];
