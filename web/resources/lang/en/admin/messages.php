<?php

return [

	// Error
	'E000001' => ':value không được tìm thấy.',
	'E000002' => 'Tên đăng nhập hoặc mật khẩu không đúng.',
	'E000003' => 'Bạn không có quyền xóa tài khoản này.',
	'E000004' => 'Không tìm thấy trang cần tìm.',
	'E000005' => 'Bạn không có quyền truy cập trang này.',
	'E000006' => 'Hệ thống xảy ra lỗi, hãy liên hệ với Quản trị viên.',
	'E000007' => 'Ngày không hợp lệ.',
	'E000008' => 'Thời gian không hợp lệ.',
	'E000009' => 'Thời gian nhỏ hơn một tuần.',
	'E000010' => 'Chưa chọn thời gian trong tuần.',
	'E000011' => 'Chưa nhập Tên khách hàng.',
	'E000012' => 'Số điện thoại không hợp lệ.',
    'E000013' => 'Thời gian này đã được đặt.' ,

	//Info
	'I000001' => 'Đăng ký thành công.',
	'I000002' => 'Cập nhật thành công.',

	'I000003' => 'Xóa thành công.',
	'I000004' => 'Xóa không thành công.',
	'I000005' => 'Đã thêm.',
	'I000006' => 'Email khởi tạo mật khẩu đã được gửi.',
	'I000007' => 'Tài khoản của bạn đã được tạo thành công!',
	'I000008' => 'Tài khoản của bạn đã được cập nhật thành công!',
	'I000009' => 'Không thể xóa. ( :reason ).',
	'I000010' => 'Cập nhật không thành công.',
	'I000011' => 'Đăng ký không thành công.',
	//phan quyen
	'I000012' => 'Cập nhật thành công.',
	'I000013' => 'Vui lòng đăng nhập lại nếu có thay đổi quyền.',
	'I000014' => 'Email Unverified',
	'I000015' => 'Password Unverified'
];
