var itemList = [];
var shippDetail = null;
var restaurantCheckout = null;
var userId = null;
var items = [];
var priceTotal = 0;
var pointTotal = 0;
var distanceFlg = 0;
var distance = 0;
var localStorage = window.localStorage;
var ship = 0;
var defaultCurrency = "vnđ"
var checkAcceptFlg = null;
var unit_point = 0;
var getGoogle = 0;
var priceAccumulativePoint = 0;
var discountPrice = 0;
var pointRemain = 0;
var pointUse = 0;
$(document).ready(function() {
    itemList = cookieService.getCookie("ShoppingCart");
    if (localStorage.getItem('ShippingDetail') != undefined || localStorage.getItem('ShippingDetail') != null) {
        shippDetail = JSON.parse(localStorage.getItem('ShippingDetail'));
    }
    // shippDetail = cookieService.getCookie("ShippDetail");
    if (shippDetail != null) {
        // document.getElementById("grandTotal").innerHTML = okResult.data[0].distance;
        document.getElementById("email").innerHTML = shippDetail.email;
        document.getElementById("phone").innerHTML = shippDetail.phone;
        document.getElementById("address").innerHTML = shippDetail.address;
        var contentComment = ConvertBR(shippDetail.content);
        document.getElementById("orderComment").innerHTML = contentComment;
        document.getElementById("name").innerHTML = shippDetail.name;
        document.getElementById("deliveryType").innerHTML = shippDetail.delivery_type_name;
        document.getElementById("deliveryTime").innerHTML = shippDetail.delivery_ts;
    }
    document.getElementById("pricePointRemain").innerHTML = document.getElementById("accumulativePoint").textContent;
    restaurantCheckout = cookieService.getCookie("restaurantCheckout");
    if (restaurantCheckout != null) {
        defaultCurrency = restaurantCheckout.default_currency;
        // document.getElementById("shipping").innerHTML = restaurantCheckout.delivery_charge + " " + defaultCurrency;
        unit_point = restaurantCheckout.unit_point;
    }
    var deliveryType = null;
    if (shippDetail != null) {
        distance = shippDetail.distance;
        deliveryType = shippDetail.delivery_type;
    }
    if (deliveryType != null && deliveryType == 0 && (distance == 0 || distance == null)) {
        getGoogle = 1;
    }
    $('#modal-user-login').on('hidden.bs.modal', function(e) {
        $('#msg-error-login').css('display', 'none');
    });
    // var shipping = document.getElementById('shipping').textContent;
    // var Dis
    addRow();
    setTimeout(function() {
        sumPrice();
    }, 1800);
    if (document.getElementById('checkAccept').checked == true) {
        checkAcceptFlg = 1;
    }

});

function sumPricehadPoint(accumulativePoint) {
    priceAccumulativePoint = accumulativePoint * unit_point;
    var pricePoint = new Intl.NumberFormat('vi-VN').format(priceAccumulativePoint) + " " + defaultCurrency;
    document.getElementById("pricePoint").innerHTML = pricePoint;
    var priceTotalNoHadPoint = priceTotal - (priceTotal * discountPrice) + ship;
    var priceAfterdisCost = 0;
    if (priceTotalNoHadPoint > priceAccumulativePoint) {
        priceAfterdisCost = priceTotalNoHadPoint - priceAccumulativePoint;
        if (checkAcceptFlg == '1') {
            document.getElementById("pricePointRemain").innerHTML = 0;
            pointUse = accumulativePoint - pointRemain;
        }
    } else {
        pointRemain = parseInt((priceAccumulativePoint - priceTotalNoHadPoint) / unit_point);
        pointUse = accumulativePoint - pointRemain;
        priceAccumulativePoint = priceTotalNoHadPoint;
        pricePoint = new Intl.NumberFormat('vi-VN').format(priceAccumulativePoint) + " " + defaultCurrency;
        document.getElementById("pricePoint").innerHTML = pricePoint;
        document.getElementById("pricePointRemain").innerHTML = pointRemain;
    }

    var priceAfterdisCostAfter = new Intl.NumberFormat('vi-VN').format(priceAfterdisCost) + " " + defaultCurrency;
    document.getElementById("grandTotal").innerHTML = priceAfterdisCostAfter;
}

function CheckAccept(event) {

    var valuecheck = event.target.checked;
    if (valuecheck == true) {
        checkAcceptFlg = '1';
        var accumulativePoint = document.getElementById("accumulativePoint").textContent;
        sumPricehadPoint(accumulativePoint);

    } else {
        checkAcceptFlg = null;
        var accumulativePoint = 0;
        sumPricehadPoint(accumulativePoint);
        var accumulativePointDisplay = document.getElementById("accumulativePoint").textContent;
        document.getElementById("pricePointRemain").innerHTML = accumulativePointDisplay;
    }

}

function ConvertBR(input) {
    // Converts carriage returns
    // to <BR> for display in HTML
    console.log(input);
    var output = "";
    console.log(input.length);
    for (var i = 0; i < input.length; i++) {
        console.log(input.charAt(i));
        console.log(input.charCodeAt(i));
        if (i + 1 < input.length && input.charCodeAt(i) == 10) {
            output += "<BR>";
        } else {
            output += input.charAt(i);
        }
    }
    console.log(output);
    return output;
}

function myFunction(event) {
    alert('hey');
}

function addRow() {
    ShoppingCartUI.loadCookieForreviewCheckout();
}


function sumPrice() {
    var address1 = null;
    var deliveryType = null;
    if (shippDetail != null) {
        address1 = shippDetail.address;
        deliveryType = shippDetail.delivery_type;
    }
    if (deliveryType != null && deliveryType == 0) {
        if (getGoogle == 1) {
            var param = {
                'address1': address1,
                'address2': restaurantCheckout.address,
            };
            serverServices.doPost('/get_distance_by_address', param).then(
                function(okResult) {
                    console.log("okResult" + JSON.stringify(okResult));
                    // document.getElementById("grandTotal").innerHTML = okResult.data[0].distance;
                    distance = 0;
                    if (okResult.data == null || okResult.data.length == 0) {
                        distanceFlg = 1;
                        swal("This address is incorrect!", "Request failed.<br> Get address fail.", "warning");
                    } else {
                        distance = okResult.data[0].distance / 1000;
                        document.getElementById("shipping").innerHTML = new Intl.NumberFormat('vi-VN').format(restaurantCheckout.delivery_charge * distance) + " " + defaultCurrency + " (" + new Intl.NumberFormat('vi-VN').format(restaurantCheckout.delivery_charge) + " " + defaultCurrency + "*" + distance + "km";
                        ship = restaurantCheckout.delivery_charge * distance;
                        getDataAfterGetGoogle(distance);
                    }
                },
                function(errResult) {
                    swal("This address is incorrect!", "Request failed.<br> Get address fail.", "warning");
                    console.log('errResult', errResult);
                }
            );
        } else {
            document.getElementById("shipping").innerHTML = new Intl.NumberFormat('vi-VN').format(restaurantCheckout.delivery_charge * distance) + " " + defaultCurrency + " (" + new Intl.NumberFormat('vi-VN').format(restaurantCheckout.delivery_charge) + " " + defaultCurrency + "*" + distance + "km)";
            ship = restaurantCheckout.delivery_charge * distance;
            getDataAfterGetGoogle(distance);
        }
    } else {
        getDataAfterGetGoogle(distance);
    }
}

function getDataAfterGetGoogle(distance) {
    var sum = 0;
    var sumPoint = 0;
    itemList.forEach(function(element) {
        // if (element.item_id == id) {
        sum = sum + element.quantity * element.price;
        sumPoint = sumPoint + element.quantity * element.point;

        var option_items = null;
        if (element.option_items != undefined && element.option_items != null && element.option_items.length > 0) {
            option_items = element.option_items;
        }
        var item = {
            'price': element.price,
            'notes': element.notes,
            'quantity': element.quantity,
            'number': element.number,
            'item_id': element.item_id,
            'option_items': option_items,
            'point': element.point,
        };
        items.push(item);
        // }
    }, this);

    priceTotal = sum;
    pointTotal = sumPoint;
    var userId = document.getElementById("login-user-id").value;

    if (userId != null && userId != "" && restaurantCheckout.promote_code == shippDetail.promo_code) {
        discountPrice = restaurantCheckout.promo_discount;

        document.getElementById("discount").innerHTML = restaurantCheckout.promo_discount;
    }
    $('#priceTotal').val(priceTotal);
    if (checkAcceptFlg == '1') {
        var accumulativePoint = document.getElementById("accumulativePoint").textContent;
        sumPricehadPoint(accumulativePoint);
    } else {
        var accumulativePoint = 0;
        sumPricehadPoint(accumulativePoint);
    }
}

function sendData() {
    var userId = document.getElementById("login-user-id").value;
    // sumPrice();
    if (distanceFlg == 0) {
        if (userId != null && userId != "") {
            sendDataServer(priceTotal, items, userId);
        } else {
            swal("Plealse login!", "warning").then(function() {
                // Redirect if url exist

            });

            swal({
                title: 'Are you sure?',
                text: "Please Login ..! if do not Login please Next?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Next'
            }).then(function() {
                sendDataServer(priceTotal, items, userId);
            });
        }
    } else {
        swal("Do not address!", "Request failed.<br> back checkout input address true.", "warning");
    }
}

function sendDataServer(priceTotal, items, userId) {

    var promoCode = null;
    if (userId != null && userId != "" && shippDetail != null && restaurantCheckout.promote_code == shippDetail.promo_code) {
        promoCode = shippDetail.promo_code;
    }
    if (document.getElementById("address").textContent != null && document.getElementById("address").textContent != null != '' &&
        document.getElementById("phone").textContent != null && document.getElementById("phone").textContent != '' &&
        document.getElementById("email").textContent != null && document.getElementById("email").textContent != '') {
        var userId = document.getElementById("login-user-id").value;
        var param = {
            'name': document.getElementById("name").textContent,
            'email': document.getElementById("email").textContent,
            'phone': document.getElementById("phone").textContent,
            'notes': shippDetail.content,
            'address': document.getElementById("address").textContent,
            'user_id': userId,
            'res_id': restaurantCheckout.id,
            'price': priceTotal,
            'point': pointTotal,
            'delivery_fee': ship,
            'delivery_distance': distance,
            'promo_code': promoCode,
            'delivery_ts': shippDetail.delivery_ts,
            'delivery_type': shippDetail.delivery_type,
            'promo_discount': document.getElementById("discount").textContent,
            'user_id': userId,
            'accumulative_point': pointRemain,
            'used_point': pointUse,
            'items': items
        };
        serverServices.doPost('/payCheckout', param).then(
            function(okResult) {
                if (okResult.data.return_cd === 0) {
                    swal("Sent data success!", "success").then(function() {
                        ShoppingCart.deleteShoppingCart();
                        localStorage.removeItem("ShippingDetail");
                        // Redirect if url exist
                        var url = ENVIRONMENT.DOMAIN_WWW;
                        window.location.href = url;
                    });
                } else {
                    swal("Sent data error!", okResult.data.message, "error");
                    //validate
                }

            },
            function(errResult) {
                swal("Sent data error!", "Request failed.<br> Please contact us again later.", "error");
                console.log('errResult', errResult);
            }
        );
    } else {
        swal("Email, phone , address not null", "error");
    }

}