/**
 * [setShoppingCartItemInOrder Add order to shopping cart with restaurant]
 * @param {[type]} item        [description]
 * @param {[type]} restaurant  [description]
 * @param {[type]} urlRedirect [url if redirect]
 */
function setShoppingCartItemInOrder(wish, urlRedirect) {

   
    var orderDetail = {};
    orderDetail['item_id'] = wish.item_id;
    orderDetail['name'] = wish.item_name;
    orderDetail['price'] = wish.price;
    orderDetail['thumb'] = wish.thumb;
    orderDetail['quantity'] = 1;
    orderDetail['notes'] = "";
    orderDetail['number'] = 0;


    var option_items = [];
    orderDetail['option_items'] = option_items;
    var restaurantCokie = null;
    if (restaurant != null) {
        restaurantCokie = {
            "id": wish.restaurant_id,
            "name": wish.restaurant_name,
            "address": wish.address,
            "promo_discount": wish.promo_discount,
            "promote_code": wish.promote_code,
            "delivery_charge": wish.delivery_charge,
            "default_currency": wish.default_currency,
            "unit_point": restaurant.unit_point,
        }
    }

    console.log('restaurantCokie ', orderDetail, restaurantCokie);

    ShoppingCart.setShoppingCartItem(orderDetail, restaurantCokie, urlRedirect);
}