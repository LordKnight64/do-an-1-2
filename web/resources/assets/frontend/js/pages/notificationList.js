
/**
 * [notificationList.js Description]
 * Function to use:
 * - loadContent(): load all news content to convert
 * - ConvertBR(input): convert ENTER <BR> to display in html
 */



$(document).ready(function() {
	loadContent();
});

function loadContent()
{
    var x = document.getElementsByClassName("newsContents");
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].innerHTML = ConvertBR(x[i].innerHTML);
    }
}

function ConvertBR(input)
{
        var output = "";
		for (var i = 0; i < input.length; i++) {
            
            if(i+1 < input.length && input.charCodeAt(i)==10)
            {
                output += "<BR>";
            }
            else
            {
                output += input.charAt(i);
            }
		}
		
		return output;
}
													
