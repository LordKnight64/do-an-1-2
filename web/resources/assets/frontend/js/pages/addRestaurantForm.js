$(document).ready(function() {
    $.validate({
        form: '#addForm',
        modules: 'location, date, security, file, confirmation',
        ignore: "",
        // ignore: ["first_name"],
        onError: function($form) {
            alert('Validation of form failed!');
        },
        onValidate: function($form) {
            // console.log('--------$form-------', $form);
        },
        onSuccess: function($form) {
            console.log('test');
            return true;
        },
        onModulesLoaded: function() {
            // $('#msg-error-login').css('display','none');
            // $('#country').suggestCountry();
        }
    });
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    $('#modal-user-login').on('hidden.bs.modal', function(e) {
        $('#msg-error-login').css('display', 'none');
    });
    $("select").select2();

    // Validation event listeners
    //   $('input')
    //     .on('beforeValidation', function(value, lang, config) {
    //       console.log('Input "'+this.name+'" is about to become validated');
    //       $(this).attr('data-validation-skipped', 0);
    //     })
    //     .on('validation', function(evt, valid) {
    //       console.log('Input "'+this.name+'" is ' + (valid ? 'VALID' : 'NOT VALID'));
    //     });

    //     $('input').validate(function(valid, elem) {
    //    console.log('Element '+elem.name+' is '+( valid ? 'valid' : 'invalid'));
    // });
    $('.nav-tabs > li').css({ "pointer-events": "none" });
    $('.tab1').click(function() {
        if ($('#addForm').isValid()) {
            checkTab1(function(flag_key) {
                console.log(flag_key);
                if (flag_key == true) {
                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                }
            });


        }
    });

    $('.tab2').click(function() {
        if ($('#addForm').isValid()) {
            checkTab2(function(flag_key) {
                console.log(flag_key);
                if (flag_key == true) {
                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                }
            });

        }
    });

    $('.btnPrevious').click(function() {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
});

$('#open_time').timepicker({ 'timeFormat': 'H:i', 'step': 15 });
$('#close_time').timepicker({ 'timeFormat': 'H:i', 'step': 15 });

$("#Subdomain").on("input", function(e) {
    $('.resturl').text($("#Subdomain").val());
});
var map = null;
var markers = [];
var markersStore = [];

function openMap() {
    setTimeout(function() {
        initAutocomplete();
    }, 800);
}

function checkTab1(callback) {
    var key = false;
    var param = {
        'First_name': $('#first_name').val(),
        'Last_name': $('#last_name').val(),
        'Mobile_number': $('#Mobileno').val(),
        'Tel_number': $('#Telno').val(),
        'Email': $('#email').val(),
        'Password': $('#pass').val(),
        'Password_confirmation': $('#conf_pass').val()
    }
    return serverServices.doPost('/checkTab1', param).then(function(success) {
        if (success.data == null || success.data == "") {
            clear("#err_first_name");
            clear("#err_last_name");
            clear("#err_mobile_no");
            clear("#err_tel_no");
            clear("#err_email");
            clear("#err_pass");
            clear("#err_conf_pass");
            key = true;
        } else {
            showAndClear("#err_first_name", success.data.First_name);
            showAndClear("#err_last_name", success.data.Last_name);
            showAndClear("#err_mobile_no", success.data.Mobile_number);
            showAndClear("#err_tel_no", success.data.Tel_number);
            showAndClear("#err_email", success.data.Email);
            showAndClear("#err_pass", success.data.Password);
            showAndClear("#err_conf_pass", success.data.Password);
        }
        callback(key);

    });
}

function checkTab2(callback) {
    var key = false;
    var param = {
        'Restaurant_name': $('#res_name').val(),
        'Restaurant_tel_number': $('#res_tel').val(),
        'Restaurant_mobile_number': $('#res_mobile').val(),
        'Restaurant_address': $('#txtAddress').val(),
        'Restaurant_email': $('#res_email').val(),
        'Restaurant_open': $('#open_time').val(),
        'Restaurant_close': $('#close_time').val(),
        'Restaurant_Url': $('#Subdomain').val(),

    }
    return serverServices.doPost('/checkTab2', param).then(function(success) {
        if (success.data == null || success.data == "") {
            clear("#err_res_name");
            clear("#err_res_tel");
            clear("#err_res_mobile");
            clear("#err_txtAddress");
            clear("#err_res_email");
            clear("#err_open_time");
            clear("#err_close_time");
            clear("#err_Subdomain");
            var object = {
                'address': $('#txtAddress').val()
            };
            swal("Checking address!", "Your address checking", "warning").then(function() {
                serverServices.doPost("/getLatlon", object).then(function(success) {
                    console.log(success.data);
                    if (success.data == null || success.data.length == 0 || success.data.lat_val == null || success.data.lat_val == '' || success.data.long_val == null || success.data.long_val == '') {
                        swal("Do not address!", "Request failed.<br> Get address fail.", "warning");
                        key = false;
                        callback(key);
                    } else {
                        var latmap = success.data.lat_val;
                        var lonMap = success.data.long_val;
                        var country = success.data.country_ln;
                        var city = success.data.city_ln;
                        var street = success.data.street_ln;
                        var province = success.data.province_ln;
                        $('#txtCountry').val(country);
                        $('#txtProvince').val(province);
                        $('#txtCity').val(city);
                        $('#txtLat').val(latmap);
                        $('#txtLon').val(lonMap);
                        $('#txtStreet').val(street);
                        key = true;
                        callback(key);
                    }
                });

            });

        } else {
            showAndClear("#err_res_name", success.data.Restaurant_name);
            showAndClear("#err_res_tel", success.data.Restaurant_tel_number);
            showAndClear("#err_res_mobile", success.data.Restaurant_mobile_number);
            showAndClear("#err_txtAddress", success.data.Restaurant_address);
            showAndClear("#err_res_email", success.data.Restaurant_email);
            showAndClear("#err_open_time", success.data.Restaurant_open);
            showAndClear("#err_close_time", success.data.Restaurant_close);
            showAndClear("#err_Subdomain", success.data.Restaurant_Url);
            // setTimeout(function() {
            //     SearchMapChange();
            // }, 800);
            callback(key);
        }
    });
}

function showAndClear(id, data) {
    if (data != null) {
        $(id).text(data);
    } else
        $(id).text('');
}

function clear(id) {
    $(id).text('');
}
// This example adds a search box to a map, using the Google  Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
function SearchMapChange() {
    fetchLatLon($('#txtAddress').val());
}
var markerCurrentLocation = null;

function fetchLatLon(address) {
    var object = {
        'address': address
    };
    serverServices.doPost("/getLatlon", object).then(function(success) {
        console.log(success.data);
        var latmap = success.data.lat_val;
        var lonMap = success.data.long_val;
        var country = success.data.country_ln;
        var city = success.data.city_ln;
        var street = success.data.street_ln;
        var province = success.data.province_ln;

        var positionCurrent = new google.maps.LatLng(latmap, lonMap); //(user used web on DB)

        markerCurrentLocation.setPosition(positionCurrent);
        map.panTo(markerCurrentLocation.position);
        var address = {
            'lat': latmap,
            'lon': lonMap
        };
        serverServices.doPost("/getAddress", address).then(function(success) {
            console.log(success.data.address);
            $("#txtAddress").val(success.data.address);
            var object = {
                'address': success.data.address
            };
            serverServices.doPost("/getLatlon", object).then(function(success1) {
                $('#txtCountry').val(country);
                $('#txtProvince').val(province);
                $('#txtCity').val(city);
                $('#txtLat').val(latmap);
                $('#txtLon').val(lonMap);
                $('#txtStreet').val(street);
            });
        });

    });

}

function initAutocomplete() {
    map = new google.maps.Map(document.getElementById('order-map'), {
        center: {
            lat: 10.8020047,
            lng: 106.6413804
        },
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Create the search box and link it to the UI element.
    var positionCurrent = new google.maps.LatLng(10.8020047, 106.6413804); //(user used web on DB)
    markerCurrentLocation = new google.maps.Marker({ draggable: true, position: positionCurrent, map: map, title: 'Test!' });

    var positionMarker = null;
    markerCurrentLocation.addListener('dragend', function(event) {
        // alert(event.latLng +"/"+ markerCurrentLocation.getPosition());
        positionMarker = markerCurrentLocation.getPosition();
        console.log(positionMarker.lat() + ' ' + positionMarker.lng());
        var address = {
            'lat': positionMarker.lat(),
            'lon': positionMarker.lng()
        };
        serverServices.doPost("/getAddress", address).then(function(success) {
            console.log(success.data.address);
            $("#txtAddress").val(success.data.address);

        });

    });
}
