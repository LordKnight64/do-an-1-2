$(document).ready(function() {
	$.validate({
		form: '#sent_contact',
		modules: 'location, date, security, file',
		onError: function($form) {
			// alert('Validation of form failed!');
		},
		onSuccess: function($form) {
			var res_id = $('#res_id').val();
			var res_email = $('#res_email').val();

			//Sent request with action is sent_contact_restaurant
			if(res_id != null && res_id.length !== 0){
				var param = {
					'restaurant_id': res_id ,
					'restaurant_email': res_email,
					'name': $form.find('input[name="name"]').val(),
					'email': $form.find('input[name="email"]').val(),
					'phone': $form.find('input[name="phone"]').val(),
					'content': $form.find('textarea[name="message"]').val(),
				};
				serverServices.doPost('/sent_contact_restaurant', param).then(
					function(okResult) {
						if(okResult.data.return_cd === 0){
							swal("Sent contact success!", "Thank you for contacting.<br> We will respond as soon as possible.", "success");
						}else{
							swal("Sent contact error!", okResult.data.message, "error");
						}
					},
					function(errResult) {
						swal("Sent contact error!", "Request failed.<br> Please contact us again later.", "error");
						console.log('errResult', errResult);
					}
				);
			}else{	//Sent request with action is sent_contact
				var param = {
					'name': $form.find('input[name="name"]').val(),
					'email': $form.find('input[name="email"]').val(),
					'phone': $form.find('input[name="phone"]').val(),
					'content': $form.find('textarea[name="message"]').val(),
				};
				serverServices.doPost('/sent_contact', param).then(
					function(okResult) {
						if(okResult.data.return_cd === 0){
							swal("Sent contact success!", "Thank you for contacting.<br> We will respond as soon as possible.", "success");
						}else{
							swal("Sent contact error!", okResult.data.message, "error");
						}
					},
					function(errResult) {
						swal("Sent contact error!", "Request failed.<br> Please contact us again later.", "error");
						console.log('errResult', errResult);
					}
				);
			}

			return false;
		},
		onModulesLoaded: function() {

		}
	});

	$('#modal-user-login').on('hidden.bs.modal', function (e) {
	 	$('#msg-error-login').css('display','none');
	});
});
