$(document).ready(function() {
    $.validate({
        form: '#forgotPass',
        modules: 'location, date, security, file, confirmation',
        ignore: "",
        // ignore: ["first_name"],
        onError: function($form) {
            //alert('Validation of form failed!');
        },
        onValidate: function($form) {
            // console.log('--------$form-------', $form);
        },
        onSuccess: function($form) {
            console.log('test');
            return true;
        },
        onModulesLoaded: function() {
            // $('#msg-error-login').css('display','none');
            // $('#country').suggestCountry();
        }
    });
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    // Validation event listeners
    //   $('input')
    //     .on('beforeValidation', function(value, lang, config) {
    //       console.log('Input "'+this.name+'" is about to become validated');
    //       $(this).attr('data-validation-skipped', 0);
    //     })
    //     .on('validation', function(evt, valid) {
    //       console.log('Input "'+this.name+'" is ' + (valid ? 'VALID' : 'NOT VALID'));
    //     });

    //     $('input').validate(function(valid, elem) {
    //    console.log('Element '+elem.name+' is '+( valid ? 'valid' : 'invalid'));
    // });
    
});