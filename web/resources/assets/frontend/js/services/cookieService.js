var cookieService = {

	setCookie: function(key, value, expireDay) {
			var day = new Date();
			day.setTime(day.getTime() + (expireDay * 24 * 60 * 60 * 1000));
			var expires = "expires="+day.toUTCString();
			document.cookie = key + "=" + JSON.stringify(value) + ";" + expires + ";path=/";
	},

	getCookie: function(key) {
			var name = key + "=";
			var tmp = document.cookie.split(';');
			for(var i = 0; i < tmp.length; i++) {
					var strVal = tmp[i];
					while (strVal.charAt(0) == ' ') {
							strVal = strVal.substring(1);
					}
					if (strVal.indexOf(name) == 0) {
							var result = strVal.substring(name.length, strVal.length);
							return JSON.parse(result);
					}
			}
			return null;
	},

	updateCookie: function(key, value, expireDay){
		this.setCookie(key, value, expireDay);
	},

	deleteCookie: function(key){
		this.setCookie(key, "", -1);
	}
};
