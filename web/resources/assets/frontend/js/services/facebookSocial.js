//=====================================================
//Start: lib facebook
window.fbAsyncInit = function() {
    FB.init({
        appId: '1828623760797825',
        cookie: true,
        xfbml: true,
        version: 'v2.9'
    });

    FB.AppEvents.logPageView();

    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//todo
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();
    } else {}
}

//todo
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        console.log('Successful login for: ' + response.name);
    });
}

//End: lib facebook

//=====================================================
//Start: FacebookSocial
var FacebookSocial = {

        login: function() {
            FB.login(function(response) {
                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ' + JSON.stringify(response.authResponse));
                    FB.api('me?fields=id,name,first_name,last_name,email,address,mobile_phone', function(responseApi) {
                        console.log('Good to see you, ' + JSON.stringify(responseApi) + '.');
                        var accessToken = response.authResponse.accessToken;
                        console.log('accessToken ' + accessToken);
                        var userInfoCreate = {
                            'first_name': responseApi.first_name,
                            'last_name': responseApi.last_name,
                            'email': responseApi.email,
                            'oauth_provider': accessToken,
                            'oauth_provider_id': 'facebook',

                        }
                        $('.modal').addClass('on_overlay_loading');
                        serverServices.doPost('/registerUrlOther', userInfoCreate).then(
                            function(okResult) {
                                $('#check_user').val(okResult.data.userInfo);
                                if (okResult.data.password != undefined && okResult.data.password != null && okResult.data.password != "") {
                                    swal("Login success! System sendding mail to you", "success").then(function() {
                                        var usermail = {
                                            'first_name': responseApi.first_name,
                                            'last_name': responseApi.last_name,
                                            'email': responseApi.email,
                                            'password': okResult.data.password
                                        }
                                        serverServices.doPost('/sendMailUrlOther', usermail);
                                        swal("System  had send mail to you!", "Please check your mail", "success").then(function() {
                                            FacebookSocial.setModelAfterLogin(okResult);
                                        });
                                    });
                                } else {
                                    swal("Login success!", "Account had exist in system.", "success").then(function() {
                                        FacebookSocial.setModelAfterLogin(okResult);
                                    });
                                }
                            },
                            function(errResult) {
                                swal("Sent data error!", "Request failed.<br> Please contact us again later.", "error");
                                console.log('errResult', errResult);
                            }
                        );

                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {
                scope: 'public_profile,email,user_address, user_mobile_phone',
                return_scopes: true
            });
        },

        logout: function() {
            FB.logout(function(response) {
                // user is now logged out
            });
        },

        getDetail: function() {
            FB.api('/me', function(response) {
                console.log("response data:");
                console.log(JSON.stringify(response));
            });
        },
        setModelAfterLogin: function(okResult) {
            $('.modal').removeClass('on_overlay_loading');
            $("#modal-user-login").modal('hide');
            document.getElementById("chatheadButton").style.display = "block";
            document.getElementById("userNameLogin").innerHTML = okResult.data.userInfo.first_name;
            $('#userIdLogin').val(okResult.data.userInfo.id);
            document.getElementById("accumulativePoint").innerHTML = okResult.data.userInfo.accumulative_point;
            serverServices.doPost('/getRestaurantAfterLogin').then(
                function(okResult2) {
                    if (okResult2.data.return_cd === 0) {
                        $('#retaurantLoginLst').val(JSON.stringify(okResult2.data.restaurantLoginList));
                        modalUserAfterLoginUI.drawRestaurant();
                    } else {
                        swal("Cannot connect to server", "Please login again", "error");
                    }
                },
                function(errResult2) {
                    console.log('errResult', errResult2);
                    swal("Cannot connect to server", "Please login again", "error");
                }
            );
        }
    }
    //End: FacebookSocial