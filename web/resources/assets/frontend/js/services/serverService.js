var serverServices = {

	doPost: function(action, params) {
		var deferred = $.Deferred();

		var url = ENVIRONMENT.DOMAIN_WWW + action;
		// url = url.replace('//', '/');
		// console.log('url', url);

		var httpParams = {
			headers: {
				'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")
			},
			method: 'POST',
			url: url,
			data: params
		};

		httpParams.actionStartDate = moment();
		if (ENVIRONMENT.APP_DEBUG) {
			console.log(httpParams.actionStartDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   START] : ' + httpParams.url);
			console.log('PARAM: ', JSON.stringify(params));
			// console.log(DateUtils.getNowString() + ' [ACTION   START] : ' + httpParams.url);
			// console.log('PARAM: ', angular.toJson(params));
		}

		$.ajax(httpParams).success(function(data, status, headers) {
			// console.log('data', data);
			// console.log('status', status);
			// console.log('headers', headers);
			if (ENVIRONMENT.APP_DEBUG) {
				var actionEndDate = moment();
				console.log(actionEndDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   END  ] : ' + httpParams.url + ' [' + (actionEndDate - httpParams.actionStartDate) + 'ms]');
				console.log('RESULT: ', JSON.stringify(data));
				// console.log('RESULT: ', angular.copy(data)));
			}

			var callbackStartDate = null;
			if (ENVIRONMENT.APP_DEBUG) {
				callbackStartDate = moment();
				console.log(callbackStartDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [CALLBACK START] : ' + httpParams.url);
			}

			deferred.resolve({
				data: data,
				status: status,
				headers: headers
			});

			if (ENVIRONMENT.APP_DEBUG) {
				var callbackEndDate = moment();
				console.log(callbackEndDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [CALLBACK END  ] : ' + httpParams.url + ' [' + (callbackEndDate - callbackStartDate) + 'ms]');
			}
		}).error(function(data, status, headers) {

			if (ENVIRONMENT.APP_DEBUG) {
				var actionEndDate = moment();
				console.log(actionEndDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   ERROR] : ' + httpParams.url + ' [' + (actionEndDate - httpParams.actionStartDate) + 'ms]');
			}

			if (status == 500) {
				// ClientService.showError($filter('translate')("MSG.F0000001"));
			} else if (status == 422) {
				// ClientService.showWarn($filter('translate')("MSG.F0000003"));
			}

			deferred.reject({
				data: data,
				status: status,
				headers: headers,
				// config: config
			});

			// if (ENVIRONMENT.APP_DEBUG) {
			// 	$log.error(httpParams.url, {
			// 		data: data,
			// 		status: status,
			// 		headers: headers,
			// 		config: config
			// 	});
			// }

		});

		return deferred.promise(); 
	}
};