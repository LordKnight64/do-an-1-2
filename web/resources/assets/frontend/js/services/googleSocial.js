var googleUser = {};

var GoogleSocial = {

    handleLogin: function(element) {
        setTimeout(function() {

            // waiting for auth2 inited
            if (gapi == null || gapi.auth2 == null) {
                GoogleSocial.handleLogin(element);
                return;
            }

            var auth2 = gapi.auth2.getAuthInstance();
            auth2.attachClickHandler(element, {},
                function(data) {
                    googleUser = data;
                    console.log("username login google: ", googleUser.getBasicProfile().getName());
                    console.log("BasicProfile login google: ", googleUser.getBasicProfile());
                    console.log("AuthResponse login google: ", googleUser.getAuthResponse());
                    $('.modal').addClass('on_overlay_loading');
                    var userInfoCreate = {
                        'first_name': googleUser.getBasicProfile().ofa,
                        'last_name': googleUser.getBasicProfile().wea,
                        'email': googleUser.getBasicProfile().U3,
                        'oauth_provider': googleUser.getAuthResponse().access_token,
                        'oauth_provider_id': 'google',

                    }
                    serverServices.doPost('/registerUrlOther', userInfoCreate).then(
                        function(okResult) {
                            $('#check_user').val(okResult.data.userInfo);
                            if (okResult.data.password != undefined && okResult.data.password != null && okResult.data.password != "") {
                                swal("Login success! System sendding mail to you", "success").then(function() {
                                    var usermail = {
                                        'first_name': googleUser.getBasicProfile().ofa,
                                        'last_name': googleUser.getBasicProfile().wea,
                                        'email': googleUser.getBasicProfile().U3,
                                        'password': okResult.data.password
                                    }
                                    serverServices.doPost('/sendMailUrlOther', usermail);
                                    swal("System  had send mail to you!", "Please check your mail", "success").then(function() {
                                        GoogleSocial.setModelAfterLogin(okResult);
                                    });

                                });
                            } else {
                                swal("Login success!", "Account had exist in system", "success").then(function() {
                                    GoogleSocial.setModelAfterLogin(okResult);
                                });
                            }

                        },
                        function(errResult) {
                            swal("Sent data error!", "Request failed.<br> Please contact us again later.", "error");
                            console.log('errResult', errResult);
                        }
                    );
                },
                function(error) {
                    console.log("Google login error: ", error);
                });
        }, 1000);
    },

    logout: function() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function() {
            console.log('User signed out.');
        });
    },

    getDetail: function() {
        if (googleUser != null) {
            var result = "";

            //todo
            console.log("Name: ", googleUser.getBasicProfile().getName());
            console.log("Base profile: ", googleUser.getBasicProfile());
            console.log("auth response: ", googleUser.getAuthResponse());

            return result;
        }
        return null;
    },
    setModelAfterLogin: function(okResult) {
        $('.modal').removeClass('on_overlay_loading');
        $("#modal-user-login").modal('hide');
        document.getElementById("chatheadButton").style.display = "block";
        document.getElementById("userNameLogin").innerHTML = okResult.data.userInfo.first_name;
        $('#userIdLogin').val(okResult.data.userInfo.id);
        document.getElementById("accumulativePoint").innerHTML = okResult.data.userInfo.accumulative_point;
        serverServices.doPost('/getRestaurantAfterLogin').then(
            function(okResult2) {
                if (okResult2.data.return_cd === 0) {
                    $('#retaurantLoginLst').val(JSON.stringify(okResult2.data.restaurantLoginList));
                    modalUserAfterLoginUI.drawRestaurant();
                } else {
                    swal("Cannot connect to server", "Please login again", "error");
                }
            },
            function(errResult2) {
                console.log('errResult', errResult2);
                swal("Cannot connect to server", "Please login again", "error");
            }
        );
    }
};