$(document).ready(function() {
	$.validate({
		form: '#subscribe',
		modules: '',
		onError: function($form) {

		},
		onSuccess: function($form) {
			var param = {
				'email': $form.find('input[name="email_sub"]').val(),
			};
			serverServices.doPost('/subscribe', param).then(
				function(okResult) {
					if(okResult.data.return_cd === 0){
						swal("Thank you for subscribe!", "Subscribe success", "success");
					}else{
						swal("This email has already been subscribed!","" ,"error");
					}
				},
				function(errResult) {
					swal("Cannot subscribe !", okResult.data.message, "error");
					console.log('errResult', errResult);
				}
			);
			return false;
		},
		onModulesLoaded: function() {

		}
	});
	});
