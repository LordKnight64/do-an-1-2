var modalUserAfterLoginUI = {
    drawRestaurant: function() {
        var div = document.createElement('ul');
        div.className = 'dropdown-menu';
        var draw = '';
        var restaurantLoginList = null;
        draw = draw + '<li><a href="/profile">Edit Profile</a></li>';
        draw = draw + '<li><a href="/historyOrder">Histoy Order</a></li>';
        if (document.getElementById("retaurantLoginLst").value != null && document.getElementById("retaurantLoginLst").value != "") {
            restaurantLoginList = JSON.parse(document.getElementById("retaurantLoginLst").value);
        }
        if (restaurantLoginList != null && restaurantLoginList.length > 0) {
            restaurantLoginList.forEach(function(element) {
                draw = draw + '<li><a href="/order?restaurant_id=' + element.id + '">' + element.name + '</a></li>';
            }, this);
        }
        draw = draw + '<li><a href="/logout">Logout</a></li>';
        draw = draw + '';
        document.getElementById("drawRestaurantHtml").innerHTML = draw;
    }
}