$(document).ready(function() {
    modalUserAfterLoginUI.drawRestaurant();
});
$(".chathead").draggable({
    containment: '.overlay_loading',
    opacity: 0.95,
    scope: "chathead",
    scroll: true
}).on("dragstop", function(e) {
    var bodyWidth = $(document.body).width(),
        middle = bodyWidth / 2,
        $this = $(this),
        offset = 10;
    if (middle > e.pageX) {
        $this.animate({
            left: bodyWidth + 10 + offset - $this.width() - 40
        });
    } else {
        $this.animate({
            left: bodyWidth + 10 + offset - $this.width() - 40
        });
    }
});

$(".chathead").click(function() {
    var user = $('#check_user').val();
    console.log(user);
    if (user == null || user.length == 0 || user == '') {
        $("#modal-user-login").modal();
        document.getElementById("chatheadButton").style.display = "none";
    } else {
        document.getElementById("chatheadButton").style.display = "block";
    }
});
