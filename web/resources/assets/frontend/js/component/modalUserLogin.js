$(document).ready(function() {

    // Handle event click for button login google
    GoogleSocial.handleLogin(document.getElementById("btnGoogleSocial"));

    $.validate({
        form: '#user-login-form',
        modules: 'location, date, security, file',
        onError: function($form) {
            // alert('Validation of form failed!');
        },
        onSuccess: function($form) {
            var param = {
                'email': $form.find('input[name="email"]').val(),
                'password': $form.find('input[name="password"]').val(),
            };
            serverServices.doPost('/login', param).then(
                function(okResult) {

                    var data = okResult.data;
                    console.log('data', data);
                    if (!data.code) {
                        swal("Login success!", "you had login to system", "success").then(function() {
                            $('#login-name').text(data.first_name);
                            $('#login-user-id').val(data.id);
                            $('#login-user-id').text(data.id);
                            $("#modal-user-login").modal('hide');
                            document.getElementById("chatheadButton").style.display = "block";
                            var pathname = window.location.pathname;
                            if (pathname != null && pathname == '/reviewCheckout') {
                                var href = window.location.href;
                                window.location.href = href;
                            }
                            if (pathname != null && pathname == '/orderDetail') {
                                console.log(pathname);
                                location.reload();
                            }
                            serverServices.doPost('/getRestaurantAfterLogin').then(
                                function(okResult1) {
                                    if (okResult1.data.return_cd === 0) {
                                        $('#retaurantLoginLst').val(JSON.stringify(okResult1.data.restaurantLoginList));
                                        modalUserAfterLoginUI.drawRestaurant();
                                    } else {
                                        swal("Cannot connect to server", "Please login again", "error");
                                    }
                                },
                                function(errResult1) {
                                    console.log('errResult', errResult1);
                                    swal("Cannot connect to server", "Please login again", "error");

                                }
                            );
                            $('#check_user').val(data);
                            document.getElementById("userNameLogin").innerHTML = data.first_name;
                            $('#userIdLogin').val(data.id);
                            document.getElementById("accumulativePoint").innerHTML = data.accumulative_point;
                            //endpoint notification
                            //PushManagerServiceWorker();
                        });
                    } else if (data.code === 422 && data.code == 401) {
                        var errors = data.errors;
                        var msg = '';
                        for (var i in errors) {
                            // var elem = errors[i];
                            msg += errors[i];
                        }
                        alert(msg);
                    } else if (data.code == 401) {
                        swal("Login failed", "Incorrect email or password", "error");
                    }

                    // $("#modal-user-login").modal('hide');
                    // return true;
                },
                function(errResult) {
                    console.log('errResult', errResult);
                    swal("Cannot connect to server", "Please login again", "error");

                }
            );
            return false;
        },
        onModulesLoaded: function() {
            // $('#msg-error-login').css('display','none');
            // $('#country').suggestCountry();
        }
    });

    $('#modal-user-login').on('hidden.bs.modal', function(e) {
        $('#msg-error-login').css('display', 'none');
    });
});

function loginFacebook() {
    FacebookSocial.login();
    // var userInfoCreate = {
    //     'first_name': 'An',
    //     'last_name': 'Nguyen',
    //     'email': 'igrabfood@gmail.com',
    //     'oauth_provider': 'EAAZAZCH6WwsIEBAG9rf6y9h9M1ZCl0ZADsoXOneaRVLcp1dZAjBotoSvt2FqiagdFUGDmb8zLdj2Bx7XiDkOG6y8tfKmUXsOUUuGdtajsgFEikvL5O9o4mK6jRKdlAZB5ZA7SL1O28V49UE1ZCzaV7rXBpo86qzsOjUyK1vuGSoWA8P9GMHMqGYZB5AP3mQv2DTmDYuBtbPYRxgZDZD',
    //     'oauth_provider_id': 'facebook',

    // }
    // $('.modal').addClass('on_overlay_loading');
    // serverServices.doPost('/registerUrlOther', userInfoCreate).then(
    //     function(okResult) {
    //         $('.modal').removeClass('on_overlay_loading');
    //         $("#modal-user-login").modal('hide');
    //         $('#check_user').val(okResult.data.userInfo);
    //     },
    //     function(errResult) {
    //         swal("Sent data error!", "Request failed.<br> Please contact us again later.", "error");
    //         console.log('errResult', errResult);
    //     }
    // );

}