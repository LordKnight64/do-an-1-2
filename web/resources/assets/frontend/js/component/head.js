$(document).ready(function() {
    // Load detail order from shopping cart when loaded
    ShoppingCartUI.loadCookieForShoppingCart();
    if (document.getElementById("isSubscribed").value == null || document.getElementById("isSubscribed").value == "") {
        PushManagerServiceWorker();
    }
});

function logout() {
    serverServices.doPost('/logout', {}).then(
        function(okResult) {
            $('#header-notifications').css('display', 'none');
            $('#header-welcome-dropdown').css('display', 'none');
            $('#header-login').css('display', 'block');
        },
        function(errResult) {
            console.log('errResult', errResult);
        }
    );
}

function deleteShoppingCartItem(itemId, number) {
    swal({
        title: 'Are you sure?',
        text: "Are you delete this item in Shopping Cart?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete!'
    }).then(function() {
        ShoppingCart.deleteShoppingCartItem(itemId, number);
        var pathname = window.location.pathname;
        if (pathname != null && pathname == '/reviewCheckout') {
            // ShoppingCartUI.loadCookieForreviewCheckout();
            location.reload();
        }
        if (pathname != null && pathname == '/checkout') {
            ShoppingCartUI.loadCookieForcheckout();
        }
    });
}
var localStorage = window.localStorage;

function chooseLang(lang) {
    var langBefore = null;
    if (localStorage.getItem('langChoosen') != undefined || localStorage.getItem('langChoosen') != null) {
        langBefore = localStorage.getItem('langChoosen');
    }
    if (langBefore == null || langBefore != lang) {
        serverServices.doPost('/changeLanguage', { 'langChange': lang }).then(
            function(okResult) {


                localStorage.setItem('langChoosen', JSON.stringify(lang));
                location.reload();

            },
            function(errResult) {
                console.log('errResult', errResult);
            }
        );
    }

}