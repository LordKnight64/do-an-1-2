$(document).ready(function() {
	$.validate({
		form: '#demo',
		modules: '',
		onError: function($form) {

		},
		onSuccess: function($form) {
			var param = {
				'name': $form.find('input[name="name"]').val(),
				'email': $form.find('input[name="email"]').val(),
			};
			serverServices.doPost('/request_demo', param).then(
				function(okResult) {
					if(okResult.data.return_cd === 0){
						swal("Sent demo success!", "Request demo success", "success");
					}else{
						swal("Sent demo error!", okResult.data.message, "error");
					}
				},
				function(errResult) {
					swal("Sent demo error!", "Request demo error", "error");
					console.log('errResult', errResult);
				}
			);
			return false;
		},
		onModulesLoaded: function() {

		}
	});

	$('#modal-user-login').on('hidden.bs.modal', function (e) {
	 	$('#msg-error-login').css('display','none');
	});
});
