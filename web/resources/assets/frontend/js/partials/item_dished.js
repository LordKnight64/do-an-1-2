/**
 * [item_dished Description]
 * Function to use:
 * - setShoppingCartItemInItemDished(item, restaurant, urlRedirect): use to add order to shopping cart with restaurant
 *
 * @author Dang Nhat Anh
 */

/**
 * [setShoppingCartItemInItemDished Add order to shopping cart with restaurant]
 * @param {[type]} item        [description]
 * @param {[type]} restaurant  [description]
 * @param {[type]} urlRedirect [url if redirect]
 */
function setShoppingCartItemInItemDished(item, restaurant, urlRedirect) {

    if (restaurant == null) {
        var param = { 'id': item.restaurant_id };
        serverServices.doPost('/getRestaurantById', param).then(
            function(okResult) {
                if (okResult.data.return_cd === 0) {
                    restaurant = okResult.data.restaurant;
                    var restaurantCokie = null;
                    if (restaurant != null) {
                        restaurantCokie = {
                            "id": restaurant.id,
                            "name": restaurant.name,
                            "address": restaurant.address,
                            "promo_discount": restaurant.promo_discount,
                            "promote_code": restaurant.promote_code,
                            "delivery_charge": restaurant.delivery_charge,
                            "default_currency": restaurant.default_currency,
                            "unit_point": restaurant.unit_point,
                        }
                    }
                    doSetShoppingCartItemInItemDished(item, restaurantCokie, urlRedirect);
                } else {
                    swal("Get data restaurant error!", okResult.data.message, "error");
                }
            },
            function(errResult) {
                swal("Get data error!", "Request failed", "error");
            }
        );
    } else {
        doSetShoppingCartItemInItemDished(item, restaurant, urlRedirect);
    }
}

function doSetShoppingCartItemInItemDished(item, restaurant, urlRedirect) {
    var orderDetail = {};
    orderDetail['item_id'] = item.id;
    orderDetail['name'] = item.name;
    orderDetail['price'] = item.price;
    orderDetail['point'] = item.point;
    orderDetail['thumb'] = item.thumb;
    orderDetail['quantity'] = 1;
    orderDetail['notes'] = "";
    orderDetail['number'] = 0;

    var option_items = [];
    orderDetail['option_items'] = option_items;

    ShoppingCart.setShoppingCartItem(orderDetail, restaurant, urlRedirect);
    // loadCookieForShoppingCart();
    // swal("Add ShoppingCart success", "You add ShoppingCart success!!", "success");
}