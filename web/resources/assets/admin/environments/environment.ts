// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const ENVIRONMENT = {
  // THIS ARE TESTING CONFIG! DONT USE THAT IN PROD! CHANGE THAT!
  PRODUCTION: false,
  SILENT: false,
  LANG_MODE : 1,
  DOMAIN_API: 'http://api.mysite.local',
  SERVER_PORT: '8000'
};

// Hostinger
//  export const ENVIRONMENT = {
//    // THIS ARE TESTING CONFIG! DONT USE THAT IN PROD! CHANGE THAT!
//    PRODUCTION: true,
//    SILENT: false,
//    LANG_MODE : 1,
//    DOMAIN_API: 'http://rest-api.ifoodgo.vn',
//    SERVER_PORT: '80'
//  };
