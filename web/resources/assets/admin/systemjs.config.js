var isPublic = typeof window != "undefined";

(function(global) {
    // map tells the System loader where to look for things
    var map = {
        'app': 'admin/app', // 'dist',
        '@angular': (isPublic) ? 'admin/libs/@angular' : 'node_modules/@angular',
        'rxjs': (isPublic) ? 'admin/libs/rxjs' : 'node_modules/rxjs',
        'angular2-moment': (isPublic) ? 'admin/libs/angular2-moment' : 'node_modules/angular2-moment',
        'moment': (isPublic) ? 'admin/libs/moment/min' : 'node_modules/moment/min',
        //'ngx-bootstrap': (isPublic) ? 'admin/libs/ngx-bootstrap/modal' : 'node_modules/ngx-bootstrap/modal',
        'ngx-bootstrap': (isPublic) ? 'admin/libs/ngx-bootstrap/bundles' : 'node_modules/ngx-bootstrap/bundles',
        'angular2-toaster': (isPublic) ? 'admin/libs/angular2-toaster' : 'node_modules/angular2-toaster',
        'ng2-translate': (isPublic) ? 'admin/libs/ng2-translate/bundles' : 'node_modules/ng2-translate/bundles',
        // 'angular2-uuid':              (isPublic)? 'admin/libs/angular2-uuid' : 'node_modules/angular2-uuid',
        // 'ui-router-ng2':              (isPublic)? 'admin/libs/ui-router-ng2/_bundles' : 'node_modules/ui-router-ng2/_bundles',
        'angular2-jwt': (isPublic) ? 'admin/libs/angular2-jwt' : 'node_modules/angular2-jwt',
        // 'auth0-js':               (isPublic)? 'admin/libs/auth0-js/build' : 'node_modules/auth0-js/build',
        'object-assign': (isPublic) ? 'admin/libs/object-assign' : 'node_modules/object-assign',
        'sweetalert2': (isPublic) ? 'admin/libs/sweetalert2/dist' : 'node_modules/sweetalert2/dist',
        'ng2-select': (isPublic) ? 'admin/libs/ng2-select/bundles' : 'node_modules/ng2-select/bundles',
        // 'bootstrap': (isPublic) ? 'admin/libs/bootstrap/dist/js' : 'node_modules/bootstrap/dist/js',
        'crypto-js': (isPublic) ? 'admin/libs/crypto-js' : 'node_modules/crypto-js',
        'chart.js': (isPublic) ? 'admin/libs/chart.js/dist' : 'node_modules/chart.js/dist',
        'ng2-charts': (isPublic) ? 'admin/libs/ng2-charts/bundles' : 'node_modules/ng2-charts/bundles',

        'primeng': (isPublic) ? 'admin/libs/primeng' : 'node_modules/primeng',
        'environments': 'admin/environments'
    };
    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app': { main: 'main.js', defaultExtension: 'js' },
        'rxjs': { main: 'Rx.js', defaultExtension: 'js' },

        'moment': {
            main: './moment.min.js',
            defaultExtension: 'js'
        },
        'angular2-moment': {
            main: './index.js',
            defaultExtension: 'js'
        },
        'ngx-bootstrap': { main: './ngx-bootstrap.umd.min.js', defaultExtension: 'js' },
        /*'ngx-bootstrap-modal': {
            main: './index.js',
            defaultExtension: 'js'
        },*/
        'angular2-toaster': {
            main: './angular2-toaster.js',
            defaultExtension: 'js'
        },
        'ng2-translate': {
            main: './ng2-translate.umd.js',
            defaultExtension: 'js'
        },
        // 'angular2-uuid': {
        //     main: './index.js',
        //     defaultExtension: 'js'
        // },
        'environments': {
            main: './environment.js',
            defaultExtension: 'js'
        },
        // 'ui-router-ng2': {
        //     main: './ui-router-ng2.js',
        //     defaultExtension: 'js'
        // },
        'angular2-jwt': {
            main: './angular2-jwt.js',
            defaultExtension: 'js'
        },
        // 'auth0-js': {
        //     main: './auth0.min.js',
        //     defaultExtension: 'js'
        // },
        'object-assign': {
            main: './index.js',
            defaultExtension: 'js'
        },
        'sweetalert2': {
            main: '/sweetalert2.min.js',
            defaultExtension: 'js'
        },
        'ng2-select': {
            main: '/ng2-select.umd.min.js',
            defaultExtension: 'js'
        },
        // 'bootstrap': {
        //     main: '/bootstrap.min.js',
        //     defaultExtension: 'js'
        // },
        'crypto-js': {
            main: '/crypto-js.js',
            defaultExtension: 'js'
        },
        'chart.js': {
            main: '/Chart.bundle.min.js',
            defaultExtension: 'js'
        },
        'ng2-charts': {
            main: '/ng2-charts.umd.min.js',
            defaultExtension: 'js'
        },
        'primeng': {
            main: '/primeng.js',
            defaultExtension: 'js'
        },

    };
    var ngPackageNames = [
        'common',
        'compiler',
        'core',
        'forms',
        'http',
        'platform-browser',
        'platform-browser-dynamic',
        'router',
        'router-deprecated',
        'upgrade'
    ];
    // Individual files (~300 requests):
    function packIndex(pkgName) {
        packages['@angular/' + pkgName] = { main: 'index.js', defaultExtension: 'js' };
    }
    // Bundled (~40 requests):
    function packUmd(pkgName) {
        packages['@angular/' + pkgName] = { main: 'bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
    }
    // Most environments should use UMD; some (Karma) need the individual index files
    var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;
    // Add package entries for angular packages
    ngPackageNames.forEach(setPackageConfig);
    var config = {
        map: map,
        packages: packages
    };
    System.config(config);
})(this);