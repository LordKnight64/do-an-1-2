import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateGuard } from './services/guard.service';

// Components
import { HomeComponent } from './components/home/home.component';
// import { PageNumComponent } from './components/page-num/page-num.component';
import { ClientComponent } from './components/client/client.component';
import { LayoutsAuthComponent } from './components/layouts/auth/auth';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SystemErrorComponent } from './components/system-error/system-error.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OrderingRequestComponent } from './components/ordering-request/ordering-request.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { CreateCategoryComponent } from './components/create-category/create-category.component';
import { OptionFoodItemsListComponent } from './components/option-food-items-list/option-food-items-list.component';
import { CreateOptionFoodItemComponent } from './components/create-option-food-item/create-option-food-item.component';
import { OptionValueFoodItemsComponent } from './components/option-value-food-items/option-value-food-items.component';
import { CreateOptionValueFoodItemComponent } from './components/create-option-value-food-item/create-option-value-food-item.component';
import { ContentsComponent } from './components/contents/contents.component';
import { NewsListComponent } from './components/news-list/news-list.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { OrderSettingComponent } from './components/order-setting/order-setting.component';
import { TurnOverComponent } from './components/turn-over/turn-over.component';
import { FoodItemsListComponent } from './components/food-items-list/food-items-list.component';
import { CreateFoodItemComponent } from './components/create-food-item/create-food-item.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { TableListComponent } from './components/table-list/table-list.component';
import { TableBookingComponent } from './components/table-booking/table-booking.component';

import { SubscribeListComponent } from './components/subscribe-list/subscribe-list.component';
import { SendEmailMarketingComponent } from './components/send-email-marketing/send-email-marketing.component';
import { SearchItemsComponent } from './components/search-items/search-items.component';
import { EmailMarketingListComponent } from './components/email-marketing-list/email-marketing-list.component';
import { ReviewItemComponent } from './components/review-item/review-item.component';
import { ViewEmailMarketingComponent } from './components/view-email-marketing/view-email-marketing.component';
import { AreaListComponent } from './components/area-list/area-list.component';
import { CreateAreaComponent } from './components/create-area/create-area.component';
import { ReserveTableComponent} from './components/reserve-table/reserve-table.component';
import { CreateTableComponent } from './components/create-table/create-table.component';
import {TableBookingScheduleComponent} from './components/table-booking-schedule/table-booking-schedule.component';
import {OrderFoodComponent} from './components/order-food/order-food.component';
const routes: Routes = [
  // logged routes
  {
    canActivate: [CanActivateGuard],
    children: [
      {
        redirectTo: 'dashboard',
        path: '',
        pathMatch: 'full'
      },
      {
        canActivate: [CanActivateGuard],
        component: HomeComponent,
        path: 'home'
      },
      {
        canActivate: [CanActivateGuard],
        component: DashboardComponent,
        path: 'dashboard'
      },
      {
        canActivate: [CanActivateGuard],
        component: ClientComponent,
        path: 'client'
      },
      {
        canActivate: [CanActivateGuard],
        component: OrderingRequestComponent,
        path: 'ordering-detail/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: ProfileComponent,
        path: 'profile'
      },
      {
        canActivate: [CanActivateGuard],
        component: CategoryListComponent,
        path: 'category/list'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateCategoryComponent,
        path: 'category/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateCategoryComponent,
        path: 'category/create/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: OptionFoodItemsListComponent,
        path: 'option-food-items/list'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateOptionFoodItemComponent,
        path: 'option-food-item/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateOptionFoodItemComponent,
        path: 'option-food-item/create/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: OptionValueFoodItemsComponent,
        path: 'option-value-food-items/list'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateOptionValueFoodItemComponent,
        path: 'option-value-food-item/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateOptionValueFoodItemComponent,
        path: 'option-value-food-item/create/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: ContentsComponent,
        path: 'contents'
      },
      {
        canActivate: [CanActivateGuard],
        component: NewsListComponent,
        path: 'news'
      },
      {
        canActivate: [CanActivateGuard],
        component: NewsItemComponent,
        path: 'news/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: NewsItemComponent,
        path: 'news/edit/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: OrdersListComponent,
        path: 'order/list'
      },
      {
        canActivate: [CanActivateGuard],
        component: OrdersListComponent,
        path: 'my-orders'
      },
      
       {
        canActivate: [CanActivateGuard],
        component: OrderFoodComponent,
        path: 'order-food'
      },
      {
        canActivate: [CanActivateGuard],
        component: ContactListComponent,
        path: 'contact'
      },
      {
        canActivate: [CanActivateGuard],
        component: ContactItemComponent,
        path: 'contact/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: ContactItemComponent,
        path: 'contact/edit/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: OrderSettingComponent,
        path: 'order-setting'
      },
       {
        canActivate: [CanActivateGuard],
        component: TurnOverComponent,
        path: 'turn-over'
      },
      {
        canActivate: [CanActivateGuard],
        component: FoodItemsListComponent,
        path: 'food-items/list'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateFoodItemComponent,
        path: 'food-item/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateFoodItemComponent,
        path: 'food-item/edit/:id'
      },
       {
        canActivate: [CanActivateGuard],
        component: FeedbackComponent,
        path: 'feedback'
      },
        {
        canActivate: [CanActivateGuard],
        component: TableListComponent,
        path: 'table-list'
      },
        {
        canActivate: [CanActivateGuard],
        component: CreateTableComponent,
        path: 'table/create'
      },
        {
        canActivate: [CanActivateGuard],
        component: CreateTableComponent,
        path: 'table/create/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: TableBookingScheduleComponent,
        path: 'table-booking-schedule/view/:id'
      },

        {
        canActivate: [CanActivateGuard],
        component: TableBookingComponent,
        path: 'table-booking'
      },
        {
        canActivate: [CanActivateGuard],
        component: SubscribeListComponent,
        path: 'subscribe-list'
      },
        {
        canActivate: [CanActivateGuard],
        component: SendEmailMarketingComponent,
        path: 'send-email-marketing'
      },  {
        canActivate: [CanActivateGuard],
        component: SearchItemsComponent,
        path: 'search-items'
      },
        {
        canActivate: [CanActivateGuard],
        component: EmailMarketingListComponent,
        path: 'email-marketing-list'
      },
       {
        canActivate: [CanActivateGuard],
        component: ReviewItemComponent,
        path: 'review-item'
      },
      {
        canActivate: [CanActivateGuard],
        component: ViewEmailMarketingComponent,
        path: 'view-email-marketing-detail/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: AreaListComponent,
        path: 'area-list'

      },

      {
        canActivate: [CanActivateGuard],
        component: CreateAreaComponent,
        path: 'area/create'
      },
      {
        canActivate: [CanActivateGuard],
        component: CreateAreaComponent,
        path: 'area/create/:id'
      },
       {
        canActivate: [CanActivateGuard],
        component: ReserveTableComponent,
        path: 'reserve-table/create/:id'
      },
      {
       canActivate: [CanActivateGuard],
       component: ReserveTableComponent,
       path: 'reserve-table/create'
     },


    ],
    component: LayoutsAuthComponent,
    path: ''
  },
  // not logged routes
  //
  {
    component: LoginComponent,
    path: 'login'
  },
  {
    component: RegisterComponent,
    path: 'register'
  },
  {
    component: SystemErrorComponent,
    path: 'error'
  },
  {
    component: PageNotFoundComponent,
    path: 'error/404'
  },
  { path: '**', redirectTo: 'error/404' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
