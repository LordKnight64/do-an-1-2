import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { NewsService } from 'app/services/news.service';

@Component({
  selector: 'news-list',
  templateUrl: Utils.getView('app/components/news-list/news-list.component.html'),
  styleUrls: [Utils.getView('app/components/news-list/news-list.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class NewsListComponent implements OnInit {

  private restId;
  private userId;
  private newsList: any;
  private data: any;
  private currentPage: number = 1;
  private totalItems: number;
  private itemsPerPage: number = 10;
  private maxSize: number = 10;

  constructor(
    private newsServ: NewsService,
    private userServ: UserService,
    private storageServ: StorageService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {
    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }

    this.doSearch();
  }

  private doSearch() {
    var params = {
      'rest_id': this.restId
    };
    this.newsServ.getNewsList(params).then(response => {
      this.newsList = response.news;
      this.totalItems = this.newsList.length;
      for(var i = 0;i<this.totalItems;i++)
      {
        this.newsList[i].description = this.ConvertBR(this.newsList[i].description);
      }
      this.pageChanged({ page: this.currentPage, itemsPerPage: this.itemsPerPage });
    }, error => {
      console.log(error);
    });
  }

  public pageChanged(event: any): void {
    let start = (event.page - 1) * event.itemsPerPage;
    let end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
    this.data = this.newsList.slice(start, end);
    
  }

  private doDeleteNews(obj) {
    obj.is_delete = 1;
    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'news': obj
    };
    this.newsServ.updateNews(params).then(response => {
      this.newsServ.deleteFile({ 'thumb': response.news.thumb }).then(response => {
        this.notifyServ.success('News has been deleted');
        this.doSearch();
      }, error => {
        console.log(error);
      });
    }, error => {
      console.log(error);
    });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }

  

  private ConvertBR(input)
  {
    console.log("convert");
    var output = "";
		for (var i = 0; i < input.length; i++) {
            
            if(i+1 < input.length && input.charCodeAt(i)==10)
            {
                output += "<br>";
            }
            else
            {
                output += input.charAt(i);
            }
		}
		
		return output;
  }
}
