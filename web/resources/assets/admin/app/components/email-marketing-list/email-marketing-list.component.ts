import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { EmailService } from 'app/services/email.service';
import { Router } from '@angular/router';

 @Component({
  selector: 'table-list',
  templateUrl: Utils.getView('app/components/email-marketing-list/email-marketing-list.component.html'),
  styleUrls: [Utils.getView('app/components/email-marketing-list/email-marketing-list.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class EmailMarketingListComponent implements OnInit {
   private restId;
  private userId;
  private newsList: any;
  private data: any;
  private currentPage: number = 1;
  private totalItems: number = 0;
  private itemsPerPage: number = 10;
  private maxSize: number = 10;

  constructor(
    private emailServ: EmailService,
    private userServ: UserService,
     private router: Router,
    private storageServ: StorageService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

     let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }
    this.doSearch();
  }

  private doSearch() {
    console.log(' this.restId ',  this.restId);
   
    var params = {
      'rest_id' : this.restId
    };
    this.emailServ.getEmailMarketingList(params).then(response => {
      this.newsList = response.emails;
      this.totalItems = this.newsList.length;
      if(this.totalItems > 0)
         this.pageChanged({ page: this.currentPage, itemsPerPage: this.itemsPerPage });
    }, error => {
      console.log(error);
    });
  }

  public pageChanged(event: any): void {
    let start = (event.page - 1) * event.itemsPerPage;
    let end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
    this.data = this.newsList.slice(start, end);
  }

  private doDeleteSearch(obj) {
   
    let id = obj.id;
    var params = {
      'id': id
    };
    var me = this;
    this.emailServ.deleteEmailMarketingList(params).then(response => {
      if(response.return_cd == 0) {
        me.notifyServ.success('Search Items has been deleted');
        me.doSearch();
      }
    }, error => {
      console.log(error);
    });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }

  private viewEmailDetail(item) {
       this.router.navigateByUrl('/view-email-marketing-detail/'+ item.id);
  }

}
