import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { OptionFoodItemService } from 'app/services/optionFoodItem.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
@Component({
  selector: 'create-option-food-item',
  templateUrl: Utils.getView('app/components/create-option-food-item/create-option-food-item.component.html'),
  styleUrls: [Utils.getView('app/components/create-option-food-item/create-option-food-item.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class CreateOptionFoodItemComponent implements OnInit {

  private mymodel: any;
	private createOptionItemForm: FormGroup;
	private name: AbstractControl;
  private userId : String;
  private restId : String;
  private id : string;
  private optionFoodItem: any;
  private sub: any;
  private error: any;

  constructor(private fb: FormBuilder,
  private OptionFoodItemService: OptionFoodItemService, 
  private StorageService: StorageService, 
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private route: ActivatedRoute) { 
  }

  ngOnInit() {
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
  	this.buildForm();
    this.sub = this.route.params.subscribe(params => {
    this.id = params['id']; });
    if(this.id!=null){
        this.optionFoodItem = this.StorageService.getScope();
        if(this.optionFoodItem == false){
          this.fetchAllOptionFoodItems(this.restId,this.id);
        }else{
            this.createOptionItemForm.controls['name'].setValue(this.optionFoodItem.name);
            this.name = this.createOptionItemForm.controls['name'];
        }
      }
  }
  private fetchAllOptionFoodItems(restId,id) {
  		this.OptionFoodItemService.getOptionFoodItems(restId,id).then(
                     response  => { 
                     	console.log(response.options); 
                        if(response.options!=null && response.options.length>0){
                       	  this.optionFoodItem = response.options[0];
                          this.createOptionItemForm.controls['name'].setValue(this.optionFoodItem.name);
                          this.name = this.createOptionItemForm.controls['name'];
                        }else{
                         this.notif.error('Food item had delete');
                         this.router.navigateByUrl('option-food-items/list');
                       }
                     },
  					 error => {console.log(error)
  					});
  	}
  private buildForm(): void {
      this.createOptionItemForm = this.fb.group({
        'name': ['', [
            Validators.required,
            Validators.maxLength(256),
          ]
        ]
      });
         this.name = this.createOptionItemForm.controls['name'];
      this.createOptionItemForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
  
      this.onValueChanged(); // (re)set validation messages now
    }
   private onValueChanged(data?: any) {
    if (!this.createOptionItemForm) { return; }
    const form = this.createOptionItemForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }
  formErrors = {
      'name': []
    };

  validationMessages = {
    'name': {
      'required':      'The name is required.',
      'maxlength':     'The name must be at less 256 characters long.'
    }
  };

  optionItemCreate(){

      let id = '-1';
      if(this.id!=null){
        id  = this.id;
      }
      this.onValueChanged();
      for (const field in this.formErrors) {
      // clear previous error message (if any)
       if(this.formErrors[field].length >0){
         return;
       }
    }
      this.optionFoodItem = {
                'rest_id': this.restId,
                'user_id':this.userId,
                'option':{
                  'id':id,
                  'name':this.createOptionItemForm.value.name,
                  'is_delete':'0'
                }};
      	this.OptionFoodItemService.createOptionFoodItems(this.optionFoodItem).then(
                    response  => this.processResult(response),
                         error =>  this.failedCreate.bind(error));
  };

  private processResult(response) {
  
      if (response.message == undefined || response.message == 'OK') {
          this.notif.success('New foot item has been added');
          this.router.navigateByUrl('option-food-items/list');
      } else {
        this.error = response.errors;
        if (response.code == 422) {
          if (this.error.name) {
            this.formErrors['name'] = this.error.name;
          }
        } else {
          swal(
            'Create Fail!',
            this.error[0],
            'error'
          ).catch(swal.noop);
        }
      }
    }
    private failedCreate (res) {
      if (res.status == 401) {
        // this.loginfailed = true
      } else {
        if (res.data.errors.message[0] == 'Email Unverified') {
          // this.unverified = true
        } else {
          // other kinds of error returned from server
          for (var error in res.data.errors) {
            // this.loginfailederror += res.data.errors[error] + ' '
          }
        }
      }
    }
}
