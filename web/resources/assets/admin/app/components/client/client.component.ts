import { Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { Client } from 'app/models/client';
// import { ClientDAL } from '../../dal/client.dal';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import Utils from 'app/utils/utils';
import { NotificationService } from 'app/services/notification.service';
import { ModalDirective } from 'ngx-bootstrap';
import { routerFade } from '../../router.animations';
// import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';
// import { ModalDirective } from 'ng2-bootstrap/ng2-bootstrap';
import objectAssign = require('object-assign');
import { CarouselConfig } from 'ngx-bootstrap';
import { AuthService } from 'app/services/auth.service';
@Component({
  // providers: [ClientDAL],
  selector: 'app-client',
  styleUrls: [Utils.getView('app/components/client/client.component.css')],
  animations: [routerFade()],
  host: {'[@routerFade]': ''},
  //  styles: [`
  // :host >>> .alert-md-color {
  //   background-color: #009688;
  //   border-color: #00695C;
  //   color: #fff;
  // }
  // `],
  templateUrl: Utils.getView('app/components/client/client.component.html'),
  // encapsulation: ViewEncapsulation.None
  providers: [{provide: CarouselConfig, useValue: {interval: 1500, noPause: true}}]
})
export class ClientComponent implements OnInit, OnDestroy {

  // private clients: FirebaseListObservable<Array<Client>>;

  // constructor(private dal: ClientDAL, private breadServ: BreadcrumbService) {
  //   // TODO
  // }

  private singleModel: string = '1';
  public checkModel: any = { left: false, middle: true, right: false };
  public radioModel: string = 'Middle';
  private clients: Array<Client>;
  public status: any = {
    isFirstOpen: true,
    isFirstDisabled: false
  };
  // public items: string[] = ['Item 1', 'Item 2', 'Item 3'];
  // public groups: any[] = [
  //   {
  //     title: 'Dynamic Group Header - 1',
  //     content: 'Dynamic Group Body - 1'
  //   },
  //   {
  //     title: 'Dynamic Group Header - 2',
  //     content: 'Dynamic Group Body - 2'
  //   }
  // ];

  public isFirstOpen: boolean = true;
  public customClass: string = 'customClass';

  public alerts: any = [
    {
      type: 'success',
      msg: `You successfully read this important alert message.`
    },
    {
      type: 'info',
      msg: `This alert needs your attention, but it's not super important.`
    },
    {
      type: 'danger',
      msg: `Better check yourself, you're not looking too good.`
    }
  ];
  
  public isCollapsed:boolean = false;

  private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;

  public items:Array<string> = ['Amsterdam', 'Antwerp', 'Athens', 'Barcelona',
    'Berlin', 'Birmingham', 'Bradford', 'Bremen', 'Brussels', 'Bucharest',
    'Budapest', 'Cologne', 'Copenhagen', 'Dortmund', 'Dresden', 'Dublin',
    'Düsseldorf', 'Essen', 'Frankfurt', 'Genoa', 'Glasgow', 'Gothenburg',
    'Hamburg', 'Hannover', 'Helsinki', 'Kraków', 'Leeds', 'Leipzig', 'Lisbon',
    'London', 'Madrid', 'Manchester', 'Marseille', 'Milan', 'Munich', 'Málaga',
    'Naples', 'Palermo', 'Paris', 'Poznań', 'Prague', 'Riga', 'Rome',
    'Rotterdam', 'Seville', 'Sheffield', 'Sofia', 'Stockholm', 'Stuttgart',
    'The Hague', 'Turin', 'Valencia', 'Vienna', 'Vilnius', 'Warsaw', 'Wrocław',
    'Zagreb', 'Zaragoza', 'Łódź'];

  @ViewChild('childModal') public childModal: ModalDirective;
  constructor(
    private breadServ: BreadcrumbService,
    private notif: NotificationService,
     private authServ: AuthService
    // private childModal:ModalDirective
  ) {
   
  }

  public ngOnInit() {
    // this.clients = this.dal.readAll();
    this.authServ.getToken().then(
       response => this.processResult(response),
       error => this.handleError(error)
    );
  }

  public ngOnDestroy() {
    // this.breadServ.clear();
    this.clients = null;
  }

  private save = (client: Client): void => {
    // this.dal.update(client.clientId, new Client(client.name, client.clientId, client.address));
  }

  private delete = (client: Client): void => {
    // this.dal.delete(client);
  }

  private add = (): void => {
    this.notif.success('New client has been added')
    // this.dal.create(new Client());
  }

  public showChildModal(): void {
    this.childModal.show();
  }

  // public addItem(): void {
  //   this.items.push(`Items ${this.items.length + 1}`);
  // }

  public reset(): void {
    // console.log('-----object-----', Object.);
    // console.log('-----objectAssign-----', objectAssign());
    this.alerts = this.alerts.map((alert: any) => objectAssign({}, alert));
  }

  public collapsed(event:any):void {
    console.log('collapsed', event);
  }
 
  public expanded(event:any):void {
    console.log('expanded', event);
  }

  private processResult(res){
    this.clients = [new Client()];
    this.breadServ.set({
      header:"Client",
      description: 'This is our Client page',
      display: true,
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
          title: 'Dashboard',
          state: 'app'
        },
        {
          icon: 'clock-o',
          link: ['/client'],
          title: 'Client',
          state: 'app.client'
        }
      ]
    });
  }

  private handleError(error){
    console.log(error);
  }

  
  private get disabledV():string {
    return this._disabledV;
  }
 
  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }
 
  public selected(value:any):void {
    console.log('Selected value is: ', value);
  }
 
  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }
 
  public typed(value:any):void {
    console.log('New search input: ', value);
  }
 
  public refreshValue(value:any):void {
    this.value = value;
  }

}
