import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from 'app/models/user';
import { UserService } from 'app/services/user.service';
import { LoggerService } from 'app/services/logger.service';

import { ToasterService, ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { AdminLTETranslateService } from 'app/services/translate.service';
import Utils from 'app/utils/utils';

@Component({
  selector: 'app-layouts-auth',
  // templateUrl: './auth.html'
  styleUrls: [Utils.getView('app/components/layouts/auth/auth.component.css')],
  templateUrl: Utils.getView('app/components/layouts/auth/auth.html'),
  encapsulation: ViewEncapsulation.None
})
export class LayoutsAuthComponent implements OnInit {
  private toastrConfig: ToasterConfig;
  private logger: LoggerService;
  private mylinks: Array<any> = [];
  private isGetUser: boolean = true;

  constructor(
    private userServ: UserService,
    private toastr: ToasterService,
    private translate: AdminLTETranslateService
  ) {
    this.toastrConfig = new ToasterConfig({
      newestOnTop: true,
      showCloseButton: true,
      tapToDismiss: false
    });
    // this.translate = translate.getTranslate();
    // this.logger = new LoggerService( this.translate );

    this.userServ.currentUser.subscribe((user) => {
      if (!user.email) {
        console.log('user', user);
        this.isGetUser = false;
      }
    });
  }

  public ngOnInit() {

    // this.userServ.currentUser.subscribe((user) => {
    //     if(!user.email){
    //       console.log('user', user);
    //        this.userServ.getAuthenticatedUser();
    //     }
    // });
    $(function () {
    
     /*$(document).on('click', '.sub-item', function (e) { 
       
          let elems: HTMLElement = document.getElementsByClassName('treeview-menu');
         
          var i;
          for (i = 0; i < elems.length; i++) {
              elems[i].style.display = "none";
          }
          
      });

      $(document).on('click', '.app-content', function (e) { 
         let elems: HTMLElement = document.getElementsByClassName('treeview-menu');
         
          var i;
          for (i = 0; i < elems.length; i++) {
              elems[i].style.display = "none";
          }
         
      });*/

      // nav
      $(document).on('click', '[ui-nav] a', function (e) {
      
        var $this = $(e.target), $active;
        $this.is('a') || ($this = $this.closest('a'));

        $active = $this.parent().siblings(".active");
        $active && $active.toggleClass('active').find('> ul:visible').slideUp(200);

        ($this.parent().hasClass('active') && $this.next().slideUp(200)) || $this.next().slideDown(200);
        $this.parent().toggleClass('active');

        $this.next().is('ul') && e.preventDefault();
      });
      $(document).on('click', '[ui-toggle-class]', function (e) {
        e.preventDefault();
        var $this = $(e.target);
        $this.attr('ui-toggle-class') || ($this = $this.closest('[ui-toggle-class]'));

        var classes = $this.attr('ui-toggle-class').split(','),
          targets = ($this.attr('target') && $this.attr('target').split(',')) || Array($this),
          key = 0;
        $.each(classes, function (index, value) {
          var target = targets[(targets.length && key)];
          $(target).toggleClass(classes[index]);
          key++;
        });
        $this.toggleClass('active');

      });
    });

    if (this.isGetUser) {
      console.log('user');
      this.userServ.getAuthenticatedUser();
      this.isGetUser = false;
    }
    //  sedding the resize event, for AdminLTE to place the height
    let ie = this.detectIE();
    if (!ie) {
      window.dispatchEvent(new Event('resize'));
    } else {
      // solution for IE from @hakonamatata
      let event = document.createEvent('Event');
      event.initEvent('resize', false, true);
      window.dispatchEvent(event);
    }

    // define here your own links menu structure
    this.mylinks = [
      {
        'title': 'Dashboard',
        'icon': 'fa-home',
        'link': ['/dashboard'],
        'state': 'dashboard'
      },
      {
        'title': 'Category',
        'icon': 'fa-cube',
        'link': ['/category/list'],
        'state': 'category-list'
      },
      {
        'title': 'Food Item',
        'icon': 'fa-cutlery',
        'link': ['/food-items/list'],
        'state': 'food-items-list'
      },
      {
        'title': 'Option Food Item',
        'icon': 'fa-sitemap',
        'link': ['/option-food-items/list'],
        'state': 'option-food-items-list'
      },
      {
        'title': 'Option Value Item',
        'icon': 'fa-plus-circle  ',
        'link': ['/option-value-food-items/list'],
        'state': 'option-food-items-list'
      },
      {
        'title': 'Table',
        'icon': 'fa-table',
        'link': ['/table'],
        'state': 'table',
         'class': 'table',
        'sublinks': [
        {
            'title': 'Area',
            'link': ['/area-list'],
            'icon': 'fa-list-alt',
            'external': false,
            'target' : '_blank'
           
        },
        {
            'title': 'Table List',
            'link': ['/table-list'],
            'icon': 'fa-list-alt',
            'external': false,
            'target' : '_blank'
           
          },
          {
            'title': 'Reservation',
            'link': ['/table-booking'],
            'icon': 'fa-calendar',
            'external': false,
             'target' : '_blank'
           
          }
        ]
      },
      {
        'title': 'Contacts',
        'icon': 'fa-address-book',
        'link': ['/contact'],
        'state': 'contact-list'
      },
      {
        'title': 'News',
        'icon': 'fa-newspaper-o',
        'link': ['/news'],
        'state': 'news-list'
      },
      {
        'title': 'Order Setting',
        'icon': 'fa-cogs',
        'link': ['/order-setting'],
        'state': 'order-setting'
      },
       {
        'title': 'Marketing',
        'icon': 'fa-envelope',
        'state': 'marketing',
        'class': 'marketing',
        'sublinks': [
        {
            'title': 'Email Marketing',
            'link': ['/email-marketing-list'],
            'icon': 'fa-envelope',
            'external': false,
             'target' : '_blank'
           
          },
          {
            'title': 'Subscribe',
            'link': ['/subscribe-list'],
            'icon': 'fa-envelope-open',
            'external': false,
             'target' : '_blank'
           
          },
           {
            'title': 'Search',
            'link': ['/search-items'],
            'icon': 'fa-search',
            'external': false,
             'target' : '_blank'
           
          },
            {
            'title': 'Reivews',
            'link': ['/review-item'],
            'icon': 'fa-star',
            'external': false,
             'target' : '_blank'
           
          }
        ]
      },
      {
        'title': 'Contents',
        'icon': 'fa-file-text-o',
        'link': ['/contents'],
        'state': 'contents'
      },
      {
        'title': 'Feedback',
        'icon': 'fa-comments',
        'link': ['/feedback'],
        'state': 'feedback'
      }
    ];
  }

  protected detectIE(): any {
    let ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …
    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
    // IE 12 / Spartan
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
    // Edge (IE 12+)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)
    // Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    let msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    let trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      let rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    let edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }

}
