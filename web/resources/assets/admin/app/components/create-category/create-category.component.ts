import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { CategoryService } from 'app/services/category.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
@Component({
  selector: 'create-category',
  templateUrl: Utils.getView('app/components/create-category/create-category.component.html'),
  styleUrls: [Utils.getView('app/components/create-category/create-category.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''
}
})
export class CreateCategoryComponent implements OnInit {
  
	private id: string;
  private sub: any;
	private mymodel: any;
	private createCategoryForm: FormGroup;
	private name: AbstractControl;
  private nameRadio: AbstractControl;
  private description: AbstractControl;
  private restId : String;
  private isCheckRadio : string;
  private categoryItem : any;
  constructor(private fb: FormBuilder,
  private categoryService: CategoryService, 
  private StorageService: StorageService, 
  private authServ: AuthService,
  private notif: NotificationService,
   private router: Router,private route: ActivatedRoute) { 
  }

  ngOnInit() {
     let userInfo = JSON.parse(localStorage.getItem('user_key'));
     this.restId = userInfo.restaurants[0].id;
     this.userId = userInfo.user_info.id;
  	 this.buildForm();
    	this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; 
      this.isCheckRadio = '1';
      this.createCategoryForm.controls['nameRadio'].setValue(1);
      if(this.id!=null){
        this.categoryItem = this.StorageService.getScope();
        if(this.categoryItem == false){
          this.fetchAllCategories(this.restId,this.id);       
        }else{
            this.createCategoryForm.controls['name'].setValue(this.categoryItem.name);
            this.createCategoryForm.controls['description'].setValue(this.categoryItem.description);
            this.createCategoryForm.controls['nameRadio'].setValue(this.categoryItem.is_active);
            this.name = this.createCategoryForm.controls['name'];
            this.nameRadio = this.createCategoryForm.controls['nameRadio'];
            this.description = this.createCategoryForm.controls['description'];
            this.isCheckRadio = this.categoryItem.is_active;
            this.imageSrc = this.categoryItem.thumb;   
            if( this.imageSrc==null){
               this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
            }
        }
      }
    });
  }
  checkFlg(value){
      if(this.isCheckRadio==undefined || this.isCheckRadio==null){
        this.isCheckRadio = '1';
      }
      if(value==this.isCheckRadio){
          return true;
      }
  }
  changenameRadio(value){
    this.isCheckRadio =value;
  }
  private fetchAllCategories(restId,id) {
  		this.categoryService.getCategories(restId,id).then(
                     response  => { 
                       if(response.categories!=null && response.categories.length>0){
                       	this.categoryItem = response.categories[0];
                       	this.createCategoryForm.controls['name'].setValue(this.categoryItem.name);
                        this.createCategoryForm.controls['description'].setValue(this.categoryItem.description);
                         this.createCategoryForm.controls['nameRadio'].setValue(this.categoryItem.is_active);
                        this.name = this.createCategoryForm.controls['name'];
                        this.nameRadio = this.createCategoryForm.controls['nameRadio'];
                        this.description = this.createCategoryForm.controls['description'];
                        this.isCheckRadio = this.categoryItem.is_active;
                        this.imageSrc = this.categoryItem.thumb;   
                        if( this.imageSrc==null){
                           this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                        }
                       }else{
                         this.notif.error('Category had delete');
                         this.router.navigateByUrl('category/list');
                       }
                     },
  					 error => {console.log(error)
  					});
  	}
private buildForm(): void {
    this.createCategoryForm = this.fb.group({
      'name': ['', [
          Validators.required,
          Validators.maxLength(256),
        ]

      ],'nameRadio':[],
      'description':[]
    });
       this.name = this.createCategoryForm.controls['name'];
       this.nameRadio = this.createCategoryForm.controls['nameRadio'];
       this.description = this.createCategoryForm.controls['description'];
    this.createCategoryForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }
   private onValueChanged(data?: any) {
    if (!this.createCategoryForm) { return; }
    const form = this.createCategoryForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }
formErrors = {
    'name': [],
    'description':[]
  };

  validationMessages = {
    'name': {
      'required':      'The name is required.',
      'maxlength':     'The name must be at less 256 characters long.'
    },
    'description': {
      'maxlength':     'The name must be at less 512 characters long.'
    }
  };

  /*upload file*/
	private sactiveColor: string = 'green';
    private baseColor: string = '#ccc';
    private overlayColor: string = 'rgba(255,255,255,0.5)';
    
    private dragging: boolean = false;
    private loaded: boolean = false;
    private imageLoaded: boolean = false;
    private imageSrc: string = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
    private flgImageChoose:boolean = false;
    private iconColor: string = '';
    private  borderColor: string = '';
    private  activeColor: string = '';
    private hiddenImage:boolean=false;
    private categories : any;
    private categoryObject : any;
    private userId : String;
    private fileChoose : File;
    private error: any;
    handleDragEnter() {
        this.dragging = true;
    }
    
    handleDragLeave() {
        this.dragging = false;
    }
    
    handleDrop(e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    }
    
    handleImageLoad() {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if(this.imageLoaded = true){
			this.hiddenImage = true;
		}
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        this.loaded = false;

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose= true;
    }
    
    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }
    
    _setActive() {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    }
    
    _setInactive() {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    }
createCategory(){
  let id  = '-1';
  if(this.id!=null){
    id  = this.id;
  }
  this.onValueChanged();
  for (const field in this.formErrors) {
      // clear previous error message (if any)
       if(this.formErrors[field].length >0){
         return;
       }
    }
  this.categoryObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'category':{
              'id':id,
              'name':this.createCategoryForm.value.name,
              'thumb':'',
              'description':this.createCategoryForm.value.description,
              'is_active':this.isCheckRadio,
              'is_delete':'0'
            }};
  	this.categoryService.createCategory(this.categoryObject).then(
            //          response  => { 
            //          	this.categories = response;
            //          	console.log(response, this.categories); 
            //          },
  					//  error => {console.log(error)
                response  => this.processResult(response),
                     error =>  this.failedCreate.bind(error));
  					// });

}
private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
     let imagePost = '';
     if( this.flgImageChoose == true){
       imagePost = this.imageSrc;
     }
      this.categories = response;  
      let categoryUpload = {
         'id':this.categories.category_id,
         'thumb':imagePost
      }
	    this.categoryService.categoryUploadFile(categoryUpload).then(
                     response  => { 
                     	this.notif.success('New Category has been added');
                      this.router.navigateByUrl('category/list');
                     },
  					 error => {console.log(error)
            
  					});
    } else {
      this.error = response.errors;
      if (response.code == 422) {
        if (this.error.name) {
          this.formErrors['name'] = this.error.name;
        }
      } else {
        swal(
          'Create Fail!',
          this.error[0],
          'error'
        ).catch(swal.noop);
      }
    }
  }
  private failedCreate (res) {
    if (res.status == 401) {
      // this.loginfailed = true
    } else {
      if (res.data.errors.message[0] == 'Email Unverified') {
        // this.unverified = true
      } else {
        // other kinds of error returned from server
        for (var error in res.data.errors) {
          // this.loginfailederror += res.data.errors[error] + ' '
        }
      }
    }
  }
}
