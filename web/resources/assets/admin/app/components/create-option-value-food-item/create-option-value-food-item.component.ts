import { INT_TYPE } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit,ViewChild,ViewEncapsulation   } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import {SelectModule,SelectComponent} from 'ng2-select';
import { OptionValueFoodItemService } from 'app/services/optionValueFoodItem.service';
import { OptionFoodItemService } from 'app/services/optionFoodItem.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
@Component({
  selector: 'create-option-value-food-item',
  templateUrl: Utils.getView('app/components/create-option-value-food-item/create-option-value-food-item.component.html'),
  styleUrls: [Utils.getView('app/components/create-option-value-food-item/create-option-value-food-item.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''},
})
export class CreateOptionValueFoodItemComponent implements OnInit {
  @ViewChild('SelectId') public select: SelectComponent;
  private mymodel: any;
	private createOptionValueItemForm: FormGroup;
	private name: AbstractControl;
	private optionName: AbstractControl;
  private isHidden: Boolean=false;
  private flgItemSelect: any;
  private userId : String;
  private restId : String;
  private id : string;
  private optionFoodItem: any;
  private optionFoodItemId: any;
  private optionFoodItems : any;
  private optionFoodValueItem : any;
  private sub: any;
  private error: any;
  private optionValueFoodItem : any;
  private items:Array<any> = [];
  private valueChoose: any;
  constructor(private fb: FormBuilder,
  private OptionValueFoodItemService: OptionValueFoodItemService,
  private OptionFoodItemService: OptionFoodItemService, 
  private StorageService: StorageService, 
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private route: ActivatedRoute) { 
  }

  ngOnInit() {
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
  	this.buildForm();
    this.sub = this.route.params.subscribe(params => {
    this.id = params['id']; });
    if(this.id!=undefined && this.id!=null){
        this.optionFoodItem = this.StorageService.getScope();
        if(this.optionFoodItem == false){
          this.fetchAllOptionValueFoodItems(this.restId,this.id);

        }else{
            this.createOptionValueItemForm.controls['name'].setValue(this.optionFoodItem.name);
            this.name = this.createOptionValueItemForm.controls['name'];
            this.optionFoodValueItem = this.StorageService.getScope();
            this.optionFoodItemId = this.optionFoodValueItem.option_id;
            this.fetchAllOptionFoodItems(this.restId);
        }
      }else{
             this.optionFoodItemId = this.StorageService.getScope();
             this.fetchAllOptionFoodItems(this.restId);
      }
  }
  private fetchAllOptionValueFoodItems(restId,id) {
  		this.OptionValueFoodItemService.getOptionValueFoodItems(restId,id).then(
                     response  => { 
                     	console.log(response.options); 
                       if(response.options!=null && response.options.length>0){
                       	  this.optionValueFoodItem = response.options[0];
                          this.createOptionValueItemForm.controls['name'].setValue(this.optionValueFoodItem.name);
                          this.createOptionValueItemForm.controls['optionName'].setValue(this.optionValueFoodItem.option_id);
                          this.optionName = this.createOptionValueItemForm.controls['optionName'];
                          this.name = this.createOptionValueItemForm.controls['name'];
                          this.optionFoodItemId = this.optionValueFoodItem.option_id;
                          this.fetchAllOptionFoodItems(this.restId);
                        }else{
                         this.notif.error('Food value item had delete');
                         this.router.navigateByUrl('option-value-food-items/list');
                       }
                     },
  					 error => {console.log(error)
  					});
  	}
  private fetchAllOptionFoodItems(restId) {
    this.flgItemSelect = 0;
  		this.OptionFoodItemService.getOptionFoodItems(restId,null).then(
                     response  => { 
                     	this.optionFoodItems = response.options;
                        this.items.push({
                                id: 0,
                                text: `choose value`
                              });
                      for (var i = 0; i <  this.optionFoodItems.length; i++) {
                              this.items.push({
                                id: this.optionFoodItems[i].id,
                                text: `${this.optionFoodItems[i].name}`
                              });
                              if(this.optionFoodItemId!=undefined && this.optionFoodItems[i].id==this.optionFoodItemId){
                                  this.flgItemSelect= i + 1;
                                  this.createOptionValueItemForm.controls['optionName'].setValue(this.optionFoodItems[i].id);
                                  this.optionName = this.createOptionValueItemForm.controls['optionName'];
                              }
                         }
                       this.select.items = this.items;
                       if(this.flgItemSelect != undefined && this.flgItemSelect != null){
                          this.value = this.items[this.flgItemSelect];
                          this.valueChoose = this.value.id;
                       }
                      if(this.optionFoodItemId==undefined || this.optionFoodItemId==false){
                        this.value = this.items[0];
                        this.createOptionValueItemForm.controls['optionName'].setValue(null);
                        this.optionName = this.createOptionValueItemForm.controls['optionName'];
                      }
                     	console.log(response.options); 
                     },
  					 error => {console.log(error)
  					});
  	}
private buildForm(): void {
    this.createOptionValueItemForm = this.fb.group({
    	'name': ['', [
          Validators.required,
          Validators.maxLength(256),
        ]
      ],
      'optionName': ['', [
          Validators.required,
        ]
      ]
    });
   	this.name = this.createOptionValueItemForm.controls['name'];
    this.optionName = this.createOptionValueItemForm.controls['optionName'];
    this.createOptionValueItemForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }
   private onValueChanged(data?: any) {
    if (!this.createOptionValueItemForm) { return; }
    const form = this.createOptionValueItemForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }
formErrors = {
    'optionName': [],
    'name': []
  };

  validationMessages = {
  	'optionName': {
      'required':      'The optionName is required.'
    },
    'name': {
      'required':      'The name is required.',
      'maxlength':     'The name must be at less 256 characters long.',
    }
  };

optionValueItemCreate(){
  let id  = '-1';
  if(this.id!=null){
    id  = this.id;
  }
  let option_id  = null;
  if(this.valueChoose!=0 && this.valueChoose!=null){
      option_id = this.value.id;
  }
  this.onValueChanged();
  for (const field in this.formErrors) {
      // clear previous error message (if any)
       if(this.formErrors[field].length >0){
         return;
       }
    }
      this.optionFoodValueItem = {
                'rest_id': this.restId,
                'user_id':this.userId,
                'option':{
                  'id':id,
                  'name':this.createOptionValueItemForm.value.name,
                  'option_id':option_id,
                  'is_delete':'0'
                }};
      	this.OptionValueFoodItemService.createOptionValueFoodItems(this.optionFoodValueItem).then(
                    response  => this.processResult(response),
                         error =>  this.failedCreate.bind(error));
  }
  private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
      this.notif.success('New Option item has been added');
       this.router.navigateByUrl('/option-value-food-items/list');
    } else {
      this.error = response.errors;
      if (response.code == 422) {
        if (this.error.name) {
          this.formErrors['name'] = this.error.name;
        }
        if (this.error.option_id) {
          this.formErrors['optionName'] = this.error.option_id;
        }
      } else {
        swal(
          'Create Fail!',
          this.error[0],
          'error'
        ).catch(swal.noop);
      }
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }

/*select start*/

private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;
 
  private get disabledV():string {
    return this._disabledV;
  }
 
  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }
 
  public selected(value:any):void {
    
    console.log('Selected value is: ', value);
    this.createOptionValueItemForm.controls['optionName'].setValue(value.id);
    this.optionName = this.createOptionValueItemForm.controls['optionName'];
  }
 
  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }
 
  public typed(value:any):void {
    console.log('New search input: ', value);
  }
 
  public refreshValue(value:any):void {
     if(value!=undefined && value!=null && value.length!=0){
      this.value = value;
      this.valueChoose = value.id;
     }else{
       this.valueChoose = null;
     }
  }

  
}
