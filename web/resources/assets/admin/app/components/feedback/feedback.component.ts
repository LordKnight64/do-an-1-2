import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { RestaurantService } from 'app/services/restaurant.service';
import { NotificationService } from 'app/services/notification.service';
import { UserService } from "app/services/user.service";
 @Component({
  selector: 'feedback',
  templateUrl: Utils.getView('app/components/feedback/feedback.component.html'),
  styleUrls: [Utils.getView('app/components/feedback/feedback.component.css')]
})
export class FeedbackComponent implements OnInit {
  private mymodel;
  private restId;
  private userId;
  private dtForm: FormGroup;
  private content: AbstractControl = null;

  constructor(
    private restServ: RestaurantService,
    private fb: FormBuilder,
    private userServ: UserService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }

    this.buildForm();
    this.mymodel = {
      content: null,    
    }; 
   
  } 

  private buildForm(): void {

    this.dtForm = this.fb.group({
      'content': ['', [Validators.maxLength(1536)]]     
    });
    this.content = this.dtForm.controls['content'];
  
    this.dtForm.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    
  }

  private onValueChanged(unDirty, data?: any) {

    if (!this.dtForm) { return; }
    const form1 = this.dtForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form1.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'content': []   
  };

  validationMessages = {
    'content': {
      'maxlength': 'The about us may not be greater than 1536 characters.'
    }
  };

   
  private sendFeedback() {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }

    var params = {
      'user_id': this.userId,     
      'content': this.content.value    
    };
    

    this.restServ.sendFeeback(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      this.notifyServ.success('your feedback has been sent');
    }, error => {
      console.log(error);
    });
  }

}
