import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { ContactService } from 'app/services/contact.service';
import { invalidNumberValidator } from "app/utils/number-validator.directive";
import { invalidEmailValidator } from "app/utils/email-validator.directive";
@Component({
  selector: 'contact-item',
  templateUrl: Utils.getView('app/components/contact-item/contact-item.component.html'),
  styleUrls: [Utils.getView('app/components/contact-item/contact-item.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class ContactItemComponent implements OnInit {

  private mymodel;
  private restId;
  private userId;
  private action;
  private dtForm: FormGroup;
  private name: AbstractControl;
  private email: AbstractControl = null;
  private phone: AbstractControl = null;
  private address: AbstractControl = null;
  constructor(
    private contactServ: ContactService,
    private userServ: UserService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storageServ: StorageService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }
    this.buildForm();
    this.mymodel = {
      id: null,
      name: null,
      email: null,
      phone: null,
      address: null,
      is_delete: 0
    };
    if (this.router.url.indexOf('/contact/edit') == 0) {
      this.action = ['Edit', 'UPDATE', 'updated'];
      if (this.storageServ.scope) {
        this.mymodel = this.storageServ.scope;
        this.setVar();
      } else {
        var params = {
          'id': this.activatedRoute.snapshot.params['id']
        };
        this.contactServ.getContact(params).then(
          response => {
            this.mymodel = response.contacts;
            this.setVar();
          },
          error => {
            console.log(error);
          });
      }
    }
    if (this.router.url.indexOf('/contact/create') == 0) {
      this.action = ['Add', 'ADD', 'added'];
      this.storageServ.setScope(null);
    }
    console.log('init contact item component');
  }

  private setVar(): void {

    if (this.mymodel != null) {
      this.mymodel = {
        id: this.mymodel.id,
        name: this.mymodel.name,
        email: this.mymodel.email,
        phone: this.mymodel.phone,
        address: this.mymodel.address,
        is_delete: 0
      };
      this.dtForm.controls['name'].setValue(this.mymodel.name);
      this.dtForm.controls['email'].setValue(this.mymodel.email);
      this.dtForm.controls['phone'].setValue(this.mymodel.phone);
      this.dtForm.controls['address'].setValue(this.mymodel.address);
      this.name = this.dtForm.controls['name'];
      this.email = this.dtForm.controls['email'];
      this.phone = this.dtForm.controls['phone'];
      this.address = this.dtForm.controls['address'];
    }
  }

  private buildForm(): void {

    this.dtForm = this.fb.group({
      'name': ['', [Validators.required, Validators.maxLength(256)]],
      'email': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(256),invalidEmailValidator()]],
      'phone': ['', [Validators.maxLength(32),invalidNumberValidator()]],
      'address': ['', [Validators.maxLength(1536)]]
    });
    this.name = this.dtForm.controls['name'];
    this.email = this.dtForm.controls['email'];
    this.phone = this.dtForm.controls['phone'];
    this.address = this.dtForm.controls['address'];
    this.dtForm.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    //this.onValueChanged(); // (re)set validation messages now
  }

  private onValueChanged(unDirty, data?: any) {

    if (!this.dtForm) { return; }
    const form = this.dtForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'name': [],
    'email': [],
    'phone': [],
    'address': []
  };

  validationMessages = {
    'name': {
      'required': 'The name field is required.',
      'maxlength': 'The name may not be greater than 256 characters.'
    },
    'email': {
      'required': 'The email field is required.',
      'maxlength': 'The email may not be greater than 256 characters.',
      'invalidEmail':  'The email must be a valid email address.',
      'minlength':     'The email must be at least 6 characters long.'
    },
    'phone': {
      'maxlength': 'The phone may not be greater than 32 characters.',
      'invalidNumber': 'he phone must be number'
    },
    'address': {
      'maxlength': 'The address may not be greater than 1536 characters.'
    }
  };

  private doSaveContact() {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }
    //dto
    if (this.action[0] === 'Add') {
      this.mymodel.id = -1;
    }
    this.mymodel.name = this.name.value;
    this.mymodel.email = this.email.value;
    this.mymodel.phone = this.phone.value;
    this.mymodel.address = this.address.value;
    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'contact': this.mymodel
    };
    this.contactServ.updateContact(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      this.notifyServ.success('Contact has been ' + this.action[2]);
      this.router.navigateByUrl('contact');
    }, error => {
      console.log(error);
    });
  };
}
