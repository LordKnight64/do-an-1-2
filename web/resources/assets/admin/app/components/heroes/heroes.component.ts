/**
 * Created by Moiz.Kachwala on 02-06-2016.
 */
import {Component, OnInit} from '@angular/core';
import {HeroService} from "../../services/hero.service";
import {ServerService} from "../../services/server.service";
import {Hero} from "../../models/hero";
// import { Router } from '@angular/router';

@Component({
    selector: 'my-heroes',
    templateUrl: './admin/app/components/heroes/heroes.component.html'
})

export class HeroesComponent implements OnInit {

    heroes: Hero[];
    selectedHero: Hero;
    error: any;

    constructor(
        // private router: Router,
        private heroService: HeroService,
        private serverService: ServerService) { }
    getHeroes() {
        // this.heroService.getHeroes().then(heroes => this.heroes = heroes);
        var action = 'test2';
        var param = {
            'id': 123
        };
        this.serverService.doPost(action, param)
                   .then(
                     // hero  => this.heroes.push(hero),
                     hero  => console.log('-------getHeroes-------', hero),
                     error =>  this.error = <any>error);

    }
    ngOnInit() {
        this.getHeroes();
    }
    onSelect(hero: Hero) { this.selectedHero = hero; }

    gotoDetail() {
        // this.router.navigate(['/detail', this.selectedHero._id]);
    }

    addHero() {
        this.selectedHero = null;
        // this.router.navigate(['/detail', 'new']);

         var action = 'test2';
        var param = {
            'id': 123
        };
        this.serverService.doPost(action, param)
                   .then(
                     // hero  => this.heroes.push(hero),
                     hero  => console.log('-------getHeroes-------', hero),
                     error =>  this.error = <any>error);
    }

    deleteHero(hero: Hero, event: any) {
        event.stopPropagation();
        this.heroService
            .delete(hero)
            .then(res => {
                this.heroes = this.heroes.filter(h => h !== hero);
                if (this.selectedHero === hero) { this.selectedHero = null; }
            })
            .catch(error => this.error = error);
    }
}
