import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';

import { FoodItemService } from 'app/services/foodItem.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
import  { ReviewService } from 'app/services/review.service';
@Component({
  selector: 'review-item',
  templateUrl: Utils.getView('app/components/review-item/review-item.component.html'),
  styleUrls: [Utils.getView('app/components/review-item/review-item.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ReviewItemComponent implements OnInit {

  private userId : String;
  private restId : String;
  private reviewFoodItems : any[];
  private foodItems : any[];

  constructor(
  private foodItemService: FoodItemService,
 
  private authServ: AuthService,
  private notif: NotificationService,
  private reviewService: ReviewService,
  private router: Router,
  private StorageService: StorageService) { }

  ngOnInit() {
       let userInfo = JSON.parse(localStorage.getItem('user_key'));
       this.restId = userInfo.restaurants[0].id;
       this.userId = userInfo.user_info.id;
      
    	 this.fetchAllFoodItems(this.restId);
       this.fetchAllReviewFoodItems(this.restId);

  }
  private fetchAllFoodItems(restId) {
     let param = {
        'rest_id' : restId
      }
  		this.reviewService.getItemsReviews(param).then(
                     response  => { 
                       console.log('fetchAllFoodItems ',response );
                     	this.foodItems = response.items;
                     
                     },
  					 error => {console.log(error)
  					});
  	}
  private fetchAllReviewFoodItems(restId) {
      let param = {
        'rest_id' : restId
      }
  		this.reviewService.getRestaurantReviews(param).then(
                     response  => { 
                      console.log('getRestaurantReviews ',response );
                     	this.reviewFoodItems = response.reviews;
                      
                     },
  					 error => {console.log(error)
  					});
  	}

    private getRating(item) {
      let rating = [];
      let itemRating = Number(item.rating);
      for(let i = 0; i < itemRating; i++) {
          rating.push(i);
      }
      return rating;
    }
  
  deleteReviewFoodItem(item){
        let param  = {
          'id': item.id
          
        };
        alert(item.id);
      	this.reviewService.deleteRestaurantReview(param).then(
                    response  => this.processResult(response),
                         error =>  this.failedCreate.bind(error));
  }

 

updateReviewFoodItems(item) {
  console.log('updateReviewFoodItems ', item);
  let param  = {
          'id': item.id,
          'active_flg': !item.active_flg
        };
  console.log('param ', param);
 
  this.reviewService.updateRestaurantReview(param).then(
                    response  => this.processResult(response),
                         error =>  this.failedCreate.bind(error));

}

  private processResult(response) {
    console.log(response);
    if (response.return_cd == 0) {
      this.notif.success(response.message);
       this.fetchAllFoodItems(this.restId);
       this.fetchAllReviewFoodItems(this.restId);

    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
}
