import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { GeoLocationService } from 'app/services/geoLocation.service';
import { OrderService } from 'app/services/order.service';
import { Location } from '@angular/common';
import { ComCodeService } from 'app/services/comCode.service';
import { EmailService } from 'app/services/email.service';
declare var google: any;

@Component({
	selector: 'ordering-request',
	templateUrl: Utils.getView('app/components/ordering-request/ordering-request.component.html'),
	styleUrls: [Utils.getView('app/components/ordering-request/ordering-request.component.css')],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})
export class OrderingRequestComponent implements OnInit {

	private restId;
	private userId;
	private id: string;
	private order: any;
	private distance: any;
	private totalAmount: any;
	private sub: any;
	private mapObj: any;
	private markerCurrentLocation: any;
	private markerStoreLocation: any;
	private deliveryMethod: any = "";
	private google: any;
	constructor(
		private userServ: UserService,
		private orderServ: OrderService,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private storageServ: StorageService,
		private geoLocationServ: GeoLocationService,
		private notifyServ: NotificationService,
		private comCodeServ: ComCodeService,
		private location: Location,
		private sendMailSer: EmailService
	) { }

	ngOnInit() {
		alert("ok");
		let userKey = JSON.parse(localStorage.getItem('user_key'));
		if (!userKey) {
			this.userServ.logout();
		} else {
			this.userId = userKey.user_info.id;
			this.restId = userKey.restaurants[0].id;
		}

		this.id = this.activatedRoute.snapshot.params['id'];
		if (this.storageServ.scope) {
			this.order = this.storageServ.scope;
			  this.comCodeServ.getCode({ 'cd_group': '0' }).then(response => {
				console.log('response ', response); 
				let codes = response.code;
				for(let i = 0; i < codes.length; i++) {
					if(this.order.delivery_type == +codes[i].cd ) {
						this.deliveryMethod = codes[i].cd_name;
						console.log(this.deliveryMethod );
					}
				}
	
			}, error => {
				console.log(error);
			});
			this.setVar();
		} else {
			var params = {
				'user_id': this.userId,
				'rest_id': this.restId,
				'id': this.id
			};
			this.orderServ.getOrderList(params).then(response => {
				this.order = response.orders[0];
			    
				this.setVar();
                 
				  this.comCodeServ.getCode({ 'cd_group': '0' }).then(response => {
						console.log('response ', response); 
						let codes = response.code;
						for(let i = 0; i < codes.length; i++) {
							if(this.order.delivery_type == +codes[i].cd ) {
								this.deliveryMethod = codes[i].cd_name;
								console.log(this.deliveryMethod );
							}
						}
			
					}, error => {
						console.log(error);
					});
			}, error => {
				console.log(error);
			});
		}

		// this.sub = this.activatedRoute.params.subscribe(params => {
		// 	this.id = params['id'];

		// 	// In a real app: dispatch action to load the details here.
		// 	this.initMap();
		// });
	}

	private setVar(): void {

		if (this.order != null) {
			let delivery_fee = +this.order.delivery_fee;
			this.totalAmount = (this.order.total_price * (1 - this.order.promo_discount)) + delivery_fee;
			var params = {
				'source': {
					'lat_val': this.order.rest_latitue,
					'long_val': this.order.rest_longtitue
				},
				'destination': {
					'lat_val': this.order.latitue,
					'long_val': this.order.longtitue
				}
			};
			this.geoLocationServ.getDistance(params).then(response => {
				this.distance = response[0].distance;
			}, error => {
				console.log(error);
			});
			this.initMap();
		}
	}

	private doUpdateOrder(orderStatus) {

		var params = {
			'user_id': this.userId,
			'order_id': this.order.id,
			'status': orderStatus
		};
		this.orderServ.updateOrder(params).then(response => {
			this.sendMail();
			this.notifyServ.success('Order has been updated');
			this.router.navigateByUrl('order/list');
			//this.goBack();
		}, error => {
			console.log(error);
		});
	}

	private sendMail()
	{		
		  var params = {
			'user_id': this.userId,
			'order_id': this.order.id,
		};
		  this.sendMailSer.sendEmailAcceptOrder(params).then(response => {
	      this.notifyServ.success('Mail has been sent');
	      let harError = false;
	      for (const field in response.errors) {
	        harError = true;
	      }
	      if (harError) {
	        return;
	      }
	      console.log("SENT");
	    }, error => {
	      console.log(error);
	    });
	}
goBack(): void {
    this.location.back();
  }
	ngOnDestroy() {
		// this.sub.unsubscribe();
		this.storageServ.setScope(null);
	}

	initMap() {
		this.mapObj = new google.maps.Map(document.getElementById('order-map'), {
			center: { lat: -34.397, lng: 150.644 },
			zoom: 6
		});

		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer;

		//getCurrentPositionBrower(); // not run un localhost 

		// set markers current Location and store location for test only start
		//var positionCurrent = new google.maps.LatLng(10.8020047, 106.6413804);//(user used web on DB)
		var positionCurrent = new google.maps.LatLng(this.order.latitue, this.order.longtitue);//(user used web on DB)
		this.markerCurrentLocation = new google.maps.Marker({
			position: positionCurrent


		});
		//var positionStore = new google.maps.LatLng(10.8000144, 106.6590802);//lat long of restaurant
		var positionStore = new google.maps.LatLng(this.order.rest_latitue, this.order.rest_longtitue);//lat long of restaurant
		this.markerStoreLocation = new google.maps.Marker({
			position: positionStore


		});

		// var distance = new google.maps.geometry.spherical.computeDistanceBetween(positionCurrent, positionStore);

		directionsService.route({
			origin: this.markerCurrentLocation.getPosition(),
			destination: this.markerStoreLocation.getPosition(),
			travelMode: google.maps.TravelMode.DRIVING
		}, (response, status) => {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				directionsDisplay.setMap(this.mapObj);
				let startLocation = new Object();
				let endLocation = new Object();
				// Display start and end markers for the route.
				let legs = response.routes[0].legs;
				// for (i = 0; i < legs.length; i++) {
				//  if (i == 0) {
				//    startLocation.latlng = legs[i].start_location;
				//    startLocation.address = legs[i].start_address;
				//    // createMarker(legs[i].start_location, "start", legs[i].start_address, "green");
				//  }
				//  if (i == legs.length - 1) {
				//    endLocation.latlng = legs[i].end_location;
				//    endLocation.address = legs[i].end_address;
				//  }
				// }


				//    createInfoWindowGoogleChooseContent();
				//    var contentResult = $('#googleRightClickPopup').html();


				//    infowindowRouteResult = new google.maps.InfoWindow({
				//   content: contentResult,
				//   position:markerRouterB.getPosition()
				// });

				//    infowindowRouteResult.open(mapObj,markerRouterB);

			} else {
				//window.alert('Directions request failed due to ' + status);
			}
			
		});
		// set markers current Location and store location for test only end
	}

	handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
			'Error: The Geolocation service failed.' :
			'Error: Your browser doesn\'t support geolocation.');
	}

	getCurrentPositionBrower() {
		var infoWindow = new google.maps.InfoWindow({ map: this.mapObj });

		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};

				infoWindow.setPosition(pos);
				infoWindow.setContent('Location found.');
				this.mapObj.setCenter(pos);
			}, function () {
				this.handleLocationError(true, infoWindow, this.mapObj.getCenter());
			});
		} else {
			// Browser doesn't support Geolocation
			this.handleLocationError(false, infoWindow, this.mapObj.getCenter());
		}
	}
}
