import { Component, OnInit,ViewChild,ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { CategoryService } from 'app/services/category.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
import { FoodItemService } from 'app/services/foodItem.service';
import {SelectModule,SelectComponent} from 'ng2-select';
import { invalidNumberValidator } from "app/utils/number-validator.directive";
import { OptionValueFoodItemService } from 'app/services/optionValueFoodItem.service';
import { OptionFoodItemService } from 'app/services/optionFoodItem.service';
import { LimitToDirective } from "app/widgets/limit-to.directive";
@Component({
  selector: 'create-food-item',
  templateUrl: Utils.getView('app/components/create-food-item/create-food-item.component.html'),
  styleUrls: [Utils.getView('app/components/create-food-item/create-food-item.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class CreateFoodItemComponent implements OnInit {
  @ViewChild('SelectId') public select: SelectComponent;
  private restId : String;
  private createItemForm: FormGroup;
  private name: AbstractControl;
  private price: AbstractControl;
  private sku: AbstractControl;
  private point: AbstractControl;
  private category_id: AbstractControl;
  private description: AbstractControl;
  private isCheckRadio : string;
  private sub: any;
  private id : string;
  private foodItem: any;
  private optionValueFoodItems : any;
  private optionFoodItems : any;
  private optionALLItemValueFoodItems : Array<any> = [];
  private optionItemLst : Array<any> = [];
  private optionItemTempLst : Array<any> = [];
  private nameRadio: string;
  private foodType: string;
  private isAlcoho: string;
  // private description:string;
  constructor(private OptionFoodItemService: OptionFoodItemService,
  private OptionValueFoodItemService: OptionValueFoodItemService,
  private fb: FormBuilder,private FoodItemService:FoodItemService,
  private categoryService: CategoryService, 
  private StorageService: StorageService, 
  private authServ: AuthService,
  private notif: NotificationService,
   private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
  	this.buildForm();
    this.fetchAllCategories(this.restId);
    this.sub = this.route.params.subscribe(params => {
    this.id = params['id']; });
    this.foodType = '1';  
    this.isAlcoho =  '1';  
    if(this.id!=undefined && this.id!=null){
     
        this.foodItem = this.StorageService.getScope();
      
      
        if(this.foodItem==false){
         
          this.fetchAllItem(this.restId,null,this.id);
        }else{
            this.createItemForm.controls['name'].setValue(this.foodItem.name);
            this.createItemForm.controls['price'].setValue(this.foodItem.price);
            this.createItemForm.controls['category_id'].setValue(this.foodItem.category_id);
            this.createItemForm.controls['sku'].setValue(this.foodItem.sku);
                this.createItemForm.controls['point'].setValue(this.foodItem.point);
            this.createItemForm.controls['description'].setValue(this.foodItem.description);
            this.description=this.createItemForm.controls['description'];
            this.name = this.createItemForm.controls['name'];
            this.price = this.createItemForm.controls['price'];
            this.category_id = this.createItemForm.controls['category_id'];
            this.sku=this.createItemForm.controls['sku'];
            this.point=this.createItemForm.controls['point'];
            this.imageSrc = this.foodItem.thumb; 
            this.foodType = this.foodItem.food_type;  
            this.isAlcoho = this.foodItem.is_alcoho;
            // this.description=this.foodItem.description;
            this.nameRadio=this.foodItem.active_flg;
            if( this.imageSrc==null){
               this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
            }
            this.nameRadio = this.foodItem.active_flg;
            this.isCheckRadio = this.foodItem.active_flg;
            if(this.foodItem!=null){
                this.optionItemLst = this.foodItem.option_items;
              
                //console.log('optionItemLst ', this.optionItemLst);
                if(this.optionItemLst!=null && this.optionItemLst.length>0){
                for (var i = 0; i <  this.optionItemLst.length; i++) {
                        this.optionItemTempLst.push({
                    option_id: this.optionItemLst[i].option_id,
                    option_value_id: this.optionItemLst[i].option_value_id,
                    option_name: this.optionItemLst[i].option_name,
                    option_value_name: this.optionItemLst[i].option_value_name,
                    add_price: this.optionItemLst[i].add_price,
                    point: this.optionItemLst[i].point,
                    active_flg: this.optionItemLst[i].active_flg,
                    old_flg: 1,//edit had in database
                    delete_flg: 0,
                    });
                    }
                }
            }
            this.fetchAllOptionFoodItems(this.restId);                 
           
        }

    }else{
      this.fetchAllOptionFoodItems(this.restId);
    }
    
  }

private fetchAllCategories(restId) {
  		this.categoryService.getCategories(restId,null).then(
                     response  => { 
                     	this.categories = response.categories;
                       this.items.push({
                                id: 0,
                                text: `choose value`
                              });
                       let flgitemChoose = null;
                       for (var i = 0; i <  this.categories.length; i++) {
                              this.items.push({
                                id: this.categories[i].id,
                                text: `${this.categories[i].name}`
                              });

                          if(this.foodItem!=undefined && this.foodItem!=null && this.foodItem.category_id==this.categories[i].id){
                            flgitemChoose = i + 1;
                          }
                         }
                       this.select.items = this.items; 
                       if(flgitemChoose!=null){
                         this.value = this.items[flgitemChoose];  
                      }else{
                        this.value = this.items[0];
                        this.createItemForm.controls['category_id'].setValue(null);
                        this.category_id = this.createItemForm.controls['category_id'];
                       }
                     	console.log(response.categories); 
                     },
  					 error => {console.log(error)
  					});
  	}
  private fetchAllItem(restId,categoryId,id) {
  		this.FoodItemService.getFoodItems(restId,categoryId,id).then(
                     response  => { 
                     	this.foodItem = response.food_items[0];
                      console.log('this.foodItem', 	this.foodItem);

                      this.fetchAllCategories(this.restId);
                      this.fetchAllOptionFoodItems(this.restId);
                         if(this.foodItem!=null){
                                this.optionItemLst = this.foodItem.option_items;
                                this.createItemForm.controls['name'].setValue(this.foodItem.name);
                                this.createItemForm.controls['price'].setValue(this.foodItem.price);
                                this.createItemForm.controls['category_id'].setValue(this.foodItem.category_id);
                                this.createItemForm.controls['sku'].setValue(this.foodItem.sku);
                                 this.createItemForm.controls['point'].setValue(this.foodItem.point);
                                this.createItemForm.controls['description'].setValue(this.foodItem.description);
                                this.description=this.createItemForm.controls['description'];
                                this.name = this.createItemForm.controls['name'];
                                this.price = this.createItemForm.controls['price'];
                                this.point = this.createItemForm.controls['point'];
                                this.category_id = this.createItemForm.controls['category_id'];
                                this.sku=this.createItemForm.controls['sku'];
                                this.imageSrc = this.foodItem.thumb; 
                                this.foodType = this.foodItem.food_type;  
                                this.isAlcoho = this.foodItem.is_alcoho;
                                // this.description=this.foodItem.description;
                                this.nameRadio=this.foodItem.active_flg;
                                if( this.imageSrc==null){
                                    this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                                }
                                this.isCheckRadio = this.foodItem.active_flg;
                                this.value = this.items[this.foodItem.category_id];  
                              if(this.optionItemLst!=null && this.optionItemLst.length>0){
                             
                                for (var i = 0; i <  this.optionItemLst.length; i++) {
                                     this.optionItemTempLst.push({
                                    option_id: this.optionItemLst[i].option_id,
                                    option_value_id: this.optionItemLst[i].option_value_id,
                                    option_name: this.optionItemLst[i].option_name,
                                    option_value_name: this.optionItemLst[i].option_value_name,
                                    add_price: this.optionItemLst[i].add_price,
                                    point: this.optionItemLst[i].point,
                                    active_flg: this.optionItemLst[i].active_flg,
                                    old_flg: 1,//edit had in database
                                    delete_flg: 0,
                                  });
                                }
                              }
                             
                         }
                     	console.log(response.foodItem); 
                     },
  					 error => {console.log(error)
  					});
  	} 
  private fetchAllOptionFoodItems(restId) {
  		this.OptionFoodItemService.getOptionFoodItems(restId,null).then(
                     response  => { 
                     	this.optionFoodItems = response.options;
                      this.fetchAllOptionValueFoodItems(this.restId);
                     	console.log(response.options); 
                     },
  					 error => {console.log(error)
  					});
  	}
  private fetchAllOptionValueFoodItems(restId) {
   
  		this.OptionValueFoodItemService.getOptionValueFoodItems(restId,null).then(
                     response  => { 
                     	this.optionValueFoodItems = response.options;
                       if(this.optionFoodItems!=null && this.optionFoodItems.length>0 && this.optionValueFoodItems!=null && this.optionValueFoodItems.length>0){
                          for (var i = 0; i <  this.optionFoodItems.length; i++) {
                            for (var j = 0; j <  this.optionValueFoodItems.length; j++) {
                             if(this.optionFoodItems[i].id==this.optionValueFoodItems[j].option_id){
                             this.optionALLItemValueFoodItems.push({
                                    option_id: this.optionFoodItems[i].id,
                                    option_value_id: this.optionValueFoodItems[j].id,
                                    option_name: this.optionFoodItems[i].name,
                                    option_value_name: this.optionValueFoodItems[j].name,
                                    add_price: null,
                                    point: null,
                                    active_flg: 1,
                                    old_flg: 0,
                                    delete_flg: 0,
                                    checkbox_flg: false
                                  });
                          }
                            }
                          }
                       }
                        if(this.foodItem!=null){
                                this.optionItemLst = this.foodItem.option_items;
                                console.log('optionItemLst ', this.optionItemLst);
                              if(this.optionItemLst!=null && this.optionItemLst.length>0){
                                for (var i = 0; i <  this.optionItemLst.length; i++) {
                                     for (var j = 0; j <  this.optionALLItemValueFoodItems.length; j++) {
                                       if(this.optionItemLst[i].option_id==this.optionALLItemValueFoodItems[j].option_id && this.optionItemLst[i].option_value_id==this.optionALLItemValueFoodItems[j].option_value_id){
                                         this.optionALLItemValueFoodItems[j].add_price=this.optionItemLst[i].add_price;
                                          this.optionALLItemValueFoodItems[j].point=this.optionItemLst[i].point;
                                         this.optionALLItemValueFoodItems[j].active_flg=this.optionItemLst[i].active_flg;
                                         this.optionALLItemValueFoodItems[j].old_flg=this.optionItemLst[i].old_flg;
                                         this.optionALLItemValueFoodItems[j].checkbox_flg=true;
                                      }
                                     }
                                }
                              }
                             
                         }
                     	console.log(response.options); 
                     },
  					 error => {console.log(error)
  					});
  	} 
private buildForm(): void {
    this.createItemForm = this.fb.group({
      'name': ['', [
          Validators.required,
          Validators.maxLength(256),
        ]

      ],'price':['', [
          Validators.required,
          Validators.maxLength(10),
          invalidNumberValidator()
        ]
      ],
      'category_id':['', [
          Validators.required
        ]
      ],
      'sku':['', [
          Validators.required,
          Validators.maxLength(128),
        ]
      ],
       'point':['', [
          Validators.required,
          Validators.maxLength(128),
        ]
      ],
      'description':[]
    });
       this.name = this.createItemForm.controls['name'];
       this.price = this.createItemForm.controls['price'];
       this.category_id = this.createItemForm.controls['category_id'];
       this.sku=this.createItemForm.controls['sku'];
        this.point=this.createItemForm.controls['point'];
       this.description=this.createItemForm.controls['description'];
       this.createItemForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }
   private onValueChanged(data?: any) {
    if (!this.createItemForm) { return; }
    const form = this.createItemForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }
formErrors = {
    'name': [],
    'price':[],
    'category_id':[],
    'sku':[],
     'point':[]
  };

  validationMessages = {
    'name': {
      'required':      'The name is required.',
      'maxlength':     'The name must be at less 256 characters long.'
    },
     'price': {
      'required':      'The price is required.',
      'maxlength':     'The price must be at less 10 characters long.',
      'invalidNumber': 'he price must be number'
    },
     'category_id': {
      'required':      'The category is required.'
    },
     'sku': {
      'required':      'The sku is required.',
      'maxlength':     'The name must be at less 128 characters long.'
    } ,
     'point': {
      'required':      'The point is required.',
      'maxlength':     'The point must be at less 10 characters long.',
      'invalidNumber': 'he point must be number'
    }
  };
  numberValue(max: Number): Validators {
  return (control: AbstractControl): {[key: string]: any} => {
    const input = control.value,
          isValid = input > max;
    if(isValid) 
        return { 'maxValue': {max} }
    else 
        return null;
  };
}
  createOptionValueFoodItem(){
    this.router.navigateByUrl('option-food-items/list');
  }
  checkFlg(value){
      if(this.isCheckRadio==undefined || this.isCheckRadio==null){
        this.isCheckRadio = '1';
        this.nameRadio='1';
      }
      if(value==this.isCheckRadio){
          return true;
      }
  }
  changenameRadio(value){
     this.nameRadio = value;
  }
  checkfoodType(){
    if(this.foodType=='1'){
        return true;
    }
    return false;
  }
  changefoodType(event){
      let valuecheck=event.target.checked;
      if(valuecheck==true){
          this.foodType = '1';
      }else{
           this.foodType = '0';
      }
  }
   checkIsAlcoho(){
    if(this.isAlcoho=='1'){
        return true;
    }
    return false;
  }
  changeIsAlcoho(event){
      let valuecheck=event.target.checked;
      if(valuecheck==true){
          this.isAlcoho = '1';
      }else{
           this.isAlcoho = '0';
      }
  }
  checkActiveFlg(value,optionValue){
      //TODO
          if(this.optionItemTempLst!=null && this.optionItemTempLst.length>0){
            for (var i = 0; i <  this.optionItemTempLst.length; i++) {
             if(this.optionItemTempLst[i].option_id==optionValue.option_id && this.optionItemTempLst[i].option_value_id==optionValue.option_value_id){
                if(value==this.optionItemTempLst[i].active_flg){
                    return true;
                }
              }
          }
      }
      return false;
  }
  checkboxFlg(optionValue){
        if(this.optionItemTempLst!=null && this.optionItemTempLst.length>0){
        for (var i = 0; i <  this.optionItemTempLst.length; i++) {
            if(this.optionItemTempLst[i].option_id==optionValue.option_id && this.optionItemTempLst[i].option_value_id==optionValue.option_value_id){
                return true;
            }
            }
        }
          return false;
  }
  setValuePrice(optionId,optionValueId){
    if(this.optionItemTempLst!=null && this.optionItemTempLst.length>0){
            for (var i = 0; i <  this.optionItemTempLst.length; i++) {
                if(this.optionItemTempLst[i].option_id==optionId && this.optionItemTempLst[i].option_value_id==optionValueId){
                    return this.optionItemTempLst[i].add_price;
                }
                }
            }
              return null;
  }

   setValuePoint(optionId,optionValueId){
    if(this.optionItemTempLst!=null && this.optionItemTempLst.length>0){
            for (var i = 0; i <  this.optionItemTempLst.length; i++) {
                if(this.optionItemTempLst[i].option_id==optionId && this.optionItemTempLst[i].option_value_id==optionValueId){
                    return this.optionItemTempLst[i].point;
                }
                }
            }
              return null;
  }
  changeCheckboxFlg(event,optionValueItem){
      let valuecheck=event.target.checked;
      let itemCheck = this.checkExit(this.optionItemTempLst,optionValueItem);
         if(itemCheck!=null){
            if(valuecheck!=true){
                    this.optionItemTempLst.splice(itemCheck, 1);
            }
         }else{
            if(valuecheck==true){
            this.optionItemTempLst.push({
                                    option_id: optionValueItem.option_id,
                                    option_value_id: optionValueItem.option_value_id,
                                    option_name: optionValueItem.option_name,
                                    option_value_name: optionValueItem.option_value_name,
                                    add_price: 0,
                                     point: 0,
                                    active_flg: 1,
                                    old_flg: 0,//edit had in database
                                    delete_flg: 0
                                  });
               console.log(this.optionItemTempLst); 
             }
         }
        
    return false;
  }
  changeActiveFlg(event,value,optionValueItem){
    let valuecheck=event.target.checked;
      let itemCheck = this.checkExit(this.optionItemTempLst,optionValueItem);
      if(itemCheck!=null){
          if(value==1){
              this.optionItemTempLst[itemCheck].active_flg=1;
          }else{
              this.optionItemTempLst[itemCheck].active_flg=0;
          }
      }
  }
  changeaddPrice(event,optionValueItem){
     let value= event.target.value;
     let itemCheck = this.checkExit(this.optionItemTempLst,optionValueItem);
      if(itemCheck!=null){
          if(value!=null){
              this.optionItemTempLst[itemCheck].add_price=value;
          }else{
              this.optionItemTempLst[itemCheck].value=0;
          }
      }
  }

   changeaddPoint(event,optionValueItem){
     let value= event.target.value;
     let itemCheck = this.checkExit(this.optionItemTempLst,optionValueItem);
      if(itemCheck!=null){
          if(value!=null){
              this.optionItemTempLst[itemCheck].point=value;
          }else{
              this.optionItemTempLst[itemCheck].value=0;
          }
      }
  }

  checkExit(optionItemLst,optionValueItem){
    if(optionItemLst!=null && optionItemLst.length>0){
        for (var i = 0; i <  optionItemLst.length; i++) {
            if(optionItemLst[i].option_id==optionValueItem.option_id && optionItemLst[i].option_value_id==optionValueItem.option_value_id){
                return i;
                }
            }
        }
        return null;
 }
  isDisabled(optionValue){
        if(this.optionItemTempLst!=null && this.optionItemTempLst.length>0){
        for (var i = 0; i <  this.optionItemTempLst.length; i++) {
            if(this.optionItemTempLst[i].option_id==optionValue.option_id && this.optionItemTempLst[i].option_value_id==optionValue.option_value_id){
                return false;
            }
        }
    }
    return true;
  }
  createItem(){
  let id  = '-1';
  if(this.id!=null){
    id  = this.id;
  }
  this.onValueChanged();
  for (const field in this.formErrors) {
      // clear previous error message (if any)
       if(this.formErrors[field].length >0){
         return;
       }
    }
  let params = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'item':{
              'id':id,
              'category_id':this.createItemForm.value.category_id,
              'name':this.createItemForm.value.name,
              'price':this.createItemForm.value.price,
              'thumb':'',
              'sku':this.createItemForm.value.sku,
              'point':this.createItemForm.value.point,
              'is_active':this.nameRadio,
              'is_alcoho':this.isAlcoho,
              'food_type':this.foodType,
              'description':this.createItemForm.value.description,
              'is_delete':0,
              'update_in_lst_flg':0,
              'option_items':this.optionItemTempLst,
            }};
  	this.FoodItemService.createFoodItems(params).then(
                response  => this.processResult(response),
                     error =>  this.failedCreate.bind(error));

}
private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
     let imagePost = '';
     if( this.flgImageChoose == true){
       imagePost = this.imageSrc;
     }
      let itemUpload = {
         'id':response.item_id,
         'thumb':imagePost
      }
	    this.FoodItemService.itemUploadFile(itemUpload).then(
                     response  => { 
                     	this.notif.success('New Food Item has been added');
                      this.router.navigateByUrl('food-items/list');
                     },
  					 error => {console.log(error)
            
  					});
    } else {
      this.error = response.errors;
      if (response.code == 422) {
        if (this.error.name) {
          this.formErrors['name'] = this.error.name;
        }
        if (this.error.price) {
          this.formErrors['price'] = this.error.price;
        }
        if (this.error.category_id) {
          this.formErrors['category_id'] = this.error.category_id;
        }
        
      } else {
        swal(
          'Create Fail!',
          this.error[0],
          'error'
        ).catch(swal.noop);
      }
    }
  }
  private failedCreate (res) {
    if (res.status == 401) {
      // this.loginfailed = true
    } else {
      if (res.data.errors.message[0] == 'Email Unverified') {
        // this.unverified = true
      } else {
        // other kinds of error returned from server
        for (var error in res.data.errors) {
          // this.loginfailederror += res.data.errors[error] + ' '
        }
      }
    }
  }

/*upload file*/
	private sactiveColor: string = 'green';
    private baseColor: string = '#ccc';
    private overlayColor: string = 'rgba(255,255,255,0.5)';
    
    private dragging: boolean = false;
    private loaded: boolean = false;
    private imageLoaded: boolean = false;
    private imageSrc: string = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
    private flgImageChoose:boolean = false;
    private iconColor: string = '';
    private  borderColor: string = '';
    private  activeColor: string = '';
    private hiddenImage:boolean=false;
    private categories : any;
    private categoryObject : any;
    private userId : String;
    private fileChoose : File;
    private error: any;
    handleDragEnter() {
        this.dragging = true;
    }
    
    handleDragLeave() {
        this.dragging = false;
    }
    
    handleDrop(e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    }
    
    handleImageLoad() {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if(this.imageLoaded = true){
			this.hiddenImage = true;
		}
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        this.loaded = false;

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose= true;
    }
    
    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }
    
    _setActive() {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    }
    
    _setInactive() {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    }

    /*select start*/
  private items:Array<any> = [];
  private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;
 
  private get disabledV():string {
    return this._disabledV;
  }
 
  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }
 
  public selected(value:any):void {
    this.createItemForm.controls['category_id'].setValue(value.id);
    this.category_id = this.createItemForm.controls['category_id'];
    console.log('Selected value is: ', value);
  }
 
  public removed(value:any):void {
    this.createItemForm.controls['category_id'].setValue(null);
    this.category_id = this.createItemForm.controls['category_id'];
    console.log('Removed value is: ', value);
  }
 
  public typed(value:any):void {
    console.log('New search input: ', value);
  }
 
  public refreshValue(value:any):void {
    if(value!=undefined && value!=null && value.length!=0){
      this.value = value;
    }
    // this.value = value;
  }
}
