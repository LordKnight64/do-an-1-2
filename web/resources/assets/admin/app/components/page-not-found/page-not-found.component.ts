import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { routerGrow } from '../../router.animations';
@Component({
  selector: 'page-not-found',
  // templateUrl: './page-not-found.component.html',
  // styleUrls: ['./page-not-found.component.css']
  styleUrls: [Utils.getView('app/components/page-not-found/page-not-found.component.css')],
  templateUrl: Utils.getView('app/components/page-not-found/page-not-found.component.html'),
  animations: [routerGrow()],
  host: {'[@routerGrow]': ''}
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
