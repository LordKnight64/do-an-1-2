import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { RestaurantService } from 'app/services/restaurant.service';
import { NotificationService } from 'app/services/notification.service';
import { UserService } from "app/services/user.service";
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
 @Component({
  selector: 'table-booking',
  templateUrl: Utils.getView('app/components/table-booking/table-booking.component.html'),
  styleUrls: [Utils.getView('app/components/table-booking/table-booking.component.css')],
    animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class TableBookingComponent implements OnInit {
  private mymodel;
  private restId;
  private userId;
  private dtForm: FormGroup;
  private content: AbstractControl = null;
  private Settings:any;
  private stadiumList: any[];
  private timeList:any[];
   private fromDate: any = null;
  constructor(
    private restServ: RestaurantService,
    private fb: FormBuilder,
    private userServ: UserService,
     private router: Router,
    private notifyServ: NotificationService
  ) { 
  
  this.Settings = {
    	statusDateEdit:{
    		t2:false,
    		t3:false,
    		t4:false,
    		t5:false,
    		t6:false,
    		t7:false,
    		cn:false,
    		hn:false
    	},
        list: null,
        currentPage: 0,
        filter: {
            title: "",
            category_id: "",
            created_at_from: "",
            created_at_to: "" 
        },
        showFilter:{
            'tinh_trang' : '1',
            'loaiKhach':'-1'
        },

        deletePopover: {
            content: 'Bạn có thực sự muốn xóa không?',
            Yes: 'Có',
            No: 'Không',
            flag: -1
        },
        timeMstList: ['07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22','23','24'],
        dayInWeek: {
            t2: 'Thứ Hai',
            t3: 'Thứ Ba',
            t4: 'Thứ Tư',
            t5: 'Thứ Năm',
            t6: 'Thứ Sáu',
            t7: 'Thứ Bảy',
            cn: 'Chủ nhật'
        }
    };

   this.timeList = [];
    for (var i = 0; i <  this.Settings.timeMstList.length; i++) {
        var timeMst =  this.Settings.timeMstList[i];

        if(timeMst != '24'){
        	var time1 = {
                    stTime: timeMst + ":00",
                    endTime: timeMst + ":30",
                    cusName: "",
                    checkFlg: 0,
                    endFlg: 0,
                    loaiKhach: '0'
                };

                this.timeList.push(time1);
                var end = '' +timeMst + 1;
                if ((end + '').length == 1) {
                    end = '0' + end;
                }
                var time1 = {
                    stTime: timeMst + ":30",
                    endTime: end + ":00",
                    cusName: "",
                    checkFlg: 0,
                    endFlg: 0,
                    loaiKhach: '0'
                };
                this.timeList.push(time1);
        }
    }

    this.stadiumList = [
          {'ten_khu_vuc': 'Floor 1','stadiumName': 'Table 1 - 5 Slots'}, 
          {'ten_khu_vuc': 'Floor 1','stadiumName': 'Table 2 - 4 Slots'},
          {'ten_khu_vuc': 'Floor 1','stadiumName': 'Table 3 - 3 Slots'},
          {'ten_khu_vuc': 'Floor 2','stadiumName': 'Table 4 - 2 Slots '},
          {'ten_khu_vuc': 'Floor 2','stadiumName': 'Table 5 - 4 Slots'},
          {'ten_khu_vuc': 'Floor 1','stadiumName': 'Table 6 - 5 Slots'}
      ];

      console.log(' this.stadiumList ',  this.stadiumList);

  }

  ngOnInit() {
   
  } 

   public onSelectFromDate(value: any): void {
    
  }
  
   
   public reserveTable() {
      this.router.navigateByUrl('/reserve-table/create/');
   }
}
