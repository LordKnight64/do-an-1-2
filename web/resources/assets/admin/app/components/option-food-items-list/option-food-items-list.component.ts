import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { OptionFoodItemService } from 'app/services/optionFoodItem.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
@Component({
  selector: 'option-food-items-list',
  templateUrl: Utils.getView('app/components/option-food-items-list/option-food-items-list.component.html'),
  styleUrls: [Utils.getView('app/components/option-food-items-list/option-food-items-list.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class OptionFoodItemsListComponent implements OnInit {
  
  private userId : String;
  private restId : String;
  private optionFoodItems : any;
  private optionFoodItem : any;
  constructor(private OptionFoodItemService: OptionFoodItemService,
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private StorageService: StorageService) { }

  ngOnInit() {
       let userInfo = JSON.parse(localStorage.getItem('user_key'));
       this.restId = userInfo.restaurants[0].id;
       this.userId = userInfo.user_info.id;
    	 this.fetchAllOptionFoodItems(this.restId);
  }
	private fetchAllOptionFoodItems(restId) {
  		this.OptionFoodItemService.getOptionFoodItems(restId,null).then(
                     response  => { 
                     	this.optionFoodItems = response.options;
                     	console.log(response.options); 
                     },
  					 error => {console.log(error)
  					});
  	}
    createOptionFoodItem(){
      this.StorageService.setScope(null);
      this.router.navigateByUrl('/option-food-item/create');
    }
    editOptionFoodItem(item){
      this.StorageService.setScope(item);
      this.router.navigateByUrl('/option-food-item/create/'+ item.id);
    }
    deleteOptionFoodItem(item,isActive){
      this.optionFoodItem = {
                'rest_id': this.restId,
                'user_id':this.userId,
                'option':{
                  'id':item.id,
                  'name':item.name,
                  'is_delete':'1'
                }};
      	this.OptionFoodItemService.createOptionFoodItems(this.optionFoodItem).then(
                    response  => this.processResult(response),
                         error =>  this.failedCreate.bind(error));
  }
  private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
      this.notif.success('New Option item has been added');
      this.fetchAllOptionFoodItems(this.restId);
    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
}
