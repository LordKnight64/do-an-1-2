import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { OrderService } from 'app/services/order.service';
import { ContactService } from 'app/services/contact.service';
import { FoodItemService } from 'app/services/foodItem.service';
import { CategoryService } from 'app/services/category.service';
import { TableService } from 'app/services/table.service';
import {TurnOverService} from 'app/services/turnover.service';
import {Observable} from "rxjs/Observable";
import { NotificationService } from 'app/services/notification.service';
import { ComCodeService } from 'app/services/comCode.service';
@Component({
  selector: 'dashboard',
  templateUrl: Utils.getView('app/components/dashboard/dashboard.component.html'),
  styleUrls: [Utils.getView('app/components/dashboard/dashboard.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class DashboardComponent implements OnInit {

  private restId;
  private userId;
  private contactCount;
  private optionFoodItemCount;
  private categoryCount;
  private tableCount;
  private orderList: any;
  private requestOrderList: any;
  private requestOrderTempList: any;
  private requestOrderDelivery: any;
  private requestOrderPickup: any;
  private verifiedOrderList: any;
  private verifiedOrderTempList: any;
  private verifiedOrderDelivery: any;
  private verifiedOrderPickup: any;
  private pendingOrderList: any;
  private pendingOrderTempList: any;
  private pendingOrderDelivery: any;
  private pendingOrderPickup: any;
  private deliveryOrderList: any;
  private deliveryOrderTempList: any;
  private deliveryOrderDelivery: any;
  private deliveryOrderPickup: any;
  private subscription:any;
  private order:any = {};
  private turnOvers:any[];
  private deliveryMethod: any;
  private totalTurnOverByMonth: any = 0;
  constructor(
    private contactServ: ContactService,
    private foodItemServ: FoodItemService,
    private categoryServ: CategoryService,
    private breadServ: BreadcrumbService,
    private userServ: UserService,
    private orderServ: OrderService,
    private tableService: TableService,
     private comCodeServ: ComCodeService,
    private notifyServ: NotificationService,
    private turnOverService: TurnOverService,
    private storageServ: StorageService

  ) {
   
   }

  ngOnInit() {
   
    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }

    this.doSearch();
    this.getOrderRequest(true);
    this.processResult();
     var params = {
      'user_id': this.userId,
      'rest_id': this.restId
    };

    this.fetchTurnOver(this.restId, 2017);
  
   // long polling
   this.subscription = Observable.interval(30000)
      .map((x) => x+1)
      .subscribe((x) => {
        this.getOrderRequest(false);
    });

    this.order = {

    };
  }

  private viewDetail(row) {
    console.log(row);
    this.order = row;
  }

   private fetchTurnOver(restId, year) {
      let param = {
        'rest_id': restId,
        'year_no': year
      };
  		this.turnOverService.getTurnover(param).then(
            response  => {
          	console.log('getTurnover abc',response);
            this.turnOvers  = response.turnover;
             let currentMonth = (new Date()).getMonth();
             console.log('currentMonth ',this.turnOvers[currentMonth]);
            this.calcTurnOverByMonth(this.turnOvers[currentMonth]);
            },
          error => {console.log(error)
        });
  	}

     private calcTurnOverByMonth(turnOversByMonth) {
      console.log('this.turnOvers ', this.turnOvers);
        if(turnOversByMonth.length == 0) {
            this.totalTurnOverByMonth = 0;
        }
        else {
          for(var j = 0; j < turnOversByMonth.length; j++)
          {
              this.totalTurnOverByMonth += Number(turnOversByMonth[j].price);
          }
        }
    }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  private processResult() {

    this.breadServ.set({
      header: "Dashboard",
      description: 'This is our Home page',
      display: true,
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
          title: 'Dashboard',
          state: 'app'
        }
      ]
    });
  }

   getDeliveryMethod() {
    this.comCodeServ.getCode({ 'cd_group': '0' }).then(response => {
				console.log('response ', response); 
				let codes = response.code;
				for(let i = 0; i < codes.length; i++) {
					if(this.order.delivery_type == +codes[i].cd ) {
						this.deliveryMethod = codes[i].cd_name;
						console.log(this.deliveryMethod );
					}
				}
	
			}, error => {
				console.log(error);
			});
  }

  private doUpdateOrder(orderStatus) {

		var params = {
			'user_id': this.userId,
			'order_id': this.order.id,
			'status': orderStatus
		};
		this.orderServ.updateOrder(params).then(response => {
			this.notifyServ.success('Order has been updated');
		 this.getOrderRequest(false);
			//this.goBack();
		}, error => {
			console.log(error);
		});
	}

  private getOrderRequest(isSelect) {
    var params = {
      'user_id': this.userId,
      'rest_id': this.restId
    };
    this.orderServ.getOrderList(params).then(response => {
      
      this.orderList = response.orders;
      this.requestOrderList = this.orderList.filter(x => x.order_sts == 0);
      this.requestOrderDelivery = this.requestOrderList.filter(x => x.delivery_type == 0);
      this.requestOrderPickup = this.requestOrderList.filter(x => x.delivery_type == 1);
      this.verifiedOrderList = this.orderList.filter(x => x.order_sts == 1);
      this.verifiedOrderDelivery = this.verifiedOrderList.filter(x => x.delivery_type == 0);
      this.verifiedOrderPickup = this.verifiedOrderList.filter(x => x.delivery_type == 1);
      this.pendingOrderList = this.orderList.filter(x => x.order_sts == 99);
      this.pendingOrderDelivery = this.pendingOrderList.filter(x => x.delivery_type == 0);
      this.pendingOrderPickup = this.pendingOrderList.filter(x => x.delivery_type == 1);
      this.deliveryOrderList = this.orderList.filter(x => x.order_sts == 2);
      this.deliveryOrderDelivery = this.deliveryOrderList.filter(x => x.delivery_type == 0);
      this.deliveryOrderPickup = this.deliveryOrderList.filter(x => x.delivery_type == 1);
      this.requestOrderTempList = this.requestOrderList;
      this.verifiedOrderTempList = this.verifiedOrderList;
      this.pendingOrderTempList = this.pendingOrderList;
      this.deliveryOrderTempList = this.deliveryOrderList
      
      // don't update order detail when auto loop
      if(isSelect == true) this.order = this.requestOrderList[0];
       this.getDeliveryMethod();
    }, error => {
      console.log(error);
    });
  }

  private doSearch() {

     var params = {
      'user_id': this.userId,
      'rest_id': this.restId
    };

    this.contactServ.getContactList({ 'rest_id': this.restId }).then(response => {
      this.contactCount = response.contacts.length;
    }, error => {
      console.log(error);
    });

    this.foodItemServ.getFoodItems(this.restId, null, null).then(response => {
      
      this.optionFoodItemCount = response.food_items.length;
    }, error => {
      console.log(error)
    });

    this.categoryServ.getCategories(this.restId, null).then(response => {
      this.categoryCount = response.categories.length;
    }, error => {
      console.log(error)
    });

    	this.tableService.getTables(this.restId,null,null).then(
                response  => {
                this.tableCount = response.tables.length;
                
                },
        error => {console.log(error)
      });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }

  private doModelDisplay(orderType, deliveryType) {
    if (orderType == 0) {
      if (deliveryType == 0) {
        this.requestOrderList = this.requestOrderTempList;
      } else if (deliveryType == 1) {
        this.requestOrderList = this.requestOrderDelivery;
      } else if (deliveryType == 2) {
        this.requestOrderList = this.requestOrderPickup;
      }
    } else if (orderType == 1) {
      if (deliveryType == 0) {
        this.verifiedOrderList = this.verifiedOrderTempList;
      } else if (deliveryType == 1) {
        this.verifiedOrderList = this.verifiedOrderDelivery;
      } else if (deliveryType == 2) {
        this.verifiedOrderList = this.verifiedOrderPickup;
      }
    } else if (orderType == 2) {
      if (deliveryType == 0) {
        this.deliveryOrderList = this.deliveryOrderTempList;
      } else if (deliveryType == 1) {
        this.deliveryOrderList = this.deliveryOrderDelivery;
      } else if (deliveryType == 2) {
        this.deliveryOrderList = this.deliveryOrderPickup;
      }
    } else if (orderType == 99) {
      if (deliveryType == 0) {
        this.pendingOrderList = this.pendingOrderTempList;
      } else if (deliveryType == 1) {
        this.pendingOrderList = this.pendingOrderDelivery;
      } else if (deliveryType == 2) {
        this.pendingOrderList = this.pendingOrderPickup;
      }
    }
  }
}
