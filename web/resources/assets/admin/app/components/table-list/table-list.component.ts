import { Component, OnInit,ViewChild,ViewEncapsulation  } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from 'app/router.animations';
import { routerTransition } from 'app/router.animations';
import { TableService } from 'app/services/table.service';
import  { AuthService } from 'app/services/auth.service'
import { NotificationService } from 'app/services/notification.service';
import { default as swal } from 'sweetalert2';
import { Router } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
import {SelectModule,SelectComponent} from 'ng2-select';
import { AreaService } from 'app/services/area.service';
import {Observable} from "rxjs/Observable";
 @Component({
  selector: 'table-list',
  templateUrl: Utils.getView('app/components/table-list/table-list.component.html'),
  styleUrls: [Utils.getView('app/components/table-list/table-list.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class TableListComponent implements OnInit {
  @ViewChild('SelectId') public select: SelectComponent;
  private tables : any;
  private token : any;
  private userId : String;
  private restId : String;
  private tableObject : any;
  private items: Array<any> = [];
  private areas : any;
  private subscription: any;
  constructor(private tableService: TableService,
  private areaService: AreaService,
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private StorageService: StorageService,) { }
  private value:any = {};
  private areaId: any;
  ngOnInit() {

    let userInfo = JSON.parse(localStorage.getItem('user_key'));

    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
    this.fetchAllAreas(this.restId);
    this.areaId = null;
    if(this.value.id!=undefined && this.value.id!=null && this.value.id!=0){
      this.areaId = this.value.id;
    }
    this.fetchAllTables(this.restId,this.areaId);
   
   // long polling
   this.subscription = Observable.interval(30000)
      .map((x) => x+1)
      .subscribe((x) => {
      this.fetchAllTables(this.restId,this.areaId);
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  	private fetchAllTables(restId,area_id) {
  		this.tableService.getTables(restId,null,area_id).then(
                     response  => {
                     	this.tables = response.tables;
                     	console.log(response.tables);
                     },
  					 error => {console.log(error)
  					});
  	}

    private fetchAllAreas(restId) {
      		this.areaService.getAreas(restId,null).then(
                         response  => {
                         	this.areas = response.areas;
                           this.items.push({
                                    id: 0,
                                    text: `ALL`
                                  });
                           for (var i = 0; i <  this.areas.length; i++) {
                                  this.items.push({
                                    id: this.areas[i].id,
                                    text: `${this.areas[i].name}`
                                  });
                             }
                           this.select.items = this.items;
                            this.value = this.items[0];
                         	console.log(response.areas);
                         },
      					 error => {console.log(error)
      					});
      	}


        public selected(value:any):void {

          console.log('Selected value is: ', value);
          
          if(this.value.id!=undefined && this.value.id!=null && this.value.id!=0){
            this.areaId = this.value.id;
          }
        }

        public removed(value:any):void {
          console.log('Removed value is: ', value);
        }

        public typed(value:any):void {
          console.log('New search input: ', value);
        }

        public refreshValue(value:any):void {
          if(value!=undefined && value!=null && value.length!=0){
            this.value = value;
           
            if(this.value.id!=undefined && this.value.id!=null && this.value.id!=0){
              this.areaId = this.value.id;
            }
            this.fetchAllTables(this.restId,this.areaId);
           }else{
              this.fetchAllTables(this.restId,null);
           }
        }

updateTable(item,isActive){
  var action = 'update_tables_by_id';
  this.tableObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'table':{
              'id':item.id,
              'name':item.name,
              'content':item.content,
              'thumb':item.thumb,
              'description':item.description,
              'is_active':isActive,
              'is_delete':'0'
            }};
  	this.tableService.createTable(this.tableObject).then(
                response  => this.processResult(response,null),
                     error =>  this.failedCreate.bind(error));
}

deleteTable(item,isActive){
  let that = this;
  swal({
  title: 'Bạn có chắc không?',
  text: "Khi đã xóa không thể khôi phục lại",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Có'
}).then(function () {
  that.tableObject = {
            'rest_id': that.restId,
            'user_id':that.userId,
            'table':{
              'id':item.id,
              'name':item.name,
              'thumb':item.thumb,
              'description':item.description,
              'is_active':isActive,
              'is_delete':'1'
            }};
  	that.tableService.createTable(that.tableObject).then(

                response  => that.processResult(response,item.thumb),
                     error =>  that.failedCreate.bind(error),
                 );
});
}

private reserveTable(id)
{
  this.router.navigateByUrl('/table-booking-schedule/view/'+ id);
}

private processResult(response,thumb) {

    if (response.message == undefined || response.message == 'OK') {
      if(thumb!=null){
        this.notif.success('Table is deleting...');
        this.tableService.deleteFile({ 'thumb': thumb }).then(response => {
          this.notif.success('Done');
        }, error => {
          console.log(error);
        });
        this.fetchAllTables(this.restId,this.value.id);

      }
      else
      {
        this.notif.success('Done');
        this.fetchAllTables(this.restId,this.value.id);

      }

    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
  createTable(){
    //   this.userService.currentUser.subscribe((user) => {
    //    console.log('user', user);

    // });
      this.StorageService.setScope(null);
      this.router.navigateByUrl('/table/create');
  }
  editTable(item){
      this.StorageService.setScope(item);
      this.router.navigateByUrl('/table/create/'+ item.id);
  }
 }
