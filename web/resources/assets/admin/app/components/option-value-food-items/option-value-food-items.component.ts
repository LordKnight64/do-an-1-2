import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { OptionValueFoodItemService } from 'app/services/optionValueFoodItem.service';
import { OptionFoodItemService } from 'app/services/optionFoodItem.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
@Component({
  selector: 'option-value-food-items',
  templateUrl: Utils.getView('app/components/option-value-food-items/option-value-food-items.component.html'),
  styleUrls: [Utils.getView('app/components/option-value-food-items/option-value-food-items.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class OptionValueFoodItemsComponent implements OnInit {

  private userId : String;
  private restId : String;
  private optionValueFoodItems : any;
  private optionValueFoodItem : any;
  private optionFoodItems : any;
  private optionFoodItem : any;

  constructor(
  private OptionFoodItemService: OptionFoodItemService,
  private OptionValueFoodItemService: OptionValueFoodItemService,
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private StorageService: StorageService) { }

  ngOnInit() {
       let userInfo = JSON.parse(localStorage.getItem('user_key'));
       this.restId = userInfo.restaurants[0].id;
       this.userId = userInfo.user_info.id;
    	 this.fetchAllOptionFoodItems(this.restId);
       this.fetchAllOptionValueFoodItems(this.restId);

  }
  private fetchAllOptionFoodItems(restId) {
  		this.OptionFoodItemService.getOptionFoodItems(restId,null).then(
                     response  => { 
                     	this.optionFoodItems = response.options;
                     	console.log(response.options); 
                     },
  					 error => {console.log(error)
  					});
  	}
  private fetchAllOptionValueFoodItems(restId) {
  		this.OptionValueFoodItemService.getOptionValueFoodItems(restId,null).then(
                     response  => { 
                     	this.optionValueFoodItems = response.options;
                     	console.log(response.options); 
                     },
  					 error => {console.log(error)
  					});
  	}
  createOptionValueFoodItemForOption(item){
    this.StorageService.setScope(item.id);
    this.router.navigateByUrl('/option-value-food-item/create');
  }
  editOptionValueFoodItemForOption(itemvalue){
    this.StorageService.setScope(itemvalue);
    this.router.navigateByUrl('/option-value-food-item/create/' + itemvalue.id);
  }
  createOptionValueFoodItem(){
    this.StorageService.setScope(null);
    this.router.navigateByUrl('/option-value-food-item/create');
  }
  deleteOptionFoodItem(item){
      this.optionFoodItem = {
                'rest_id': this.restId,
                'user_id':this.userId,
                'option':{
                  'id':item.id,
                  'name':item.name,
                  'option_id':item.option_id,
                  'is_delete':'1'
                }};
      	this.OptionValueFoodItemService.createOptionValueFoodItems(this.optionFoodItem).then(
                    response  => this.processResult(response),
                         error =>  this.failedCreate.bind(error));
  }
  private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
      this.notif.success('New Option item has been added');
      this.fetchAllOptionFoodItems(this.restId);
      this.fetchAllOptionValueFoodItems(this.restId);
    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
}
