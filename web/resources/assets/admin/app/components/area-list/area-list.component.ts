import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from 'app/router.animations';
import { routerTransition } from 'app/router.animations';
import { AreaService } from 'app/services/area.service';
import  { AuthService } from 'app/services/auth.service'
import { NotificationService } from 'app/services/notification.service';
import { default as swal } from 'sweetalert2';
import { Router } from '@angular/router';
import { StorageService } from 'app/services/storage.service';

 @Component({
  selector: 'area-list',
  templateUrl: Utils.getView('app/components/area-list/area-list.component.html'),
  styleUrls: [Utils.getView('app/components/area-list/area-list.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class AreaListComponent implements OnInit {
 private areas : any;
  private token : any;
  private userId : String;
  private restId : String;
  private areaObject : any;

  constructor(private areaService: AreaService,
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private StorageService: StorageService,) { }

  ngOnInit() {

    let userInfo = JSON.parse(localStorage.getItem('user_key'));

    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
  	this.fetchAllAreas(this.restId);

  }

  	private fetchAllAreas(restId) {
      console.log('hihi');
  		this.areaService.getAreas(restId,null).then(
                     response  => {
                     	this.areas = response.areas;
                     	console.log(response.areas);
                     },
  					 error => {console.log(error)
  					});
  	}

updateArea(item,isActive){
  this.areaObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'area':{
              'id':item.id,
              'name':item.name,
              'thumb':item.thumb,
              'is_active':isActive,
              'is_delete':'0'
            }};
  	this.areaService.createArea(this.areaObject).then(
                response  => this.processResult(response,null),
                     error =>  this.failedCreate.bind(error));
}

deleteArea(item,isActive){
  var action = 'update_areas_by_id';
  this.areaObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'area':{
              'id':item.id,
              'name':item.name,
              'thumb':item.thumb,
              'is_active':isActive,
              'is_delete':'1'
            }};
  	this.areaService.createArea(this.areaObject).then(
                response  => this.processResult(response,item.thumb),
                     error =>  this.failedCreate.bind(error));
}
private processResult(response,thumb) {

    if (response.message == undefined || response.message == 'OK') {
      if(thumb!=null){
        this.areaService.deleteFile({ 'thumb': thumb }).then(response => {
          this.notif.success('News has been deleted');
        }, error => {
          console.log(error);
        });
      }
      this.notif.success('New area has been added');
      this.fetchAllAreas(this.restId);
    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
  createArea(){
    //   this.userService.currentUser.subscribe((user) => {
    //    console.log('user', user);

    // });
      this.StorageService.setScope(null);
      this.router.navigateByUrl('/area/create');
  }
  editArea(item){
      this.StorageService.setScope(item);
      this.router.navigateByUrl('/area/create/'+ item.id);
  }
}
