import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { User } from 'app/models/user';
import { UserService } from 'app/services/user.service';
import { ServerService } from 'app/services/server.service';
import { routerGrow } from '../../router.animations';
import { Router } from '@angular/router';
import Utils from 'app/utils/utils';
import moment = require('moment')
import { AuthService } from "app/services/auth.service";
import { invalidEmailValidator } from "app/utils/email-validator.directive";
import { default as swal } from 'sweetalert2';
import { SelectComponent } from 'ng2-select';
import { TranslateService } from 'ng2-translate';
import { ENVIRONMENT } from 'environments/environment';
import { ModalModule , ModalDirective} from 'ngx-bootstrap';

import { LoginModalComponent} from 'app/components/login-modal/login-modal.component';




declare var myExtObject: any;
@Component({
  selector: 'app-login',
  styleUrls: [Utils.getView('app/components/login/login.component.css')],
  templateUrl: Utils.getView('app/components/login/login.component.html'),
  animations: [routerGrow()],
  host: {'[@routerGrow]': ''}

})
export class LoginComponent implements OnInit {
  private password: AbstractControl;
  private email: AbstractControl;
  private error: any;
  private loginForm: FormGroup;
  private isHidden: any = true;
   
  private items: Array<any> = [];
  private url = ENVIRONMENT.DOMAIN_API + ':' + ENVIRONMENT.SERVER_PORT + '/';  
  @ViewChild('SelectId') private SelectId: SelectComponent;
  @ViewChild('childModal') private lgModal: LoginModalComponent;


  constructor(
    private serverServ: ServerService,
    private userServ: UserService,
  
    private viewContainerRef: ViewContainerRef,
   
    private authServ: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private translateService: TranslateService
  ) {
    
  }

  public ngOnInit() {
   
    this.buildForm();
    window.dispatchEvent( new Event( 'resize' ) );
    this.items.push({ id: 1, text: `English` });
    this.items.push({ id: 2, text: `Việt Nam` });
    this.items.push({ id: 3, text: `Japan` });
    // this.SelectId.items = this.items;
    // this.SelectId.active = this.items[1];
    
     if(localStorage.getItem('langChoosen')!=undefined && localStorage.getItem('langChoosen')!=null){
      this.translateService.setDefaultLang(localStorage.getItem('langChoosen'));
      this.translateService.use(localStorage.getItem('langChoosen'));
      for(var i = 1; i <= this.items.length; i++){
        var langDisplay = null;
        if(this.items[i-1].id==1){
            langDisplay = 'en';
        }
         if(this.items[i-1].id==2){
            langDisplay = 'vi';
        }
         if(this.items[i-1].id==3){
            langDisplay = 'ja';
        }
       if(langDisplay==localStorage.getItem('langChoosen')){
        this.value = this.items[i-1];
       }
      }
    }else{
      this.value = this.items[0];
    }
  }
  private login() {
  
    this.isHidden = false;
   
    // test les champs en js

    // envoyer les champs a php
    this.onValueChanged();
     for (const field in this.formErrors) {
      // clear previous error message (if any)
       if(this.formErrors[field].length >0){
         return;
       }
    }
    // si retour positif, log le user
    if ( 1 === 1 ) {   
      var action = 'login';
      var params = {
          'password': this.loginForm.value.password,
          'email': this.loginForm.value.email,
          'rest_id': 1
       
      };
      
      this.serverServ.doPost(action, params).then(
                     response  => this.processResult(response),
                     error =>  this.failedLogin.bind(error));

      // this.router.navigate( ['home'] );
    } else {
      // je recupere l'erreur du php
      // et on le place dans un label, ou un toaster
    }

  }

  private processResult(response) {
    this.isHidden = true;
    if (response.message == undefined) {  
      if(response.restaurants.length > 1) {
         this.lgModal.restaurants = response.restaurants;
         this.lgModal.response = response;
         this.lgModal.user = response.user_info;
         this.lgModal.show();  
      }
      else
      {
          var user = response.user_info;
          var restaurant = response.restaurants;
          console.log('user ', restaurant.lenght);
    
          // var token = response.token;
          let loginedUser = new User( {           
                email: user.email,
                firstname: user.first_name,
                lastname: user.last_name,
                roles: user.roles,
                phone:user.phone,
                tel:user.tel
            }, restaurant);
    
          // set token
          this.authServ.setToken(response.token); 
          loginedUser.isInit = true;
          this.userServ.setCurrentUser( loginedUser );
          this.userServ.setUserInfo( response );
      }

    
    } else {
      this.error = response.errors;  
      if (response.code == 422) {
        if (this.error.email) {
          this.formErrors['email'] = this.error.email;
        }
        if (this.error.password) {
          this.formErrors['password'] = this.error.password;
        }
      
      } else if (response.code == 401) {
        swal(
          'Login Fail!',
          this.error[0],
          'error'
        ).catch(swal.noop);
      }
  }
  }

  private failedLogin (res) {
    if (res.status == 401) {
      // this.loginfailed = true
    } else {
      if (res.data.errors.message[0] == 'Email Unverified') {
        // this.unverified = true
      } else {
        // other kinds of error returned from server
        for (var error in res.data.errors) {
          // this.loginfailederror += res.data.errors[error] + ' '
        }
      }
    }
  }
  
  private buildForm(): void {
    this.loginForm = this.fb.group({
      'email': ['', [
          Validators.required,
          Validators.minLength(6),
          invalidEmailValidator()
        ]
      ],
     'password': ['', [
          Validators.required,
          Validators.minLength(6),
        ]
      ]
    });
    this.email = this.loginForm.controls['email'];
    this.password = this.loginForm.controls['password'];
  
    this.loginForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }
  private onValueChanged(data?: any) {
    if (!this.loginForm) { return; }
    const form = this.loginForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'email': [],
    'password': []
   
  };

  validationMessages = {
    'email': {
      'required':      'The email is required.',
      'minlength':     'The email must be at least 6 characters long.',
      'invalidEmail':  'The email must be a valid email address.',
    },
    'password': {
      'required':      'The password is required.',
      'minlength':     'The password must be at least 6 characters long.',
    }
  };

  /*select start*/

private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;
 
  private get disabledV():string {
    return this._disabledV;
  }
 
  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }
 
  public selected(value:any):void {
    let langchoosen = 'en';
    if(value.id == 2){
      langchoosen = 'vi';
    }
    if(value.id == 3){
      langchoosen = 'ja';
    }
    if(localStorage.getItem('langChoosen')!=undefined && localStorage.getItem('langChoosen')!=null && langchoosen!=localStorage.getItem('langChoosen')){
      localStorage.setItem('langChoosen', langchoosen); 
      this.translateService.setDefaultLang(localStorage.getItem('langChoosen'));
      this.translateService.use(localStorage.getItem('langChoosen'));
    }
    console.log('Selected value is: ', value);
  }
 
  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }
 
  public typed(value:any):void {
    console.log('New search input: ', value);
  }
 
  public refreshValue(value:any):void {
      this.value = value;
  }

}
