import { Component, OnInit,ViewChild,ViewEncapsulation  } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { FoodItemService } from 'app/services/foodItem.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
import { CategoryService } from 'app/services/category.service';
import {SelectModule,SelectComponent} from 'ng2-select';
@Component({
  selector: 'food-items-list',
  templateUrl: Utils.getView('app/components/food-items-list/food-items-list.component.html'),
  styleUrls: [Utils.getView('app/components/food-items-list/food-items-list.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class FoodItemsListComponent implements OnInit {
  @ViewChild('SelectId') public select: SelectComponent;
  private items:Array<any> = [];
  private itemLst : any;
  private userId : String;
  private restId : String;
  private categoryObject : any;
  private categories : any;
  constructor(private FoodItemService:FoodItemService,
  private categoryService: CategoryService,
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private StorageService: StorageService,) { }

  ngOnInit() {
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
    
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
  	this.fetchAllCategories(this.restId);
    let categoryId = null;
    if(this.value.id!=undefined && this.value.id!=null && this.value.id!=0){
      categoryId = this.value.id;
    }
    this.fetchAllItem(this.restId,categoryId);
  }
	private fetchAllItem(restId,categoryId) {
  		this.FoodItemService.getFoodItems(restId,categoryId,null).then(
                     response  => { 
                     	this.itemLst = response.food_items;
                     	console.log(response.categories, 	this.itemLst); 
                     },
  					 error => {console.log(error)
  					});
  	}

private fetchAllCategories(restId) {
  		this.categoryService.getCategories(restId,null).then(
                     response  => { 
                     	this.categories = response.categories;
                       this.items.push({
                                id: 0,
                                text: `ALL`
                              });
                       for (var i = 0; i <  this.categories.length; i++) {
                              this.items.push({
                                id: this.categories[i].id,
                                text: `${this.categories[i].name}`
                              });
                         }
                       this.select.items = this.items; 
                        this.value = this.items[0];  
                     	console.log(response.categories); 
                     },
  					 error => {console.log(error)
  					});
  	}

    createItem(){
      this.StorageService.setScope(null);
      this.router.navigateByUrl('/food-item/create');
    }
    editItem(item){
       this.StorageService.setScope(item);
      this.router.navigateByUrl('/food-item/edit/'+ item.id);
    }
deleteItem(item){
  let params = {
            'rest_id': this.restId,
            'user_id':this.userId,
             'item':{
              'id':item.id,
              'category_id':item.category_id,
              'name':item.name,
              'price':item.price,
              'thumb':'',
              'sku':item.sku,
              'is_active':item.active_flg,
              'is_alcoho':item.is_alcoho,
              'food_type':item.food_type,
              'description':item.description,
              'is_delete':1,
              'option_items':item.option_items,
            }};
  	this.FoodItemService.createFoodItems(params).then(
                response  => this.processResult(response,item.thumb),
                     error =>  this.failedCreate.bind(error));
}
updateItem(item,active){
   let params = {
            'rest_id': this.restId,
            'user_id':this.userId,
             'item':{
              'id':item.id,
              'category_id':item.category_id,
              'name':item.name,
              'price':item.price,
              'thumb':'',
              'point':item.point,
              'sku':item.sku,
              'is_active':active,
              'is_alcoho':item.is_alcoho,
              'food_type':item.food_type,
              'description':item.description,
              'is_delete':0,
              'update_in_lst_flg':1,
              'option_items':item.option_items,
            }};
  	this.FoodItemService.createFoodItems(params).then(
                response  => this.processResult(response,null),
                     error =>  this.failedCreate.bind(error));

}
private processResult(response,thumb) {

    if (response.message == undefined || response.message == 'OK') {
      if(thumb!=null){
        this.FoodItemService.deleteFile({ 'thumb': thumb }).then(response => {
          this.notif.success('News has been deleted');
        }, error => {
          console.log(error);
        });
      }
      this.notif.success('Item has been deleted');
      this.fetchAllItem(this.restId,this.value.id);
    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
 /*select start*/

private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;
 
  private get disabledV():string {
    return this._disabledV;
  }
 
  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }
 
  public selected(value:any):void {
    
    console.log('Selected value is: ', value);
    let categoryId = null;
    if(this.value.id!=undefined && this.value.id!=null && this.value.id!=0){
      categoryId = this.value.id;
    }
    this.fetchAllItem(this.restId,categoryId);
  }
 
  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }
 
  public typed(value:any):void {
    console.log('New search input: ', value);
  }
 
  public refreshValue(value:any):void {
    if(value!=undefined && value!=null && value.length!=0){
      this.value = value;
      let categoryId = null;
        if(this.value.id!=undefined && this.value.id!=null && this.value.id!=0){
          categoryId = this.value.id;
        }
        this.fetchAllItem(this.restId,categoryId);
     }else{
        this.fetchAllItem(this.restId,null);
     }
  }   
}
