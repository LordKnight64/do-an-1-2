import { Component, OnInit,ViewChild,ViewEncapsulation } from '@angular/core';
import Utils from 'app/utils/utils';
import { routerFade, routerTransition } from '../../router.animations';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { ProfileService } from 'app/services/profile.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
import {SelectModule,SelectComponent} from 'ng2-select';
import { invalidNumberValidator } from "app/utils/number-validator.directive";
import { LimitToDirective } from "app/widgets/limit-to.directive";
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { invalidEmailValidator } from "app/utils/email-validator.directive";
import { ComCodeService } from 'app/services/comCode.service';
import { UserService } from 'app/services/user.service';
import { TimepickerModule,TimepickerComponent } from 'ngx-bootstrap';
import { ENVIRONMENT } from '../../../environments/environment';
declare var google: any;
@Component({
  selector: 'profile', 
  styleUrls: [Utils.getView('app/components/profile/profile.component.css')],
  templateUrl: Utils.getView('app/components/profile/profile.component.html'),
	animations: [routerFade()],
  host: {'[@routerFade]': ''}
})
export class ProfileComponent implements OnInit {
  @ViewChild('SelectId') public select: SelectComponent;
  @ViewChild('SelectCurrencyId') public selectCurrency: SelectComponent;
  private mapObj: any;
  private markerCurrentLocation: any;
  private markerStoreLocation: any; 
	private tabChangeFisrt: Boolean = true;
	private userId : String;
  private restId : String;
	private userInfo: any; 
	private createProfileForm: FormGroup;
	private first_name: AbstractControl;
  private last_name: AbstractControl;
	private email: AbstractControl;
  private phone: AbstractControl;
  private tel: AbstractControl;
	private password: AbstractControl;
  private confirm_password: AbstractControl;
	private restaurant_name: AbstractControl;
	private restaurant_address: AbstractControl;
	private restaurant_tel_number: AbstractControl;
	private restaurant_mobile_number: AbstractControl;
	private restaurant_other_email: AbstractControl;
	private rest_type: AbstractControl;
	private addressInfo: any;
	private restaurantInfo : any;
  private orderSetting : any;
  private open_time: AbstractControl = null;
  private close_time: AbstractControl = null;
  private website:AbstractControl;
  private promotion:AbstractControl;
  private promo_discount:AbstractControl;
  private default_currency:AbstractControl;
  private facebook_link:AbstractControl;
  private twitter_link:AbstractControl;
  private linkedIn_link:AbstractControl;
  private google_plus_link:AbstractControl;
  private lat: any=null;
  private lon: any=null;
  private changelonlatFlag: Boolean=false;
  private payment_fee: any;
  private delivery_charge: any;
  private minimum_order: any;
  private payment_method: any;
  private delivery_methods: any;
  private delivery_time: any;
  private food_ordering_system_type: any;
  constructor(
  private fb: FormBuilder,
	private comCodeService: ComCodeService, 
  private profileService: ProfileService, 
  private StorageService: StorageService, 
  private userService: UserService, 
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,private route: ActivatedRoute) { }
  private items:Array<any> = [];
  private itemCurrencys:Array<any> = [];
  private markers:any = {};
  private idmarker:any = 0;
  ngOnInit() {
		 this.buildForm();
     let userLogin = JSON.parse(localStorage.getItem('user_key'));
     this.restId = userLogin.restaurants[0].id;
     this.userId = userLogin.user_info.id;
		 console.log(userLogin);
		 this.userInfo = userLogin.user_info;
		 this.createProfileForm.controls['first_name'].setValue( this.userInfo.first_name);
		 this.createProfileForm.controls['last_name'].setValue( this.userInfo.last_name);
		 this.createProfileForm.controls['email'].setValue( this.userInfo.email);
		 this.createProfileForm.controls['phone'].setValue( this.userInfo.phone);
     this.createProfileForm.controls['tel'].setValue( this.userInfo.tel);
		 this.first_name = this.createProfileForm.controls['first_name'];
     this.last_name = this.createProfileForm.controls['last_name'];
		 this.email = this.createProfileForm.controls['email'];
     this.phone = this.createProfileForm.controls['phone'];
     


		 this.fetchProfileRetaurant();
      
  }
		private fetchCode(cd) {
				let param = {
			              'cd_group': cd
				};
				this.comCodeService.getCode(param).then(
                     response  => {
											 if(response.code!=null && response.code.length>0){
	                     	for (var i = 0; i <  response.code.length; i++) {
													 this.items.push({
                                id: response.code[i].cd.toString(),
                                text: response.code[i].cd_name
                              });
												 }
													this.select.items = this.items;
													this.value = this.items[this.rest_type.value];
											 }
                     },
  					 error => {console.log(error)
  					});
		}
    	private fetchCurrentCyCode(cd) {
				let param = {
			              'cd_group': cd
				};
				this.comCodeService.getCode(param).then(
                     response  => {
											 if(response.code!=null && response.code.length>0){
                         let cdCurrentcy = null;
	                     	for (var i = 0; i <  response.code.length; i++) {
													 this.itemCurrencys.push({
                                id: response.code[i].cd,
                                text: `${response.code[i].cd_name}`
                              });
                              if(this.default_currency.value==response.code[i].cd_name){
                                    cdCurrentcy = response.code[i].cd;
                              }
												 }
													this.selectCurrency.items = this.itemCurrencys;
													this.valueCurrentcy = this.itemCurrencys[cdCurrentcy];
											 }
                     },
  					 error => {console.log(error)
  					});
		}
	private fetchProfileRetaurant() {
  		this.profileService.getProfileRetaurant(this.restId,this.userId).then(
                     response  => { 
											 if(response.restaurant_info!=null && response.restaurant_info.length>0){
	                     	this.restaurantInfo = response.restaurant_info[0];
	                     	console.log(this.restaurantInfo); 
												  this.createProfileForm.controls['restaurant_name'].setValue( this.restaurantInfo.name);
												  this.createProfileForm.controls['restaurant_address'].setValue( this.restaurantInfo.address);
													this.createProfileForm.controls['restaurant_tel_number'].setValue( this.restaurantInfo.tel);
													this.createProfileForm.controls['restaurant_mobile_number'].setValue( this.restaurantInfo.mobile);
													this.createProfileForm.controls['restaurant_other_email'].setValue( this.restaurantInfo.email);
                          this.createProfileForm.controls['website'].setValue( this.restaurantInfo.website);
                          this.createProfileForm.controls['promotion'].setValue( this.restaurantInfo.promotion);
                          this.createProfileForm.controls['promo_discount'].setValue( this.restaurantInfo.promo_discount);
                          this.createProfileForm.controls['facebook_link'].setValue( this.restaurantInfo.facebook_link);
                          this.createProfileForm.controls['twitter_link'].setValue( this.restaurantInfo.twitter_link);
                          this.createProfileForm.controls['linkedIn_link'].setValue( this.restaurantInfo.linkedIn_link);
                          this.createProfileForm.controls['google_plus_link'].setValue( this.restaurantInfo.google_plus_link);
                          this.createProfileForm.controls['rest_type'].setValue( this.restaurantInfo.rest_type);
													this.restaurant_name = this.createProfileForm.controls['restaurant_name'];
												  this.restaurant_address = this.createProfileForm.controls['restaurant_address'];
												  this.restaurant_tel_number = this.createProfileForm.controls['restaurant_tel_number'];
													this.restaurant_mobile_number = this.createProfileForm.controls['restaurant_mobile_number'];
												  this.restaurant_other_email = this.createProfileForm.controls['restaurant_other_email'];
                          this.restaurant_other_email = this.createProfileForm.controls['restaurant_other_email'];
                          this.promotion = this.createProfileForm.controls['promotion'];
                          this.promo_discount = this.createProfileForm.controls['promo_discount'];
                          this.facebook_link = this.createProfileForm.controls['facebook_link'];
                          this.twitter_link = this.createProfileForm.controls['twitter_link'];
                          this.linkedIn_link = this.createProfileForm.controls['linkedIn_link'];
                          this.google_plus_link = this.createProfileForm.controls['google_plus_link'];
													this.rest_type=this.createProfileForm.controls['rest_type'];
                          this.lat = this.restaurantInfo.latitue;
                          this.lon = this.restaurantInfo.longtitue;
                          this.imageSrc = this.restaurantInfo.logo;   
                          if( this.imageSrc==null){
                             this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                          }

													this.fetchCode(3);
                          let tg = this.restaurantInfo.open_time.match(/(\d+)\:(\d+)\:(\d+)/);
                          let ng = new Date();
                          ng.setHours(tg[1]);
                          ng.setMinutes(tg[2]);
                          ng.setSeconds(tg[3]);
                          this.createProfileForm.controls['open_time'].setValue(ng);
                          this.open_time=this.createProfileForm.controls['open_time'];
                          
                          let tg2 = this.restaurantInfo.close_time.match(/(\d+)\:(\d+)\:(\d+)/);
                          let ng2 = new Date();
                          ng2.setHours(tg2[1]);
                          ng2.setMinutes(tg2[2]);
                          ng2.setSeconds(tg2[3]);
                          this.createProfileForm.controls['close_time'].setValue(ng2);
                          this.close_time=this.createProfileForm.controls['close_time'];
                          this.fetchOrderSeetingProfile();
											 }
                     },
  					 error => {console.log(error)
  					});
  	}
    	private fetchOrderSeetingProfile() {
				this.profileService.getOrderSeetingProfile(this.restId).then(
                     response  => {
                        if(response.ordering_setting!=null){
	                     	  this.orderSetting = response.ordering_setting;
											    this.payment_fee=this.orderSetting.payment_fee;
                          this.delivery_charge=this.orderSetting.delivery_charge;
                          this.minimum_order=this.orderSetting.minimum_order;
                          this.payment_method=this.orderSetting.payment_method;
                          this.delivery_methods=this.orderSetting.delivery_methods;
                          this.delivery_time=this.orderSetting.delivery_time;
                          this.food_ordering_system_type=this.orderSetting.food_ordering_system_type;
                          this.createProfileForm.controls['default_currency'].setValue(this.orderSetting.default_currency);
                          this.default_currency = this.createProfileForm.controls['default_currency'];
                          this.fetchCurrentCyCode(2);
                        }
                     },
  					 error => {console.log(error)
  					});
		}
	private buildForm(): void {
    this.createProfileForm = this.fb.group({
      'first_name': ['', [
          Validators.required,
          Validators.maxLength(256),
        ]

      ],'last_name':['', [
          Validators.required,
          Validators.maxLength(256)
        ]
      ],'email':['', [
          Validators.required,
          Validators.maxLength(256),
          invalidEmailValidator()
        ]
      ],'phone':['', [
          Validators.required,
          Validators.maxLength(32),
          invalidNumberValidator()
        ]
      ],'tel':['', [
          Validators.required,
          Validators.maxLength(32)
        ]
      ],'password':['', [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(256),
        ]],
			'confirm_password':['', [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(256),
        ]],
			'restaurant_name':['', [
          Validators.required,
          Validators.maxLength(256)
        ]
      ],
			'restaurant_address':['', [
          Validators.required,
          Validators.maxLength(1024)
        ]
      ],'restaurant_tel_number':[],
			'restaurant_mobile_number':[],
			'restaurant_other_email':[],
			'open_time':['', [
          Validators.required
        ]],
			'close_time':['', [
          Validators.required
        ]],
      'website':['', [
          Validators.required
        ]],
      'promotion':[],
      'promo_discount':[],
      'default_currency':['', [
          Validators.required
        ]],
      'rest_type':['', [
          Validators.required
        ]],
      'facebook_link':[],
      'twitter_link':[],
      'linkedIn_link':[],
      'google_plus_link':[]
    });
       this.first_name = this.createProfileForm.controls['first_name'];
       this.last_name = this.createProfileForm.controls['last_name'];
			 this.email = this.createProfileForm.controls['email'];
       this.phone = this.createProfileForm.controls['phone'];
       this.tel = this.createProfileForm.controls['tel'];
			 this.password = this.createProfileForm.controls['password'];
       this.confirm_password = this.createProfileForm.controls['confirm_password'];
			 this.restaurant_name = this.createProfileForm.controls['restaurant_name'];
			 this.restaurant_address = this.createProfileForm.controls['restaurant_address'];
			 this.restaurant_tel_number = this.createProfileForm.controls['restaurant_tel_number'];
			 this.restaurant_mobile_number = this.createProfileForm.controls['restaurant_mobile_number'];
			 this.restaurant_other_email = this.createProfileForm.controls['restaurant_other_email'];
       this.open_time = this.createProfileForm.controls['open_time'];
       this.close_time = this.createProfileForm.controls['close_time'];
       this.website = this.createProfileForm.controls['website'];
       this.promotion = this.createProfileForm.controls['promotion'];
       this.promo_discount = this.createProfileForm.controls['promo_discount'];
       this.default_currency = this.createProfileForm.controls['default_currency'];
       this.facebook_link = this.createProfileForm.controls['facebook_link'];
       this.twitter_link = this.createProfileForm.controls['twitter_link'];
       this.linkedIn_link = this.createProfileForm.controls['linkedIn_link'];
       this.google_plus_link = this.createProfileForm.controls['google_plus_link'];
       this.rest_type = this.createProfileForm.controls['rest_type'];
       this.createProfileForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }
private onValueChanged(data?: any) {
    if (!this.createProfileForm) { return; }
    const form = this.createProfileForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }
formErrors = {
    'first_name': [],
    'last_name':[],
    'email':[],
		'phone':[],
    'tel':[],
    'password':[],
    'confirm_password':[],
    'restaurant_name':[],
    'restaurant_address':[],
    'open_time':[],
    'close_time':[],
    'website':[],
    'default_currency':[],
    'rest_type':[],
  };

  validationMessages = {
    'first_name': {
      'required':      'The name is required.',
      'maxlength':     'The name must be at less 256 characters long.'
    },
     'last_name': {
      'required':      'The last_name is required.',
      'maxlength':     'The last_name must be at less 256 characters long.'
    },
     'email': {
      'required':      'The email is required.',
      'minlength':     'The email must be at least 256 characters long.',
      'invalidEmail':  'The email must be a valid email address.',
    },
     'phone': {
      'required':      'The phone is required.',
      'maxlength':     'The phone must be at less 32 characters long.',
      'invalidNumber': 'he phone must be number'
    },
     'tel': {
      'required':      'The tel is required.',
      'maxlength':     'The tel must be at less 32 characters long.'
    },
    'password': {
      'required':      'The password is required.',
      'minlength':     'The password must be at least 6 characters long.',
      'maxlength':     'The last_name must be at less 256 characters long.'
    },
    'confirm_password': {
      'required':      'The password is required.',
      'minlength':     'The password must be at least 6 characters long.',
      'maxlength':     'The last_name must be at less 256 characters long.'
    },
     'restaurant_name': {
      'required':      'The restaurant_name is required.',
      'maxlength':     'The restaurant_name must be at less 256 characters long.'
    },
     'restaurant_address': {
      'required':      'The restaurant_address is required.',
      'maxlength':     'The restaurant_address must be at less 1024 characters long.'
    },
     'open_time': {
      'required':      'The close_time is required / formatNumber.'
    },
     'close_time': {
      'required':      'The close_time is required / formatNumber.'
    },
     'website': {
      'required':      'The website is required.'
    },
     'default_currency': {
      'required':      'The default_currency is required.'
    },
     'rest_type': {
      'required':      'The required is required.'
    }
  };
	createProfile(){
		// this.open_time = this.open_time.value.toLocaleTimeString([], { hour12: false });
    // this.close_time = this.close_time.value.toLocaleTimeString([], { hour12: false });
    // this.fetchLatLon(this.restaurant_address.value);
    this.onValueChanged();
    for (const field in this.formErrors) {
        // clear previous error message (if any)
         if(this.formErrors[field].length >0){
           return;
         }
      }
	let param = {
		              'address': this.restaurant_address.value
		};
  		this.profileService.getLatlon(param).then(
              response  => { 
              this.lat = response.lat_val;
              this.lon = response.long_val;
              let city = null;
              let street = null;
              let province = null;
              let country = null;
              if(response.city_ln!=undefined){
                city = response.city_ln;
              }
               if(response.street_ln!=undefined){
                street = response.street_ln;
              }
               if(response.province_ln!=undefined){
                province = response.province_ln;
              }
               if(response.country_ln!=undefined){
                country = response.country_ln;
              }

              if(this.lat==null || this.lon==null){
                        swal(
                  'get lat lon no value',
                  this.error[0],
                  'error'
                  ).catch(swal.noop);
              }
              let object = {
                'user_id': this.userId,
                'user_info': {
                  'id': this.userInfo.id,
                  'phone': this.createProfileForm.value.phone,
                  'email': this.createProfileForm.value.email,
                  'first_name': this.createProfileForm.value.first_name,
                  'last_name': this.createProfileForm.value.last_name,
                  'password': this.createProfileForm.value.password,
                  'confirm_password': this.createProfileForm.value.confirm_password,
                  'roles':this.userInfo.roles,
                  'tel': this.createProfileForm.value.tel,
                },
                'restaurant_info':{
                  'id': this.restId,
                  'name': this.createProfileForm.value.restaurant_name,
                  'address': this.createProfileForm.value.restaurant_address,
                  'tel': this.createProfileForm.value.restaurant_tel_number,
                  'mobile': this.createProfileForm.value.restaurant_mobile_number,
                  'email': this.createProfileForm.value.restaurant_other_email,
                  'latitue': this.lat,
                  'longtitue': this.lon,
                  'website': this.createProfileForm.value.website,
                  'logo': '',
                  'open_time': this.open_time.value.toLocaleTimeString([], { hour12: false }),
                  'close_time': this.close_time.value.toLocaleTimeString([], { hour12: false }),
                  'promotion': this.createProfileForm.value.promotion,
                  'rest_type': this.createProfileForm.value.rest_type,
                  'facebook_link': this.createProfileForm.value.facebook_link,
                  'twitter_link': this.createProfileForm.value.twitter_link,
                  'linkedIn_link': this.createProfileForm.value.linkedIn_link,
                  'google_plus_link': this.createProfileForm.value.google_plus_link,
                  'promo_discount':this.createProfileForm.value.promo_discount,
                  'city':city,
                  'province':province,
                  'country':country,
                  'street':street,
                  'ordering_setting':{
                    'payment_fee': this.payment_fee,
                    'delivery_charge': this.delivery_charge,
                    'minimum_order': this.minimum_order,
                    'payment_method': this.payment_method,
                    'delivery_methods': this.delivery_methods,
                    'delivery_time': this.delivery_time,
                    'default_currency': this.createProfileForm.value.default_currency,
                    'food_ordering_system_type': this.food_ordering_system_type,
                  }
          }
		};
  		this.profileService.createProfile(object).then(
                     	 response  => this.processResult(response),
                     error =>  this.failedCreate.bind(error));
  					});
    
	}
  setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
          this.markers[i].setMap(map);
        }
      }
  clearMarkers() {
    this.setMapOnAll(null);
  }
  private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
     let imagePost = '';
     if( this.flgImageChoose == true){
       imagePost = this.imageSrc;
     }
      let profileUpload = {
         'id':response.id,
         'thumb':imagePost
      }
	    this.profileService.profileUploadFile(profileUpload).then(
                     response  => { 
                     	this.notif.success('New File has been added');
                      // this.router.navigateByUrl('');
                      this.clearMarkers();
                      this.userService.logout();
                     },
  					 error => {console.log(error)
            
  					});
    } else {
      this.error = response.errors;
      if (response.code == 422) {
        if (this.error.first_name) {
          this.formErrors['first_name'] = this.error.first_name;
        }
         if (this.error.last_name) {
          this.formErrors['last_name'] = this.error.last_name;
        }
         if (this.error.tel) {
          this.formErrors['tel'] = this.error.tel;
        }
         if (this.error.phone) {
          this.formErrors['phone'] = this.error.phone;
        }
         if (this.error.password) {
          this.formErrors['password'] = this.error.password;
        }
         if (this.error.confirm_password) {
          this.formErrors['confirm_password'] = this.error.confirm_password;
        }
         if (this.error.name) {
          this.formErrors['restaurant_name'] = this.error.name;
        }
        if (this.error.restaurant_address) {
          this.formErrors['address'] = this.error.restaurant_address;
        }
         if (this.error.default_currency) {
          this.formErrors['default_currency'] = this.error.default_currency;
        }
         if (this.error.website) {
          this.formErrors['website'] = this.error.website;
        }
        if (this.error.rest_type) {
          this.formErrors['rest_type'] = this.error.rest_type;
        }

      } else {
        swal(
          'Create Fail!',
          this.error[0],
          'error'
        ).catch(swal.noop);
      }
    }
  }
  private failedCreate (res) {
    if (res.status == 401) {
      // this.loginfailed = true
    } else {
      if (res.data.errors.message[0] == 'Email Unverified') {
        // this.unverified = true
      } else {
        // other kinds of error returned from server
        for (var error in res.data.errors) {
          // this.loginfailederror += res.data.errors[error] + ' '
        }
      }
    }
  }

  initMap() {
  let latmap = this.lat;
  let lonMap = this.lon;
	this.mapObj = new google.maps.Map(document.getElementById('order-map'), {
	  center: {lat: +latmap, lng: +lonMap},
	  zoom: 15
	});
	
	
	//getCurrentPositionBrower(); // not run un localhost 	
	// set markers current Location and store location for test only start
	let idMarker = this.idmarker;
	var positionCurrent = new google.maps.LatLng(+latmap,+lonMap);//(user used web on DB)
	this.markerCurrentLocation = new google.maps.Marker({
      id:idMarker,
			draggable: true,
    	position: positionCurrent,    	
    	map: this.mapObj,
      title: 'Hello World!'
    });		
  this.markers[+idMarker] = this.markerCurrentLocation ;
	// google.maps.event.addListener(this.mapObj, 'click', function(event) {
	//     alert(event.latLng);
	// });
	let positionMarker = null;
	let self = this;
	google.maps.event.addListener(this.markerCurrentLocation, 'dragend', function(event) {
								  // alert(event.latLng +"/"+ this.getPosition());
								positionMarker = this.getPosition();
								self.fetchAddress(this.getPosition().lat(),this.getPosition().lng());
                this.lat=this.getPosition().lat();
                this.long=this.getPosition().lng();
                this.changelonlatFlag=true;
            });

}
SearchMapChange(){
   this.fetchLatLon(this.restaurant_address.value);
}
private fetchAddress(lat,lon) {
			let object = {
		              'lat': lat,
		              'lon': lon,
		};
  		this.profileService.getAddress(object).then(
                     response  => { 
                     	this.addressInfo = response;
                      this.createProfileForm.controls['restaurant_address'].setValue( this.addressInfo.address);
                      this.restaurant_address = this.createProfileForm.controls['restaurant_address'];
                     	console.log(response); 
                     },
  					 error => {console.log(error)
  					});
  	}
    private fetchLatLon(address) {
			let object = {
		              'address': address
		};
  		this.profileService.getLatlon(object).then(
                     response  => { 
                     this.lat = response.lat_val;
                     this.lon = response.long_val;
                     	console.log(response); 
                      let latmap = this.lat;
                      let lonMap = this.lon;
                      var positionCurrent = new google.maps.LatLng(+latmap,+lonMap);//(user used web on DB)
                      let idMarker = +this.idmarker + 1;
                  	  this.markerCurrentLocation = new google.maps.Marker({
                  			draggable: true,
                      	position: positionCurrent,    	
                      	map: this.mapObj,  
                        title: 'Hello World!'
                      });
                  	this.markers[+idMarker] = this.markerCurrentLocation ;
                    this.mapObj.panTo(this.markerCurrentLocation.position);
                    var marker = this.markers[+this.idmarker]; // find the marker by given id
                    this.idmarker = idMarker;
                    marker.setMap(null);
                  	// google.maps.event.addListener(this.mapObj, 'click', function(event) {
                  	//     alert(event.latLng);
                  	// });
                  	let positionMarker = null;
                  	let self = this;
                  	google.maps.event.addListener(this.markerCurrentLocation, 'dragend', function(event) {
								  // alert(event.latLng +"/"+ this.getPosition());
    								positionMarker = this.getPosition();
    								self.fetchAddress(this.getPosition().lat(),this.getPosition().lng());
                    this.lat=this.getPosition().lat();
                    this.long=this.getPosition().lng();
                    this.changelonlatFlag=true;
                    });
                     },
  					 error => {console.log(error)
  					});
  	}

handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
	                      'Error: The Geolocation service failed.' :
	                      'Error: Your browser doesn\'t support geolocation.');
}
chagetab(value){
	if(this.tabChangeFisrt==true){
     setTimeout(() => {
			this.initMap();
       }, 800);
			this.tabChangeFisrt=false;
	}
}

/*select start*/

private value:any = {};
private valueCurrentcy:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;
 
 
  public selected(value:any):void {
    
    console.log('Selected value is: ', value);
  }
 
  public removed(value:any):void {
    console.log('Removed value is: ', value);
    this.createProfileForm.controls['rest_type'].setValue(null);
    this.rest_type = this.createProfileForm.controls['rest_type'];
  }
 public currencyremoved(value:any):void {
    console.log('Removed value is: ', value);
    this.createProfileForm.controls['default_currency'].setValue(null);
    this.default_currency = this.createProfileForm.controls['default_currency'];
  }
  public typed(value:any):void {
    console.log('New search input: ', value);
  }
 
  public refreshValue(value:any):void {
    if(value!=undefined && value!=null && value.length!=0){
      this.value = value;
      this.createProfileForm.controls['rest_type'].setValue(value.id);
    this.rest_type = this.createProfileForm.controls['rest_type'];
    }else{
      this.createProfileForm.controls['rest_type'].setValue(null);
      this.rest_type = this.createProfileForm.controls['rest_type'];
    }
    
  }
  
  public refreshCurrencyValue(value:any):void {
    // this.valueCurrentcy = value;
     if(value!=undefined && value!=null && value.length!=0){
      this.valueCurrentcy = value;
      this.createProfileForm.controls['default_currency'].setValue(value.text);
      this.default_currency = this.createProfileForm.controls['default_currency'];
     }else{
       this.createProfileForm.controls['default_currency'].setValue(null);
       this.default_currency = this.createProfileForm.controls['default_currency'];
     }
    
  }
   public onChangeOpen(value: any): void {
    console.log('Changed value is: ', value);
  }
   public onChangeOpenValue(event: any): void {
    let value1= event.target.value;
    console.log('Changed value is: ', event.target.value);
    if(value1==""|| value1==null){
      this.createProfileForm.controls['open_time'].setValue(null);
       this.open_time=this.createProfileForm.controls['open_time'];
    }
    const nameRe = new RegExp(/^[0-9]+$/i);
    if(!nameRe.test(value1) || value1.length>2){
      this.createProfileForm.controls['open_time'].setValue(null);
       this.open_time=this.createProfileForm.controls['open_time'];
    }
  }
  public onChangeCloseValue(event: any): void {
    let value1= event.target.value;
    console.log('Changed value is: ', event.target.value);
    if(value1==""|| value1==null){
      this.createProfileForm.controls['close_time'].setValue(null);
       this.close_time=this.createProfileForm.controls['close_time'];
    }
    const nameRe = new RegExp(/^[0-9]+$/i);
    if(!nameRe.test(value1) || value1.length>2){
      this.createProfileForm.controls['close_time'].setValue(null);
       this.close_time=this.createProfileForm.controls['close_time'];
    }
    
  }
  public onChangeClose(value1: any): void {
    console.log('Changed value is: ', value1);
  }
   /*upload file*/
	private sactiveColor: string = 'green';
    private baseColor: string = '#ccc';
    private overlayColor: string = 'rgba(255,255,255,0.5)';
    
    private dragging: boolean = false;
    private loaded: boolean = false;
    private imageLoaded: boolean = false;
    private imageSrc: string = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
    private flgImageChoose:boolean = false;
    private iconColor: string = '';
    private  borderColor: string = '';
    private  activeColor: string = '';
    private hiddenImage:boolean=false;
    private categories : any;
    private categoryObject : any;
    private fileChoose : File;
    private error: any;
    handleDragEnter() {
        this.dragging = true;
    }
    
    handleDragLeave() {
        this.dragging = false;
    }
    
    handleDrop(e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    }
    
    handleImageLoad() {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if(this.imageLoaded = true){
			this.hiddenImage = true;
		}
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        this.loaded = false;

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose= true;
    }
    
    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }
    
    _setActive() {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    }
    
    _setInactive() {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    }
    // end upload file
}
