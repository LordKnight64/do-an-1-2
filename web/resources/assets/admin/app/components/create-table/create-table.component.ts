import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from '../../router.animations';
import { routerTransition } from '../../router.animations';
import { TableService } from 'app/services/table.service';
import  { AuthService } from 'app/services/auth.service';
import { default as swal } from 'sweetalert2';
import { NotificationService } from 'app/services/notification.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
import { invalidNumberValidator } from "app/utils/number-validator.directive";

@Component({
  selector: 'create-table',
  templateUrl: Utils.getView('app/components/create-table/create-table.component.html'),
  styleUrls: [Utils.getView('app/components/create-table/create-table.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''
}
})
export class CreateTableComponent implements OnInit {

	private id: string;
  private sub: any;
	private mymodel: any;
	private createTableForm: FormGroup;
	private name: AbstractControl;
  private slots: AbstractControl;
  private description: AbstractControl;
  private nameRadio: AbstractControl;
  private restId : String;
  private isCheckRadio : string;
  private tableItem : any;

  constructor(private fb: FormBuilder,
  private tableService: TableService,
  private StorageService: StorageService,
  private authServ: AuthService,
  private notif: NotificationService,
   private router: Router,private route: ActivatedRoute) {
  }

  ngOnInit() {
     let userInfo = JSON.parse(localStorage.getItem('user_key'));
     this.restId = userInfo.restaurants[0].id;
     this.userId = userInfo.user_info.id;
  	 this.buildForm();
    	this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.isCheckRadio = '1';
      this.createTableForm.controls['nameRadio'].setValue(1);
      if(this.id!=null){
        this.tableItem = this.StorageService.getScope();
        if(this.tableItem == false){
          this.fetchAllTables(this.restId,this.id);
        }else{
            this.createTableForm.controls['name'].setValue(this.tableItem.name);
            this.createTableForm.controls['nameRadio'].setValue(this.tableItem.is_active);
            this.createTableForm.controls['slots'].setValue(this.tableItem.slots);
            this.createTableForm.controls['description'].setValue(this.tableItem.content);

            this.name = this.createTableForm.controls['name'];
            this.nameRadio = this.createTableForm.controls['nameRadio'];
            this.description = this.createTableForm.controls['description'];
            this.slots = this.createTableForm.controls['slots'];
            this.isCheckRadio = this.tableItem.is_active;
            this.imageSrc = this.tableItem.thumb;
            if( this.imageSrc==null){
               this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
            }
        }
      }
    });
  }
  checkFlg(value){
      if(this.isCheckRadio==undefined || this.isCheckRadio==null){
        this.isCheckRadio = '1';
      }
      if(value==this.isCheckRadio){
          return true;
      }
  }
  changenameRadio(value){
    this.isCheckRadio =value;
  }
  private fetchAllTables(restId,id) {
  		this.tableService.getTables(restId,id,null).then(
                     response  => {
                       if(response.tables!=null && response.tables.length>0){
                       	this.tableItem = response.tables[0];
                       	this.createTableForm.controls['name'].setValue(this.tableItem.name);
                         this.createTableForm.controls['nameRadio'].setValue(this.tableItem.is_active);
                         this.createTableForm.controls['description'].setValue(this.tableItem.content);
                         this.createTableForm.controls['slots'].setValue(this.tableItem.slots);

                        this.name = this.createTableForm.controls['name'];
                        this.description = this.createTableForm.controls['description'];
                        this.nameRadio = this.createTableForm.controls['nameRadio'];
                        this.slots = this.createTableForm.controls['slots'];
                        this.isCheckRadio = this.tableItem.is_active;
                        this.imageSrc = this.tableItem.thumb;
                        if( this.imageSrc==null){
                           this.imageSrc = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                        }
                       }else{
                         this.notif.error('table had delete');
                         this.router.navigateByUrl('table-list');
                       }
                     },
  					 error => {console.log(error)
  					});
  	}
private buildForm(): void {
    this.createTableForm = this.fb.group({
      'name': ['', [
          Validators.required,
          Validators.maxLength(256),
        ]
      ],
      'slots': ['', [
          Validators.required,
          Validators.maxLength(2),
          invalidNumberValidator()
        ]
      ],'nameRadio':[],
      'description':[]

    });
       this.name = this.createTableForm.controls['name'];
       this.nameRadio = this.createTableForm.controls['nameRadio'];
       this.description = this.createTableForm.controls['description'];
       this.slots = this.createTableForm.controls['slots'];

    this.createTableForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

   private onValueChanged(data?: any) {
    if (!this.createTableForm) { return; }
    const form = this.createTableForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }
formErrors = {
    'name': []
  };

  validationMessages = {
    'name': {
      'required':      'The name is required.',
      'maxlength':     'The name must be at less 256 characters long.'
    },
    'slots': {
      'required':      'The slot is required.',
      'maxlength':     'The slot must be at less 2 characters long.',
      'invalidNumber': 'The slot must be number'

    }
  };

  /*upload file*/
	private sactiveColor: string = 'green';
    private baseColor: string = '#ccc';
    private overlayColor: string = 'rgba(255,255,255,0.5)';

    private dragging: boolean = false;
    private loaded: boolean = false;
    private imageLoaded: boolean = false;
    private imageSrc: string = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
    private flgImageChoose:boolean = false;
    private iconColor: string = '';
    private  borderColor: string = '';
    private  activeColor: string = '';
    private hiddenImage:boolean=false;
    private tables : any;
    private tableObject : any;
    private userId : String;
    private fileChoose : File;
    private error: any;
    handleDragEnter() {
        this.dragging = true;
    }

    handleDragLeave() {
        this.dragging = false;
    }

    handleDrop(e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    }

    handleImageLoad() {
        this.imageLoaded = true;
        this.iconColor = this.overlayColor;
        if(this.imageLoaded = true){
			this.hiddenImage = true;
		}
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileChoose = file;
        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        this.loaded = false;

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        this.flgImageChoose= true;
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

    _setActive() {
        this.borderColor = this.activeColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.activeColor;
        }
    }

    _setInactive() {
        this.borderColor = this.baseColor;
        if (this.imageSrc.length === 0) {
            this.iconColor = this.baseColor;
        }
    }
createTable(){
  let id  = '-1';
  if(this.id!=null){
    id  = this.id;
  }
  this.onValueChanged();
  for (const field in this.formErrors) {
      // clear previous error message (if any)
       if(this.formErrors[field].length >0){
         return;
       }
    }
  this.tableObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'table':{
              'id':id,
              'name':this.createTableForm.value.name,
              'content':this.createTableForm.value.description,
              'thumb':'',
              'slots':this.createTableForm.value.slots,
              'is_active':this.isCheckRadio,
              'is_delete':'0'
            }};
  	this.tableService.createTable(this.tableObject).then(
            //          response  => {
            //          	this.tables = response;
            //          	console.log(response, this.tables);
            //          },
  					//  error => {console.log(error)
                response  => this.processResult(response),
                     error =>  this.failedCreate.bind(error));
  					// });

}

private processResult(response) {

    if (response.message == undefined || response.message == 'OK') {
     let imagePost = '';
     if( this.flgImageChoose == true){
       imagePost = this.imageSrc;
     }
      this.tables = response;
      let tableUpload = {
         'id':this.tables.table_id,
         'thumb':imagePost
      }
	    this.tableService.tableUploadFile(tableUpload).then(
                     response  => {
                     	this.notif.success('New table has been added');
                      this.router.navigateByUrl('table-list');
                     },
  					 error => {console.log(error)

  					});
    } else {
      this.error = response.errors;
      if (response.code == 422) {
        if (this.error.name) {
          this.formErrors['name'] = this.error.name;
        }
      } else {
        swal(
          'Create Fail!',
          this.error[0],
          'error'
        ).catch(swal.noop);
      }
    }
  }
  private failedCreate (res) {
    if (res.status == 401) {
      // this.loginfailed = true
    } else {
      if (res.data.errors.message[0] == 'Email Unverified') {
        // this.unverified = true
      } else {
        // other kinds of error returned from server
        for (var error in res.data.errors) {
          // this.loginfailederror += res.data.errors[error] + ' '
        }
      }
    }
  }

  goBack(): void {
  this.router.navigate(['/table-list'], { relativeTo: this.route });
  }
}
