import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { routerTransition } from '../../router.animations';
import { RestaurantService } from 'app/services/restaurant.service';
import { NotificationService } from 'app/services/notification.service';
import { UserService } from "app/services/user.service";
import { EmailService } from 'app/services/email.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SubscribeListModalComponent } from 'app/components/subscribe-list-modal/subscribe-list-modal.component';
import { SubscribeService } from 'app/services/subscribe.service';
 @Component({
  selector: 'table-list',
  templateUrl: Utils.getView('app/components/send-email-marketing/send-email-marketing.component.html'),
  styleUrls: [Utils.getView('app/components/send-email-marketing/send-email-marketing.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class SendEmailMarketingComponent implements OnInit {
  private mymodel;
  private restId;
  private userId;
  private dtForm: FormGroup;
  public sendMarketingMail:FormGroup
  private subject:AbstractControl = null;
  private sendto:AbstractControl = null;
  private sendbcc:AbstractControl = null;
  private sendcc:AbstractControl = null;
  private content: AbstractControl = null;
  @ViewChild('childModal') private mailModal: SubscribeListModalComponent;
  constructor(
    private restServ: RestaurantService,
    private fb: FormBuilder,
    private userServ: UserService,
    private notifyServ: NotificationService,
    private sendMailSer: EmailService,
    private router: Router,
    private subscribeServ: SubscribeService
  ) { }

  ngOnInit() {
    //alert("OK");
    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }

    this.buildForm();
    this.mymodel = {
      content: null,
      sendbcc: null,
      sendto: null,
      sendcc: null,
      subject: null
    }; 
   
  } 
  
  openModal(object)
  {
    var params = {
    };
    this.subscribeServ.getSubscribeList(params).then(response => {
      console.log('response ', response.subscribe);
      this.mailModal.newsList = response.subscribe;
      this.mailModal.choose = object;
      this.mailModal.show(this);
    }, error => {
      console.log(error);
    });
    
  }

  sendEmail(){
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }
    console.log("SEND MAIL STARTED");
    var params = {
      'send_to': this.sendMarketingMail.value.sendto,     
      'send_bb': this.sendMarketingMail.value.sendbcc,
      'send_cc': this.sendMarketingMail.value.sendcc,
      'subject': this.sendMarketingMail.value.subject,
      'content': this.sendMarketingMail.value.content,
      'user_id': this.userId,
      'rest_id': this.restId
    };
    console.log(params);
    
    this.sendMailSer.sendEmailMarketingList(params).then(response => {
      this.notifyServ.success('Mail has been sent');
      this.router.navigateByUrl('email-marketing-list');
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      console.log("SENT");
    }, error => {
      console.log(error);
    });
  }

  public buildForm(): void {

    this.sendMarketingMail = this.fb.group({
      'content': ['', [Validators.required, Validators.maxLength(1536)]],
      'subject':['', [Validators.required, Validators.maxLength(1536)]],
      'sendbcc':['',[Validators.maxLength(1536)]],
      'sendcc':['',[Validators.maxLength(1536)]],
      'sendto':['', [Validators.required]]     
    });
    this.subject = this.sendMarketingMail.controls['subject'];
    this.sendto = this.sendMarketingMail.controls['sendto'];
    this.sendbcc = this.sendMarketingMail.controls['sendbcc'];
    this.sendcc= this.sendMarketingMail.controls['sendcc'];
    this.content = this.sendMarketingMail.controls['content'];
  
    this.sendMarketingMail.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    
  }

  public setVar(emailAddresses): void {
    
     this.mymodel = {
      content: emailAddresses.content,
      sendbcc: emailAddresses.sendBCC,
      sendto: emailAddresses.sendTo,
      sendcc: emailAddresses.sendCC,
      subject: emailAddresses.subject
    }; 
    
    
    if (this.mymodel != null) {
     
      this.sendMarketingMail.controls['content'].setValue(this.mymodel.content);
      this.sendMarketingMail.controls['sendbcc'].setValue(this.mymodel.sendbcc);
      this.sendMarketingMail.controls['sendto'].setValue(this.mymodel.sendto);
      this.sendMarketingMail.controls['sendcc'].setValue(this.mymodel.sendcc);
      this.sendMarketingMail.controls['subject'].setValue(this.mymodel.subject);
       
      this.subject = this.sendMarketingMail.controls['subject'];
      this.sendto = this.sendMarketingMail.controls['sendto'];
      this.sendbcc = this.sendMarketingMail.controls['sendbcc'];
      this.sendcc= this.sendMarketingMail.controls['sendcc'];
      this.content = this.sendMarketingMail.controls['content'];
    
    }

  }

  private onValueChanged(unDirty, data?: any) {
    
    if (!this.sendMarketingMail) { return; }
    const form1 = this.sendMarketingMail;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form1.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'content': [],
    'subject':[],
    'sendbcc':[],
    'sendcc':[],
    'sendto':[]     
  };

  validationMessages = {
    'subject': {
      'required': 'The subject field is required.',
      'maxlength': 'The title may not be greater than 1536 characters.'
    },
    'content': {
      'required': 'The subject field is required.',
      'maxlength': 'The title may not be greater than 1536 characters.'
    },
    'sendto':{
      'required': 'The Send to field is required.'
    },
    'sendbcc': {
      'maxlength': 'The title may not be greater than 1536 characters.'
    },
    'sendcc': {
      'maxlength': 'The title may not be greater than 1536 characters.'
    }
  };

   
  private sendFeedback() {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }

    var params = {
      'user_id': this.userId,     
      'content': this.content.value    
    };
    

    this.restServ.sendFeeback(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      this.notifyServ.success('your feedback has been sent');
    }, error => {
      console.log(error);
    });
  }

}
