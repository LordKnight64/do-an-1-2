import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { RestaurantService } from 'app/services/restaurant.service';
import { NotificationService } from 'app/services/notification.service';
import { UserService } from "app/services/user.service";

@Component({
  selector: 'contents',
  templateUrl: Utils.getView('app/components/contents/contents.component.html'),
  styleUrls: [Utils.getView('app/components/contents/contents.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class ContentsComponent implements OnInit {

  private mymodel;
  private restId;
  private userId;
  private dtForm: FormGroup;
  private aboutUs: AbstractControl = null;
  private privatePolicy: AbstractControl = null;
  private termOfUse: AbstractControl = null;

  constructor(
    private restServ: RestaurantService,
    private fb: FormBuilder,
    private userServ: UserService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }

    // this.userServ.currentUser.subscribe((user) => {
    //   console.log(user);
    //   if (!user.email) {
    //     console.log('user 1', user);
    //   }
    // });

    this.buildForm();
    this.mymodel = {
      aboutUs: null,
      privatePolicy: null,
      termOfUse: null
    };

    var params = {
      'rest_id': this.restId
    };
    this.restServ.getContents(params).then(response => {
      this.mymodel.aboutUs = response.about_us;
      this.mymodel.privatePolicy = response.private_policy;
      this.mymodel.termOfUse = response.term_of_use;
      this.setVar();
    }, error => {
      console.log(error);
    });
    console.log('init contents component');
  }

  private setVar(): void {

    if (this.mymodel != null) {
      this.mymodel = {
        aboutUs: this.mymodel.aboutUs,
        privatePolicy: this.mymodel.privatePolicy,
        termOfUse: this.mymodel.termOfUse
      };
      this.dtForm.controls['aboutUs'].setValue(this.mymodel.aboutUs);
      this.dtForm.controls['privatePolicy'].setValue(this.mymodel.privatePolicy);
      this.dtForm.controls['termOfUse'].setValue(this.mymodel.termOfUse);
      this.aboutUs = this.dtForm.controls['aboutUs'];
      this.privatePolicy = this.dtForm.controls['privatePolicy'];
      this.termOfUse = this.dtForm.controls['termOfUse'];
    }
  }

  private buildForm(): void {

    this.dtForm = this.fb.group({
      'aboutUs': ['', [Validators.maxLength(1024)]],
      'privatePolicy': ['', [Validators.maxLength(1024)]],
      'termOfUse': ['', [Validators.maxLength(1024)]]
    });
    this.aboutUs = this.dtForm.controls['aboutUs'];
    this.privatePolicy = this.dtForm.controls['privatePolicy'];
    this.termOfUse = this.dtForm.controls['termOfUse'];
    this.dtForm.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    //this.onValueChanged(); // (re)set validation messages now
  }

  private onValueChanged(unDirty, data?: any) {

    if (!this.dtForm) { return; }
    const form1 = this.dtForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form1.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'aboutUs': [],
    'privatePolicy': [],
    'termOfUse': []
  };

  validationMessages = {
    'aboutUs': {
      'maxlength': 'The about us may not be greater than 1024 characters.'
    },
    'privatePolicy': {
      'maxlength': 'The private policy may not be greater than 1024 characters.'
    },
    'termOfUse': {
      'maxlength': 'The term of use may not be greater than 1024 characters.'
    }
  };

  private addAboutUs() {
    this.saveContent('About us');
  }

  private addPrivatePolicy() {
    this.saveContent('Private policy');
  }

  private addTermOfUse() {
    this.saveContent('Term of use');
  }

  private saveContent(msg) {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }

    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'about_us': this.aboutUs.value,
      'private_policy': this.privatePolicy.value,
      'term_of_use': this.termOfUse.value
    };

    this.restServ.updateContents(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      this.notifyServ.success(msg + ' has been updated');
    }, error => {
      console.log(error);
    });
  }
}
