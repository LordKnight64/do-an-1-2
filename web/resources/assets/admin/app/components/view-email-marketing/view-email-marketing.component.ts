import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { EmailService } from 'app/services/email.service';
import { Router, ActivatedRoute } from '@angular/router';

 @Component({
  selector: 'table-list',
  templateUrl: Utils.getView('app/components/view-email-marketing/view-email-marketing.component.html'),
  styleUrls: [Utils.getView('app/components/view-email-marketing/view-email-marketing.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ViewEmailMarketingComponent implements OnInit {
   private id: number = 1;
  private userId;
  private email: any = {};
  private data: any;
  private currentPage: number = 1;
  private totalItems: number = 0;
  private itemsPerPage: number = 10;
  private maxSize: number = 10;

  constructor(
    private emailServ: EmailService,
    private userServ: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private storageServ: StorageService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

   
    this.route.params.subscribe(params => {
      this.id = params['id']; 
      
       let userKey = JSON.parse(localStorage.getItem('user_key'));
        if (!userKey) {
          this.userServ.logout();
        } 
        this.doSearch();
    });

  }

  private doSearch() {
   
   
    var params = {
      'id' : this.id
    };
    this.emailServ.getEmailMarketingDetail(params).then(response => {
        this.email = response.email[0];
        console.log( this.email);
    }, error => {
      console.log(error);
    });
  }



  private doDeleteSearch() {
   
    let id = 1;
    var params = {
      'id': id
    };
    var me = this;
    this.emailServ.deleteEmailMarketingList(params).then(response => {
      if(response.return_cd == 0) {
        me.notifyServ.success('Search Items has been deleted');
        me.doSearch();
      }
    }, error => {
      console.log(error);
    });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }

  private doBack() {
      this.router.navigateByUrl('/email-marketing-list');
  }

}
