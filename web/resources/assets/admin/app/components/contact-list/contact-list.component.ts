import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { ContactService } from 'app/services/contact.service';

@Component({
  selector: 'contact-list',
  templateUrl: Utils.getView('app/components/contact-list/contact-list.component.html'),
  styleUrls: [Utils.getView('app/components/contact-list/contact-list.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class ContactListComponent implements OnInit {

  private restId;
  private userId;
  private contactList: any;
  private data: any;
  private currentPage: number = 1;
  private totalItems: number;
  private itemsPerPage: number = 10;
  private maxSize: number = 10;

  constructor(
    private contactServ: ContactService,
    private userServ: UserService,
    private storageServ: StorageService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }

    this.doSearch();
  }

  private doSearch() {

    var params = {
      'rest_id': this.restId
    };
    this.contactServ.getContactList(params).then(response => {
      this.contactList = response.contacts;
      this.totalItems = this.contactList.length;
      this.pageChanged({ page: this.currentPage, itemsPerPage: this.itemsPerPage });
    }, error => {
      console.log(error);
    });
  }

  public pageChanged(event: any): void {
    let start = (event.page - 1) * event.itemsPerPage;
    let end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.contactList.length;
    this.data = this.contactList.slice(start, end);
  }

  private doDeleteContact(obj) {
    obj.is_delete = 1;
    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'contact': obj
    };
    this.contactServ.updateContact(params).then(response => {
      this.doSearch();
    }, error => {
      console.log(error);
    });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }
}
