import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectComponent } from 'ng2-select';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { OrderService } from 'app/services/order.service';
import { ComCodeService } from 'app/services/comCode.service';
import {Observable} from "rxjs/Observable";
import { NotificationService } from 'app/services/notification.service';
@Component({
  selector: 'orders-list',
  templateUrl: Utils.getView('app/components/orders-list/orders-list.component.html'),
  styleUrls: [Utils.getView('app/components/orders-list/orders-list.component.css')]
})
export class OrdersListComponent implements OnInit {

  private restId;
  private userId;
  private userIdSearch;
  private orderList: any;
  private requestOrderList: any;
  private requestOrderTempList: any;
  private requestOrderDelivery: any;
  private requestOrderPickup: any;
  private verifiedOrderList: any;
  private verifiedOrderTempList: any;
  private verifiedOrderDelivery: any;
  private verifiedOrderPickup: any;
  private pendingOrderList: any;
  private pendingOrderTempList: any;
  private pendingOrderDelivery: any;
  private pendingOrderPickup: any;
  private deliveryOrderList: any;
  private deliveryOrderTempList: any;
  private deliveryOrderDelivery: any;
  private deliveryOrderPickup: any;
  private order_sts: any = null;
  private fromDate: any = null;
  private toDate: any = null;
  private disabled: boolean = false;
  private paymentMethodItems: Array<any> = [];
  private subscription:any;
  private deliveryMethod: any;
   private order:any = {};
   private isShow:any = true;
  @ViewChild('StatusSelect') private statusSelect: SelectComponent;
  @ViewChild('OrderPersionSelect') private orderPersionSelect: SelectComponent;
  constructor(
    private userServ: UserService,
    private router: Router,
    private orderServ: OrderService,
    private storageServ: StorageService,
    	private notifyServ: NotificationService,
    private comCodeServ: ComCodeService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }
    this.paymentMethodItems.push({ id: 1, text: `Me` });
    this.paymentMethodItems.push({ id: 2, text: `Other` });
    this.orderPersionSelect.items = this.paymentMethodItems;
    if (this.router.url.indexOf('/my-orders') == 0) {
      this.disabled = true;
      this.userIdSearch = "1";
      this.orderPersionSelect.active = [{ id: 1, text: `Me` }];
    }
    this.comCodeServ.getCode({ 'cd_group': '5' }).then(response => {
      this.statusSelect.items = response.code.map(function (x) {
        return { id: x.cd.toString(), text: x.cd_name };
      });
    }, error => {
      console.log(error);
    });
    this.doSearch(true);

    // long polling
   this.subscription = Observable.interval(3000)
      .map((x) => x+1)
      .subscribe((x) => {
       this.doSearch(false);
    });

  }

  getDeliveryMethod() {
    this.comCodeServ.getCode({ 'cd_group': '0' }).then(response => {
				console.log('response ', response); 
				let codes = response.code;
				for(let i = 0; i < codes.length; i++) {
					if(this.order.delivery_type == +codes[i].cd ) {
						this.deliveryMethod = codes[i].cd_name;
						console.log(this.deliveryMethod );
					}
				}
	
			}, error => {
				console.log(error);
			});
  }

   private viewDetail(row) {
   
    this.order = row;
    this.getDeliveryMethod();
      
  }

  	private doUpdateOrder(order, orderStatus) {

		var params = {
			'user_id': this.userId,
			'order_id': order.id,
			'status': orderStatus
		};
		this.orderServ.updateOrder(params).then(response => {
			this.notifyServ.success('Order has been updated');
			this.router.navigateByUrl('order/list');
			//this.goBack();
		}, error => {
			console.log(error);
		});
	}

  orderFood() {
    	this.router.navigateByUrl('order-food');
  }

  ngOnDestroy(){

    this.subscription.unsubscribe();
  }

  private doSearch(isSelect) {

    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'order_sts': this.order_sts,
      'delivery_ts_to': this.fromDate,
      'delivery_ts_from': this.toDate,
      'user_id_search': this.userIdSearch
    };
    this.orderServ.getOrderList(params).then(response => {
      this.orderList = response.orders;
      this.requestOrderList = this.orderList.filter(x => x.order_sts == 0);
      this.requestOrderDelivery = this.requestOrderList.filter(x => x.delivery_type == 0);
      this.requestOrderPickup = this.requestOrderList.filter(x => x.delivery_type == 1);
      this.verifiedOrderList = this.orderList.filter(x => x.order_sts == 1);
      this.verifiedOrderDelivery = this.verifiedOrderList.filter(x => x.delivery_type == 0);
      this.verifiedOrderPickup = this.verifiedOrderList.filter(x => x.delivery_type == 1);
      this.pendingOrderList = this.orderList.filter(x => x.order_sts == 99);
      this.pendingOrderDelivery = this.pendingOrderList.filter(x => x.delivery_type == 0);
      this.pendingOrderPickup = this.pendingOrderList.filter(x => x.delivery_type == 1);
      this.deliveryOrderList = this.orderList.filter(x => x.order_sts == 2);
      this.deliveryOrderDelivery = this.deliveryOrderList.filter(x => x.delivery_type == 0);
      this.deliveryOrderPickup = this.deliveryOrderList.filter(x => x.delivery_type == 1);
      this.requestOrderTempList = this.requestOrderList;
      this.verifiedOrderTempList = this.verifiedOrderList;
      this.pendingOrderTempList = this.pendingOrderList;
      this.deliveryOrderTempList = this.deliveryOrderList
      if(isSelect == true) this.order =  this.requestOrderTempList[0];
      this.getDeliveryMethod();
      if(this.requestOrderList.length != 0 ) this.isShow = true;
      else this.isShow = false;
    }, error => {
      console.log(error);
    });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }

  private doModelDisplay(orderType, deliveryType) {
    //this.storageServ.setScope(obj);
    if (orderType == 0) {
      if (deliveryType == 0) {
        this.requestOrderList = this.requestOrderTempList;
      } else if (deliveryType == 1) {
        this.requestOrderList = this.requestOrderDelivery;
      } else if (deliveryType == 2) {
        this.requestOrderList = this.requestOrderPickup;
      }
    } else if (orderType == 1) {
      if (deliveryType == 0) {
        this.verifiedOrderList = this.verifiedOrderTempList;
      } else if (deliveryType == 1) {
        this.verifiedOrderList = this.verifiedOrderDelivery;
      } else if (deliveryType == 2) {
        this.verifiedOrderList = this.verifiedOrderPickup;
      }
    } else if (orderType == 2) {
      if (deliveryType == 0) {
        this.deliveryOrderList = this.deliveryOrderTempList;
      } else if (deliveryType == 1) {
        this.deliveryOrderList = this.deliveryOrderDelivery;
      } else if (deliveryType == 2) {
        this.deliveryOrderList = this.deliveryOrderPickup;
      }
    } else if (orderType == 99) {
      if (deliveryType == 0) {
        this.pendingOrderList = this.pendingOrderTempList;
      } else if (deliveryType == 1) {
        this.pendingOrderList = this.pendingOrderDelivery;
      } else if (deliveryType == 2) {
        this.pendingOrderList = this.pendingOrderPickup;
      }
    }
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  public refreshValue(value: any): void {
    this.order_sts = value.id;
    this.doSearch(false);
    console.log('Refresh value is: ', value);
  }

  public onSelectFromDate(value: any): void {
    this.doSearch(false);
    console.log('New search input: ', value);
  }

  public onSelectToDate(value: any): void {
    this.doSearch(false);
    console.log('New search input: ', value);
  }

  public refreshValuePersion(value: any): void {
    this.userIdSearch = value.id;
    this.doSearch(false);
    console.log('Refresh value is: ', value);
  }
}
