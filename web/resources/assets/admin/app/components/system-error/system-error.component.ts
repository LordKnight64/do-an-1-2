import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';

@Component({
  selector: 'system-error',
  // templateUrl: './system-error.component.html',
  // styleUrls: ['./system-error.component.css']
  styleUrls: [Utils.getView('app/components/system-error/system-error.component.css')],
  templateUrl: Utils.getView('app/components/system-error/system-error.component.html')
})
export class SystemErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
