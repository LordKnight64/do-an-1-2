import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { Message } from 'app/models/message';
import { MessagesService } from 'app/services/messages.service';
import { AuthService } from 'app/services/auth.service';
import { User } from 'app/models/user';
import Utils from 'app/utils/utils';
import { routerFade } from '../../router.animations';
import moment = require('moment');

@Component({
  selector: 'app-home',
  styleUrls: [Utils.getView('app/components/home/home.component.css')],
  templateUrl: Utils.getView('app/components/home/home.component.html'),
  animations: [routerFade()],
  host: {'[@routerFade]': ''},
})
export class HomeComponent implements OnInit, OnDestroy {
  // public date: Date = new Date();
  public dt: Date = new Date();
  public minDate: Date = void 0;
  public events: any[];
  public tomorrow: Date;
  public afterTomorrow: Date;
  public dateDisabled: {date: Date, mode: string}[];
  public formats: string[] = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY',
    'shortDate'];
  public format: string = this.formats[0];
  public dateOptions: any = {
    formatYear: 'YY',
    startingDay: 1
  };
  private opened: boolean = false;
  
  constructor(
    private msgServ: MessagesService,
    private breadServ: BreadcrumbService,
    private authServ: AuthService
  ) {
    // TODO
    (this.minDate = new Date()).setDate(this.minDate.getDate() - 1000);
    (this.tomorrow = new Date()).setDate(this.tomorrow.getDate() + 1);
  }

  public ngOnInit() {
    // setttings the header for the home
    this.authServ.getToken().then(
       response => this.processResult(response),
       error => this.handleError(error)
    );
  }

  public ngOnDestroy() {
    // removing the header
    this.breadServ.clear();
  }

  private processResult(res){
    this.breadServ.set({
      description: 'HomePage',
      display: true,
      header: 'Home',
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
          title: 'Home',
          state: 'app'
        }
      ]
    });

    // defining some test users
    let user1 = new User( {
        avatarUrl: 'public/assets/img/user2-160x160.jpg',
        email: 'weber.antoine.pro@gmail.com',
        firstname: 'WEBER',
        lastname: 'Antoine'
    });
    let user2 = new User( {
        avatarUrl: 'public/assets/img/user2-160x160.jpg',
        email: 'EMAIL',
        firstname: 'FIRSTNAME',
        lastname: 'LASTNAME'
    });
    // sending a test message
    // this.msgServ.addMessage( new Message( {
    //     author: user2,
    //     content: 'le contenu d\'un message d\'une importance extreme',
    //     destination: user1,
    //     title: 'un message super important'
    // }) );
  }

  private handleError(error){
    console.log(error);
  }

  public today(): void {
    this.dt = new Date();
  }

  public d20090824(): void {
    this.dt = moment('2009-08-24', 'YYYY-MM-DD')
      .toDate();
  }

  public clear(): void {
    this.dt = void 0;
    this.dateDisabled = undefined;
  }

  public toggleMin(): void {
    this.dt = new Date(this.minDate.valueOf());
  }

  public disableTomorrow(): void {
    this.dateDisabled = [{date: this.tomorrow, mode: 'day'}];
  }

}
