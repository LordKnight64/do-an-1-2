import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
// import { Location } from '@angular/common';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { NewsService } from 'app/services/news.service';

@Component({
  selector: 'news-item',
  templateUrl: Utils.getView('app/components/news-item/news-item.component.html'),
  styleUrls: [Utils.getView('app/components/news-item/news-item.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class NewsItemComponent implements OnInit {

  private mymodel;
  private restId;
  private userId;
  private action;
  private dtForm: FormGroup;
  private title: AbstractControl;
  private description: AbstractControl = null;
  private is_active: AbstractControl = null;
  private imageSrcInit: any;
  private imageSrc: string = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';

  constructor(
    private newsServ: NewsService,
    private userServ: UserService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storageServ: StorageService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }
    this.imageSrcInit = this.imageSrc;
    this.buildForm();
    this.mymodel = {
      id: null,
      title: null,
      description: null,
      is_active: null,
      thumb: null,
      is_delete: 0,
      slug: null
    };

    if (this.router.url.indexOf('/news/edit') == 0) {
      this.action = ['Edit', 'UPDATE', 'updated'];
      if (this.storageServ.scope) {
        this.mymodel = this.storageServ.scope;
        this.setVar();
      } else {
        var params = {
          'id': this.activatedRoute.snapshot.params['id']
        };
        this.newsServ.getNews(params).then(response => {
          this.mymodel = response.news;
          this.setVar();
        }, error => {
          console.log(error);
        });
      }
    }
    if (this.router.url.indexOf('/news/create') == 0) {
      this.action = ['Add', 'ADD', 'added'];
      this.storageServ.setScope(null);
      this.dtForm.controls['is_active'].setValue('1');
    }
    console.log('init news item component');
  }

  // doToBack() {
  //   this.location.back();
  // }

  private setVar(): void {

    // subscribe to router event
    // this.activatedRoute.params.subscribe((params: Params) => {
    //   if (params['id'] != null) {
    //     //this.mymodel = params;
    //     this.mymodel = {
    //       id : params['id'],
    //       title : params['title'],
    //       description : params['description'],
    //       is_active : params['is_active'],
    //       thumb : params['thumb'],
    //       is_delete : 0,
    //       slug : params['title']
    //     };
    //     this.title = params['title'];
    //     this.description = params['description'];
    //     this.is_active = params['is_active'];
    //     this.imageSrc = params['thumb'];
    //   }
    // });

    if (this.mymodel != null) {
      this.mymodel = {
        id: this.mymodel.id,
        title: this.mymodel.title,
        description: this.mymodel.description,
        is_active: this.mymodel.is_active.toString(),
        thumb: this.mymodel.thumb,
        is_delete: 0,
        slug: this.mymodel.title
      };
      this.dtForm.controls['title'].setValue(this.mymodel.title);
      this.dtForm.controls['description'].setValue(this.mymodel.description);
      this.dtForm.controls['is_active'].setValue(this.mymodel.is_active);
      this.title = this.dtForm.controls['title'];
      this.description = this.dtForm.controls['description'];
      this.is_active = this.dtForm.controls['is_active'];
      if (this.mymodel.thumb) {
        this.imageSrc = this.mymodel.thumb;
      }
    }
  }

  private buildForm(): void {

    this.dtForm = this.fb.group({
      'title': ['', [Validators.required, Validators.maxLength(1536)]],
      'description': [],
      'is_active': []
    });
    this.title = this.dtForm.controls['title'];
    this.description = this.dtForm.controls['description'];
    this.is_active = this.dtForm.controls['is_active'];
    this.dtForm.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    //this.onValueChanged(); // (re)set validation messages now
  }

  private onValueChanged(unDirty, data?: any) {

    if (!this.dtForm) { return; }
    const form = this.dtForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'title': []
  };

  validationMessages = {
    'title': {
      'required': 'The title field is required.',
      'maxlength': 'The title may not be greater than 1536 characters.'
    }
  };

  /*upload file*/
  private sactiveColor: string = 'green';
  private baseColor: string = '#ccc';
  private overlayColor: string = 'rgba(255,255,255,0.5)';
  private dragging: boolean = false;
  private loaded: boolean = false;
  private imageLoaded: boolean = false;
  private iconColor: string = '';
  private borderColor: string = '';
  private activeColor: string = '';
  private hiddenImage: boolean = false;

  handleDragEnter() {
    this.dragging = true;
  }

  handleDragLeave() {
    this.dragging = false;
  }

  handleDrop(e) {
    e.preventDefault();
    this.dragging = false;
    this.handleInputChange(e);
  }

  handleImageLoad() {
    this.imageLoaded = true;
    this.iconColor = this.overlayColor;
    if (this.imageLoaded = true) {
      this.hiddenImage = true;
    }
  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

    var pattern = /image-*/;
    var reader = new FileReader();

    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }

    this.loaded = false;

    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    var reader = e.target;
    this.imageSrc = reader.result;
    this.loaded = true;
  }

  _setActive() {
    this.borderColor = this.activeColor;
    if (this.imageSrc.length === 0) {
      this.iconColor = this.activeColor;
    }
  }

  _setInactive() {
    this.borderColor = this.baseColor;
    if (this.imageSrc.length === 0) {
      this.iconColor = this.baseColor;
    }
  }

  private doSaveNews() {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }

    //dto
    if (this.action[0] === 'Add') {
      this.mymodel.id = -1;
    }
    this.mymodel.title = this.title.value;
    this.mymodel.slug = this.title.value;
    this.mymodel.description = this.description.value;
    this.mymodel.is_active = this.is_active.value;
    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'news': this.mymodel
    };
    this.newsServ.updateNews(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      } else if (this.imageSrcInit !== this.imageSrc && this.mymodel.thumb !== this.imageSrc) {
        this.mymodel.id = response.news.id;
        this.mymodel.thumb = this.imageSrc;
        this.newsServ.uploadFile(this.mymodel).then(response => {
          this.notifyServ.success('News has been ' + this.action[2]);
          this.router.navigateByUrl('news');
        }, error => {
          console.log(error);
        });
      } else {
        this.notifyServ.success('News has been ' + this.action[2]);
        this.router.navigateByUrl('news');
      }
    }, error => {
      console.log(error);
    });
  };
}
