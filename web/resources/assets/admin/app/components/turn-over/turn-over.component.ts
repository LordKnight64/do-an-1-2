import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { RestaurantService } from 'app/services/restaurant.service';
import { NotificationService } from 'app/services/notification.service';
import { UserService } from "app/services/user.service";
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import  { AuthService } from 'app/services/auth.service';
import { StorageService } from 'app/services/storage.service';
import {TurnOverService} from 'app/services/turnover.service';
 import {SelectModule,SelectComponent} from 'ng2-select';
 @Component({
  selector: 'turn-over',
  templateUrl: Utils.getView('app/components/turn-over/turn-over.component.html'),
  styleUrls: [Utils.getView('app/components/turn-over/turn-over.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class TurnOverComponent implements OnInit {

  @ViewChild('SelectId') public select: SelectComponent;

  private token : any;
  private userId : String;
  private restId : String;
  private selectedYear: any;
  private turnOverPlaning: any[];
  private turnOvers : any[];
  private mymodel;
  private turnOverByMonth: any[] = [];

  private dtForm: FormGroup;
  private january: AbstractControl;
  private february: AbstractControl = null;
  private march: AbstractControl = null;
  private april: AbstractControl = null;
  private may: AbstractControl = null;
  private june: AbstractControl = null;
  private july: AbstractControl = null;
  private august: AbstractControl = null;
  private september:AbstractControl = null;
  private october:AbstractControl = null;
  private november:AbstractControl = null;
  private december:AbstractControl = null;
  private items: any[];
  private value:any = {};
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartLabels:string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [], label: 'Planing'},
    {data: [], label: 'Actual'}
  ];
 
 
 public lineChartData:Array<any> = [
    {data: [], label: 'Planing'},
    {data: [], label: 'Actual'}
   
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public lineChartOptions:any = {
    responsive: true
  };
 
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  constructor(
  private turnOverService: TurnOverService,
  private authServ: AuthService,
  private notifyService: NotificationService,
  private router: Router,
  private fb: FormBuilder,
  private StorageService: StorageService) { }  

  ngOnInit() { 
   
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
     this.fetchAllYears();
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
    let currentYear = (new Date()).getFullYear();
    this.selectedYear = currentYear;
    this.fetchTurnOverPlaning(this.restId, currentYear);
    this.fetchTurnOver(this.restId, currentYear);
   
     this.buildForm();
     this.mymodel = {
      january: 0,
      february:0,
      march: 0,
      april: 0,
      may: 0,
      june: 0,
      july: 0,
      august: 0,
      september: 0,
      october: 0,
      november: 0,
      december: 0
    };
   
  } 

   private setVar(): void {

    if (this.mymodel != null) {
      this.mymodel = {
        january: this.mymodel.january,
        february: this.mymodel.february,
        march: this.mymodel.march,
        april: this.mymodel.april,
        may: this.mymodel.may,
        june: this.mymodel.june,
        july: this.mymodel.july,
        august: this.mymodel.august,
        september: this.mymodel.september,
        october: this.mymodel.october,
        november: this.mymodel.november,
        december: this.mymodel.december
      };
      this.dtForm.controls['january'].setValue(this.mymodel.january);
      this.dtForm.controls['february'].setValue(this.mymodel.february);
      this.dtForm.controls['march'].setValue(this.mymodel.march);
      this.dtForm.controls['april'].setValue(this.mymodel.april);
      this.dtForm.controls['may'].setValue(this.mymodel.may);
      this.dtForm.controls['june'].setValue(this.mymodel.june);

      this.dtForm.controls['july'].setValue(this.mymodel.july);
      this.dtForm.controls['august'].setValue(this.mymodel.august);
      this.dtForm.controls['september'].setValue(this.mymodel.september);
      this.dtForm.controls['october'].setValue(this.mymodel.october);
      this.dtForm.controls['november'].setValue(this.mymodel.november);
      this.dtForm.controls['december'].setValue(this.mymodel.december);
      
      this.january = this.dtForm.controls['january'];
      this.february = this.dtForm.controls['february'];
      this.march = this.dtForm.controls['march'];
      this.april = this.dtForm.controls['april'];
      this.may = this.dtForm.controls['may'];
      this.june = this.dtForm.controls['june'];
      this.july = this.dtForm.controls['july'];
      this.august = this.dtForm.controls['august'];
      this.september = this.dtForm.controls['september'];
      this.october = this.dtForm.controls['october'];
      this.november = this.dtForm.controls['november'];
      this.december = this.dtForm.controls['december'];
      
    }
  }

  private buildForm(): void {

    this.dtForm = this.fb.group({
      'january': ['', [Validators.required, Validators.maxLength(11)]],
      'february': ['', [Validators.required, Validators.maxLength(11)]],
      'march': ['', [Validators.required, Validators.maxLength(11)]],
      'april': ['', [Validators.required, Validators.maxLength(11)]],
      'may': ['', [Validators.required, Validators.maxLength(11)]],
      'june': ['', [Validators.required, Validators.maxLength(11)]],
      'july': ['', [Validators.required, Validators.maxLength(11)]],
      'august': ['', [Validators.required, Validators.maxLength(11)]],
      'september': ['', [Validators.required, Validators.maxLength(11)]],
      'october': ['', [Validators.required, Validators.maxLength(11)]],
      'november': ['', [Validators.required, Validators.maxLength(11)]],
      'december': ['', [Validators.required, Validators.maxLength(11)]]
    });

      this.january = this.dtForm.controls['january'];
      this.february = this.dtForm.controls['february'];
      this.march = this.dtForm.controls['march'];
      this.april = this.dtForm.controls['april'];
      this.may = this.dtForm.controls['may'];
      this.june = this.dtForm.controls['june'];
      this.july = this.dtForm.controls['july'];
      this.august = this.dtForm.controls['august'];
      this.september = this.dtForm.controls['september'];
      this.october = this.dtForm.controls['october'];
      this.november = this.dtForm.controls['november'];
      this.december = this.dtForm.controls['december'];

      this.dtForm.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    //this.onValueChanged(); // (re)set validation messages now
  }

  private onValueChanged(unDirty, data?: any) {

    if (!this.dtForm) { return; }
    const form = this.dtForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'january': [],
    'february': [],
    'march': [],
    'april': [],
    'may': [],
    'june': [],
    'july': [],
    'august': [],
    'september': [],
    'october': [],
    'november': [],
    'december': []

  };

  validationMessages = {
    'january': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'february': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'march': {
     'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'april': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'may': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'june': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'july': {
       'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'august': {
     'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
    'september': {
       'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
     'october': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
     'november': {
      'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    },
     'december': {
       'required': 'The turnover planing field is required.',
      'maxlength': 'The turnover planing may not be greater than 11 characters.',
      'pattern': 'The turnover planing must be a number.'
    }

  };

  private doSaveTurnOverPlaning() {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }
    this.mymodel.january = this.january.value;
    this.mymodel.february = this.february.value;
    this.mymodel.march = this.march.value;
    this.mymodel.april = this.april.value;
    this.mymodel.may = this.may.value;
    this.mymodel.june = this.june.value;
    this.mymodel.july = this.july.value;
    this.mymodel.august = this.august.value;
    this.mymodel.september = this.september.value;
    this.mymodel.october = this.october.value;
    this.mymodel.november = this.november.value;
    this.mymodel.december = this.december.value;

    var params = {
      'year_no': this.selectedYear,
      'rest_id': this.restId,
      'turnover_planing': JSON.stringify(this.mymodel),
      'user_id': this.userId
    };

  this.turnOverService.updateTurnoverPlaning(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      this.notifyService.success('Turn over has been updated');
       this.fetchTurnOverPlaning(this.restId, this.selectedYear);
      this.fetchTurnOver(this.restId, this.selectedYear);
    }, error => {
      console.log(error);
    });

  };

  private fetchTurnOverPlaning(restId, year) {
      let param = {
        'rest_id': restId,
        'year_no': year
      };
      let self = this;  
  		this.turnOverService.getTurnoverPlaning(param).then(
                     response  => {
                  
                       if(response.turn_over_planing.length == 0) {
                        
                         this.mymodel =  {
                                            january: 0,
                                            february:0,
                                            march: 0,
                                            april: 0,
                                            may: 0,
                                            june: 0,
                                            july: 0,
                                            august: 0,
                                            september: 0,
                                            october: 0,
                                            november: 0,
                                            december: 0
                                          };
                            this.turnOverPlaning = this.mymodel;
                           console.log('response.turn_over_planing ',this.mymodel);
                       }
                       else {
                           this.turnOverPlaning = JSON.parse( response.turn_over_planing[0].turn_over_planing);
                      	   console.log('getTurnoverPlaning ',this.turnOverPlaning);
                           this.mymodel = this.turnOverPlaning;
                       }
                     
                        this.setVar();
                        this.updatePlaningChart();
                      
                     },
  					 error => {  alert('ok fd'); console.log(error)
  					});
  	}

    private updatePlaningChart() {
       let clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [+ this.mymodel.january,
                        +this.mymodel.march,
                        +this.mymodel.april,
                        +this.mymodel.april,
                        +this.mymodel.may,
                        +this.mymodel.june,
                        +this.mymodel.july,
                        +this.mymodel.august,
                        +this.mymodel.september,
                        +this.mymodel.october,
                        +this.mymodel.november,
                        +this.mymodel.december];
                  
        this.barChartData = clone;
      
       let clone1 = JSON.parse(JSON.stringify(this.lineChartData));
        clone1[0].data = [+ this.mymodel.january,
                        +this.mymodel.march,
                        +this.mymodel.april,
                        +this.mymodel.april,
                        +this.mymodel.may,
                        +this.mymodel.june,
                        +this.mymodel.july,
                        +this.mymodel.august,
                        +this.mymodel.september,
                        +this.mymodel.october,
                        +this.mymodel.november,
                        +this.mymodel.december];
                  
        this.lineChartData = clone1;

    }

    private updateActualChart() {

        let clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[1].data = this.turnOverByMonth;
        this.barChartData = clone;

         let clone1 = JSON.parse(JSON.stringify(this.lineChartData));
        clone1[1].data = this.turnOverByMonth;
        this.lineChartData = clone1;

    }

    private fetchTurnOver(restId, year) {
      let param = {
        'rest_id': restId,
        'year_no': year
      };
  		this.turnOverService.getTurnover(param).then(
                     response  => {
                    // 	console.log('getTurnover ',response);
                      this.turnOvers  = response.turnover;
                    //  console.log('this.turnOvers ',this.turnOvers);
                      this.calcTurnOverByMonth();
                     },
  					 error => {console.log(error)
  					});
  	}

   private fetchAllYears() {
        this.items = [];
       
        for (var i = 1; i <  15; i++) {
              this.items.push({
                id: i,
                text: 2016 + i
              });
        }
       this.select.items = this.items; 
       this.value = this.items[0];  
  	}


  public selected(value:any):void {
    this.fetchTurnOverPlaning(this.restId, Number(value.text));
    this.fetchTurnOver(this.restId,  Number(value.text));
   // console.log(value.text);
    this.selectedYear = value.text;
    this.value =value;
  }
 
  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }
 
  public typed(value:any):void {
    console.log('New search input: ', value);
  }

  private calcTurnOverByMonth() {
      this.turnOverByMonth = [];
      for(var i = 0; i < this.turnOvers.length; i++) {
          let turnOver: any[] = this.turnOvers[i];
          let total = 0;
          for(var j = 0; j < turnOver.length; j++)
          {
              total += Number(turnOver[j].price);
          }
          
          this.turnOverByMonth.push(total);
      }
     
      this.updateActualChart();
       
    }

  

  
 
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
 
 
 
 
 
 
 
}
