import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { User } from 'app/models/user';
import { UserService } from 'app/services/user.service';
import { ServerService } from 'app/services/server.service';
import { routerGrow } from '../../router.animations';


import Utils from 'app/utils/utils';
import moment = require('moment')
import { AuthService } from "app/services/auth.service";
import { invalidEmailValidator } from "app/utils/email-validator.directive";
import { default as swal } from 'sweetalert2';
import { SelectComponent } from 'ng2-select';
import { TranslateService } from 'ng2-translate';
import { ENVIRONMENT } from 'environments/environment';
import { ModalModule , ModalDirective} from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { SubscribeService } from 'app/services/subscribe.service';
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { SendEmailMarketingComponent } from 'app/components/send-email-marketing/send-email-marketing.component';

@Component({
  selector: 'subscribe-list-modal',
  styleUrls: [Utils.getView('app/components/subscribe-list-modal/subscribe-list-modal.component.css')],
  templateUrl: Utils.getView('app/components/subscribe-list-modal/subscribe-list-modal.component.html'),
  animations: [routerGrow()],
  host: {'[@routerGrow]': ''}

})
export class SubscribeListModalComponent implements OnInit {

  @ViewChild('childModal') public childModal:ModalDirective;
  @Input() title?:string;
  private restId;
  private userId;
  public choose;
  public newsList: any = [];
  private data: any = [];
  private stringmail;
  private currentPage: number = 1;
  private totalItems: number;
  private itemsPerPage: number = 10;
  private maxSize: number = 10;
 private sendMailCmp: SendEmailMarketingComponent
  constructor(
    private subscribeServ: SubscribeService,
    private userServ: UserService,
    private storageServ: StorageService,
    private notifyServ: NotificationService
   
  ) { }
    
  
  public ngOnInit() {
    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } 
    this.newsList = this.data = [];
    this.stringmail = "";
    this.doSearch();
  }
  
  show(sendMailCmp){
    this.sendMailCmp = sendMailCmp;
    this.childModal.show();
  }
  hide(){
    this.childModal.hide();
  }

  private doSearch() {
      console.log(this.newsList);
      this.totalItems = this.newsList.length;
      this.pageChanged({ page: this.currentPage, itemsPerPage: this.itemsPerPage });
  }

  public pageChanged(event: any): void {
    let start = (event.page - 1) * event.itemsPerPage;
    let end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
    this.data = this.newsList.slice(start, end);
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }
  
  private addEmail(obj) {

    let mailAddresses = {
      'sendTo' :$("#sendto").val(),
      'sendCC' :  $("#sendcc").val(),
      'sendBCC' :$("#sendbcc").val(),
      'subject':$("#subject").val(),
      'content':$("#content").val()
    }
    //alert("OK");
    let email = obj.email;
    console.log(this.stringmail);
    obj.added = true;
    if(this.choose == 1)
    {
      this.stringmail = $("#sendto").val() + email +";";
      mailAddresses.sendTo = this.stringmail;
    }
    if(this.choose == 2)
    {
      this.stringmail = $("#sendcc").val() + email +";";
       mailAddresses.sendCC = this.stringmail;
    }
    if(this.choose == 3)
    {
      this.stringmail = $("#sendbcc").val() + email +";";
       mailAddresses.sendBCC = this.stringmail;
    }

    this.sendMailCmp.setVar(mailAddresses);
  }
  

}
