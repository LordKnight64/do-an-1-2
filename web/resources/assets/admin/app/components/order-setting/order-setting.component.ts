import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectComponent } from 'ng2-select';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { NotificationService } from 'app/services/notification.service';
import { RestaurantService } from 'app/services/restaurant.service';
import { UserService } from "app/services/user.service";
import { ComCodeService } from 'app/services/comCode.service';
import { invalidDecimalValidator } from "app/utils/decimal-validator.directive";
import { TranslateService } from 'ng2-translate';
@Component({
  selector: 'order-setting',
  templateUrl: Utils.getView('app/components/order-setting/order-setting.component.html'),
  styleUrls: [Utils.getView('app/components/order-setting/order-setting.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class OrderSettingComponent implements OnInit {

  private mymodel;
  private restId;
  private userId;
  private dtForm: FormGroup;
  private payment_fee: AbstractControl;
  private delivery_charge: AbstractControl = null;
  private minimum_order: AbstractControl = null;
  private payment_method: AbstractControl = null;
  private delivery_methods: AbstractControl = null;
  private delivery_time: AbstractControl = null;
  private default_currency: AbstractControl = null;
  private food_ordering_system_type: AbstractControl = null;
  private unit_point:AbstractControl = null;
  // private items: Array<string> = ['VND', 'USD', 'EUR', 'GBP',
  //   'INR', 'AUR', 'CAD', 'SGD', 'JPY'];
  // private paymentMethodItems: Array<any> = [
  //   { id: '1', text: 'Cash' },
  //   { id: '2', text: 'ATM' },
  //   { id: '3', text: 'Credit Card' }
  // ];
  // private deliveryMethodItems: Array<any> = [
  //   { id: '1', text: 'Home' },
  //   { id: '2', text: 'TakeAway' }
  // ];
  @ViewChild('Select') private select: SelectComponent;
  @ViewChild('PaymentMethodSelect') private paymentMethodSelect: SelectComponent;
  @ViewChild('DeliveryMethodSelect') private deliveryMethodSelect: SelectComponent;
  constructor(
    private restServ: RestaurantService,
    private userServ: UserService,
    private fb: FormBuilder,
    private notifyServ: NotificationService,
    private comCodeServ: ComCodeService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    if(localStorage.getItem('langChoosen')!=undefined && localStorage.getItem('langChoosen')!=null){
      this.translateService.setDefaultLang(localStorage.getItem('langChoosen'));
      this.translateService.use(localStorage.getItem('langChoosen'));
    }
    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } else {
      this.userId = userKey.user_info.id;
      this.restId = userKey.restaurants[0].id;
    }
    this.comCodeServ.getCode({ 'cd_group': '0' }).then(response => {
      this.deliveryMethodSelect.items = response.code.map(function (x) {
        return { id: x.cd.toString(), text: x.cd_name };
      });
    }, error => {
      console.log(error);
    });
    this.comCodeServ.getCode({ 'cd_group': '1' }).then(response => {
      this.paymentMethodSelect.items = response.code.map(function (x) {
        return { id: x.cd.toString(), text: x.cd_name };
      });
    }, error => {
      console.log(error);
    });
    this.comCodeServ.getCode({ 'cd_group': '2' }).then(response => {
      this.select.items = response.code.map(function (x) {
        return { id: x.cd.toString(), text: x.cd_name };
      });
    }, error => {
      console.log(error);
    });
    this.buildForm();
    this.mymodel = {
      payment_fee: null,
      delivery_charge: null,
      minimum_order: null,
      payment_method: null,
      delivery_methods: null,
      delivery_time: null,
      default_currency: null,
      unit_point: null,
      food_ordering_system_type: null
    };
    var params = {
      'rest_id': this.restId
    };
    this.restServ.getOrderSetting(params).then(response => {
      this.mymodel = response.ordering_setting;
      this.setVar();
    }, error => {
      console.log(error);
    });
    console.log('init order setting component');
  }

  private setVar(): void {

    if (this.mymodel != null) {
      this.mymodel = {
        payment_fee: this.mymodel.payment_fee,
        delivery_charge: this.mymodel.delivery_charge,
        minimum_order: this.mymodel.minimum_order,
        payment_method: this.mymodel.payment_method,
        delivery_methods: this.mymodel.delivery_methods,
        delivery_time: this.mymodel.delivery_time,
        default_currency: this.mymodel.default_currency,
        unit_point: this.mymodel.unit_point,
        food_ordering_system_type: this.mymodel.food_ordering_system_type
      };
      this.dtForm.controls['payment_fee'].setValue(this.mymodel.payment_fee);
      this.dtForm.controls['delivery_charge'].setValue(this.mymodel.delivery_charge);
      this.dtForm.controls['minimum_order'].setValue(this.mymodel.minimum_order);
      this.dtForm.controls['payment_method'].setValue(this.mymodel.payment_method);
      this.dtForm.controls['delivery_methods'].setValue(this.mymodel.delivery_methods);
      this.dtForm.controls['unit_point'].setValue(this.mymodel.unit_point);
      let tg = this.mymodel.delivery_time.match(/(\d+)\:(\d+)\:(\d+)/);
      let ng = new Date();
      ng.setHours(tg[1]);
      ng.setMinutes(tg[2]);
      ng.setSeconds(tg[3]);
      this.dtForm.controls['delivery_time'].setValue(ng);
      this.dtForm.controls['default_currency'].setValue(this.mymodel.default_currency);
      this.dtForm.controls['food_ordering_system_type'].setValue(this.mymodel.food_ordering_system_type);
      this.payment_fee = this.dtForm.controls['payment_fee'];
      this.delivery_charge = this.dtForm.controls['delivery_charge'];
      this.minimum_order = this.dtForm.controls['minimum_order'];
      this.payment_method = this.dtForm.controls['payment_method'];
      this.delivery_methods = this.dtForm.controls['delivery_methods'];
      this.delivery_time = this.dtForm.controls['delivery_time'];
      this.default_currency = this.dtForm.controls['default_currency'];
      this.unit_point = this.dtForm.controls['unit_point'];
      this.food_ordering_system_type = this.dtForm.controls['food_ordering_system_type'];

      this.select.active = this.select.itemObjects.filter((x) => {
        if (this.mymodel.default_currency === x.text) {
          return true;
        }
      });
      this.paymentMethodSelect.active = this.paymentMethodSelect.itemObjects.filter((x) => {
        if (this.mymodel.payment_method.split(',').filter((y) => y === x.id).length > 0) {
          return true;
        }
      });
      this.deliveryMethodSelect.active = this.deliveryMethodSelect.itemObjects.filter((x) => {
        if (this.mymodel.delivery_methods.split(',').filter((y) => y === x.id).length > 0) {
          return true;
        }
      });
    }
  }

  private buildForm(): void {

    this.dtForm = this.fb.group({
      'payment_fee': ['', [Validators.required, Validators.maxLength(256)]],
      'delivery_charge': ['', [Validators.required, Validators.maxLength(256)]],
      'minimum_order': ['', [Validators.required, Validators.maxLength(20), invalidDecimalValidator()]],
      'payment_method': ['', [Validators.required, Validators.maxLength(20)]],
      'delivery_methods': ['', [Validators.required, Validators.maxLength(20)]],
      'delivery_time': ['', [Validators.required]],
      'default_currency': ['', [Validators.required, Validators.maxLength(4)]],
      'food_ordering_system_type': ['', [Validators.maxLength(4)]],
      'unit_point': ['', [Validators.maxLength(11)]],
    });
    this.payment_fee = this.dtForm.controls['payment_fee'];
    this.delivery_charge = this.dtForm.controls['delivery_charge'];
    this.minimum_order = this.dtForm.controls['minimum_order'];
    this.payment_method = this.dtForm.controls['payment_method'];
    this.delivery_methods = this.dtForm.controls['delivery_methods'];
    this.delivery_time = this.dtForm.controls['delivery_time'];
    this.default_currency = this.dtForm.controls['default_currency'];
    this.food_ordering_system_type = this.dtForm.controls['food_ordering_system_type'];
     this.unit_point = this.dtForm.controls['unit_point'];
    this.dtForm.valueChanges
      .subscribe(data => this.onValueChanged(false, data));
    //this.onValueChanged(); // (re)set validation messages now
  }

  private onValueChanged(unDirty, data?: any) {

    if (!this.dtForm) { return; }
    const form = this.dtForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);
      if (control && (control.dirty || unDirty) && control.invalid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'payment_fee': [],
    'delivery_charge': [],
    'minimum_order': [],
    'payment_method': [],
    'delivery_methods': [],
    'delivery_time': [],
    'default_currency': [],
    'food_ordering_system_type': [],
    'unit_point': []

  };

  validationMessages = {
    'payment_fee': {
      'required': 'The payment fee field is required.',
      'maxlength': 'The payment fee may not be greater than 256 characters.',
      'pattern': 'The payment fee must be a number.'
    },
    'delivery_charge': {
      'required': 'The delivery charge field is required.',
      'maxlength': 'The delivery charge may not be greater than 256 characters.',
      'pattern': 'The delivery charge must be a number.'
    },
    'minimum_order': {
      'required': 'The minimum order field is required.',
      'maxlength': 'The payment fee may not be greater than 20 characters.',
      'pattern': 'The minimum order must be a number.',
      'invalidDecimal': 'The order may not decimal ( 15 inerger , 4 decimal )'
    },
    'payment_method': {
      'required': 'The payment method field is required.',
      'maxlength': 'The payment method may not be greater than 4 characters.'
    },
    'delivery_methods': {
      'required': 'The delivery methods field is required.',
      'maxlength': 'The delivery methods may not be greater than 4 characters.'
    },
    'delivery_time': {
      'required': 'The delivery time field is required / formatNumber.'
    },
    'default_currency': {
      'required': 'The default currency field is required.',
      'maxlength': 'The default currency may not be greater than 4 characters.'
    },
    'food_ordering_system_type': {
      'maxlength': 'The food ordering system type may not be greater than 4 characters.'
    },
    'unit_point': {
      'maxlength': 'The food ordering system type may not be greater than 11 characters.'
    }

  };

  private doSaveOrderSetting() {
    this.onValueChanged(true);
    for (const field in this.formErrors) {
      if (this.formErrors[field].length > 0) {
        return;
      }
    }
    this.mymodel.payment_fee = this.payment_fee.value;
    this.mymodel.delivery_charge = this.delivery_charge.value;
    this.mymodel.minimum_order = this.minimum_order.value;
    this.mymodel.payment_method = this.payment_method.value;
    this.mymodel.delivery_methods = this.delivery_methods.value;
    this.mymodel.delivery_time = this.delivery_time.value.toLocaleTimeString([], { hour12: false });
    this.mymodel.default_currency = this.default_currency.value;
    this.mymodel.food_ordering_system_type = this.food_ordering_system_type.value;
     this.mymodel.unit_point = this.unit_point.value;
    var params = {
      'user_id': this.userId,
      'rest_id': this.restId,
      'order_setting': this.mymodel
    };
    this.restServ.updateOrderSetting(params).then(response => {
      let harError = false;
      for (const field in response.errors) {
        harError = true;
        this.formErrors[field] = response.errors[field];
      }
      if (harError) {
        return;
      }
      this.notifyServ.success('Contact has been updated');
    }, error => {
      console.log(error);
    });
  };

  public onChange(value: any): void {
    console.log('Changed value is: ', value);
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  public refreshValue(value: any): void {
    this.dtForm.controls['default_currency'].setValue(value.text);
    this.default_currency = this.dtForm.controls['default_currency'];
  }

  public refreshPaymentMethodValue(value: any): void {
    this.dtForm.controls['payment_method'].setValue(value.map(function (elem) { return elem.id; }).join(','));
    this.payment_method = this.dtForm.controls['payment_method'];
  }

  public refreshDeliveryMethodValue(value: any): void {
    this.dtForm.controls['delivery_methods'].setValue(value.map(function (elem) { return elem.id; }).join(','));
    this.delivery_methods = this.dtForm.controls['delivery_methods'];
  }
   public onChangeDeliveryValue(event: any): void {
    let value1= event.target.value;
    console.log('Changed value is: ', event.target.value);
    if(value1==""|| value1==null){
      this.dtForm.controls['delivery_time'].setValue(null);
       this.delivery_time=this.dtForm.controls['delivery_time'];
    }
    const nameRe = new RegExp(/^[0-9]+$/i);
    if(!nameRe.test(value1) || value1.length>2){
      this.dtForm.controls['delivery_time'].setValue(null);
       this.delivery_time=this.dtForm.controls['delivery_time'];
    }
  }
}