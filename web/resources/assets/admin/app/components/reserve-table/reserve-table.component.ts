import { Component, OnInit, ViewChild, ViewEncapsulation  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl } from '@angular/forms';
import Utils from 'app/utils/utils';
import { RestaurantService } from 'app/services/restaurant.service';
import { NotificationService } from 'app/services/notification.service';
import { UserService } from "app/services/user.service";
import { routerTransition } from '../../router.animations';
import {Observable} from 'rxjs/Observable';
import {SelectModule, SelectComponent} from 'ng2-select';
import { AreaService } from 'app/services/area.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { invalidNumberValidator } from "app/utils/number-validator.directive";
import { invalidEmailValidator } from "app/utils/email-validator.directive";
import { ReserveService } from 'app/services/reserve.service';

import { TableService } from 'app/services/table.service';
import { default as swal } from 'sweetalert2';

@Component({
  selector: 'reserve-table',
  templateUrl: Utils.getView('app/components/reserve-table/reserve-table.component.html'),
  styleUrls: [Utils.getView('app/components/reserve-table/reserve-table.component.css')],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class ReserveTableComponent implements OnInit {
  @ViewChild('SelectAreasId') public selectAreas: SelectComponent;
  @ViewChild('SelectTablesId') public selectTables: SelectComponent;

  private mymodel;
  private restId;
  private userId;
  private open_time: AbstractControl;
  private close_time: AbstractControl;
  private phone: AbstractControl;
  private email: AbstractControl;
  private date: AbstractControl;
  private name: AbstractControl;
  private address: AbstractControl;
  private people_count: AbstractControl;
  private comment: AbstractControl;




  private minDate: Date;
  private areaItems: Array<any> = [];
  private tableItems: Array<any> = [];
  private tableReserve: Array<any> = [];
  private tablesRemoved: Array<any> = [];

  private areas: any;
  private tables: any;
  private valueArea: any = {};
  private valueTable: any = {};
  private slots: any;

  private createReserveForm: FormGroup;
  private content: AbstractControl = null;
  private Settings: any;
  private stadiumList: any[];
  private timeList: any[];
  private fromDate: any = null;

  constructor(
    private areaService: AreaService,
    private reserveService: ReserveService,
    private restServ: RestaurantService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private tableService: TableService,
    private userServ: UserService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
    this.buildForm();
    this.minDate = new Date();
    this.fetchAllAreasAndTables(this.restId);

  }

  private fetchAllAreasAndTables(restId) {
    this.areaService.getAreas(restId, null).then(
      response => {
        this.areas = response.areas;

        for (var i = 0; i < this.areas.length; i++) {
          this.areaItems.push({
            id: this.areas[i].id,
            text: `${this.areas[i].name}`
          });
        }
        this.selectAreas.items = this.areaItems;
        this.valueArea = '';
        this.fetchAllTables(restId, this.valueArea.id);
        console.log(response.areas);
      },
      error => {
        console.log(error)
      });
  }

  private fetchAllTables(restId, area_id) {
    this.tableService.getTables(restId, null, null).then(
      response => {
        this.tables = response.tables;
        this.tableItems = [];

        for (var i = 0; i < this.tables.length; i++) {
          if (this.tables[i].area_id == area_id) {
            this.tableItems.push({
              id: this.tables[i].id,
              text: `${this.tables[i].name}`
            });
          }
        }
        this.slots = 0;
        this.fetchTableByArea(null);
        this.valueTable = '';
        console.log(response.areas);
      },
      error => {
        console.log(error)
      });

  }

  public fetchTableByArea(area_id) {
    this.tableItems = [];
    if (area_id != null) {
      for (var i = 0; i < this.tables.length; i++) {
        if (this.tables[i].area_id == area_id) {
          this.tableItems.push({
            id: this.tables[i].id,
            text: `${this.tables[i].name}`
          });
        }
      }
    }
    else {
      for (var i = 0; i < this.tables.length; i++) {
        this.tableItems.push({
          id: this.tables[i].id,
          text: `${this.tables[i].name}`
        });
      }
    }
    this.selectTables.items = this.tableItems;
    this.valueTable = this.tableItems[0];
  }


  public selectedAreas(value: any): void {

    console.log('Selected value is: ', value);
    let areaId = null;
    if (this.valueArea.id != undefined && this.valueArea.id != null && this.valueArea.id != 0) {
      areaId = this.valueArea.id;
      this.valueTable = '';
    }
  }

  public selectedTables(value: any): void {
    console.log('Selected value is: ', value);
    let tableId = null;
    if (this.valueTable.id != undefined && this.valueTable.id != null && this.valueTable.id != 0) {
      tableId = this.valueTable.id;
    }
    let tableAreaId = null;
    let areaItem;
    for (let i = 0; i < this.tables.length; i++) {
      if (value.id == this.tables[i].id) {
        console.log(this.tables[i].id);
        this.slots = this.tables[i].slots;
        for (let j = 0; j < this.areaItems.length; j++) {
          if (this.areaItems[j].id == this.tables[i].area_id) {
            areaItem = {
              id: this.areaItems[j].id,
              text: `${this.areaItems[j].text}`
            }
          }
        }
        this.valueArea = areaItem;
        return;
      }
    }
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
    this.slots = 0;
    this.valueArea = '';
    this.valueTable = '';
  }

  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  public refreshAreaValue(value: any): void {
    if (value != undefined && value != null && value.length != 0) {
      this.valueArea = value;
      let areaId = null;
      if (this.valueArea.id != undefined && this.valueArea.id != null && this.valueArea.id != 0) {
        areaId = this.valueArea.id;
      }
      this.fetchTableByArea(areaId);
    } else {
      this.fetchTableByArea(null);
    }
  }

  public refreshTableValue(value: any): void {
    if (value != undefined && value != null && value.length != 0) {
      this.valueTable = value;
    }
  }

  public onChangeOpen(value: any): void {
    console.log('Changed value is: ', value);
  }
  public onChangeOpenValue(event: any): void {
    let value1 = event.target.value;
    console.log('Changed value is: ', event.target.value);
    if (value1 == "" || value1 == null) {
      this.createReserveForm.controls['open_time'].setValue(null);
      this.open_time = this.createReserveForm.controls['open_time'];
    }
    const nameRe = new RegExp(/^[0-9]+$/i);
    if (!nameRe.test(value1) || value1.length > 2) {
      this.createReserveForm.controls['open_time'].setValue(null);
      this.open_time = this.createReserveForm.controls['open_time'];
    }
  }
  public onChangeCloseValue(event: any): void {
    let value1 = event.target.value;
    console.log('Changed value is: ', event.target.value);
    if (value1 == "" || value1 == null) {
      this.createReserveForm.controls['close_time'].setValue(null);
      this.close_time = this.createReserveForm.controls['close_time'];
    }
    const nameRe = new RegExp(/^[0-9]+$/i);
    if (!nameRe.test(value1) || value1.length > 2) {
      this.createReserveForm.controls['close_time'].setValue(null);
      this.close_time = this.createReserveForm.controls['close_time'];
    }

  }
  public onChangeClose(value1: any): void {
    console.log('Changed value is: ', value1);
  }

  public onSelectFromDate(value: any): void {

    console.log('New search input: ', value);
  }

  public check() {
    console.log(this.close_time.value);
    console.log(this.open_time.value);
    console.log(this.date.value);


  }

  public chooseSpaceTime() {
    alert('click');
  }

  private buildForm(): void {
    this.createReserveForm = this.fb.group({

      'open_time': ['', [
        Validators.required
      ]],
      'close_time': ['', [
        Validators.required
      ]],
      'date': ['', [
        Validators.required
      ]],
      'name': ['', [
        Validators.required,
        Validators.maxLength(256),
      ]],
      'phone': ['', [
        Validators.required,
        Validators.maxLength(32),
        invalidNumberValidator()

      ]],
      'email': ['', [
        Validators.maxLength(256),
        invalidEmailValidator()
      ]],
      'address': ['', [
        Validators.maxLength(256),
      ]],
      'comment': ['', [
        Validators.maxLength(256),
      ]],
      'people_count': ['', [
        Validators.required,
        Validators.maxLength(99),
        invalidNumberValidator()
      ]],
    });
    let ng = new Date();
    let ng1 = new Date();

    this.createReserveForm.controls['open_time'].setValue(ng);
    this.open_time = this.createReserveForm.controls['open_time'];
    this.createReserveForm.controls['close_time'].setValue(ng1);
    this.close_time = this.createReserveForm.controls['close_time'];
    this.phone = this.createReserveForm.controls['phone'];
    this.email = this.createReserveForm.controls['email'];
    this.address = this.createReserveForm.controls['address'];
    this.people_count = this.createReserveForm.controls['people_count'];
    this.date = this.createReserveForm.controls['date'];
    this.name = this.createReserveForm.controls['name'];
    this.comment = this.createReserveForm.controls['comment'];

    this.createReserveForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now

  }

  private onValueChanged(data?: any) {
    if (!this.createReserveForm) { return; }
    const form = this.createReserveForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = [];
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  formErrors = {
    'name': [],
  	'phone': [],
    'email': [],
    'date': [],
    'open_time': [],
    'close_time': [],
    'reserve_date': [],
    'address': [],
    'people_count': [],
    'comment': []
  };

  validationMessages = {
    'name': {
      'required': 'The name is required.',
      'maxlength': 'The name must be at less 256 characters long.'
    },
    'email': {
      'minlength': 'The email must be at least 256 characters long.',
      'invalidEmail': 'The email must be a valid email address.',
    },
    'phone': {
      'required': 'The phone is required.',
      'maxlength': 'The phone must be at less 32 characters long.',
      'invalidNumber': 'The phone must be number'
    },
    'address': {
      'required': 'The address is required.',
      'maxlength': 'The address must be at less 256 characters long.'
    },
    'date': {
      'required': 'The date is required.'
    },
    'open_time': {
      'required': 'The open time is required.'
    },
    'close_time': {
      'required': 'The close time is required.'
    },
    'people_count': {
      'required': 'The the number of people is required.',
      'maxlength': 'The number of people must be at less 2 characters long.',
      'invalidNumber': 'The phone must be number'
    },
    'comment': {
      'required': 'The comment is required.',
      'maxlength': 'The comment must be at less 256 characters long.'
    },
  };

  private addToReserve() {
    if (this.valueArea == null || this.valueTable == null || this.slots == 0
      || this.valueArea == '' || this.valueTable == '' || this.slots == '') {

    }
    else {
      for (var v of this.tables) {
        if (v.id == this.valueTable.id) {
          this.tables.splice(this.tables.indexOf(v), 1);
          this.tablesRemoved.push(v);
        }
      }
      let itemReserve =
        {
          area_id: this.valueArea.id,
          textArea: this.valueArea.text,
          id: this.valueTable.id,
          name: this.valueTable.text,
          slots: this.slots
        }

      this.tableReserve.unshift(itemReserve);
      this.valueArea = '';
      this.slots = 0;
      this.fetchTableByArea(null);
      this.valueTable = '';
    }
  }

  private removeTable(item) {
    console.log(item);
    this.tableReserve.splice(this.tableReserve.indexOf(item), 1);
    this.tables.unshift(item);
    this.fetchTableByArea(null);
    this.valueArea = '';
    this.valueTable = '';
  }

  private createReserve() {
    this.onValueChanged();
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      if (this.formErrors[field].length > 0) {
        swal("Please check all the information again!","","error");
        return;
      }

      if (!this.open_time.value)
      {
        this.formErrors.open_time.push("The open time is required.");
      }
      if (!this.close_time.value)
      {
        this.formErrors.close_time.push("The close time is required.");
      }
      if (this.name.value=="")
      {
        this.formErrors.name.push("The name is required.");
      }
      if (this.phone.value=="")
      {
        this.formErrors.phone.push("The phone is required.");
      }
      if (this.people_count.value=="")
      {
        this.formErrors.people_count.push("The number of person is required.");
      }
      if (this.date.value=="")
      {
        this.formErrors.date.push("The date is required.");
      }

      if (!this.open_time.value || !this.close_time.value
        || this.name.value=="" || this.phone.value==""
        || this.people_count.value==""
        || this.date.value==""
      ) {
        swal("Please fill all the blank!","","error");
        return;
      }


    }
    console.log("name: " + this.name.value);
    console.log("phone: " + this.phone.value);
    console.log("email: " + this.email.value);
    console.log("address: " + this.address.value);
    console.log("people_count: " + this.people_count.value);
    console.log("date: " + this.date.value);
    console.log("comment: " + this.comment.value);
    console.log("open_time: " + this.open_time.value.toLocaleTimeString([], { hour12: false }));
    console.log("close_time: " + this.close_time.value.toLocaleTimeString([], { hour12: false }));
    console.log(this.tableReserve);
    var sum = 0;
    var param = {
      'user':{
        'name': this.name.value,
        'phone': this.phone.value,
        'email': this.email.value,
        'address': this.address.value,
        'people_count': this.people_count.value,
        'date': this.date.value,
        'comment': this.comment.value,
        'open_time': this.open_time.value.toLocaleTimeString([], { hour12: false }),
        'close_time': this.close_time.value.toLocaleTimeString([], { hour12: false }),
      },
      'table': this.tableReserve
    }
    var that = this;
    for (var v of this.tableReserve) {
      sum += Number(v.slots);
    }
    if(sum< Number(this.people_count.value))
    {
      swal({
        title: 'Bạn có chắc không ?',
        text: "Số người tham dự nhiều hơn số bàn đặt!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText:'Không',
        confirmButtonText: 'Chấp nhận!'
      }).then(function () {
        swal(
          'Thành công !',
          'Bàn của bạn đã được đặt thành công.',
          'success'
        );
        that.reserveService.createReserve(param);
        return;
      });
    }
  }

  goBack(): void {
    this.router.navigate(['/table-booking'], { relativeTo: this.route });
  }

}
