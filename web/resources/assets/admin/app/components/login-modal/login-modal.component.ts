import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { User } from 'app/models/user';
import { UserService } from 'app/services/user.service';
import { ServerService } from 'app/services/server.service';
import { routerGrow } from '../../router.animations';


import Utils from 'app/utils/utils';
import moment = require('moment')
import { AuthService } from "app/services/auth.service";
import { invalidEmailValidator } from "app/utils/email-validator.directive";
import { default as swal } from 'sweetalert2';
import { SelectComponent } from 'ng2-select';
import { TranslateService } from 'ng2-translate';
import { ENVIRONMENT } from 'environments/environment';
import { ModalModule , ModalDirective} from 'ngx-bootstrap';
import { Router } from '@angular/router';



@Component({
  selector: 'login-modal',
  styleUrls: [Utils.getView('app/components/login-modal/login-modal.component.css')],
  templateUrl: Utils.getView('app/components/login-modal/login-modal.component.html'),
  animations: [routerGrow()],
  host: {'[@routerGrow]': ''}

})
export class LoginModalComponent implements OnInit {

  @ViewChild('childModal') public childModal:ModalDirective;
  @Input() title?:string;
  public user: any;
  public restaurants: any [];
  public response: any;
  constructor(
   private serverServ: ServerService,
    private userServ: UserService,
    private authServ: AuthService,
    private router: Router,
   
    private translateService: TranslateService
  ) {
    
  }

  public ngOnInit() {
    
  }

  show(){
    this.childModal.show();
  }
  hide(){
    this.childModal.hide();
  }

  gotoDashboard (rest) {
      var restaurant = [rest];
      console.log('restaurant ', restaurant);
      let loginedUser = new User( {           
            email: this.user.email,
            firstname:  this.user.first_name,
            lastname:  this.user.last_name,
            roles:  this.user.roles,
            phone: this.user.phone,
            tel: this.user.tel
        }, restaurant);

      // set token
      this.authServ.setToken(this.response.token); 
      loginedUser.isInit = true;
      console.log('loginedUser ', loginedUser);
      this.userServ.setCurrentUser( loginedUser );
      
      this.response.restaurants = restaurant;
  
      this.userServ.setUserInfo( this.response );
      this.childModal.hide();

      this.router.navigateByUrl('/dashboard');
  }

}
