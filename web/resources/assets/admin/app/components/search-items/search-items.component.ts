import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade, routerTransition } from '../../router.animations';
import { UserService } from "app/services/user.service";
import { StorageService } from "app/services/storage.service";
import { NotificationService } from 'app/services/notification.service';
import { SearchsService } from 'app/services/searchs.service';

 @Component({
  selector: 'table-list',
  templateUrl: Utils.getView('app/components/search-items/search-items.component.html'),
  styleUrls: [Utils.getView('app/components/search-items/search-items.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class SearchItemsComponent implements OnInit {
  private restId;
  private userId;
  private newsList: any;
  private data: any;
  private currentPage: number = 1;
  private totalItems: number;
  private itemsPerPage: number = 10;
  private maxSize: number = 10;

  constructor(
    private searchServ: SearchsService,
    private userServ: UserService,
    private storageServ: StorageService,
    private notifyServ: NotificationService
  ) { }

  ngOnInit() {

    let userKey = JSON.parse(localStorage.getItem('user_key'));
    if (!userKey) {
      this.userServ.logout();
    } 

    this.doSearch();
  }

  private doSearch() {
    var params = {
    };
    this.searchServ.getSearchList(params).then(response => {
   
      let searchWithoutLogin: any[] = response.searchWithoutLogin;
    
      this.newsList = response.search;
      for(var i = 0; i < searchWithoutLogin.length; i++) {
        var searchItem = searchWithoutLogin[i];
        
        searchItem.first_name = "anonymous";
        searchItem.last_name = "anonymous";
      
        this.newsList.push(searchItem);
      }
      

      this.totalItems = this.newsList.length;
       this.pageChanged({ page: this.currentPage, itemsPerPage: this.itemsPerPage });
    }, error => {
      console.log(error);
    });
  }

  public pageChanged(event: any): void {
    let start = (event.page - 1) * event.itemsPerPage;
    let end = event.itemsPerPage > -1 ? (start + event.itemsPerPage) : this.newsList.length;
    this.data = this.newsList.slice(start, end);
  }

  private doDeleteSearch(obj) {
   
    let id = obj.id;
    var params = {
      'id': id
    };
    var me = this;
    this.searchServ.deleteSearch(params).then(response => {
      if(response.return_cd == 0) {
        me.notifyServ.success('Search Items has been deleted');
        me.doSearch();
      }
    }, error => {
      console.log(error);
    });
  }

  private doSetScope(obj) {
    this.storageServ.setScope(obj);
  }

}
