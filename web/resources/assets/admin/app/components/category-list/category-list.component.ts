import { Component, OnInit } from '@angular/core';
import Utils from 'app/utils/utils';
import { BreadcrumbService } from 'app/services/breadcrumb.service';
import { routerFade } from 'app/router.animations';
import { routerTransition } from 'app/router.animations';
import { CategoryService } from 'app/services/category.service';
import  { AuthService } from 'app/services/auth.service'
import { NotificationService } from 'app/services/notification.service';
import { default as swal } from 'sweetalert2';
import { Router } from '@angular/router';
import { StorageService } from 'app/services/storage.service';
@Component({
  selector: 'category-list',
  templateUrl: Utils.getView('app/components/category-list/category-list.component.html'),
  styleUrls: [Utils.getView('app/components/category-list/category-list.component.css')],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})

export class CategoryListComponent implements OnInit {
  private categories : any;
  private token : any;
  private userId : String;
  private restId : String;
  private categoryObject : any;

  constructor(private categoryService: CategoryService,
  private authServ: AuthService,
  private notif: NotificationService,
  private router: Router,
  private StorageService: StorageService,) { }
 
  ngOnInit() {
   
    let userInfo = JSON.parse(localStorage.getItem('user_key'));
    
    this.restId = userInfo.restaurants[0].id;
    this.userId = userInfo.user_info.id;
  	this.fetchAllCategories(this.restId);
  
  }

  	private fetchAllCategories(restId) {
  		this.categoryService.getCategories(restId,null).then(
                     response  => { 
                     	this.categories = response.categories;
                     	console.log(response.categories); 
                     },
  					 error => {console.log(error)
  					});
  	}

updateCategory(item,isActive){
  var action = 'update_categories_by_id';
  this.categoryObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'category':{
              'id':item.id,
              'name':item.name,
              'thumb':item.thumb,
              'description':item.description,
              'is_active':isActive,
              'is_delete':'0'
            }};
  	this.categoryService.createCategory(this.categoryObject).then(
                response  => this.processResult(response,null),
                     error =>  this.failedCreate.bind(error));
}
deleteCategory(item,isActive){
  var action = 'update_categories_by_id';
  this.categoryObject = {
            'rest_id': this.restId,
            'user_id':this.userId,
            'category':{
              'id':item.id,
              'name':item.name,
              'thumb':item.thumb,
              'description':item.description,
              'is_active':isActive,
              'is_delete':'1'
            }};
  	this.categoryService.createCategory(this.categoryObject).then(
                response  => this.processResult(response,item.thumb),
                     error =>  this.failedCreate.bind(error));
}
private processResult(response,thumb) {

    if (response.message == undefined || response.message == 'OK') {
      if(thumb!=null){
        this.categoryService.deleteFile({ 'thumb': thumb }).then(response => {
          this.notif.success('News has been deleted');
        }, error => {
          console.log(error);
        });
      }
      this.notif.success('New Category has been added');
      this.fetchAllCategories(this.restId);
    } else {
      swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
    }
  }
  private failedCreate (res) {
    swal(
          'Update Fail!',
          'error'
        ).catch(swal.noop);
  }
  createCategory(){
    //   this.userService.currentUser.subscribe((user) => {
    //    console.log('user', user);

    // });
      this.StorageService.setScope(null);
      this.router.navigateByUrl('/category/create');
  }
  editCategory(item){
      this.StorageService.setScope(item);
      this.router.navigateByUrl('/category/create/'+ item.id);
  }
}
