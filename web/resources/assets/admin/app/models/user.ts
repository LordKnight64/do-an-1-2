export class User {
    public firstname: string;
    public lastname: string;
    public email: string;
    public avatarUrl: string;
    public creationDate: string;
    public preferredLang: string;
    public phone: string;
    public roles:any;
    public tell:string;
    public restaurants: any;
    public isInit: boolean = false;


    public constructor( data: any = {}, restaurant: any = {}) {
        this.firstname = data.firstname || '';
        this.lastname = data.lastname || '';
        this.email = data.email || '';
        this.avatarUrl = data.avatarUrl || '';
        this.phone = data.phone || "";
        this.creationDate = data.creation_date || Date.now();
        this.preferredLang = data.preferredLang || null;
        this.roles = data.roles || [];
        this.tell = data.tell || '';
        // this.connected = data.connected || false;
        this.isInit = data.isInit || false;
        this.restaurants = restaurant;
    }

    public getName() {
        return this.firstname + ' ' + this.lastname;
    }
}
