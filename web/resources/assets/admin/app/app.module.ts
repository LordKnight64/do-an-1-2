import { NgModule, NgModuleFactoryLoader, SystemJsNgModuleLoader } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule }    from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions, Headers } from '@angular/http';
import { RouterModule, Router } from '@angular/router';
import { AlertModule, DatepickerModule, TimepickerModule,  ButtonsModule, ModalModule, AccordionModule, CarouselModule, CollapseModule, TabsModule, PaginationModule } from 'ngx-bootstrap';
// import { ModalModule  } from 'ng2-bootstrap/modal/modal.module';
import { ToasterModule } from 'angular2-toaster/angular2-toaster';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';
// import {trace, Category, UIRouterModule, UIView} from "ui-router-ng2";
// import { MyRootUIRouterConfig } from 'app/router.config';
// import {MAIN_STATES} from "app/app.states";
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';
// import { provideAuth, JwtHelper } from 'angular2-jwt';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ENVIRONMENT } from 'environments/environment';
import { SelectModule } from 'ng2-select';
import * as CryptoJS from 'crypto-js';
import { ChartsModule } from 'ng2-charts';
import { CalendarModule } from 'primeng/primeng';



export function createTranslateLoader(http: Http) {
    // return new TranslateStaticLoader( http, 'admin/app/i18n/');
    return new TranslateStaticLoader(http, 'locale');
}

// save a jwt token
// localStorage.setItem('token','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ');


export function authHttpServiceFactory(http: Http, options: RequestOptions, router: Router) {
    // let jwtHelper: JwtHelper = new JwtHelper();
    return new AuthHttp(new AuthConfig({
        tokenName: 'id_token',
        tokenGetter: function () {
            console.log('tokenGetter');
            var key = CryptoJS.enc.Base64.parse("#base64Key#");
            var iv = CryptoJS.enc.Base64.parse("#base64IV#");
            var idToken = "";
            var encryptedToken = localStorage.getItem('id_token');
            console.log('encryptedToken ', encryptedToken);
            if (encryptedToken != undefined && encryptedToken != null) {
                var decrypted = CryptoJS.AES.decrypt(encryptedToken.toString(), key, { iv: iv });
                idToken = decrypted.toString(CryptoJS.enc.Utf8);
                console.log('descrypted ', idToken);
            }

            if (idToken) {
                let jwtHelper: JwtHelper = new JwtHelper();
                if (jwtHelper.isTokenExpired(idToken)) {
                    console.log('------AuthHttp get token server------');
                    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + idToken, 'skipAuthorization': true });
                    let options = new RequestOptions({ headers: headers });
                    return http.post(ENVIRONMENT.DOMAIN_API + ':' + ENVIRONMENT.SERVER_PORT + '/token', options).toPromise().then(function (response) {
                        let data = response.json();

                        //Impementing the Key and IV and encrypt the password
                        var encryptedToken = CryptoJS.AES.encrypt(data.token, key, { iv: iv });
                        var token = encryptedToken.toString();
                        console.log('encrypted 123 ', encryptedToken.toString());
                        localStorage.setItem('id_token', token);

                        return token;
                    }, function (response) {
                        if (response) {
                            switch (response.status) {
                                case 403:
                                    // this.router.navigate(['log']);
                                    break;
                                case 401:
                                    localStorage.removeItem('id_token');
                                    this.router.navigate(['login']);
                                    break;
                                default:
                                    this.router.navigate(['error']);
                                    break;
                            }
                        }
                    });
                } else {
                    console.log('------AuthHttp get token localStorage------');
                    return idToken;
                }
            }
        },
        globalHeaders: [{ 'Content-Type': 'application/json' }],
        noJwtError: true
    }), http, options);
}

let modules = [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ChartsModule,
    ReactiveFormsModule,
    RouterModule,
    ToasterModule,
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),

    DatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    TranslateModule.forRoot({
        deps: [Http],
        provide: TranslateLoader,
        useFactory: (createTranslateLoader)
    }),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    // UIRouterModule.forRoot({
    //   states: MAIN_STATES,
    //   otherwise: { state: 'app', params: {} },
    //   useHash: true,
    //   configClass: MyRootUIRouterConfig
    // })
    SelectModule,
    CalendarModule
];

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './widgets/app-header/app-header.component';
import { MenuAsideComponent } from './widgets/menu-aside/menu-aside.component';
import { BreadcrumbComponent } from './widgets/breadcrumb/breadcrumb.component';
import { AppFooterComponent } from './widgets/app-footer/app-footer.component';
import { ControlSidebarComponent } from './widgets/control-sidebar/control-sidebar.component';
import { MessagesBoxComponent } from './widgets/messages-box/messages-box.component';
import { NotificationBoxComponent } from './widgets/notification-box/notification-box.component';
import { TasksBoxComponent } from './widgets/tasks-box/tasks-box.component';
import { UserBoxComponent } from './widgets/user-box/user-box.component';
import { LimitToDirective } from './widgets/limit-to.directive';

let widgets = [
    AppComponent,
    BreadcrumbComponent,
    AppHeaderComponent,
    AppFooterComponent,
    MenuAsideComponent,
    ControlSidebarComponent,
    MessagesBoxComponent,
    NotificationBoxComponent,
    TasksBoxComponent,
    UserBoxComponent,
    LimitToDirective
];

import { StorageService } from "./services/storage.service";
import { UserService } from './services/user.service';
import { MessagesService } from './services/messages.service';
import { CanActivateGuard } from './services/guard.service';
import { NotificationService } from './services/notification.service';
import { BreadcrumbService } from './services/breadcrumb.service';
import { AdminLTETranslateService } from './services/translate.service';
import { LoggerService } from './services/logger.service';
import { HeroService } from './services/hero.service';
import { ServerService } from './services/server.service';
import { AuthService } from './services/auth.service';
import { CategoryService } from './services/category.service';
import { AreaService } from './services/area.service';
import { TableService } from './services/table.service';
import { NewsService } from './services/news.service';
import { RestaurantService } from './services/restaurant.service';
import { ContactService } from './services/contact.service';
import { OptionFoodItemService } from './services/optionFoodItem.service';
import { OptionValueFoodItemService } from './services/optionValueFoodItem.service';
import { ComCodeService } from './services/comCode.service';
import { OrderService } from './services/order.service';
import { FoodItemService } from './services/foodItem.service';
import { ProfileService } from './services/profile.service';
import { GeoLocationService } from './services/geoLocation.service';
import { SubscribeService } from './services/subscribe.service';
import { SearchsService } from './services/searchs.service';
import { EmailService } from './services/email.service';
import { ReviewService } from './services/review.service';
import { ReserveService } from './services/reserve.service';
import {TurnOverService} from './services/turnover.service';

let services = [
    StorageService,
    AdminLTETranslateService,
    ServerService,
    AuthService,
    UserService,
    BreadcrumbService,
    MessagesService,
    CanActivateGuard,
    NotificationService,
    LoggerService,
    CategoryService,
    AreaService,
    TableService,
    HeroService,
    NewsService,
    RestaurantService,
    ReserveService,
    ContactService,
    OptionFoodItemService,
    OptionValueFoodItemService,
    ComCodeService,
    OrderService,
    FoodItemService,
    ProfileService,
    SubscribeService,
    SearchsService,
    EmailService,
    ReviewService,
    TurnOverService,
    GeoLocationService
];

import { routing } from './app.routing';

// les pages
import { HomeComponent } from './components/home/home.component';
import { ClientComponent } from './components/client/client.component';
import { LayoutsAuthComponent } from './components/layouts/auth/auth';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SystemErrorComponent } from './components/system-error/system-error.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { OrderingRequestComponent } from './components/ordering-request/ordering-request.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { CreateCategoryComponent } from './components/create-category/create-category.component';

import { OptionFoodItemsListComponent } from './components/option-food-items-list/option-food-items-list.component';
import { CreateOptionFoodItemComponent } from './components/create-option-food-item/create-option-food-item.component';
import { OptionValueFoodItemsComponent } from './components/option-value-food-items/option-value-food-items.component';
import { CreateOptionValueFoodItemComponent } from './components/create-option-value-food-item/create-option-value-food-item.component';
import { ContentsComponent } from './components/contents/contents.component';
import { NewsListComponent } from './components/news-list/news-list.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { OrderSettingComponent } from './components/order-setting/order-setting.component';
import { TurnOverComponent } from './components/turn-over/turn-over.component';
import { FoodItemsListComponent } from './components/food-items-list/food-items-list.component';
import { CreateFoodItemComponent } from './components/create-food-item/create-food-item.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { TableListComponent } from './components/table-list/table-list.component';
import { TableBookingComponent } from './components/table-booking/table-booking.component';


import { SubscribeListComponent } from './components/subscribe-list/subscribe-list.component';
import { SendEmailMarketingComponent } from './components/send-email-marketing/send-email-marketing.component';
import { SearchItemsComponent } from './components/search-items/search-items.component';
import { EmailMarketingListComponent } from './components/email-marketing-list/email-marketing-list.component';
import { ReviewItemComponent } from './components/review-item/review-item.component';
import { ViewEmailMarketingComponent } from './components/view-email-marketing/view-email-marketing.component';
import { AreaListComponent } from './components/area-list/area-list.component';
import { CreateAreaComponent } from './components/create-area/create-area.component';
import { CreateTableComponent } from './components/create-table/create-table.component';
import {ReserveTableComponent} from './components/reserve-table/reserve-table.component';
import {TableBookingScheduleComponent} from './components/table-booking-schedule/table-booking-schedule.component';
import {OrderFoodComponent} from './components/order-food/order-food.component';

import { LoginModalComponent} from './components/login-modal/login-modal.component';
import { SubscribeListModalComponent } from './components/subscribe-list-modal/subscribe-list-modal.component';

let pages = [
    HomeComponent,
    // // PageNumComponent,
    CreateAreaComponent,
    ClientComponent,
    LayoutsAuthComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent,
    PageNotFoundComponent,
    SystemErrorComponent,
    OrderingRequestComponent,
    ProfileComponent,
    CategoryListComponent,
    CreateCategoryComponent,
    OptionFoodItemsListComponent,
    CreateOptionFoodItemComponent,
    OptionValueFoodItemsComponent,
    CreateOptionValueFoodItemComponent,
    ContentsComponent,
    NewsListComponent,
    NewsItemComponent,
    OrdersListComponent,
    ContactListComponent,
    ContactItemComponent,
    OrderSettingComponent,
    TurnOverComponent,
    FoodItemsListComponent,
    CreateFoodItemComponent,
    FeedbackComponent,
    TableListComponent,
    TableBookingComponent,
    SubscribeListComponent,
    SendEmailMarketingComponent,
    SearchItemsComponent,
    ReviewItemComponent,
    ViewEmailMarketingComponent,
    AreaListComponent,
    CreateTableComponent,
    ReserveTableComponent,
    LoginModalComponent,
    OrderFoodComponent,
    TableBookingScheduleComponent,
    EmailMarketingListComponent,
    SubscribeListModalComponent
];

// trace.enable(Category.TRANSITION, Category.VIEWCONFIG);

@NgModule({
    imports: [
        ...modules,
        routing
    ],
    declarations: [
        ...widgets,
        ...pages,
    ],
    providers: [
        // { provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader },
        ...services,
        { provide: "windowObject", useValue: window },
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions, Router]
        },
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        }
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
