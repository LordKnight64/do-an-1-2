import {Component} from '@angular/core';
import Utils from 'app/utils/utils';
import { AuthService } from "app/services/auth.service";

@Component({
    selector:'my-app',
    //templateUrl: './app/app.html'
    //templateUrl: '/tpl/admin/index'
    templateUrl: Utils.getView('app/app.html')
})

export class AppComponent {
    title = 'Tour of Heroes';

   //  constructor(private authServ: AuthService) {
	  //   this.authServ.handleAuthentication();
	  // }
}