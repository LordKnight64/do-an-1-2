import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class OrderService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getOrderList(params) {

    var pr = {
      'user_id': params.user_id,
      'rest_id': params.rest_id,
      'order_sts': params.order_sts != null ? params.order_sts : null,
      'delivery_ts_to': params.delivery_ts_to != null ? params.delivery_ts_to : null,
      'delivery_ts_from': params.delivery_ts_from != null ? params.delivery_ts_from : null,
      'user_id_search': params.user_id_search != null ? params.user_id_search : null,
      'id': params.id != null ? params.id : null,
    };
    let action = 'get_orders_by_rest_id';
    return this.serverServ.doPost(action, pr);
  }

  public updateOrder(params) {

    let action = 'update_order_by_id';
    return this.serverServ.doPost(action, params);
  }
}