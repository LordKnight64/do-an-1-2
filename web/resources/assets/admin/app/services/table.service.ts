import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router} from '@angular/router';
import { ServerService } from './server.service';

@Injectable()
export class TableService {

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private serverServ: ServerService
    ) {
      // TODO
    }



    public getTables(restId,id,area_id){
       console.log('start get areas service ');

        let action = 'get_table_by_rest_id';
         let params = {
            'rest_id': restId,//get from login
            'id': id,//get from login
            'area_id': area_id
         };
       console.log('get tables service ', action, params);
           return  this.serverServ.doPost(action, params);
      }

      public createTable(tableObject){
       console.log('start table ');

        let action = 'update_table_by_id';
         let params = {
            'rest_id': tableObject.rest_id,//get from login
            'user_id':tableObject.user_id,
            'table':{
              'id':tableObject.table.id,
              'name':tableObject.table.name,
              'thumb':tableObject.table.thumb,
              'slots':tableObject.table.slots,
              'content': tableObject.table.content,
              'is_active':tableObject.table.is_active,
              'is_delete':tableObject.table.is_delete
            },

         };
       console.log('get table service ', action, params);
           return  this.serverServ.doPost(action, params);
      }

      public tableUploadFile(tableObject){
       console.log('start createTable upload file');

        let action = 'upload_file';
         let params = {
              'id':tableObject.id,
              'thumb':tableObject.thumb,
              'typeScreen':'table'
         };
       console.log('get areas service ', action, params);
           return  this.serverServ.doPost(action, params);
      }
    public deleteFile(params) {

    let action = 'delete_file';
    console.log('get delete file service ', action, params);
    return this.serverServ.doPost(action, params);
  }
}
