import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router} from '@angular/router';
import { ServerService } from './server.service';

@Injectable()
export class AreaService {

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private serverServ: ServerService
    ) {
      // TODO
    }



    public getAreas(restId,id){
       console.log('start get areas service ');

        let action = 'get_area_by_rest_id';
         let params = {
            'rest_id': restId,//get from login
            'id': id,//get from login
         };
       console.log('get areas service ', action, params);
           return  this.serverServ.doPost(action, params);
      }
      
      public createArea(areaObject){
       console.log('start area ');

        let action = 'update_area_by_id';
         let params = {
            'rest_id': areaObject.rest_id,//get from login
            'user_id':areaObject.user_id,
            'area':{
              'id':areaObject.area.id,
              'name':areaObject.area.name,
              'thumb':areaObject.area.thumb,
              'is_active':areaObject.area.is_active,
              'is_delete':areaObject.area.is_delete
            },

         };
       console.log('get areas service ', action, params);
           return  this.serverServ.doPost(action, params);
      }

      public areaUploadFile(areaObject){
       console.log('start createArea upload file');

        let action = 'upload_file';
         let params = {
              'id':areaObject.id,
              'thumb':areaObject.thumb,
              'typeScreen':'area'
         };
       console.log('get areas service ', action, params);
           return  this.serverServ.doPost(action, params);
      }
    public deleteFile(params) {

    let action = 'delete_file';
    console.log('get delete file service ', action, params);
    return this.serverServ.doPost(action, params);
  }
}
