import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class ContactService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getContactList(params) {

    let action = 'get_contacts_by_rest_id';
    return this.serverServ.doPost(action, params);
  }

  public getContact(params) {

    let action = 'get_contact_by_id';
    return this.serverServ.doPost(action, params);
  }

  public updateContact(params) {

    let action = 'update_contact_by_id';
    return this.serverServ.doPost(action, params);
  }
}
