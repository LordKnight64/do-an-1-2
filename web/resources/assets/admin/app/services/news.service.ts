import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class NewsService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getNewsList(params) {

    let action = 'get_news_by_rest_id';
    return this.serverServ.doPost(action, params);
  }

  public getNews(params) {

    let action = 'get_news_by_id';
    return this.serverServ.doPost(action, params);
  }

  public updateNews(params) {

    let action = 'update_news_by_id';
    return this.serverServ.doPost(action, params);
  }

  public uploadFile(obj) {

    console.log('start upload file');
    let action = 'upload_file';
    let params = {
      'id': obj.id,
      'thumb': obj.thumb,
      'typeScreen': 'news'
    };
    console.log('get upload service ', action, params);
    return this.serverServ.doPost(action, params);
  }

  public deleteFile(params) {
    
    let action = 'delete_file';
    console.log('get delete file service ', action, params);
    return this.serverServ.doPost(action, params);
  }
}
