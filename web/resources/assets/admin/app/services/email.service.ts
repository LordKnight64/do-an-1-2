import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class EmailService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getEmailMarketingList(params) {

    let action = 'get_email_marketing_list';
    return this.serverServ.doPost(action, params);
  }


  public deleteEmailMarketingList(params) {

    let action = 'delete_email_marketing_list';
    return this.serverServ.doPost(action, params);
  }

  public getEmailMarketingDetail(params) {

    let action = 'get_email_marketing_detail';
    return this.serverServ.doPost(action, params);
  }

   public sendEmailMarketingList(params) {

    let action = 'send_email_marketing_list';
    return this.serverServ.doPost(action, params);
  }

  public sendEmailAcceptOrder(params){
    let action = 'send_email_accept_order';
    return this.serverServ.doPost(action, params);
  }

 


}
