/**
 * Created by Moiz.Kachwala on 02-06-2016.
 */
import { Injectable, Inject } from '@angular/core';
// import { Http, Response } from '@angular/http';
import { Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import { LoggerService } from 'app/services/logger.service';
import 'rxjs/add/operator/toPromise';
import { ENVIRONMENT } from 'environments/environment';
import { Router } from '@angular/router';

import moment = require('moment')

@Injectable()
export class ServerService {

    private url = ENVIRONMENT.DOMAIN_API + ':' + ENVIRONMENT.SERVER_PORT + '/';
    constructor(
        // private http: Http, 
        private logger: LoggerService, 
        @Inject("windowObject") window: Window, 
        public authHttp: AuthHttp,
        public router: Router
    ) {}

    doPost(action: string, params: any): Promise<any> {

        // let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':  'Bearer'});
        // // let headers = new Headers({ 'Content-Type': 'application/json'});
       
        // // var token = JSON.parse(window.sessionStorage.getItem('token'));
        // var token = localStorage.getItem('token');
        //   if (token) {
        //     // headers.set('Authorization', 'Bearer ' + token.key);
        //     headers.set('Authorization', 'Bearer ' + token);
        //   } 
        //  let options = new RequestOptions({ headers: headers });
         // console.log('headers', headers);
        if (ENVIRONMENT.PRODUCTION ===  false) {
            var actionStartDate = moment();
            console.log(params);
            this.logger.log(actionStartDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   START]', action);
            this.logger.log('PARAM', JSON.stringify(params));
        }
        // console.log('this.authHttp.get', this.authHttp.get());
        
        return this.authHttp.post(this.url + action, params)
            .map(
                response => this.extractData(response, actionStartDate)
            )
            .toPromise()
            .catch(error => this.handleError(error, this.router));
    }

    private extractData(res: Response, actionStartDate: any) {
        if (ENVIRONMENT.PRODUCTION ===  false) {
            var actionEndDate = moment();
            this.logger.log(actionEndDate.format("YYYY/MM/DD HH:mm:ss.SSS") + ' [ACTION   END  ]', res.url + ' [' + (actionEndDate.diff(actionStartDate)) + 'ms]');
            this.logger.log('RESULT' , JSON.stringify(res.json()));
        }
        let data = res.json();
        return data || {};
    }

    private handleError(error: Response | any, router: Router): Promise<any> {
        // In a real world app, we might use a remote logging infrastructure
      
         if(error){
            switch (error.status) {
                case 403:
                    // this.router.navigate(['log']);
                    break;
                case 401:
                    localStorage.removeItem('id_token');
                    this.router.navigate( ['login'] );
                    break;
                default:
                    this.router.navigate(['error']);
                    break;
            }
        }

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        // this.logger.log('Error', errMsg);
        console.log('Error: ' + errMsg);
        return Promise.reject(error);
    }
}