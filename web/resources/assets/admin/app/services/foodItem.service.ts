import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router} from '@angular/router';
import { ServerService } from './server.service';

@Injectable()
export class FoodItemService {    

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private serverServ: ServerService
    ) {
      // TODO
    }
    public getFoodItems(restId,categoryId,id){
       console.log('start OptionFoodItems service ');
      
        let action = 'get_items_by_rest_id'; 
         let params = {
            'rest_id': restId,//get from login
            'category_id': categoryId,
            'id': id,//get from login
         };
       console.log('get Item service ', action, params);
           return  this.serverServ.doPost(action, params);
      }
               
      public createFoodItems(itemObject){
       console.log('start createOptionFoodItems ');
       console.log('itemObject ', itemObject);
       let option_items = itemObject.item.option_items;
        let action = 'update_item_by_id'; 
         let params = {
            'rest_id': itemObject.rest_id,//get from login
            'user_id':itemObject.user_id,
            'item':{
              'category_id':itemObject.item.category_id,
              'id':itemObject.item.id,
              'name':itemObject.item.name,
              'price':itemObject.item.price,
              'thumb':itemObject.item.thumb,
              'sku':itemObject.item.sku,
              'point':itemObject.item.point,
              'is_active':itemObject.item.is_active,
              'is_alcoho':itemObject.item.is_alcoho,
              'food_type':itemObject.item.food_type,
              'description':itemObject.item.description,
              'is_delete':itemObject.item.is_delete,
              'update_in_lst_flg':itemObject.item.update_in_lst_flg,
              'option_items':option_items,
            }
         };
       console.log('get createFoodItems service ', action, params);
           return  this.serverServ.doPost(action, params);
      }      
      public itemUploadFile(itemObject){
       console.log('start item upload file');
     
        let action = 'upload_file'; 
         let params = {
              'id':itemObject.id,
              'thumb':itemObject.thumb,
              'typeScreen':'items'
         };
       console.log('get item service ', action, params);
           return  this.serverServ.doPost(action, params);
      } 
    public deleteFile(params) {
    
    let action = 'delete_file';
    console.log('get delete file service ', action, params);
    return this.serverServ.doPost(action, params);
  }      
}
