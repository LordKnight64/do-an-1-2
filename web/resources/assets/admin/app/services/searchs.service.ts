import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class SearchsService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getSearchList(params) {

    let action = 'get_search_items';
    return this.serverServ.doPost(action, params);
  }


  public deleteSearch(params) {

    let action = 'delete_search_items';
    return this.serverServ.doPost(action, params);
  }

 


}
