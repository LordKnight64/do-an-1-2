import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router} from '@angular/router';
import { ServerService } from './server.service';

@Injectable()
export class ProfileService {    

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private serverServ: ServerService
    ) {
      // TODO
    }
    public getProfileRetaurant(restId,userId){
       console.log('start OptionFoodItems service ');
     
        let action = 'get_restaurant_detail_by_id'; 
         let params = {
            'rest_id': restId,//get from login
            'user_id': userId
         };
       console.log('ProfileService', action, params);
           return  this.serverServ.doPost(action, params);
      }  
      public getOrderSeetingProfile(restId){
       console.log('start OptionFoodItems service ');
     
        let action = 'get_order_setting_by_rest_id'; 
         let params = {
            'rest_id': restId
         };
       console.log('ProfileService', action, params);
           return  this.serverServ.doPost(action, params);
      }         
      public createProfile(params){
       console.log('start createOptionFoodItems ');
      //  let option_items = itemObject.item.option_items;
        let action = 'update_profile'; 
        //  let params = {
        //     'rest_id': itemObject.rest_id,//get from login
        //     'user_id':itemObject.user_id,
        //     'item':{
        //       'category_id':itemObject.item.category_id,
        //       'id':itemObject.item.id,
        //       'name':itemObject.item.name,
        //       'price':itemObject.item.price,
        //       'thumb':itemObject.item.thumb,
        //       'sku':itemObject.item.sku,
        //       'is_active':itemObject.item.is_active,
        //       'is_alcoho':itemObject.item.is_alcoho,
        //       'food_type':itemObject.item.food_type,
        //       'description':itemObject.item.description,
        //       'is_delete':itemObject.item.is_delete,
        //       'update_in_lst_flg':itemObject.item.update_in_lst_flg,
        //       'option_items':option_items,
        //     }
        //  };
       console.log('get createProfile service ', action, params);
           return  this.serverServ.doPost(action, params);
      }      
      public profileUploadFile(itemObject){
       console.log('start item upload file');
     
        let action = 'upload_file'; 
         let params = {
              'id':itemObject.id,
              'thumb':itemObject.thumb,
              'typeScreen':'profile'
         };
       console.log('get item service ', action, params);
           return  this.serverServ.doPost(action, params);
      } 
    public deleteFile(params) {
    
    let action = 'delete_file';
    console.log('get delete file service ', action, params);
    return this.serverServ.doPost(action, params);
  }      

  
  public getAddress(object){
       console.log('start getAddress');
     
        let action = 'get_addrress_by_latlon'; 
         let params = {
              'lat':object.lat,
              'lon':object.lon
         };
       console.log('getAddress ', action, params);
           return  this.serverServ.doPost(action, params);
      } 
      
  public getLatlon(object){
    console.log('start getLatlon');
  
    let action = 'get_data_coordinates'; 
      let params = {
          'address':object.address
      };
      return  this.serverServ.doPost(action, params);
  } 
}
