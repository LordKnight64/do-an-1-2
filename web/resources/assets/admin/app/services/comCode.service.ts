import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class ComCodeService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getCode(params) {

    let action = 'get_code_by_cd_group';
    return this.serverServ.doPost(action, params);
  }

  private handleError(res) {
    //TODO
  }
}