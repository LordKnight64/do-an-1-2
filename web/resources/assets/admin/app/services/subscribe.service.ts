import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class SubscribeService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getSubscribeList(params) {

    let action = 'get_subscribe';
    return this.serverServ.doPost(action, params);
  }


  public deleteSubscribe(params) {

    let action = 'delete_subscribe';
    return this.serverServ.doPost(action, params);
  }

 


}
