import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class TurnOverService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getTurnoverPlaning(params) {
    let action = 'get_turnover_planing';
    return this.serverServ.doPost(action, params);
  }

  public getTurnover(params) {
    let action = 'get_turnover';
    return this.serverServ.doPost(action, params);
  }

  public updateTurnoverPlaning(params) {
    let action = 'update_turnover_planing';
    return this.serverServ.doPost(action, params);
  }

  private handleError(res) {
    //TODO
  }
}