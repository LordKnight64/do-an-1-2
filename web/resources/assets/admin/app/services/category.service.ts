import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router} from '@angular/router';
import { ServerService } from './server.service';

@Injectable()
export class CategoryService {    

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private serverServ: ServerService
    ) {
      // TODO
    }

    public getCategories(restId,id){
       console.log('start get categories service ');
     
        let action = 'get_categories_by_rest_id'; 
         let params = {
            'rest_id': restId,//get from login
            'id': id,//get from login
         };
       console.log('get categories service ', action, params);
           return  this.serverServ.doPost(action, params);
      }         
      public createCategory(categoryObject){
       console.log('start createCategory ');
     
        let action = 'update_categories_by_id'; 
         let params = {
            'rest_id': categoryObject.rest_id,//get from login
            'user_id':categoryObject.user_id,
            'category':{
              'id':categoryObject.category.id,
              'name':categoryObject.category.name,
              'thumb':categoryObject.category.thumb,
              'description':categoryObject.category.description,
              'is_active':categoryObject.category.is_active,
              'is_delete':categoryObject.category.is_delete
            },

         };
       console.log('get categories service ', action, params);
           return  this.serverServ.doPost(action, params);
      }      
      public categoryUploadFile(categoryObject){
       console.log('start createCategory upload file');
     
        let action = 'upload_file'; 
         let params = {
              'id':categoryObject.id,
              'thumb':categoryObject.thumb,
              'typeScreen':'category'
         };
       console.log('get categories service ', action, params);
           return  this.serverServ.doPost(action, params);
      }    
    public deleteFile(params) {
    
    let action = 'delete_file';
    console.log('get delete file service ', action, params);
    return this.serverServ.doPost(action, params);
  }        
}
