import { Injectable, Inject } from '@angular/core';
// import { StateService } from "ui-router-ng2";
import { UserService } from 'app/services/user.service';
import { ServerService } from 'app/services/server.service';
import { User } from 'app/models/user';
import moment = require('moment');
import { ENVIRONMENT } from 'environments/environment';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Router} from '@angular/router';
// jwtHelper: JwtHelper = new JwtHelper();
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthService {
  private KEY:string;
  private IV: string;
  constructor(
    private serverServ: ServerService, 
    // private userServ: UserService, 
    private router: Router
   ) {
    //TODO
    this.KEY = CryptoJS.enc.Base64.parse("#base64Key#");
    this.IV = CryptoJS.enc.Base64.parse("#base64IV#");
  }

  public checkToken(token) {
   
    if(token){
      // var current = moment();
      // var tokenExpiresAt = moment(token.expiresAt);
      // //check expires
      // if(current.diff(tokenExpiresAt) > environment.JWTTtl){
      //   // not expires 
      //   return 2;
      // }
      //is expires
      return 1;
    }
    return 3;
  }

  public authenticated() {
   
    // Check if there's an unexpired JWT
    // This searches for an item in localStorage with key == 'id_token'
    var token = localStorage.getItem('id_token'); 
   
    var  idToken = "";
    console.log('encryptedToken ',token);
    if(token != undefined && token != null) {
        var decrypted = CryptoJS.AES.decrypt(token.toString(), this.KEY, {iv: this.IV});       
        idToken = decrypted.toString(CryptoJS.enc.Utf8);
        console.log('descrypted ',idToken);
        return tokenNotExpired('id_token', idToken);
    }       
    return false;   
  }

  public setToken(tokenKey){   
    if(tokenKey){     
      var key = CryptoJS.enc.Base64.parse("#base64Key#");
      var iv  = CryptoJS.enc.Base64.parse("#base64IV#");      
      //Impementing the Key and IV and encrypt the password
      var encryptedToken = CryptoJS.AES.encrypt(tokenKey, this.KEY, {iv: this.IV});  
      console.log('encrypted ', encryptedToken.toString());     
       localStorage.setItem('id_token', encryptedToken.toString()); 
    } else {
      // window.sessionStorage.removeItem('token');
      localStorage.removeItem('id_token');
    }
  }

  public getToken(){
    var action = 'token';
    return this.serverServ.doPost(action, {}).then( 
      response => this.processResult(response),
      error => this.handleError(error)
    );
  };

  private processResult(res){
    var token = res.token;
    this.setToken(token);
  }

  private handleError(error){
    console.log(error);
    // localStorage.removeItem('id_token');
    // this.userServ.removeCurrentUser();
    // this.router.navigate( ['login'] );
  }
}
