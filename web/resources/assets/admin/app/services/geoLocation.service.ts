import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class GeoLocationService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getDistance(params) {

    let action = 'get_distance';
    return this.serverServ.doPost(action, params);
  }
}