import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class RestaurantService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getContents(params) {

    let action = 'get_contents_by_rest_id';
    return this.serverServ.doPost(action, params);
  }

  public updateContents(params) {

    let action = 'update_contents_by_rest_id';
    return this.serverServ.doPost(action, params);
  }

  public getOrderSetting(params) {

    let action = 'get_order_setting_by_rest_id';
    return this.serverServ.doPost(action, params);
  }

  public updateOrderSetting(params) {

    let action = 'update_order_setting_by_rest_id';
    return this.serverServ.doPost(action, params);
  }
  
   public sendFeeback(params) {
    let action = 'send_feedback';
    return this.serverServ.doPost(action, params);
  }
  private handleError(res) {
    //TODO
  }
}