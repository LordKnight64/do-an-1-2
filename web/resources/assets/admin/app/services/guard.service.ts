import { Injectable, Inject } from '@angular/core';
//import { StateService } from "ui-router-ng2";
import { UserService } from './user.service';
//import { ServerService } from './server.service';
import { AuthService } from './auth.service';
import { User } from 'app/models/user';
import { Router} from '@angular/router';
// import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class CanActivateGuard {
  // private connected: boolean = false;
  private isGetUser: boolean = false;
  // private error: any;
  constructor(
    private router: Router,
    private userServ: UserService,
    //private stateServ: StateService,
    //private serverServ: ServerService,
    private authServ: AuthService
    // private authHttp: AuthHttp
  ) {
    // this.userServ.currentUser.subscribe((user) => {
    //   if(!user.email){
    //     console.log('user', user);
    //     this.isGetUser = true;
    //   }
    // });
  }

  // public isLoggedIn() {

  //   // // var token = JSON.parse(window.sessionStorage.getItem('token'));
  //   // var token = window.localStorage.getItem('token');
  //   // var tokenState = this.authServ.checkToken(token);


  //   // //is token && is expires
  //   // if(tokenState === 1){
  //   //   //set expires
  //   //   // this.authServ.setToken(token.key);
  //   //   this.authServ.setToken(token);

  //   //   // is refesh page
  //   //   // if(!this.connected){
  //   //     //get authenticated user
  //   //     this.userServ.getAuthenticatedUser();
  //   //   // }
      
  //   //   return true;
  //   // } else if(tokenState === 2){ // is token && not expires 
  //   //     //remove token
  //   //    this.authServ.setToken('');
  //   //    //remove user
  //   //    this.userServ.removeCurrentUser();

  //   //    return false;
  //   // }
     
  //   // return false;
  //   var isAuthenticated = this.authServ.authenticated();
  //   if(isAuthenticated){
  //     //get authenticated user
  //     if(!this.isInit){
  //       this.userServ.getAuthenticatedUser();
  //     } else {
  //       this.isInit = false;
  //     }
      
  //   }
  //   return isAuthenticated;
  // }

  public canActivate() {

    console.log('--------------canActivate--------------------');
    // test here if you user is logged
    var isAuthenticated = this.authServ.authenticated();
    if ( !isAuthenticated ) {
      this.authServ.setToken('');
      this.userServ.removeCurrentUser();
      
      this.router.navigate( [ 'login' ] );

      // if(!this.isInit){
      //   this.userServ.getAuthenticatedUser();
      // } else {
      //   this.isInit = false;
      // }
    } 
    // else {
    //   // this.authHttp.get
    //   // console.log(this.authHttp);
    //   this.userServ.currentUser.subscribe((user) => {
    //     if(!user.email){
    //       console.log('user', user);
    //        this.userServ.getAuthenticatedUser();
    //     }
    //   },
    //   error => console.log(error));
    //   console.log( this.userServ.currentUser.subscribe());
    // }
    // console.log('------canActivate------', this.authServ.authenticated());
    return isAuthenticated;
  }
}
