import { User } from 'app/models/user';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
// import { StateService } from "ui-router-ng2";
import { Router} from '@angular/router';
import { AuthService } from "./auth.service";
import { ServerService } from './server.service';

@Injectable()
export class UserService {
    public currentUser: ReplaySubject<User> = new ReplaySubject<User>( 1 );

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private authServ: AuthService,
       private serverServ: ServerService
    ) {
      // TODO
    }

    public setCurrentUser( user: User ) {
      this.currentUser.next( user );
    }

    public logout() {
      
      //remove token
      this.authServ.setToken('');
      //remove user
      this.removeCurrentUser();

      this.router.navigate( ['login'] );

      // this.stateServ.go('login');
    }

    public removeCurrentUser(){
      let user = new User();
      // user.connected = false;
      user.isInit = false;
      this.setCurrentUser( user );
    }

    public getAuthenticatedUser(){
      var action = 'authenticate/user';

      this.serverServ.doPost(action, {}).then(
                     response  => this.processResult(response),
                     error =>  this.handleError.bind(error));
    }

    private processResult(response) {

      var user = response.user;
      var token = response.token;
      let user1 = new User( {
            // avatarUrl: 'public/assets/img/user2-160x160.jpg',
            email: user.email,
            firstname: '',
            lastname: user.name
        } );
      // set token
      this.authServ.setToken(token);

      this.setCurrentUser( user1 );
    }

    private handleError (res) {
      //TODO
    }

    public setUserInfo(userInfo) {
      if(userInfo != null){
         var user = userInfo;
         user.token = "";
         localStorage.setItem('user_key', JSON.stringify(user));
      }        
    }

    public getUserInfo() {
      var userInfo =  localStorage.getItem('user_key');
      if(userInfo != "" && userInfo != null && userInfo != undefined)
        return JSON.parse(userInfo);
      return null;
    }
}
