import { Injectable } from '@angular/core';
import { ServerService } from './server.service';

@Injectable()
export class ReviewService {

  constructor(
    private serverServ: ServerService
  ) {
    // TODO
  }

  public getRestaurantReviews(params) {

    let action = 'get_restaurant_reviews';
    return this.serverServ.doPost(action, params);
  }

  public updateRestaurantReview(params) {

    let action = 'update_restaurant_review';
    return this.serverServ.doPost(action, params);
  }

  public deleteRestaurantReview(params) {

    let action = 'delete_restaurant_review';
    return this.serverServ.doPost(action, params);
  }

   public getItemsReviews(params) {

    let action = 'get_items_reviews';
    return this.serverServ.doPost(action, params);
  }

  

 
  private handleError(res) {
    //TODO
  }
}