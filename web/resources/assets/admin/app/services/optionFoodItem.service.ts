import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router} from '@angular/router';
import { ServerService } from './server.service';

@Injectable()
export class OptionFoodItemService {    

    constructor(
       // private stateServ: StateService,
       private router: Router,
       private serverServ: ServerService
    ) {
      // TODO
    }
    public getOptionFoodItems(restId,id){
       console.log('start OptionFoodItems service ');
     
        let action = 'get_options_by_rest_id'; 
         let params = {
            'rest_id': restId,//get from login
            'id': id,//get from login
         };
       console.log('get categories service ', action, params);
           return  this.serverServ.doPost(action, params);
      }         
      public createOptionFoodItems(optionItemObject){
       console.log('start createOptionFoodItems ');
     
        let action = 'update_option_by_id'; 
         let params = {
            'rest_id': optionItemObject.rest_id,//get from login
            'user_id':optionItemObject.user_id,
            'option':{
              'id':optionItemObject.option.id,
              'name':optionItemObject.option.name,
              'is_delete':optionItemObject.option.is_delete
            },

         };
       console.log('get createOptionFoodItems service ', action, params);
           return  this.serverServ.doPost(action, params);
      }      
       
}
