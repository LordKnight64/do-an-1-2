import {trigger, state, animate, style, transition} from '@angular/core';

export function routerTransition() { 
  return slideToLeft(); 
}

function slideToLeft() {
 
  return trigger('routerTransition', [
    state('void', style({position:'fixed', width:'100%', 'padding-right':  '60px' }) ),
    state('*', style({position:'static', width:'100%' }) ),
    transition(':enter', [  // before 2.1: transition('void => *', [
      style({transform: 'translateX(100%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
    ]),
    transition(':leave', [  // before 2.1: transition('* => void', [
     // style({transform: 'translateX(0%)'}),
      //animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
    ])
  ]);
}
export function routerFade() {
  return fadeIn();
}

function fadeIn() {
  return trigger('routerFade', [
    state('void', style({position:'fixed', width:'100%', 'padding-right': '60px' }) ),
    state('*', style({position:'static', width:'100%'}) ),
    transition(':enter', [  // before 2.1: transition('void => *', [
      style({opacity: 0}),
      animate('0.5s ease-in-out', style({opacity: 1}))
    ]),
    transition(':leave', [  // before 2.1: transition('* => void', [
     // style({transform: 'translateX(0%)'}),
      //animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
    ])
  ]);
}
export function routerGrow() {
  return GrowUp();
}
function GrowUp() {
  return trigger('routerGrow', [
    state('void', style({position:'fixed', width:'100%' }) ),
    state('*', style({position:'fixed', width:'100%'}) ),
    transition(':enter', [  // before 2.1: transition('void => *', [
      style({ transform: 'scale(0)'}),
      animate('0.5s ease-in-out', style({transform: 'scale(1)'}))
    ]),
    transition(':leave', [  // before 2.1: transition('* => void', [
     // style({transform: 'translateX(0%)'}),
      //animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
    ])
  ]);
}