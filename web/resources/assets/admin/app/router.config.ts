// import {UIRouter, Transition, StateService} from "ui-router-ng2";
// import {Injectable} from "@angular/core";
// declare var SystemJS;
// import { GuardService } from './services/guard.service';

// /**
//  * Create your own configuration class (if necessary) for any root/feature/lazy module.
//  *
//  * Pass it to the UIRouterModule.forRoot/forChild factory methods as `configClass`.
//  *
//  * The class will be added to the Injector and instantiate when the module loads.
//  */
// @Injectable()
// export class MyRootUIRouterConfig {
//   /** You may inject dependencies into the constructor */
//   constructor(uiRouter: UIRouter, guardServ: GuardService, stateServ: StateService) {
//     // Show the ui-router visualizer
//     // let vis = window['ui-router-visualizer'];
//     // vis.visualizer(uiRouter);

//     // let criteria = { to: (state) => state.data && state.data.auth };
//     // uiRouter.transitionService.onBefore(criteria, this.requireAuthentication);
	
//     uiRouter.transitionService.onBefore({to: 'app.**'}, (transition: Transition) => {
//   	    // let auth = transition.injector().get(guardServ);
//         // console.log('------guardServ.isLoggedIn()-------', guardServ.isLoggedIn());
//   	    if (!guardServ.isLoggedIn()) {
//   	        return transition.router.stateService.target('login');
//             // return stateService
//   	    }
//   	    return true;
        
//   	});
//   }
// }