import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

/** A hero's name can't match the given regular expression */
export function invalidEmailValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {

    const nameRe = new RegExp(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i);
    const name = control.value;
    if (name == '') {
      return null;
    }
    const no = !nameRe.test(name);
    return no ? {'invalidEmail': {name}} : null;
  };
}

@Directive({
  selector: '[invalidEmail]',
  providers: [{provide: NG_VALIDATORS, useExisting: InvalidEmailValidatorDirective, multi: true}]
})
export class InvalidEmailValidatorDirective implements Validator, OnChanges {
  @Input() invalidEmail: string;
  private valFn = Validators.nullValidator;

  ngOnChanges(changes: SimpleChanges): void {
    const change = changes['invalidEmail'];
    if (change) {
      const val: string | RegExp = change.currentValue;
      const re = val instanceof RegExp ? val : new RegExp(val, 'i');
      this.valFn = invalidEmailValidator();
    } else {
      this.valFn = Validators.nullValidator;
    }
  }

  validate(control: AbstractControl): {[key: string]: any} {
    return this.valFn(control);
  }
}



/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/