import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

/** A hero's name can't match the given regular expression */
export function invalidNumberValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {

    const nameRe = new RegExp(/^[0-9]+$/i);
    const name = control.value;
    if (name == '') {
      return null;
    }
    const no = !nameRe.test(name);
    return no ? {'invalidNumber': {name}} : null;
  };
}

@Directive({
  selector: '[invalidNumber]',
  providers: [{provide: NG_VALIDATORS, useExisting: InvalidNumberValidatorDirective, multi: true}]
})
export class InvalidNumberValidatorDirective implements Validator, OnChanges {
  @Input() invalidNumber: string;
  private valFn = Validators.nullValidator;

  ngOnChanges(changes: SimpleChanges): void {
    const change = changes['invalidNumber'];
    if (change) {
      const val: string | RegExp = change.currentValue;
      const re = val instanceof RegExp ? val : new RegExp(val, 'i');
      this.valFn = invalidNumberValidator();
    } else {
      this.valFn = Validators.nullValidator;
    }
  }

  validate(control: AbstractControl): {[key: string]: any} {
    return this.valFn(control);
  }
}



/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/