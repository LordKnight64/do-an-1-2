import { Component, Input } from '@angular/core';
import Utils from 'app/utils/utils';

@Component( {
    selector: 'app-aside',
    styleUrls: [ Utils.getView('app/widgets/control-sidebar/control-sidebar.component.css')],
    // templateUrl: './control-sidebar.component.html'
    templateUrl: Utils.getView('app/widgets/control-sidebar/control-sidebar.component.html')
})
export class ControlSidebarComponent {

  constructor() {
    
    // TODO
  }
}
