import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from 'ng2-translate';
import { MessagesService } from 'app/services/messages.service';
import { LoggerService } from 'app/services/logger.service';
import { Message } from 'app/models/message';
import Utils from 'app/utils/utils';

@Component( {
    /* tslint:disable */
    selector: '.messagesBox',
    /* tslint:enable */
    styleUrls: [ Utils.getView('app/widgets/messages-box/messages-box.component.css')],
    // templateUrl: './messages-box.component.html'
    templateUrl: Utils.getView('app/widgets/messages-box/messages-box.component.html')
})
export class MessagesBoxComponent implements OnInit {
    // Declaring the variable for binding with initial value
    private messages: Message[];
    private msgLength: {};

    constructor( private msgServ: MessagesService, private logger: LoggerService ) {
        this.messages = [];
    }

    public ngOnInit() {
        // Every incoming message changes entire local message Array.
        this.msgServ.messages.subscribe(( msg: Message[] ) => {
            this.logger.log( 'MsgBox', null, 'RECIEVED.MESSAGE', null );
            this.messages = msg;
            console.log('---------messages-------', this.messages);
            this.msgLength = { 0: this.messages.length };
        });
    }
}
