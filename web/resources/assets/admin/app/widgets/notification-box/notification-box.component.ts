import { Component, OnInit } from '@angular/core';
import { Message } from 'app/models/message';
import { MessagesService } from 'app/services/messages.service';
import Utils from 'app/utils/utils';

@Component( {
    /* tslint:disable */
    selector: '.notificationsBox',
    /* tslint:enable */
    styleUrls: [Utils.getView('app/widgets/notification-box/notification-box.component.css')],
    templateUrl: Utils.getView('app/widgets/notification-box/notification-box.component.html')
})
export class NotificationBoxComponent implements OnInit {

    private messages: Message[];
    private notifLength: {} = {0: '10'};

    constructor() {
        // TODO 
    }

    public ngOnInit() {
        // TODO
    }

}
