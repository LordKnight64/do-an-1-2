import { Component, Input } from '@angular/core';
import Utils from 'app/utils/utils';

@Component( {
    selector: 'app-footer',
    styleUrls: ['admin/app/widgets/app-footer/app-footer.component.css'],
    // templateUrl: './app-footer.component.html'
    templateUrl: Utils.getView('app/widgets/app-footer/app-footer.component.html')
})
export class AppFooterComponent {

    constructor() {
      // TODO
    }
}
