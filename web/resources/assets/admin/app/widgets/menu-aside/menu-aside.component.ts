import { Component, OnInit, Input } from '@angular/core';
import { User } from 'app/models/user';
// import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import Utils from 'app/utils/utils';

@Component({
  selector: 'app-menu-aside',
  styleUrls: [ Utils.getView('app/widgets/menu-aside/menu-aside.component.css')],
  // templateUrl: './menu-aside.component.html'
  templateUrl: Utils.getView('app/widgets/menu-aside/menu-aside.component.html')
})
export class MenuAsideComponent implements OnInit {
  private currentUrl: string;
  private currentUser: User = new User();
  private restaurant: any = {};
  @Input() private links: Array<any> = [];

  constructor(private userServ: UserService) {
    // getting the current url
    // this.router.events.subscribe((evt) => this.currentUrl = evt.url);
    // this.userServ.currentUser.subscribe((user) => this.currentUser = user);
  }

  public ngOnInit() {
    // TODO
     let userKey = JSON.parse(localStorage.getItem('user_key'));
     console.log('userKey.restaurants ',userKey.restaurants[0]);
     this.restaurant = userKey.restaurants[0];
  }

}

