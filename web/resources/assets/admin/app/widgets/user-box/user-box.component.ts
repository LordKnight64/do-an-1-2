import { Component, OnInit } from '@angular/core';
import {User} from 'app/models/user';
import {UserService} from 'app/services/user.service';
import { Router } from '@angular/router';
import Utils from 'app/utils/utils';

@Component({
  /* tslint:disable */
  selector: '.userBox',
  /* tslint:enable */
  styleUrls: [Utils.getView('app/widgets/user-box/user-box.component.css')],
  templateUrl: Utils.getView('app/widgets/user-box/user-box.component.html')
})
export class UserBoxComponent implements OnInit {
  private currentUser: User = new User();
  private restaurant: any = {};
  constructor(private userServ: UserService, private router: Router) {
    // se connecter au modif du user courant
      this.userServ.currentUser.subscribe((user: User) => this.currentUser = user);
  }

  public ngOnInit() {
    // TODO
     let userKey = JSON.parse(localStorage.getItem('user_key'));
     console.log('userKey.restaurants ',userKey.restaurants[0]);
     this.restaurant = userKey.restaurants[0];
    
  }

  private logout = (): void => {
    this.userServ.logout();
  }
}
