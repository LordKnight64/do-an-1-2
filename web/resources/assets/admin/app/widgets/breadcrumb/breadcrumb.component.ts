import { Component }  from '@angular/core';
// import { Router }             from '@angular/router';
import { BreadcrumbService }        from 'app/services/breadcrumb.service';
import Utils from 'app/utils/utils';

@Component({
  selector: 'app-breadcrumb',
  styleUrls: [ Utils.getView('app/widgets/breadcrumb/breadcrumb.component.css')],
  templateUrl: Utils.getView('app/widgets/breadcrumb/breadcrumb.component.html')
})
export class BreadcrumbComponent {
  private display: boolean = false;
  private header: string = '';
  private description: string = '';
  private levels: Array<any> = [];

  constructor(private breadServ: BreadcrumbService) {
    // getting the data from the services
    this.breadServ.current.subscribe((data) => {
      this.display = data.display;
      this.header = data.header;
      this.description = data.description;
      this.levels = data.levels;
      console.log('breadcrumb',this);
    });
  }

}
