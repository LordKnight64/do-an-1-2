import { Component, Input } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import Utils from 'app/utils/utils';

@Component( {
    selector: 'app-header',
    styleUrls: [ Utils.getView('app/widgets/app-header/app-header.component.css')],
    // templateUrl: './app-header.component.html'
    templateUrl: Utils.getView('app/widgets/app-header/app-header.component.html')
})
export class AppHeaderComponent {

  constructor(
  ) {
    // TODO
  }
}
