import { Component, OnInit, Input } from '@angular/core';
import { Message } from 'app/models/message';
import Utils from 'app/utils/utils';

@Component( {
    /* tslint:disable */
    selector: '.tasksBox',
    /* tslint:enable */
    styleUrls: [Utils.getView('app/widgets/tasks-box/tasks-box.component.css')],
    templateUrl: Utils.getView('app/widgets/tasks-box/tasks-box.component.html')
})
export class TasksBoxComponent implements OnInit {

    private messages: Message[];
    private tasksLength: {} = { 0: '9' };
    @Input() public user;

    constructor() {
        // TODO 
    }

    public ngOnInit() {
        // TODO
    }

}
