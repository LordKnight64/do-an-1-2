// import {AppComponent} from 'app/app.component';
// import {Ng2StateDeclaration, Transition} from 'ui-router-ng2';
// import { LayoutsAuthComponent } from './components/layouts/auth/auth';
// import { HomeComponent } from './components/home/home.component';
// import { ClientComponent } from './components/client/client.component';
// import { HeroesComponent }      from './components/heroes/heroes.component';
// import { LoginComponent } from './components/login/login.component';

// /** The top level state(s) */
// export let MAIN_STATES: Ng2StateDeclaration[] = [
//     // The top-level app state.
//     // This state fills the root <ui-view></ui-view> (defined in index.html) with the AppComponent
//     {
// 		name: 'app',
// 		url: '/',
// 		component: LayoutsAuthComponent
// 	},
//     {
// 	 	name: 'app.home',
// 	 	url: '/home',
// 	 	data: {
// 	        auth: true
// 	      },
// 	 	component: HomeComponent
// 	 },
//     {
// 	 	name: 'app.client',
// 	 	url: '/client',
// 	 	data: {
// 	        auth: true
// 	      },
// 	 	component: ClientComponent
// 	 },
//     {
// 	 	name: 'app.heroes',
// 	 	data: {
// 	        auth: true
// 	      },
// 	 	url: '/heroes',
// 	 	component: HeroesComponent
// 	 },
// 	 {
// 	 	name: 'login',
// 	 	url: '/login',
// 	 	component: LoginComponent
// 	 }
// ];