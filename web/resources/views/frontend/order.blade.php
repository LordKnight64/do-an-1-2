@extends('frontend.layouts.masterNoSlider')

@section('content')

		<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">{{$restaurant->name}}</h2>
	            <ol class="breadcrumb">
	                <li><i class="fa fa-map-marker " ></i> <a style="color: white; font-size:14px;" href="#">{{$restaurant->address}}</a></li>
	            </ol>
				<p style="color: white;"><i class="fa fa-phone " ></i> {{$restaurant->tel}}</p>
				<p style="color: white;"><i class="fa fa-clock-o  " ></i> {{date('G:i', strtotime($restaurant->open_time))}} AM - {{date('G:i', strtotime($restaurant->close_time))}} PM</p>
	           <a style="color: white;; font-size:14px;" title="click here to view about [ {{$restaurant->name}} ]" href="/about?id={{$restaurant->id}}"><i class="fa fa-info-circle" >	</i>&nbsp; About us </a> &nbsp;&nbsp; &nbsp;| &nbsp; &nbsp; &nbsp;<a title="click here to view contact[ {{$restaurant->name}} ]" style="color: white; font-size:14px;" href="/contact?id={{$restaurant->id}}"><i class="fa fa-envelope" >	</i>&nbsp; Contact us </a>
				<ol class="breadcrumb">
	                <li><a style="color: white" href="/">Home</a></li>
	                <li ><a style="color: white" href="search">Restaurant list</a></li>
					<li ><a style="color: white" href="restaurantDetail?id={{$restaurant->id}}"> Restaurant Detail  </a></li>
					<li style="color: red;">Order List</li>
	            </ol>
	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>

		<br>
		 <div class="default-heading">
                <!-- Crown image -->
                <img class="img-responsive" src="frontend/img/crown.png" alt="" />
                <!-- Heading -->
                <h2>Order List</h2>
                <!-- Paragraph -->

                <!-- Border -->
                <div class="border"></div>
            </div>


			<!-- Inner Content -->
			<div class="inner-page"">

				<!-- Shopping Start -->

				<div class="shopping" >
					<div class="col-md-3 col-sm-6">
						<div class="restaurant-categories-item">
								<ul>
									<form class="" action="/order" method="get">
										<li class="{{$active_category_all}}" onclick="clickLoading();event.preventDefault(); this.parentNode.submit()">All</li>
										<input type="hidden" name="restaurant_id" value="{{$restaurant_id}}" style="display:none">
										<input type="hidden" name="category_id" value="" style="display:none">
									</form>
									@foreach ($categories as $category)
									<form class="" action="/order" method="get">
										<li class="{{$category->active}}" onclick="clickLoading();event.preventDefault(); this.parentNode.submit()">{{$category->name}}</li>
										<input type="hidden" name="restaurant_id" value="{{$category->restaurant_id}}" style="display:none">
										<input type="hidden" name="category_id" value="{{$category->id}}" style="display:none">
									</form>
									@endforeach

								</ul>
							</div>
						</div>
					<div class="container" style="width:75%;margin-left:25%;">

						<!-- Shopping items content -->
						<div class="shopping-content">

							<div class="row">

								@foreach ($item as $i)

								<div class="col-md-4 col-sm-6">
									<!-- Shopping items -->
										<!--span class="hot-tag br-green">News</span-->
									<div class="shopping-item">
										<form action="/orderDetail" method="get">
										<!-- Image -->
										<a title="view food detail" href="#" onclick="event.preventDefault(); this.parentNode.submit()"><img class="img-responsive" src="{{$i->thumb}}" style="width:100%;height:215px" alt="" /></a>
										<!-- Shopping item name / Heading -->
										<a title="view food detail" href="#" class="pull-left" onclick="event.preventDefault(); this.parentNode.submit()"><h4><strong>{{$i->name}}</strong></h4></a>
										<span class="item-price pull-right">  {{$i->price}}
										@if ($i->discount != 0.0 )
											<span style="color:#888; font-weight:normal;text-decoration: line-through;">  {{($i->discount + 1) * $i->price}} </span>
										  @endif
										 </span>

										<div class="clearfix"></div>
										<!-- Paragraph -->
										<p class="ellipsis">{{$i->description}}.</p>
										<!-- Buy now button -->
										<div class="visible-xs">
											<a class="btn btn-danger btn-sm" href="#" onclick="event.preventDefault(); this.parentNode.submit()">Buy Now</a>
										</div>
										<!-- Shopping item hover block & link -->
										<div class="item-hover br-red hidden-xs"></div>

										<a title="add to cart" class="link hidden-xs" href="#"
										onclick="setShoppingCartItemInOrder({{json_encode($i)}}, {{json_encode($restaurant)}})"> Add To Cart  &nbsp;&nbsp; <i class="fa fa-cart-plus" style="color:white"></i>  </a>
										<span class="hot-tag br-red">Best</span>
										<input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
										<input type="hidden" name="item_id" value="{{$i->id}}">
									</form>
									</div>
								</div>
								@endforeach

							</div>
							<!-- Pagination -->
							<div class="shopping-pagination">
								<!-- <ul class="pagination">
									<li class="disabled"><a href="#">&laquo;</a></li>
									<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a href="#">&raquo;</a></li>
								</ul> -->
								{{$item->appends(Request::capture()->except('page'))->fragment('main')->links() }}
							</div>
							<!-- Pagination end-->
						</div>
					</div>
				</div>

				<!-- Shopping End -->



			</div><!-- / Inner Page Content End -->

			<!-- Footer Start -->
			@stop
		 @section('script-screen')
		 <script src="{{'/frontend/js/pages/order.js'}}"></script>
		 
		 @stop
