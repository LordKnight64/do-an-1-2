@extends('frontend.layouts.masterNoSlider')

@section('content')

		<!-- Banner Start -->
	    <div class="banner padd" style="padding-bottom:25px">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">{{$topTittle}}</h2>
							<ol class="breadcrumb">
	                <li><a style="color: #009688;" href="#">Home</a></li>
	                <li style="color:red">{{$topTittle}}</li>
	            </ol>
	           <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
		<br>



			<!-- Inner Content -->
			<div class="inner-page" style="padding-left:3%;padding-right:3%;">

				<!-- Shopping Start -->

				<div class="shopping" >　
						<!-- Shopping items content -->
						<div class="shopping-content">

							<div class="row">

								@foreach ($item as $i)
								<div class="col-md-3 col-sm-6">
									<!-- Shopping items -->
									<div class="shopping-item">
										<form action="/orderDetail" method="get">
										<!-- Image -->
										<a title="view food detail" href="#" onclick="event.preventDefault(); this.parentNode.submit()"><img class="img-responsive" src="{{$i->thumb}}" style="width:100%;height:215px" alt="" /></a>
										<!-- Shopping item name / Heading -->
										<a title="view food detail" href="#" class="pull-left" onclick="event.preventDefault(); this.parentNode.submit()"><h4><strong>{{$i->name}}</strong></h4></a>
										<span class="item-price pull-right">${{$i->price}}</span>
										<div class="clearfix"></div>
										<!-- Paragraph -->
										<p class="ellipsis">{{$i->description}}.</p>
										<!-- Buy now button -->
										<div class="visible-xs">
											<a class="btn btn-danger btn-sm" href="#" onclick="event.preventDefault(); this.parentNode.submit()">Buy Now</a>
										</div>
										<!-- Shopping item hover block & link -->
										<div class="item-hover br-red hidden-xs"></div>

										<a title="Add to cart" class="link hidden-xs" href="#"
										onclick="setShoppingCartItemInItemDished({{json_encode($i)}})">Add To Cart  &nbsp;&nbsp; <i class="fa fa-cart-plus" style="color:white"></i>  </a>
										<span class="hot-tag br-red">{{$tag}}</span>
										<input type="hidden" name="restaurant_id" value="{{$i->restaurant_id}}">
										<input type="hidden" name="item_id" value="{{$i->id}}">
									</form>
									</div>
								</div>
								@endforeach

							</div>
							<!-- Pagination -->

							<!-- Pagination end-->
						</div>
					</div>
				</div>

				<!-- Shopping End -->



			</div><!-- / Inner Page Content End -->

			<!-- Footer Start -->
			@stop
		 @section('script-screen')
		 <script src="{{'/frontend/js/partials/item_dished.js'}}"></script>
		 @stop
