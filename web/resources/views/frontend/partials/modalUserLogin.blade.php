<div class="modal fade" id="modal-user-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  
	<div class="modal-dialog">
        <div class="modal-content">

		    <div class="inner-page" id='main-login'>
		       <div class='login-form-container'>
		       		<div >
					   	<div class="row">
			        		<div class="col-md-6 col-sm-6 col-xs-6">
					        	<div class='login-form'>
									<h5 style=" margin-top: 0px;
									margin-bottom: 25px;
									border-bottom: red solid;
									padding-bottom: 5px;
									width: 130px;
									font-size: 16px;"> Login with Email </h5>
									
									<form  action="" role="form" id="user-login-form">
									       <div class="row">
												<div class="col-md-1 col-sm-1 col-xs-1" >
														<span style=" line-height: 32px; padding-left: 30px;" class="fa fa-envelope form-control-feedback"></span>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10" >
													<div class="form-group">
												
														<input name="email" data-validation="email"  class="form-control" placeholder="Email...">
													</div>
												</div>
											</div>

											  <div class="row">
												<div class="col-md-1 col-sm-1 col-xs-1" >
													 <span style=" line-height: 32px; padding-left: 30px;" class="glyphicon glyphicon-lock form-control-feedback"></span>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10" >
														<div class="form-group">
														
															<input type="password" class="form-control" name="password" id="password" placeholder="Password" data-validation="required | length" data-validation-length="min6">

														</div>
												</div>
												
											</div>
											
											<div class="row">
												<div class="col-md-1 col-sm-1 col-xs-1" >
													
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10" >
													<div id="msg-error-login" class="form-error alert alert-danger" style="display: none;">
														<strong>Form submission failed!</strong>
													</div>
												</div>
												
											</div>
										   <div class="row">
												<div class="col-md-1 col-sm-1 col-xs-1" >
													
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10" >
													<button style=" background: #FF5722;border: 1px solid #FF5722;width: 140px;" class="btn btn-primary pull-right" type="submit"><i class="fa  fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;Login</button>
												</div>
												
											</div>
											
											
											<div class='clearfix'> </div>
										</form>
								</div>
				        		</div>
									<div class="col-md-6 col-sm-6 col-xs-6 "  style="border-left:1px solid #eee">
											<div class="row">
											   <div class="col-md-2 col-sm-2 col-xs-2" >
											   </div>
												<div class="col-md-8 col-sm-8 col-xs-8" >
													<span >	If you don't already have an account click the button below to create your account.
													</span>
												</div>
												<div class="col-md-1 col-sm-1 col-xs-1" >
											   </div>
											</div>
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12" >
													<a href="/userRegister" style=" width: 70%; margin-left: 15%; margin-top: 10px;"  class='btn btn-success'>Register</a></span>
												</div>
											</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12" >
													<h5 style="text-align:center; color:#666">  ---------------------- Or --------------------  </h5>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12" >
												<button onclick="loginFacebook()" style=" background: #1b4072; border: 1px solid #102e57; width: 70%; margin-left: 15%; margin-top: 10px;"  class='btn btn-primary'><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;<span> Login with facebook </span> </button>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12" >
												<button id="btnGoogleSocial" style=" background:red; color:white !important; width: 70%; margin-left: 15%; margin-top: 10px;"  class='btn btn-danger'><i class="fa fa-google-plus" aria-hidden="true"></i>&nbsp;&nbsp;<span>Login with google+ </span></button>
											</div>
										</div>
								</div>
				        	</div>
		        		</div>
									@if(Request::get('id') != null )
									<input name="changepass" id="changepass" type="hidden" value ="cp">
									@endif
						<div  class="row" style="margin-right: -20px;margin-left: -20px; background:#eee; height:50px; line-height:50px;text-align:center; margin-top: 50px;">
							<div class="col-md-12 col-sm-12 col-xs-12">
								So you can't get in your account? Did you <a href="{{route('user.password.request')}}" style="color: #009688;font-weight: 500;text-decoration: underline;"> forget your password? </a>
							</div>
						</div>

		       		
		       		</div>
			   </div>
		    </div>
        </div>

    </div>
</div>


