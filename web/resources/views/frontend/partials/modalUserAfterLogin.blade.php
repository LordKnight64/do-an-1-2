<div class="chathead">
    <nav class="chatheadButton" role="navigation" id="chatheadButton">

    <!-- A container for menu items -->
        <div class="container-fluid">

        <!-- A list of items that will float right -->
        <ul class="nav navbar-nav navbar-right">

            <!-- Your dropdown menu item -->
            <li class="dropdown">
            <!-- Define your dropdown menu item -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <div class="name-chat-head">
                <label class="dropdown-toggle-label" id = "userNameLogin"> {{ Auth::check()? Auth::user()->first_name : 'Login'}}</label>
              </div>
                <input type="hidden" id = "userIdLogin" value ="{{ Auth::check()? Auth::user()->id : ''}}"> </input>

            </a>
            <!-- The list of dropdown menu items -->
			<ul class ="dropdown-menu" id="drawRestaurantHtml"> </ul>

            </li>
        </ul>
        </div>
    </nav>
    </div>
	<input id="check_user" type="hidden" value="{{ Auth::check()? json_encode(Auth::user()) : ''}}" />
	<input id="retaurantLoginLst" type="hidden" value='<?php echo (session("restaurantLoginList")); ?>' />
