<!-- Header Start -->
<div class="header">

	<!-- <div class="header-login-bar" >
		<div  style="display: {{ Auth::check() ? 'block': 'none' }}" id="header-welcome-dropdown" >
			<a href="/logout"  class="pull-right login-bar" style="margin-left:20px;margin-right:20px;margin-top: 2px;"><strong>Logout</strong></a>
			<a href="/profile" class="pull-right login-bar" style="margin-left:10px;"><strong>Welcome</strong> <span id="login-name">{{  Auth::check()? Auth::user()->first_name : ''}} </span> </a>
		 </div>
		 <a href="#" id="header-register-login1" class="pull-right login-bar" style="display:{{ Auth::check() ? 'none': 'block' }};margin-right:10px;font-weight:bold;" data-toggle="modal" data-target="#modal-user-login">User Login</a>
		<a href="#" id="header-register-login2" class="pull-right login-bar" style="display:{{ Auth::check() ? 'none': 'block' }};margin-right:10px;font-weight:bold;" data-toggle="modal" data-target="#modal-user-login">User Register</a>

	</div> -->

  <div class="container menu-bar" style="height: 63px;">
      <div class="row">
          <div class="col-md-3 col-sm-5">
              <!-- Link -->
              <a href="/">
                  <!-- Logo area -->
                  <div class="logo" id="logo-header">
                      <img class="img-responsive" src="frontend/img/logo.png" alt="" />
                      <!-- Heading -->
                      <h1>iFoodGo</h1>
                      <!-- Paragraph -->
                      <p>Món ngon bên bạn</p>
                  </div>
              </a>
          </div>

          <div class="col-md-9 col-sm-7">
              <!-- Navigation -->
              <nav class="navbar navbar-default navbar-right" role="navigation">
                  <div class="container-fluid">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                          </button>
                      </div>
                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav max-width-150-li">
							<li>
									<a href="/" style="border-bottom: {{session('path')=='index' ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} ">{{trans('admin/labels.home.home')}}</a>
							</li>

							@if(session('path')== 'restaurant' || session('path')== 'about'|| session('path')== 'userLogin' || session('path')== 'restaurantLogin'|| session('path')=='contact')
							<li>
								<form method="get" action="/about">
									<a href="#" onclick="event.preventDefault(); this.parentNode.submit()"  style="border-bottom: {{session('path')=='about' ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "><img src="" class="img-responsive" alt="" /> About</a>
									<input name="id" type="text" style="display:none;position:absolute" value="">
										</form>
							</li>
							@endif


							<li>
									<a href="/features" style="border-bottom: {{session('path')=='features' ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "> Features</a>

							</li>
							@if(session('path')!= 'restaurant' && session('path')!= 'about' && session('path')!= 'userLogin' && session('path')!= 'restaurantLogin' && session('path')!= 'contact')
							<li>
									<a href="/howitswork" style="border-bottom: {{session('path')=='howitswork' ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "> How It Works</a>
							</li>
							<li>
									<a href="/addRestaurant" style="border-bottom: {{session('path')=='addRestaurant' ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "> Add Your Restaurant </a>
							</li>
							@endif
							<li>
								<form method="get" action="/contact">
									<a href="#" onclick="event.preventDefault(); this.parentNode.submit()"  style="border-bottom: {{session('path')=='contact'||session('path')=='contact1'  ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "> Contact</a>
									<input name="id" type="text" style="display:none;position:absolute" value="{{$restaurant->id or ''}}">
								</form>
							</li>
							<li>
								<form method="get" action="/notificationList">
									<a href="#" onclick="event.preventDefault(); this.parentNode.submit()"  style="border-bottom: {{session('path')=='notificationList'  ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "> News</a>
									<input name="restaurant_type" type="text" style="display:none;position:absolute" value=""><!-- {{$restaurant->id or ''}} -->
									<input name="inputContent" type="text" style="display:none;position:absolute" value="">
								</form>
							</li>
							<li>
								<nav class="" role="navigation"> 
								<!-- A container for menu items -->
									<div class="container-fluid">

									<!-- A list of items that will float right -->
									<ul class="nav navbar-nav navbar-right" style="margin-top: 0px;">

										<!-- Your dropdown menu item -->
										<li class="dropdown"> 

										<!-- Define your dropdown menu item -->
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<label class="dropdown-toggle-label" id = "localizeChossen"> Language</label>
										</a>

										<!-- The list of dropdown menu items -->
										<ul class ="dropdown-menu"> 
											<li><a onclick="chooseLang('en')">English</a></li>
											<li><a onclick="chooseLang('vi')">Việt Nam</a></li>
											<li><a onclick="chooseLang('ja')">japan</a></li>
										</ul>	

										</li>
									</ul>
									</div>
								</nav>
							</li>
							<!-- <li style="display: {{ Auth::check() ? 'block': 'none' }}" id="header-welcome-dropdown">
								 <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="border-bottom: {{session('path')=='index' ? '#da1e20  2px solid;color:#da1e20  !important' : 'rgba(0,0,0,0) 2px solid'}} "> Welcome <span id="login-name">{{  Auth::check()? Auth::user()->first_name : ''}} </span> </a>
									<ul class="dropdown-menu">
									 <li><a href="/profile">My Profile</a></li>
									 <li><a href="/historyOrder">My Orders</a></li>
									 <li><a href="/logout">Log Out</a></li>

									</ul>
							 </li> -->

							 <!--li  style="display: {{ Auth::check() ? 'none': 'block'}}" id="header-login" data-toggle="modal" data-target="#modal-user-login">
								<a href="#" style="color: {{session('path')=='userLogin' ? '#da1e20 ' : ''}}">User Login</a>
							</li-->

						 @if(session('path')== 'restaurant' || session('path')=='userLogin' || session('path')=='restaurantLogin' || session('path')== 'about' || session('path')== 'contact')
						 <li>
								 <a href="https://restaurant.ifoodgo.vn" style="border-bottom: {{session('path')=='restaurantLogin' ? '#da1e20  2px solid' : ''}} "><img src="" class="img-responsive" alt="" /> Restaurant login</a>

						 </li>
						 @endif
						 <li>
							 <!-- Button Kart -->
					 		<div class="btn-cart-md btn-cart-md-order" id='btn-cart-md-order' >
					 			 <a class="cart-link cart-link-order pull-right" href="#">
					 					<!-- Image -->
					 					<i  style="font-size: 20px; color: white;" class="fa fa-cart-arrow-down"></i>
					 					<!-- Heading -->
					 					<span id="total_item"></span>
					 					<div class="clearfix"></div>
					 			 </a>
					 			 <ul id="list-item-in-shoppingcart"  class="cart-dropdown cart-dropdown-order" role="menu" >
					 			 </ul>
					 			 <div class="clearfix"></div>
					 		</div>
					 		<div class='clearfix'> </div>
						 </li>
                          </ul>
                      </div>
                      <!-- /.navbar-collapse -->
                  </div>
                  <!-- /.container-fluid -->
              </nav>
          </div>
      </div>
  </div>
  <!-- / .container -->
</div>
<input id="isSubscribed" type="hidden" value='<?php echo (session("isSubscribed")); ?>' />
<!-- Header End -->
