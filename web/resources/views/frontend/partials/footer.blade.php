		    <!-- Footer Start -->
		    <div class="footer padd">
		        <div class="container">
		            <div class="row">
					<div class="col-md-3 col-sm-6">
		                    <!-- Footer widget -->
		                    <div class="footer-widget">
		                        <!-- Logo area -->
		                        <div class="logo">
		                            <img class="img-responsive" src="frontend/img/logo.png" alt="" />
		                            <!-- Heading -->
		                            <h1>iFoodGo</h1>

									<p style="padding-top:10px"> Đăng ký để nhận thông tin khuyến mãi từ các nhà hàng</p>
		                        </div>
		                        <!-- Paragraph -->

								 <form role="form" id="subscribe">
								 	{{ csrf_field() }}
		                            <div class="form-group">

		                                <input name="email_sub" class="form-control" type="text" placeholder="Your email" data-validation="email|length|required" data-validation-length="max1536"/>
		                            </div>
		                            <button class="btn btn-danger" type="submit">Subscribe</button>
		                        </form>
		                    </div>

		                    <!--/ Footer widget end -->
		                </div>
					 <div class="col-md-3 col-sm-6">
		                    <!-- Footer widget -->
		                    <div class="footer-widget">
		                        <!-- Heading -->
		                        <h4>Useful Links</h4>
		                        <!-- Images -->
		                        <!-- Heading -->
								<div class="fbox-nav">
                                    <ul>
                                        <li><a href="/addRestaurant">Add Your Restaurant</a></li>
										 <li><a href="/userRegister">User Register</a></li>
                                        <li><a href="http://restaurant.ifoodgo.vn" target="_blank">Restaurant Login</a></li>
										<li><a href="#" data-toggle="modal" data-target="#modal-user-login">User Login</a></li>
                                        <li><a href="/termandcondition">Terms and Conditions</a></li>
                                        <li><a href="/privatepolicy"> Private Policy</a></li>
                                        <li><a href="/howitswork">How it Works</a></li>
										     <li><a href="/sitemap">Sitemap</a></li>
										<li><a href="/faq">FAQ</a></li>
                                    </ul>
                                </div>
							</div>
		                    <!--/ Footer widget end -->
		                </div>


		                <div class="clearfix visible-sm"></div>
		                <div class="col-md-3 col-sm-6">
		                    <!-- Footer widget -->
		                    <div class="footer-widget">
		                        <!-- Heading -->
		                        <h4>Join Us Today</h4>
		                        <!-- Paragraph -->
		                        <p>There is no one who loves pain itself, who seeks after it and wants to have it.</p>
		                        <!-- Subscribe form -->
		                        <form role="form" id="demo">
															{{ csrf_field() }}
		                            <div class="form-group">
		                                <input name="name" class="form-control" type="text" placeholder="Your name" data-validation="length|required" data-validation-length="max256"/>
		                            </div>
		                            <div class="form-group">
		                                <input name="email" class="form-control" type="text" placeholder="Your email" data-validation="email|length|required" data-validation-length="max1536"/>
		                            </div>
		                            <button class="btn btn-danger" type="submit">Request Demo</button>
		                        </form>
		                    </div>
		                    <!--/ Footer widget end -->
		                </div>
		                <div class="col-md-3 col-sm-6">
		                    <!-- Footer widget -->
		                    <div class="footer-widget">
		                        <!-- Heading -->
		                        <h4>Contact Us</h4>
		                        <div class="contact-details">
		                            <!-- Address / Icon -->
		                            <i class="fa fa-map-marker br-red"></i> <span>Lầu 4, Etown 1, 364 Cộng Hòa, <br />Phuong 13, Q.Tân Bình,<br />Tp. Ho Chi Minh</span>
		                            <div class="clearfix"></div>
		                            <!-- Contact Number / Icon -->
		                            <i class="fa fa-phone br-green"></i> <span>+84 8 3810 3385</span>
		                            <div class="clearfix"></div>
		                            <!-- Email / Icon -->
		                            <i class="fa fa-envelope-o br-lblue"></i> <span><a href="#">info@system-exe.com.vn</a></span>
		                            <div class="clearfix"></div>
		                        </div>
		                        <!-- Social media icon -->
		                        <div class="social">
		                            <a href="https://vi-vn.facebook.com/pages/System-EXE-Viet-Nam/303609016342170" class="facebook"><i class="fa fa-facebook"></i></a>
		                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
		                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
		                            <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
		                        </div>
		                    </div>
		                    <!--/ Footer widget end -->
		                </div>
		            </div>
		            <!-- Copyright -->
		            <div class="footer-copyright">
		                <!-- Paragraph -->
		                <p>© 2017 SystemEXE VN Co., Ltd. All rights reserved <a href="#"></a></p>
		            </div>
		        </div>
		    </div>
		    <!-- Footer End -->
