            <!-- Slider Start
                    #################################
                        - THEMEPUNCH BANNER -
                    #################################   -->
            <div class="tp-banner-container" style="margin-top: 75px;">
                @if(session('hasSearch')!='yes')
                <div class="bg-search" style="background: rgba(220, 217, 217, 0.51);width: 100%;height: 75px;margin-top: -73px;position: absolute;"></div>
                <form class="search-form" method="get" action="/search">
  			<select name="province" class="select-search" id="searchOption" >
  					  <option value="">Chọn Quận</option>
              @foreach ($districts as $district)
              <option value="{{$district->province}}">{{$district->province}}</option>
              @endforeach
  			</select>

  		  <input name="key" style="box-shadow: 12px 13px 12px rgba(0, 0, 0, 0.375) !important;" type="text" onclick="clickSearch()" onblur="blurSearch()" id="search" class="typeaheadIndex form-control out-focus-search"   autofocus="" autocomplete="off" placeholder="Tìm kiếm nhà hàng...">
        <a  class="red map-btn-search"  data-toggle="modal" onclick='openMap()' data-target="#currentLocationModal"><i class="fa  fa-map-marker fa-2x" aria-hidden="true"></i></a>

        <div class="bg-search-btn" style="background:0"><button type="submit" class="btn-search" onclick="clickLoading()">GO
    </button></div>
  </form>
@show
@endif

                <div class="tp-banner"  onclick="blurSearch()">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                            <!-- MAIN IMAGE -->
                            <img src="frontend/img/slider/slide2.jpg" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            <!--div class="tp-caption lfl largeblackbg br-red" data-x="20" data-y="50" data-speed="1500" data-start="1200" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" style="z-index: 3">We Make Delicious...
                            </div-->
                            <!-- LAYER NR. 2.0 -->
                            <div class="tp-caption lfl medium_bg_darkblue br-green" data-x="20" data-y="100" data-speed="1500" data-start="1800" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Power4.easeIn" data-captionhidden="off">Tìm nhà hàng yêu thích của bạn
                            </div>
                            <!-- LAYER NR. 2.1 -->
                            <div class="tp-caption lfl medium_bg_darkblue br-lblue" data-x="20" data-y="170" data-speed="1500" data-start="2100" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" style="z-index: 3">Xem và đặt món yêu thích
                            </div>
                            <!-- LAYER NR. 2.2 -->
                            <div class="tp-caption lfl medium_bg_darkblue br-purple" data-x="20" data-y="240" data-speed="1500" data-start="2400" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" style="z-index: 3">Trả tiền khi nhận hàng
                            </div>
                            <!-- LAYER NR. 2.3 -->
                            <div class="tp-caption lfl medium_bg_darkblue br-orange" data-x="20" data-y="310" data-speed="1500" data-start="2700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" style="z-index: 3">Thưởng thức món
                            </div>
                            <div class="tp-caption lfl medium_bg_darkblue br-red" data-x="20" data-y="380" data-speed="1500" data-start="3000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" style="z-index: 3">Chấm điểm để nhận điểm thưởng
                            </div>

                            <!-- LAYER NR. 3.0 -->
                            <div class="tp-caption customin customout" data-x="right" data-hoffset="-50" data-y="100" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="400" data-start="3600" data-easing="Power3.easeInOut" data-endspeed="300" style="z-index: 5"><img class="slide-img img-responsive" src="frontend/img/slider/s21.png" alt="" />
                            </div>
                            <!-- LAYER NR. 3.1 -->
                            <div class="tp-caption customin customout" data-x="right" data-hoffset="-120" data-y="130" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="400" data-start="3900" data-easing="Power3.easeInOut" data-endspeed="300" style="z-index: 6"><img class="slide-img img-responsive" src="frontend/img/slider/s22.png" alt="" />
                            </div>
                            <!-- LAYER NR. 3.2 -->
                            <div class="tp-caption customin customout" data-x="right" data-hoffset="-10" data-y="160" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="400" data-start="4200" data-easing="Power3.easeInOut" data-endspeed="300" style="z-index: 7"><img class="slide-img img-responsive" src="frontend/img/slider/s23.png" alt="" />
                            </div>
                            <!-- LAYER NR. 3.3 -->
                            <div class="tp-caption customin customout" data-x="right" data-hoffset="-80" data-y="190" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="400" data-start="4500" data-easing="Power3.easeInOut" data-endspeed="300" style="z-index: 8"><img class="slide-img img-responsive" src="frontend/img/slider/s24.png" alt="" />
                            </div>

                        </li>


                        <li data-transition="cube" data-slotamount="7" data-masterspeed="600">
                            <!-- MAIN IMAGE -->
                            <img src="frontend/img/slider/slide2.jpg" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat">
                            <!-- LAYERS NR. 1 -->
                            <div class="tp-caption lfl" data-x="110" data-y="130" data-speed="800" data-start="1500" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Power4.easeIn" data-captionhidden="off"><img src="frontend/img/slider/s31.png" class="img-responsive" alt="" />
                            </div>
                            <!-- LAYERS NR. 2 -->
                            <div class="tp-caption lfl" data-x="80" data-y="265" data-speed="800" data-start="2200" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Power4.easeIn" data-captionhidden="off"><img src="frontend/img/slider/s33.png" class="img-responsive" alt="" />
                            </div>
                            <!-- LAYERS NR. 3 -->
                            <div class="tp-caption lfl" data-x="450" data-y="312" data-speed="800" data-start="2700" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Power4.easeIn" data-captionhidden="off"><img src="frontend/img/slider/s34.png" class="img-responsive" alt="" />
                            </div>
                            <!-- LAYERS NR. 4 -->
                            <div class="tp-caption sfr  thinheadline_dark white" data-x="right" data-hoffset="-10" data-y="90" data-speed="800" data-start="3200" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeIn" style="z-index: 3">Online
                            </div>
                            <!-- LAYERS NR. 4.1 -->
                            <div class="tp-caption lfr largepinkbg br-green" data-x="right" data-hoffset="-10" data-y="135" data-speed="800" data-start="3500" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Linear.easeNone" data-captionhidden="off">Cài đặt nhanh
                            </div>
                            <!-- LAYERS NR. 5 -->
                            <div class="tp-caption skewfromright medium_text text-right paragraph" data-x="right" data-hoffset="-10" data-y="225" data-speed="800" data-start="3800" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power4.easeOut" data-captionhidden="off">Đặt hàng trực tuyến <br> Quản lý thực đơn <br> Quản lý đơn đặt hàng <br> Xúc tiến

                            </div>
                            <!-- LAYERS NR. 6 // -->
                            <div class="tp-caption lfr modern_big_redbg br-red" data-x="right" data-hoffset="-10" data-y="335" data-speed="1500" data-start="4100" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Linear.easeNone" data-captionhidden="off">Desktop or Laptop
                            </div>
                            <!-- LAYERS NR. 6.1 // -->
                            <div class="tp-caption lfr modern_big_redbg br-yellow" data-x="right" data-hoffset="-10" data-y="395" data-speed="1500" data-start="4400" data-easing="Power4.easeOut" data-endspeed="300" data-endeasing="Linear.easeNone" data-captionhidden="off">Tablet or Phone
                            </div>
                        </li>
                    </ul>
                    <!-- Banner Timer -->
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
            <!-- Slider End -->
