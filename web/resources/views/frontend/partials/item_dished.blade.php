<div class="col-md-3 col-sm-6">
    <div class="dishes-item-container">
        <!-- Image Frame -->
        <div class="img-frame">
            <!-- Image -->
            <img src="{{$item->thumb}}" class="img-responsive" alt="" />
            <!-- Block for on hover effect to image -->
            <div class="img-frame-hover">
              <form action="/orderDetail" method="get">
                <!-- Hover Icon -->
                <a href="#" onclick="event.preventDefault(); this.parentNode.submit()"><i class="fa fa-cutlery"></i></a>
                <a id="add-shopping-cart" onclick="setShoppingCartItemInItemDished({{json_encode($item)}})"><i class="fa fa-cart-plus"></i></a>
            </div>
            <input type="text" name="item_id" value="{{$item->id}}" style="display:none">
            <input type="text" name="restaurant_id" value="{{$item->restaurant_id}}" style="display:none">
          </form>
        </div>
        <!-- Dish Details -->
        <div class="dish-details">
          <form action="/orderDetail" method="get">
            <!-- Heading -->
            <h3>{{$item->name}}</h3>
            <!-- Paragraph -->
            <p>{{$item->description}}.</p>
            <!-- Button -->
            <a href="#" onclick="event.preventDefault(); this.parentNode.submit()" class="btn btn-danger btn-sm">Read more</a>
            <input type="text" name="item_id" value="{{$item->id}}" style="display:none">
            <input type="text" name="restaurant_id" value="{{$item->restaurant_id}}" style="display:none">
            </form>
        </div>
    </div>
</div>
