@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
 <!-- Banner Start -->
	    <div class="banner banner1 padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">News</h2>
	            <ol class="breadcrumb">
	                <li><a style="color: #009688;" href="/">Home</a></li>
	                <li style="color: red;">News</li>
	            </ol>
			
	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
		<div class="testimonial padd restaurant-banner">
		    <div class="container">      


			<!-- Inner Content -->
			<div class="inner-page">
				<!-- Blog Start -->

				<div class="blog">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<!-- The new post done by user's all in the post block -->
								<div class="blog-post">
									@foreach ($data['news'] as $n)
										<!-- Entry is the one post for each user -->
										<div class="entry">
										<!-- Post Images -->
											<div class="blog-img pull-left">
												<img style="height:300px" src="{{$n->thumb}}" class="img-responsive img-thumbnail" alt="News thumb" />
											</div>
										<!-- Meta for this block -->
											<div class="meta">
												<i class="fa fa-calendar"></i>&nbsp; {{$n->cre_ts}}

											</div>
										<!-- Heading of the  post -->
											<h3><a style="color:#009688" href="/newsDetail?news_id={{$n->id}}">{{$n->title}}</a></h3>
											<hr  /><!-- Horizontal line -->
										<!-- Paragraph -->
											<div class="newsContent" >
												<p class="newsContents">{{$n->content}}</p>
											</div>
											<div class="clearfix"></div>
										</div>
									@endforeach

									<!-- Pagination -->
									<div class='page-result '>
										{{ $data['news']->appends(Request::capture()->except('page'))->fragment('main')->links() }}
										<div class='clearfix'> </div>
        							</div>
									
									<!-- Pagination end-->
								</div>
							</div> <!--/ Main blog column end -->
							<div class="col-md-4">
								<!-- Blog page sidebar -->
								<div class="blog-sidebar">
									<div class="row">
										<div class="col-md-12 col-sm-6">
											<!-- Blog sidebar page widget -->
											<div class="sidebar-widget">
												<!-- Widget Heading -->
												<h4>Search</h4>
												<!-- search button and input box -->
												<form role="form" method="get" class="form-inline" action="/notificationList">
													<div class="input-group" style="width:100%">
														<input type="text" name="restaurant_type" style="display:none;position:absolute" value="{{$saveData['restaurant_type_tag']}}">
														<input type="text" name="inputContent" class="form-control" placeholder="Type to search" value="{{$saveData['searchContent']}}">
														
														<span class="input-group-btn">
															<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
														</span>
													</div>
												</form><!--/ Form end -->
											</div><!--/ Widget end -->
										</div>
										<div class="col-md-12 col-sm-6">
											<!-- Blog sidebar page widget -->
											<div class="sidebar-widget">
												<!-- Widget Heading -->
												<h4>Recent Post</h4>
												<!-- Recent post list -->
												<ul class="list-unstyled">
													@foreach($data['recentNews'] as $rn)
														<li><i class="fa fa-angle-double-right"></i> <a href="/newsDetail?news_id={{$rn->id}}">{{$rn->title}}</a>
														<br />
														<div id="recentContent" style="white-space:nowrap;overflow:hidden">
															<p>{{$rn->content}}</p>
														</div>
														</li>
													@endforeach
												</ul>
											</div><!--/ Widget end -->
										</div>
										<div class="col-md-12 col-sm-6">
											<!-- Blog sidebar page widget -->
											<div class="sidebar-widget">
												<!-- Widget Heading -->
												<h4>Tags</h4>
												<div class="tag">
												@foreach($data['tagType'] as $t)
													<a href="notificationList?restaurant_type={{$t->cd}}&inputContent={{$saveData['searchContent']}}" class="{{$t->color}}">{{$t->cd_name}}</a>
												@endforeach
												</div>
											</div><!--/ Widget end -->
										</div>

									</div><!--/ Inner row end -->
								</div><!--/ Page sidebar end -->
							</div>
						</div><!--/ Row end -->
					</div>
				</div>

				<!-- Blog End -->




			</div><!-- / Inner Page Content End -->

			<!-- Footer Start -->
			@stop
			@section('script-screen')
				<script src="{{'/frontend/js/pages/notificationList.js'}}"></script>
			@stop
