@extends('frontend.layouts.masterNoTemplate')

@section('content')
 <div class="container menu-bar" style="height: 63px;">
	<div class="logo" id="logo-header" style="text-align: center;">
		<img class="img-responsive" src="frontend/img/logo.png" alt="" style="display: inline-block;max-width: 50px;margin-top: 5px;"/>
		<!-- Heading -->
		<h1 style="text-align: center;">iFoodGo</h1>
		<!-- Paragraph -->
	</div>
	    <!-- Inner Content -->
	    <div class="inner-page padd">
	        <!-- About company -->
	        <div class="about-company ">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-12">
	                        <!-- About Compnay Item -->
	                        <div class="about-company-item">
	                            <!-- About Company Image -->
								<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: center;">Hê thống ifoodGo đang gửi mail đến cho bạn. Xin vui lòng kiểm tra mail.</p>								
								<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: center;">Trong trường hợp không có mail cần trợ giúp, hãy gọi cho chúng tôi theo số  +84 8 3810 3385. Hoặc gửi thư vào hộp thư ifoodgo-support@system-exe.com.vn.</p>
	                        </div>
	                    </div>
	                    <a href="/" class="btn btn-danger" type="submit">Quay về trang chủ</a>
	                </div>
	            </div>
	        </div>
	    </div>
</div>
@stop
@section('script-screen')
	<script src="{{'/frontend/js/pages/sendMail.js'}}"></script>
@stop