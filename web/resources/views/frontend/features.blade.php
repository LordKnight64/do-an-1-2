@extends('frontend.layouts.masterNoSlider')

@section('content')
  <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white"> Features</h2>
	            <ol class="breadcrumb">
	                <li ><a style="color:white" href="/">Home</a></li>
	                <li style="color: red">Features</li>
	            </ol>
	           <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
		    <!-- Main Content -->
		    <div class="main-content">
		        <div class='features padd'>
		        	<div class='container'>
		        		<div class="row">
			        		<div class="col-md-8 col-sm-8 col-xs-8">
					        	<div class='features-info'>
					        		<h3> TĂNG LƯỢNG KINH DOANH </h3>
					        		<span>Tối đa hóa tiềm năng kinh doanh của bạn thông qua tăng tiếp xúc với khách hàng đang có nhu cầu ăn uống </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Danh sách nhà hàng của bạn cho miễn phí </li>
						        			<li><i class='fa fa-check-circle'> </i> Đường dẫn (URL) cho nhà hàng của bạn là duy nhất </li>
						        			<li><i class='fa fa-check-circle'> </i> Tăng sự hiện diện nhà hàng của bạn đến những người yêu thích ẩm thực </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				        		<img class="img-responsive" src="frontend/img/features/INCREASE_BUSINESS_VOLUME.png" alt="">
				        	</div>
		        		</div>
						
			        	<div class="row">
			        		<div class="col-md-4 col-sm-4 col-xs-4">
					        	<img class="img-responsive" src="frontend/img/features/CUSTOMIZAB_ DESIGN.png" alt="">
				        	</div>
				        	<div class="col-md-8 col-sm-8 col-xs-8">
				        		<div class='features-info'>
					        		<h3> THIẾT KẾ TÙY BIẾN </h3>
					        		<span>iFoodGo được cung cấp tùy chỉnh thiết kế để mỗi chủ nhà hàng đã đăng ký với iFoodGo. </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Dễ dàng cài đặt riêng tên miền của bạn với riêng Logo, Thiết kế & Thương hiệu </li>
						        			<li><i class='fa fa-check-circle'> </i> Không đòi hỏi kiến thức về html và thiết kế web </li>
						        	    </ul>
					        		</div>
					        	</div>

				        	</div>
		        		</div>
					
						<br>
		        		<div class="row">
			        		<div class="col-md-8 col-sm-8 col-xs-8">
					        	<div class='features-info'>
					        		<h3> DỄ SỬ DỤNG & CHI PHÍ THẤP </h3>
					        		<span>Để bắt đầu nhà hàng kinh doanh trực tuyến của bạn là rất dễ dàng với iFoodGo. Chỉ cần vài phút và bạn đã sẵn sàng để bắt đầu </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Nhanh chóng và dễ dàng để cài đặt </li>
						        			<li><i class='fa fa-check-circle'> </i> Bắt đầu đặt hàng trong vài phút </li>
						        			<li><i class='fa fa-check-circle'> </i> Không cần có chuyên môn kỹ thuật </li>
						        			<li><i class='fa fa-check-circle'> </i> Giá cả hợp lý </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				        		<img class="img-responsive" src="frontend/img/features/low_cost.png" alt="">
				        	</div>
		        		</div>
						<br>
						<br>
		        		<div class="row">
			        		<div class="col-md-4 col-sm-4 col-xs-4">
					        	<img class="img-responsive" src="frontend/img/features/EASY_TO_CONTROL.png" alt="">
				        	</div>
				        	<div class="col-md-8 col-sm-8 col-xs-8">
				        		<div class='features-info'>
					        		<h3> DỄ KIỂM SOÁT </h3>
					        		<span>CNTT là rất dễ dàng để quản lý và kiểm soát website nhà hàng của bạn với iFoodGo. Nếu không có bất kỳ chuyên môn bạn vẫn có thể quản lý và kiểm soát thông tin của nhà hàng của bạn. </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Các món ăn </li>
						        			<li><i class='fa fa-check-circle'> </i> Bộ sưu tập hình ảnh món ăn của nhà hàng </li>
						        			<li><i class='fa fa-check-circle'> </i> Phí hàng & phí (phí dịch vụ, phí Cổng thanh toán, chi phí giao hàng) </li>
						        	    </ul>
					        		</div>
					        	</div>

				        	</div>
		        		</div>
						<br>
						<br>
		        		<div class="row">
			        		<div class="col-md-8 col-sm-8 col-xs-8">
					        	<div class='features-info'>
					        		<h3> PHƯƠNG THỨC GIAO HÀNG </h3>
					        		<span>iFoodGo Cung cấp nhiều tùy chọn Phương pháp giao hàng cho chủ nhà hàng và người dùng. Chủ nhà hàng có thể cung cấp bất kỳ hoặc tất cả các phương pháp giao hàng trong số đó và cũng Khách hàng có thể chọn bất kỳ phương pháp phân phối để có thức ăn của họ. </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Giao hàng tận nhà </li>
						        			<li><i class='fa fa-check-circle'> </i> Mang đi, đến lấy </li>
						        			<li><i class='fa fa-check-circle'> </i> Đặt chỗ trước </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				        		<img class="img-responsive" src="frontend/img/features/delivery.png" alt="">
				        	</div>
		        		</div>
						<br>
		        		<div class="row">
			        		<div class="col-md-4 col-sm-4 col-xs-4">
			        			<img class="img-responsive" src="frontend/img/features/ANALYTICS_&_REPORTS.png" alt="">

				        	</div>
				        	<div class="col-md-8 col-sm-8 col-xs-8">
				        		<div class='features-info'>
					        		<h3> PHÂN TÍCH THỐNG KÊ & BÁO CÁO </h3>
					        		<span>iFoodGo cũng đang cung cấp cơ sở có báo cáo khác nhau.</span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Theo dõi bán hàng của bạn </li>
						        			<li><i class='fa fa-check-circle'> </i> Thế hệ doanh thu cho các lĩnh vực cụ thể bao gồm các xu hướng đặt hàng </li>
						        			<li><i class='fa fa-check-circle'> </i> Phân tích Báo cáo dự đoán và đồ thị để quản lý bán hàng tương lai để tăng giá trị kinh doanh </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
		        		</div>
						<br>
						<br>
						<br>
		        		<div class="row">
			        		<div class="col-md-8 col-sm-8 col-xs-8">
					        	<div class='features-info'>
					        		<h3> ỨNG DỤNG TRÊN THIẾT BỊ DI ĐỘNG THÔNG MINH </h3>
					        		<span>Hệ thống đặt hàng đi kèm với ứng dụng điện thoại di động tích hợp có thể chạy dễ dàng trên thiết bị di động thông minh nền tảng Android và IOS. Dễ dàng và thân thiện với giao diện di động làm cho nó thuận tiện cho khách hàng thông qua điện thoại thông minh. </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Nhận thị trường đã sẵn sàng IOS và Android App Kiến thức kỹ thuật là không cần thiết </li>
						        	    </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-3 col-sm-3 col-xs-3 text-right">
				        		<img class="img-responsive" src="frontend/img/features/app_install.png" alt="">
				        	</div>
		        		</div>
						<br>
		        		<div class="row">
			        		<div class="col-md-4 col-sm-4 col-xs-4">
			        			<img class="img-responsive" src="frontend/img/features/MARKETING_TOOLS.png" alt="">

				        	</div>
				        	<div class="col-md-8 col-sm-8 col-xs-8">
				        		<div class='features-info'>
					        		<h3> CÔNG CỤ TIẾP THỊ </h3>
					        		<span>iFoodGo cung cấp các công cụ tiếp thị khác nhau để giúp nhà hàng để có được khách hàng mới hoặc khách hàng hiện tại có sức thuyết phục.</span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Mass Mailer </li>
						        			<li><i class='fa fa-check-circle'> </i> Newsletters </li>
						        			<li><i class='fa fa-check-circle'> </i> Social Media </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
		        		</div>
						<br>
		        		<div class="row">
			        		<div class="col-md-8 col-sm-8 col-xs-8">
					        	<div class='features-info'>
					        		<h3> XÂY DỰNG TRANG WEB HOÀN CHỈNH </h3>
					        		<span>iFoodGo là nhiều hơn một hệ thống đặt hàng thực phẩm. Mở chủ nhà hàng iFoodGo có thể xây dựng trang web hoàn chỉnh của họ mà không có kiến thức kỹ thuật.</span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Online Ordering </li>
						        			<li><i class='fa fa-check-circle'> </i> Menu (Photos) </li>
						        			<li><i class='fa fa-check-circle'> </i> About US </li>
						        			<li><i class='fa fa-check-circle'> </i> Privacy </li>
						        			<li><i class='fa fa-check-circle'> </i> Contact US </li>
						        			<li><i class='fa fa-check-circle'> </i> Terms of Use </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				        		<img class="img-responsive" src="frontend/img/features/BUILD_COMPLETE_WEBSITE.png" alt="">
				        	</div>
		        		</div>
						<br>
						<br>
		        		<div class="row">
			        		<div class="col-md-4 col-sm-4 col-xs-4">
			        			<img class="img-responsive" src="frontend/img/features/support.ico" alt="">

				        	</div>
				        	<div class="col-md-8 col-sm-8 col-xs-8">
				        		<div class='features-info'>
					        		<h3> HỖ TRỢ KHÁCH HÀNG </h3>
					        		<span>Tất cả các vấn đề của khách hàng, khiếu nại, thắc mắc và vấn đề kỹ thuật được xử lý bởi đội ngũ hỗ trợ của chúng tôi.</span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Tiếp cận với nhóm hỗ trợ của chúng tôi 24 * 7 và chúng tôi sẽ chủ động theo dõi các vấn đề. </li>

						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
		        		</div>
		        	</div>
		        </div>
		    </div>
		    <!-- / Main Content End -->
		    <!-- Footer Start -->
				@stop
 			 @section('script-screen')
 			 @stop
