@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
				<div class="banner padd" id="main">
						<div class="container">
								<!-- Image -->
								<img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
								<!-- Heading -->
								<h2 class="white" id="#main"><strong>Restaurant Profile</strong></h2>
								 <ol class="breadcrumb">
									<li ><a style="color: #009688;!imporant" href="/">Home</a></li>
									<li  ><a style="color: #009688;" href="/addRestaurant">Add Your Restaurant</a></li>
									<li style="color: red;">Add Your Restaurant Form</li>
								</ol>
								<div class="clearfix" style="height:20px;"></div>
							</div>
					</div>

			<!-- Inner Content -->
			<div class="inner-page">

					<div class="component-content restaurantList-profile" style="width:70%;margin-left:15%">
											<!-- tabs -->
						<ul id="myTab" class="nav nav-tabs">
						  <li class="active" style="width:33.3%;text-align:center"><a href="#owner" data-toggle="tab">Owner</a></li>
						  <li style="width:33.3%;text-align:center"><a href="#restaurant" data-toggle="tab" onclick="openMap()" >Restaurant</a></li>
							<li style="width:33.4%;text-align:center"><a href="#social" data-toggle="tab">Social</a></li>
						</ul>
						<form id="addForm" method="post" action="/addRestaurantForm" enctype="multipart/form-data">
							{{ csrf_field() }}
						<div id="myTabContent" class="tab-content">
						<?php if(Session::has('messageErrorAddRes'))?>
						<p class="{{ Session::get('alert-class', '') }}">
						<?php
						 $messageList  = Session::get('messageErrorAddRes');
						 echo $messageList;
						 ?>
						
						</p>
							<div class="tab-pane active fade in" id="owner">
									<div class="row rst-form-input">
											<div class="col-sm-12">
													<div class="form-group col-sm-6">
															<label>
																			First Name <span class="errorRequired">*</span>
																	</label>
															<input type="text" id="first_name" name="first_name" value="{{ Request::old('first_name') }}" class="form-control Required" data-validation="length" data-validation-length="1-12" placeholder="Name" >
															<span id="err_first_name"class="error-color"></span>
													</div>
													<div class="form-group col-sm-6">
															<label>
																			Last Name <span class="errorRequired">*</span>
																	</label>
															<input type="text" name="last_name" id="last_name" value="{{ Request::old('last_name') }}" class="form-control Required" data-validation="length" data-validation-length="1-12" placeholder="Last Name" >
															<span id="err_last_name"class="error-color"></span>

													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group col-sm-3">
															<label>
																			Mobile Number <span class="errorRequired">*</span>
																	</label>
															<input type="text" name="Mobileno" id="Mobileno" class="form-control Required" value="{{ Request::old('Mobileno') }}" placeholder="Mobile Number" data-validation-length="max12" data-validation="required | number | length">
															<span id="err_mobile_no"class="error-color"></span>
													</div>
													<div class="form-group col-sm-3">
															<label>
																			Tel Number <span class="errorRequired">*</span>
																	</label>
															<input type="text" name="Telno" id="Telno" class="form-control Required" value="{{ Request::old('Telno') }}" placeholder="Tel Number" data-validation-length="max12" data-validation="required |length">
															<span id="err_tel_no"class="error-color"></span>
													</div>
													<div class="form-group col-sm-6">
															<label>
																			Email<span class="errorRequired">*</span>:
																	</label>
															<input type="text" name="email" id="email" class="form-control Required"  value="{{ Request::old('email') }}" placeholder="Your email" data-validation="email">
															<span id="err_email"class="error-color"></span>
													</div>
											</div>
											<div class="form-group col-sm-12">
												<div class="form-group col-sm-6">
														<label>
																		Password <span class="errorRequired">*</span>
																</label>
														<input type="password" name="pass" id="pass" class="form-control Required" placeholder="Password"  data-validation="length" data-validation-length="min6">
														<span id="err_pass"class="error-color"></span>
												</div>
												<div class="form-group col-sm-6">
														<label>
																		Confirm password <span class="errorRequired">*</span>
																</label>
														<input type="password" name="conf_pass" id="conf_pass" class="form-control Required" placeholder="Password" data-validation="confirmation|length" data-validation-length="min6"  data-validation-confirm="pass">
														<span id="err_conf_pass"class="error-color"></span>

												</div>
											</div>
									</div>
									<a class="btn btn-success btnNext tab1">Next</a>
							</div>
							<!-- step 2-->
							<div class="tab-pane fade" id="restaurant">
							    <div class="row rst-form-input">
							        <div class="col-sm-6 ">
							            <div class="form-group">
							                <label>
							                        <h5>Restaurant Name <span class="errorRequired">*</span></h5></label>
							                <input type="text" id="res_name" name="res_name" class="form-control required" value="{{ Request::old('res_name') }}" placeholder="Restaurant Name" data-validation="length|required" data-validation-length="max256">
															<span id="err_res_name"class="error-color"></span>

												  </div>
							        </div>
							        <div class="col-sm-6 ">
							            <div class="form-group">
							                <label>
							                        <h5>Restaurant Address <span class="errorRequired">*</span></h5></label>
							                <textarea name="res_address" class="form-control required location " id="txtAddress" value="{{ Request::old('res_address') }}" placeholder="Restaurant Address" data-validation="length|required" data-validation-length="max1024"> </textarea>
							                <input type="hidden" name="country" id="txtCountry" class="location">
							                <input type="hidden" name="province" id="txtProvince" class="location">
							                <input type="hidden" name="city" id="txtCity" class="location">
							                <input type="hidden" name="lat" id="txtLat" class="location">
							                <input type="hidden" name="long" id="txtLon" class="location">
							                <input type="hidden" name="street" id="txtStreet" class="location">
															<a class="btn btn-info" onclick="SearchMapChange()" style="height: 30px;margin-top: 5px;">Check</a>
															<span id="err_txtAddress"class="error-color"></span>

															<br>
							                <span class="red font12">Note:-Enter Full Restaurant Address to display Route
							                        to your restaurant properly in Mobile App.</span>
							            </div>
							        </div>
							        <div class="col-sm-12">
							            <label>
							                Drag and Drop Marker to Restaurant Address
							            </label>
							            <div class='order-google-map'>
							                <div id="order-map">

							                </div>
							            </div>
							        </div>
							        <div class="col-sm-6" >
							            <div class="form-group">
							                <label>
							                        <h5>Restaurant Phone Number<span class="errorRequired">*</span></h5></label>
																			<div class="col-sm-12" style="padding:0">
																				<div class="form-group col-sm-12">
																					<input type="text" name="res_telno" id="res_tel" class="form-control" value="{{ Request::old('res_telno') }}" placeholder="Tel Number" data-validation-length="max12" data-validation="length | required">
																					<span id="err_res_tel"class="error-color"></span>

																				</div>
																				<div class="form-group col-sm-12">
																					<input type="text" name="res_mobileno" id="res_mobile" class="form-control" value="{{ Request::old('res_mobileno') }}" placeholder="Mobile Number" data-validation-length="max12" data-validation="number | length | required">
																					<span id="err_res_mobile"class="error-color"></span>

																				</div>
																			</div>

													</div>
							            <div class="form-group">
							                <label>
							                        <h5>Restaurant Timing <span class="errorRequired">*</span></h5></label>
							                <div class="row">
							                    <div class="col-sm-12">
							                        <div class="form-group col-sm-3">
							                          <label for=""><b>Open:</b></label>
							                            <input id="open_time" type="text" name="open_time"  class="form-control" value="{{ Request::old('open_time') }}" style="width:6.5em" data-validation="time|required" data-validation-help="HH:mm">
																					<span id="err_open_time"class="error-color"></span>

							                        </div>
																			<div class="form-group col-sm-3">
																				<label for=""><b>Close:</b></label>
																			<input id="close_time" type="text" name="close_time"  class="form-control" value="{{ Request::old('close_time') }}" style="width:6.5em" data-validation="time | required" data-validation-help="HH:mm">
																			<span id="err_close_time"class="error-color"></span>

																			</div>
							                    </div>


							                    <label id="InvalidTime" class="color-red" style="margin-top: 9px; margin-left: 16px;
							                            display: none;">
							                            Open and close time could not be same!
							                        </label>
							                </div>
							            </div>
							            <!--<div class="form-group">
							                <label>
							                        <h5>Upload Logo (165 x 34)</h5></label>
							            </div>-->
							            <!--<div class="form-group">
							                      <div class="fileinput fileinput-new" data-provides="fileinput">
							                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
							                              <img id='image-restaurant' src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
							                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
							                          <div>
							                              <button class="btn br-grey btn-file">
							                                  <span class="fileinput-new"> Select image </span>
							                                  <span class="fileinput-exists"> Change </span>
							                                  <input type="hidden" value="" ><input name="logo"  value="{{ Request::old('logo') }}" id='image-restaurant-input' type="file" > </button>
							                              <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
							                          </div>
							                      </div>





							                      <div class="clearfix margin-top-10">
							                          <span class="label label-danger" style='margin-right:10px'>NOTE! </span>
							                          <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
							                      </div>
							                  </div>-->

							            <div class="form-group">
							                <label>
							                        <h5>Default Currency <span class="errorRequired">*</span></h5></label>
																			<br>
							                        <select class="res_currency form-control" style="width:200px" name="res_type">
							                          @foreach ($currency as $c)
							                            <option value="{{$c->cd}}">{{$c->cd_name}}</option>
							                          @endforeach
							                        </select>
							                <br>
							                <span class="red font12">Note:-This is the default currency accepted by your payment
							                        processor.</span>
							            </div>
							        </div>
							        <div class="col-sm-6">
							            <div class="form-group">
							                <label>
							                        <h5>Restaurant Other Email<span class="errorRequired">*</span></h5></label>
																			<input type="text" id="res_email" name="res_email" value="{{ Request::old('res_email') }}" class="form-control Required"  placeholder="Restaurant email" data-validation="email | required | length" data-validation-length="max256">
																			<span id="err_res_email"class="error-color"></span>

												  </div>

							            <div class="form-group">
							                <h5>
							                    Select Your Restaurant Type <span class="errorRequired">*</span></h5>

							                    <select class="res_type form-control" style="width:200px" name="res_type">
							                      @foreach ($type as $t)
							                        <option value="{{$t->cd}}">{{$t->cd_name}}</option>
							                      @endforeach
							                    </select>
							            </div>
							            <div class="row">
							                <div class="clearfix">
							                </div>
							                <div class="col-sm-12">
							                    <h5>
							                        Get Your Own Restaurant Website URL <span class="errorRequired">*</span></h5>
							                    <a style="font-size: 18px;">http://ifoodgo.com/<span class="resturl"></span></a>
							                </div>
							                <div class="col-sm-12">
							                    <div class="form-group">
							                        <input type="text" name="Subdomain" id="Subdomain" class="form-control  required" value="{{ Request::old('Subdomain') }}" placeholder="Enter Restaurant URL" data-validation="alphanumeric|required|length" data-validation-length="max256" data-validation-allowing="-_">
																			<span id="err_Subdomain"class="error-color"></span>

																  </div>
							                </div>
							                <div class="col-sm-12 ">
							                    <div class="form-group">
							                        <input type="button" class="btn btn-info" style="height: 30px;margin-top: -5px;" value="Check" id="btncheck" >
							                    </div>
							                    <!-- Notification-->

							                    <div class="form-group">
							                        <span class="color-green font12">Hint: If Restaurant Name is "TT Food" than you can enter
							                                "TTFood" for your unique URL. Your Own Restaurant Website URL will be http://ifoodgo.com/TTFood
							                            </span>
							                        <br>
							                        <span class="color-red font12">Note : Space and Special Characters are Not Allowed.</span>
							                    </div>
							                </div>
							            </div>
							            <div class="form-group">
							                <h5>
							                    Promo code</h5>
							                <input type="text" name="Promocode" id="Promocode" value="{{ Request::old('Promocode') }}" class="form-control" placeholder="Enter Promocode" value="" >
							                <label id="PromoError" class="color-red">
							                    </label>
							                <label id="PromoSuccess" class="color-green">
							                    </label>
							                <input type="hidden" id="ischeckpromocode" value="">
							            </div>

							            <div class="form-group">
							                <h5>
							                    Promo Discount</h5>
							                <input type="text" name="Promodis" id="Promodis" value="{{ Request::old('Promodis') }}" class="form-control" placeholder="Enter Promocode" value="" >
							                <label id="PromoError" class="color-red">
							                    </label>
							                <label id="PromoSuccess" class="color-green">
							                    </label>
							            </div>
							        </div>
							    </div>
							    <a class="btn btn-success btnPrevious">Previous</a>
							    <a class="btn btn-success btnNext tab2" >Next</a>
							</div>
							<!-- step 3-->
							<div class="tab-pane fade" id="social">
							    <div class="row rst-form-input">
							        <div class="col-sm-6">
							            <div class="form-group">
							                <label>
							                        Facebook URL</label>
							                <input type="text" name="Facebook" value="{{ Request::old('Facebook') }}" class="form-control" placeholder="Facebook URL">
							            </div>
							            <div class="form-group">
							                <label>
							                        Twitter URL</label>
							                <input type="text" name="Twitter" value="{{ Request::old('Twitter') }}" class="form-control" placeholder="Twitter URL">
							            </div>
							        </div>
							        <div class="col-sm-6">
							            <div class="form-group">
							                <label>
							                        Linkedin URL</label>
							                <input type="text" name="Linkedin" value="{{ Request::old('Linkedin') }}" class="form-control" placeholder="Linkedin URL">
							            </div>
							            <div class="form-group">
							                <label>
							                        Google Plus URL</label>
							                <input type="text" name="Google" class="form-control" value="{{ Request::old('Google') }}" placeholder="Google URL">
							            </div>
							        </div>
							    </div>
							    <a class="btn btn-success btnPrevious">Previous</a>
									<button id="submitForm" class="btn btn-success" type="submit" >Done</button>

							    <!-- <button id="submitForm" class="btn btn-success" type="submit" >Done</button> -->
							</div>
						</div>
					</form>
					</div>

			</div><!-- / Inner Page Content End -->

			<!-- Footer Start -->
			@stop
			@section('script-screen')
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoQDoMMccBAEVdy72njIRVylyN_hqInyQ&libraries=places"
					 async defer></script>
			<script src="{{'/frontend/js/pages/addRestaurantForm.js'}}"></script>
			@stop
