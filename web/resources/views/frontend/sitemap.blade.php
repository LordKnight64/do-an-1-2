@extends('frontend.layouts.masterNoSlider')

@section('content')
		
<!-- Banner Start -->
<div class="banner padd">
	<div class="container">
		<!-- Image -->
		<img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
		<!-- Heading -->
		
	     <h2 class="white">SiteMap</h2>
		<ol class="breadcrumb">
			<li><a style="color: white" href="/">Home</a></li>
			<li ><a style="color: red" >Sitemap</a></li>
		
		
		</ol>	
		<div class="clearfix" style="height:20px;"></div>
	</div>
</div>

	<div class='register-content' style="width:95%">
		<div class="row" style="margin-left: 50px;">
			<div class="col-md-5 col-sm-5 col-xs-5" >
				<div class="row" >
					
					<div class="col-md-8 col-sm-8 col-xs-8" style=" margin-top: 15px;
						font-size: 20px;
						color: #009688;
						font-weight: bold;">
						Home
					</div>
				</div>
				<div class="row" >
					
					<div class="col-md-8 col-sm-8 col-xs-8" style=" margin-top: 15px;
						font-size: 20px;
						color: #009688;
						font-weight: bold;">
						Sitemap
					</div>
				</div>

				<div class="row" style=" margin-top: 15px;">
					<div class="col-md-10 col-sm-10 col-xs-10">
						<div class="feature-details">
							<ul style="list-style-type: none;">
								
							<a href="features" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; Features </li> </a>
							<a href="howitswork" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; How it works </li> </a>
							<a href="addRestaurant" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; Add Your Restaurant </li> </a>
							<a href="contact" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; Contact </li> </a>
							<a href="notificationList" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; News </li> </a>
						    <a  href="userRegister" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; User Register </li> </a>
							<a href="https:restaurant.ifoodgo.vn" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; Restaurant Login </li> </a>
							<a data-toggle="modal" data-target="#modal-user-login" href="" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; User Login </li> </a>
							<a href="/termandcondition" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; Terms and Conditions </li> </a>
							<a href="/privatepolicy" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; Private Policy </li> </a>
							<a href="/faq" target="_blank">		<li style="color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i>&nbsp; FAQ</li> </a>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-7 col-sm-7 col-xs-7" >
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="category-box">
						<h2>
						<a href="#" style="color:red">
						Home
						</a>
						</h2>
						<ul class="level-2" style="list-style-type: none;">
						<li>
						<a href="/search?province=&key=" class="level-2" target="_blank">
						Search Restaurant By Name 
						</a>
						</li>
						<li>
						<a href="/search?province=&key=" class="level-2" target="_blank">
						Search Restaurant By District 
						</a>
						</li>
						<li>
						<a href="#" data-toggle="modal" onclick="openMap(1)" class="level-2" target="_blank">
						Search Restaurant on Map <!-- CHECK -->
						</a>
						</li>
						<li>
						<a href="/orderBest" class="level-2" target="_blank">
						Best Sales
						</a>
						</li>
						<li>
						<a href="/orderDiscount" class="level-2" target="_blank">
						Sale Off 50%
						</a>
						</li>
						<li>
						<a href="/search?rank=1" class="level-2" target="_blank">
						Famous Restaurant
						</a>
						</li>
						<li>
						<a href="" class="level-2" target="_blank">
						Restaurants NearBy <!-- CHECK -->
						</a>
						</li>
						<li>
						<a href="/search?type=" class="level-2" target="_blank">
						Popular Foods
						</a>
						</li>
							<li>
						<a href="" class="level-2" target="_blank">
						Download App on AppStore <!-- CHECK -->
						</a>
						</li>
							<li>
						<a href="" class="level-2" target="_blank">
						Download App on PlayStore <!-- CHECK -->
						</a>
						</li>
						</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="category-box">
						<h2>
						<a style="color:red" href="/search?province=&key=" target="_blank">
						Find Restaurants
						</a>
						</h2>
						<ul class="level-2" style="list-style-type: none;">
						<li>
						<a href="/search?province=&key=" class="level-2" target="_blank">
						Restaurant List
						</a>
						</li>
						<li>
						<a href="/restaurantDetail?id=1" class="level-2" target="_blank">
						Restaurant Detail
						</a>
						</li>
						<li> 
						<a href="/order?restaurant_id=1" class="level-2" target="_blank">
						Order List 
						</a>
						</li>
						<li>
						<a href="/orderDetail?restaurant_id=1&item_id=1" class="level-2" target="_blank">
						Food Detail 
						</a>
						</li>
						<li>
						<a href="/checkout" class="level-2" target="_blank">
						Checkout 
						</a>
						</li>
						<li>
						<a href="/reviewCheckout" class="level-2" target="_blank">
						Confirm Checkout 
						</a>
						</li>
						
						</ul>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="category-box">
						<h2>
						<a href="#" style="color:red">
						Popular Foods
						</a>
						</h2>
						<ul class="level-2" style="list-style-type: none;">
						<li>
						<a href="/search?type=8" class="level-2" target="_blank">
						Ethnic Restaurant 
						</a>
						</li>
						<li>
						<a href="/search?type=1" class="level-2" target="_blank">
						Family Style 
						</a>
						</li>
						<li>
						<a href="/search?type=9" class="level-2" target="_blank">
						Casual Dining 
						</a>
						</li>
						<li>
						<a href="/search?type=0" class="level-2" target="_blank">
						Fast Casual 
						</a>
						</li>
						<li>
						<a href="/search?type=1" class="level-2" target="_blank">
						Family Style 
						</a>
						</li>
						<li>
						<a href="/search?type=10" class="level-2" target="_blank">
						Barbecue 
						</a>
						</li>
						<li>
						<a href="/search?type=11" class="level-2" target="_blank">
						Online Restaurant 
						</a>
						</li>
						<li>
						<a href="/search?type=2" class="level-2" target="_blank">
						Fine Dining 
						</a>
						</li>
						<li>
						<a href="/search?type=3" class="level-2" target="_blank">
						Café or Bistro 
						</a>
						</li>
						<li>
						<a href="/search?type=4" class="level-2" target="_blank">
						Fast Food 
						</a>
						</li>
						<li>
						<a href="/search?type=5" class="level-2" target="_blank">
						Food Truck 
						</a>
						</li>
						<li>
						<a href="/search?type=6" class="level-2" target="_blank">
						Restaurant Buffet 
						</a>
						</li>
						<li>
						<a href="/search?type=7" class="level-2" target="_blank">
						Pop Up Restaurant
						</a>
						</li>
						</ul>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="category-box">
						<h2>
						<a style="color:red" href="/" target="_blank">
						USER 
						</a>
						</h2>
						<ul class="level-2" style="list-style-type: none;">
						<li>
						<a href="#" data-toggle="modal" data-target="#modal-user-login" class="level-2" target="_blank">
						Login 
						</a>
						</li>
						<li>
						<a href="/userRegister" class="level-2" target="_blank">
						Signup
						</a>
						</li>
						<li>
						<a href="/reset" class="level-2" target="_blank">
						Forgot Password
						</a>
						</li>
						<li>
						<a href="/profile" class="level-2" target="_blank">
						Profile
						</a>
						</li>
						<li>
						<a href="/historyOrder" class="level-2" target="_blank">
						History Orders
						</a>
						</li>
						<li>
						<a href="/myWishList" class="level-2" target="_blank">
						Wishlist 
						</a>
						</li>
						
						</ul>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="category-box">
						<h2><a style="color:red" href="/" target="_blank">	restaurant owner</a></h2>
						<ul class="level-2" style="list-style-type: none;">
						<li>
						<a href="https://restaurant.ifoodgo.vn" class="level-2" target="_blank">
						Login
						</a>
						</li>
						<li>
						<a href="/addRestaurant" class="level-2" target="_blank">
						Add Your Restaurant
						</a>
						</li>
						<li>
						<a href="/addRestaurantForm" class="level-2" target="_blank">
						Add Restaurant Form 
						</a>
						</li>
						</ul>
						</div>
					</div>
				</div>
			
			</div>

			

		</div>
	       			
</div>

<!-- Footer Start -->
@stop
