@extends('frontend.layouts.masterNoTemplate')

@section('content')
 <div class="container menu-bar" style="height: 63px;">
	<div class="logo" id="logo-header" style="text-align: center;">
		<img class="img-responsive" src="frontend/img/logo.png" alt="" style="display: inline-block;max-width: 50px;margin-top: 5px;"/>
		<!-- Heading -->
		<h1 style="text-align: center;">iFoodGo</h1>
		<!-- Paragraph -->
	</div>
	    <!-- Inner Content -->
	    <div class="inner-page padd">
	        <!-- About company -->
	        <div class="about-company ">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-12">
	                        <!-- About Compnay Item -->
	                        <div class="about-company-item">
	                            <!-- About Company Image -->
								@if(Session::has('activeRestaurant')=='1')
								<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: center;">Nhà hàng của bạn đã được hoạt động.</p>								
								@endif
								@if(Session::has('activeRestaurant')=='0')
								<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: center;">Nhà hàng của bạn chưa được hoạt động.</p>								
								@endif
	                        </div>
	                    </div>
	                </div>
					 <div class="row">
					 <div class="col-md-6" style="text-align: center;">
						<a href="/" class="btn btn-danger" type="submit">Quay về trang chủ</a>
					 </div>
					 <div class="col-md-6" style="text-align: center;">
						<a href="{{$url}}" class="btn btn-danger" type="submit">Goto your restaurant</a>
					 </div>
					 </div>
	            </div>
	        </div>
	    </div>
</div>
@stop
@section('script-screen')
	<script src="{{'/frontend/js/pages/sendMail.js'}}"></script>
@stop