@extends('frontend.layouts.masterNoSlider')

@section('content')
			<!-- Header End -->
	    <!-- Banner Start -->
			<div class="text-center m-b-lg">
	        <h1 class="text-shadow text-white" style="font-size:650px">404</h1>
	    </div>
	  

		<!-- Inner Content -->
<div class="inner-page" id='main-login'>

<div class="container w-xxl w-auto-xs" >
		<div class="text-center m-b-lg">
			<h1 class="text-shadow text-white">404</h1>
		</div>
		<div class="list-group auto m-b-sm m-b-lg">
			<a class="list-group-item bg-info text-lt"  >
				<i class="fa fa-chevron-right text-muted"></i>
				<i class="fa fa-fw fa-mail-forward m-r-xs text-lt"></i> <span class="text-lt">Goto application</span>
			</a>
			<a  class="list-group-item bg-success">
				<i class="fa fa-chevron-right text-muted"></i>
				<i class="fa fa-fw fa-sign-in m-r-xs text-lt"></i> <span class="text-lt">Sign in</span>
			</a>
		</div> 
		<div class="text-center" >
			<p>
				<small class="text-muted">@ 2017 by SystemEXE VN Co., Ltd.</small>
			</p>
		</div>
	</div>
</div>
	    <!-- / Inner Page Content End -->

	    <!-- Footer Start -->
			@stop
			@section('script-screen')
			@stop
