@extends('frontend.layouts.masterNoSlider')

@section('content')
		<!-- Header End -->
	<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">Check out</h2>
	            <ol class="breadcrumb">
	                <li><a style="color: white" href="/">Home</a></li>
	                <li ><a style="color:red " href="">Checkout</a></li>
	            </ol>	
				 <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>


	<!-- Inner Content -->
	<div class="inner-page ">

		<!-- Checkout Start -->

		<div class="checkout">
			<div class="container">
			<!-- Heading -->
			<h4>Shipping & Billing Details</h4>
				<div class="row">
					<form class="form-horizontal" role="form" id="checkout">
					<div class="col-md-5 col-sm-6">
						<!-- Checkout Form ################################################### -->
							<div class="form-group">
								<label for="inputName" class="col-md-3 control-label">Name</label>
								<div class="col-md-8">
									<input type="text" name="name" class="form-control" id="inputName" placeholder="Name" data-validation-length="max128">
									<span id="err_name"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail1" class="col-md-3 control-label">Email<span class="errorRequired">*</span></label>
								<div class="col-md-8">
									<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" data-validation="email" data-validation="length" data-validation-length="1-256">
									<span id="err_email"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPhone" class="col-md-3 control-label">Phone<span class="errorRequired">*</span></label>
								<div class="col-md-8">
									<input type="text"  name="phone" class="form-control" id="inputPhone" placeholder="Phone"  data-validation="number | length" data-validation-length="10-12">
									<span id="err_phone"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<label for="inputAddress" class="col-md-3 control-label">Address<span class="errorRequired">*</span></label>
								<div class="col-md-8">
									<textarea class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="1-1024"></textarea>
									<span id="err_address"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<label  class="col-md-3 control-label">Delivery type<span class="errorRequired">*</span></label>
								<div class="col-md-8">
									<select class="res_currency form-control" name="delivery_type" id="delivery_type">
											@foreach ($deliveryLst as $c)
											<option value="{{$c->cd}}">{{$c->cd_name}}</option>
											@endforeach
							         </select>
									<span id="err_deliveryType"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<label for="inputZip"  class="col-md-3 control-label">Delivery time<span class="errorRequired">*</span></label>
								<div class="col-md-4" style="padding-left:0px;">
									<div class="container">
									<div class="row">
										<div class='col-sm-4'>
											<div class="form-group">
												<div class='input-group date' id='datetimepicker1'>
													<input id ="deliveryTime" type='text' class="form-control" data-validation-require-leading-zero="false" data-validation="length" data-validation-length="min18"/>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
													<span id="err_deliveryTs"class="error-color"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
							<div class="form-group">
								<label for="promoDiscount" class="col-md-3 control-label">Code</label>
								<div class="col-md-8">
									<input type="hidden"  name="promoCode" class="form-control" id="promoCode">
									<input type="text"  name="inputPromoCode" class="form-control" id="inputPromoCode" placeholder="promo Code" data-validation-length="max256" data-validation-confirm="promoCode">
									<span id="err_promoCode"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-offset-3 col-md-8">
									<div class="checkbox">
										<label>
										<input type="hidden"  name="phone" class="form-control" id="items">
											<input type="checkbox" name="checkAccept" id="checkAccept" onchange = "CheckAccept(event)" data-validation="checkbox_group" data-validation-qty="min1"> Accept Terms & Conditions
										</label>
									</div>
									<span id="err_checkAccept"class="error-color"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-offset-3 col-md-8">
									<button type="submit" class="btn btn-danger btn-sm">Confirm Order</button>&nbsp;
									<button type="reset" class="btn btn-default btn-sm">Reset</button>
								</div>
							</div>
						
						<!-- /Checkout Form ################################################### -->
					</div>
					<div class="col-md-7 col-sm-6">
						<div class='clearfix'> </div>
						<div class="table-order">
							<h3 class='table-order-title'> Shopping cart</h3>
							<div class="table-responsive">
								<div id="content"> </div>	
									
							</div>
							
							<div class="form-group">
								
								<div >
									<textarea class="form-control" name="orderComment" id="orderComment" rows="3" placeholder="orderComment"></textarea>
								</div>
							</div>
							
							<div class='clearfix'> </div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Checkout End -->

	</div><!-- / Inner Page Content End -->
	<!-- Footer Start -->
	@stop
	@section('script-screen')
		<script src="{{'/frontend/js/pages/checkout.js'}}"></script>
	@stop
