@extends('frontend.layouts.masterNoSlider')

@section('content')
		<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">{{$restaurant->name}}</h2>
	            <ol class="breadcrumb">
	                <li style="color: white;"><i class="fa fa-map-marker" ></i> <a style="color: white;">{{$restaurant->address}} </a></li>
	            </ol>
				<p style="color: white;"><i class="fa fa-phone " ></i> {{$restaurant->tel}}</p>
				<p style="color: white;"><i class="fa fa-clock-o " ></i> {{date('G:i', strtotime($restaurant->open_time))}} AM - {{date('G:i', strtotime($restaurant->close_time))}} PM</p>
	           	<a style="color: white;; font-size:14px;" title="click here to view about [ {{$restaurant->name}} ]" href="/about?id={{$restaurant->id}}"><i class="fa fa-info-circle" >	</i>&nbsp; About us </a> &nbsp;&nbsp; &nbsp;| &nbsp; &nbsp; &nbsp;<a title="click here to view contact[ {{$restaurant->name}} ]" style="color: white; font-size:14px;" href="/contact?id={{$restaurant->id}}"><i class="fa fa-envelope" >	</i>&nbsp; Contact us </a>
				<ol class="breadcrumb">
	                <li><a style="color: white" href="/">Home</a></li>
	                <li ><a style="color: white" href="search">Restaurant list</a></li>
					<li style="color: red;">Restaurant Detail</li>
	            </ol>

				<div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>

		<br>
			<div class="restaurant-wizard">
				<div class="container">
					<div class='wizard-title'>Thật dễ dàng khi đặt món ăn trực tuyến trên <span>iFoodGo</span></div>
					<div class='wizard-description'>Chỉ trong 4 bước dưới đây </div>
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div class='wizard'>
								<span class='wizard-step'> 1</span>
								<div class='wizard-img'>
									<img class="img-responsive" src="frontend/img/features/icon_home_1.png" alt="" />
								</div>

								<div class='wizard-step-detail'>Tìm nhà hàng yêu thích của bạn</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class='wizard'>
								<span class='wizard-step'> 2</span>
								<div class='wizard-img'>
									<img class="img-responsive" src="frontend/img/features/icon_home_2.png" alt="" />
								</div>
								<div class='wizard-step-detail'>Xem và đặt món yêu thích</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class='wizard'>
								<span class='wizard-step'> 3</span>
								<div class='wizard-img'>
									<img class="img-responsive" src="frontend/img/features/icon_home_3.png" alt="" />
								</div>
								<div class='wizard-step-detail'>Trả tiền khi nhận hàng</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class='wizard'>
								<span class='wizard-step'> 4</span>
								<div class='wizard-img'>
									<img class="img-responsive" src="frontend/img/features/icon_home_4.png" alt="" />
								</div>
								<div class='wizard-step-detail'>Thưởng thức món và chấm điểm để nhận điểm thưởng</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		<!-- Dishes Start -->

			<div class="menu padd">
				<div class="container">
					<!-- Default Heading -->
					<div class="default-heading">
						<!-- Crown image -->
						<img class="img-responsive" src="frontend/img/crown.png" alt="" />
						<!-- Heading -->
						<h2>Menu</h2>
						<!-- Paragraph -->
						<!-- Border -->
						<div class="border"></div>
					</div>
					<!-- Menu content container -->
					<div class="menu-container">
						<div class="row">

							@foreach ($menu_header as $header)

							@if($header->item->count()>0)

							<div class="col-md-4 col-sm-4">
								<!-- Menu header -->
								<div class="menu-head">
									<!-- Image for menu item -->
									<img class="menu-img img-responsive img-thumbnail" src="{{$header->thumb}}" alt="" />
									<!-- Menu title / Heading -->
									<h3>{{$header->name}}</h3>
									<!-- Border for above heading -->
									<div class="title-border {{$header->color}}"></div>
								</div>
								<!-- Menu item details -->
								<div class="menu-details {{$header->color}}">
									<!-- Menu list -->
									<ul class="list-unstyled">
										<?php $limit = 0; ?>
										@foreach ($header->item->slice(0,3) as $items)
										<li>
											<div class="menu-list-item">
												<!-- Icon -->
												<form action="/order" method="get">
												<i class="fa fa-angle-right"></i> <a href="/orderDetail?restaurant_id={{$header->restaurant_id}}&item_id={{$items->id}}" >{{$items->name}}</a>
												<!-- Price badge -->
												<span class="pull-right" style="background: 0;">VNĐ</span>
												<span class="pull-right" >{{$items->price}}</span>
												<input type="hidden" name="restaurant_id" value="{{$header->restaurant_id}}" style="display:none">
												<div class="clearfix"></div>
											</form>
											</div>
										</li>
										<?php $limit++; ?>
										@endforeach
										@if($limit!=3)
										@for ($i = $limit; $i <= 2; $i++)
										<li>
											<div class="menu-list-item">
												<!-- Icon -->
												<form action="/order" method="get">
												<i class="fa fa-angle-right"></i> <a href="" onclick="event.preventDefault(); this.parentNode.submit()"></a>
												<!-- Price badge -->
												<span class="pull-right" style="background: 0;"></span>
												<span class="pull-right" ></span>
												<input type="hidden" name="restaurant_id" value="{{$header->restaurant_id}}" style="display:none">
												<input type="hidden" name="category_id" value="{{$header->id}}" style="display:none">
												<div class="clearfix"></div>
											</form>
											</div>
										</li>
										@endfor
										@endif
										<li>
											<form action="/order" method="get">
												<button href="#"class="show-more" type="submit">VIEW MORE</button>
												<input type="hidden" name="restaurant_id" value="{{$header->restaurant_id}}" style="display:none">
												<input type="hidden" name="category_id" value="{{$header->id}}" style="display:none">

											</form>
										</li>
									</ul>
								</div><!-- / Menu details end -->
							</div>
							@endif
							@endforeach

						</div>
					</div> <!-- /Menu container end -->
				</div>
			</div>
				<!-- Dishes Start -->

		<div class="dishes padd">
			<div class="container">
				<!-- Default Heading -->
				<div class="default-heading">
					<!-- Crown image -->
					<img class="img-responsive" src="frontend/img/crown.png" alt="" />
					<!-- Heading -->
					<h2>Best Sale</h2>
					<!-- Paragraph -->

					<!-- Border -->
					<div class="border"></div>
				</div>
				<div class="row">
					@foreach ($item_best as $ib)
						@include('frontend.partials.item_dished', ['item' => $ib])
					@endforeach
				</div>
			</div>
		</div>

		<!-- Dishes End -->
		<div class="dishes padd">
			<div class="container">
				<!-- Default Heading -->
				<div class="default-heading">
					<!-- Crown image -->
					<img class="img-responsive" src="frontend/img/crown.png" alt="" />
					<!-- Heading -->
					<h2>New Dishes</h2>
					<!-- Paragraph -->
					<!-- Border -->
					<div class="border"></div>
				</div>
				<div class="row">
					@foreach ($item as $i)
						@include('frontend.partials.item_dished', ['item' => $i])
					@endforeach
				</div>
			</div>
		</div>

		<br>
		<!-- Footer Start -->
		@stop
		@section('script-screen')
		<script src="{{'/frontend/js/partials/item_dished.js'}}"></script>
		@stop
