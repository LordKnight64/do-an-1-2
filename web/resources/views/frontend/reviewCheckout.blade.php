@extends('frontend.layouts.masterNoSlider')

@section('content')
		<!-- Header End -->
		<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">Check out</h2>
	              <ol class="breadcrumb">
	                <li><a style="color: white" href="/">Home</a></li>
	                <li ><a style="color:red " href="">Checkout</a></li>
	            </ol>	
				 <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>


	<!-- Inner Content -->
	<div class="inner-page padd">

		<!-- Checkout Start -->
	
		<div class="checkout">
		<!--<form class="form-horizontal" role="form" id="checkout">-->
		<div class = "form-horizontal">
			<div class="container">
			<!-- Heading -->
			<h4>Shipping & Billing Details</h4>
				<div class="row">
					<div class="col-md-6 col-sm-6 infoContent">
					<div class="infoCenter">
							<div class="form-group">
								<label class="col-md-3 control-label">Name:</label>
								<div class="col-md-8">
									<label id ='name' class="control-label"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Email:</label>
								<div class="col-md-8">
									<label class="control-label" id ='email'></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Phone:</label>
								<div class="col-md-8">
									<label class="control-label" id ='phone'></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Address:</label>
								<div class="col-md-8">
									<!--<textarea type="hidden" class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="3-1024"></textarea>-->
									<label class="control-label" id ='address'></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Delivery type:</label>
								<div class="col-md-8">
									<!--<textarea type="hidden" class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="3-1024"></textarea>-->
									<label class="control-label" id ='deliveryType'></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Delivery time:</label>
								<div class="col-md-8">
									<!--<textarea type="hidden" class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="3-1024"></textarea>-->
									<label class="control-label" id ='deliveryTime'></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Point accumulative:</label>
								<div class="col-md-7">
									<!--<textarea type="hidden" class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="3-1024"></textarea>-->
									<label class="control-label" id = "accumulativePoint">{{ Auth::check()? Auth::user()->accumulative_point : ''}}</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-8">
									<div class="checkbox">
										<label>
										<input type="hidden"  name="phone" class="form-control" id="items">
											<input type="checkbox" name="checkAccept" id="checkAccept" onchange = "CheckAccept(event)" data-validation="checkbox_group" data-validation-qty="min1"> You want use point accumulative ?
										</label>
									</div>
									<span id="err_checkAccept"class="error-color"></span>
								</div>
							</div>
						</div>	
						<!-- /Checkout Form ################################################### -->
					</div>
					<div class="col-md-6 col-sm-6">
						<!-- Button Kart -->
						
						<div class='clearfix'> </div>
						<div class="table-order">
							<h3 class='table-order-title shopCartTitle'> Shopping cart</h3>
							<div class="table-responsive">
								<div id="content"> </div>								
							</div>
							
							
							<div class="form-group checkout-title-label">
								<label class="checkout-title">SubTotal</label>
							</div>

							<div class="form-group line">
								<label class="col-md-4 control-label">Shipping & Handding</label>
								<div class="col-md-8 ">
									<label id='shipping' class="control-label checkout-unit">  </label>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-md-4 control-label">Discount</label>
								<div class="col-md-8">
									<label id='discount' class="control-label checkout-unit"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Price for point accumulative:</label>
								<div class="col-md-7">
									<!--<textarea type="hidden" class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="3-1024"></textarea>-->
									<label class="control-label checkout-unit" id = "pricePoint">0</label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Point accumulative remain:</label>
								<div class="col-md-7">
									<!--<textarea type="hidden" class="form-control" name="address" id="inputAddress" rows="3" placeholder="Address" data-validation="length" data-validation-length="3-1024"></textarea>-->
									<label class="control-label checkout-unit" id = "pricePointRemain">0</label>
								</div>
							</div>
							<div class="form-group checkout-title-label">
								<label class="col-md-4 checkout-title">Grand Total</label>
								<div class="col-md-8">
									<label id='grandTotal' class="control-label checkout-unit"></label>
									<input type = "hidden" id='priceTotal' class="control-label checkout-unit"></input>
								</div>
							</div>
							<div class="form-group">
							
								<div >
									<!--<textarea class="form-control" name="orderComment" id="orderComment" rows="3" placeholder="orderComment"></textarea>-->
									<div style=' color: #aaa; border: 1px solid #ddd; min-height: 100px;'>
									<label class="control-label" style="padding-left: 10px;" id ='orderComment' style='text-align: left;'></label>
									</div>
								</div>
							</div>

							<div class="form-group">
							<!--<input type = "hidden" name = "userLogin" id = "userLogin">-->
								<div style="padding-left: 0px;" class="col-md-offset-0 col-md-12">
									<a type="button" class="btn btn-danger btn-sm" href = "checkout">Back Checkout</a>
									<input type="hidden" id="login-user-id" value="{{ Auth::check()? Auth::user()->id : ''}}" onchange="myFunction(event)"></input> </a>
									<button type="reset" class="btn btn-success btn-sm" onclick = 'sendData()'>Payment</button>
								</div>
							</div>
							<div class='clearfix'> </div>
						</div>
					</div>
					
				</div>
			
			</div>
			</div>
		</div>

		<!-- Checkout End -->

	</div><!-- / Inner Page Content End -->

	<!-- Footer Start -->
	@stop
	@section('script-screen')
		<script src="{{'/frontend/js/pages/reviewCheckout.js'}}"></script>
	@stop
