@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">New User</h2>

	             <ol class="breadcrumb">
	                <li><a style="color: #009688;" href="/">Home</a></li>
	                <li ><a style="color:red " href="">register</a></li>
	            </ol>	
				 <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->

	    <!-- Inner Content -->
	    <div class="inner-page" id='main-login'>
	       <div class='register-form-container' style="padding: 30px;">
	       		<div class='register-content' style="width:850px">
	       				<div class="row">
			        		<div class="col-md-5 col-sm-5 col-xs-5" style="background: linear-gradient(rgba(255,255,255,0.6), rgba(255,255,255,0.6)), url(frontend/img/register.jpg) center center;
								height: 519px;
								border-right: 1px solid #E91E63;
								background-size: 150% 100%;
								background-repeat: no-repeat;">
								<div class="row">
			        				<div class="col-md-4 col-sm-4 col-xs-4" >
									
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8" style="    margin-top: 15px;
										font-size: 20px;
										color: #009688;
										font-weight: bold;">
										Signup and Get:
									</div>
								</div>

								<div class="row" style=" margin-top: 27px;">
			        				
									<div class="col-md-10 col-sm-10 col-xs-10">
										<div class="feature-details">
 						        		<ul style="list-style-type: none;">
						        			<li style="    color: #FF5722; font-weight:bold;"><i class="fa fa-check-circle"> </i> Khám phá món ăn </li> 
											<span style="font-size: 13px; color:#795548" >Tìm ra nhà hàng gần bạn với ẩm thực yêu thích từ iFoodGo</span>
						        		
											<li style="    color: #FF5722;font-weight:bold;"><i class="fa fa-check-circle"> </i> Lựa chọn thực phẩm yêu thích với iFoodGo </li>
						        			<span style="font-size: 13px; color:#795548" >Chọn thực phẩm theo tâm trạng và sở thích từ hàng trăm thực đơn đặc biệt hấp dẫn</span>
											
											<li style="    color:#FF5722;font-weight:bold;"><i class="fa fa-check-circle"> </i> Đặt hàng và Thanh toán </li>
											<span style="font-size: 13px; color:#795548" >Thanh toán tiền ẩm thực, tiền phí giao hàng khi nhận hàng</span>
											
											<li style="    color: #FF5722;font-weight:bold;"><i class="fa fa-check-circle"> </i> Nhận điểm thưởng với iFoodGo </li>
											<span style="font-size: 13px; color:#795548" >Thưởng thức thực phẩm của bạn ở bất kì địa điểm và cung cấp nhận xét</span>
										</ul>
					        		</div>
									</div>
								</div>

							</div>

							<div class="col-md-7 col-sm-7 col-xs-7" >
								
								<div class='register-form' style="width:500px">
									<form id="userReg" role="form" method="post" action="{{url('userRegister')}}">
												{{csrf_field()}}
										@if(Request::get('error') != null)
											<div class="alert alert-danger">
												<strong>Email này đã được đăng ký</strong> Vui lòng kiểm tra lại.
											</div>
										@endif
										<h6> First Name <span class="errorRequired">*</span></h6>
										<div class="form-group">
											<input name="first-name" type="text" class="form-control" data-validation="required"  placeholder="First name...">
											<span id="err_first_name"class="error-color"></span>
										</div>
										<h6> Last Name <span class="errorRequired">*</span> </h6>
										<div class="form-group">
											<input name="last-name" type="text" class="form-control" data-validation="required" data-validation-length="1-12" placeholder="Last name...">
											<span id="err_last_name"class="error-color"></span>
										</div>
										<h6> Email Address <span class="required">*</span></h6>
										<div class="form-group">
											<input name="email" type="text" class="form-control" data-validation="email" placeholder="Email...">
											<span id="err_email"class="error-color"></span>
										</div>
										<h6> Password <span class="required">*</span></h6>
										<div class="form-group">
											<input name="password" type="password" class="form-control" data-validation="length" data-validation-length="min6" placeholder="Password...">
											<span id="err_password"class="error-color"></span>
										</div>
								</div>
								<div class="register-footer">
									<h6><a href="/userLogin">Already on iFoodGo?</a></h6>
									<button class="btn btn-success pull-left " type="submit"><i class="fa fa-check-circle br-green"></i> Register</button>
								</form>

								<div class='clearfix'> </div>
								<div class="register-footer">
							</div>
						</div>
	       			



	       		</div>
	       </div>
	    </div>
	    <!-- / Inner Page Content End -->

	    <!-- Footer Start -->
			@stop
			@section('script-screen')
				<script src="{{'/frontend/js/pages/registerUser.js'}}"></script>
			@stop
