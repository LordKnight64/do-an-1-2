@extends('frontend.layouts.master')

@section('content')
   <div class="main-content">
     <!-- Dishes Start -->
     <div class="dishes padd">
         <div class="container">
             <!-- Default Heading -->
             <div class="default-heading">
                 <!-- Crown image -->
                 <img class="img-responsive" src="frontend/img/crown.png" alt="" />
                 <!-- Heading -->
                 <h2>CUSTOMER FEATURES</h2>
                 <!-- Paragraph -->
                 <p style="color:#333">Bạn đang đói bụng? Chỉ vài bước đơn giản từ danh sách nhà hàng đã có ngay món bạn yêu thích</p>
                 <!-- Border -->
                 <div class="border"></div>
             </div>
             <div class="modal fade" id="currentLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style='display:none'>
               <div class="modal-dialog" role="document">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title" id="myModalLabel">Drag drop marker to your location</h4>
                   </div>
                   <div class="modal-body">
                       <div class='map-current-location'>
                         <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                         <div id="map"></div>
                       </div>

                   </div>

                 </div>
               </div>
             </div>
             <div class="row" data-aos="fade-in"  data-aos-easing="ease-in" data-aos-duration="500" data-aos-offset="200">
                 <div class="col-md-3 col-sm-6">
                     <div class="dishes-item-container">
                         <!-- Image Frame -->
                         <div class="img-frame features-icons">
                             <i class="fa fa-search"></i>
                         </div>
                         <!-- Dish Details -->
                         <div class="dish-details">
                             <!-- Heading -->
                             <h3>Khám phá món ăn</h3>
                             <!-- Paragraph -->
                             <p>Tìm ra nhà hàng gần bạn với ẩm thực yêu thích từ iFoodGo</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-3 col-sm-6">
                     <div class="dishes-item-container">
                         <!-- Image Frame -->
                         <div class="img-frame features-icons">
                             <i class="fa fa-check-square-o"></i>
                         </div>
                         <!-- Dish Details -->
                         <div class="dish-details">
                             <!-- Heading -->
                             <h3>Lựa chọn</h3>
                             <!-- Paragraph -->
                             <p>Chọn thực phẩm theo tâm trạng và sở thích từ hàng trăm thực đơn đặc biệt hấp dẫn</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-3 col-sm-6">
                     <div class="dishes-item-container">
                         <!-- Image Frame -->
                         <div class="img-frame features-icons">
                             <i class="fa fa-money"></i>
                         </div>
                         <!-- Dish Details -->
                         <div class="dish-details">
                             <!-- Heading -->
                             <h3>Thanh toán</h3>
                             <!-- Paragraph -->
                             <p>Thanh toán tiền ẩm thực, tiền phí giao hàng khi nhận hàng</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-3 col-sm-6">
                     <div class="dishes-item-container">
                         <!-- Image Frame -->
                         <div class="img-frame features-icons">
                             <i class="fa fa-cutlery"></i>
                         </div>
                         <!-- Dish Details -->
                         <div class="dish-details">
                             <!-- Heading -->
                             <h3>iFoodGo</h3>
                             <!-- Paragraph -->
                             <p>Thưởng thức thực phẩm của bạn ở bất kì địa điểm và cung cấp nhận xét</p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- Dishes End -->
				<!-- Showcase Start -->

				<div class="showcase">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-6"  data-aos="zoom-out-right" data-aos-easing="ease-in" data-aos-duration="300">
								<!-- Showcase section item -->
								<div class="showcase-item">
									<!-- Image -->
									<img class="img-responsive" src="frontend/img/best_sale.jpg" alt="" />
									<!-- Heading -->
									<h3><a style="color:#009688 !important;" href="/orderBest">Best Sales</a></h3>
									<!-- Paragraph -->
									<p>Nam libero tempore, cum soluta nobis est minis voluptas assum simple and easy to distinguis quo.</p>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6"  data-aos="zoom-out-left" data-aos-easing="ease-in" data-aos-duration="300">
								<!-- Showcase section item -->
								<div class="showcase-item">
									<!-- Image -->
									<img class="img-responsive" style="    max-width: 168px;" src="frontend/img/s111.jpg" alt="" />
									<!-- Heading -->
									<h3><a style="color:#009688 !important;" href="/orderDiscount">Sale Off 50%</a></h3>
									<!-- Paragraph -->
									<p>Nam libero tempore, cum soluta nobis est minis voluptas assum simple and easy to distinguis quo.</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Showcase End -->




    <!-- menu Start -->
    <div class="dishes padd">
        <div class="container">
            <!-- Default Heading -->
            <div class="default-heading">
                <!-- Crown image -->
                <img class="img-responsive" src="frontend/img/crown.png" alt="" />
                <!-- Heading -->
                <h2>RESTAURANT OWNER FEATURES</h2>
                <!-- Paragraph -->

                <p style="color:#333">Nhanh chóng, Dễ dàng và giá cả phải chăng với hệ thống nhà hàng trực tuyến</p>
                <!-- Border -->
                <div class="border"></div>
            </div>
            <div class="row"  data-aos="fade-in"  data-aos-easing="ease-in" data-aos-duration="500" data-aos-offset="200">
                <div class="col-md-3 col-sm-6">
                    <div class="dishes-item-container">
                        <!-- Image Frame -->
                        <div class="img-frame features-icons">
                            <i class="fa fa-gears"></i>
                        </div>
                        <!-- Dish Details -->
                        <div class="dish-details">
                            <!-- Heading -->
                            <h3>Cài đặt nhanh</h3>
                            <!-- Paragraph -->
                            <p>Tạo website nhà hàng của bạn trong vài phút với iFoodGo</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="dishes-item-container">
                        <!-- Image Frame -->
                        <div class="img-frame features-icons">
                            <i class="fa fa-hand-o-up"></i>
                        </div>
                        <!-- Dish Details -->
                        <div class="dish-details">
                            <!-- Heading -->
                            <h3>Đặt hàng trực tuyến</h3>
                            <!-- Paragraph -->
                            <p>Hãy kinh doanh nhà hàng trực tuyến và bắt đầu nhận đơn hàng trực tuyến</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="dishes-item-container">
                        <!-- Image Frame -->
                        <div class="img-frame features-icons">
                            <i class="fa fa-list-alt"></i>
                        </div>
                        <!-- Dish Details -->
                        <div class="dish-details">
                            <!-- Heading -->
                            <h3>Quản lý đơn đặt hàng</h3>
                            <!-- Paragraph -->
                            <p>Quản lý đơn đặt hàng và bán hàng trực tuyến qua hệ thống báo cáo của nhà hàng</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="dishes-item-container">
                        <!-- Image Frame -->
                        <div class="img-frame features-icons">
                            <i class="fa fa-bullhorn"></i>
                        </div>
                        <!-- Dish Details -->
                        <div class="dish-details">
                            <!-- Heading -->
                            <h3>Xúc tiến</h3>
                            <!-- Paragraph -->
                            <p>Thúc đẩy kinh doanh nhà hàng trực tuyến của bạn với iFoodGo</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Menu End -->

    <!-- Showcase Start -->

       <div class="showcase">
         <div class="container">
           <div class="row">
             <div class="col-md-6 col-sm-6"  data-aos="zoom-out-right"  data-aos-easing="ease-in" data-aos-duration="300">
               <!-- Showcase section item -->
               <div class="showcase-item">
                 <!-- Image -->
                 <img class="img-responsive" style="top: 22px; left: 26px; width: 100%; max-width: 160px;" src="frontend/img/famous-rest.jpg" alt="" />
                 <!-- Heading -->
                 <h3><a href="/search?rank=1" style="color:#009688 !important;">Famous Restaurant</a></h3>
                 <!-- Paragraph -->
                 <p>Nam libero tempore, cum soluta nobis est minis voluptas assum simple and easy to distinguis quo.</p>
                 <div class="clearfix"></div>
               </div>
             </div>
             <div class="col-md-6 col-sm-6"  data-aos="zoom-out-left" data-aos-easing="ease-in" data-aos-duration="300">
               <!-- Showcase section item -->
               <div class="showcase-item">
                 <!-- Image -->
                 <img class="img-responsive" style="top: 22px; left: 26px; width: 100%; max-width: 160px;"  src="frontend/img/nearby.png" alt="" />
                 <!-- Heading -->
                 <h3><a href="#" style="color:#009688 !important;"  data-toggle="modal" onclick='openMap(1)' data-target="#currentLocationModal">Restaurants NearBy</a></h3>
                 <!-- Paragraph -->
                 <p>Nam libero tempore, cum soluta nobis est minis voluptas assum simple and easy to distinguis quo.</p>
                 <div class="clearfix"></div>
               </div>
             </div>
           </div>
         </div>
       </div>

       <!-- Showcase End -->

    <div class="dishes padd">
        <div class="container">
            <div class="default-heading">
                <!-- Crown image -->
                <img class="img-responsive" src="frontend/img/crown.png" alt="" />
                <!-- Heading -->
                <h2>Popular FOODS</h2>
                <!-- Paragraph -->
                <p style="color:#333">Danh sách các loại nhà hàng trong iFoodGo</p>
                <!-- Border -->
                <div class="border"></div>
            </div>
            <br>
            <div class="row">
                 <div class="gallery">
					<div class="container" data-aos="zoom-in"  data-aos-easing="ease-in" data-aos-duration="500" data-aos-offset="200">
						<!-- Gallery elements with pretty photo -->
						<div class="gallery-content">
							<div class="row">
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" >
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish1.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Ethnic Restaurant</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="/search?type=8">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish2.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Casual Dining</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=9">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish3.jpg" alt="">
											<h3  class="category-text" >Fast Casual</h3>
                                        <!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=0">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish4.jpg" alt="">
										<h3  class="category-text" >Family Style</h3>
                                        <!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=1">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>

								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish12.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Barbecue</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=10">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish13.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
										<h3  class="category-text" >Online Restaurant</h3>
                                    	<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=11">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish14.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Fine Dining</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=2">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish15.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Café or Bistro</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=3">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish16.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Fast Food</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=4">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish17.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Food Truck</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=5">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish18.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Restaurant Buffet</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=6">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="frontend/img/dish/dish19.jpg" alt="">
										<!-- Gallery Image Hover Effect -->
											<h3  class="category-text" >Pop Up Restaurant</h3>
                                        <span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->

										<a href="/search?type=7">
											<i class="fa fa-cutlery hover-icon icon-right"></i>
										</a>
									</div>
								</div>
							</div>
						</div><!-- Separate gallery element --><!--/ End Gallery content class -->
					</div>
				</div>
            </div>
        </div>
    </div>

    <!-- Pricing Start -->
    <div class="padd">
        <div class="container">
            <!-- Default Heading -->
            <div class="default-heading">
                <!-- Crown image -->
                <img class="img-responsive" src="frontend/img/crown.png" alt="" />
                <!-- Heading -->
                <h2>DOWNLOAD OUR MOBILE APP</h2>

                <!-- Border -->
                <div class="border"></div>
            </div>
        </div>
    </div>
    <!-- Pricing End -->
    <!-- Testimonial Start -->
    <div class="testimonial padd">
        <div class="container" >
            <div class="row">
                <div class="col-md-6"  >
                    <!-- BLock heading -->

                    <!-- Flex slider Content -->
                    <div class="flexslider-recent flexslider">
                        <ul class="slides">
                            <li>
                                <!-- Image for background -->
                                <img class="img-responsive" src="frontend/img/dish/thumb1.png" alt="" />
                            </li>
                             <li>
                                <!-- Image for background -->
                                <img class="img-responsive" src="frontend/img/dish/thumb4.png" alt="" />
                            </li>
                            <li>
                                <!-- Image for background -->
                                <img class="img-responsive" src="frontend/img/dish/thumb2.png" alt="" />
                            </li>
                             <li>
                                <!-- Image for background -->
                                <img class="img-responsive" src="frontend/img/dish/thumb5.png" alt="" />
                            </li>
                            <li>
                                <!-- Image for background -->
                                <img class="img-responsive" src="frontend/img/dish/thumb3.png" alt="" />
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="col-md-6">
                    <!-- BLock heading -->

                    <!-- Flex slider Content -->
                    <div class="flexslider-testimonial flexslider">
                        <ul class="slides">
                            <li>
                                <!-- Testimonial Content -->
                                <div class="testimonial-item">
                                    <!-- Quote -->
                                    <span class="quote lblue">&#8220;</span>
                                    <!-- Your comments -->
                                    <blockquote>
                                        <!-- Paragraph -->
                                        <p>Download Our Mobile App
iFoodGo has an Innovative food ordering app available on iPhone and Android. You can find nearby Restaurants and place order by viewing Restaurant Menu. The App helps you to take you to the Restaurant you like to visit through GPS, as well as share your experience by giving your own review and rating.</p>
                                    </blockquote>

                                </div>
                            </li>

                        </ul>
                    </div>
                    <div class='link-download pull-right'>
                        <img class="img-responsive" width="190px" src="frontend/img/downIos.png" alt="" />
                        <img class="img-responsive" width="190px" src="frontend/img/downAndroid.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="check_user" type="hidden" value="{{ Auth::check()? json_encode(Auth::user()) : ''}}" />

    <!-- Testimonial End -->
@stop
@section('script-screen')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoQDoMMccBAEVdy72njIRVylyN_hqInyQ&libraries=geometry,places" async defer></script>
<script src="{{'/frontend/js/pages/restaurantList.js'}}"></script>
<script src="{{'/frontend/js/pages/index.js'}}"></script>
<script src="{{'/frontend/js/partials/item_dished.js'}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script>
   AOS.init();
 </script>
@stop
