<!DOCTYPE html>
<html>
	<head>
		@if(Request::path() == 'orderDetail')
		<meta property="og:title"              content="{{$item->name}} - iFoodGo" />
		<meta property="og:description"        content="{{$item->description}} " />
		<meta property="og:image"              content="http://mysite.local:8000/{{$item->thumb}}"/>
		@endif
		<meta charset="utf-8">
		<!-- Title here -->
		<title>iFoodGo</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<!-- TEST -->
		
		<!--  -->
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<META Http-Equiv="Cache-Control" Content="no-cache">
		<META Http-Equiv="Pragma" Content="no-cache">
		<META Http-Equiv="Expires" Content="0">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Google social -->
		<meta name="Web client  (auto created by Google Service)" content="114503954721-sdtk8t3eme7s67641kkbukhebfkderek.apps.googleusercontent.com">

		<link rel="manifest" href="{{'/frontend/json/manifest.json'}}">

		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<!-- Styles -->
		<link  href="{{'/frontend/css/frontend-common.css'}}" rel="stylesheet">
		<link  href="{{'/frontend/css/frontend-styles.css'}}" rel="stylesheet">
		<!-- Bootstrap CSS -->
		<!-- <link  href="{{'/frontend/css/bootstrap.min.css'}}" rel="stylesheet"> -->
		<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<!-- <link href="{{'/frontend/css/settings.css'}}" rel="stylesheet"> -->
		<!-- FlexSlider Css -->
		<!-- <link rel="stylesheet" href="{{'/frontend/css/flexslider.css'}}" media="screen" /> -->
		<!-- Portfolio CSS -->
		<!-- <link href="{{'/frontend/css/prettyPhoto.css'}}" rel="stylesheet"> -->
		<!-- Font awesome CSS -->
		<!-- <link href="{{'/frontend/css/font-awesome.min.css'}}" rel="stylesheet"> -->
		<!-- Custom Less -->
		<link href="{{'/frontend/css/less-style.css'}}" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="{{'/frontend/css/style.css'}}" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <!--link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-pink.min.css"-->
	    <script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>
	    <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
		<!-- [if IE]><link rel="stylesheet" href="css/ie-style.css"><![endif] -->
		<script src="{{'/frontend/js/services/facebookShare.js'}}"></script>
		<script src="{{'/frontend/js/services/googleShare.js'}}"></script>
		<!-- Favicon -->
		<!-- <link rel="shortcut icon" type="image/x-icon" href="<% asset("favicon.ico") %>" />  -->
		@yield('head-lib')
		@yield('head')

	</head>
    <body @yield('bodyAttr') >
    	<!--<main>
		      <button disabled class="js-push-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
		        Enable Push Messaging
		      </button>
		    </p>
		    <section class="subscription-details js-subscription-details is-invisible">

		      <pre><code class="js-subscription-json"></code></pre>
		    </section>
		  </main>-->



		<!-- Google social -->
		<script>
			function googleOnload(idElement) {
				gapi.load('auth2', function(){
					auth2 = gapi.auth2.init({
						client_id: '114503954721-sdtk8t3eme7s67641kkbukhebfkderek.apps.googleusercontent.com',
						cookiepolicy: 'single_host_origin',
						scope: 'email profile'
					});
			});
		}
		</script>
		<script src="https://apis.google.com/js/platform.js?onload=googleOnload" async defer></script>
		<!-- /Google social -->
	<?php
    App::setLocale(session("langChoosen"));
    ?>
   	<!-- .modal -->
		@include('frontend.partials.modalUserLogin')
		<!-- /.modal -->
		<div class="wrapper">
			@include('frontend.partials.head')
			<!--loading overlay -->
			<div class="overlay_loading">
				<img class="img-responsive" src="frontend/img/logo.png" />
			</div>

			 <div class="main-content">
			@include('frontend.partials.modalUserAfterLogin')
	        	<!-- <div class="app-content-body"> -->
					@yield('content')
	        	<!-- </div> -->
	        </div>
	        @include('frontend.partials.footer')
	        <span class="totop"><a hre="#"><i class="fa fa-angle-up"></i></a></span>
		</div>
		<script type="text/javascript">
			var path_url = '<?php echo url('/')?>/';
		</script>
		<!-- <script type="text/javascript" src="<% asset("common/js/vendor-frontend.js") %>"></script> -->
		<!-- <script type="text/javascript" src="<% asset("vendor/ckeditor/ckeditor.js") %>"></script>
		<script type="text/javascript" src="<% asset("vendor/ckeditor/config.js") %>"></script> -->

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Javascript files -->
		<script src="{{'/frontend/libs/frontend-common.js'}}"></script>
		<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.36/jquery.form-validator.min.js"></script> -->

		<!-- jQuery -->
		<!-- <script src="{{'/frontend/js/jquery.js'}}"></script> -->
		<!-- Bootstrap JS -->
		<!-- <script src="{{'/frontend/js/bootstrap.min.js'}}"></script> -->
		<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<!-- 		<script type="text/javascript" src="{{'/frontend/js/jquery.themepunch.tools.min.js'}}"></script> -->
		<!-- <script type="text/javascript" src="{{'/frontend/js/jquery.themepunch.revolution.min.js'}}"></script> -->
		<!-- FLEX SLIDER SCRIPTS  -->
		<!-- <script defer src="{{'/frontend/js/jquery.flexslider-min.js'}}"></script> -->
		<!-- Pretty Photo JS -->
		<!-- <script src="{{'/frontend/js/jquery.prettyPhoto.js'}}"></script> -->
		<!-- Respond JS for IE8 -->
		<!-- <script src="{{'/frontend/js/respond.min.js'}}"></script> -->
		<!-- HTML5 Support for IE -->
		<!-- <script src="{{'/frontend/js/html5shiv.js'}}"></script> -->
		<!-- Custom JS -->
		<!-- <script src="{{'/frontend/js/custom.js'}}"></script> -->
		<!-- <script src="{{'/frontend/js/component/head.js'}}"></script> -->
		<script src="{{'/frontend/js/component/head.js'}}"></script>
		<script src="{{'/frontend/js/component/modalUserLogin.js'}}"></script>
		<script src="{{'/frontend/js/component/shoppingCart.js'}}"></script>
		<script src="{{'/frontend/js/component/shoppingCartUI.js'}}"></script>
		<script src="{{'/frontend/js/component/modalUserAfterLoginUI.js'}}"></script>
		<script src="{{'/frontend/js/services/serverService.js'}}"></script>
		<script src="{{'/frontend/js/services/cookieService.js'}}"></script>
		<script src="{{'/frontend/js/services/facebookSocial.js'}}"></script>
		<script src="{{'/frontend/js/services/googleSocial.js'}}"></script>

		<script src="{{'/frontend/js/component/jquery.timepicker.min.js'}}"></script>
		<script src="{{'/frontend/js/component/demo.js'}}"></script>
		<script src="{{'/frontend/js/component/subscribe.js'}}"></script>

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
		<script src="{{'/frontend/js/component/modalUserAfterLogin.js'}}"></script>


		<script src="{{'/scripts/main.js'}}"></script>

		<!-- JS code for this page -->
				<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

		<script>
		/* ******************************************** */
		/*  JS for SLIDER REVOLUTION  */
		/* ******************************************** */
				jQuery(document).ready(function() {
					   jQuery('.tp-banner').revolution(
						{
							delay:9000,
							startheight:500,

							hideThumbs:10,

							navigationType:"bullet",

							hideArrowsOnMobile:"on",

							touchenabled:"on",
							onHoverStop:"on",

							navOffsetHorizontal:0,
							navOffsetVertical:20,

							stopAtSlide:-1,
							stopAfterLoops:-1,

							shadow:0,

							fullWidth:"on",
							fullScreen:"off"
						});



				});
		/* ******************************************** */
		/*  JS for FlexSlider  */
		/* ******************************************** */

			$(window).load(function(){
				$('.flexslider-recent').flexslider({
					animation:		"fade",
					animationSpeed:	1000,
					controlNav:		true,
					directionNav:	false
				});
				$('.flexslider-testimonial').flexslider({
					animation: 		"fade",
					slideshowSpeed:	5000,
					animationSpeed:	1000,
					controlNav:		true,
					directionNav:	false
				});
			});


		/* Gallery */

		jQuery(".gallery-img-link").prettyPhoto({
		   overlay_gallery: false, social_tools: false
		});

				/* search */

		var path = "/autocomplete";
		$('#search').typeahead({
			source:  function (query, process) {
				console.log(query);
				var e = document.getElementById("searchOption");
				if(e==null)
				{
					var selOption = 0;
				}
				else{
				var selOption = e.options[e.selectedIndex].value;
				console.log(selOption);
			}
				return $.get(path, { query: query , selOption: selOption}, function (data) {
				console.log(data);
							return process(data);
					});
		}
});
	function clickSearch()
	{
		$('#search').addClass('focus-search').removeClass('out-focus-search');
		$('.header').addClass('blurEffect').removeClass('.out-blurEffect ');
		$('.tp-banner').addClass('blurEffect').removeClass('.out-blurEffect ');
		$('.main-content').addClass('blurEffect').removeClass('.out-blurEffect ');
	}
	function blurSearch()
	{
		$('#search').addClass('out-focus-search').removeClass('focus-search');
		$('.header').addClass('.out-blurEffect ').removeClass('blurEffect');
		$('.tp-banner').addClass('.out-blurEffect ').removeClass('blurEffect');
		$('.main-content').addClass('.out-blurEffect ').removeClass('blurEffect');

	}
		var ENVIRONMENT = {
			'APP_DEBUG': <?php echo json_encode(env('APP_DEBUG')); ?>,
			'DOMAIN_WWW':  <?php echo json_encode(URL::to('/')); ?>
		};
// 		$('.btnNext').click(function(){
// 			if($('#addForm').isValid()){
// 				$('.nav-tabs > .active').next('li').find('a').trigger('click');
// 			}
// });

// $('.btnPrevious').click(function(){
// $('.nav-tabs > .active').prev('li').find('a').trigger('click');
// });

		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 1) {
					$('.menu-bar').addClass('fixed');
					$('#logo-header h1').addClass('white minimize');
					$('.header .navbar-default .navbar-nav>li>a').addClass('white');
					$('.header .navbar-default .navbar-nav>li>form>a').addClass('white');
					$('.cart-link-order').addClass('white');
					$('.header .navbar-default .navbar-nav>li>div').addClass('shopping-cart');
					$('.header .logo p').addClass('hide');
					$('#logo-header').addClass('minimize');
					$('.icon-bar').addClass('icon-menu-toggle');
					$('.search-form').addClass('fixed-search');


			} else {
					$('.menu-bar').removeClass('fixed');
					$('#logo-header h1').removeClass('white minimize');
					$('#logo-header').removeClass('minimize');
					$('.header .logo p').removeClass('hide');
					$('.header .navbar-default .navbar-nav>li>a').removeClass('white');
					$('.header .navbar-default .navbar-nav>li>form>a').removeClass('white');
					$('.cart-link-order').removeClass('white');
					$('.header .navbar-default .navbar-nav>li>div').removeClass('shopping-cart');
					$('.icon-bar').removeClass('icon-menu-toggle');
					$('.search-form').removeClass('fixed-search');

			}

		});

		testLoading();
		 function testLoading()
		 {
		    $('.overlay_loading').addClass('on_overlay_loading');
			setTimeout(function () {
			$('.overlay_loading').removeClass('on_overlay_loading');
		}, 800);
		 }

		</script>
		@yield('script-lib')

		@yield('script-screen')
    </body>
</html>
