<!DOCTYPE html>
<html>
	<head>

		<meta charset="utf-8">
		<!-- Title here -->
		<title>iFoodGo</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Google social -->
		<meta name="Web client  (auto created by Google Service)" content="114503954721-sdtk8t3eme7s67641kkbukhebfkderek.apps.googleusercontent.com">
		<link  href="{{'/frontend/css/frontend-common.css'}}" rel="stylesheet">
		<link  href="{{'/frontend/css/frontend-styles.css'}}" rel="stylesheet">
		<link href="{{'/frontend/css/less-style.css'}}" rel="stylesheet">
		
	</head>
    <body @yield('bodyAttr') >

		<!-- /Google social -->
	<?php
	App::setLocale(session("langChoosen"));
	?>
	@yield('content')
	<script type="text/javascript">
			var path_url = '<?php echo url('/')?>/';
		</script>
		<script src="{{'/frontend/libs/frontend-common.js'}}"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
		<script src="{{'/frontend/js/services/serverService.js'}}"></script>
		<!-- JS code for this page -->
				<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

		<script>
		var ENVIRONMENT = {
			'APP_DEBUG': <?php echo json_encode(env('APP_DEBUG')); ?>,
			'DOMAIN_WWW':  <?php echo json_encode(URL::to('/')); ?>
		};
		</script>
	@yield('script-screen')
    </body>
</html>
