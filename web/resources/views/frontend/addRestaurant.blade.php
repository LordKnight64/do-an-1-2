@extends('frontend.layouts.masterNoSlider')

@section('content')
	    <!-- Banner Start -->
	    <div class="add-restaurant-banner" style=" background-image: url(frontend/img/addRestaurant.png);height: 400px; background-size: 100%;background-repeat: no-repeat;">
	        <div class="container" style="height:350px;">
	            <!-- Image -->
	            <div style=" padding-top: 40px;">
                   <span style ="font-size: 20px;"> Đến với   </span>
                   <span style="color: red;
                    font-size: 36px;
                    font-weight: bold;
                    font-family: fantasy;"> iFoodGo
                   </span>
                </div>
                 <div style=" padding-bottom: 10px;">
                       <span style ="font-size: 18px; "> Tạo và quản lý nhà hàng trực tuyến của bạn </span>  <span style ="font-size: 25px; color:red">  MIỄN PHÍ !!!! </span>
                </div>
                  <div style=" padding-bottom: 10px;">
                      <span style ="font-size: 18px;"> Đưa kinh doanh nhà hàng trực tuyến của bạn vào hoạt động  <br>mà không cần bất kỳ chi phí ban đầu </span>
                </div>
                <div style=" padding-bottom: 10px;">
                      <span style ="font-size: 18px;"> Hãy bắt đầu nhận đơn hàng trực tuyến với </span>
                      <span style="color: red;
                    font-size: 25px;
                    font-weight: bold;
                    font-family: fantasy;"> iFoodGo
                   </span>
                </div>
                <div style="width:400px; margin-top: 40px;">
                     <div class="add-restaurant-btn">
                        <a href="/addRestaurantForm" target="_blank">TẠO MỚI</a>
                    </div> &nbsp; &nbsp;
                    <div class="goto-restaurant-btn">
                        <a style="color:white;" href="http://restaurant.ifoodgo.vn" target="_blank">VÀO NHÀ HÀNG</a>
                    </div>
                </div>

	        </div>
	    </div>

	    <!-- Banner End -->
	    <div class="dishes padd">
            <div class="container">
                <!-- Default Heading -->
                <div class="default-heading">
                    <!-- Crown image -->
                    <img class="img-responsive" src="frontend/img/crown.png" alt="">
                    <!-- Heading -->
                    <h2>RESTAURANT OWNER FEATURES</h2>
                    <!-- Paragraph -->
                    <p>NHANH CHÓNG DỄ DÀNG VÀ GIÁ CẢ PHẢI CHĂNG VỚI HỆ THỐNG NHÀ HÀNG TRỰC TUYẾN </p>
                    <!-- Border -->
                    <div class="border"></div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="dishes-item-container">
                            <!-- Image Frame -->
                            <div class="img-frame features-icons">
                                <i class="fa fa-gears"></i>
                            </div>
                            <!-- Dish Details -->
                            <div class="dish-details">
                                <!-- Heading -->
                                <h3>Cài đặt nhanh</h3>
                                <!-- Paragraph -->
                                <p>Tạo website nhà hàng của bạn trong vài phút với iFoodGo</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="dishes-item-container">
                            <!-- Image Frame -->
                            <div class="img-frame features-icons">
                                <i class="fa fa-hand-o-up"></i>
                            </div>
                            <!-- Dish Details -->
                            <div class="dish-details">
                                <!-- Heading -->
                                <h3>Đặt hàng trực tuyến</h3>
                                <!-- Paragraph -->
                                <p>Hãy kinh doanh nhà hàng trực tuyến và bắt đầu nhận đơn hàng trực tuyến</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="dishes-item-container">
                            <!-- Image Frame -->
                            <div class="img-frame features-icons">
                                <i class="fa fa-list-alt"></i>
                            </div>
                            <!-- Dish Details -->
                            <div class="dish-details">
                                <!-- Heading -->
                                <h3>Quản lý đơn đặt hàng</h3>
                                <!-- Paragraph -->
                                <p>Quản lý đơn đặt hàng và bán hàng trực tuyến qua hệ thống báo cáo của nhà hàng</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="dishes-item-container">
                            <!-- Image Frame -->
                            <div class="img-frame features-icons">
                                <i class="fa fa-bullhorn"></i>
                            </div>
                            <!-- Dish Details -->
                            <div class="dish-details">
                                <!-- Heading -->
                                <h3>Xúc tiến</h3>
                                <!-- Paragraph -->
                                <p>Thúc đẩy kinh doanh nhà hàng trực tuyến của bạn với iFoodGo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

	    <!-- Main Content -->
		    <div class="main-content">
		        <div class='features'>
		        	<div class='container'>
		        		<div class='features-info text-center'>
		        			<h3> Lý do bạn sử dụng <span class='name-app-color'> iFoodGo </span> cho việc kinh doanh nhà hàng trực tuyến là? </h3>
		        		</div>
		        		<div class="row">
			        		<div class="col-md-6 col-sm-6">
					        	<div class='features-info'>
					        		<h3> CÁC TÍNH NĂNG </h3>
					        		<span>Lý do bạn chọn iFoodGo là: </span>
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> Tạo ra hệ thống đặt món ăn trực tuyến cho nhà hàng của bạn </li>
						        			<li><i class='fa fa-check-circle'> </i> Thiết lập chi phí riên cho dịch vụ, giao hàng và nhiều hơn nữa </li>
						        			<li><i class='fa fa-check-circle'> </i> Dễ dàng tạo ra website quản lí nhà hàng trực tuyến trong vài phút </li>
						        			<li><i class='fa fa-check-circle'> </i> Hỗ trợ và bảo trì miễn phí </li>
						        			<li><i class='fa fa-check-circle'> </i> Hệ thống tiếp thị email riêng để quảng bá doanh nghiệp nhà hàng của bạn  </li>
						        			<li><i class='fa fa-check-circle'> </i> Một giải pháp để nhận đơn đặt hàng thực phẩm thông qua ứng dụng di động và trang web cho dịch vụ ăn uống(mang đi, giao hàng, đặt chỗ) </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-6 col-sm-6">
				        		<img class="img-responsive pull-right" src="frontend/img/restaurant-img.png">
				        	</div>
		        		</div>


		        	</div>
		        </div>
		    </div>
		    <!-- / Main Content End -->
	    <!-- Footer Start -->
			@stop
		 @section('script-screen')
		 <script>
		 $(".add-restaurant-btn").hover(function(){
		   $(this).toggleClass("is-active");
		 });
		 </script>
		 @stop
