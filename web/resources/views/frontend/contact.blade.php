@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
		    <!-- Banner Start -->
		    <div class="banner padd">
		        <div class="container">
		            <!-- Image -->
		            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
		            <!-- Heading -->
		            <h2 class="white">CONTACT US</h2>
		            <ol class="breadcrumb">
		                <li><a  style="color: white" href="/">Home</a></li>
		                <li style="color: red;">Contact to {{$restaurant->name == '' ? "SystemEXE VietNam" : $restaurant->name}}</li>
		            </ol>
		            <div class="clearfix" style="height:20px;"></div>
		        </div>
		    </div>
		    <!-- Banner End -->
		    <!-- Inner Content -->
		    <div class="inner-page ">
		        <!-- Contact Us Start -->
		        <div class="contactus">
		            <div class="container">
					   <br>
		                <div class="row">
		                    <div class='restaurant-info'>
		                        <div class='restaurant-name'> {{$restaurant->name  ==  '' ? "SystemEXE Vietnam" : $restaurant->name}}</div>
														@if($restaurant->open_time != ' ')
		                        <div class='restaurant-open'>
		                            <img height="50" src="frontend/img/shop-open.png" alt="">
		                            <span>{{date('G:i', strtotime($restaurant->open_time))}} - {{date('G:i', strtotime($restaurant->close_time))}}</span>
														</div>
														@endif
		                    </div>
		                    <div class="col-md-6">
		                        <!-- Contact Us content -->
		                        <div class="row">
		                            <div class="col-md-6 col-sm-6">
		                                <!-- Contact content details -->
		                                <div class="contact-details">
		                                    <!-- Heading -->
		                                    <h4>Location</h4>
		                                    <!-- Address / Icon -->
		                                    <i class="fa fa-map-marker br-red"></i> <span style="width: 80%;">{{$restaurant->address}}</span>
		                                    <div class="clearfix"></div>
		                                </div>
		                            </div>
		                            <div class="col-md-6 col-sm-6">
		                                <!-- Contact content details -->
		                                <div class="contact-details">
		                                    <!-- Heading -->
		                                    <h4>On-line Order</h4>
		                                    <!-- Contact Number / Icon -->
		                                    <i class="fa fa-phone br-green"></i> <span>{{$restaurant->mobile}}</span>
		                                    <div class="clearfix"></div>
		                                    <!-- Email / Icon -->
		                                    <i class="fa fa-envelope-o br-lblue"></i> <span><a href="#">{{$restaurant->email}}</a></span>
		                                    <div class="social">
												<a href="https://vi-vn.facebook.com/pages/System-EXE-Viet-Nam/303609016342170" class="facebook"><i class="fa fa-facebook"></i></a>
												<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
												<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
												<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
											</div>
											<div class="clearfix"></div>

		                                </div>
		                            </div>
		                        </div>
		                        <!--/ Inner row end -->
		                        <!-- Contact form -->
		                        <div class="contact-form">
		                            <!-- Heading -->
		                            <h3>Contact Form</h3>
		                            <!-- Form -->
		                            <form role="form" action="/sent_contact" method="post" id="sent_contact">
																	{{ csrf_field() }}
																	@if(isset($restaurant->id))
																		<input hidden="hidden" value="{{$restaurant->id}}" id="res_id"/>
																		<input hidden="hidden" value="{{$restaurant->email}}" id="res_email"/>
																	@endif

		                                <div class="form-group">
		                                    <!-- Form input -->
		                                    <input name="name" class="form-control" type="text" placeholder="Name" data-validation="length|required" data-validation-length="max256"/>
		                                </div>
		                                <div class="form-group">
		                                    <!-- Form input -->
		                                    <input name="email" class="form-control" type="email" placeholder="Email" data-validation="length|required|email" data-validation-length="max256"/>
		                                </div>
																		<div class="form-group">
		                                    <!-- Form input -->
		                                    <input name="phone" class="form-control" type="text" placeholder="Phone" data-validation="length|required|number" data-validation-length="max256"/>
		                                </div>
		                                <div class="form-group">
		                                    <!-- Form text area -->
		                                    <textarea name="message" class="form-control" rows="3" placeholder="Message..." data-validation="length|required" data-validation-length="max1536"></textarea>
		                                </div>
		                                <!-- Form button -->
		                                <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-paper-plane " >	</i>&nbsp;Send</button>&nbsp;
		                                <button class="btn btn-danger btn-sm" type="reset"><i class="fa fa-eraser" >	</i>&nbsp; Reset</button>
		                            </form>
		                        </div>
		                        <!--/ Contact form end -->
		                    </div>
		                    <div class="col-md-6">
		                        <!-- Map holder -->
		                        <div class="map-container">
		                            <!-- Google Map -->
		                            <iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJQzZZ508pdTEReT5_WlXCXwc&key=AIzaSyCyIJqaHVEe5ucEJyDxBbtJe33BZRPEJ-A" allowfullscreen></iframe>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <!-- Contact Us End -->
		    </div>
		    <!-- / Inner Page Content End -->
		    <!-- Footer Start -->
@stop
@section('script-screen')
	<script src="{{'/frontend/js/pages/contact.js'}}"></script>
@stop
