@extends('frontend.layouts.masterNoSlider')

@section('content')
			<!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">User Login</h2>

	            <div class="clearfix"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
	    <!-- Inner Content -->
	    <div class="inner-page" id='main-login'>
	       <div class='login-form-container' style="padding: 30px;">
	       		<div class='login-content'>
				  
	       			<div class='company-name'> iFoodGo</div>
	       			<div class='devider'> </div>
	       			<div class='social-login'>
	       				<h5> Login with </h5>
	       				<button class='btn btn-primary'><i class="fa fa-facebook" aria-hidden="true"></i><span> Login with facebook </span> </button>
	       				<button class='btn btn-danger'><i class="fa fa-google-plus" aria-hidden="true"></i><span>Login with google+ </span></button>
	       			</div>
	       			<div class='devider'> </div>
	       			<div class='login-form'>
	       				<h5> Or with  email</h5>
	       				<form  action="" role="form">
									 {{csrf_field()}}
								<div class="form-group">
									<input name="email" type="email" class="form-control" placeholder="Email...">
								</div>
								<div class="form-group">
									<input name="password" type="password" class="form-control" placeholder="Password...">
								</div>

								<button class="btn btn-success pull-right" type="submit">Login</button>
								<div class='clearfix'> </div>
							</form>
	       			</div>
	       			<div class='login-footer'>
		       			<span><a href="/userRegister">Register</a></span>
		       			<span class="login-footer-Forgotpassword"><a href="/userForgotPassword">ForgotPassword</a></span>
	       			</div>
	       		</div>
	       </div>
	    </div>
	    <!-- / Inner Page Content End -->

	    <!-- Footer Start -->
@stop
@section('script-screen')
@stop
