@extends('frontend.layouts.masterNoSlider')

@section('content')
  <!-- Banner Start -->
	    <div class="banner banner1 padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white"> How It Works</h2>
	            <ol class="breadcrumb">
	               <li><a  href="/">Home</a></li>
	               <li style="color: red;">How It Works</li>
	            </ol>
				<br>

				<p style="color:white">Here Are your Questions with Answers ! </p>
	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
   		<div class="main-content">
		        <div class='features padd'>
		        	<div class='container'>
		        		<div class="row">
			        		<div class="col-md-8 col-sm-8 col-xs-8">
					        	<div class='features-info'>
					        		<h3> {{trans('admin/labels.howItsWork.userlabel')}} </h3>
					        		
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> How to Register as customer in iFoodGo? </li>
						        			<li><i class='fa fa-check-circle'> </i> How to update profile of customer in iFoodGo? </li>
						        			<li><i class='fa fa-check-circle'> </i> How to change password of customer account in iFoodGo? </li>
											<li><i class='fa fa-check-circle'> </i> How to reset password of customer account in iFoodGo? </li>
											<li><i class='fa fa-check-circle'> </i> How to place order in iFoodGo? </li>
											<li><i class='fa fa-check-circle'> </i> How to check history of placed order from customer account in iFoodGo? </li>
						        		 </ul>
					        		</div>
					        	</div>
				        	</div>
				        	<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				        		<img class="img-responsive" src="frontend/img/features/customer-icon.png" alt="">
				        	</div>
		        		</div>
						<br>
					
						<br>
			        	<div class="row">
			        		<div class="col-md-5 col-sm-5 col-xs-5">
					        	<img class="img-responsive" src="frontend/img/features/shop.png" alt="">
				        	</div>
				        	<div class="col-md-7 col-sm-7 col-xs-7">
				        		<div class='features-info'>
								
					        		<h3> RESTAURANT OWNER </h3>
					        		
					        		<div class="feature-details">
						        		<ul>
						        			<li><i class='fa fa-check-circle'> </i> How to add restaurant on iFoodGo? </li>
						        			<li><i class='fa fa-check-circle'> </i> How to edit restaurant details in iFoodGo? </li>
						        			<li><i class='fa fa-check-circle'> </i> How to upload menus of restaurant in iFoodGo? </li>
											<li><i class='fa fa-check-circle'> </i> How to approve order in iFoodGo? </li>
											<li><i class='fa fa-check-circle'> </i> How to decline order in iFoodGo? </li>
											
						        	    </ul>
					        		</div>
					        	</div>

				        	</div>
		        		</div>	        	
		        	</div>
		        </div>
		    </div>
		 
		    
			@stop
 			 @section('script-screen')
 			 @stop
