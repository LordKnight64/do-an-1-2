@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
			<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">Profile</h2>
	            <ol class="breadcrumb">
	                <li><a href="#">Home</a></li>
	                <li class="active">Profile</li>
	            </ol>
			
	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
			<!-- Inner Content -->
			<div class="inner-page">
				<div class="col-md-3 col-sm-6" style="margin-top: 30px;">
								 <div class="restaurant-categories-item">
										 <ul>
											 <form class="" action="/order" method="get">
												 <li class="active" style="">Profile</li>
												 <input type="hidden" name="restaurant_id" value="1" style="display:none">
												 <input type="hidden" name="category_id" value="" style="display:none">
											 </form><form class="" action="/order" method="get">
												 <li class="" onclick="window.location='/historyOrder'">History Orders</a></li>
												 <input type="hidden" name="restaurant_id" value="1" style="display:none">
												 <input type="hidden" name="category_id" value="" style="display:none">
											 </form>
											 <form>
												<li onclick="window.location='/myWishList'">My Wishlist</a></li>
											</form>
										 </ul>
									 </div>
								 </div><div class="container col-md-9 col-sm-12" style="">

				<div class="component-content restaurantList-profile">
											<!-- tabs -->
						<ul id="myTab" class="nav nav-tabs">
						  <li class="active"><a href="#profile" data-toggle="tab">User Profile</a></li>
						  <li><a href="#pass" data-toggle="tab">Change password</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane active fade in" id="profile">
							<form role="form" method="post" action="{{url('profile/detail')}}">
								{{csrf_field()}}
								<div class="form-group">
									<h5>Your email address: <span class='email'>{{Auth::user()->email}}</span> </h5>
								</div>
								<div class="form-group">
									<h5>First name:</h5>
									<input name="first-name" type="text" class="form-control" value="{{$data->first_name}}" placeholder="First name" required>
								</div>
								<div class="form-group">
									<h5>Last name:</h5>
									<input name="last-name" type="text" class="form-control" value="{{$data->last_name}}" placeholder="Last name"required>
								</div>
								<div class="form-group">
									<h5>Mobile Number:</h5>
									<input name="phone" type="text" class="form-control" value="{{$data->mobile}}" placeholder="Mobile Number" required>
								</div>
								<div class="form-group">
									<h5>Address:</h5>
									<textarea name="address" class="form-control" rows="3" placeholder="Address" required>{{$address->address}}</textarea>
								</div>
								<button class="btn btn-success" type="submit">Save changes</button>

							</form>
						  </div>

						  <div class="tab-pane fade" id="pass">
								<form role="form" id="changepass" method="post" action="{{url('profile/password')}}">
									{{csrf_field()}}
									<div class="form-group">
										<h5>Current Password <span class='require'>*</span>:</h5>
										<input name="currentpass" id="currentpass" type="password" class="form-control" placeholder="Current Password" data-validation="length" data-validation-length="min6">
									</div>
									<div class="form-group">
										<h5>New Password <span class='require'>*</span>:</h5>
										<input name="newpass" id="newpass" type="password" class="form-control" placeholder="New Password" data-validation="length" data-validation-length="min6">
									</div>
									<div class="form-group">
										<h5>Retype Password:</h5>
										<input name="newpass_confirm" id="newpass_confirm" type="password" class="form-control" placeholder="Retype Password" data-validation="confirmation|length" data-validation-length="min6"  data-validation-confirm="newpass">
									</div>

									<button class="btn btn-success" type="submit">Update password</button>

								</form>
						  </div>
						</div>
					</div>
				</div>
			</div><!-- / Inner Page Content End -->
			<div class="clearfix"></div>
			<!-- Footer Start -->
			@stop
			@section('script-screen')
			<script src="{{'/frontend/js/pages/profilepassword.js'}}"></script>
			@stop
