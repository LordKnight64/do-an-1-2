@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">Forgot Password</h2>

	             <ol class="breadcrumb">
	                <li><a style="color: #009688;" href="/">Home</a></li>
	                <li ><a style="color:red " href="">forgot password</a></li>
	            </ol>	
				 <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
	    <!-- Inner Content -->
	    

		 <!-- Inner Content -->
	    <div class="inner-page" id='main-login'>
	       <div class='register-form-container' style="padding: 30px;">
	       		<div class='register-content' style="width:850px">
	       				<div class="row" >
			        		<div class="col-md-5 col-sm-5 col-xs-5" style="
								background: linear-gradient(rgba(255,255,255,0.6), rgba(255,255,255,0.6)), url(frontend/img/forgotpass.jpg) center center no-repeat;
								    height: 275px;
									background-size: 96% 100%;
									background-position: 16px 16px;
								border-right: 1px solid #E91E63" >
								<div class="row" >
			        				<div class="col-md-4 col-sm-4 col-xs-4" >
									
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8" style=" margin-top: 15px;
										font-size: 20px;
										color: #009688;
										font-weight: bold;">
										Get Password:
									</div>
								</div>

								<div class="row" style=" margin-top: 15px;">
			        				
									<div class="col-md-10 col-sm-10 col-xs-10">
										<div class="feature-details">
 						        		<ul style="list-style-type: none;">
						        			<li style="    color:#795548; line-height: 25px;"><i class="fa fa-check-circle"> </i> Enter your email </li> 
											<li style="    color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i> Submit your email to get new password </li> 
						        		   <li style="    color: #795548; line-height: 25px;"><i class="fa fa-check-circle"> </i> Go to your email to change new password </li> 
						        		 </ul>
					        		</div>
									</div>
								</div>

							</div>

							<div class="col-md-7 col-sm-7 col-xs-7" >
								
								<div class='register-form' style="width:500px">
									<form id="forgotPass" role="form" method="post" action="{{ route('user.password.email') }}">
    						{{ csrf_field() }}
	       			      <div class='forgotPassWord-form'>
						   @if(session('statusReset') == 'success')
						   	<div class="alert alert-success">
								<strong>Email lấy lại mật khẩu đã được gửi</strong> Vui lòng kiểm tra hộp thư mail của bạn.
							</div>
						   @endif
						   @if(session('statusReset') == 'fail')
					   		<div class="alert alert-danger">
								<strong>Email không tồn tại</strong> Vui lòng kiểm tra lại.
							</div>
							@endif
							<h6> Email Address <span class="required">*</span></h6>
							<div class="form-group">
								<input type="text" name="email" data-validation="email" class="form-control" placeholder="Email...">
								
							</div>
							</div>
								<div class="forgotPassWord-footer">
									<h6><a style="color:#009688" href="/userLogin">Already on iFoodGo?</a></h6>
									<button class="btn btn-success pull-left" type="submit"> <i class="fa fa-paper-plane"></i> Send </button>
									<div class='clearfix'> </div>
								</div>
								</form>
							</div>
						</div>
	       			
	       		</div>
	       </div>
	    </div>
	    <!-- / Inner Page Content End -->


	    <!-- Footer Start -->
			@stop
			@section('script-screen')
				<script src="{{'/frontend/js/pages/forgotPassword.js'}}"></script>
			@stop
