@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">About System</h2>
	            <ol class="breadcrumb">
	                <li><a href="index.html">Home</a></li>
	                <li class="active">About System</li>
	            </ol>
	            <div class="clearfix"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
	    <!-- Inner Content -->
	    <div class="inner-page-about-system padd">
	    	<div class="default-heading">
		                    <!-- Crown image -->
		                    <img class="img-responsive" src="frontend/img/crown.png" alt="">
		                    <!-- Heading -->
		                    <h2>About</h2>
		                    <!-- Border -->
		                    <div class="border"></div>
		    </div>
	        <!-- About system -->


	    </div>
	    <!-- / Inner Page Content End -->
	    <div class="inner-page">
	        <!-- About company -->
	        <div class="about-system-tab1">
	            <div class="grid_system outline row" id="index_2nd">

			        <section class="grid_1_2 col-md-6" style="height: 348px;">
			            <h2 class="h2_1">
							<a href="#tab2">
							    <div>
									<i class="fa fa-arrow-circle-right title-icon-change" aria-hidden="true"></i>
			                    	<font class="title-name-change">Company Profile</font>
			                    	<div class='clearfix'> </div>
			                    </div>
			                    <img class='img-responsive' src="../frontend/img/aboutSystem/sec_index01.png" alt="Company Profile">
			                </a>
						</h2>
			            <p><font><font>Dummy text dummy text dummy text dummy text dummy text dummy text</font></font></p>
			        </section>

			        <section class="grid_1_2 col-md-6" style="height: 348px;">
			            <h2 class="h2_1">
							<a href="#tab3">
								 <div>
									<i class="fa fa-arrow-circle-right title-icon-change" aria-hidden="true"></i>
				                   <font class="title-name-change">Message from the President</font>
				                   <div class='clearfix'> </div>
			                    </div>
			                    <img class='img-responsive' src="../frontend/img/aboutSystem/sec_index02.png" alt="Message from the President">
			                </a>
						</h2>
			            <p><font><font>Dummy text dummy text dummy text dummy text dummy text dummy text</font></font></p>
			        </section>
                 	 <div class='clearfix'> </div>
    			</div>
	        </div>
	        <div class="about-system-tab2" id='tab2'>
	            <table class="table2">

	                <tbody>
		                <tr>
		                    <th>Company name</th>
		                    <td>System Exe Vietnam (SYSTEMEXE VIETNAM COMPANY LIMITED)</td>
		                </tr>
		                <tr>
		                    <th>Company date</th>
		                    <td>October 2010</td>
		                </tr>
		                <tr>
		                    <th>Capital</th>
		                    <td>300.000 USD</td>
		                </tr>
		                <tr>
		                    <th>Location</th>
		                    <td>
		                        <p>Ho Chi Minh City Tan Binh District 13-chome, Cong Hoa Street 364 Etown building the fourth floor</p>
		                        <p class="lnk1"><a href="https://goo.gl/maps/ogwffXzij332" target="_blank">Google Map<img src="../frontend/img/aboutSystem/window.png" alt="icon" class="icon"></a></p>

		                    </td>
		                </tr>
		                <tr>
		                    <th>Management</th>
		                    <td>
								<dl class="dl2">
									<dt>Chairman</dt>
		                            	<dd>Kiyotaka Goto</dd>
		                              <dt>The President</dt>
		                            	<dd>Nguyen Tan ton</dd>
		                        	</dl>
		                    </td>
		                </tr>
		                <tr>
		                    <th>Affiliate</th>
		                    <td><p class="lnk1"><a href="http://www.system-exe.co.jp/" target="_blank">Corporation System Exe<img src="../frontend/img/aboutSystem/window.png" alt="Corporation System Exe" class="icon"></a></p>
						</td>
		                </tr>
		                <tr>
		                    <th>Number of employees</th>
		                    <td>51 (December 2015 now)</td>
		                </tr>

	          	 	</tbody>
          	    </table>
	        </div>
	        <div class="about-system-tab3" id='tab3'>
	            <div class="outline">

					<div class="img_center"><img class="flex_img" src="../frontend/img/aboutSystem/greeting_img01.png" alt="Representative Director and President"></div>

			        <p class="lead"><font><font>Dummy text dummy text </font></font><br><font><font>
			        dummy text dummy text dummy text </font></font><br><font><font>
			        dummy text dummy text</font></font></p>

			        <p class="p1"><font><font>Dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text Stummy text dummy text dummy text dummy text dummy text</font></font></p>

			        <p class="p1"><font><font>Dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text dummy text Stummy text dummy text dummy text dummy text dummy text</font></font></p>

			        <div class="name">
			        		<p><font><font>Representative Director and President</font></font></p>
			             <p><font><font>NGUYEN THANH THONG</font></font></p>
			            <p><font><font>(Quantantong) </font></font></p>
			        </div>

			    </div>
	        </div>
	    </div>
	    <!-- Footer Start -->
			@stop
			@section('script-screen')
			@stop
