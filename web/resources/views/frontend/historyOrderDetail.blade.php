@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">History Order</h2>
	            <ol class="breadcrumb">
	                <li><a href="#">Home</a></li>
	                <li class="active">History Order</li>
	            </ol>
			
	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->

		        <!-- Testimonial End -->


			<!-- Inner Content -->
			<div class="inner-page">
				 <div class="container">
				 	<div class="default-heading" style="margin-top:25px;">
	                    <!-- Crown image -->
	                    <!-- Heading -->
	                    <h2>HISTORY ORDER DETAIL</h2>

	                    <!-- Border -->
	                    <div class="border"></div>
	                </div>
				<div class="component-content history-order-detail">
					<div class="row">
						<div class='col-md-4 col-sm-4'>
							<div class='history-order-detail-info'>
								<div class='customer-info'>
									<div class='pull-left'>
										<div class=''> <img class="img-responsive" src="frontend/img/icon1/images.jpeg"> OrderId: <span> {{$req->id}}</span> </div>
										<div class=''> <img class="img-responsive" src="frontend/img/icon1/avatar-128.png">{{$req->name}}</div>
										<div class=''> <img class="img-responsive" src="frontend/img/icon1/phone-24.png"> {{$req->phone}}  </div>
									</div>
									<div class='pull-right status-order badge br-green'>Status:<span>
										<font class="bold" style="color:{{$req->order_sts==0 ? '#fff' : ''}}
										{{$req->order_sts==1 ? '#fff' : ''}}
										{{$req->order_sts==2 ? '#fff' : ''}}
										{{$req->order_sts==99 ? '#fff' : ''}}">
										{{$req->order_sts== '0'? 'Requested' : ''}}
										{{$req->order_sts== '1'? 'Verified' : ''}}
										{{$req->order_sts== '2'? 'Delivery' : ''}}
										{{$req->order_sts== '99'? 'Pending' : ''}}
									</font></span></div>
									<div class='clearfix'> </div>
								</div>
								<div class='order-time'>
									<div>
										<h5 class='bold' style="color:#666666"> Request time: </h5>
										<div>
											 <img class="img-responsive" src="frontend/img/icon1/calendar-date-month-planner-24.png"> <span>{{$req->order_ts}}</span>
										</div>
									</div>
									<div>
										<h5 class='bold' style="color:#666666"> Delivery time: </h5>
										<div>
											 <img class="img-responsive" src="frontend/img/icon1/calendar-date-month-planner-24.png"> <span>{{$req->delivery_ts}}</span>
										</div>
									</div>
								</div>

								<div class='order-price'>
									<div>
										Estimate:
										<span class='pull-right bold text-right'> <span>{{$estimate}}</span> VND </span>
										<div class='clearfix'> </div>
									</div>
									<div>
										Ship fee: ({{$req->delivery_distance}}KM)
										<span class='pull-right bold text-right'> <span>{{$req->delivery_fee}}</span> VND </span>
										<div class='clearfix'> </div>
									</div>
									<div>
										Discount code: <span class='discount-code'> {{$req->promo_code == null ? 'Not activated' : $req->promo_code}} </span>
										<span class='discount-code br-purple'> {{$req->promo_code == null ? '0%' : $req->promo_discount*100}}{{$req->promo_code == null ? '' : '%'}} </span>
										<span class='pull-right bold text-right'> <span>  {{$req->promo_code == null ? '0' : $discount_fee}}</span> VND </span>
										<div class='clearfix'> </div>
									</div>
									<div class='border-line'> </div>
									<div>
										<span class='' style="font-size:20px">Total:</span>
										<span class='pull-right bold text-right' style="font-size:20px"> <span class="red">{{$total}}</span> VND </span>
										<div class='clearfix'> </div>
									</div>
									<div class='border-line'> </div>
									<div>
										<span> Payment method: </span>
										<span class='pull-right bold color-orange'>{{$req->delivery_type==0? ' Cash On Delivery': ''}} </span>
										<div class='clearfix'> </div>
									</div>
								</div>
								<div class='note'>
									<h5 class='bold' style="color:#666666">Note </h5>
									<div>
										<span id="note">{{$req->notes}}</span>
										<script type="text/javascript">
										var note = document.getElementById("note").textContent;

										function ConvertBR(input)
										{
										// Converts carriage returns
										// to <BR> for display in HTML
										var output = "";
											for (var i = 0; i < input.length; i++) {
													if (i + 1 < input.length && (input.charCodeAt(i + 1) == 10)) {
															i++;
															output += "<BR>";
													} else {
															output += input.charAt(i);
													}
											}
											return output;
											}
											document.getElementById("note").innerHTML= ConvertBR(note);
										</script>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-9">
							<div class='restaurant-list-result chefs'>
								<div class='row'>
									@foreach ($data as $dat)
									<form method="get" action="/orderDetail" class="ellipsis" style="display:inline;">
									<div class='restaurant-item col-md-4 col-sm-4' onclick="event.preventDefault(); this.parentNode.submit()">
										<div class="chefs-member item" style="border: 1.8px solid rgba(128, 128, 128, 0.47);margin-top: 3px;">
									<!-- Chefs member header -->
									<div class="chefs-head" style="margin-bottom:15px">
										<!-- Member background image -->
										<img class="chefs-back img-responsive" src="{{$dat->thumb}}" alt="">
									</div>
									<div class='restaurant-detail' onclick="event.preventDefault(); this.parentNode.submit()">
										<div class='restaurant-name'>
											
											<a href="#" onclick="event.preventDefault(); this.parentNode.submit()" class="restaurant-list-name" style="font-size:20px" ><strong>{{$dat->name}}</strong></a>
											<input name="restaurant_id" style="display:none;" value="{{$dat->restaurant_id}}" type="text">
											<input name="item_id" style="display:none;" value="{{$dat->id}}" type="text">
											<!--Tin add them -->
											<input name="order_id" style="display:none;" value="{{$dat->order_id}}" type="text">
											<input name="seq_no" style="display:none" value="{{$dat->seq_no}}" type="text">
											
										</div>
										<div class='restaurant-address' style="overflow: hidden;height: 42.5px;word-break: normal;white-space: initial;text-overflow: ellipsis;">
											<span class="" style="color:#666666">Price: </span><span class="bold" style="font-size:18px">{{number_format($dat->price)}}&nbsp</span><span class="" style="color:#666666">VND</span>
											<span class="bold" style="font-size:18px">X{{$dat->quantity}}</span>
											
											<div class='clearfix'> </div>
											
										</div>
										<div id="addoption">
											@foreach($haveOption as $ho)
												@if($dat->item_id == $ho->item_id)
												</br>
													<h4 style='text-align:left'>Chọn thêm</h4>
													<p align="left">-----------------------------------</p>
													<!--<ul>-->
												@endif
											@endforeach
											
											@foreach($option as $op)
												@if($dat->item_id == $op->item_id && $dat->seq_no == $op->seq_no)
													@foreach($optionGroup as $g)
														@if($g->id == $op->option_id)
															<p align="left"><b>{{$g->name}} ({{$op->name}}) : 
															@foreach($optionPrice as $price)
																@if($op->item_id == $price->item_id & $op->option_id == $price->option_id & $op->option_value_id==$price->option_value_id )
																	<label>{{number_format($price->add_price)}} VND</label>
																@endif
															@endforeach
															</b></p>
														@endif
													@endforeach
												@endif
											@endforeach
											<!--</ul>-->
										</div>
									</div>
								</div>
									</div>
									</form>
									@endforeach
								</div>
							</div>
						</div>
						<div class='page-result '>
							{{ $data->appends(Request::capture()->except('page'))->fragment('main')->links() }}
						<div class='clearfix'> </div>
						</div>
					</div>
				</div>
			</div><!-- / Inner Page Content End -->

			<!-- Footer Start -->
			@stop
			@section('script-screen')

			@stop
