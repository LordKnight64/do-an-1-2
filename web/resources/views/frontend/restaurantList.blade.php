@extends('frontend.layouts.masterNoSlider') @section('content')
<!-- Header End -->
<!-- Slider Start
        #################################
            - THEMEPUNCH BANNER -
        #################################	-->
<!-- Banner Start -->
<div class="banner padd">
    <div class="container">
        <!-- Image -->
        <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
        <!-- Heading -->
        <h2 class="white" id="#main">Restaurant list</h2>
         <ol class="breadcrumb">
	                <li><a style="color:white" href="/">Home</a></li>
	                <li style="color: red;">Restaurant list</li>
	            </ol>
        <div class="clearfix" style="height:20px;"></div>
    </div>
</div>
<!-- Banner End -->
<!-- Slider End -->
<!-- Main Content -->
<div class="restaurant-list" id="main">
    <div class='restaurant-list-container'>
        <div class="restaurant-location-search">
            <div class='restaurant-location-choose margin-bt-5'>
                <span><i class="fa fa-street-view" aria-hidden="true"></i>   </span>
            </div>
            <div class='restaurant-location-change'>
                <a class="ghost-button-semi-transparent" href='#' data-toggle="modal" onclick='openMap()' data-target="#currentLocationModal">Change Location</a> <a class="ghost-button-semi-transparent margin-t-5" data-toggle="modal" onclick='openMap(1)' data-target="#currentLocationModal"><i class="fa fa-compass" aria-hidden="true"></i> Search Restaurants Nearby</a>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="currentLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style='display:none'>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Drag drop marker to your location</h4>
                        </div>
                        <div class="modal-body">
                            <div class='map-current-location'>
                                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                <div id="map"></div>


                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>
    <div class='main-content'>
        @if(Request::get('rank') == null && Request::get('type') == null)
        <div class='search-restaurant-result'>Tìm thấy <strong style="color:red;font-size:35px">{{$itemCount}}</strong> nhà hàng theo yêu cầu của bạn</div>
        @else @if(Request::get('rank') != null)
        <div class='search-restaurant-result'><strong style="color:red;font-size:35px">Top</strong> nhà hàng được đánh giá cao nhất</div>
        @else @if(Request::get('type') != null)
        <div class='search-restaurant-result'>Nhà hàng thuộc loại <strong style="color:red;font-size:35px">{{$res_type->cd_name}}</strong></div>
        @endif @endif @endif
        <div class='container'>
            <div class='row'>
                <div class="restaurant-search-form col-md-3 col-sm-3">

                    <form role="form" method="get" action="/search">
                        <input type="hidden" name="province" value="{{ Request::get('province')}}">

                        <div class="form-group">
                            <h5>Search restaurants</h5>
                            <div class="input-group" style="    width: 102%;">
                                <input name="key" style="height:34px" type="text" class="form-control" id="search" autocomplete="off" placeholder="Restaurant name.." value="{{$searchInput}}">

                                <span class="input-group-btn">
												<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
											</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Order by</h5>
                            <select class="form-control" name="sort">
											<option></option>
											<option value="az"  {{ Request::get('sort')=='az' ? 'selected' : '' }} >Alphabet</option>
											<option value="fee" {{ Request::get('sort')=='fee' ? 'selected' : '' }}>Delivery fee</option>
										</select>
                        </div>
                        <div class="form-group">
                            <h5>Filter by</h5>
                            <select class="form-control" name="filter">
											<option></option>
											@foreach($filter_value as $filter)
											<option value="{{$filter->cd}}" {{ Request::get('filter')== $filter->cd ? 'selected' : '' }}>{{$filter->cd_name}}</option>
											@endforeach
											<!--<option>Dine-in</option>-->
										</select>
                        </div>
                        <div class="form-group">
                            <h5>Restaurant Type</h5>
                            <select class="form-control" name="restaurant_type">
											<option></option>
											@foreach($restaurant_type as $rt)

											<option value="{{$rt->cd}}"  {{ Request::get('restaurant_type')== $rt->cd ? 'selected' : '' }}
												@if(Request::get('type') != null)
													@if($res_type->cd == $rt->cd)
														selected
													@endif
												@endif
												>{{$rt->cd_name}}</option>

											@endforeach
										</select>
                        </div>
                    </form>
                </div>
                <div class="col-md-9 col-sm-9">
                    <div class='restaurant-list-result chefs'>
                        <h5>Restaurant List</h5>
                        <div class='row'>
                            @foreach ($data as $dat)
                            <form method="get" action="/restaurantDetail" class="ellipsis" style="display:inline;">
                                <div class='restaurant-item col-md-4 col-sm-4' onclick="event.preventDefault(); this.parentNode.submit()">
                                    <div class="chefs-member item">

                                        <div class="chefs-head">
                                            <!-- Member background image -->
                                            <img alt="restaurant logo" class="chefs-back img-responsive" style="    border-bottom: solid 1px #eee;    " src="{{$dat->logo_path}}" alt="">
                                        </div>
                                        <div class="item-hover br-red hidden-xs"></div>
                                        <a title="Order now" class="link hidden-xs" href="/order?restaurant_id={{$dat->id}}&category_id=" onclick="event.stopImmediatePropagation();">Order now </a>
                                          <!-- Chefs member header -->
                                        <div class='restaurant-detail' onclick="event.preventDefault(); this.parentNode.submit()">

                                            <div class='restaurant-name'>
                                                <a href="#" onclick="event.preventDefault(); this.parentNode.submit()" class="restaurant-list-name"><strong>{{$dat->name}}</strong></a>
                                                <input name="id" style="display:none;" value="{{$dat->id}}" type="text">


                                                <div class='order-status pull-right'>
                                                    @if($dat->food_ordering_system_type ==0)
                                                    <i class="fa fa-circle view" aria-hidden="true"></i> @else
                                                    <i class="fa fa-circle order" aria-hidden="true"></i> @endif
                                                </div>

                                                <div class='clearfix'> </div>
                                            </div>
                                            <div class='restaurant-rate'>
                                                @for ($i = 1; $i <= $dat->rating; $i++)
                                                    <span><i class="fa fa-star" aria-hidden="true" ></i></span>
                                                @endfor
                                                @for ($i = $dat->rating; $i < 5; $i++)
                                                    <span><i class="fa fa-star-o" aria-hidden="true"></i></span>
                                                @endfor

                                            </div>
                                           <div style=" text-align: left; padding-left: 10px;"><span style="color:#4CAF50;font-weight:bold;"> Open: </span> {{$dat->open_time}} - {{$dat->close_time}} </div>
                                            <div class='restaurant-address' style="overflow: hidden;height:50.5px;word-wrap:break-word;white-space:normal">
                                                <span><i class="fa fa-cutlery pull-left" aria-hidden="true"></i>{{$dat->address}} </span>
                                                <div class='clearfix'> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class='page-result '>
                    {{ $data->appends(Request::capture()->except('page'))->fragment('main')->links() }}
                    <div class='clearfix'> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- / Main Content End -->
<!-- Footer Start -->

@stop @section('script-screen')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoQDoMMccBAEVdy72njIRVylyN_hqInyQ&libraries=geometry,places" async defer></script>
<script src="{{'/frontend/js/pages/restaurantList.js'}}"></script>
@stop
