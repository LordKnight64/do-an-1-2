@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">Restaurant Login</h2>

	            <div class="clearfix"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
	    <!-- Inner Content -->
	    <div class="inner-page" id='main-login'>
	       <div class='restaurantLogin-form-container'>
	       		<div class='restaurantLogin-content'>
	       			<div class='restaurantLogin-title'>
	       				<h4> Restaurant Login </h4>
	       			</div>
	       			<div class='restaurantLogin-form'>
	       				<form role="form">
							<h6> Enter Email Address <span class="required">*</span></h6>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Password...">
							</div>
							<h6> Enter Password <span class="required">*</span></h6>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Password...">
							</div>
						</form>
	       			</div>
	       			<div class="restaurantLogin-footer">
						<h6><a href="/forgotPassword">Forgot Password?</a></h6>
						<button class="btn btn-primary pull-left" type="button">Login</button>
						<button class="btn btn-primary pull-left restaurantLogin-footer-signup" type="button">Sign up</button>
						<div class='clearfix'> </div>
					<div class="restaurantLogin-footer">
	       		</div>
	       </div>
	    </div>
	    <!-- / Inner Page Content End -->

	    <!-- Footer Start -->
			@stop
			@section('script-screen')
			@stop
