@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">My Wish List</h2>
	            <ol class="breadcrumb">
	                <li><a href="#">Home</a></li>
	                <li style="color:red">My Wish List</li>
	            </ol>

	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->

			<!-- Inner Content -->
			<div class="inner-page" style="padding-top:30px">
				<div class="col-md-3 col-sm-6" style="">
								<div class="restaurant-categories-item">
										<ul>
											<form>
												<li class="" style="" onclick="window.location='/profile'">Profile</li>
												<input type="hidden" name="restaurant_id" value="1" style="display:none">
												<input type="hidden" name="category_id" value="" style="display:none">
											</form>
											<form>
												<li class="" onclick="window.location='/historyOrder'">History Orders</a></li>
												<input type="hidden" name="restaurant_id" value="1" style="display:none">
												<input type="hidden" name="category_id" value="" style="display:none">
											</form>
											<form>
												<li class="active" onclick="window.location='/myWishList'">My Wishlist</a></li>
												<input type="hidden" name="restaurant_id" value="1" style="display:none">
												<input type="hidden" name="category_id" value="" style="display:none">
											</form>
										</ul>
									</div>
								</div>
				 <div class="container">
				    <div class="component-content history-order" style="background:0">
					<!-- order-requests-->
					<div id='order-requests'>
							<div class="table-responsive">
								<table class="table-wishlist" >
									<tbody>
										<tr style="background: #f75353;color: white;  height: 42px;">
											<td style="width:20%" class='text-center'>&nbsp;Image</td>
											<td style="width:40%"class='text-center'>&nbsp;Detail</td>
											<td style="width:20%"class='text-center'>&nbsp;Restaurant</td>
											<td class='text-center'>&nbsp;Option</td>
										</tr>
										@foreach($wishlist as $wish)
										<tr>
											<td>
                                                <div style="padding:10% 0 10% 0;">
                                                    <img  src="{{$wish->thumb}}">
                                                </div>
                                            </td>
											<td>
                                                <div class="info-wish" style="height: 198px;">
                                                    <a href="/orderDetail?restaurant_id={{$wish->restaurant_id}}&item_id={{$wish->item_id}}"><h4>{{$wish->item_name}}</h4></a>
                                                    <p style="    padding-bottom: 10px;">{{$wish->description}} </p>
                                                    <span style="color: #FF5722;">Price: {{ number_format($wish->price, 0) }} {{$wish->default_currency}} </span>
                                                   &nbsp;&nbsp; | &nbsp;&nbsp;
													<span style="color: #FF5722;">Point: {{$wish->point }} </span>
                                                    <br>
                                                    <a style="margin-top:10px" id="view" href="/orderDetail?restaurant_id={{$wish->restaurant_id}}&item_id={{$wish->item_id}}" class="btn btn-info btn-sm"><span>View Detail</span></a>
                                                </div>
                                            </td>
											<td class='text-center'>
                                                <div id="restaurant" style="padding: 0 10% 0 10%; height: 198px;">
                                                <div class="restaurant-name">
                                                    <a href="/restaurantDetail?id={{$wish->restaurant_id}}" class="restaurant-list-name">
                                                        <p><strong>{{$wish->restaurant_name}}</strong></p>
                                                    </a>
                                                </div>
                                                
                                                <div class="restaurant-rate">
                                                    @for ($i = 1; $i <= $wish->rating; $i++)
                                                    <span><i class="fa fa-star" aria-hidden="true" ></i></span>
                                                    @endfor
                                                    @for ($i = $wish->rating; $i < 5; $i++)
                                                    <span><i class="fa fa-star-o" aria-hidden="true"></i></span>
                                                    @endfor
                                                </div>
                                                </div>
                                            </td>
											<td class='text-center'>
                                                <div style="padding: 0 10% 0 10%; height: 198px;text-align: left;">
                                                    <span style="text-align: left;">Data added:</span>
                                                    <br>
													<span style="text-align: left;">{{$wish->cre_ts}}</span>
                                                    <div style="padding-bottom:10%" >
                                                    <br>
													<a href="/deleteWishlist?restaurant_id={{$wish->restaurant_id}}&item_id={{$wish->item_id}}" class="btn btn-danger-active btn-sm" > <i class="fa fa-trash-o fa-lg" style="color:white"></i><span> Delete</span></a>
                                                    </div>
                                                    <div>
                                                    <a class="btn btn-success btn-sm" onclick="setShoppingCartItemInOrder({{json_encode($wish)}})"> <i class="fa fa-cart-plus" style="color:white"></i><span> Add to cart</span></a>   
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                        @endforeach
									</tbody>
								</table>
							  </div>
					</div>
					<!-- order-requests-->

					</div> 
					<!-- order-pending-->
					<!-- order-delivery-->

					<!-- order-pending-->
				</div>
			</div><!-- / Inner Page Content End -->
			<!-- Footer Start -->
			@stop
			@section('script-screen')
            <script src="{{'/frontend/js/pages/wishlist.js'}}"></script>
			@stop
