@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
 <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">News</h2>
	            <ol class="breadcrumb">
	                <li><a style="color: #009688;" href="">Home</a></li>
	                <li ><a style="color: #009688;" href="notificationList">News</a></li>
					<li style="color:red">News Detail</li>
	            </ol>
			
	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
		<div class="testimonial padd restaurant-banner">
		    <div class="container">      


			<!-- Inner Content -->
			<div class="inner-page">
				<!-- Blog Start -->

				<div class="blog">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<!-- The new post done by user's all in the post block -->
								<div class="blog-post">
									
									<div class="sidebar-widget">
									
										@foreach($news as $n)
										<div class="">
										<!-- Post Images -->
											<div class="blog-img pull-left">
												<img style="height:300px; border:0px; margin-right:30px" src="{{$n->thumb}}" class="img-responsive img-thumbnail" alt="" />
											</div>
										<!-- Meta for this block -->
											<div class="meta">
												<i class="fa fa-calendar"></i>&nbsp; {{$n->cre_ts}}

											</div>
										<!-- Heading of the  post -->
											<h2 style="font-size:25px;    margin-top: 10px;">{{$n->title}}</h2>
											
											<hr style="    border-top: 4px solid #f75353; 
												width: 70px;
												margin-top: 0px;
												margin-bottom: 12px;
												margin-left: 270px;" />
												<!-- Horizontal line -->
											<h5 style=" text-align: right; color:#FF5722"> {{$n->restaurant_name}}</h5>
										<!-- Paragraph -->
											<p class="newsContents">{{$n->content}}</p>
											
											<div class="clearfix"></div>
										</div>
										@endforeach
									</div>
									
									<!-- Pagination end-->
								</div>
							</div> <!--/ Main blog column end -->
							<div class="col-md-4">
								<!-- Blog page sidebars -->
								<div class="blog-sidebar">
									<div class="row">
										<div class="col-md-12 col-sm-6">
											<!-- Blog sidebar page widget -->
											<div class="sidebar-widget">
												<!-- Widget Heading -->
												<h4>Search</h4>
												<!-- search button and input box -->
												<form role="form" method="get" class="form-inline" action="/notificationList">
													<div class="input-group">
														
														<input type="hidden" name="restaurant_type" value="">
														<input type="text" name="inputContent" class="form-control" placeholder="Type to search" value="">
														
														<span class="input-group-btn">
															<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
														</span>
													</div>
												</form><!--/ Form end -->
											</div><!--/ Widget end -->
										</div>
										<div class="col-md-12 col-sm-6">
											<!-- Blog sidebar page widget -->
											<div class="sidebar-widget">
												<!-- Widget Heading -->
												<h4>Recent Post</h4>
												<!-- Recent post list -->
												<ul class="list-unstyled">
														@foreach($recentNews as $rn)
														<li><i class="fa fa-angle-double-right"></i> <a href="/newsDetail?news_id={{$rn->id}}">{{$rn->title}}</a>
														<br />
														<div id="recentContent" style="white-space:nowrap;overflow:hidden">
															<p>{{$rn->content}}</p>
														</div>
														</li>
													@endforeach
												</ul>
											</div><!--/ Widget end -->
										</div>
										<div class="col-md-12 col-sm-6">
											<!-- Blog sidebar page widget -->
											<div class="sidebar-widget">
												<!-- Widget Heading -->
												<h4>Tags</h4>
												<div class="tag">
												@foreach($tagType as $t)
													<a href="notificationList?restaurant_type={{$t->cd}}&inputContent=" class="{{$t->color}}">{{$t->cd_name}}</a>
												@endforeach 
												</div>
											</div><!--/ Widget end -->
										</div>

									</div><!--/ Inner row end -->
								</div><!--/ Page sidebar end -->
							</div>
						</div><!--/ Row end -->
					</div>
				</div>

				<!-- Blog End -->




			</div><!-- / Inner Page Content End -->

			<!-- Footer Start -->
			@stop
			@section('script-screen')
				<script src="{{'/frontend/js/pages/notificationList.js'}}"></script>
			@stop
