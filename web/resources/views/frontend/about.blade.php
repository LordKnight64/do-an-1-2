@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">About Us</h2>
	            <ol class="breadcrumb">
	                <li><a href="/">Home</a></li>
	                <li >{{$restaurant->name}} /	 About Us</li>
	            </ol>
	            <div class="clearfix" style="padding-bottom:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
	    <!-- Inner Content -->
	    <div class="inner-page padd">
	        <!-- About company -->
	        <div class="about-company ">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-5">
	                        <!-- About Compnay Item -->
	                        <div class="about-company-item">
	                            <!-- About Company Image -->
	                            <img class="img-responsive img-thumbnail" src="{{$restaurant->logo_path}}" alt="" />
	                        </div>
	                    </div>
	                    <div class="col-md-7">
	                        <!-- About Compnay Item -->
	                        <div class="about-company-item">
	                            <!-- Heading -->
	                            <h3 style="color:#666">About Our <span class="lblue">{{$restaurant->name}} </span></h3>
	                            <!-- Paragraph -->
	                            <p style="word-wrap:break-word;word-space:normal">{{$restaurant->description}}</p>
	                            <br />
	                            <!--/ Line break -->
	                            <div class="row">
	                                <div class="col-md-12 col-sm-6 col-xs-6">
	                                    <!-- About company inner items -->
	                                    <div class="about-company-inner">
	                                        <!-- Paragraph with icon -->
	                                        <span class="company-feature"><i class="fa fa-mobile br-red" style="font-size:26px"></i> <b style="    color: #795548;
    font-size: 15px;">{{$restaurant->mobile}}</b></span>
	                                    </div>
	                                </div>
	                                <div class="col-md-12 col-sm-6 col-xs-6">
	                                    <!-- About company inner items -->
	                                    <div class="about-company-inner">
	                                        <!-- Paragraph with icon -->
	                                        <span class="company-feature"><i class="fa fa-internet-explorer br-green" style="font-size:18px"></i> <b style="    color: #795548;
    font-size: 15px;">{{$restaurant->website}}</b></span>
	                                    </div>
	                                </div>
	                                <div class="clearfix visible-xs"></div>
	                                <div class="col-md-12 col-sm-6 col-xs-6">
	                                    <!-- About company inner items -->
	                                    <div class="about-company-inner ">
	                                        <!-- Paragraph with icon -->
	                                        <span class="company-feature"><i class="fa fa-facebook br-blue" style="font-size:23px"></i> <b style="    color: #795548;
    font-size: 15px;"><?php echo substr($restaurant->facebook_link, 12);?></b></span>
	                                    </div>
	                                </div>
	                                <div class="col-md-12 col-sm-6 col-xs-6">
	                                    <!-- About company inner items -->
	                                    <div class="about-company-inner">
	                                        <!-- Paragraph with icon -->
	                                        <span class="company-feature"><i class="fa  fa-map-marker br-blue"></i> <b style="    color: #795548;
    font-size: 15px;">{{$restaurant->address}}</b></span>
	                                    </div>
	                                </div>

									   <div class="col-md-12 col-sm-6 col-xs-6">
	                               
		                     
														@if($restaurant->open_time != ' ')
		                        <div class='restaurant-open'>
		                            <img height="40" src="frontend/img/shop-open.png" alt="">
		                            <span>{{date('G:i', strtotime($restaurant->open_time))}} - {{date('G:i', strtotime($restaurant->close_time))}}</span>
														</div>
														@endif
	                               

									  
		                    </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- / Inner Page Content End -->
	    <!-- Inner Content -->
	    <div class="inner-page">
	        <!-- General Info Start -->
	        <div class="general">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-12 col-sm-12">
	                        <!-- General information content -->
	                        <div class="general-content">
	                            <!-- Navigation tab -->
	                            <ul class="nav nav-tabs">
	                                <!-- Navigation tabs (Job titles ). Use unique value for "href" in "anchor tags". -->
	                                <li class="active"><a href="#tab1" data-toggle="tab">About Us</a></li>
	                                <li><a href="#tab2" data-toggle="tab">Terms &amp; Conditions</a></li>
	                                <li><a href="#tab3" data-toggle="tab">Privacy Policy</a></li>
	                            </ul>
	                            <!-- Tab content -->
	                            <div class="tab-content">
	                                <!-- In "id", use the value which you used in above anchor tags -->
	                                <div class="tab-pane active" id="tab1">
	                                    <!-- Heading -->
	                                    <h5>About Us</h5>
	                                    <!-- Paragraph -->
										{{$restaurant->about_us}}
	                                </div>
	                                <!--/ tab-pane end -->
	                                <div class="tab-pane fade" id="tab2">
	                                    <!-- heading -->
	                                    <h5>Terms &amp; Conditions</h5>
	                                    {{$restaurant->term_of_use}}
	                                </div>
	                                <!--/ tab-pane end -->
	                                <div class="tab-pane fade" id="tab3">
	                                    <!-- Heading -->
	                                    <h5>Privacy Policy</h5>
	                                    {{$restaurant->private_policy}}
	                                </div>
	                            </div>
	                            <!--/ Tab content end -->
	                        </div>
	                    </div>
	                   
	                </div>
	            </div>
	        </div>
	        <!-- General Info End -->
	    </div>
	    <!-- / Inner Page Content End -->
	    <!-- Footer Start -->
			@stop
 			@section('script-screen')
 			@stop
