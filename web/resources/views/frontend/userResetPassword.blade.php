@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
	    <!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">Reset Password</h2>

	           <ol class="breadcrumb">
	                <li><a style="color: #009688;" href="/">Home</a></li>
	                <li ><a style="color:red " href="">Reset password</a></li>
	            </ol>	
				 <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->
		 <!-- Inner Content -->
	    <div class="inner-page" id='main-login'>
	       <div class='register-form-container' style="padding: 30px;">
	       		<div class='register-content' style="width:850px">
	       				<div class="row">
			        		<div class="col-md-4 col-sm-4 col-xs-4" style="background: linear-gradient(rgba(255,255,255,0.6), rgba(255,255,255,0.6)), url(frontend/img/register.jpg) center center;
								height: 419px;
							    border-right: 1px solid #E91E63;
								background-size: 150% 100%;
								background-repeat: no-repeat;">
								<div class="row">
			        				<div class="col-md-2 col-sm-2 col-xs-2" >
									
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8" style=" margin-top: 15px;
										font-size: 20px;
										color: #009688;
										font-weight: bold;">
										Change password
									</div>
								</div>

								<div class="row" style=" margin-top: 15px;">
			        				
									<div class="col-md-10 col-sm-10 col-xs-10">
										<div class="feature-details">
 						        		<ul style="list-style-type: none;">
						        			<li style="    color: #FF5722; font-weight:bold;"><i class="fa fa-check-circle"> </i> Khám phá món ăn </li> 
											<span style="font-size: 13px; color:#795548" >Tìm ra nhà hàng gần bạn với ẩm thực yêu thích từ iFoodGo</span>
						        			
											<li style="    color: #FF5722;font-weight:bold;"><i class="fa fa-check-circle"> </i> Lựa chọn thực phẩm yêu thích với iFoodGo </li>
						        			<span style="font-size: 13px; color:#795548" >Chọn thực phẩm theo tâm trạng và sở thích từ hàng trăm thực đơn đặc biệt hấp dẫn</span>
											<li style="    color:#FF5722;font-weight:bold;"><i class="fa fa-check-circle"> </i> Đặt hàng và Thanh toán </li>
											<span style="font-size: 13px; color:#795548" >Thanh toán tiền ẩm thực, tiền phí giao hàng khi nhận hàng</span>
											<li style="    color: #FF5722;font-weight:bold;"><i class="fa fa-check-circle"> </i> Nhận điểm thưởng với iFoodGo </li>
											<span style="font-size: 13px; color:#795548" >Thưởng thức thực phẩm của bạn ở bất kì địa điểm và cung cấp nhận xét</span>
											
						        		 </ul>
					        		</div>
									</div>
								</div>

							</div>

							<div class="col-md-8 col-sm-8 col-xs-8" style="">
							 	<form id="forgotPass" role="form" method="post" action="{{ route('user.password.request') }}">
											{{ csrf_field() }}
									<input type="hidden" name="token" value="{{ $token }}">		
									<div class='forgotPassWord-form'>
											<h6> Email <span class="required">*</span></h6>
											<div class="form-group" style="    margin-right: 15px;">
												<input type="email" id="email" class="form-control Required" name="email" placeholder="Email" data-validation="email" placeholder="Email...">
											</div>
											<h6> New Password <span class="required">*</span></h6>
											<div class="form-group" style="    margin-right: 15px;">
												<input type="password" name="password" id="password" class="form-control Required" placeholder="Password"  data-validation="length" data-validation-length="min6">
											</div>
											<h6> Confirm new password  <span class="required">*</span></h6>
											<div class="form-group" style="    margin-right: 15px;">
												<input type="password" name="password_confirmation" id="password-confirm" class="form-control Required" placeholder="Password" data-validation="confirmation|length" data-validation-length="min6"  data-validation-confirm="password">
											</div>
									</div>
									<div class="forgotPassWord-footer">
										<button class="btn btn-success pull-left" type="submit"> <i class="fa fa-paper-plane"></i> Send </button>
										<div class='clearfix'> </div>
									</div>
									</form>
								
							</div>
						</div>
	       			



	       		</div>
	       </div>
	    </div>


	    <!-- Footer Start -->
			@stop
			@section('script-screen')
				<script src="{{'/frontend/js/pages/forgotPassword.js'}}"></script>
			@stop
