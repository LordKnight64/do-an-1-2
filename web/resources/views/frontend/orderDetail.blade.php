@extends('frontend.layouts.masterNoSlider')
@section('content')

<input id="restaurant_id" type="hidden" value="{{$restaurant->id}}"/>
<input id="item_id" type="hidden" value="{{$item->id}}"/>
<input id="number" type="hidden" value="{{$number}}"/>

<!-- Inner Content -->
<div class="banner padd">
   <div class="container">
      <!-- Image -->
      <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
      <!-- Heading -->

      <h2 class="white">{{$restaurant->name}}</h2>
        <ol class="breadcrumb">
	                <li><i style="color: white;" class="fa fa-map-marker " ></i> <a style="color: white; font-size:14px;" href="#">{{$restaurant->address}}</a></li>
	            </ol>
      <p style="color: white;"><i class="fa fa-phone " ></i> {{$restaurant->tel}}</p>
				<p style="color: white;"><i class="fa fa-clock-o  " ></i> {{date('G:i', strtotime($restaurant->open_time))}} AM - {{date('G:i', strtotime($restaurant->close_time))}} PM</p>
	      <a style="color: white;; font-size:14px;" title="click here to view about [ {{$restaurant->name}} ]" href="/about?id={{$restaurant->id}}"><i class="fa fa-info-circle" >	</i>&nbsp; About us </a> &nbsp;&nbsp; &nbsp;| &nbsp; &nbsp; &nbsp;<a title="click here to view contact[ {{$restaurant->name}} ]" style="color: white; font-size:14px;" href="/contact?id={{$restaurant->id}}"><i class="fa fa-envelope" >	</i>&nbsp; Contact us </a>
				<ol class="breadcrumb">
	                <li><a style="color: white" href="/">Home</a></li>
	                <li ><a style="color: white" href="search">Restaurant list</a></li>
                  <li ><a style="color: white" href="restaurantDetail?id={{$restaurant->id}}"> Restaurant Detail  </a></li>
                  <li ><a style="color: white" href="order?restaurant_id={{$restaurant->id}}"> Order List  </a></li>
                  <li  style="color: red;">Order Detail</li>
	            </ol>
    <div class="clearfix" style="height:20px;"></div>
   </div>
</div>
<div class="inner-page">
   <!-- Single Item Start -->
   <div class="single-item">
      <div class="container">
         <!-- Shopping single item contents -->
         <div class="single-item-content">
            <div class="row">
                  @if (count($option) == 0 )
                       <div class="col-md-4 col-sm-12 table-order-detail-left">
                   @endif
                   @if (count($option) != 0 )
                       <div class="col-md-4 col-sm-12 table-order-detail-left">
                    @endif
                  <!-- Product image -->
                      <img class="img-responsive img-thumbnail" src="{{$item->thumb}}" alt="" />
                        <div class="form-group btn-order">
                         <h3 style="float:left;margin-top: 7px;font-size:25px">{{$item->name}}</h3>
                          <div class="social" style="float:right">
                            <a href="#" id="fbbtn" class="facebook" onclick="shareFB()"><i class="fa fa-facebook"></i></a>
                            <a id="googlebtn" href="https://plus.google.com/share?url=https://www.ifoodgo.vn/" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=380');return false;" class="google-plus"><i class="fa fa-google-plus" data-action="share"></i></a>
                            @if(Auth::check() == false)
                                <a id="wishlist" title="add to wishlist" href="#" onclick="addWishlist()" class="google-plus"><i class="fa fa-heart-o f-2x"></i></a>
                            @else
                                <a id="wishlist" title="add to wishlist" href="#" onclick="addWishlist()" class="
                                  @if($Wish == 1)
                                google-plus-active
                                @else
                                google-plus
                                @endif"><i class="fa fa-heart-o f-2x"></i></a>
                            @endif
                          </div>
                        </div>
                      <br>
                      <br>
               </div>


               @if (count($option) != 0 )
               <div class="col-md-8 col-sm-7">
                @endif
                 @if (count($option) == 0 )
               <div class="col-md-8 col-sm-7">
                @endif
                  <div class="row">
                   @if (count($option) != 0 )
                     <!-- Heading -->
                     <div class="col-md-6 col-sm-12" style="background: white;
    border-radius: 5px;
    box-shadow: 0 0 15px rgba(0,0,0,0.3);">
                        <!-- Heading -->
                        <!-- Recipe ingredients -->

                        <div class="menu-details">
                           <ul class="list-unstyled">
                              @foreach ($option as $o)
                              <li class="br-red" style="padding: 2px;">
                                 <div class="menu-list-item">
                                    <!-- Icon -->
                                    <a href="#" class="option-menu">{{$o->name}}</a>
                                    <!-- Price badge -->
                                    <div class="clearfix"></div>
                                 </div>
                              </li>
                              @foreach ($option_value as $ov)
                              @if ($ov->option_id == $o->option_id)
                              <li class="{{$ov->id}}" onclick="checkOption({{$ov->id}})">
                                 <div class="menu-list-item">
                                    <!-- Icon -->
                                    <i class="fa fa-angle-right"></i>
                                    <a href="#" onclick="checkOption()" class="option-value-menu" style="color:#000" >
                                      <span>{{$ov->name}}</span>
                                    </a>
                                    <!-- Price badge -->
                                    <div class="fix-click-radio">
                                    </div>

                                    <input name="{{$o->name}}" type="radio" value="{{$ov->name}}" class="pull-right option-input radio"/>
                                    <span class="pull-right" style="background: 0;"></span>
                                    <span class="pull-right option-value-price" > {{number_format($ov->add_price,0,'','.')}} {{$restaurant->default_currency}}(<strong style="color:green">{{$ov->point}}</strong> <i class="fa fa-btc" aria-hidden="true"></i>)</span>

                                    <div class="clearfix"></div>
                                    <input type="hidden" class="option-value-add_price" value="{{$ov->add_price}}"/>
                                    <input type="hidden" class="option_id" value="{{$o->id}}"/>
                                    <input type="hidden" class="option_name" value="{{$o->name}}"/>
                                    <input type="hidden" class="option_value_id" value="{{$ov->id}}"/>
                                    <input type="hidden" class="option_value_name" value="{{$ov->name}}"/>
                                    <input type="hidden" class="option_point" value="{{$ov->point}}"/>

                                    @foreach($order_option as $oo)
                                      @if(Request::get('seq_no') == $oo->seq_no)
                                        @if($oo->option_value_id == $ov->id )
                                            <input type="hidden" class="checker" value="{{$oo->option_value_id}}">
                                        @endif
                                      @endif
                                    @endforeach
                                 </div>
                              </li>
                              @endif
                              @endforeach
                              @endforeach
                           </ul>
                        </div>

                     </div>
                   @endif

                     @if (count($option) == 0 )
                       <div class="col-md-7 col-sm-12">
                        <div class="component-content" style="margin-top:0px; padding-top:0px">

                          <ul id="myTab" class="nav nav-tabs">
                            <li class="active" style="padding:0!important"><a href="#detail" data-toggle="tab">Detail</a></li>
                            <li style="padding:0!important;border:0"><a href="#review" data-toggle="tab">Reviews <span class="review-count">{{$count_review}}</span></a> </li>
                          </ul>
                          <div id="myTabContent" class="tab-content">
                              <div class="tab-pane active fade in" id="detail">
                                <p class="text-justify" style="font-size:15px;">{{$item->description}}</p>
                              </div>

                              <div class="tab-pane fade" id="review">
                                <!-- <h2 style="color:#9a9a9a"><strong><strong></h2> -->
                                @if($count_review!=0)
                                @foreach($review as $rv)
                                <div class="tab-content" style="border-top:2px solid #ddd;margin-bottom:15px">
                                    <span style="color:#9a9a9a;font-size:20px">{{$rv->title}}</span>
                                    <span class="pull-right">Review by <strong>{{$rv->user_name}}</strong> {{$rv->last_time}}</span>
                                    <br />
                                 @for ($i = 1; $i <= $rv->rating; $i++)
                                  <span ><i class="fa fa-star" aria-hidden="true" ></i></span>
                                  @endfor
                                   <p style="font-size:15px">{{$rv->content}}</p>
                                </div>
                                @endforeach
                                @endif

                                <div class="tab-content" style="border-top:3px solid #f75353 !important;margin-bottom:15px">

                                <div class="rating" >
                                <span  onclick="star(5)"><i id="5" class="fa fa-star-o" aria-hidden="true" ></i></span>
                                <span  onclick="star(4)"><i id="4" class="fa fa-star-o" aria-hidden="true" ></i></span>
                                <span  onclick="star(3)"><i id="3" class="fa fa-star-o" aria-hidden="true" ></i></span>
                                <span  onclick="star(2)"><i id="2" class="fa fa-star-o" aria-hidden="true" ></i></span>
                                <span  onclick="star(1)"><i id="1" class="fa fa-star-o" aria-hidden="true" ></i></span>
                               </div>
                               <span id="guilde-star" style="display:inherit;color:orange;font-size:25px">Đánh giá: </span>

                                 <div class="alert alert-warning alert-dismissible fade in" style="margin-bottom:0">
                                   Để gửi đánh giá bạn phải hoàn tất đánh giá chất lượng dựa trên sao <i class="fa fa-smile-o" aria-hidden="true"></i>
                                 </div>
                               <br>
                               <input id="title_review" type="text" name="title" class="form-control" value="" placeholder="Title...." style="margin-bottom:5px">

                               <textarea id="content_review" name="content" class="form-control" style="margin-bottom:5px" rows="4" cols="53" style="resize:none" placeholder="Content..."></textarea>
                               <button id="sendReview" class="btn btn-danger" onclick="sendReview()" disabled>Gửi</button>
                             </div>

                              </div>
                          </div>
                      </div>
                       </div>
                      @endif
                       @if (count($option) == 0 )
                     <div class="col-md-5 col-sm-12">
                      @endif
                      @if (count($option) != 0 )
                     <div class="col-md-6 col-sm-12">
                      @endif
                        <!-- Form inside table wrapper -->
                         <div class="restaurant-rate">
                           @for ($i = 1; $i <= $avg; $i++)
                           <i class="fa fa-star" aria-hidden="true"></i>
                           @if(($avg-$i)<1 && ($avg-$i)>0)
                             <i class="fa fa-star-half-o" aria-hidden="true"></i>
                           @endif
                           @endfor



                            <a> Reviews ({{$count_review}}) </a>

                            <a href="#" onclick="addReview()" data-toggle="#review" style="float:right"> Add Your Review </a>

                          </div>

                        <div class="table-responsive table-order-detail-right">
                           <!-- Ordering form -->
                           <form role="form">
                             {{ csrf_field() }}
                              <!-- Table -->
                              <table class="table table-bordered">

                                <tr>
                                   <td>SKU</td>
                                   <td>{{$item->sku}}</td>
                                </tr>
                                <tr>
                                   <td>Shipping</td>
                                   <td>{{$restaurant->delivery_charge}}</td>
                                </tr>
                                 <tr>
                                   <td>Quantity</td>
                                   <td class="cell-quantity">
                                      <div class="form-group">
                                         <input id="input-quantity" class="form-control" type="number" value="1"/>
                                      </div>
                                   </td>
                                </tr>
                                 <tr>
                                    <td>Price</td>
                                    <td>
                                      <span id="priceDisplay">{{number_format($item->price,0,'','.')}} {{$restaurant->default_currency}}</span>
                                      <input id="price" type="hidden" value="{{$item->price}}">
                                      <input id="priceAfter" type="hidden" value="{{$item->price}}">
                                    </td>
                                 </tr>

                                  <tr>
                                    <td>Point</td>
                                    <td>
                                      <span id="pointDisplay"><strong style="color:green">{{number_format($item->point,0,'','.')}} </strong><i class="fa fa-btc icon-point" aria-hidden="true" ></i> </span>
                                      <input id="point" type="hidden" value="{{$item->point}}">
                                      <input id="pointAfter" type="hidden" value="{{$item->point}}">
                                    </td>
                                 </tr>

                                 <tr>
                                    <td>Total Price</td>
                                    <td><span id="total-price">{{number_format($item->price,0,'','.')}} {{$restaurant->default_currency}}</span></td>
                                 </tr>
                                <tr>
                                   <td>Delivery Time</td>
                                   <td>45 Min</td>
                                </tr>
                                <tr>
                                   <td>Min order</td>
                                   <td></td>
                                </tr>

                                  <tr>
                                   <td>Notes</td>
                                   <td class="cell-quantity">
                                      <div class="form-group">
                                         <textarea  id="notes" class="form-control"></textarea>
                                      </div>
                                   </td>
                                </tr>
                              </table>



                              <div class="form-group btn-order">
                                <button type="button" style="width: 160px;" class="btn btn-success btn-sm" onclick="doContinue({{json_encode($item)}}, {{json_encode($restaurant)}})"><i class="fa fa-cart-plus " style="color:white;font-size: 15px;"></i> &nbsp; Add to cart & Continue</button>
                                <button type="button" class="btn btn-danger btn-sm" onclick="doCheckout({{json_encode($item)}}, {{json_encode($restaurant)}})"><i class="fa fa-cart-arrow-down" style="color:red;font-size: 15px;"></i> &nbsp; Checkout</button>
                                <!-- <a href="/order?restaurant_id={{$restaurant->id}}" > -->
                                <!-- </a> -->
                                <a href="/checkout" >
                                </a>
                              </div>

                           </form>
                           <!--/ Table End-->
                        </div>
                        <!--/ Table responsive class end -->
                     </div>
                  </div>

                  <!--/ Inner row end  -->
                  <div class='clearfix'> </div>
               </div>
               @if (count($option) != 0 )
                <!-- Single item details -->
                <div id="addReviewForm" class=" col-md-12 col-sm-12">
                 <div class="component-content" style="margin-top:15px; padding-top:0px;">

                   <ul id="myTab" class="nav nav-tabs">
                     <li style="padding:0!important" class="active"><a href="#detail" data-toggle="tab">Detail</a></li>
                     <li style="padding:0!important;border:0"><a href="#review" data-toggle="tab">Reviews <span class="review-count">{{$count_review}}</span></a> </li>
                   </ul>
                   <div id="myTabContent" class="tab-content">
                       <div class="tab-pane active fade in" id="detail">
                         <p class="text-justify" style="font-size:15px;">{{$item->description}}</p>
                       </div>

                       <div class="tab-pane fade" id="review">
                         <!-- <h2 style="color:#9a9a9a"><strong><strong></h2> -->
                         @if($count_review!=0)
                         @foreach($review as $rv)
                         <div class="tab-content" style="border-top:2px solid #ddd;margin-bottom:15px">
                             <span style="color:#9a9a9a;font-size:20px">{{$rv->title}}</span>
                             <span class="pull-right">Review by <strong>{{$rv->user_name}}</strong> {{$rv->last_time}}</span>
                             <br />
                          @for ($i = 1; $i <= $rv->rating; $i++)
                           <span ><i class="fa fa-star" aria-hidden="true" ></i></span>
                           @endfor
                            <p style="font-size:15px">{{$rv->content}}</p>
                         </div>
                         @endforeach
                         <div class='page-result pagin-review' >
       										{{ $review->appends(Request::capture()->except('page'))->fragment('addReviewForm')->links() }}
       								<div class='clearfix'> </div>
               					</div>
                        @endif

                         <div class="tab-content" style="border-top:3px solid #f75353 !important;margin-bottom:15px">

                         <div class="rating" >
                         <span  onclick="star(5)"><i id="5" class="fa fa-star-o" aria-hidden="true" ></i></span>
                         <span  onclick="star(4)"><i id="4" class="fa fa-star-o" aria-hidden="true" ></i></span>
                         <span  onclick="star(3)"><i id="3" class="fa fa-star-o" aria-hidden="true" ></i></span>
                         <span  onclick="star(2)"><i id="2" class="fa fa-star-o" aria-hidden="true" ></i></span>
                         <span  onclick="star(1)"><i id="1" class="fa fa-star-o" aria-hidden="true" ></i></span>
                        </div>
                          <span id="guilde-star" style="display:inherit;color:orange;font-size:25px">Đánh giá: </span>
                          <div class="alert alert-warning alert-dismissible fade in" style="margin-bottom:0">
                            Để gửi đánh giá bạn phải hoàn tất đánh giá chất lượng dựa trên sao <i class="fa fa-smile-o" aria-hidden="true"></i>

                          </div>
                        <br>
                        <input id="title_review" type="text" name="title" class="form-control" value="" placeholder="Title...." style="margin-bottom:5px">

                        <textarea id="content_review" name="content" class="form-control" style="margin-bottom:5px" rows="4" cols="53" style="resize:none" placeholder="Content..."></textarea>
                        <button id="sendReview" class="btn btn-danger" onclick="sendReview()" disabled>Gửi</button>
                      </div>
                       </div>
                  </div>
              </div>
            </div>
               @endif
            </div>
         </div>
      </div>
   </div>
   <!-- Single Item End -->
</div>
<!-- / Inner Page Content End -->
<!-- Footer Start -->
@stop
@section('script-screen')
<script src="{{'/frontend/js/pages/orderDetail.js'}}"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'vi'}
</script>

@stop
