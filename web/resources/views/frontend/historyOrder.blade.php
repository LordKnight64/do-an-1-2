@extends('frontend.layouts.masterNoSlider')

@section('content')
		    <!-- Header End -->
<!-- Banner Start -->
	    <div class="banner padd">
	        <div class="container">
	            <!-- Image -->
	            <img class="img-responsive" src="frontend/img/crown-white.png" alt="" />
	            <!-- Heading -->
	            <h2 class="white">History Order</h2>
	            <ol class="breadcrumb">
	                <li><a href="#">Home</a></li>
	                <li style="color:red">History Order</li>
	            </ol>

	            <div class="clearfix" style="height:20px;"></div>
	        </div>
	    </div>
	    <!-- Banner End -->

			<!-- Inner Content -->
			<div class="inner-page" style="padding-top:30px">
				<div class="col-md-3" >
					<div class="restaurant-categories-item">
						<ul>
							<form>
								<li class="" style="" onclick="window.location='/profile'">Profile</li>
								<input type="hidden" name="restaurant_id" value="1" style="display:none">
								<input type="hidden" name="category_id" value="" style="display:none">
							</form>
							<form>
								<li class="active" onclick="window.location='/historyOrder'">History Orders</a></li>
								<input type="hidden" name="restaurant_id" value="1" style="display:none">
								<input type="hidden" name="category_id" value="" style="display:none">
							</form>
							<form>
								<li onclick="window.location='/myWishList'">My Wishlist</a></li>
								<input type="hidden" name="restaurant_id" value="1" style="display:none">
								<input type="hidden" name="category_id" value="" style="display:none">
							</form>
						</ul>
					</div>
				</div>
				<div class=" container ">
					<div class="component-content history-order " style="background:0">
					<!-- order-requests-->
					<div id='order-requests'>
					<div class="table-responsive">
						<table class="table table-hover table-bordered table-striped">
							<tbody>
								<tr style="background: #f75353;color: white;">
									<td class='text-center'><i class="fa fa-shopping-cart " style="color:white"></i>&nbsp;Order #</td>
									<td class='text-center'><i class="fa fa-calendar " style="color:white"></i>&nbsp;Date</td>
									<td class='text-center'><i class="fa fa-address-card-o " style="color:white"></i>&nbsp;Ship to</td>
									<td class='text-center'><i class="fa  fa-money" style="color:white"></i>&nbsp;Order total</td>
									<td class='text-center'>Status</td>
									<td class='text-center'>Action</td>
								</tr>
								@foreach ($request as $req)
								<tr>
									<td>{{$req->id}}</td>
									<td class='text-center'>{{$req->delivery_ts}}</td>
									<td class='text-center'>{{$req->name}}</td>
									<td class='text-right bold'><span>{{$req->price}}</span> VND</td>
									<td class='text-center'>
										<font class="" style="color:{{$req->order_sts==0 ? '#FFCA28' : ''}}
											{{$req->order_sts==1 ? '#1E88E5' : ''}}
											{{$req->order_sts==2 ? '#4CAF50' : ''}}
											{{$req->order_sts==99 ? '#ef5350' : ''}}">
											{{$req->order_sts== '0'? 'Requested' : ''}}
											{{$req->order_sts== '1'? 'Verified' : ''}}
											{{$req->order_sts== '2'? 'Delivery' : ''}}
											{{$req->order_sts== '99'? 'Pending' : ''}}
										</font>
									</td>
									<td class='text-center'><a href='/historyOrderDetail?id={{$req->id}}'><u style="color:#1979c3">View order</u></a>
									@if($req->order_sts==2 && $req->recieved_point != 0)
									| <a ><u style="color:#1979c3">Get point</u></a>
									@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						</div>
					</div>
					<!-- order-requests-->

					</div>
					<!-- order-pending-->
					<!-- order-delivery-->

					<!-- order-pending-->
				</div>
			</div>
			<!-- / Inner Page Content End -->
			<!-- Footer Start -->
			@stop
			@section('script-screen')
			@stop
