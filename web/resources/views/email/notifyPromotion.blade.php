
<table class="mcntwrapper" width="100%" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: rgb(245, 248, 250); margin: 0px; padding: 0px; width: 100%; border-spacing: 0px;">
	<tbody>
		<tr>
			<td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
				<table class="mcntcontent" width="100%" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0px; padding: 0px; width: 100%; border-spacing: 0px;">
					<tbody>
						<tr>

							<td style="padding-top:10px; background:orange;">
								<table width="200"  align="left" border="0" >
									<tbody>
										<tr>
											<td >
												<div >
													<img style="float:left; padding-left:10px;" width="50" height="50" src="http://www.ifoodgo.vn/frontend/img/logo.png" alt="Foodchow.com" border="0" style="display:block;border:none;outline:none;text-decoration:none;height:55px" class="CToWUd">    </a>
													<div width="50" height="50" >
														<br>
														<span style="color:#fff; font-size:20px; padding-left:10px; ">iFoodGo </span>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<table width="280" align="right" border="0" cellpadding="0" cellspacing="0" class="m_-2071666447203080194devicewidth">
									<tbody>
										<tr>
											<td align="center" style="font-family:Helvetica,arial,sans-serif;font-size:15px;color:#fff;text-align:center;padding-right:0px" height="35">Liên hệ: +84 8 3810 3385
											</td>
										</tr>
										<tr>
											<td align="center" style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#ffffff;text-align:center;padding-right:0px" height="30">    <a href="http://www.ifoodgo.vn" style="color:#fff;text-decoration:none" target="_blank" >ifoodgo-support@system-exe.com.vn</a>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<!-- Email Body -->
						<tr>
							<td class="mcntbody" width="100%" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
								<table class="mcntinner-body" align="center" width="570" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px; width: 570px; border-spacing: 0px;">
								<!-- Body content -->
									<tbody>
										<tr>
											<td class="mcntcontent-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
												<h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">Dear An,</h1>
												<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 19px; line-height: 1.5em; margin-top: 0; text-align: center;">Thank you for joining iFoodGo !</p>

												<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: center;">Click on the below button to activate your account.</p>
												<table class="mcntaction" align="center" width="100%" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 30px auto; padding: 0px; text-align: center; width: 100%; border-spacing: 0px;">
													<tbody>
														<tr>
															<td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
																<table width="100%" border="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-spacing: 0px;">
																	<tbody>
																		<tr>
																			<td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
																				<table border="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-spacing: 0px;">
																					<tbody>
																						<tr>
																							<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
																								<a href="" class="mcntbutton mcntbutton-blue" target="_blank" style="    background-color: #519000;
																									color: #ffffff;
																									font-family: Open Sans,Arial,sans-serif;
																									font-size: 20px;
																									font-weight: bold;
																									padding: 8px 15px;
																									text-decoration: none;
																									text-align: center;
																									border-radius: 3px;
																									border: 4px solid #70b518;
																									margin: 0 auto;
																									display: inline-block;">Active Now</a>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>

												<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: left;">Nếu bạn có thắc mắc cần trợ giúp, hãy gọi cho chúng tôi theo số  +84 8 3810 3385. Hoặc gửi thư vào hộp thư ifoodgo-support@system-exe.com.vn.</p>
												<!-- Salutation -->
												<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: left;">Cảm ơn bạn đã lựa chọn iFoodGo, <br> Nhóm hỗ trợ iFoodGo </p>
												<!-- Subcopy -->

											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
								<table class="mcntfooter" align="center" width="570" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0px auto; padding: 0px; text-align: center; width: 570px; border-spacing: 0px;">
									<tbody>
										<tr>
											<td class="mcntcontent-cell" align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
												<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: center;">© 2017 SystemEXE VN Co., Ltd. All rights reserved.</p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
