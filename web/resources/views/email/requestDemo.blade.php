
<table  width="100%" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: rgb(245, 248, 250); margin: 0px; padding: 0px; width: 100%; border-spacing: 0px;">
	<tbody>
		<tr>
			<td align="center" style=" box-sizing: border-box;">
				<table  width="100%" style=" box-sizing: border-box; margin: 0px; padding: 0px; width: 100%; border-spacing: 0px;">
					<tbody>
						<tr>

							<td style="padding-top:10px; background:orange;">
								<table width="200"  align="left" border="0" >
									<tbody>
										<tr>
											<td >
												<div >
													<img style="float:left; padding-left:10px;" width="50" height="50" src="http://www.ifoodgo.vn/frontend/img/logo.png" alt="ifoodgo.vn" border="0" style="display:block;border:none;outline:none;text-decoration:none;height:55px" >   
													</a>
													<div width="50" height="50" >
														<br>
														<span style="color:#fff; font-size:20px; padding-left:10px; ">iFoodGo </span>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<table width="280" align="right" border="0" cellpadding="0" cellspacing="0" >
									<tbody>
										<tr>
											<td align="center" style="font-family:Helvetica,arial,sans-serif;font-size:15px;color:#fff;text-align:center;padding-right:0px" height="35">Liên hệ: +84 8 3810 3385
											</td>
										</tr>
										<tr>
											<td align="center" style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#ffffff;text-align:center;padding-right:0px" height="30">    <a href="http://www.ifoodgo.vn" style="color:#fff;text-decoration:none" target="_blank" >ifoodgo-support@system-exe.com.vn</a>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<!-- Email Body -->
						<tr>
							<td  width="100%" style=" box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
								<table  align="center" width="570" style=" box-sizing: border-box; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px; width: 570px; border-spacing: 0px;">
								<!-- Body content -->
									<tbody>
										<tr>
											<td  style=" box-sizing: border-box; padding: 35px;">
												<h1 style=" box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">Xin chào, {{$name}}!</h1>
												<p style=" box-sizing: border-box; color: #74787E; font-size: 17px; line-height: 1.5em; margin-top: 0; text-align: left;">Chúng tôi đã nhận được yêu cầu demo hệ thống từ bạn !</p>

												<p style=" box-sizing: border-box; color: #74787E; font-size: 17px; line-height: 1.5em; margin-top: 0; text-align: left;">(Mã số yêu cầu: {{$id}}) </p>
												<p style=" box-sizing: border-box; color: #74787E; font-size: 14px; line-height: 1.5em; margin-top: 0; text-align: left;">Bộ phận kỹ thuật của chúng tôi sẽ liên hệ với bạn ngay để thiết lập cuộc hẹn demo và giải thích hệ thống</p>

												<table  align="center" width="100%" style=" box-sizing: border-box; margin: 30px auto; padding: 0px; text-align: center; width: 100%; border-spacing: 0px;">
													<tbody>
														<tr>
															<td>
															<img style="float:left; padding-left:100px;" width="200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAADqCAYAAAAYu3hoAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAlx0lEQVR42u3deZwU1b338U/13j0zDNPDetk3Zd9M0LQgCr6Mer3kalyCK7gg0WskJupzSa5kTGISo3EN8cb7ClfjdiXPExeCyyAgSuOIgF4hbLJvMjD79PRSXVXPH2NPQGbpGXq6qnp+79fLP+il+ldl93dOnTrnFAghhBBCCCGE6KKUdF8YCoW6AXnAbcAFZhcuhMgJtcB8QAuHw+VtvTitwAqFQrcCNwLTzN47IUROqgQeAp4Lh8PHW3pRq4EVCoW8wH3ATwCv2XskhMh57wDXhsPhyuaedLTx5v8DPIiElRAiO74NvBoKhYqae7LFwAqFQn2AG8yuXgjR5cwEJjf3RGstrJuBYWZXLoTokn7Z3IPO5h4MhUIDgZeQU0EhhDl6DRgw4MsDBw5sPPHBllpYTqDA7IqFEF2Wh8ZhVCdpq9NdCCEsQwJLCGEbElhCCNtwdcZGFUVBUdKe9SOEyFG6rmd0exkPrCFDhjBt2jS6deuWtYMihLCeRCLBZ599xieffIKmaRnZZkYDa8iQIdx///2MGTMGh0PONoXoygzDoLq6mkceeYTVq1dnZJsZSxWHw8GMGTMYO3ashJUQAkVRKCoq4qqrriIvL+/0N0iGA6tbt27SdyWEOInf78fn82VkW9IUEkLYhgSWEMI2JLCEELYhgSWEsA0JLCGEbUhgCSFsQwJLCGEbElhCCNuQwBJC2IYElhDCNiSwhBC2IYElhLANCSwhhG1IYAkhbEMCSwhhGxJYQgjbkMASQtiGBJYQwjYksIQQtiGBJYSwDQksIYRtSGAJIWxDAksIYRsSWEII25DAEkLYhgSWEMI2JLCEELYhgSWEsA0JLCGEbUhgCSFsQwJLCGEbLrMLsDrDMDAMA13XT/o3gMPxj7x3OBwoioKiKGaXLETOksBqhqqqJBIJVFUlmUyi6zq6rjcFVSq8UoGlKAoOhwOHw4Hb7cbtduPxeHC55PAKkUld/hdlGEZTQMViMWKxWFMgtUXTtFafdzqd+Hw+fD5fU4BJC0yIjuuygaXrOrFYjIaGBhKJBJqmNbWgMkXTNCKRCA0NDbhcLjweD3l5eXg8npNOJ4UQ6elygaWqKtFolPr6epLJZFY+M9WKU1WVSCTSFFx+v19OG4Vohy7za0m1durr6zulNdUeqf6xSCRCfn4+gUBAWlxCpCHnA8swDKLRKDU1NaiqanY5J9WVSCSorKwkEonQrVs3fD6f9HEJ0YqcDqxkMkl1dXW7OtLNEI/HqaioIC8vj4KCAjlNFKIFOfnLMAyDhoYGamtrLdWqao2u69TV1RGPx+nevTs+n8/skoSwnJzrONF1ndraWqqqqmwTVidKJBJUVFRQW1traj+bEFaUUy0sTdOoqamhvr7e7FIysh+GYVBQUCAd8kJ8JWd+CclkksrKStuHVYphGE0tRSv3vwmRTTkRWJqmUV1dTTQaNbuUjDIMg0gkQk1NjYSWEORAYGmaRkVFBQ0NDWaX0mnq6uqoqqqSPi3R5dk6sFId7LFYzOxSOl0kEqGurk5CS3Rptg6s2tranOmzSkcuXFAQ4nTYNrCi0WiXa3GkOuITiYTZpQhhClsGlqqqVFdXd1pYKfEqHLV7wbBeR7emaVRVVWVt4rYQVmLLwOrMeYGKWo/vw/vwr7gF14GVYGinv9EMi8fjcmoouiTbBVY0Gu3cK4JqBOex/8VR/QW+tffj3v2GJVta9fX1xONxs8sQIqtsFVjJZJLa2tpO/QzD34PEyOswXH6U6HG8H5Xg2r8CdGudgqXmHsr4LNGV2CqwUquDdirFiTr2NhITfwBOD0q8Cl94Ie7db5q9+6eIRqNdYkiHECm2CSxN07J2VdBw+UmMuYX4hLsw3Hko0WN4P3oA1753QLfOhGrDMKSVJboU2wRWQ0NDmzd9yCinF3XsrSQm3AWKEyVRi2/dT3HvWW72oTiJqqrSyhJdhi1Wa9A0zZSpN4YrQGLsrWBoeP73DygN5XjX/RTD4SQ56NvgcJt9aNB1nUgkgs/nk1UdRM6zxTc8Ho+bN1jS4UYdfXNjnxY0trTKSnDvfdvsw9IkFovJuCzRJdiihdXQ0GDqiHbD/VVLS9fwfL64saUVXgiGhjrkn01vaaVWWPV4PKbWIURns3wLS9d1a4w3Upyoo+eQGH9H4z8TtXg//gWu/aVmVwY0tkKl813kOsu3sKLRqGV+iIY7QGLcfFBceD57EiV6DN8H9xJXG1CHfcfUlpaqqsTjcfx+f0a2t3r1at577z0A8vPzOeecc5g6dSpOpzOr+6XrOps2baKoqIihQ4dm9bOF9Vi6hWUYBvF43FoTnBUH6qjrSYy/ExwuFLUe7ye/wrXvXVNHxOu6ntHpSh988AHLli0jPz+f3bt3s2DBAlatWtX0fDKZJJFInPTHJHXrMlVV0XUdwzAwDOOk16QeT70+dYPZE/8fp7ataRqqqrJkyRJWrVpl+v0khfks3cLK9I8wUxqvHt4GihPPp0+gRI/jW/vvxJMNqMMuB4c5hzUV7pm6t+GoUaO4//77AZg7dy7Lli3jggsuYOPGjbz88svU1dVxzjnncN1116HrOn/7299YsWIFgUCAwsJCrrrqKurr6zl06BBXXHEFqqry17/+lTPPPJOxY8fyxhtvsHLlSpLJJJdddhmXXHIJn376Kc899xzxeJyioiIuvPBC1q5dyxdffMHWrVv58Y9/TJ8+fUw5vsJ8lg8sy179crhQR90Aho530+9QEjV41z8ETh/q4EtMCa1USyVTgaVpGolEgkgkQnV1NWPGjOHo0aP85je/4Xvf+x6DBw/m17/+NUVFRXTr1o3nnnuOhQsX4na7uf3225k8eTLl5eVs2rSJK664Ak3TKC1t7POrrq5m6dKlzJs3j/r6en73u9/Rq1cvFi1axLx58xg0aBDr16/H7/czZMgQRowYwSWXXEJBQUHWj6uwDksHVjKZzO5g0XZqHBF/MziceDY9hhKrxLvuP0CLoQ6/ApTs9vekjlemxmNt2rSJBQsWcPDgQQoLC5kzZw4rVqwgEomgqiq7d++me/fufPjhh1RWVnL11VcTCoUAWu1vUlWV119/nR49elBeXo6u62iaxsqVK6moqCASiVBQUMDNN9+Mruv069ePkSNHcv7552f1eArrsXQflhVPB0/h9JAYeQOJiXdjuAIo8Sq8H/8C956/mTKNJ5PHrLi4mHHjxlFWVsaFF15IcXExhw4dIhqNsnfvXnbt2sXAgQO54IILqKqqori4OK3tJhIJjh07RnV1Nbt27WLPnj1Mnz6dGTNmUFJSwgsvvMC8efO46aab2Lt3b9aPobAuS7ewbBFYAC4fidFzQHHg2fgoSrwa70eLIBlDHXElKNn7u5DJYzZkyBBuv/12VFXlt7/9LePHj2fgwIEMHjyYu+++m0Ag0NSJvnTpUqqqqprdTmrQb6qT3ev10rt3bwYOHMgPfvCDptfFYjHcbjcXX3wxhw4d4rbbbjupo18ISweWlU8HT+H0khh1IxhGY0d8rBLvxw82zkkcfAk4szOoM9PHzOFwcNttt3HkyBF+9KMfsXDhQo4ePcrTTz/NzJkzKS0tZdSoUcyaNYslS5bQr1+/pn6rG2+8kREjRvDqq6/y/vvvs2XLFj744AMuvfRSZs2axUMPPURxcTH9+/dn+fLlzJgxgw8++IBp06bh9/vxer0MHTqUmpoaPvzwQ4YPH86kSZPIz8/PyrEU1tNsJ8uAAQOKgLvbsyGHw8HZZ5/N6NGjM1ZcbW2tZcZgpXcQXGjFY8Cdh+vLMpRkA86jH2P4e6AXj8lOCQ4HeXl5p70dr9fL8OHDOeOMM3C73Zx99tmoqsrQoUO5/PLL2bp1K6tWrSIYDHL++eczceJEfD4fpaWl5OfnU1dXx6RJk5g+fToFBQW8/fbb9O7dm8svv5xx48Zx1llnMWjQID766CM+++wzhgwZwowZMwAoLS1l+/btzJ49m/PPP5+hQ4dy6NAhNmzYwJQpUySwbKaiooLS0tKO3Df07QMHDpSd+ECzl5NCodAQYHd7tuxyubjrrru48sorM7ajhw8ftu5VwtboKp6//zeeT59ESdRiuPzEz/016uBLO72l5fP56NWrV+fv4ldXcF0uV1Mnf+qUz+VycfXVV3PjjTcya9aspsfdbvcpVzCTySS6rp/0XOq01u3+x0BcTdPQdR2Xy5Wxq6AiO7Zv3869995LRUVFe9+6IBwOP3HiA5Y+JbQthxt15PWgOPCufwglGcVb9nPQk419WjnA4XCcMndRUZSmx2bNmsWwYcNOefzrXK5Tv4InBlWK0+nM+ih7YT2WDSy7j2g2XP7GjnhdxbPpcZTYcbzrfgoOJ+qQfzFtcGm2XH/99dISEhmX278asylOcPrhqx+uosVR6vYD9g7jdMjaXKIzWDawbP/XWYvh3rG08UqhnsRwB1BHzSExsV3XMoQQJ7BsYNmaoePe+X/xbny08W47qaVpxt1hdmXtkprE3BJFUZr+czqdaf2Rac/sBYfDcVJ/Vqoej8fToT9oqbmpX99uSwzDaKr3xOPgcDhwuVxp77PIHEsHlsvlst9VQi2OZ8f/4P34F6DFG1tWI28gftb9Wfn4THZMP/3005SVlTX7nMPhIBgMEggEKCoqYtSoUYwdO5Zhw4a1+iNetmwZr7zySlqfP2PGDObNmwdAJBLhscceY+/evdx2221MmTKlXWGh6zqvvfYar776Kueffz7z589v9bUHDx5k/fr1bNiwgc2bN1NZWYlhGE37PX78eCZPnsxZZ51F//795RQ4SywdWLb7EuhJ3DtexbPxEdDi/1heefz3s1ZCJgOroqKCw4cPc+GFF54y6dgwDI4fP05lZSVbtmxhxYoVFBcXM3PmTGbNmkXfvn2b3WZtbS1Hjhxh8uTJDBw4sNXPHzJkyD8Ora5z7Ngxtm3bxuLFi+nTpw+DBg1Kaz8Mw+Cjjz7i+eef59ixY1RWVrb42oaGBl5//XXeeustDh48iNfrpW/fvpx77rl4PB7i8Tjbt29nw4YNrF27lv79+3PZZZcxa9YsfD5fxo69aJ6lAyudZrtlaHE821/B88mvUZINGK4A6uibiE++J6uToDN9zJxOJ3PmzGk1XCoqKnjnnXd45513+POf/8z777/Pgw8+yPDhw1us8eKLL+bb3/52h+o5cuQITzzxBD/5yU/Smr+4fft2nnjiiTZvwltRUcEzzzzD8uXL6dmzJzfccANTp05lxIgRp7x23759rFixgmXLlvHEE0+wa9cu5s+fT1FRUUaPvziZpZswzY3RsSQtgWfH/+DZ+FuUZMMptwjLJjOOWXFxMddccw0PP/wwF154IUeOHOFXv/oVW7duzfhn9enTh6uvvprNmzfz7LPPtnk3paNHj/LMM89QU1PD9773vRZfF4vFeOSRRygtLWXixImUlJQ0TS1qzqBBg5gzZw4/+9nPmDBhAu+88w5PPvmk3HKtk1k6sJobGW05uopn+0t4PvkNSqLulJuwZlO6ncmdwel00rt3b+69915mzZrFtm3beOaZZzLeB+nxeLj22ms555xzWL58OcuWLWvxwkA8HuePf/wjmzdvZvbs2UybNq3Z1yWTSZYuXcqaNWsYOnQoDzzwABMmTGjz9NrpdDJhwgQeeOABBg0axHvvvcfrr79urzmwNmPpwEpdjbEsLY5n20t4NjyMotY3htW4eU23uc82K5xC5+XlMW/ePIYNG8Znn33G2rVrMz4IOBAIMG/ePCZNmsTzzz/Pu+++e8pnJBIJXnzxRVatWsXMmTO55ppr8Hq9zW5v586dvPnmm/Ts2ZM777yT3r17t6uePn36cOedd1JUVMRrr70mS+J0IssHlhV+hM3Sk3i2v4xn4yMoagRcPtQxc0mMuwPDlZkbQbRXRy/3Z1peXh5XX301iqJ0dNJrm/r378+dd96Jz+fjD3/4Azt37mx6zjAM1qxZw1/+8hdGjx7N3LlzW+0QX7duHYcOHeJb3/oWY8eO7VA9kydPZvr06ezfv7/FK6vi9Fk+sKzyIzxJMoZn2wt41//qq8nNAeLjvk984gJwmXOlKDVfzyrHauLEiQwYMICdO3dy9OjRTvmMESNGcPfdd6PrOo888gh79uzBMAw2b97Mo48+Svfu3bnvvvvabDGtWbMGgMsuu6zFVlhbXC5X00WE1PZE5lk6sKBx9QGr/AgBMHQ8O17Gs/F3oMUaO9jHzEUdNw+cHfuyZ4LT6bTUjVR79+7dtARyTU1Np3yGoiicc845zJkzhx07drB48WL27dvH4sWLcTqdzJ8/n4EDB7b6/amtreXAgQN069aNkSNHnlY9w4cPJxgMsn//furq6jr1+HZVFu4gauTxeHC73Za4maqSjOLe8UrjoFA9ieHOIzHu9qZbfpnJ6/Va6vTZ7XbTt29fysrKTumETiQSvPHGG3zyySctvt/pdPLd7363xat0J37O5Zdfzt69e3nzzTebVkj94Q9/2LS+fGtqamrQdZ1gMHjaY9gURaFnz54cPnyY2tpauWFGJ7B8YEFjJ6sVAsu94xU8Gx9rnG7jcKOOnoM65lbTwyp1jKympdOr1Ejy6urqFt/rdDq56KKL0voch8PBnDlz2LdvHxs2bOA73/kOl156aVoBlMkLAoqiNO2z3VcbsSrzf2lp8Pl8uN1u09Z4V5JR3NtfwVv2MwAMd/5XVwOtMZHZ4/F0uO+ls2iaxv79+3G73acEh8/n44477ujQwNGWFBcX8+CDD/LKK68we/bstFtLeXl5KIqSkfFTuq7z5ZdfoiiKJf+A5ALL92FBY4emaT9IQ8O9/SU8mx5r/LfDQ2LsrY0tKwtI/TisNo2poqKC6upqevbsSbdu3bLymd27d2fu3LkUFham/Z7i4mIKCwspLy/vyIqYJzl+/DjHjx+nV69eBIPBrOxzV2Otb3kLFEUhPz8/653vSjKK5+//3bhqaKIGw51PfMIdJCb9MOuDQlvidDrx+80ZRtGabdu2sX//fgYPHkzPnj2z9rkd+cN23nnnoes6y5cvP61TuVWrVqHrOt/61reytr9djS0CCxpPe7L6w9RV3NtewLPp8aY+q8T4+ahj55l9KE4SCAQs1dkOjadGy5YtIxqNctZZZ1n+phHTpk2je/furF69moMHD3ZoG/v27WPlypUEg0HOPfdcs3cpZ9kmsAAKCgqyM/I9GcPz9+fwbvht4zgrdz6J8fNJmDDdpjVut9tyYaDrOqtXr2bt2rX069ePiy++2FrDUpoxduxYpk2bxrZt23jxxRfbPbUmmUzy3HPP8cUXXzBjxgzOOOMMs3cpZ9kqsDweT0ZuYdUW9xd/aeyz0uLg9JGYcAeJ8dZafE9RlOwFeJqSySTvv/8+v//97yksLOSWW26he/fuZpfVJp/Px9y5c+nXrx+lpaU8//zzba7skFJdXc2SJUtYuXIlw4cP5/rrr7fcBZBcYp1vexpSP9JIJNKpC/u5t7/UODfQnYc65hYS4+ZnfdWFtni9Xktdidq1axelpaW8+uqraJrGPffcw/Tp080uK229e/fm/vvv5/HHH+dPf/oTO3bs4KabbmLEiBHNXnFUVZUtW7bw5z//mY8//piRI0dy1113ZbW/riuyVWBB45ib7t27U1lZ2Wk3WU2OuAaUpajDv4t6xmzLhZXL5aKwsDArVwYNw2DPnj2nzAfUNI3y8nLKy8v59NNP2bJlCw0NDYwYMYLZs2czderUFlt/uq5z5MgRtm/f3upn5+Xl0bdv36zd3mvSpEk89NBDPPXUU2zYsIHPP/+c0aNHM2XKFM4880w8Hg/19fXs2LGDtWvXsnv3bnRdZ+rUqSxYsEDCKgtsF1gAfr+f/Pz8tJvt7ZUYfROJ0TeZvZvNSrUyszUNR1VVFi5c2OLzLpeLPn36MGHCBL75zW8yderUNhexi8fjPPvsszz77LOtvi4UCvHAAw9kbcS4w+FgwIABLFq0iHXr1lFaWtq04sTXFRYWMnnyZC666CKmTJkiq41miaXv/NwaXdc5fvx4l1owLTXmKhgMZqUju66urs0ZBoqi4HK58Pl8aU2+jkajRCKRtD7f4/FQUFCAoigYhkFtbS2GYXS4X0zTNKqqqvD5fG1erDAMg1gsRiQSYe/evRw6dIhkMonb7aZfv34MHjyYvLw8Cao0yJ2f+cepYUVFhWkj4LPN4/FQWFiYtatuBQUFGW/d+P3+Dg1PURSlXQNCm+N0OunRo0fan5eqtUePHnzjG9/I6HEQHWOrq4Rf5/F4KC4u7hK3MHe73fTo0cNSVwWFyDZbBxY0hlZRUVFOh5bL5cr5fRQiHTnx5zo1l66ioiLn1tN2uVz07NnTcqPZhTCD7VtYKT6fj2AwmFOnTKlTXgkrIRrlzq+bxg5dl8tFZWWlJdbP6qjUukpdpX9OiHTlTAsrJdU5nVrnyG5S46wkrIQ4VU61sFKcTifBYBCfz0dNTU2nTuPJJLfbTWFhoaWm3IjMMmLVGPEaHIWDzC7FlnKuhZWiKAp5eXn06NHDkgvcncjhcJCfn99Uq8hdRsNxou/eh15zwOxSbMm6v+IMSXVcFxUVWW4WvaIo+Hw+iouLCQaD0rneRai73yP67o8x1AazS7GdnDwl/LpUa8vv99PQ0EAkEiGRSJh2o4DU/Rbz8/Px+XyWbv2JTmDoJLb+Pxw9RuGbeh+KW1rV6eoSgZWSOvUKBAJEo9Gm/7IVXA6Hg0AggM/nw+/32/KigMgQPUm87HEc+b3wftNaa61ZWZcKrBSHw0FeXh6BQABN05qCK5lMomlaxgIsNTHY5XLh9/vx+Xw4nU4JKgGAEash+u69OILDcQ+ZYYnbxVldlz5CqUApKCggPz+fZDKJqqokEgkSiQTJZLLdVxjdbjcul6vpBrCpf0tIieYYagPRd+7BcfnzOPtONrscy+vSgXUiRVGaAiYQCDS1sgzDaGp5tbRgoMPhwOVyndR6koAS6dLK/07kr3PIv+5vOAoHmF2OpUlgteDE4MnWYnmiqzLQjm0muuoBAhc/huLrbnZBliWXp4SwAsMg8el/E31vIYYaPf3t5SgJLCEsJPHZ8yQ+f7HxXpjiFBJYQliIkYgQffc+EluWml2KJUlgCWExRqyK6OpFaF9uApMGN1uVBJYQFqRX7KThzflolTvMLsVSJLCEsKjk4U+IvnsfRsNxs0uxDAksIazK0FF3Lie68qcyUforElhCWJmeJP7pc8TXLwbNvqvoZooElhBWl4wR+/DXJLb+FQz99LdnYxJYQtiA0VBB9O0fkjywzuxSTCWBJYRN6PVf0vDOPWgVO80uxTQSWELYiHZ4PdFl8zEi5WaXYgoJLCHsxDBQ96wktvZhDDVidjVZJ4ElhA3Fyp4i9v4vutycQwksIexISxAve4LE9je71JVDCSwhbMpQo0SX34W6/U2zS8kaCSwhbEyvO0T0vX9Hr9ptdilZIYElhM1px7YSeeNW9Op9ZpfS6SSwhMgByX0fEF29KOevHEpgCZEL9CSJz18m/uHDOT1RWgJLiFyhJYh99ASJz1/K2SuHElhC5BAjXkP0vZ+g7lmVk6ElgSVEjjEix4i+vYDkkQ1ml5JxElhC5BwDrXwz0XfvQ689aHYxGSWBJUSOSu57n+jbC3JqiWUJLCFylWGQ2PoasbInQVfNriYjJLCEyGWGRnzd74itezwnJkpLYAmR44xEhNiHvyG5ZxVg7/scSmAJ0QUY0Qoiy+aTPFiGnUNLAkuILkKv3kP0rbvR646YXUqHSWAJ0VUYBslDHxN9+x50my6xnNHASibt36knRK5LbHuN2Jqfg5bIyudpmoamaRnZVsYCS9M0Nm3aRE1NTVYOghCig7Q48bKniW94FpKdf3PW9evXU11dnZFtuTJVlGEYlJWV8dRTT3HVVVfh9/s7/UAIYTeO2sMEdB3F7EKA2Jqfo7nyOd7jgk7ZvqZplJWV8corr2RsmxkLLABVVXnrrbdYs2YNeXl5nXIQhLCzXt4ID4xoIOA0uxLQ649S9eYP+P3uKeyozc/49lVVpaqqKqPbzGhgpUQiESKR3F5ITIiOcAZi6MMMsEBgAQSMWv41WMZTXw5jX33A7HLaJFcJhejiRhbWMWfEfvJd1r9oJoElhGBycRU3jthPwJWZq3mdRQJLCIFTMZjZt5xL+39pdimtksASQgDgdepcM+Qg0/scx6lYc/qOBJYQoonfpXHj8P2MLKwzu5RmSWAJIU7S2x/j30bvYlC+9e6+I4ElhDhF/0CUW87YS5HXWgv/SWAJIU6hKDC5uJobhu0nz0LDHSSwhBAtmt7nGN8ZdAS3wxq3DJPAEkK0yOvUuXLQIab1rjC7FEACSwjRBo9T5/rh+/lGjyrTJ21LYAkh2tTLF+fWM/bSNxAztQ4JLCFEWvrnRfn+yN309psXWhJYQoi0jQ/WMHfEPlwmdcJ3yvIyQghr+KyykCU7B+NQDPoFonT3qPTwJejhjRNwafhdGnmuJG6HQZ5LI+BKtjotx6kYnNu7gkMNfpbu6UdMy+46ORJYQuSwEd3qUXWFffV57Kg5eZE+t0Mnz6VR4E7icerku5IUuJMEXBo9fXF6+OIUeVR6++MUeRLkuxvHYynAZQO+pCLmYfnBPlndHwksIXJYwKUx85+O8acdg055TtUdVCccVCfcaW3L5dDp4483tdLyXElcDp2knr2eJQksIWxINxoHGDjSWFVhfFENhR6VmjSDqSVJ3cHBiJ+DEfPu19BSNNYB602rSgjRopjm5IVdA9ie5jrs/fKiTAza7m5W5cDmrz/YbGCFw+HjwAuAdSYRCSGoV128tKs/f933T2ytLkjrPX6nxuTiKstMr0nTxnA4/N7XH2zx5DMcDj8JPGp21UKIRlVxD3/aOYjX9v8Tqu7g/SM9037vOb0qKfZm58apGfAhcF1zT7TVW1YCPAxYa40JIbqY8piX/9w+hPcO92rqv9pbH+DLqC+t9+e5NM7uVWn2bqTjQ+CqcDjcbLGtDqI4cOBA8sCBAysGDBjQDQiZvSdC2F2BO8nF/Y7icaZ/elYV9/DMtqGEy4sxTpjNZ6AQ9CYY3T291UH9Tp2PjgVJZPGqXjt9AFwZDoePtvSCdCv/D+BMYBWwAdhl9p4JYUdeh47SjvXSa1U3i7cN5ePjRc0+//fqbkTTHLzZP6+BsUW1pk9gPsFxGvNkA/DPwBXhcLi8tTekNawhHA7HgB3ADIBQKDQM+Fez91akzQM8ZHYRWfAisMnsIlpz2YAj3/E59Wnpvr5edT6yrjzY4q1shhZEJrkU47p0ttXNneTaoQf4vLLbwvqkywodWp+Hw+F32/MGC4Wt6CyhUCgPqDe7jiy4NhwOv2x2Ea2pXMTjwN1pvDQG/DvwdLCk5av1lYvwA68BF7WjjCnBEnsOW7LsyazIqFvNLiBLbrnhhhvMriFTfh4s4fHWwgogWEKUxnBrj5+avXMdJYHVNcwwu4AsmXneeeeZXcPpigN3Ar9px3tubudnXFq5iCvN3tGOkKk5QljLL4MlLG7ne9rbwnIBpzdPxyTSwspxoVBoNjDL7DqyZcmSJS+ZXUMHJYF5RTfy8/a+8ZqXiAC3tfNtLxnWvLlzqySwcl96IwtzhzsUCtntzCEJlARLeFYZ1v43l+4EGgd3a+15X9XP7PfdkMDKfX8yu4AsuxK41Owi2un7wRJ+cTobCJbwHLC6nW+z3XdDAksI88SAhduO8V8Z2l51O18/s3KRvWaw2K3pLNohFAr9s9k1mORS4A2zi0jDgmAJ/5nB7d0BfLcdr+8F9DD7ILSHtLByW1ojoHPQ9WYX0IY3gG8Az2Z4uxqN60i1xy/NPhjtIYElRHa9DVwbLGFDsISMLlAVLKGC9k/BOqNykX2uIktg5ahQKDQDuMzsOkziD4VCp9WJ3VmCJbwdLCHSiR9RDu3avgf4F3OPSvoksHJXPpDekpS5xwH0NrsIMwRLeJlmlhZuw9mVixhhdu3pkMDKXb83uwCTXRsKhaaaXYRJ/t7O148DBppddDoksHJX0elvwtYCgNfsIkzS7tHywC1mF50OCawcFAqFJtDGarJdxCSzCzDJPuDBdr7nkspFjDG78LZIYOWmf6PrTclpzr1mF2CGr64+xtv5tu60fz5i1klg5ZhQKNQb6MCMtJzkC4VCk80uwgxFP+Mh2t/5fmblIorNrr01Eli5ZyxwgdlFWEQ34EazizCD0riWcLSdb7sYGG527a2RwBIid7VnYb8o8EfaP1I+qySwco9tl7/tJN8NhUKjzS7CJEnaXsu/EngJuDBYwu3BEvaYXXRrJLByT1f9cbakP110AG2whG3Ak6285FHgvGAJ1wVLCJtdbzoksHJIKBT6ERA0uw4LstUE3ww7zsl3bj8KvAx8I7yPHwdL2GJ2ge0hy8vklgHI/9PmDDW7ALMES3ischF3AduBj4A3gyVsNLuujpIvd4746t6Dvcyuw6I8oVCoXzgcPmR2ISaZCVQHS6gyu5DTJYGVO8YCs80uwqL60bi43U/MLsQMVu9Ibw/pwxJC2IYEVu7oKnd37qhLQqFQP7OLEKdHAit3zDS7AIubhKxgYXsSWDkgFApdDfQ0uw4b+DezCxCnRwIrN0ygcYVR0bpzzS5AnB4JLJsLhUIOGtflFm1zhEIhOVY2JsMa7K8vjasS/NHsQmxiJvCW2UWIjlHMLqArCYVCCtDH7DpExlWHw+H2LuUiOkBaWNkVAA6bXYTIuGtpnJ8nOpn0YWWXTuPkU5E76oFas4voKiSwsuir04ZLIXemSgjuDIfDfzO7iK5CAivLwuHwRuA9s+sQGbEPWGp2EV2JBJY57gZeN7sIcVp2Av8ine3ZJVcJTfLVcjDdzK5DdFgiHA5XmF2EEEIIIYQQQgjRRfx/7Ud/V3JhEbMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTUtMTAtMDhUMTc6Mjg6NDMrMDA6MDA54QZ6AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE1LTEwLTA4VDE3OjI4OjQzKzAwOjAwSLy+xgAAAABJRU5ErkJggg==" alt="ifoodgo.vn" border="0" style="display:block;border:none;outline:none;text-decoration:none;" >    </a>
															</td>
														</tr>
													</tbody>
												</table>

											<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: left;">Nếu bạn có thắc mắc cần trợ giúp, hãy gọi cho chúng tôi theo số  +84 8 3810 3385. Hoặc gửi thư vào hộp thư ifoodgo-support@system-exe.com.vn.</p>
												<!-- Salutation -->
												<p style=" box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 1.5em; margin-top: 0; text-align: left;">Cảm ơn bạn đã lựa chọn iFoodGo, <br> Nhóm hỗ trợ iFoodGo </p>
												<!-- Subcopy -->

											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style=" box-sizing: border-box;">
								<table  align="center" width="570" style=" box-sizing: border-box; margin: 0px auto; padding: 0px; text-align: center; width: 570px; border-spacing: 0px;">
									<tbody>
										<tr>
											<td  align="center" style=" box-sizing: border-box; padding: 35px;">
												<p style=" box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: center;">© 2017 SystemEXE VN Co., Ltd. All rights reserved.</p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
