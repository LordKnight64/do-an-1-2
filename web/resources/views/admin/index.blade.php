<!DOCTYPE html>
<html>
<head>
  <script>document.write('<base href="' + document.location + '" />');</script>
  <title>iFoodGo Admin Portal</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{'/admin/css/admin-common.css'}}"></link>
  <link rel="stylesheet" type="text/css" href="{{'/admin/css/primeng/resources/primeng.min.css'}}"></link>
  <link rel="stylesheet" type="text/css" href="{{'/admin/css/primeng/resources/themes/omega/theme.css'}}"></link>

  <!-- 1. Load libraries -->
  <!-- Polyfill(s) for older browsers -->
  <script src="{{'/admin/libs/bootstrap/dist/js/bootstrap.min.js'}}"></script>
  <script src="{{'/admin/libs/core-js/client/shim.min.js'}}"></script>
  <script src="{{'/admin/libs/zone.js/dist/zone.js'}}"></script>
  <script src="{{'/admin/libs/reflect-metadata/Reflect.js'}}"></script>
  <script src="{{'/admin/libs/systemjs/dist/system.src.js'}}"></script>
  <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyIJqaHVEe5ucEJyDxBbtJe33BZRPEJ-A">
  </script>

  <!-- 2. Configure SystemJS -->
  <script src="{{'/admin/systemjs.config.js'}}"></script>
  <script src="{{'/scripts/pushNofitication.js'}}"></script>
  <script src="{{'/sw.js'}}"></script>
  <script>
      System.import('app').catch(function(err){ console.error(err); });
    </script>
 <style >
     .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width:80px;
        height: 80px; 
        margin-left:50%; 
        margin-top:300px; 
        position:absolute; 
        z-index:1000;   
        animation: spin 2s linear infinite;
    }

    body {
        width:100%;
        height: 100%;
      /*  background-image:url("frontend/img/banner.png");*/
        background-size: cover;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
</head>

<body>
    <div class="app app-header-fixed app-aside-folded">
        <my-app >
            <div class="loader"></div>    
        </my-app>  
    </div>
    
</body>
</html>