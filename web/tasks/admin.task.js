module.exports = function(gulp, $, MODE_PRODUCT) {
    'use strict';

    const del = require('del'),
        tsc = require('gulp-typescript'),
        sourcemaps = require('gulp-sourcemaps'),
        tsProject = tsc.createProject('tsconfig.json'),
        tslint = require('gulp-tslint'),
        concat = require('gulp-concat'),
        runSequence = require('run-sequence'),
        nodemon = require('gulp-nodemon'),
        gulpTypings = require('gulp-typings'),
        elixir = require('laravel-elixir'),
        less = require('gulp-less'),
        gulpif = require('gulp-if'),
        uglify = require('gulp-uglify'),
        cssmin = require('gulp-cssmin'),
        htmlmin = require('gulp-htmlmin'),
        suffix = 'admin';

    var paths = {
        'destPath': 'public/admin',
        'sourcePath': 'resources/assets/admin',
    };

    var tasks = {
        'clean': 'clean' + ':' + suffix,
        'buildTypeScript': 'buildTypeScript' + ':' + suffix,
        'build': 'build' + ':' + suffix,
        'tslint': 'tslint' + ':' + suffix,
        'compile': 'compile' + ':' + suffix,
        'clientResources': 'clientResources' + ':' + suffix,
        'libs': 'libs' + ':' + suffix,
        'css': 'css' + ':' + suffix,
        'watch': 'watch' + ':' + suffix,
        'add': 'add' + ':' + suffix,
        'default': suffix,
        'installTypings': 'installTypings' + ':' + suffix,
        'fonts': 'fonts' + ':' + suffix,
        // 'less' : 'less' + ':' + suffix
    };

    /**
     * Remove build directory.
     */
    gulp.task(tasks.clean, (cb) => {
        return del([
            paths.destPath + '/**/*',
            '!' + paths.destPath + '/images/**',
        ], cb);
    });

    /**
     * Build Admin typescript
     */
    gulp.task(tasks.buildTypeScript, function() {
        var tsProject = tsc.createProject(paths.sourcePath + '/tsconfig.json');
        var tsResult = gulp.src(paths.sourcePath + '/**/*.ts')
            .pipe(sourcemaps.init())
            .pipe(tsProject());
        return tsResult.js
            .pipe(sourcemaps.write())
            .pipe(gulpif(MODE_PRODUCT, uglify()))
            .pipe(gulp.dest(paths.destPath));
    });

    /**
     * Lint all custom TypeScript files.
     */
    gulp.task(tasks.tslint, () => {
        return gulp.src(paths.sourcePath + '/app/**/*.ts')
            .pipe(tslint({
                formatter: 'prose'
            }))
            .pipe(tslint.report());
    });


    /**
     * Compile TypeScript sources and create sourcemaps in build directory.
     */
    gulp.task(tasks.compile, [tasks.tslint], () => {
        let tsResult = gulp.src(paths.sourcePath + '/**/*.ts')
            .pipe(sourcemaps.init())
            .pipe(tsProject());
        return tsResult.js
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(paths.destPath));
    });

    /**
     * Copy all resources that are not TypeScript files into build directory. e.g. index.html, css, images
     */
    gulp.task(tasks.clientResources, () => {

        gulp.src([
                paths.sourcePath + '/**/*',
                '!**/*.css',
                '!**/*.ts',
                '!' + paths.sourcePath + '/typings',
                '!' + paths.sourcePath + '/typings/**',
                '!' + paths.sourcePath + '/*.json',
                '!' + paths.sourcePath + '/fonts/**',
            ])
            .pipe(gulp.dest(paths.destPath));

        gulp.src([
                paths.sourcePath + '/**/*.css',
                '!' + paths.sourcePath + '/css/**',
                '!' + paths.sourcePath + '/typings',
                '!' + paths.sourcePath + '/typings/**',
                '!' + paths.sourcePath + '/*.json',
                '!' + paths.sourcePath + '/fonts/**',
            ])
            .pipe(gulpif(MODE_PRODUCT, cssmin()))
            .pipe(gulp.dest(paths.destPath));

    });

    /**
     * Copy all required libraries into build directory.
     */
    gulp.task(tasks.libs, () => {
        return gulp.src([
                // 'iFoodGo/**',
                'core-js/client/**',
                'zone.js/dist/zone.js',
                'reflect-metadata/Reflect.js',
                'reflect-metadata/Reflect.js.map',
                'systemjs/dist/system.src.js',
                '@angular/**/*.js', '@angular/**/*.map', '@angular/**/*.json',
                'rxjs/**/*.js', 'rxjs/**/*.map', 'rxjs/**/*.json',
                // 'moment/*.js', 'moment/*.ts', 'moment/*.json', 'moment/**/*.js',
                'moment/min/moment.min.js',
                // 'angular2-moment/*.js', 'angular2-moment/*.map', 'angular2-moment/*.json',
                // // 'ng2-bootstrap/**/*.js', 'ng2-bootstrap/**/*.map', 'ng2-bootstrap/**/*.json',
                // 'ng2-bootstrap/bundles/ng2-bootstrap.umd.min.js',
                'ngx-bootstrap/bundles/ngx-bootstrap.umd.min.js',

                // 'ng2-translate/**/*.js', 'ng2-translate/**/*.map', 'ng2-translate/**/*.json',
                'ng2-translate/bundles/ng2-translate.umd.js',
                // 'angular2-uuid/**/*.js', 'angular2-uuid/**/*.map', 'angular2-uuid/**/*.json',
                // 'angular2-uuid/**/*.js', 'angular2-uuid/**/*.map', 'angular2-uuid/**/*.json',
                // // 'ui-router-ng2/**',
                'angular2-jwt/angular2-jwt.js',
                // // // 'auth0-js/**',
                'object-assign/index.js',
                'sweetalert2/dist/sweetalert2.min.js',
                'ng2-select/bundles/ng2-select.umd.min.js',
                'angular2-toaster/*.js', 'angular2-toaster/lib/*.js',
                'bootstrap/dist/js/bootstrap.min.js',
                'crypto-js/*.js',
                'chart.js/dist/Chart.bundle.min.js',
                'ng2-charts/bundles/ng2-charts.umd.min.js',
                // 'ng2-charts/**/*.js',
                'primeng/**/*.js',
            ], {
                cwd: 'node_modules/**'
            }) /* Glob required here. */
            .pipe(gulp.dest(paths.destPath + '/libs'));
    });

    /**
     * Copy all required libraries into build directory.
     */
    gulp.task(tasks.css, () => {
        gulp.src([
                'primeng/resources/themes/omega/**',
                'primeng/resources/primeng.min.css',
                'primeng/resources/themes/omega/theme.css'
            ], {
                cwd: 'node_modules/**'
            }) /* Glob required here. */
            .pipe(gulp.dest(paths.destPath + '/css'));

        return gulp.src([
                'node_modules/bootstrap/dist/css/bootstrap.min.css',
                'node_modules/font-awesome/css/font-awesome.min.css',
                'node_modules/ionicons/dist/css/ionicons.min.css',
                'node_modules/sweetalert2/dist/sweetalert2.min.css',
                paths.sourcePath + '/css/font.css',
                paths.sourcePath + '/css/animate.css',
                paths.sourcePath + '/css/material-design-icons.css',
                paths.sourcePath + '/css/md.css',
                paths.sourcePath + '/css/simple-line-icons.css',
                'node_modules/angular2-toaster/src/toaster.css',
                paths.sourcePath + '/css/styles.css',
            ]) /* Glob required here. */
            .pipe(gulpif(MODE_PRODUCT, cssmin()))
            .pipe(concat('admin-common.css'))
            .pipe(gulp.dest(paths.destPath + '/css'));


    });

    /**
     * Copy all required libraries into build directory.
     */
    gulp.task(tasks.fonts, () => {
        return gulp.src([
                // 'admin-lte/**/**/**',
                'node_modules/ionicons/dist/fonts/**',
                paths.sourcePath + '/fonts/**',
                'node_modules/font-awesome/fonts/**',
                'node_modules/bootstrap/dist/fonts/**',
            ]) /* Glob required here. */
            .pipe(gulp.dest(paths.destPath + '/fonts'));
    });

    /**
     * Install typings for server and client.
     */
    gulp.task(tasks.installTypings, function() {
        var stream = gulp.src(['./' + paths.sourcePath + '/typings.json'])
            .pipe(gulpTypings(null)); //will install all typingsfiles in pipeline.
        return stream; // by returning stream gulp can listen to events from the stream and knows when it is finished.
    });

    /**
     * Watch for changes in TypeScript, HTML and CSS files.
     */
    gulp.task(tasks.watch, function() {
        gulp.watch([paths.sourcePath + '/**/*.ts'], [tasks.buildTypeScript]).on('change', function(e) {
            console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
        });

        gulp.watch([paths.sourcePath + '/**/*.html', paths.sourcePath + '/**/*.css', paths.sourcePath + '/**/*.less'], [tasks.clientResources]).on('change', function(e) {
            console.log('Resource file ' + e.path + ' has been changed. Updating.');
        });
        // gulp.watch([paths.sourcePath + '/**/*.css', paths.sourcePath + '/**/*.less'], [tasks.clientResources, tasks.less]).on('change', function(e) {
        // 	console.log('Resource file ' + e.path + ' has been changed. Updating.');
        // });
    });

    /**
     * Build the project.
     */
    gulp.task(tasks.add, function(callback) {
        runSequence(tasks.buildTypeScript, tasks.clientResources, tasks.watch);
    });

    gulp.task(tasks.build, function(callback) {
        // runSequence(tasks.clean, tasks.buildTypeScript, tasks.clientResources, tasks.libs, tasks.css, tasks.less, callback);
        runSequence(tasks.clean, tasks.buildTypeScript, tasks.clientResources, tasks.libs, tasks.css, tasks.fonts, callback);
    });

    gulp.task(tasks.default, function() {
        // runSequence(tasks.clean, tasks.buildTypeScript, tasks.clientResources, tasks.libs, tasks.css, tasks.less, tasks.watch);
        runSequence(tasks.clean, tasks.buildTypeScript, tasks.clientResources, tasks.libs, tasks.css, tasks.fonts, tasks.watch);
    });

    // gulp.task(tasks.less, () => {
    // 	return gulp.src(paths.sourcePath + '/app/*.less')
    // 		.pipe(sourcemaps.init())
    // 		.pipe(less())
    // 		.pipe(sourcemaps.write())
    // 		.pipe(gulp.dest(paths.destPath  + '/css'));
    // });
};