module.exports = function(gulp, $, MODE_PRODUCT) {
    'use strict';

    const del = require('del'),
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        elixir = require("laravel-elixir"),
        sass = require('gulp-ruby-sass'),
        runSequence = require('run-sequence'),
        jsmin = require('gulp-jsmin'),
        gulpif = require('gulp-if'),
        cssmin = require('gulp-cssmin'),
        // nodemon = require('gulp-nodemon'),
        // gulpTypings = require('gulp-typings'),
        // elixir = require('laravel-elixir'),
        // less = require('gulp-less'),
        suffix = 'frontend';

    var paths = {
        'destPath': 'public/frontend',
        'sourcePath': 'resources/assets/frontend',
    };
    var tasks = {
        'clean': 'clean' + ':' + suffix,
        'buildTypeScript': 'buildTypeScript' + ':' + suffix,
        'build': 'build' + ':' + suffix,
        'compile': 'compile' + ':' + suffix,
        'clientResources': 'clientResources' + ':' + suffix,
        'libs': 'libs' + ':' + suffix,
        'css': 'css' + ':' + suffix,
        'watch': 'watch' + ':' + suffix,
        'default': suffix,
        'fonts': 'fonts' + ':' + suffix,
    };
    var licences = {
        // 'angular-chart': 'resources/vendor/angular-chart/LICENSE',
        'bootstrap': 'resources/vendor/bootstrap/LICENSE',
        'jquery': 'resources/vendor/jquery/MIT-LICENSE.txt',
        'jquery-ui': 'resources/vendor/jquery-ui/LICENSE.txt'
    };

    var fonts = [
        'resources/vendor/bootstrap/fonts/*.*',
        'resources/vendor/font-awesome/fonts/*.*'
    ];
    elixir.config.sourcemaps = false;

    //nested compact expanded compressed
    var sassConfig = {
        style: 'nested',
        compass: true,
        lineNumbers: true
    };

    if (MODE_PRODUCT) {
        sassConfig = {
            style: 'nested',
            compass: true,
            lineNumbers: false
        };
    }

    /**
     * Remove build directory.
     */
    gulp.task(tasks.clean, (cb) => {
        return del([
            paths.destPath + '/css',
            paths.destPath + '/fonts',
            paths.destPath + '/js',
            paths.destPath + '/libs',
        ], cb);
    });

    /**
     * Copy all required libraries into build directory.
     */
    gulp.task(tasks.libs, () => {

        return gulp.src([
                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/bootstrap/dist/js/bootstrap.min.js',
                paths.sourcePath + '/common/js/jquery.themepunch.tools.min.js',
                paths.sourcePath + '/common/js/jquery.themepunch.revolution.min.js',
                'node_modules/flexslider/jquery.flexslider-min.js',
                paths.sourcePath + '/common/js/jquery.prettyPhoto.js',
                'node_modules/respond.js/dest/respond.min.js',
                'node_modules/html5shiv/dist/html5shiv.min.js',
                paths.sourcePath + '/common/js/custom.js',
                'node_modules/sweetalert2/dist/sweetalert2.min.js',
                'node_modules/moment/min/moment.min.js', 'node_modules/moment/min/locales.min.js', 'node_modules/moment/min/moment-with-locales.min.js',
                'node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js',
                'node_modules/jquery-form-validator/form-validator/security.js',
                'node_modules/jquery-form-validator/form-validator/location.js',
                'node_modules/jquery-form-validator/form-validator/date.js',
                'node_modules/jquery-form-validator/form-validator/file.js',
                'node_modules/jquery-form-validator/form-validator/html5.js',
                'node_modules/jquery-form-validator/form-validator/jsconf.js',
                'node_modules/jquery-form-validator/form-validator/logic.js',
                'node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                'node_modules/aos/dist/aos.js',

                // 'node_modules/jquery-form-validator/form-validator/poland.js',
            ]) /* Glob required here. */
            .pipe(concat('frontend-common.js'))
            .pipe(gulp.dest(paths.destPath + '/libs'));
    });

    /**
     * Copy all required libraries into build directory.
     */
    gulp.task(tasks.css, () => {

        gulp.src([
                paths.sourcePath + '/common/css/less-style.css',
                paths.sourcePath + '/common/css/style.css',
                paths.sourcePath + '/common/css/ie-style.css'
            ]) /* Glob required here. */
            .pipe(gulpif(MODE_PRODUCT, cssmin()))
            .pipe(gulp.dest(paths.destPath + '/css'));

        return gulp.src([
                'node_modules/bootstrap/dist/css/bootstrap.min.css',
                paths.sourcePath + '/common/css/settings.css',
                // paths.sourcePath + '/common/css/jquery.themepunch.revolution.min.js',
                'node_modules/flexslider/flexslider.css',
                paths.sourcePath + '/common/css/prettyPhoto.css',
                // paths.sourcePath + '/common/css/styles.css',
                'node_modules/font-awesome/css/font-awesome.min.css',
                'node_modules/aos/dist/aos.css',
                // paths.sourcePath + '/common/css/less-style.css',
                // paths.sourcePath + '/common/css/style.css',
                'node_modules/sweetalert2/dist/sweetalert2.min.css',
                'node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
            ]) /* Glob required here. */
            .pipe(concat('frontend-common.css'))
            .pipe(gulp.dest(paths.destPath + '/css'));
    });

    /**
     * Compile TypeScript sources and create sourcemaps in build directory.
     */
    gulp.task(tasks.compile, () => {
        let task = gulp.src([
            paths.sourcePath + '/js/**/*.js'
        ]);
        // if (MODE_PRODUCT) {
        //     task.pipe(uglify());
        // }

        return task.pipe(gulpif(MODE_PRODUCT, jsmin()))
                    .pipe(gulp.dest(paths.destPath + '/js'));
    });

    gulp.task(tasks.clientResources, () => {

        sass(paths.sourcePath + '/sass/frontend-styles.scss', sassConfig)
            .pipe(gulp.dest(paths.destPath + '/css'));
    });

    gulp.task(tasks.watch, function() {

        gulp.watch([paths.sourcePath + '/js/**/*.js'], [tasks.compile]).on('change', function(e) {
            console.log('JavaScript file ' + e.path + ' has been changed. Compiling.');
        });

        gulp.watch([paths.sourcePath + '/**/*.scss'], [tasks.clientResources]).on('change', function(e) {
            console.log('Resource file ' + e.path + ' has been changed. Updating.');
        });

        gulp.watch([paths.sourcePath + '/**/*.css'], [tasks.css]).on('change', function(e) {
            console.log('Resource file ' + e.path + ' has been changed. Updating.');
        });
    });

    gulp.task(tasks.build, function(callback) {
        runSequence(tasks.clean, tasks.clientResources, tasks.libs, tasks.css, tasks.compile, tasks.fonts, callback);
    });

    gulp.task(tasks.default, function() {
        runSequence(tasks.clean, tasks.clientResources, tasks.libs, tasks.css, tasks.compile, tasks.fonts, tasks.watch);
    });

    /**
     * Copy fonts
     */
    gulp.task(tasks.fonts, () => {

        return gulp.src([
            'node_modules/bootstrap/dist/fonts/**',
            'node_modules/flexslider/fonts/**',
            'node_modules/font-awesome/fonts/**',
        ]).pipe(gulp.dest(paths.destPath + '/fonts'));
    });
};
