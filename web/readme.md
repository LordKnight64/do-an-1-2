## Config hosts
127.0.0.1	mysite.local
127.0.0.1	admin.mysite.local
127.0.0.1	api.mysite.local

## Install
1. mở cmd
2. gõ cd [đường dẫn mỗi máy]/web
3. chạy lệnh "npm install"
4. chạy lệnh "composer install"
5. Chạy lệnh "composer update davibennun/laravel-push-notification" nếu chưa từng chạy

## Run web
chạy file web/cmd_web_start.cmd

## chạy gulp
mở 1 cmd khác

## nếu đang làm admin
chạy lệnh "gulp admin"

## nếu đang làm frontend
chạy lệnh "gulp frontend"

## chạy web
## nếu đang làm admin
chạy link "admin.mysite.local:8000"

## nếu đang làm frontend
chạy link "mysite.local:8000"
