/**
 * Created by Moiz.Kachwala on 08-06-2016.
 */


"use strict";

const gulp = require("gulp"),
    plugins  = require('gulp-load-plugins')(),
    taskPath = './tasks/',
    args   = require('yargs').argv,
    taskList = require('fs').readdirSync(taskPath);
    // del = require("del"),
    // tsc = require("gulp-typescript"),
    // sourcemaps = require('gulp-sourcemaps'),
    // tsProject = tsc.createProject("tsconfig.json"),
    // tslint = require('gulp-tslint'),
    // concat = require('gulp-concat'),
    // runSequence = require('run-sequence'),
    // nodemon = require('gulp-nodemon'),
    // gulpTypings = require("gulp-typings"),
    // elixir = require('laravel-elixir'),
    // less = require('gulp-less');

var MODE_PRODUCT = false; 

MODE_PRODUCT = (args.production === undefined) ? false : true;
console.log('MODE_PRODUCT=' + MODE_PRODUCT);

taskList.forEach(function (taskFile) {
    // or .call(gulp,...) to run this.task('foobar')...
    console.log('taskFile', taskFile);
    require(taskPath + taskFile)(gulp, plugins, MODE_PRODUCT);
});

// /**
//  * Remove build directory.
//  */
// gulp.task('clean:admin', (cb) => {
//     return del(["public/admin"], cb);
// });

// gulp.task('clean:client', (cb) => {
//     return del(["public/admin"], cb);
// });

// /**
//  * Build Admin typescript
//  */
// gulp.task('build:admin', function () {
//     var tsProject = tsc.createProject('resources/assets/admin/tsconfig.json');
//     var tsResult = gulp.src('resources/assets/admin/**/*.ts')
//         .pipe(sourcemaps.init())
//         .pipe(tsProject());
//     return tsResult.js
//         .pipe(sourcemaps.write())
//         .pipe(gulp.dest('public/admin'));
// });

// /**
//  * Lint all custom TypeScript files.
//  */
// gulp.task('tslint', () => {
//     return gulp.src("resources/assets/admin/app/**/*.ts")
//         .pipe(tslint({
//             formatter: "prose"
//         }))
//         .pipe(tslint.report());
// });


// /**
//  * Compile TypeScript sources and create sourcemaps in build directory.
//  */
// gulp.task("compile", ["tslint"], () => {
//     let tsResult = gulp.src("resources/assets/admin/**/*.ts")
//         .pipe(sourcemaps.init())
//         .pipe(tsProject());
//     return tsResult.js
//         .pipe(sourcemaps.write("."))
//         .pipe(gulp.dest("public/admin"));
// });

// /**
//  * Copy all resources that are not TypeScript files into build directory. e.g. index.html, css, images
//  */
// gulp.task("clientResources:admin", () => {
//     return gulp.src([
//             "resources/assets/admin/**/*", 
//             "!**/*.ts", 
//             "!resources/assets/admin/typings", 
//             "!resources/assets/admin/typings/**", 
//             "!resources/assets/admin/*.json"
//         ])
//         .pipe(gulp.dest("public/admin"));
// });

// gulp.task("clientResources:client", () => {
//     return gulp.src([
//             "resources/assets/admin/**/*", 
//             "!**/*.ts", 
//             "!resources/assets/admin/typings", 
//             "!resources/assets/admin/typings/**", 
//             "!resources/assets/admin/*.json"
//         ])
//         .pipe(gulp.dest("public/admin"));
// });

// /**
//  * Copy all required libraries into build directory.
//  */
// gulp.task("libs:admin", () => {
//     return gulp.src([
//         'iFoodGo/**',
//         'core-js/client/**',
//         'zone.js/dist/zone.js',
//         'reflect-metadata/Reflect.js',
//         'reflect-metadata/Reflect.js.map',
//         'systemjs/dist/system.src.js',
//         "@angular/**/*.js", "@angular/**/*.map", "@angular/**/*.json",
//         "rxjs/**/*.js", "rxjs/**/*.map", "rxjs/**/*.json",
//         "moment/*.js","moment/*.ts","moment/*.json","moment/**/*.js",
//         "angular2-moment/*.js", "angular2-moment/*.map", "angular2-moment/*.json",
//         "ng2-bootstrap/**/*.js", "ng2-bootstrap/**/*.map", "ng2-bootstrap/**/*.json",
//         "ng2-translate/**/*.js", "ng2-translate/**/*.map", "ng2-translate/**/*.json",
//         "angular2-uuid/**/*.js", "angular2-uuid/**/*.map", "angular2-uuid/**/*.json",
//         // "ui-router-ng2/**",
//         "angular2-jwt/*.js", "angular2-jwt/*.map", "angular2-jwt/*.json",
//         // "auth0-js/**",
//         'object-assign/index.js',
//         "sweetalert2/**",
//         "ng2-select/**",
//     ], { cwd: "node_modules/**" }) /* Glob required here. */
//         .pipe(gulp.dest("public/admin/libs"));
// });

// gulp.task("libs:client", () => {
//     return gulp.src([
//         'iFoodGo/**',
//         'core-js/client/**',
//         'zone.js/dist/zone.js',
//         'reflect-metadata/Reflect.js',
//         'reflect-metadata/Reflect.js.map',
//         'systemjs/dist/system.src.js',
//         "@angular/**/*.js", "@angular/**/*.map", "@angular/**/*.json",
//         "rxjs/**/*.js", "rxjs/**/*.map", "rxjs/**/*.json",
//         "moment/*.js","moment/*.ts","moment/*.json","moment/**/*.js",
//         "angular2-moment/*.js", "angular2-moment/*.map", "angular2-moment/*.json",
//         "ng2-bootstrap/**/*.js", "ng2-bootstrap/**/*.map", "ng2-bootstrap/**/*.json",
//         "ng2-translate/**/*.js", "ng2-translate/**/*.map", "ng2-translate/**/*.json",
//         "angular2-uuid/**/*.js", "angular2-uuid/**/*.map", "angular2-uuid/**/*.json",
//         // "ui-router-ng2/**",
//         "angular2-jwt/*.js", "angular2-jwt/*.map", "angular2-jwt/*.json",
//         // "auth0-js/**",
//         'object-assign/index.js',
//         "sweetalert2/**",
//         "ng2-select/**",
//     ], { cwd: "node_modules/**" }) /* Glob required here. */
//         .pipe(gulp.dest("public/admin/libs"));
// });

// /**
//  * Copy all required libraries into build directory.
//  */
// gulp.task("css:admin", () => {
//     return gulp.src([
//         'bootstrap/dist/**/**',
//         "font-awesome/**",
//         "admin-lte/**/**/**",
//         "ionicons/**/**/**",
//         "angular2-toaster/**/**/**"
//     ], { cwd: "node_modules/**" }) /* Glob required here. */
//         .pipe(gulp.dest("public/admin/css"));
// });

// gulp.task("css:client", () => {
//     return gulp.src([
//         'bootstrap/dist/**/**',
//         "font-awesome/**",
//         "admin-lte/**/**/**",
//         "ionicons/**/**/**",
//         "angular2-toaster/**/**/**"
//     ], { cwd: "node_modules/**" }) /* Glob required here. */
//         .pipe(gulp.dest("public/admin/css"));
// });


// /**
//  * Install typings for server and client.
//  */
// gulp.task("installTypings", function () {
//     var stream = gulp.src(["./resources/assets/admin/typings.json"])
//         .pipe(gulpTypings(null)); //will install all typingsfiles in pipeline.
//     return stream; // by returning stream gulp can listen to events from the stream and knows when it is finished.
// });

// /**
//  * Watch for changes in TypeScript, HTML and CSS files.
//  */
// gulp.task('watch:admin', function () {
//     gulp.watch(["resources/assets/admin/**/*.ts"], ['build:admin']).on('change', function (e) {
//         console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
//     });

//     gulp.watch(["resources/assets/admin/**/*.html", "client/**/*.css"], ['clientResources:admin']).on('change', function (e) {
//         console.log('Resource file ' + e.path + ' has been changed. Updating.');
//     });
//     gulp.watch(["resources/assets/admin/**/*.css", "resources/assets/admin/**/*.less"], ['clientResources:admin', 'less:admin']).on('change', function (e) {
//         console.log('Resource file ' + e.path + ' has been changed. Updating.');
//     });
// });

// gulp.task('watch:client', function () {
//     gulp.watch(["resources/assets/admin/**/*.ts"], ['build:client']).on('change', function (e) {
//         console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
//     });

//     gulp.watch(["resources/assets/admin/**/*.html", "client/**/*.css"], ['clientResources:client']).on('change', function (e) {
//         console.log('Resource file ' + e.path + ' has been changed. Updating.');
//     });
//     gulp.watch(["resources/assets/admin/**/*.css", "resources/assets/admin/**/*.less"], ['clientResources:client', 'less:client']).on('change', function (e) {
//         console.log('Resource file ' + e.path + ' has been changed. Updating.');
//     });
// });

// /**
//  * Build the project.
//  */
// gulp.task("add:admin", function (callback) {
//     runSequence('build:admin', 'clientResources', 'watch');
// });

// gulp.task("build:admin", function (callback) {
//     runSequence('clean:admin', 'clientResources:admin', 'libs:admin', 'css:admin', 'less:admin', callback);
// });

// gulp.task("build:client", function (callback) {
//     runSequence('clean:client', 'clientResources:client', 'libs:client', 'css:client', 'less:client', callback);
// });


// gulp.task('default:admin', function () {
//     runSequence('clean', 'build:client', 'clientResources', 'libs', 'css', 'less', 'watch');
// });

// gulp.task('default:admin', function () {
//     runSequence('clean', 'build:client', 'clientResources', 'libs', 'css', 'less', 'watch');
// });


// gulp.task("less", () => {
//     return gulp.src('resources/assets/admin/app/*.less')
//       .pipe(sourcemaps.init())
//       .pipe(less())
//       .pipe(sourcemaps.write())
//       .pipe(gulp.dest('public/admin/css'));
// });